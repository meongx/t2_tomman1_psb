@extends('layout')

@section('content')
  @include('partial.alerts')
  <a href="/dashboard/provisioning/NAL" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
          <tr>
            <th width="10">
              NO
            </th>
            <th width="10%">
              ORDER STATUS
            </th>
            <th width="10%">
              SC / NDEM / NCLI
            </th>
            <th>
              WITEL / DATEK
            </th>
            <th>
              Tgl Reg
            </th>
            <th>
              Customer Name / Address
            </th>
            <th>
              K-Contact / Status
            </th>
          </tr>
          <?php
            $number = 1;
          ?>
          @foreach ($GetList as $List)
          <tr>
            <td>
              {{ $number++ }}.
            </td>
            <td>
              <a href="/dispatch/{{ $List->orderId ? :  $List->ID }}" class="label label-info">{{ $List->job }} {{ $List->uraian ? : 'Undispatch' }}</a>
              @if ($List->uraian <> "")
              <b>Dispatch : </b>{{ $List->updated_at }}
              @endif
            </td>
            <td>
              <a  href="/{{ $List->id_dt }}"><b>SC{{ $List->orderId  ? : 'None'  }}</b></a><br />
              <span class="label label-warning">{{ $List->jenisPsb ? : $List->ALPRO_NOSS   }}</span><br />
              <b>{{ $List->ndemPots ? : $List->ND_POTS }}</b><br />
              <b>{{ $List->ndemSpeedy ? : $List->ND_REFERENCE }}</b><br />
              NCLI : <b>{{ $List->Ncli ? : $List->NCLI }}</b><br />
            </td>
            <td>
              <b>{{ $List->area }}</b><br />
              {{ $List->alproname ? : $List->CMDF.'/'.$List->RKNAME.'/'.$List->DPNAME }}<br />
            </td>
            <td>
              {{ $List->orderDate ? : $List->date_created }}
            </td>
            <td>
              <b>{{ $List->orderName }}</b><br />
              ({{ $List->orderNotel ? : $List->ND_POTS }})<br />
              {{ $List->Jalan ? : $List->LVOIE }}
              {{ $List->No_Jalan ? : $List->NVOIE }}<br />
              {{ $List->Distrik }}
            </td>
            <td>
              <b>KContact : </b>{{ $List->Kcontact }}<br />
              <b>Status : </b>{{ $List->orderStatus  }}
            </td>
          </tr>
          @endforeach
      </table>
    </div>
    </div>
  </div>
@endsection
