@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    MS2N NOSSF
    <a href="/ms2n/sync/PS" class="btn btn-info">
      <span class="glyphicon glyphicon-refresh"> Sync Now</span>
    </a>
  </h3>
  <div>
    <div class="input-group">
      <div class="input-group-btn">
        <button type="button" class="btn btn-default aksi" aria-label="Left Align">
          <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
        </button>
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
        <ul class="dropdown-menu">
          <li><a href="#" id="daily">Daily</a></li>
          <li><a href="#" id="monthly">Monthly</a></li>
         
        </ul>
      </div><!-- /btn-group -->
      <div class="ins">
        <input type="text" class="form-control" id="tanggal" value="">
      </div>
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->


  <br/>
  <div class="panel panel-primary">
    <div class="panel-heading">REKAP STATUS MS2N CHANEL </div>
    <div class="panel-body">
      <div class="list-group fiber">
			 <div class="table-responsive">  
				       
			  <table class="table" border="0">
			    <thead>
			      <tr>
			        <th width="20">No</th>
			        <th width="100">Sebab</th>
			        <th width="100">DSHR</th>
			        <th width="100">AVG</th>
			        <th width="100">PLASA</th>
			        <th width="100">MI</th>
					<th width="100">OTHERS</th>
					
			        
			      </tr>
			    </thead>
			    <tbody>
						{{-- */
							$total_bjm = 0;
							$total_bjb = 0;
							$total_bln = 0;
							$total_ttg = 0;
							$total_others = 0;
							$total_total = 0;
						/* --}} 
				    @foreach($get_ps_harian as $no => $ps_harian) 
				    {{-- */
							$total_bjm = $total_bjm + $ps_harian->DSHR;
							$total_bjb = $total_bjb + $ps_harian->AVG;
							$total_bln = $total_bln + $ps_harian->PLASA;
							$total_ttg = $total_ttg + $ps_harian->MI;
							$total_others = $total_others + $ps_harian->OTHERS;
							
							
						/* --}} 
				   
				    <tr>
			        <td>{{ ++$no }}</td>
			        <td>{{ $ps_harian->Sebab }}</td>
			        <td><a href="/getdata2/{{ $ps_harian->Sebab }}/Digital Sales Home Reach">{{ $ps_harian->DSHR }}</a></td>
			        <td><a href="/getdata2/{{ $ps_harian->Sebab }}/Avengers">{{ $ps_harian->AVG }}</a></td>
			        <td><a href="/getdata2/{{ $ps_harian->Sebab }}/Telkom_Group_Store">{{ $ps_harian->PLASA }}</a></td>
			        <td><a href="/getdata2/{{ $ps_harian->Sebab }}/MI">{{ $ps_harian->MI }}</a></td>
			        <td><a href="/getdata2/{{ $ps_harian->Sebab }}/OTHERS">{{ $ps_harian->OTHERS }}</a></td>
			        
			      </tr>

			      @endforeach
			    </tbody>
			      <tr>
			        <th width="20">No</th>
			        <th width="100">Sebab</th>
			        <th width="100">{{ $total_bjm }}</th>
			        <th width="100">{{ $total_bjb }}</th>
			        <th width="100">{{ $total_bln }}</th>
			        <th width="100">{{ $total_ttg }}</th>
					<th width="100">{{ $total_others }}</th>
					
			        
			      </tr>
			  </table>
			 </div>
      </div>
    </div> 
  </div>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />
  <script>
    $(function(){
      var day = {
        format: "yyyy-mm-dd",
        viewMode: 0, 
        minViewMode: 0
      };
      var month = {
        format: "yyyy-mm",
        viewMode: 2, 
        minViewMode: 1
      };
      $("input").datepicker(day)
      	.on('changeDate',function(e){
	      	document.location = '/ms2n/PS/'+$(this).val();
	      });



      $("#daily").click(function(){
        $("input").remove();
        $(".ins").after("<input type=text class=form-control id=tanggal>");
        $("input").datepicker(day);
      });

      $("#monthly").click(function(){
        $("input").remove();
        $(".ins").after("<input type=text class=form-control id=tanggal>");
        $("input").datepicker(month);
      });
      $(".aksi").click(function(){
        var tgl = document.getElementById("tanggal").value;
        if(tgl == ""){
          alert("Can you please select the date? or i will smash u down!");
        }else{
          $(".loading").css({"visibility":"visible"});
          $.getJSON( "/ms2n/getJsonPs/" + tgl, function( data ) {
            var fiber = [];
            var dslam = [];
            var msan = [];
            $.each( data, function( key, val) {
              fiber.push( "<li class='list-group-item'>" + val.Nama + "</li>" );
              console.log(val);
            });
            $(".list-group").html(fiber);
            $(".loading").css({"visibility":"hidden"});
          });
        }
      });
        
    });
  </script>
@endsection
