@extends('layout')
@section('content')
	<table class="table table-bordered">
		<tr>
			<th>NDEM</th>
			<th>No. INternet</th>
			<th>ODP</th>
		</tr>

		@foreach($data as $dt)
			<tr>
				<td>{{ $dt->orderId }}</td>
				<td>{{ $dt->internet }}</td>
				<td>{{ $dt->nama_odp ?: $dt->alproname }}</td>
			</tr>
		@endforeach

	</table>
@endsection