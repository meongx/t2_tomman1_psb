@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    My Materials
  </h3>

  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <li><a href="/">Input Baru</a></li>
    <li><a href="/editable">Edit Laporan</a></li>
    <li class="active"><a href="/stok">Materials/NTE</a></li>  
  </ul>

  <div class="panel panel-default">
    <div class="panel-heading">Materials</div>
    <div class="panel-body">
      <div class="list-group">
        @foreach($list as $no => $data)
          <div class="list-group-item">
            <span class="label label-info">{{ ++$no }}</span>
            <span>{{ $data->id_item }}</span>
            <span class="label label-info badge">{{ $data->unit_item }}</span>
            <span class="label label-info badge">{{ $data->stok }}</span>
          </div>
        @endforeach
      </div>  
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">NTE</div>
    <div class="panel-body">
      <div class="list-group">
      @foreach($ntes as $no => $data)
        <div class="list-group-item">
          <span class="label label-info">{{ ++$no }}</span>
          <span>{{ $data->sn }}</span>
          <span class="label label-info badge">{{ $data->jenis_nte }}</span>
        </div>
      @endforeach
      </div>
    </div>
  </div>
@endsection
