      <div class="panel panel-primary">
        <div class="panel-heading">10. LAPORAN BATAL BULAN INI PER TEAM</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>TEAM</th>
                <th>BATAL</th>
                </tr>
              </thead>
              <tbody>
                {{-- */
                  $total = 0;
                /* --}}
                @foreach($sepuluh as $no => $te)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $te->k }}</td>
                  <td class="strong">{{ $te->batal }}</td>
                  </tr>   
                  {{-- */
                    $total = $total+$te->batal;
                  /* --}}
                @endforeach
                <tr class="strong"><td colspan="2">TOTAL</td><td>{{ $total }}</td></tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>