@extends('layout')

@section('content')
  @include('partial.alerts')

<a href="/migrasi/monitoring/form" class="btn btn-primary btn-sm">
  Tambah Cluster
</a>

<a href="/migrasi/monitoring" class="btn btn-default btn-sm">
  Kembali
</a>

<br><br>

<div class="panel panel-default">
    <div class="panel-body">
          <div class="table-responsive">          
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Cluster</th>
                    <td>Progress Akhir</td>
                  </tr>
                </thead>

                <tbody>
                    @foreach($clusters as $no=>$cluster)
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $cluster->nama_cluster }}</td>
                            <td>{{ $cluster->total }}% </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
          </div>
    </div>
</div>


@endsection