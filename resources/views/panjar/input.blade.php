@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id }}" />
    @endif
    <h3>
      <a href="/panjar" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Input Laporan
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-id">Penanggung Jawab</label>
      <input name="pj" type="text" id="input-id" class="form-control" value="<?= $data->pj ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-keperluan">Keperluan</label>
      <input name="keperluan" type="text" id="input-keperluan" class="form-control" value="<?= $data->keperluan ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-nominal">Nominal</label>
      <input name="nominal" type="text" id="input-nominal" class="form-control" value="<?= $data->nominal ?>" />
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>
  
  <form id="delete-form" method="post" autocomplete="off">
    <input name="_method" type="hidden" value="DELETE">
    @if (isset($data->id))
      <div style="margin:40px 0 20px">
        <button class="btn btn-danger">Hapus</button>
      </div>
    @endif
  </form>
  <script>
  $(function() {
    $('.btn-danger').click(function() {
      var sure = confirm('Yakin hapus data ?');
      if (sure) {
        $('#delete-form').submit();
      }
    })
  })
  </script>
@endsection
