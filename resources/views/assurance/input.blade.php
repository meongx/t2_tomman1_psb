@extends('tech_layout')

@section('content')
  @include('partial.alerts')
  <!-- <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script> -->
  <script src="/js/mapmarkerASR.js"></script>
  <div id="mapModal" class="modal fade">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button class="close" data-dismiss="modal">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <h4 class="modal-title">
                          Koordinat ODP (<span id="lonText">0</span>, <span id="latText">0</span>)
                      </h4>
                  </div>
                  <div class="modal-body">
                      <div id="mapView" style="height:400px;"></div>
                  </div>
                  <div class="modal-footer">
                      <button class="btn btn-default" data-dismiss="modal">Batal</button>
                      <button id="btnGetMarker" class="btn btn-primary">OK</button>
                  </div>
              </div>
          </div>
      </div>

  
  <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      @if($redamanIboster==NULL)
        <strong>Ukur Redaman IBOOSTER WO sebelum di UP</strong>
      @else
         <strong>Ukur Redaman IBOOSTER {{ $redamanIboster  }}, Order akan dikembalikan jika redaman tidak layak</strong>
      @endif
  </div>
        
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    @if (isset($data->id_pl))
      <input type="hidden" name="id" value="{{ $data->id_pl }}" />
    @endif
    <h3>
      <a href="/" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
       
      <a href="/grabIboosterbyIN/{{ $Ndem }}/assurance" class="btn btn-success">Ukur Ibooster</a>
    
      @if(session('auth')->level <> 19)
        <button class="btn btn-primary">Simpan</button>
      @endif
      UPDATE WO
    </h3>

    <input type="hidden" name="dispatch_id" id="dispatch_id" value="{{ $project->id_dt }}" />
    <input type="hidden" name="no_tiket" value="{{ $project->no_tiket }}" />
<div class="row">
  <!-- <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading" style="border-bottom-right-radius: 10px !important;border-bottom-left-radius: 10px !important;">
        <a class="btn btn-info" href="/nossaInx/{{ $project->Incident }}"><span class="glyphicon glyphicon-refresh"></span></a>
        Status Nossa
        <span class="label label-warning" style="font-size:12px">{{ $project->Status }}</span>
        <input type="hidden" name="status_nossa" value="{{ $project->Status }}" />
        {!! $errors->first('status_nossa', '<span class="label label-danger">:message</span>') !!}
      </div>
    </div>
  </div> -->
<div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        Workorder Information
      </div>
      <div class="panel-body">

    <div class="form-group">
      <label class="control-label" for="input-status">Status</label>
      <input name="status" type="hidden" id="input-status" class="form-control" value="{{ $data->status_laporan or '' }}" />
    </div>

    <div class="form-group">
      <label class="control-label" for="input-action">Action</label>
      <input name="action" type="hidden" id="input-action" class="form-control" value="{{ $data->action or '' }}" />
      {!! $errors->first('action', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group {{ $errors->has('penyebab') ? 'has-error' : '' }}">
      <label class="control-label" for="input-penyebab">Penyebab</label>
      <input name="penyebab" type="hidden" id="input-penyebab" class="form-control" value="{{ $data->idPenyebab or '' }}" />
      {!! $errors->first('penyebab', '<span class="label label-danger">:message</span>') !!}
    </div>

     <div class="form-group {{ $errors->has('konOdp') ? 'has-error' : '' }}">
      <label class="control-label" for="input-konOdp">Kondisi ODP</label>
      <input name="konOdp" type="hidden" id="input-konOdp" class="form-control" value="{{ $data->idKonOdp or '' }}" />
      {!! $errors->first('konOdp', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
        <label class="label-control">No. Telpon</label>
        <input type="text" name="noTelp" class="form-control" value="{{ $project->no_telp }}">
        {!! $errors->first('noTelp', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
	    <label class="control-label" for="input-idpel">{{ $project->Ndem ? :'a' }} </br>
        {{ $project->neND_TELP ? : '0' }} ~ {{ $project->neND_INT ? : '0' }}<br />
        {{ $project->sto ? : $project->neSTO ? : '' }}<br />
        {{ $project->neHEADLINE ? : '' }}<br />
        <?php
          $headline = explode('+',$project->headline);
          if (count($headline)>2) :
          for($i=0;$i<4;$i++){
            echo $headline[$i]."<br />";
          }
          echo "<br />Status <br />Link : ";
          if ($headline[7]=="ONLINE"){
            $status_link = "success";
          } else {
            $status_link = "danger";
          }
          echo '<span class="label label-'.$status_link.'">'.$headline[7].'</span><br />';
          echo ' INET : ';
          if ($headline[9]=="Start"){
            $status_link = "success";
          } else {
            $status_link = "danger";
          }
          echo '<span class="label label-'.$status_link.'">'.$headline[9].'</span><br />';
          echo $headline[10].'<br />';
        ?>
        <br />
        Hasil Ukur :
      <?php endif; ?>
    </div>
    <div class="form-group">
      <label class="control-label"><b>Catatan Pemasangan</b></label>
      <textarea name="catatan" class="form-control" rows="5"></textarea>
      {!! $errors->first('catatan', '<span class="label label-danger">:message</span>') !!}
    </div>

		  <div class="form-group">
        <button id="btnLoadMap" title="Beri tanda pada Peta" class="btn btn-default" data-toggle="tooltip" type="button">
          <i class="glyphicon glyphicon-map-marker"></i>
        </button><label for="" class="control-label"><button id="btn-gps" type="button" class="btn btn-sm btn-info">
          <i class="glyphicon glyphicon-map-marker"></i>
        </button> <b>Koordinat Pelanggan</b></label>
        <div>
          <input name="kordinat_pelanggan" id="kordinat_pelanggan" class="form-control" rows="1" value="{{ $data->kordinat_pelanggan or '' }}" onKeyPress="return goodchars(event,'0123456789-,',this)" />
        </div>
      </div>
      <div class="col-xs-12">{!! $errors->first('kordinat_pelanggan', '<span class="label label-danger">:message</span>') !!}</div>
		  <div class="form-group">
        <label class="control-label"><b>Nama ODP</b></label>
        <input name="nama_odp" class="form-control" id="nama_odp" rows="1" value="{{ $data->nama_odp or '' }}">
        {!! $errors->first('nama_odp', '<span class="label label-danger">:message</span>') !!}
      </div>
      <div class="form-group">
		    <label for="" class="control-label">
          <button id="btn-gps-odp" type="button" class="btn btn-sm btn-info">
            <i class="glyphicon glyphicon-map-marker"></i>
          </button>
          <b>Koordinat ODP</b>
        </label>
        <input name="kordinat_odp" id="kordinat_odp" class="form-control" rows="1" value="{{ $data->kordinat_odp or '' }}" />
      </div>
      <div class="col-xs-12">{!! $errors->first('kordinat_odp', '<span class="label label-danger">:message</span>') !!}</div>
    </div>
</div>
</div>

  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">Material & NTE</div>
      <div class="panel-body">
        <input type="hidden" name="materials" value="[]" />

      	  @if(session('auth')->level <> 19)
            <button data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info" type="button">
              <span class="glyphicon glyphicon-list"></span>
              Edit
            </button>
      	  @endif

        <input type="checkbox" value="Input Manual" name="nte-manual" id="nte-manual" checked="checked">Input Manual

        <div class="form-group">
          <label class="control-label" for="jenis_ont" >Jenis ONT</label>
          <input name="jenis_ont" type="hidden" id="jenis_ont" class="form-control" value="{{ $data->typeont ?: $project->jenis_ont }}"/>
          {!! $errors->first('jenis_ont','<p class="label label-danger">:message</p>') !!}
        </div>  

        <div class="form-group">
          <label class="control-label" for="input-nte">Nte ( STB / ONT )</label>
          <input type="text" name="nte" id="input-nte" class="form-control" value="{{ $ntes or '' }}"/>
        </div>
        <ul id="material-list" class="list-group">
          <li class="list-group-item" v-repeat="$data | hasQty ">
            <span class="badge" v-text="qty"></span>
            <strong v-text="id_item"></strong><br>
            <strong v-text="nama_item"></strong><br>
            <strong v-text="rfc"></strong><br>
            <strong>Terpakai : </strong> <strong v-text="jmlTerpakai"></strong><br>
            <strong>Saldo : </strong> <strong v-text="saldo"></strong>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        Log
      </div>
      <div class="panel-body">
        <ul class="list-group">
        @foreach ($data_log as $log)
          <li class="list-group-item">
            {{ $log->created_by }} - {{ $log->nama }}<br />
            {{ $log->laporan_status }} - {{ $log->created_at }}<br />
            {{ $log->catatan }}
          </li>
        @endforeach
        </ul>
      </div>
    </div>
  </div>
  <div class="col-sm-12" id="dokumentasi-row">
    <div class="panel panel-default" id="dokumentasi">
      <div class="panel-heading">Dokumentasi</div>
      <div class="panel-body">
        <div class="row text-center input-photos" style="margin: 20px 0">
          @foreach ($check_foto as $input)

            <div class="col-xs-6 col-sm-2">
              <?php
                $path  = "/upload3/asurance/{$project->id_dt}/$input->name";
                $path2 = "/upload2/asurance/{$project->id_dt}/$input->name";
                $th    = "$path-th.jpg";
                $th2   = "$path2-th.jpg";
                $img   = "$path.jpg";
                $img2  = "$path2.jpg";
                $flag  = "";
                $name  = "flag_".$input->name;
              ?>
              @if (file_exists(public_path().$th))
                <a href="{{ $img }}">
                  <img src="{{ $th }}" alt="{{ $input->name }}" width="100" />
                </a>
                <?php
                  $flag = 1;
                ?>
               @elseif (file_exists(public_path().$th2))
                  <a href="{{ $img2 }}">
                    <img src="{{ $th2 }}" alt="{{ $input->name }}" width="100" />
                  </a>
                  <?php
                    $flag = 1;
                  ?>
              @else
                <img src="/image/placeholder.gif" width="100" alt="" />
              @endif
              <br />
              <input type="text" class="hidden" name="flag_{{ $input->name }}" value="{{ $flag }}"/>
              <input type="file" class="hidden" name="photo-{{ $input->name }}" accept="image/jpeg" />

              <button type="button" class="btn btn-sm btn-info">
                <i class="glyphicon glyphicon-camera"></i>
              </button>
              <p>{{ str_replace('_',' ',$input->name) }}</p>
              {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
            </div>
          @endforeach
        </div>
      </div>
    </div>

  <br />
  </form>
</div>
  <br />
  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Material PSB</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <div class="form-group">
            <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
          </div>

          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data | hasSaldo">

              <strong v-text="id_item"></strong><br>
              <strong v-text="nama_item"></strong><br>
              <strong v-text="rfc"></strong><br>
              <p><strong>Saldo : </strong> <strong v-text="saldo"></strong></p>

<!--               <strong v-text="id_item"></strong>
              <p v-text="nama_item"></p> -->

              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <!-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit" disabled></button> -->
                <input v-on="change: onChange(this)" v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('plugins')
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
     function getkey(e){
      if (window.event)
         return window.event.keyCode;
      else if (e)
         return e.which;
      else
         return null;
      }

      function goodchars(e, goods, field){
      var key, keychar;
      key = getkey(e);
      if (key == null) return true;

      keychar = String.fromCharCode(key);
      keychar = keychar.toLowerCase();
      goods = goods.toLowerCase();

      // check goodkeys
      if (goods.indexOf(keychar) != -1)
          return true;
      // control keys
      if ( key==null || key==0 || key==8 || key==9 || key==27 )
         return true;

      if (key == 13) {
          var i;
          for (i = 0; i < field.form.elements.length; i++)
              if (field == field.form.elements[i])
                  break;
          i = (i + 1) % field.form.elements.length;
          field.form.elements[i].focus();
          return false;
          };
      // else return false
      return false;
      }

    $(function() {

      $("#input-penyebab").on("change",function(e){
        $("#dokumentasi").remove();
        $.ajax({
        dataType: "html",
        url: "/dokumentasi/"+e.val+"/"+$("#dispatch_id").val()+"/"+$("#input-action").val(),
        success: function (data) {
            console.log(data);
            location.reload();x
            // $("#dokumentasi-row").append(data);
        }});
      });

      $("#input-penyebab").select2({
        initSelection: function (element, callback) {
	                var data = { "id": "{{ $data->idPenyebab ? : ''}}", "text": "{{ $data->penyebab ? : ''}}" };
	                callback(data);
	        },
	      ajax : {
				url : "/assurance/inputAction/",
				dataType : "json",
        data: function() {
          return {
            action : $("#input-action").val()
          }
        },
				results : function (data){
					var myResults = [];
					$.each(data, function (index, item) {

	                myResults.push({
	                    'id': item.id,
	                    'text': item.text
	                });
	            });
	            return {
	                results: myResults
	            };
				}
			}
    });

    var dataOnt = [
        {"id":"ZTE", "text":"ZTE"},
        {"id":"HUAWEI", "text":"HUAWEI"},
        {"id":"ALU", "text":"ALU"}
    ];

    var ont = function() {
      return {
        data: dataOnt,
        placeholder: 'Input Jenis ONT',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#jenis_ont').select2(ont());

    $('#nte-manual').on('click', function(e){
        var me = $(this);
            if (me.is(":checked")){
                $('#input-nte').val('');

                var dataOnt = [
                                {"id":"ZTE", "text":"ZTE"},
                                {"id":"HUAWEI", "text":"HUAWEI"},
                                {"id":"ALU", "text":"ALU"}
                              ];

                var typeont = function() {
                    return {
                      data: dataOnt,
                      placeholder: 'Input Type ONT',
                      formatSelection: function(data) { return data.text },
                      formatResult: function(data) {
                        return  data.text;
                      }
                    }
                  }

                  $('#jenis_ont').select2(typeont());
            }
            else{
                $('#input-nte').val('');

                var dataOnt = <?= json_encode($nte);?>;
                var typeont = function() {
                    return {
                      data: dataOnt,
                      placeholder: 'Input Type ONT',
                      formatSelection: function(data) { return data.text },
                      formatResult: function(data) {
                        return  data.text;
                      }
                    }
                  }
                  $('#jenis_ont').select2(typeont());
            }
    })      

    $('#jenis_ont').on('click', function(){
        if ($('#nte-manual').is (":checked")){
          // 
        }
        else{
            var me = $(this);
                isi = me.val();
        
            $('#input-nte').val(isi);
        }

    });


      $("#nama_odp").inputmask("AAA-AAA-A{2,3}/999");
      var materials = <?= json_encode($materials) ?>;

      Vue.filter('hasQty', function(value) {
        return value.filter(function(a) { return a.qty > 0 });
      });

      Vue.filter('hasSaldo', function(value) {
        return value.filter(function(a) { return a.saldo > 0});
      });

      Vue.filter('doubleDigit', function(value) {
        var v = Number(value);
        if (v < 1) return '00';
        else if (String(v).length < 2) return '0' + v;
      });

      var listVm = new Vue({
        el: '#material-list',
        data: materials
      });

      var modalVm = new Vue({
        el: '#material-modal',
        data: materials,
        methods: {
          onChange : function(item){
            if (item.qty > item.saldo) item.qty=item.saldo;
          },
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;

            if (item.qty > item.saldo) item.qty = item.qty-1;
            console.log(item.qty);
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });

      $('#kordinat_pelanggan').bind('copy paste', function (e) {
         e.preventDefault();
      });

      var $btnGps = $('#btn-gps');
      var $btnGpsODP = $('#btn-gps-odp');

      var $kordinat_pelanggan = $('#kordinat_pelanggan');
      $btnGps.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_pelanggan.val('ERROR');
          return;
        }

        $kordinat_pelanggan.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
	        $kordinat_pelanggan.val(result.coords.latitude+','+result.coords.longitude);
/*
					$labelGps.val('(' + Math.round(result.coords.accuracy) + 'm) ' + result.coords.latitude + ' ' + result.coords.longitude);

          $('input[name=gps_accuracy]').val(result.coords.accuracy);
          $('input[name=gps_latitude]').val(result.coords.latitude);
          $('input[name=gps_longitude]').val(result.coords.longitude);
*/
        }, function(error) {
          $kordinat_pelanggan.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert('Tidak mendapat izin menggunakan GPS');
              break;

            case error.POSITION_UNAVAILABLE:
              alert('Gagal menghubungi satelit GPS');
              break;

            case error.TIMEOUT:
              $kordinat_pelanggan.val('ERROR: TIMEOUT');
              break;
          }
        });
      });
      var $kordinat_odp = $('#kordinat_odp');
      $btnGpsODP.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_odp.val('ERROR');
          return;
        }

        $kordinat_odp.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
	        $kordinat_odp.val(result.coords.latitude+','+result.coords.longitude);
/*
					$labelGps.val('(' + Math.round(result.coords.accuracy) + 'm) ' + result.coords.latitude + ' ' + result.coords.longitude);

          $('input[name=gps_accuracy]').val(result.coords.accuracy);
          $('input[name=gps_latitude]').val(result.coords.latitude);
          $('input[name=gps_longitude]').val(result.coords.longitude);
*/
        }, function(error) {
          $kordinat_odp.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert('Tidak mendapat izin menggunakan GPS');
              break;

            case error.POSITION_UNAVAILABLE:
              alert('Gagal menghubungi satelit GPS');
              break;

            case error.TIMEOUT:
              $kordinat_odp.val('ERROR: TIMEOUT');
              break;
          }
        });
      });

      $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);
          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });

      $('#submit-form').submit(function() {
        var result = [];
        materials.forEach(function(item) {
          // if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty});
          if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty, rfc: item.rfc});
        });
        $('input[name=materials]').val(JSON.stringify(result));
      });

      $('.modal-body').css({ maxHeight: window.innerHeight - 170 });

      var data = <?= json_encode($get_laporan_status) ?>;
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Status',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-status').select2(select2Options());

      var dataAction = <?= json_encode($get_laporan_action) ?>;
      var selectDataAction = function() {
        return {
          data: dataAction,
          placeholder: 'Input Action',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-action').select2(selectDataAction());

      var dataKonOdp = <?= json_encode($get_laporan_odp) ?>;
      var selectDataKonOdp = function() {
        return {
          data: dataKonOdp,
          placeholder: 'Input Kondisi ODP',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-konOdp').select2(selectDataKonOdp());

      // var dataAction = <?= json_encode($get_laporan_penyebab) ?>;
      // var selectDataAction = function() {
      //   return {
      //     data: dataAction,
      //     placeholder: 'Input Penyebab',
      //     formatSelection: function(data) { return data.text },
      //     formatResult: function(data) {
      //       return  data.text;
      //     }
      //   }
      // }
      // $('#input-penyebab').select2(selectDataAction());

      // var data =  <?=json_encode($nte) ?>;
      // var nte1 =  <?=json_encode($nte1) ?>;
      // $.extend(data, nte1);
      // var combo = $('#input-nte').select2({
      //   data: data,
      //   maximumSelectionSize: 2,
      //   multiple:true
      // });
      // $('#searchlist').btsListFilter('#searchinput', {itemChild: 'strong'});

    })
  </script>
@endsection
