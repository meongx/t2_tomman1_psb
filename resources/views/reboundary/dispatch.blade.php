@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <div class="col-md-12">
        <h3>
          <a href="/reboundary/list/{{ date('Y-m-d')}}" class="btn btn-sm btn-default">
            <span class="glyphicon glyphicon-arrow-left"></span>
          </a>
          @if($status==2)
            Dispatch WO REBOUNDARY [INX{{$tiket}}]
          @else
            Re-Dispatch WO REBOUNDARY [INX{{$tiket}}]
          @endif
        </h3>
    </div>

    <div class="form-group col-md-12">
      <label for="no_tiket">No Tiket</label>
      <input type="text" class="form-control" name="no_tiket" value="INX{{ $tiket }}" readonly>
      <input type="hidden" name="noTiketAsli" value="{{ $tiket }}">
    </div>

    <div class="form-group col-md-12 {{ $errors->has('refSc') ? 'has-error' : '' }}">
      <label for="refSc">References SC</label>
      <input type="text" class="form-control" name="refSc" value="{{ $getData->scid }}" readonly>
      {!! $errors->first('refSc','<p class=help-block>:message</p>') !!}
    </div>
  
    <div class="form-group col-md-12">
      <label class="control-label" for="input-regu">Alpro</label>
      <input name="alpro" type="hidden" id="input-alpro" class="form-control" />
    </div>

    <div class="form-group col-md-12">
      <label class="control-label" for="input-sektor">Sektor</label>
      <input name="input-sektor" type="hidden" id="input-sektor" class="form-control" value="{{ old('input-sektor') }}"  />
      {!! $errors->first('input-sektor', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group col-md-12">
      <label class="control-label" for="input-regu">Regu</label>
      <input name="id_regu" type="hidden" id="input-regu" class="form-control" value="{{ old('id_regu') }}" />
      {!! $errors->first('id_regu', '<span class="label label-danger">:message</span>') !!}
      {{-- <div class="col-xs-10">{!! $errors->first('id_regu', '<span class="label label-danger">:message</span>') !!}</div> --}}
    </div>

    <div class="form-group col-md-12">
      <label class="control-label" for="input-tgl">Tanggal</label>
      <input name="tgl" type="tgl" id="input-tgl" class="form-control" value="{{ date('Y-m-d') }}"/>
      {!! $errors->first('tgl', '<span class="label label-danger">:message</span>') !!}

    </div>

    <div class="form-group col-md-2 {{ $errors->has('tglOpen') ? 'has-error' : '' }}">
        <label class="control-label" for="tglOpen">Tanggal Open</label>
        <input name="tglOpen" type="tgl" id="tglOpen" class="form-control" value="{{ date('Y-m-d') }}" />  
        {!! $errors->first('tglOpen','<p class=help-block>:message</p>') !!}
    </div>

    <div class="form-group col-md-2 {{ $errors->has('jamOpen') ? 'has-error' : '' }}">
         <div class="input-group bootstrap-timepicker timepicker">
            <label class="label-control">Jam Open</label>
            <input id="jamOpen" name="jamOpen" type="text" class="form-control input-small" readonly>
            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
            {!! $errors->first('jamOpen','<p class=help-block>:message</p>') !!}
        </div>
    </div>

    <div style="margin:40px 0 20px" class="col-md-12">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  
  {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> --}}
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script>
    $(function() {
        var dataalpro = [
          {"id":"COPPER", "text":"COPPER"},
          {"id":"GPON", "text":"GPON"}
          ];

        var alpro = function() {
          return {
            data: dataalpro,
            placeholder: 'Input Alpro',
            formatSelection: function(data) { return data.text },
            formatResult: function(data) {
              return  data.text;
            }
          }
        }
        $('#input-alpro').select2(alpro());

         
        // 
          
        var day = {
          format : 'yyyy-mm-dd',
          viewMode: 0,
          minViewMode: 0
        };
   
        $('#input-tgl').datepicker(day).on('changeDate', function(e){
          $(this).datepicker('hide');
        });

        $('#tglOpen').datepicker(day).on('changeDate', function(e){
          $(this).datepicker('hide');
        });

        $('#jamOpen').timepicker({
            showMeridian : false,
            disableMousewheel : false,
            minuteStep : 1
        });

        var data_sektor= <?= json_encode($dataSektor) ?>;
        var sektor = function() {
          return {
            data: data_sektor,
            placeholder: 'Pilih Sektor',
            formatResult: function(data) {
            return '<strong style="margin-left:5px">'+data.text+'</strong>';
            }
          }
        }
        $('#input-sektor').select2(sektor());

        $("#input-regu").select2({
            placeholder: 'Pilih Regu',
            ajax : {
              url : "/assurance/inputRegu/",
              dataType : "json",
              data: function() {
                return {
                  sektor : $("#input-sektor").val()
                }
              },
              results : function (data){
                var myResults = [];
                $.each(data, function (index, item) {
                  myResults.push({
                        'id': item.id,
                        'text': item.text
                    });
                });
                return {
                    results: myResults,
                };
              }
            }
        });

    });

  </script>
@endsection
