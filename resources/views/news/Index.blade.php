@extends('layout')

@section('content')
<style>
.btn-thumb {
	position: relative;
	overflow: hidden;
}
.btn-thumb input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	min-width: 100%;
	min-height: 100%;
	font-size: 100px;
	text-align: right;
	filter: alpha(opacity=0);
	opacity: 0;
	outline: none;
	background: white;
	cursor: inherit;
	display: block;
}

#thumb-upload{
	width: 100%;
}
</style>
<script>
	$(document).ready( function() {
		$(document).on('change', '.btn-thumb :file', function() {
			var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [label]);
		});

		$('.btn-thumb :file').on('fileselect', function(event, label) {

			var input = $(this).parents('.input-group').find(':text'),
			log = label;

			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}

		});
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#thumb-upload').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#imgInp").change(function(){
			readURL(this);
		});
		document.getElementById('input-judul').value = "";
		document.getElementById('content').value = "";
	});	
</script>
<form method="post" enctype="multipart/form-data" id="news-form">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<label class="control-label" for="input-judul">Judul Berita</label>
	<input name="judul" type="text" id="input-judul" class="form-control new" autofocus required/>
	<br>
	<div class="form-group">
		<label class="control-label" for="input-judul">Thumbnail</label>
		<div class="input-group">
			<label class="btn btn-primary">
				Browse&hellip; <input type="file" id="imgInp" name="thumb-img" required accept=".png, .jpeg, .jpg" style="display: none;">
			</label>
		</div>
		<img id='thumb-upload'/>
	</div>
	<br>
	<textarea name="content" id="content"></textarea>
	<script type="text/javascript" src="/bower_components/ckeditor/ckeditor.js"></script>
	<script> CKEDITOR.replace( 'content' ,{
		filebrowserBrowseUrl : '/bower_components/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
		filebrowserUploadUrl : '/bower_components/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
		filebrowserImageBrowseUrl : '/bower_components/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
	});
</script>
<br>
<button class="btn btn-primary">Submit</button>
</form>
@endsection