<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
Use App\DA\TicketingModel;

class TicketingController extends Controller
{
  function index(Request $req){
    $customer = $req->input('customer');
    $ibooster = '';
    $gamas = '';
    $Calling_Station_Id = '';
    if ($req->input('customer')<>""){
      $get_customer_info = TicketingModel::get_customer_info($customer);
      $get_history_order = TicketingModel::get_history_order($customer);

      if (count($get_customer_info)<>0){
          $customer_info = $get_customer_info;
          if (count($get_customer_info)>0){
            $check_fitur = DB::table('fitur')->first();
            if ($check_fitur->ibooster == 1){
            	#$resultIbooster = $this->grabIboosterbyIN($getData->Incident,'dispatch');
              $ibooster = $this->grabIbooster($customer,0,'customer_info');
            } else {
              $ibooster = "";
        			#skip grabIbooster
        		}
            // $data =  (object) $ibooster[0];
            // $Calling_Station_Id = substr($data->Calling_Station_Id, 0,29);
            // $query_gamas = DB::SELECT('SELECT * FROM data_nossa_1 a WHERE a.Summary LIKE "%'.$Calling_Station_Id.'%" ');
            // if (count($query_gamas)>0){
            //   $gamas = "IMPACT GAMAS";
            // }
          }
      }
      else{
        return back()->with('alerts',[['type' => 'danger', 'text' => 'Data Tidak Ditemukan']]);
      }
    }
    return view('ticketing.home',compact('customer','customer_info','get_history_order','ibooster','gamas','Calling_Station_Id'));
  }

  public function listPersonal(){
    $auth = session('auth');
    $query = TicketingModel::listPersonal($auth);
    return view('ticketing.list',compact('query'));
  }

  public function create($id){
    $get_customer_info = TicketingModel::get_customer_info($id);
    $get_keluhan = DB::table('keluhan')->get();
    $customer_info = $get_customer_info;

    return view('ticketing.input',compact('customer_info','get_keluhan','id'));
  }

  public function dashboard(){
    $auth = session('auth');
    $query = TicketingModel::dashboard();
    $undispatch = TicketingModel::getJumlahUndispatch();
    // dd($undispatch);
    return view('ticketing.dashboard',compact('auth','query','undispatch'));
  }

  public function dashboardClose($date){
    $query = TicketingModel::dashboardClose($date);
    return view('ticketing.dashboardClose',compact('date','query'));
  }

  public function dashboardCloseList($status,$sektor,$periode){
    $getData = TicketingModel::dashboardCloseList($status,$sektor,$periode);
     $auth = session('auth');
    return view('ticketing.dashboardCloseList',compact('auth','date','getData','periode','sektor'));
  }

  public function dashboardList($sektor,$periode){
    $auth = session('auth');
    $getData = TicketingModel::dashboardDetail($sektor,$periode);
    return view('ticketing.dashboardList',compact('auth','sektor','periode','getData'));
  }

  public function createSave(Request $req){
    date_default_timezone_set('Asia/Makassar');
    $rules = array(
      'ndemSpeedy' => 'required',
      'orderName' => 'required',
      'alproname' => 'required',
      'sto' => 'required',
      'pelapor' => 'required',
      'kontak' => 'required'
    );
    $messages = [
      'ndemSpeedy.required'  => 'Silahkan isi kolom "NDEM SPEEDY" rekan!',
      'orderName.required'  => 'Silahkan isi kolom "Nama Pelanggan" rekan!',
      'alproname.required'  => 'Silahkan isi kolom "ODP" rekan!',
      'sto.required'  => 'Silahkan isi kolom "STO" Bang!',
      'pelapor.required'  => 'Silahkan isi kolom "Pelapor" rekan!',
      'kontak.required'  => 'Silahkan isi kolom "Kontak" rekan!',
    ];
    $input = $req->input();
    $validator = Validator::make($req->all(), $rules, $messages);
    $validator->sometimes('ndemSpeedy', 'required', function ($input) {
              return true;
          }); 
    $validator->sometimes('orderName', 'required', function ($input) {
              return true;
          });
    $serviceType = '';
    if($req->input('ndemSpeedy')==''){
          $serviceType = 'Voice';
    };
    if ($req->input('orderName')==""){
      echo "kosong";
    }
      if ($validator->fails()) {
            return redirect()->back()
                ->withInput($req->input())
                        ->withErrors($validator)
                        ->with('alerts', [
                          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan']
                        ]);
    };
    $auth = session('auth');
    $get = DB::table('data_nossa_1_log')->get();
    $int  = count($get);
    $in = 'INT'.$int;
    // cek no tiket

    $data = DB::table('data_nossa_1_log')->where('Incident',$in)->get();
    if (count($data) <> 0){
      $status = false;
      while($status==false){
          $get = DB::table('data_nossa_1_log')->get();
          $int  = count($get);
          $in = 'INT'.$int;

          $cekData = DB::table('data_nossa_1_log')->where('Incident',$in)->get();
          if (count($cekData)==0){
              $status = true;
          };
      };
    };

    $ser = $req->input('ndemPots').'_'.$req->input('ndemSpeedy');
    $getSer = DB::table('data_nossa_1_log')->where('Service_ID', $ser)->whereDate('Reported_Date',date('Y-m-d'))->first();
    if(count($getSer)==0){
        $Reported_Date = date('Y-m-d H:i:s');
        $save = DB::table('data_nossa_1_log')->insert([
          'Incident' => $in,
          'Customer_Name' => $req->input('orderName'),
          'Contact_Name' => $req->input('pelapor'),
          'Contact_Phone' => $req->input('kontak'),
          'Segment_Status' => $req->input('segment_status'),
          'Summary' => $req->input('ndemPots').'_'.$req->input('ndemSpeedy').' '.$req->input('keluhan').' '.$req->input('catatan'),
          'Owner_Group' => 'TA HD WITEL KALSEL',
          'Source' => 'TOMMAN',
          'Service_ID' => $req->input('ndemPots').'_'.$req->input('ndemSpeedy'),
          'Service_Type' => $req->input('kat_layanan'),
          'Datek' => $req->input('alproname'),
          'Reported_Date' => $Reported_Date,
          'Workzone' => $req->input('sto'),
          'Witel' => 'KALSEL (BANJARMASIN)',
          'Regional' => 'REG-6',
          'Incident_Symptom' => $req->input('keluhan').' '.$req->input('catatan'),
          'MANJA' => $req->input('tglManja').' '.$req->input('jamManja'),
          'JENIS_GGN' => $req->input('kat_gangguan'),
          'ODP' => $req->input('alproname'),
          'user_created' => $auth->id_karyawan,
          'no_internet'  => $req->input('internet')
        ]);

        $messaggio = "Open Ticket Tomman\n";
        $messaggio .= "===================\n";
        $messaggio .= "Created_By : ".session('auth')->nama."\n";
        $messaggio .= "Reported_Date : ".$Reported_Date."\n";
        $messaggio .= "Kategori :   ".$req->input('kat_gangguan')."\n";
        $messaggio .= "Incident : ".$in."\n";
        $messaggio .= "Segment_Status : ".$req->input('segment_status')."\n";
        $messaggio .= "Customer_Name : ".$req->input('orderName')."\n";
        $messaggio .= "Contact_Name : ".$req->input('pelapor')."\n";
        $messaggio .= "Contact_Phone : ".$req->input('kontak')."\n";
        $messaggio .= "Service_ID : ".$req->input('ndemPots').'_'.$req->input('ndemSpeedy')."\n";
        $messaggio .= "Datek : ".$req->input('alproname')."\n";
        $messaggio .= "Incident_Symptom : ".$req->input('keluhan').' '.$req->input('catatan')."\n";

        // grup sm tl hd assurance
        $chatID = "-294350654";

         Telegram::sendMessage([
          'chat_id' => $chatID,
          'text' => $messaggio
        ]);

        // grup plasa
        $chatID = "-8383667";

         Telegram::sendMessage([
          'chat_id' => $chatID,
          'text' => $messaggio
        ]);
    }

     return redirect('/ticketInfo/'.$in)->with('alerts', [
                ['type' => 'success', 'text' => '<strong>SUKSES</strong> membuat pengaduan dg No Tiket '.$in]
              ]);

    // print_r($req->input());
  }

  public function ticketInfo($id){
    $query = DB::table('data_nossa_1_log')->where('Incident',$id)->get();
    $customer_info = $query[0];
    return view('ticketing.info',compact('id','query','customer_info'));
  }

  public function grabIbooster($spd,$orderId,$menu){
    $ch = curl_init();
    //curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login_a.php');
    // curl_setopt($ch, CURLOPT_URL, 'https://ibooster.telkom.co.id/login.php');
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login1.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    curl_setopt($ch, CURLOPT_COOKIEJAR, 'ibooster.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $login = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($login));
    // dd($login);
    $input = $dom->getElementsByTagName('input')->item(0)->getAttribute("value");
    //$value = $input->item($i)->getAttribute("value");
    // dd($input);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login_code_a.php');
    // curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login1.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $akun = DB::table('akun')->where('id', '7')->first();
    $username = $akun->user;
    $password = $akun->pwd;
    curl_setopt($ch, CURLOPT_POSTFIELDS, "sha256=".$input."&username=".$username."&password=".$password."&dropdown_login=sso");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $rough_content = curl_exec($ch);

    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    //print_r($matches['cookie']);
    $cookiesOut = "";
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookie = null;
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/ukur_massal_speedy.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "nospeedy=".$spd."&analis=ANALISA");
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $result = curl_exec($ch);
    // dd($results);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($result, 0, $header_size);
    $result = substr($result, $header_size);
    $columns = array(
      1=>
      "ND",
      "IP_Embassy",
      "Type",
      "Calling_Station_Id",
      "IP_NE",
      "ADSL_Link_Status",
      "Upstream_Line_Rate",
      "Upstream_SNR",
      "Upstream_Attenuation",
      "Upstream_Attainable_Rate",
      "Downstream_Line_Rate",
      "Downstream_SNR",
      "Downstream_Attenuation",
      "Downstream_Attainable_Rate",
      "ONU_Link_Status",
      "ONU_Serial_Number",
      "Fiber_Length",
      "OLT_Tx",
      "OLT_Rx",
      "ONU_Tx",
      "ONU_Rx",
      "Framed_IP_Address",
      "MAC_Address",
      "Last_Seen",
      "AcctStartTime",
      "AccStopTime",
      "AccSesionTime",
      "Up",
      "Down",
      "Status_Koneksi",
      "Nas_IP_Address"
    );
    $dom = @\DOMDocument::loadHTML(trim($result));
    // print_r($result);
    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 3, $count = $rows->length; $i < $count; $i++)
    {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 1, $jcount = count($columns); $j < $jcount; $j++)
      {
          //echo $j." ";
          $td = $cells->item($j);
          if (is_object($td)) {
            $node = $td->nodeValue;
          } else {
            $node = "empty";
          }
          $data[$columns[$j]] = $node;
      }
      $data['order_id'] = $orderId;
      $data['user_created'] = @session('auth')->id_user;
      $data['menu'] = $menu;
      $result[] = $data;
    }
    return ($result);
  }

  public static function undispatch(){
    $pesan = 'List Ticketing Undispatch'."\n";
    $pesan .= '================================'."\n";

    $list = TicketingModel::getUndispatchList();
    $listSto = TicketingModel::getStoUndispatchList();
    foreach ($listSto as $sto){
        $pesan .= $sto->Workzone."\n";
        foreach($list as $lst){
            if ($sto->Workzone==$lst->Workzone){
                $pesan .= $lst->Incident.'- '.$lst->Workzone.' - '.$lst->Reported_Date."\n";
            }
        }

        $pesan .= "\n";

    }

    echo $pesan;

    // Telegram::sendMessage([
    //   'chat_id' => '102050936',
    //   'text' => $pesan
    // ]);
  }

  public function hapusTicket($id){
      DB::table('data_nossa_1_log')->where('ID',$id)->delete();

      return redirect('/assurance/search')->with('alerts',[['type' => 'success', 'text' => 'Tiket Berhasil Dihapus']]);
  }

  public function dashboardListUndispacth($status){
      $auth = session('auth');
      $getData = TicketingModel::dashboardDetailUndispatch($status);
      $sektor = 'UNDISPATCH';

      return view('ticketing.dashboardList',compact('auth','sektor','getData'));

  }
}
