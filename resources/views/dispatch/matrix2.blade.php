@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      text-align: center;
      vertical-align: middle;
    }
    .red {
      background-color: #FF0000;
      color : #FFF;
    }
    .label-greenx{
      background-color: #1abc9c;
    }
    .myindihome {
      border : 1px solid #000;
    }
    .label-darkgrey {
      background-color: #6b6b6b;

    }
    .label-bluepas {
      background-color : #fd79a8;
    }
    .label-purple {
      background-color : #800080;
    }
    .label-black {
      background-color: #000000;
    }

    .label-coklat {
      background-color: #A0522D;
    }
    .label-golden{
       background-color: #008B8B;
    }

    a {
      cursor: pointer;
    }
    .geser_kiri_dikit {
      position: relative;
      left : -12px;
    }
  </style>

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>

    <script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <div style="padding-bottom: 10px;">
    <h3>Matrix Workorder {{ $ketSektor }}</h3>
    <form method="get">
      SEKTOR :
      <select name="sektor">
        <option value="">--</option>
        <option value="ALL" <?php if ($area=="ALL") echo 'selected = "selected"'; else ''; ?>>ALL</option>
        <option value="PROV" <?php if ($area=="PROV") echo 'selected = "selected"'; else ''; ?>>PROV ONLY</option>
        <option value="INNER" <?php if ($area=="INNER") echo 'selected = "selected"'; else ''; ?>>INNER ONLY</option>
        <option value="OUTER" <?php if ($area=="OUTER") echo 'selected = "selected"'; else ''; ?>>OUTER ONLY</option>

        @foreach ($get_sektor as $sektor)
        <?php if ($sektor->sektor==$area) { $selected = 'selected="selected"'; } else { $selected = ''; }  ?>
        <option value="{{ $sektor->sektor }}" {{ $selected }}>{{ $sektor->sektor }}</option>
        @endforeach
      </select>

      DATE :
      <input type="text" name="date" id="input-tgl" value="{{ $date }}" />
      <input type="submit" />
    </form>
  </div>

  @if (count($get_area)>0)
  @foreach ($get_area as $result_area)
  @if (array_key_exists($result_area->chat_id, $data))
  <div class="panel panel-primary" id="matrix">
    <div class="panel-heading">MATRIX ORDER {{ $result_area->title }}</div>
    <div class="panel-body">
      <div class="list-group">
	    <div class="table-responsive">
        <?php
          if (strpos($result_area->title,'ASR') !== false){
            //echo "SISA TIKET : ";
          }
        ?>

        <table class="table table-striped table-bordered">
          <tr>
            <th width="10" rowspan="2">No</th>
            <th width="300" rowspan="2">Nama Tim</th>
            <th rowspan="2">LIST</th>
          </tr>
          <tr>
          	<th width="50">NP</th>
          	<th width="50">OGP</th>
          	<th width="50">KT</th>
            <th width="50">KP</th>
          	<th width="50">HR</th>
          	<th width="50">UP</th>
          	<th width="50">Total</th>

          </tr>
          <!-- <tr>
            <th width="30">ALL</th>
            <th width="30">A</th>
            <th width="30">F</th>
            <th width="30">ALL</th>
            <th width="30">A</th>
            <th width="30">F</th>
          </tr> -->

          <?php          

            $number = 1;
            $list = $data[$result_area->chat_id];

            $totalNp    = 0;
            $totalKt    = 0;
            $totalKp    = 0;
            $totalOgp   = 0;
            $totalHR   = 0;
            $totalUp    = 0;
            $grandTotal = 0;
         
            for ($i=0;$i<count($list);$i++){
              ?>
              @if( count($list[$i]['listWO'])<>0 ) 

              <tr>
                <td>{{ $number }}</td>
                     
                <td>
                	{{ $list[$i]['uraian'] }}  <span class="label label-primary">{{ @$list[$i]['mitra'] }}</span>
                  <span class="label label-success">{{ $list[$i]['nik1'] }} : {{ $list[$i]['ketNik1']  }}</span>
                  @if($list[$i]['nik2']<>'')
                    <span class="label label-success">{{ $list[$i]['nik2'] }} : {{ $list[$i]['ketNik2']  }}</span>
                  @endif
                </td>
                 
                <td>
                  <?php
                  		$np     	= 0;
      			         	$ogp 	    = 0;
      			         	$kt 	    = 0;
      			         	$kp 	    = 0;
                      $up       = 0;
      			         	$hr 	    = 0;
      			         	$total    = 0;
                   ?> 

                  @foreach ($list[$i]['listWO'] as $num => $wo)
                    <?php
                      if ($wo->grup=='UP'){
                          $up += 1;
                      }
                      elseif($wo->grup=='OGP'){
                      	  $ogp += 1;
                      }
                      elseif($wo->grup=="KT"){
                      	  $kt += 1;
                      }
                      elseif($wo->grup=="KP"){
                          $kp += 1;
                      }elseif($wo->grup=="HR"){
                      	  $hr += 1;
                      };

                      if($wo->status_laporan==6 || $wo->status_laporan==NULL){
                      	  $np += 1;
                      };

                      $total += 1;

                     
                      if ($wo->status_laporan==6 || $wo->status_laporan==NULL){
                        $label = 'default';
                      } else if ($wo->status_laporan <> 28 && $wo->status_laporan <> 29 && $wo->status_laporan <> 30 && $wo->status_laporan <> 7 && $wo->status_laporan<>5 && $wo->status_laporan<>6 && $wo->status_laporan<>1 && $wo->status_laporan<>4 && $wo->status_laporan<>31 && $wo->status_laporan<>37 && $wo->status_laporan<>38 && $wo->status_laporan<>43 && $wo->status_laporan<>44 && $wo->status_laporan<>45 && $wo->status_laporan<>48 && $wo->status_laporan<>50 && $wo->status_laporan<>46 && $wo->status_laporan<>67) {
                        $label = 'danger';
                      } else if ( $wo->status_laporan==5){
                        $label = 'warning';
                      } else if ( $wo->status_laporan==4){
                        $label = 'info';
                      } else if ($wo->status_laporan==1 || $wo->status_laporan==37 || $wo->status_laporan==38){
                        $label = 'success';
                      } else if ($wo->status_laporan==7){
                        $label = 'darkgrey';
                      } else if ($wo->status_laporan==28 || $wo->status_laporan==29 || $wo->status_laporan==67) {
                        $label = 'bluepas';
                      } else if ($wo->status_laporan==31){
                        $label = 'purple';
                      } else if ($wo->status_laporan==43 || $wo->status_laporan==44 ){
                        $label = 'black';
                      } else if ($wo->status_laporan==45 || $wo->status_laporan==48 || $wo->status_laporan==46){
                        $label = 'coklat';
                      } else if ($wo->status_laporan==50){
                        $label = 'golden';
                      }
                        else {
                        $label = 'default';
                      };

                                          
                      if ($wo->dispatch_by=='4'){
                          $noTiket = substr($wo->Ndem, 0, 2);
                          if ($noTiket=='IN'){
                              $tiket = $wo->Ndem;
                          }
                          else{
                              $tiket = 'FG '.$wo->Ndem;
                          };
                      }
                      else{
                          $tiket = $wo->Ndem;
                      };

                      if ($wo->action==1){
                        $wook = "• ".$tiket;
                      } else {
                        $wook = $tiket;
                      };

                      if ($wo->status_laporan<>6 && $wo->status_laporan<>null ){
                      		$ketStatus = $wo->laporan_status;
                      }
                      else{
                      		$ketStatus = 'NEED PROGRESS';
                      }
                      
                      if ($wo->qcondesk_order==""){
                      $wook .= "";
                      } else {
                      $wook .= " QC";
                      }

                      $jnsPsb = substr($wo->jenisPsb, 0, 2);
  
		                date_default_timezone_set('Asia/Makassar');
		                if ($wo->modified_at=='' || $wo->modified_at==NULL){
		                	$jamWo = $wo->updated_at;
		                }
		                else{
		                	$jamWo = $wo->modified_at;
		                };

		                if ($jamWo<>''){
		                    $awal  = date_create($jamWo);
		                    $akhir = date_create(); // waktu sekarang
		                    $diff  = date_diff( $awal, $akhir );
		                                        
		                    if($diff->d <> 0){
		                          $waktu = ' // '.$diff->d.' Hari | '.$diff->h.' Jam | '.$diff->i.' Menit';
		                    }
		                    else {
		                          $waktu = ' // '.$diff->h.' Jam | '.$diff->i.' Menit'; 
		                    }
		                }
		                else{
		                  $waktu = ' //';
		                }

		                if ($wo->status_laporan==1){
		                	$waktu = ' //';
		                }

                    ?>

                      @if ($wo->dispatch_by=='4')
                          <a data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Open Order</b> :<br />{{ $wo->orderDate }}<br /><b>Dispatch at</b> :<br />{{ $wo->updated_at }}<br /><b>Status</b> : <br />{{ $wo->laporan_status ? : 'NO UPDATE' }} / <small>{{ $wo->modified_at ? : $wo->created_at ? : '~' }}</small><br /><b>Jenis Psb : </b><br />{{ $wo->jenisPsb ?: '-'}}<br /><b>Jenis Layanan : </b><br />{{ $wo->jenis_layanan ?: '-'}}<br /><b>Catatan Teknisi</b> : <br />{{ $wo->catatan }}<br /><a href='/guarantee/{{ $wo->id_dt }}'>Detil</a>" class="label label-{{ $label }}">{{ $tiket }} // {{ $ketStatus }}{{ $waktu }}</a> 
                      @elseif ($wo->dispatch_by=='5')
                          <a data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Open Order</b> :<br />{{ $wo->orderDate ? : $wo->created_at }}<br /><b>Dispatch at</b> :<br />{{ $wo->updated_at }}<br /><b>Status</b> : <br />{{ $wo->laporan_status ? : 'NO UPDATE' }} / <small>{{ $wo->modified_at ? : $wo->created_at ? : '~' }}</small><br /><b>Jenis Psb : <br /></b>{{ $wo->jenisPsb ?: '-'}}<br /><b>Jenis Layanan : </b><br />{{ $wo->jenis_layanan ?: '-'}}<br /><b>Catatan Teknisi</b> : <br />{{ $wo->catatan }}<br /><a href='/{{ $wo->id_dt }}'>Detil</a>" class="label label-{{ $label }}">{{ 'MYIR-' }} {{ $wook }} // {{ $ketStatus }}{{ $waktu }}</a>
                      @elseif ($wo->dispatch_by=='6')
                          <a data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Open Order</b> :<br />{{ $wo->orderDate ? : $wo->created_at }}<br /><b>Dispatch at</b> :<br />{{ $wo->updated_at }}<br /><b>Status</b> : <br />{{ $wo->laporan_status ? : 'NO UPDATE' }} / <small>{{ $wo->modified_at ? : $wo->created_at ? : '~' }}</small><br /><b>Jenis Psb : <br /></b>{{ $wo->jenisPsb ?: '-'}}<br /><b>Jenis Layanan : </b><br />{{ $wo->jenis_layanan ?: '-'}}<br /><b>Catatan Teknisi</b> : <br />{{ $wo->catatan }}<br /><a href='/reboundary/{{ $wo->id_dt }}'>Detil</a>" class="label label-{{ $label }}">{{ 'INX' }} {{ $wook }} // {{ $ketStatus }}{{ $waktu }}</a>
                      @else
                          <a data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Open Order</b> :<br />{{ $wo->orderDate }}<br /><b>Dispatch at</b> :<br />{{ $wo->updated_at }}<br /><b>Status</b> : <br />{{ $wo->laporan_status ? : 'NO UPDATE' }} / <small>{{ $wo->modified_at ? : $wo->created_at ? : '~' }}</small><br /><b>Jenis Psb : </b><br />{{ $wo->jenisPsb ?: '-'}}<br /><b>Jenis Layanan : </b><br />{{ $wo->jenis_layanan ?: '-'}}<br /><b>Catatan Teknisi</b> : <br />{{ $wo->catatan }}<br /><a href='/{{ $wo->id_dt }}'>Detil</a>" class="label label-{{ $label }}">{{ $jnsPsb=='MO' ? 'SC' : '' }} {{ $wook }} // {{ $ketStatus }} {{ $waktu }}</a>
                      @endif<br />

                    <?php
              
                      ++$num;                      
                    ?>

                    
                  @endforeach
                  <?php
                      $totalNp  += $np;
                      $totalKp  += $kp;
                      $totalKt  += $kt;
                      $totalUp  += $up;
                      $totalOgp += $ogp;
                      $totalHR += $hr;
                      $grandTotal += $total;
                   ?>

                </td>
                    <td>{{ $np ? : '0'}}</td>
                    <td>{{ $ogp ? : '0' }}</td>
                    <td>{{ $kt ? : '0' }}</td>
                    <td>{{ $kp ? : '0' }}</td>
                    <td>{{ $hr ? : '0' }}</td>
                    <td>{{ $up ? : '0' }}</td>
                    <td>{{ $total ? : '0' }}</td>
              </tr>

              @endif

              <?php
              $number++;
            
            }
          ?>
         
          <tr>
              <td colspan="3">TOTAL</td>
              <td>{{ $totalNp }}</td>
              <td>{{ $totalOgp }}</td>
              <td>{{ $totalKt }}</td>
              <td>{{ $totalKp }}</td>
              <td>{{ $totalHR }}</td>
              <td>{{ $totalUp }}</td>
              <td>{{ $grandTotal }}</td>
          </tr>
        </table>

      </div>
    </div>
  </div>
</div>
@endif
@endforeach
@else
<div id="matrix"></div>
@endif





		<div class="panel-body">
		<script>
        $(document).ready(function(){
          $("[data-toggle='popover']").popover({html:true});
          var day = {
            format : 'yyyy-mm-dd',
            viewMode: 0,
            minViewMode: 0
          };

          $('#input-tgl').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
          });

        })


				var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
        var c=0;
        var minutes= 0;
        var t;
        var timer_is_on=0;

        function timedCount(element)
        {
        document.getElementById(element).innerHTML = minutes+' min '+c+' sec';
        c=c+1;
        if (c%60==0){
          minutes+=1;
          c=0;
        }
        t=setTimeout("timedCount()",1000);
        }

        function showElement(element){
          document.getElementById(element).innerHTML = "Detected";
        }


    </script>

						</div>
					</div>
  </div>
@endsection
