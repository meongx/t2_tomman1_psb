@extends('layout')

@section('content')
  @include('partial.alerts')
	<div class="panel panel-primary">
		<div class="panel-heading">List Plasa - Sales</div>
		<div class="panel-body table-responsive">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">	
						<a href="/dshr/plasa-sales/plasa/form" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span></a>
					</div>
										
					<ul class="nav nav-tabs" style="width: 100%;">
					  <li><a href="/dshr/plasa-sales/list-wo-by-sales">Transaksi</a></li>	
					  <li class="active"><a href="/dshr/plasa-sales/pencarian">Pencarian</a></li>							
					</ul>

					<br>
					
					<div id="isi">
					  	<form class="row" style="margin-bottom: 20px">
					      <div class="col-md-12 {{ $errors->has('cari') ? 'has-error' : '' }}">
					          <div class="input-group">
					            <input type="text" class="form-control" name="cari" placeholder="Masukan Myir" />
					              <span class="input-group-btn">
					                <button class="btn btn-primary" type="submit">
					                  <span class="glyphicon glyphicon-search"></span>
					                </button>
					              </span>
					          </div>
					          {!! $errors->first('cari','<p class=help-block>:message</p>') !!}
					      </div>
					  	</form>


						@if(count($dataUnDispatch)==0 && count($dataDispatch)==0)
							
						@else
						  	<table class="table table-bordered">
								<tr>	
									<th>Tgl Order</th>
									<th>Myir / SC</th>
									<th>Tgl Dispatch</th>
									<th>SC Sinkron</th>
									<th>Pelanggaan</th>
									<th>Alamat</th>
									<th>STO</th>
									<th>ODP</th>
									<th>Layanan</th>
									<th>PSB</th>
									<th>Paket Indihome</th>
									<th>Ket Dispatch</th>
									<th>Sales ID</th>		
									<th>Status Laporan</th>
									<th>Ket</th>
								</tr>

								@if(count($dataUnDispatch)<>0)
									@foreach($dataUnDispatch as $no=>$data)
										<tr>	
											<td>{{ $data->orderDate }}</td>
											<td>{{ $data->myir }}</td>
											<td>-</td>
											<td>-</td>
											<td>{{ $data->customer }}</td>
											<td>{{ $data->alamatLengkap }}</td>
											<td>{{ $data->sto }}</td>
											<td>{{ $data->namaOdp }}</td>
											<td>{{ $data->layanan }}</td>
											<td>{{ $data->psb ?: '-'}}</td>
											<td>{{ $data->paket_harga ?: '-' }}</td>									
											<td>Belum Terdispatch</td>
											<td>{{ $data->sales_id }}</td>
											<td>-</td>

											@if($data->ket_input==0)
												<td>
													PLASA
													<a href="/dshr/plasa-sales/cetak-plasa/{{ $data->id_wo }}" target="_blank" class="btn btn-info btn-sm">
														<i class="glyphicon glyphicon-print" aria-hidden="true"></i>
													</a>
												</td>
											@else
												<td>SALES</td>
											@endif
										</tr>
									@endforeach
								@endif

								@if(count($dataDispatch)<>0)
									@foreach($dataDispatch as $no=>$data)
										<tr>	
											<td>{{ $data->orderDate }}</td>
											<td>{{ $data->myirInput }}</td>
											<td>{{ $data->tglDispatch }}</td>
											<td>{{ $data->Ndem ?: '-'}}</td>
											<td>{{ $data->customer }}</td>
											<td>{{ $data->alamatLengkap }}</td>
											<td>{{ $data->sto_wo }}</td>
											<td>{{ $data->namaOdp }}</td>
											<td>{{ $data->layanan }}</td>
											<td>{{ $data->psb ?: '-'}}</td>
											<td>{{ $data->paket_harga ?: '-' }}</td>									

											<td>
												<label class="label label-danger btn-sm">{{ $data->uraian }} - {{ $data->title }}</label>
											</td>
											
											<td>{{ $data->sales_id }}</td>
											<td>{{ $data->laporan_status ?: 'NEED PROGRESS' }}</td>

											@if($data->ket_input==0)
												<td>
													PLASA
													<a href="/dshr/plasa-sales/cetak-plasa/{{ $data->id_wo }}" target="_blank" class="btn btn-info btn-sm">
														<i class="glyphicon glyphicon-print" aria-hidden="true"></i>
													</a>
												</td>
											@else
												<td>SALES</td>
											@endif
										</tr>
									@endforeach
								@endif								
								
							</table>
						@endif
									
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection

@section('plugins')
	<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  	<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
	<script>
		$(function(){
			var day = {
		      	  	format: 'yyyy-mm-dd',
		        	viewMode: 0,
		       	 	minViewMode: 0
		      	};
		    
		    $('#tglAll').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });

		    $('#woToday').on('click', function(e){
		    	e.preventDefault();
		    	var me  = $(this),
		    		url = me.attr('href');

		    	$.ajax({
		    		url: url,
		    		dataType: 'html',
		    		success: function(data){
		    			console.log(data);
		    			$('#isiKlik').html(data);
		    		}
		    	})
		    });

		    $('.woProgress').on('click', function(e){
		    	e.preventDefault();
		    	var me  = $(this),
		    		url = me.attr('href');
		    		
		    	$.ajax({
		    		url: url,
		    		dataType: 'html',
		    		success: function(data){
		    			console.log(data);
		    			$('#isiKlik').html(data);
		    		}
		    	})
		    	
		    })
		})
	</script>
@endsection