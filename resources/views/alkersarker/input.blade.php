@extends('layout')

@section('content')
  @include('partial.alerts')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">ALKER & SARKER // INPUT</div>
                <div class="panel-body">
                    
                    <label class="form-control" for="nama">Nama</label>
                    <input type="text" id="nama" class="form-control" />
                    
                    <label class="form-control" for="Qty">Qty</label>
                    <input type="text" id="Qty" class="form-control" />

                    <label class="form-control" for="SN">Serial Number</label>
                    <input type="text" id="SN" class="form-control" />

                    <label class="form-control" for="Status_Kepemilikan">Status Kepemilikan</label>
                    <input type="text" id="Status_Kepemilikan" class="form-control" />

                    <input type="submit" class="form-control btn-success" />
                </div>
            </div>
        </div>
    </div>
@endsection