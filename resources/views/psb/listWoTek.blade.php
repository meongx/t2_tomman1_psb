@extends('tech_layout')

@section('content')
  @include('partial.alerts')
  <style>
    .red {
      background-color: #FF0000;

    }
    .warning1 {
      background-color: #f39c12;
    }
    .warning2 {
      background-color: #e67e22;
    }
    .warning3 {
      background-color: #e74c3c;
    }


    .default {
      background-color: #888888;
    }
    .line {
      border-radius: 5px;
      padding : 3px;
    }
  </style>

  <br>
  <div class="list-group" style="margin-top:-20px;">
        @if(empty($list))
            <h3>Belum Ada Wo</h3>
        @endif

        @foreach($list as $data)
          <?php
            if ($data->rebUmur>84){
              $color = "warning3";
            } else if ($data->rebUmur>42 && $data->rebUmur<=84){
              $color = "warning2";
            } else if ($data->rebUmur>24 && $data->rebUmur<=42){
              $color = "warning1";
            } else {
              $color = "default";
            }
          ?>
          @if($data->dispatch_by == '4')
            <div href="/guarantee/{{ $data->id_dt }}" class="list-group-item">
          @elseif($data->dispatch_by != '2')
            <div href="/{{ $data->id_dt }}" class="list-group-item">
          @else
            <div href="/tiket/{{ $data->id_dt }}" class="list-group-item">
          @endif

           <!-- <div class="line {{ $color }}"></div> -->


           <?php
            if ($data->jenisPsb<>NULL && $data->dispatch_by<>4){
              $orderId = "SC.".$data->orderId;
              $jenisWO = $data->jenisPsb;
            } else if ($data->jenisPsb<>NULL && $data->dispatch_by==4){
              $orderId = "FG.".$data->orderId;
              $jenisWO = $data->jenisPsb;
            } else if ($data->ND<>NULL){
              $orderId = $data->ND;
              $jenisWO = "MIGRASI AS IS";
            } else if ($data->datek=="ONTOFFLINE"){
              $orderId = "";
              $jenisWO = "ONT OFFLINE";
            } else if ($data->dispatch_by==4){
              $orderId = "FG.".$data->orderId;
              $jenisWO = $data->jenisPsb;
            } else {
              $orderId = $data->no_tiket;
              $jenisWO = "ASSURANCE";
            };

           ?>
           <span class="label label-info">{{ $jenisWO }}</span>
           <span class="label label-info">{{ $data->laporan_status ? : "NO UPDATE" }}</span>

           @if ($data->no_tiket<>NULL)
           <span class="label label-info">{{ $data->rebUmur }} h</span>
           @endif
            <br />
            {{ $orderId ? : $data->no_tiket }}<br />
            @if ($data->no_tiket<>NULL)
              ORDER OPEN : {{ $data->TROUBLE_OPENTIME ? : ''  ? : $data->tglOpen}}
            @else
              ORDER OPEN : {{ $data->orderDate  }}
            @endif<br />

            STO : {{ $data->sto_dps ? : $data->STO ? : $data->sto_trim ? : $data->neSTO ? : "UNKOWN" }}<br />
            ALPRO : {{ $data->alproname ? : $data->DP ? : $data->RK_ROC ? : $data->neRK ? : '' }}<br />
            @if ($data->no_tiket<>NULL)
            @else
            NCLI : {{ $data->orderNcli ? : $data->NCLI ? : "UNKNOWN" }}<br />
            @endif
            <strong>{{ $data->ndemPots ? : $data->neND_TELP ? : $data->no_telp  ? : $data->ND ? : '' }} ~ {{  $data->neND_INT ? : $data->ndemSpeedy ? : $data->no_speedy ? : $data->ND_REFERENCE ? : ''}}</strong><br />
            LAYANAN : {{ $data->jenis_layanan ? : $data->LOKER_DISPATCH ? : $data->nePRODUK_GGN ? : "UNKNOWN" }}<br />
            <strong>{{ $data->orderName ? : $data->NAMA ? : $data->neLOKER_DISPATCH }} ({{ $data->orderKontak ? : $data->neHEADLINE }})</strong><br />
            <strong>Email Pelanggan : {{ $data->email ? : "~" }}</strong><br />
            {{ $data->orderCity ? : $data->LQUARTIER ? :  $data->KANDATEL ? : $data->neKANDATEL ? : "UNKNOWN" }}<br />
            @if ($data->jenisPsb<>NULL)
            LOCATION : {{ $data->lat }}, {{ $data->lon }}<br />
            @endif
            DURATION : {{ $data->duration ? : $data->rebUmur }} Jam<br />
            <strong>Koordinat Pelanggan : {{ $data->kordinatPel ?: '-' }}</strong><br>
            <strong>Alamat Sales : {{ $data->alamatSales ?: '-' }}</strong><br>
            <strong>Pelanggan : {{ $data->customer ?: '-' }}</strong><br>
            <strong>MYIR : {{ $data->myir ?: '-' }}</strong><br>


           @if ($data->status_gangguan<>NULL)
           <span class="label label-danger">OPEN</span>
           @else

           @endif
           <span>{{ $data->KcontactSC ? : $data->LVOIE ? : $data->rocKcontact ? : '' }}</span><br />
           <span>{{ $data->LCOM ? : '' }}</span>
           <span>{{ $data->DP ? : '' }}</span>
           <span>{{ $data->LQUARTIER ? : '' }}</span>

           @if ($data->jenisPsb<>NULL)
           MANJA : {{ $data->manja }}<br />
           @endif

           @if (!empty($data->ndmw))
               {{ $data->ndmw }} <br />
           @endif

           @if (!empty($data->jnslayananmw))
               JENIS LAYANAN : {{ $data->jnslayananmw }} <br />
           @endif

           @if (!empty($data->namamw))
               <b> {{ $data->namamw }} </b> <br />
           @endif

           @if (!empty($data->alamatmw))
               {{ $data->alamatmw }} <br />
           @endif

           @if (!empty($data->pic))
               {{ $data->pic }} <br />
           @endif

           @if (empty($data->ndemPots))
             <b> Keterangan Fullfillment Guarante : </b> <br />
             <b>{{ $data->ketFull ?: '~'}}</b>
           @endif

          <?php
            //if ($data->headline<>'') :
            //$headline = explode('+',$data->headline);
            // echo "<b><i>".$data->headline."</i></b><br />";
            //for($i=0;$i<4;$i++){
            //  echo $headline[$i]."<br />";
            //}
            //echo "Link : ";
            //if ($headline[7]=="ONLINE"){
            //  $status_link = "success";
            //} else {
            //  $status_link = "danger";
            //}
            //echo '<span class="label label-'.$status_link.'">'.$headline[7].'</span>';
            //echo ' INET : ';
            //if ($headline[9]=="Start"){
            //  $status_link = "success";
            //} else {
            //  $status_link = "danger";
            //}
            //echo '<span class="label label-'.$status_link.'">'.$headline[9].'</span>';
          //endif;
          ?>

          </div>
          <br />

        @endforeach
  </div>
@endsection
