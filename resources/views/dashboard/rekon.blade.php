@extends('layout')

@section('content')
  @include('partial.alerts')
 <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }

    .warna1{
      background-color: #9abcf4;
    }

    .habang{
      background-color: red;
      color : white;
      font-weight: bold;
    }

    .warna2{
      background-color: #e0962f;
    }

    .warna3{
      background-color: #3ec156;
    }

    .warna4{
      background-color: #fca4a4;
    }

    .warna5{
      background-color: #f2de5e ;
    }

    .warna6{
      background-color: #e2410b;
    }

    .warna7{
      background-color: #309bff;
    }

    .text1{
      color: white;
    }

    .link:link{
      color: white;
    }

    .link:visited{
      color: white;
    }
</style>

    <div class="row">
      <div class="col-sm-12">
        <h3>REKON MITRA TA {{ session('witel') }}</h3>
        <small>Periode {{ $date }}</small>
        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th>Nama Mitra</th>
            <th>Provisioning</th>
            <th>Migrasi</th>
            <th>Assurance</th>
            <th>Jumlah Pekerjaan</th>
            <!-- <th>DC > 75</th> -->
          </tr>
          <?php
            $total_pekerjaan = 0;
            $total_prov = 0;
            $total_migrasi = 0;
            $total_asr = 0;
            $total_dc = 0;
            $jumlah = 0;
            $total = 0;
          ?>
          @foreach ($rekonMitra as $data)
          <?php
            $total_pekerjaan += $data['jumlah'];
            $total_prov += $data['jumlah_prov'];
            $total_migrasi += $data['jumlah_migrasi'];
            $total_asr += $data['jumlah_asr'];
            $total_dc += $data['jumlahDc'];

          ?>
            <tr>
              <td><a href="/dashboard/listTimMitra/{{ $data['mitra'] ? : 'NONE' }}/{{ $date }}">{{ $data['mitra'] ? : 'NONE' }}</a></td>
              <td><a href="/dashboard/provisioningListMitra/{{ $data['mitra'] ? : 'NONE' }}/{{ $date }}">{{ $data['jumlah_prov'] }}</a></td>
              <td><a href="/dashboard/migrasiListMitra/{{ $data['mitra'] ? : 'NONE' }}/{{ $date }}">{{ $data['jumlah_migrasi'] }}</a></td>
              <td><a href="/dashboard/assuranceListMitra/{{ $data['mitra'] ? : 'NONE' }}/{{ $date }}">{{ $data['jumlah_asr'] }}</a></td>
              <td><a href="/dashboard/ListMitra/{{ $data['mitra'] ? : 'NONE' }}/{{ $date }}">{{ $data['jumlah']}}</a></td>
              <!-- <td><a href="/dashboard/ListMitra-jmldc/{{ $data['mitra'] ? : 'NONE' }}/{{ $date }}">{{ $data['jumlahDc'] }}</a></td> -->
            </tr>
          @endforeach
          <tr>
            <td>Total</td>
            <td><a href="/dashboard/provisioningListMitra/ALL/{{ $date }}">{{ $total_prov }}</a></td>
            <td><a href="/dashboard/migrasiListMitra/ALL/{{ $date }}">{{ $total_migrasi }}</a></td>
            <td><a href="/dashboard/assuranceListMitra/ALL/{{ $date }}">{{ $total_asr }}</a></td>
            <td>{{ $total_pekerjaan}}</td>
            <!-- <td><a href="/dashboard/ListMitra-jmldc/ALL/{{ $date }}">{{ $total_dc }}</a></td> -->

          </tr>
        </table>
      </div>
    </div>
    <div class="row">
    	<div id="col-sm-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				Hasil QC {{ $date }}
    			</div>
    			<div class="panel-body">
    				<table class="table">
    					<caption></caption>
    					<thead>
    						<tr>
    							<th rowspan="2">MITRA</th>
    							<th rowspan="2">JML_QC</th>
    							<th colspan="2">RUMAH</th>
    							<th colspan="2">TEKNISI</th>
    							<th colspan="2">ODP</th>
    							<th colspan="2">REDAMAN</th>
    							<th colspan="2">ONT</th>
    							<th colspan="2">BERITA ACARA</th>
                  <th colspan="2">TAGGING</th>
                </tr>
    						<tr>
    							<th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
    						</tr>
    					</thead>
    					<tbody>
    					<?php
	    					$jml_foto_berita_ok = 0;
	    					$jml_foto_berita_nok = 0;
                $jml_foto_rumah_ok = 0;
                $jml_foto_rumah_nok = 0;
                $jml_foto_teknisi_ok = 0;
                $jml_foto_teknisi_nok = 0;
                $jml_foto_odp_ok = 0;
                $jml_foto_odp_nok = 0;
                $jml_foto_redaman_ok = 0;
                $jml_foto_redaman_nok = 0;
                $jml_foto_ont_ok = 0;
                $jml_foto_ont_nok = 0;
                $jml_tagging_pelanggan_ok = 0;
                $jml_tagging_pelanggan_nok = 0;

	    					$total = 0;
	    				?>
    					@foreach ($get_qc_borneo as $qc_borneo)
    					<?php
	    					$foto_berita_nok = $qc_borneo->jumlah-$qc_borneo->foto_berita_ok;
	    					$foto_rumah_nok = $qc_borneo->jumlah-$qc_borneo->foto_rumah_ok;
	    					$foto_teknisi_nok = $qc_borneo->jumlah-$qc_borneo->foto_teknisi_ok;
	    					$foto_odp_nok = $qc_borneo->jumlah-$qc_borneo->foto_odp_ok;
	    					$foto_redaman_nok = $qc_borneo->jumlah-$qc_borneo->foto_redaman_ok;
	    					$foto_ont_nok = $qc_borneo->jumlah-$qc_borneo->foto_ont_ok;
	    					$tagging_pelanggan_nok = $qc_borneo->jumlah-$qc_borneo->tagging_pelanggan_ok;
    					?>
    						<tr>
    							<td>{{ $qc_borneo->mitra }}</td>
    							<td>{{ $qc_borneo->jumlah }}</td>
                  <td>{{ $qc_borneo->foto_rumah_ok }}</td>
    							<td class="habang">{{ $foto_rumah_nok }}</td>
                  <td>{{ $qc_borneo->foto_teknisi_ok }}</td>
                  <td class="habang">{{ $foto_teknisi_nok }}</td>
                  <td>{{ $qc_borneo->foto_odp_ok }}</td>
                  <td class="habang">{{ $foto_odp_nok }}</td>
                  <td>{{ $qc_borneo->foto_redaman_ok }}</td>
                  <td class="habang">{{ $foto_redaman_nok }}</td>
                  <td>{{ $qc_borneo->foto_ont_ok }}</td>
                  <td class="habang">{{ $foto_ont_nok }}</td>
                  <td>{{ $qc_borneo->foto_berita_ok }}</td>
    							<td class="habang">{{ $foto_berita_nok }}</td>
                  <td>{{ $qc_borneo->tagging_pelanggan_ok }}</td>
                  <td class="habang">{{ $tagging_pelanggan_nok }}</td>
              </tr>
    					<?php
	    					$total += $qc_borneo->jumlah;
	    					$jml_foto_rumah_ok += $qc_borneo->foto_rumah_ok;
	    					$jml_foto_rumah_nok += $foto_rumah_nok;
                $jml_foto_teknisi_ok += $qc_borneo->foto_teknisi_ok;
	    					$jml_foto_teknisi_nok += $foto_teknisi_nok;
                $jml_foto_odp_ok += $qc_borneo->foto_odp_ok;
	    					$jml_foto_odp_nok += $foto_odp_nok;
                $jml_foto_redaman_ok += $qc_borneo->foto_redaman_ok;
	    					$jml_foto_redaman_nok += $foto_redaman_nok;
                $jml_foto_ont_ok += $qc_borneo->foto_ont_ok;
	    					$jml_foto_ont_nok += $foto_ont_nok;
                $jml_foto_berita_ok += $qc_borneo->foto_berita_ok;
	    					$jml_foto_berita_nok += $foto_berita_nok;
                $jml_tagging_pelanggan_ok += $qc_borneo->tagging_pelanggan_ok;
	    					$jml_tagging_pelanggan_nok += $tagging_pelanggan_nok;
	    				?>
    					@endforeach
    					<tfoot>
    						<tr>
    							<td >TOTAL</td>
    							<td>{{ $total }}</td>
    							<td>{{ $jml_foto_rumah_ok }}</td>
    							<td class="habang">{{ $jml_foto_rumah_nok }}</td>
                  <td>{{ $jml_foto_teknisi_ok }}</td>
    							<td class="habang">{{ $jml_foto_teknisi_nok }}</td>
                  <td>{{ $jml_foto_odp_ok }}</td>
    							<td class="habang">{{ $jml_foto_odp_nok }}</td>
                  <td>{{ $jml_foto_redaman_ok }}</td>
    							<td class="habang">{{ $jml_foto_redaman_nok }}</td>
                  <td>{{ $jml_foto_ont_ok }}</td>
    							<td class="habang">{{ $jml_foto_ont_nok }}</td>
                  <td>{{ $jml_foto_berita_ok }}</td>
    							<td class="habang">{{ $jml_foto_berita_nok }}</td>
                  <td >{{ $jml_tagging_pelanggan_ok }}</td>
    							<td class="habang">{{ $jml_tagging_pelanggan_nok }}</td>
    						</tr>
    					</tfoot>
    					</tbody>
    				</table>

    			</div>
    		</div>
    	</div>
    </div>
    <!-- <div class="row">
      <div class="col-sm-6">
        <h3>MS2N NAL</h3>
        <small>Periode {{ $date }}</small>
        <table class="table table-responsive">
          <tr>
            <th rowspan="2">AREA</th>
            <th colspan="3">JUMLAH 3P</th>
            <th colspan="3">JUMLAH 2P</th>
            <th rowspan="2">JUMLAH</th>
          </tr>
          <tr>
            <th>ASG</th>
            <th>NOT</th>
            <th>JML</th>
            <th>ASG</th>
            <th>NOT</th>
            <th>JML</th>
          </tr>
          <?php
            $Jumlah_3P_ASG = 0;
            $Jumlah_3P_NOTASG = 0;
            $Jumlah_3P = 0;
            $Jumlah_2P_ASG = 0;
            $Jumlah_2P_NOTASG = 0;
            $Jumlah_2P = 0;
            $Jumlah = 0;
          ?>
          @foreach ($datams2n as $ms2n)
          <?php
            $Jumlah_3P_ASG      += $ms2n->Jumlah_3P_ASG;
            $Jumlah_3P_NOTASG   += $ms2n->Jumlah_3P_NOTASG;
            $Jumlah_3P          += $ms2n->Jumlah_3P;
            $Jumlah_2P_ASG      += $ms2n->Jumlah_2P_ASG;
            $Jumlah_2P_NOTASG   += $ms2n->Jumlah_2P_NOTASG;
            $Jumlah_2P          += $ms2n->Jumlah_2P;
            $Jumlah             += $ms2n->Jumlah;
          ?>
          <tr>
            <td>{{ $ms2n->Kandatel }}</td>
            <td><a href="/dashboard/listms2n/{{ $date }}/{{ $ms2n->Kandatel }}/3P/ASG">{{ $ms2n->Jumlah_3P_ASG }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/{{ $ms2n->Kandatel }}/3P/NOTASG">{{ $ms2n->Jumlah_3P_NOTASG }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/{{ $ms2n->Kandatel }}/3P/ALL">{{ $ms2n->Jumlah_3P }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/{{ $ms2n->Kandatel }}/2P/ASG">{{ $ms2n->Jumlah_2P_ASG }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/{{ $ms2n->Kandatel }}/2P/NOTASG">{{ $ms2n->Jumlah_2P_NOTASG }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/{{ $ms2n->Kandatel }}/2P/ALL">{{ $ms2n->Jumlah_2P }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/{{ $ms2n->Kandatel }}/ALL/ALL">{{ $ms2n->Jumlah }}</a></td>
          </tr>
          @endforeach
          <tr>
            <td>Total</td>
            <td><a href="/dashboard/listms2n/{{ $date }}/ALL/3P/ASG">{{ $Jumlah_3P_ASG }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/ALL/3P/NOTASG">{{ $Jumlah_3P_NOTASG }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/ALL/3P/ALL">{{ $Jumlah_3P }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/ALL/2P/ASG">{{ $Jumlah_2P_ASG }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/ALL/2P/NOTASG">{{ $Jumlah_2P_NOTASG }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/ALL/2P/ALL">{{ $Jumlah_2P }}</a></td>
            <td><a href="/dashboard/listms2n/{{ $date }}/ALL/ALL/ALL">{{ $Jumlah }}</a></td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <h3>MS2N MIGRASI</h3>
        <small>Periode {{ $date }}</small>
        <table class="table table-responsive">
          <tr>
            <th>AREA</th>
            <th>ASSIGN</th>
            <th>NOT</th>
            <th>JUMLAH</th>
          </tr>
          @foreach ($datams2nmigrasi as $ms2nmigrasi)
          <tr>
            <td>{{ $ms2nmigrasi->datel }}</td>
            <td>{{ $ms2nmigrasi->jumlah_asg }}</td>
            <td>{{ $ms2nmigrasi->jumlah_notasg }}</td>
            <td>{{ $ms2nmigrasi->jumlah }}</td>
          </tr>
          @endforeach
        </table>
      </div> -->
    </div>
    <br />
    <br />
@endsection
