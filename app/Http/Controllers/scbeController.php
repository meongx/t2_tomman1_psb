<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\scbe;
use DB;

class scbeController extends Controller
{
    public function seacrhForm(Request $req){
    	// $this->validate($req,[
    	// 	'scbeq'	=> 'numeric'
    	// ],[
    	// 	'scbeq.numeric'	=> 'Inputan Angka',
    	// ]);

    	$datas = [];
    	$list  = [];
    	if($req->has('scbeq')){
    		$cari  = $req->input('scbeq');
    		$datas = scbe::cariMyir($cari);
    		$ket   = '0';
    		$sc    = 0;
    		$myirAda = 0;

    		$list = array();
    		if(count($datas)){
    			$list = [
    				'myir'			=> $datas[0]->myir,
					'customer'		=> $datas[0]->customer,
					'jenis_layanan' => $datas[0]->layanan,
					'kordinatPel'   => $datas[0]->koorPelanggan,
					'alamatSales'   => $datas[0]->alamatSales ?: $datas[0]->alamatLengkap,
					'namaOdp'    	=> $datas[0]->namaOdp,
					'odpByTeknisi'	=> $datas[0]->odpByTeknisi,
					'picPelanggan'  => $datas[0]->picPelanggan,
					'idMyir'		=> $datas[0]->idMyir,
					'Ndem'			=> $datas[0]->Ndem,
					'uraian'		=> $datas[0]->uraian,
					'tgl'			=> $datas[0]->tgl,
					'laporan_status'=> $datas[0]->laporan_status,
					'sc'		    => $datas[0]->sc,
					'id'			=> $datas[0]->id_dt,
					'ketInput' 		=> $datas[0]->ket_input,
					'ket'			=> $datas[0]->ket,
					'no_internet'   => $datas[0]->no_internet,
					'no_telp' 		=> $datas[0]->no_telp,
					'addSc'			=> '0',
					'laporan_status_id' => $datas[0]->laporan_status_id
    			];

    			$myirAda = 0;
    		}
    		else{
    			$datas = scbe::getMyirByMyir($cari);
    			if(count($datas)){
	    			$list = [
	    				'myir'			=> $datas->myir,
						'customer'		=> $datas->customer,
						'jenis_layanan' => $datas->layanan,
						'kordinatPel'   => $datas->koorPelanggan,
						'alamatSales'   => $datas->alamatLengkap,
						'namaOdp'    	=> $datas->namaOdp,
						'odpByTeknisi'	=> '-',
						'picPelanggan'  => $datas->picPelanggan,
						'idMyir'		=> $datas->idMyir,
						'Ndem'			=> '-',
						'uraian'		=> '-',
						'tgl'			=> '-',
						'laporan_status'=> '-',
						'sc'		    => $datas->sc,
						'id'			=> $datas->id,
						'ketInput' 		=> $datas->ket_input,
						'ket'			=> $datas->ket,
						'no_internet'   => $datas->no_internet,
						'no_telp' 		=> $datas->no_telp,
						'addSc'			=> '1',
						'laporan_status_id' => '-'
	    			];

    				$myirAda = 1;
    			}

    		}

    		if (count($datas)){
	    		// if(@$datas[0]->ket==0){
	    		// 	$cekSc = DB::table('Data_Pelanggan_Starclick')->where('kcontact','LIKE','%'.$cari.'%')->whereNotIN('orderStatus',['UNSC','REVOKE ORDER UNSC','REVOKE ORDER','CANCEL COMPLETED','Cancel Order'])->first();
	    		// 	if(count($cekSc)){
	    		// 		$sc = 1;
	    		// 	}
	    		// };
    		}

    		// if (empty($datas)){
    		// 	$ada = scbe::getMyirByMyir($cari);
    		// 	if ($ada){
    		// 		$ket = $ada[0]->ket;
	    	// 		// $datas = scbe::cariMyirStatusSatu($ada->myir);
	    	// 		$datas = $ada;
	    	// 		$sc = 1; 
    		// 	};
    		// }

    		// sementra starclick error
    		// dd($list);


    	};

    	return view('scbe.search',compact('ket', 'sc', 'list', 'myirAda'));
    }

    public function input(Request $req){
    	$regu = DB::select('
	      SELECT a.id_regu as id,a.uraian as text, b.title
	      FROM regu a
	        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
	      WHERE
	        ACTIVE = 1
	    ');

	    $get_manja_status = DB::SELECT('
	      SELECT
	        a.manja_status_id as id,
	        a.manja_status as text
	      FROM
	        manja_status a
	    ');

	     $dataSektor = DB::SELECT('
	      SELECT
	      a.chat_id as id,
	      a.title as text
	      FROM
	        group_telegram a
	      WHERE
	      	a.Witel_New = "'.session('witel').'"
	    ');

	    $sto = DB::select('SELECT sto as id, sto as text from maintenance_datel where witel = "'.session('witel').'"');
	    $odpSektor = '';
	    $odp = '';
	    $lat = '';
	    $long = '';
	    if ($req->has('searchOdp')){
	    	$cari = $req->input('searchOdp');

	    	$sql = 'SELECT b.title, a.alproname, c.LATITUDE, c.LONGITUDE FROM
	    			alpro_owner a
	    			LEFT JOIN group_telegram b ON a.nik = b.TL_NIK
	    			LEFT JOIN 1_0_master_odp c ON a.alproname = c.ODP_NAME
	    			WHERE a.alproname = "'.$cari.'" GROUP BY b.title';

	    	$odpSektor = DB::SELECT($sql);
	    	$odp = $cari;

	    	if (count($odpSektor)<>0){
	    		$lat  = $odpSektor[0]->LATITUDE;
	    		$long = $odpSektor[0]->LONGITUDE;
	    	}

	    };

	   	$dataSales = DB::select('SELECT id, kode_sales, nama_sales, kode_sales as text FROM psb_sales where ket=1');
    	return view('scbe.input',compact('regu', 'get_manja_status', 'sto', 'odpSektor', 'odp', 'lat', 'long', 'dataSektor', 'dataSales'));
    }

    public function dispatchSimpan(Request $req){
  		date_default_timezone_set('Asia/Makassar');
    	$this->validate($req,[
    		'myir'			=> 'numeric',
    		'nmCustomer'	=> 'required',
    		'picPelanggan'	=> 'required',
    		'picPelanggan'	=> 'numeric|digits_between:11,14',
    		'nama_odp'		=> 'required',
    		'input-sektor'	=> 'required',
    		'id_regu'		=> 'required',
    		'sales'			=> 'required'
    	],[
    		'myir.numeric'	=> 'Inputan Angka',
    		'nmCustomer.required'	=> 'Nama Pelanggan Diisi Bang !',
    		'picPelanggan.required' => 'PIC Pelanggan Diisi Bang !',
    		'nama_odp.required'		=> 'ODP Diisi Bang ! ',
    		'input-sektor.required'	=> 'Sektor Diisi Bang !',
    		'id_regu.required'		=> 'Regu Diisi',
    		'picPelanggan.numeric'  => 'Inputan Hanya Angka',
    		'picPelanggan.digits_between' => 'Inputan Diperbolehan 11 - 14 Digit',
    		'sales.required'		=> 'Sales Diisi'
    	]);

	    // insert into log status
	    $auth = session('auth');
    	$exists = DB::select('
	      SELECT a.*,b.status_laporan, c.mainsector
	        FROM dispatch_teknisi a
	      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
	      LEFT JOIN regu c ON a.id_regu = c.id_regu
	      WHERE a.Ndem = ?
	    ',[
	      $req->myir
	    ]);

	    $status_laporan = 6;
    	if (count($exists)>0){
			if ($exists[0]->status_laporan==4){
				$status_laporan = 4;
			}
    	};

	    DB::transaction(function() use($req, $auth, $status_laporan) {
	       DB::table('psb_laporan_log')->insert([
	         'created_at'      => DB::raw('NOW()'),
	         'created_by'      => $auth->id_karyawan,
	         'status_laporan'  => $status_laporan,
	         'id_regu'         => $req->input('id_regu'),
	         'catatan'         => "DISPATCH at ".date('Y-m-d'),
	         'Ndem'            => $req->myir
	       ]);
	     });

	     DB::transaction(function() use($req, $auth) {
	       DB::table('dispatch_teknisi_log')->insert([
	         'updated_at'          => DB::raw('NOW()'),
	         'updated_by'          => $auth->id_karyawan,
	         'tgl'                 => $req->input('tgl'),
	         'id_regu'              => $req->input('id_regu'),
	         'manja'               => $req->input('manja'),
	         'manja_status'        => $req->input('manja_status'),
	         'updated_at_timeslot' => $req->input('timeslot'),
	         'Ndem'                => $req->myir
	       ]);
	     });

	    // reset status if redispatch
	    if (count($exists)) {
	      $data = $exists[0];
	      DB::transaction(function() use($req, $data, $auth, $status_laporan) {
	        DB::table('psb_laporan')
	          ->where('id_tbl_mj', $data->id)
	          ->update([
	            'status_laporan' => $status_laporan
	          ]);
	          DB::table('psb_laporan_log')->insert([
	             'created_at'      => DB::raw('NOW()'),
	             'created_by'      => $auth->id_karyawan,
	             'status_laporan'  => $status_laporan,
	             'id_regu'         => $req->input('id_regu'),
	             'catatan'         => "REDISPATCH",
	             'Ndem'            => $req->myir,
	             'psb_laporan_id'  => $data->id
	           ]);
	      });

	      DB::transaction(function() use($req, $data, $auth) {
	        DB::table('dispatch_teknisi')
	          ->where('id', $data->id)
	          ->update([
	            'updated_at'            => DB::raw('NOW()'),
	            'updated_by'            => $auth->id_karyawan,
	            'tgl'                   => $req->input('tgl'),
	            'updated_at_timeslot'   => $req->input('timeslot'),
	            'id_regu'               => $req->input('id_regu'),
	            'jenis_layanan'         => $req->input('jenis_layanan'),
	            'step_id'               => "1.0",
	            'kordinatPel'           => $req->input('kordinatPel'),
	            'alamatSales'           => $req->alamatSales
	          ]);
	      });
	      scbe::simpanDispatch($req,'1');
	    }
	    else {
 	     // cari berdsarkan pic kalo sudah ad sinkron kan
	   	 $getPic = DB::table('psb_myir_wo')->where('picPelanggan',$req->picPelanggan)->first();
	     if ($getPic){
	    	if ($getPic->ketMyirGangguan==1)
	        return back()->with('alerts',[['type'=>'danger','text'=>'Pic Pelanggan sudah Terdispatch di Myir - '.$getPic->myir.'
	            <a href="/sinkron-myir/'. $getPic->picPelanggan .'/'.$req->myir.'" class="btn btn-primary">Sinkronkan MYIR</a>
	        ']]);
	     };

	     // cek myir kalo sudah ada
	     $getMyirExist = DB::table('psb_myir_wo')->where('myir',$req->myir)->first();
	     if(count($getMyirExist)){
	     	return back()->with('alerts',[['type' => 'danger', 'text' => 'Myir Sudah Ada, Periksa Lagi Myir']]);
	     };

	     DB::transaction(function() use($req, $auth) {
	      	$dispatchBy = '5';
	    	if ($req->jenis_layanan=='SMARTINDIHOME'){
	    		$dispatchBy = '7';
	    	};

	        DB::table('dispatch_teknisi')->insert([
	          'updated_at'          => DB::raw('NOW()'),
	          'updated_by'          => $auth->id_karyawan,
	          'tgl'                 => $req->input('tgl'),
	          'id_regu'             => $req->input('id_regu'),
	          'created_at'          => DB::raw('NOW()'),
	          'updated_at_timeslot' => $req->input('timeslot'),
	          'jenis_layanan'       => $req->input('jenis_layanan'),
	          'Ndem'                => $req->myir,
	          'step_id'             => "1.0",
	          'kordinatPel'         => $req->kordinatPel,
	          'alamatSales'         => $req->alamatSales,
	          'dispatch_by'			=> $dispatchBy
	        ]);
	      });
	      scbe::simpanDispatch($req,'0');
	      echo "Sukses";
	    }

	    // ubah status dispacth tabel psb_myir_wo
	    // scbe::updateDispatch($req->myir);

	    $id = $req->myir;
	    exec('cd ..;php artisan sendTelegramSSC1 '.$id.' '.$auth->id_user.' > /dev/null &');
     	return redirect('/scbe/search')->with('alerts',[['type'=>'success', 'text'=>'Dispatch Sukses']]);
    }

    public function edit(Request $req, $id){
    	$regu = DB::select('
	      SELECT a.id_regu as id,a.uraian as text, b.title
	      FROM regu a
	        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
	      WHERE
	        ACTIVE = 1
	    ');

	    $sto = DB::select('SELECT sto as id, sto as text from maintenance_datel');
    	$datas = scbe::getMyir($id);

    	$dataSektor = DB::SELECT('
	      SELECT
	      a.chat_id as id,
	      a.title as text
	      FROM
	        group_telegram a
	    ');

    	$sql = 'SELECT b.title, a.alproname, c.LATITUDE, c.LONGITUDE FROM
	    			alpro_owner a
	    			LEFT JOIN group_telegram b ON a.nik = b.TL_NIK
	    			LEFT JOIN 1_0_master_odp c ON a.alproname = c.ODP_NAME
	    			WHERE a.alproname = "'.$datas[0]->namaOdp.'" GROUP BY b.title';

	    $odpSektor = DB::SELECT($sql);

	    if (count($odpSektor)==0){
	    	$odp = '';
	    	$lat = '';
	    	$long = '';
	    }
	    else{
		    $odp = $odpSektor[0]->alproname;
		    $lat = $odpSektor[0]->LATITUDE;
		    $long = $odpSektor[0]->LONGITUDE;
	    };

	    if ($req->has('searchOdp')){
	    	$cari = $req->input('searchOdp');

	    	$sql = 'SELECT b.title, a.alproname, c.LATITUDE, c.LONGITUDE FROM
	    			alpro_owner a
	    			LEFT JOIN group_telegram b ON a.nik = b.TL_NIK
	    			LEFT JOIN 1_0_master_odp c ON a.alproname = c.ODP_NAME
	    			WHERE a.alproname = "'.$cari.'" GROUP BY b.title';

	    	$odpSektor = DB::SELECT($sql);
	    	$odp = $cari;

	    	if (count($odpSektor)<>0){
	    		$lat  = $odpSektor[0]->LATITUDE;
	    		$long = $odpSektor[0]->LONGITUDE;
	    	}

	    };

	   	$dataSales = DB::select('SELECT id, kode_sales, nama_sales, kode_sales as text FROM psb_sales where ket=1');
    	return view('scbe.edit',compact('regu', 'datas', 'sto', 'odp', 'lat', 'long', 'odpSektor', 'dataSektor', 'dataSales'));
    }

    public function hapusSc($myir){
    	scbe::hapusMyir($myir);
    	scbe::hapusNdemMyir($myir);
    	return redirect('/scbe/search')->with('alerts',[['type'=>'success', 'text'=>'Data Sudah Dihapus']]);
    }

    public function sinkSc($myir){
    	 $auth = session('auth');
    	// cari myir di data_startclick
    	$dataStartclik = scbe::getDataStarClickByMyir($myir);
    	if ($dataStartclik){
	    	// cek di table dispact_teknisi
	    	$dataDispacth = scbe::getDataDispatchByNdem($dataStartclik->orderId);

	    	if ($dataDispacth){
	    		scbe::hapusNdemMyir($myir);
	    		scbe::ubahKetScMyir($myir, $dataStartclik->orderId);
	    		exec('cd ..;php artisan sendTelegramSSC1SyncSc '.$dataStartclik->orderId.' '.$auth->id_user.' > /dev/null &');
	    		return back()->with('alerts',[['type'=>'success', 'text'=>'Syncron SC Berhasil !!!']]);

	    	}
	    	else{
	    		// ubah Ndem myir jadi sc
	    		DB::table('dispatch_teknisi')->where('Ndem',$myir)->update([
	    			'Ndem'			=> $dataStartclik->orderId,
	    			'dispatch_by'	=> NUll
	    		]);

	    		scbe::ubahKetScMyir($myir, $dataStartclik->orderId);

	    		// kirim ssc
	    		exec('cd ..;php artisan sendTelegramSSC1SyncSc '.$dataStartclik->orderId.' '.$auth->id_user.' > /dev/null &');
    			return back()->with('alerts',[['type'=>'success', 'text'=>'Syncron SC Berhasil !!!']]);
	    	}
    	}
    	else{
    		return back()->with('alerts',[['type'=>'danger', 'text'=>'Syncron SC Gagal, Sc Tidak Ditemukan !!!']]);
    	}
    }

    public function searchOdpFull(Request $req){
    	$datas = [];
    	$cari  = '';
    	if ($req->has('odpq')){
    		$cari = $req->input('odpq');

    		$datas = scbe::getOdpFull($cari);
    	}

    	return view('scbe.searchOdp',compact('datas', 'cari'));
    }

    public function inputOdpFull(){
    	return view('scbe.inputOdp');
    }

    public function simpanOdpFull(Request $req){

    }

    public function sinkronMyir($pic, $myir){
    	// update psb_myir_wo
	    $auth = session('auth');
    	$getMyirLama = DB::table('psb_myir_wo')->where('picPelanggan',$pic)->first();
    	DB::table('dispatch_teknisi')->where('Ndem',$getMyirLama->myir)->update([
    		'Ndem'	=> $myir
    	]);

    	$getPsbLog = DB::table('psb_laporan_log')->where('Ndem',$getMyirLama->myir)->orderBY('psb_laporan_log_id','DESC')->first();
    	DB::table('psb_laporan_log')->insert([
    		'id_regu'				=> $getPsbLog->id_regu,
    		'Ndem'					=> $myir,
    		'psb_laporan_id'		=> $getPsbLog->psb_laporan_id,
    		'status_laporan'		=> $getPsbLog->status_laporan,
    		'catatan'				=> 'Perubahan Myir',
    		'created_at'			=> DB::raw('NOW()'),
    		'created_by'			=> $auth->id_user,
    		'mttr'					=> $getPsbLog->mttr,
    		'penyebabId'			=> $getPsbLog->penyebabId,
    		'action'				=> $getPsbLog->action,
    		'jadwal_manja'			=> $getPsbLog->jadwal_manja,
    	]);

    	DB::table('psb_myir_wo')->where('picPelanggan',$pic)->update([
    		'myir'		=> $myir
    	]);

    	return back()->with('alerts',[['type' => 'success', 'text' => 'Sinkron Myir Sukses']]);
    }
}
