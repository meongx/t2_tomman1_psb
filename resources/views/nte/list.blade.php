@extends('layout')

@section('content')
  @include('partial.alerts')
  <style type="text/css">
    th {
      font-size : 11px;
    }
  </style>
  <h3>
    <a href="/nte/input" class="btn btn-info">
      <span class="glyphicon glyphicon-plus"></span>
    </a>
    NTE ( <i>Network Terminal Equipment</i> )
  </h3>

  <div class="panel panel-default">
    <div class="panel-heading">STOCK NTE</div>
    <div class="panel-body table-responsive">
      <table class="table">
        <tr>
          <th rowspan="2">AREA</th>
          <th rowspan="2">TOTAL</th>
          <th colspan="6">ONT</th>
          <th colspan="5">STB</th>
          <th>PLC</th>
          <th>WIFI EXTENDER</th>
          <th>ADSL</th>
        </tr>
        <tr>
          <th>ZTE<br />F609</th>
          <th>ZTE PREMIUM<br />F670</th>
          <th>HUAWEI<br />HG8245H</th>
          <th>HUAWEI<br />HG8245H5</th>
          <th>HUAWEI Premium<br />HG8245U</th>
          <th>NOKIA<br />G240W-F</th>

          <th>Fiber Home<br />HG680-P</th>
          <th>Huawei<br />EC6108V9</th>
          <th>ZTE<br />B860H</th>
          <th>ZTE<br />B760H</th>
          <th>ZTE<br />B700</th>

          <th>D-LINK<br />DHP-W310AV</th>
          <th>SMART<br />N300</th>
          <th>TP-LINK<br />TD W8961N</th>

        </tr>
        @foreach ($stock_nte as $data)
        <tr>
          <td>{{ $data->nama_gudang }}</td>
           <td><a href="/nte/list/gudang/ALL/ALL">{{ $data->jumlah }}</a></td>
          <td><a href="/nte/list/gudang/ztef609/{{ $data->nama_gudang }}">{{ $data->ztef609 }}</a></td>
          <td><a href="/nte/list/gudang/ztef670/{{ $data->nama_gudang }}">{{ $data->ztef670 }}</a></td>
          <td><a href="/nte/list/gudang/hwiHG8245H/{{ $data->nama_gudang }}">{{ $data->hwiHG8245H }}</a></td>
          <td><a href="/nte/list/gudang/hwiHG8245H5/{{ $data->nama_gudang }}">{{ $data->hwiHG8245H5 }}</a></td>
          <td><a href="/nte/list/gudang/hwiHG8245U/{{ $data->nama_gudang }}">{{ $data->hwiHG8245U }}</a></td>
          <td><a href="/nte/list/gudang/nokiaG240WF/{{ $data->nama_gudang }}">{{ $data->nokiaG240WF }}</a></td>
          <td><a href="/nte/list/gudang/fiberhomeHG680P/{{ $data->nama_gudang }}">{{ $data->fiberhomeHG680P }}</a></td>
          <td><a href="/nte/list/gudang/hwiEC6108V9/{{ $data->nama_gudang }}">{{ $data->hwiEC6108V9 }}</a></td>
          <td><a href="/nte/list/gudang/zteB860H/{{ $data->nama_gudang }}">{{ $data->zteB860H }}</a></td>
          <td><a href="/nte/list/gudang/zteB760H/{{ $data->nama_gudang }}">{{ $data->zteB760H }}</a></td>
          <td><a href="/nte/list/gudang/zteB700/{{ $data->nama_gudang }}">{{ $data->zteB700 }}</a></td>
          <td><a href="/nte/list/gudang/dhpW310AV/{{ $data->nama_gudang }}">{{ $data->dhpW310AV }}</a></td>
          <td><a href="/nte/list/gudang/smartN300/{{ $data->nama_gudang }}">{{ $data->smartN300 }}</a></td>
          <td><a href="/nte/list/gudang/tdW8961N/{{ $data->nama_gudang }}">{{ $data->tdW8961N }}</a></td>
       </tr>
        @endforeach
      </table>
    </div>
  </div> 

  <div class="panel panel-default">
    <div class="panel-heading">STOCK NTE TEKNISI</div>
    <div class="panel-body table-responsive">
      <table class="table">
        <tr>
          <th rowspan="2">AREA</th>
          <th rowspan="2">TOTAL</th>
          <th colspan="6">ONT</th>
          <th colspan="5">STB</th>
          <th>PLC</th>
          <th>WIFI EXTENDER</th>
          <th>ADSL</th>
        </tr>
        <tr>
          <th>ZTE<br />F609</th>
          <th>ZTE PREMIUM<br />F670</th>
          <th>HUAWEI<br />HG8245H</th>
          <th>HUAWEI<br />HG8245H5</th>
          <th>HUAWEI Premium<br />HG8245U</th>
          <th>NOKIA<br />G240W-F</th>

          <th>Fiber Home<br />HG680-P</th>
          <th>Huawei<br />EC6108V9</th>
          <th>ZTE<br />B860H</th>
          <th>ZTE<br />B760H</th>
          <th>ZTE<br />B700</th>

          <th>D-LINK<br />DHP-W310AV</th>
          <th>SMART<br />N300</th>
          <th>TP-LINK<br />TD W8961N</th>

        </tr>
        @foreach ($stock_nte_teknisi as $data)
        <tr>
          <td>{{ $data->nama_gudang }}</td>
          <td><a href="/nte/list/teknisi/ALL/ALL">{{ $data->jumlah }}</a></td>
          <td><a href="/nte/list/teknisi/ztef609/{{ $data->nama_gudang }}">{{ $data->ztef609 }}</a></td>
          <td><a href="/nte/list/teknisi/ztef670/{{ $data->nama_gudang }}">{{ $data->ztef670 }}</a></td>
          <td><a href="/nte/list/teknisi/hwiHG8245H/{{ $data->nama_gudang }}">{{ $data->hwiHG8245H }}</a></td>
          <td><a href="/nte/list/teknisi/hwiHG8245H5/{{ $data->nama_gudang }}">{{ $data->hwiHG8245H5 }}</a></td>
          <td><a href="/nte/list/teknisi/hwiHG8245U/{{ $data->nama_gudang }}">{{ $data->hwiHG8245U }}</a></td>
          <td><a href="/nte/list/teknisi/nokiaG240WF/{{ $data->nama_gudang }}">{{ $data->nokiaG240WF }}</a></td>
          <td><a href="/nte/list/teknisi/fiberhomeHG680P/{{ $data->nama_gudang }}">{{ $data->fiberhomeHG680P }}</a></td>
          <td><a href="/nte/list/teknisi/hwiEC6108V9/{{ $data->nama_gudang }}">{{ $data->hwiEC6108V9 }}</a></td>
          <td><a href="/nte/list/teknisi/zteB860H/{{ $data->nama_gudang }}">{{ $data->zteB860H }}</a></td>
          <td><a href="/nte/list/teknisi/zteB760H/{{ $data->nama_gudang }}">{{ $data->zteB760H }}</a></td>
          <td><a href="/nte/list/teknisi/zteB700/{{ $data->nama_gudang }}">{{ $data->zteB700 }}</a></td>
          <td><a href="/nte/list/teknisi/dhpW310AV/{{ $data->nama_gudang }}">{{ $data->dhpW310AV }}</a></td>
          <td><a href="/nte/list/teknisi/smartN300/{{ $data->nama_gudang }}">{{ $data->smartN300 }}</a></td>
          <td><a href="/nte/list/teknisi/tdW8961N/{{ $data->nama_gudang }}">{{ $data->tdW8961N }}</a></td>
        </tr>
        @endforeach
      </table>
    </div>
  </div> 
    <div class="panel panel-default">
    <div class="panel-heading">NTE USED</div>
    <div class="panel-body table-responsive">
      <table class="table">
        <tr>
          <th rowspan="2">AREA</th>
          <th rowspan="2">TOTAL</th>
          <th colspan="6">ONT</th>
          <th colspan="5">STB</th>
          <th>PLC</th>
          <th>WIFI EXTENDER</th>
          <th>ADSL</th>
        </tr>
        <tr>
          <th>ZTE<br />F609</th>
          <th>ZTE PREMIUM<br />F670</th>
          <th>HUAWEI<br />HG8245H</th>
          <th>HUAWEI<br />HG8245H5</th>
          <th>HUAWEI Premium<br />HG8245U</th>
          <th>NOKIA<br />G240W-F</th>

          <th>Fiber Home<br />HG680-P</th>
          <th>Huawei<br />EC6108V9</th>
          <th>ZTE<br />B860H</th>
          <th>ZTE<br />B760H</th>
          <th>ZTE<br />B700</th>

          <th>D-LINK<br />DHP-W310AV</th>
          <th>SMART<br />N300</th>
          <th>TP-LINK<br />TD W8961N</th>

        </tr>
        @foreach ($stock_nte_used as $data)
        <tr>
          <td>{{ $data->nama_gudang }}</td>
          <td><a href="/nte/list/used/ALL/ALL">{{ $data->jumlah }}</a></td>
          <td><a href="/nte/list/used/ztef609/{{ $data->nama_gudang }}">{{ $data->ztef609 }}</a></td>
          <td><a href="/nte/list/used/ztef670/{{ $data->nama_gudang }}">{{ $data->ztef670 }}</a></td>
          <td><a href="/nte/list/used/hwiHG8245H/{{ $data->nama_gudang }}">{{ $data->hwiHG8245H }}</a></td>
          <td><a href="/nte/list/used/hwiHG8245H5/{{ $data->nama_gudang }}">{{ $data->hwiHG8245H5 }}</a></td>
          <td><a href="/nte/list/used/hwiHG8245U/{{ $data->nama_gudang }}">{{ $data->hwiHG8245U }}</a></td>
          <td><a href="/nte/list/used/nokiaG240WF/{{ $data->nama_gudang }}">{{ $data->nokiaG240WF }}</a></td>
          <td><a href="/nte/list/used/fiberhomeHG680P/{{ $data->nama_gudang }}">{{ $data->fiberhomeHG680P }}</a></td>
          <td><a href="/nte/list/used/hwiEC6108V9/{{ $data->nama_gudang }}">{{ $data->hwiEC6108V9 }}</a></td>
          <td><a href="/nte/list/used/zteB860H/{{ $data->nama_gudang }}">{{ $data->zteB860H }}</a></td>
          <td><a href="/nte/list/used/zteB760H/{{ $data->nama_gudang }}">{{ $data->zteB760H }}</a></td>
          <td><a href="/nte/list/used/zteB700/{{ $data->nama_gudang }}">{{ $data->zteB700 }}</a></td>
          <td><a href="/nte/list/used/dhpW310AV/{{ $data->nama_gudang }}">{{ $data->dhpW310AV }}</a></td>
          <td><a href="/nte/list/used/smartN300/{{ $data->nama_gudang }}">{{ $data->smartN300 }}</a></td>
          <td><a href="/nte/list/used/tdW8961N/{{ $data->nama_gudang }}">{{ $data->tdW8961N }}</a></td>
        </tr>
        @endforeach
      </table>
    </div>
  </div> 
 <!--     @foreach($transaksi as $data)
      <a href="/nte/{{ $data->id }}" class="list-group-item">
        <strong>{{ $data->nama_gudang }}</strong>
        <span class="label label-info">{{ $data->trx }}</span>
        <br/>
        <span>{{ $data->nama }}</span> / <span>{{ $data->created_at }}</span>
        <br/> 
        <small>{{ $data->catatan }}</small>
      </a>
    @endforeach -->
@endsection
