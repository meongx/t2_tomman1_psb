@extends('layout')

@section('content')
  @include('partial.alerts')
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmAwGwcaeJKLu7f3Noyhw2ihC8s8aaoPs"></script>
  <form method="GET">
      <div class="row">
        <div class="col-md-6">
          <h3>
            <a href="/scbe/search" class="btn btn-sm btn-default">
              <span class="glyphicon glyphicon-arrow-left"></span>
            </a>
            RE-Dispatch MYIR-{{ $datas[0]->myir }}
          </h3>
        </div>

        <div class="input-group col-sm-6">
            <input type="text" class="form-control inputSearch" placeholder="Cari ODP . ." name="searchOdp" id="searchOdp" />
            <span class="input-group-btn">
                <button class="btn btn-default search">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </div>
      </div>
  </form>  

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
     <div class="panel panel-default">
        <div class="panel-heading">MAP ODP</div>
        <div class="panel-body">
          <div id="Maps" class="Maps" style="height:200px;">
          </div>
          Koordinat ODP : {{ $lat }},{{ $long }}
        </div>
    </div>    

    @if (count($odpSektor)>1)
      <div class="panel panel-default">
          <div class="panel-body">
          Rekomendasi Sektor Berdasarkan ALPRO ({{$odp}}) :  
          @foreach ($odpSektor as $sektor)
              <label class="label label-primary">{{ $sektor->title }}</label>
          @endforeach
          </div>
      </div>
    @endif

    <div class="form-group{{ $errors->has('myir') ? 'has-error' : '' }}">
      <label class="control-label" for="myir">MYIR</label>
      <input type="text" name="myir" id="myir" value="{{ old('myir') ?: $datas[0]->myir }}" class="form-control" readonly>
      {!! $errors->first('myir','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group{{ $errors->has('sales') ? 'has-error' : '' }}">
      <label class="control-label" for="sales">Sales</label>
      <input type="text" name="sales" id="sales" value="{{ old('sales') ?: $datas[0]->sales_id }}" class="form-control">
      {!! $errors->first('sales','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group{{ $errors->has('nmCustomer') ? 'has-error' : '' }}">
      <label class="control-label" for="myir">Nama Pelanggan</label>
      <input type="text" name="nmCustomer" id="nmCustomer" value="{{old('nmCustomer') ?: $datas[0]->customer }}" class="form-control" readonly>
      {!! $errors->first('nmCustomer','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group{{ $errors->has('sto') ? 'has-error' : '' }}">
      <label class="control-label" for="myir">STO</label>
      <input type="hidden" name="sto" id="sto" value="{{old('sto') ?: $datas[0]->sto}}" class="form-control">
      {!! $errors->first('sto','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group">
      <label class="control-label" for="jenis_layanan">Jenis Layanan</label>
      <input name="jenis_layanan" type="hidden" id="jenis_layanan" class="form-control" value="{{ old('jenis_layanan') ?: $datas[0]->jenis_layanan }}"  />
      {!! $errors->first('jenis_layanan', '<span class="label label-danger">:message</span>') !!}
    </div>

     <div class="form-group">
      <label class="control-label" for="input-sektor">Sektor</label>
      <input name="input-sektor" type="hidden" id="input-sektor" class="form-control" value="{{ old('input-sektor') ?: $datas[0]->mainsector }}"  />
      {!! $errors->first('input-sektor', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
      <label class="control-label" for="input-regu">Regu</label>
      <input name="id_regu" type="hidden" id="input-regu" class="form-control" value="{{ old('id_regu') ?: $datas[0]->id_regu }}" />
      {!! $errors->first('id_regu', '<span class="label label-danger">:message</span>') !!}
      {{-- <div class="col-xs-10">{!! $errors->first('id_regu', '<span class="label label-danger">:message</span>') !!}</div> --}}

    </div>

    <div class="form-group">
      <label class="control-label" for="input-tgl">Tanggal</label>
      <input name="tgl" type="tgl" id="input-tgl" class="form-control" value="{{ date('Y-m-d') ?: $datas[0]->tgl}}" />
      {!! $errors->first('tgl', '<span class="label label-danger">:message</span>') !!}
      {{-- <div class="col-xs-10">{!! $errors->first('tgl', '<span class="label label-danger">:message</span>') !!}</div> --}}
    </div>

    <div class="form-group">
      <label class="control-label">Nama ODP</label>
      <input type="text" name="nama_odp" id="odp2" class="form-control" rows="1" value="{{ old('nama_odp') ?: $odp ?: $datas[0]->namaOdp}}" readonly />
      {!! $errors->first('nama_odp', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group{{ $errors->has('kordinatPel') ? 'has-error' : '' }}">
      <label class="control-label" for="input-timeslot">Koordinat Pelanggan</label>
      <input type="text" name="kordinatPel" id="kordinatPel" value="{{ old('kordinatPel') ?: $datas[0]->kordinatPel}}" class="form-control">
      {!! $errors->first('kordinatPel','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group{{ $errors->has('alamatSales') ? 'has-error' :'' }} ">
      <label class="control-label" for="input-timeslot">Alamat Sales</label>
      <textarea name="alamatSales" id="alamatSales" class="form-control" >{{ old('alamatSales') ?: $datas[0]->alamatSales }}</textarea>
      {!! $errors->first('alamatSales','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group{{ $errors->has('picPelanggan') ? 'has-error' :'' }} ">
      <label class="control-label" for="input-timeslot">PIC Pelanggan</label>
      <textarea name="picPelanggan" id="picPelanggan" class="form-control" >{{ old('picPelanggan') ?: $datas[0]->picPelanggan }}</textarea>
      {!! $errors->first('picPelanggan','<p class="label label-danger">:message</p>') !!}
    </div>
    
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>
  
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
    $(function() {
      var initMap = function(lat, lon) {
        var marker = null;
        var center = new google.maps.LatLng(lat, lon),
            withMarker = (!isNaN(lat) && !isNaN(lon)) && (lat != 0 && lon != 0),
            zoom = withMarker ? 18 : 12;
        map = new google.maps.Map(
            document.getElementById('Maps'),
            {
                center: center,
                zoom: zoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        );
      }

      var showMarker = function(lat,lng) {
              marker = new google.maps.Marker({
                  position: new google.maps.LatLng(lat,lng),
                  map: map,
                  draggable: true,
                  animation: google.maps.Animation.DROP
              });
      }
  
      var state = <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Input Regu',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-regu').select2(regu());

      var data_sektor= <?= json_encode($dataSektor) ?>;
      console.log(data_sektor);
      var sektor = function() {
        return {
          data: data_sektor,
          placeholder: 'Pilih Sektor',
          formatResult: function(data) {
          return '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-sektor').select2(sektor());
      $('#input-sektor').click(function(){
         $('#input-regu').val(null).trigger('change');;
      })

      $("#input-regu").select2({
          initSelection: function (element, callback) {
                  var data = { "id": "{{ $datas[0]->id_regu ? : ''}}", "text": "{{ $datas[0]->uraian ? : ''}}" };
                  callback(data);
          },
          placeholder: 'Pilih Regu',
          ajax : {
            url : "/assurance/inputRegu/",
            dataType : "json",
            data: function() {
              return {
                sektor : $("#input-sektor").val()
              }
            },
            results : function (data){
              var myResults = [];
              $.each(data, function (index, item) {
                myResults.push({
                      'id': item.id,
                      'text': item.text
                  });
              });
              return {
                  results: myResults,
              };
            }
          }
      });

      var datajenislayanan = [
        {"id":"1PVOICE", "text":"<span class='label label-success'>1P</span> VOICE"},
        {"id":"1PINET", "text":"<span class='label label-success'>1P</span> INET"},
        {"id":"2PINETVOICE", "text":"<span class='label label-success'>2P</span> INET VOICE"},
        {"id":"2PINETUSEETV", "text":"<span class='label label-success'>2P</span> INET USEETV"},
        {"id":"3P", "text":"<span class='label label-success'>3P</span> VOICE INET USEETV"},
        {"id":"ADDONSTB", "text":"<span class='label label-success'>ADD ON</span> STB"},
        {"id":"CHANGESTB", "text":"<span class='label label-success'>CHANGE</span> STB"},
      ];

      var jenislayanan = function() {
        return {
          data: datajenislayanan,
          placeholder: 'Input Jenis Layanan',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }

      $('#jenis_layanan').select2(jenislayanan());

      var stoData = <?= json_encode($sto) ?>;
      var sto = function() {
        return {
          data: stoData,
          placeholder: 'Pilih STO',
          formatResult: function(data) {
          return '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#sto').select2(sto());
      
      $("#odp2").inputmask("AAA-AAA-A{2,3}/999");

      var day = {
        format: 'yyyy-mm-dd',
        viewMode: 0,
        minViewMode: 0
      };
      $('#input-tgl').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });

      let dataSales = <?=json_encode($dataSales) ?>;
      console.log(dataSales);
      let sales = function () {
          return {
                data : dataSales,
                placeholder : 'Pilih Sales',
                formatResult : function(data){
                      return '<span class="label label-success">'+data.kode_sales+'</span> '+data.nama_sales;
                }
          }
      };

      $('#sales').select2(sales());

      initMap({{ $lat ? : '-3.332081'}},{{ $long ? : '114.67323'}});
      showMarker({{ $lat ? : '-3.332081'}},{{ $long ? : '114.67323'}});
    })
  </script>
@endsection
