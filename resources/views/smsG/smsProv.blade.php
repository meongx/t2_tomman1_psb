@extends('layout')

@section('content')
@include('partial.alerts')
	<div class="panel panel-primary">
		<div class="panel-heading">SMS Provisioning</div>
		<div class="panel-body">
			<form method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
				<div class="form-group">
					<label class="label-control">No. SC</label>
					<textarea class="form-control" name="noTelp" placeholder="Pisahkan No. SC dengan Koma (,)"></textarea>
				</div>

				<div class="form-group">
					<label class="label-control">Pesan Brodcast</label>
					<textarea class="form-control" name="pesan" rows="5"></textarea>
				</div>

				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Kirim">
				</div>		
			</form>	
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">List Pesan Masuk</div>
		<div class="panel-body">
			<table class="table table-bordered">
				<tr>
					<th>#</th>
					<th>Tanggal Masuk</th>
					<th>Pelanggan</th>
					<th>SC</th>
					<th>Isi Pesan</th>
				</tr>

				@foreach($lists as $no=>$list)
					<tr>
						<td>{{++$no}}</td>
						<td>{{ $list->ReceivingDateTime }}</td>
						<td>{{ $list->orderName == '' ? $list->SenderNumber : $list->orderName }}</td>
						<td>{{ $list->orderId==''? '-' : $list->orderId }}</td>
						<td>{{ $list->TextDecoded }}</td>
					</tr>
				@endforeach
			</table>	
		</div>
	</div>
@endsection