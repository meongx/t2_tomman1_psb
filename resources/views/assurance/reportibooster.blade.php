@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3></h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
      Report Hasil Ukur Ibooster Periode {{ $periode }}  </div>
        <div class="panel-content table-responsive">
          <table class="table">
            <tr>
              <th>No</th>
              <th>Tiket</th>
              <th>Petugas</th>
              <th>Tgl Ukur</th>
              <th>ND</th>
              <th>IP_Embassy</th>
              <th>Type</th>
              <th>Calling_Station_Id</th>
              <th>IP_NE</th>
              <th>ONU_Link_Status</th>
              <th>ONU_Serial_Number</th>
              <th>Fiber_Length</th>
              <th>OLT_Tx</th>
              <th>OLT_Rx</th>
              <th>ONU_Tx</th>
              <th>Framed_IP_Address</th>
              <th>MAC_Address</th>
              <th>Last_Seen</th>
              <th>AcctStartTime</th>
              <th>AccStopTime</th>
              <th>Up</th>
              <th>Down</th>
              <th>Status_Koneksi</th>
              <th>ADSL_Link_Status</th>
              <th>Upstream_Line_Rate</th>
              <th>Upstream_SNR</th>
              <th>Upstream_Attenuation</th>
              <th>Upstream_Attainable_Rate</th>
              <th>Downstream_Line_Rate</th>
              <th>Downstream_SNR</th>
              <th>Downstream_Attainable_Rate</th>
            </tr>
            @foreach ($data as $num => $result)
            <tr>
              <td>{{ ++$num }}.</td>
              <td>{{ $result->order_id }}</td>
              <td>{{ $result->user_created }}</td>
              <td>{{ $result->date_created }}</td>
              <td>{{ $result->ND }}</td>
              <td>{{ $result->IP_Embassy }}</td>
              <td>{{ $result->Type }}</td>
              <td>{{ $result->Calling_Station_Id }}</td>
              <td>{{ $result->IP_NE }}</td>
              <td>{{ $result->ONU_Link_Status }}</td>
              <td>{{ $result->ONU_Serial_Number }}</td>
              <td>{{ $result->Fiber_Length }}</td>
              <td>{{ $result->OLT_Tx }}</td>
              <td>{{ $result->OLT_Rx }}</td>
              <td>{{ $result->ONU_Tx }}</td>
              <td>{{ $result->Framed_IP_Address }}</td>
              <td>{{ $result->MAC_Address }}</td>
              <td>{{ $result->Last_Seen }}</td>
              <td>{{ $result->AcctStartTime }}</td>
              <td>{{ $result->AccStopTime }}</td>
              <td>{{ $result->Up }}</td>
              <td>{{ $result->Down }}</td>
              <td>{{ $result->Status_Koneksi }}</td>
              <td>{{ $result->ADSL_Link_Status }}</td>
              <td>{{ $result->Upstream_Line_Rate }}</td>
              <td>{{ $result->Upstream_SNR }}</td>
              <td>{{ $result->Upstream_Attenuation }}</td>
              <td>{{ $result->Upstream_Attainable_Rate }}</td>
              <td>{{ $result->Downstream_Line_Rate }}</td>
              <td>{{ $result->Downstream_SNR }}</td>
              <td>{{ $result->Downstream_Attainable_Rate }}</td>
            </tr>
            @endforeach
          </table>

        </div>
      </div>
    </div>
  </div>
@endsection
