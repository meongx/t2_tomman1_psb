<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<table class="table table-striped table-bordered table-condensed">
  <thead>
    <tr>
      <th>#</th>
      <th>Perangkat</th>
      <th>Status</th>
      <th>Downtime</th>
    </tr>
  </thead>
  <tbody>
    @foreach($parse as $no => $data)
      <tr>
        <td>{{ ++$no }}</td>
        <td>{{ $data['perangkat'] }}</td>
        <td><span class="label label-{{ ($data['status'] == 'DOWN') ? 'danger':'success' }}">
        {{ $data['status'] }}</span></td>
        <td>{{ $data['downtime'] }}</td>
      </tr>
    @endforeach
  </tbody>
</table>