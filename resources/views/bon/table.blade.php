@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
  </style>
  <div style="padding-bottom: 10px;">
<!--     <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong> -->
  </div>
  <h3>Monitoring Laporan Teknisi</h3>
  @foreach ($get_area as $area)
  @if (array_key_exists($area->area, $data))
  <div class="panel panel-primary" id="timeslot">
    <div class="panel-heading">WFM {{ $area->DESKRIPSI }}</div>
    <div class="panel-body">
      <div class="list-group">
	    <div class="table-responsive">
				<table class="table table-striped table-bordered">
          <tr>
            <th width=5%>No</th>
            <th width=20%>Nama Tim</th>
            <th width=80%>Workorder</th>
          </tr>
          @foreach ($get_regu as $number => $regu)
          @if ($regu->area == $area->area)
          <?
          $number = 1;
          ?>
          <tr>
            <td>{{ $number }}</td>
            <td>{{ $regu->uraian }}</td>
            <td>
              <table>
                <tr>
              @if (array_key_exists($regu->id_regu, $data[$area->area]))
              <?php
                $field  = $data[$area->area][$regu->id_regu];
                $jumlah = count($data[$area->area][$regu->id_regu]);
                // echo $jumlah;
                if ($jumlah>0){
                  for($i=0;$i<$jumlah;$i++){
                    $result = '';
                    if ($field[$i]['status_laporan']==1) $label = "success"; else $label = "warning";

                    $result .= '<tr>
                      <td valign=top><a href="/'.$field[$i]['id'].'" class="label label-'.$label.'">SC'.$field[$i]['SC'].
                    '</a></td>';
                    if ($field[$i]['nte']<>"")
                    $result .= '<td valign=top>'.$field[$i]['nte'].'</td>';
                    else
                    $result .= '<td valign=top><span class="label label-default">Tidak Melaporkan NTE</span></td>';
                    foreach($photoInputs as $input) {

                        $path = "/upload/evidence/".$field[$i]['id']."/$input";
                        $th   = "$path-th.jpg";
                        $img  = "$path.jpg";

                    $result .='
                    <td style="padding:5px;" valign=top>';
                    if (file_exists(public_path().$th)){
                      $result .='
                      <a href="/upload/evidence/'.$field[$i]['id'].'/'.$input.'.jpg">
                      <img src="/upload/evidence/'.$field[$i]['id'].'/'.$input.'-th.jpg" alt="'.$input.'">

                      </a>';
                    } else {
                      $result .='<img src="/image/placeholder.gif" alt="" />';
                    }
                    $result .='<br />'.$input.'</td>';
                  }
                    $result .= '</tr>';
                    echo $result;
                  }
                }
              ?>
              @else
              <span class="label label-default">BELUM ADA MATERIAL TERINSTALL</span>
              @endif
            </tr>
            </table>
            </td>
          </tr>
          <?
            $number++;
          ?>
          @endif
          @endforeach
        </table>
        </div>
      </div>
    </div>
  </div>
  @endif
  @endforeach

<div id="timexcount">0 ms</div>
		<div class="panel-body">

							<div id="bar-10" style="height: 450px; width: 100%;"></div>
						</div>
					</div>
  </div>
@endsection
