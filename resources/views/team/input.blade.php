@extends('layout')

@section('content')
  @include('partial.alerts')
	<style>
		.panel-content {
			padding : 10px;
		}
	</style>
  <h3>
    <a href="/team" class="btn btn-info">
      <span class="glyphicon glyphicon-home"></span>
    </a>
    Team
  </h3>

	<div class="panel panel-primary">
		<div class="panel-heading">Input</div>
		<div class="panel-content">
			<form method="post">
				<input type="hidden" name="id_regu" value="{{ $data->id_regu }}" />
			<div class="form-group">
				<label for="uraian">Group Name</label>
				<input type="text" name="uraian" id="uraian" class="form-control" value="{{ $data->uraian }}" />
			</div>

			<div class="form-group">
				<label for="status">Status</label>
				<select name="mitra" class="form-control">
					<option value=""> -- </option>
					@foreach ($getStatus as $status)
					<?php if ($status->id == $data->mitra) $selected = "selected='selected'"; else $selected = ""; ?>
					<option value="{{ $status->id }}" {{ $selected }}>{{ $status->text }}</option>
					@endforeach
				</select>
			</div>
      <div class="form-group">
        <label for="status">Manhours</label>
        <select class="form-control" name="manhours">
          <option value=""> -- </option>
          <option value="8" <?php if ($data->kemampuan==8) { echo "selected='selected'"; } else {  } ?> > 8 MH </option>
          <option value="16" <?php if ($data->kemampuan==16) { echo "selected='selected'"; } else {  } ?>> 16 MH </option>
        </select>
      </div>
      <div class="form-group">
				<label for="mainsector">Main Sector</label>
        <select name="mainsector" id="mainsector" class="form-control">
          <option value=""> -- </option>
        @foreach ($mainsector as $r_mainsector)
          <?php if ($r_mainsector->chat_id==$data->mainsector) { $select = 'selected="selected"'; } else { $select = ''; } ?>
          <option value="{{ $r_mainsector->chat_id }}" {{ $select }}>{{ $r_mainsector->title  }}</option>
        @endforeach
      </select>
      </div>
			<div class="form-group">
				<label for="nik1">CREW ID</label>
				<input type="text" name="crewid" id="crewid" class="form-control" value="{{ $data->crewid }}" />
			</div>
			<div class="form-group">
				<label for="nik1">NIK 1</label>
				<input type="text" name="nik1" id="nik1" class="form-control" value="{{ $data->nik1 }}" />
			</div>

			<div class="form-group">
				<label for="nik1">NIK 2</label>
				<input type="text" name="nik2" id="nik2" class="form-control" value="{{ $data->nik2 }}" />
			</div>
			<div class="form-group">
				<label for="nik1">STATUS</label>
				<select name="status_team" class="form-control">
					<option value=""> -- </option>
					@foreach ($get_status_team as $status_team)
					<?php
						if ($data->status_team==$status_team->status_team) {
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
					?>
					<option value="{{ $status_team->status_team }}" <?php echo $selected ?>>{{ $status_team->status_team }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label for="status_alker">STATUS ALKER</label>
				<select name="status_alker" class="form-control">
					<option value=""> -- </option>
					@foreach ($get_status_alker as $status_alker)
					<?php
						if ($data->status_alker==$status_alker->status_alker) {
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
					?>
					<option value="{{ $status_alker->status_alker }}" <?php echo $selected ?>>{{ $status_alker->status_alker }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<input type="submit" name="submit" id="submit" class="btn btn-success" />
				@if ($data->id_regu <> "")
				<a href="/team/disable/{{ $data->id_regu }}" class="btn btn-danger">DELETE</a>
				@endif
			</div>

			</form>


		</div>
	</div>
	<script>
		$(document).ready(function () {
      var data = <?= json_encode($karyawan) ?>;

      var select2Options = function() {
				return {
					data: data,
					placeholder: 'Input Karyawan',
					allowClear: true,
					minimumInputLength: 1,
					escapeMarkup: function(m) { return m },
					formatSelection: function(data) { return data.text },
					formatResult: function(data) {
						return	'<span class="label label-default">'+data.id+'</span>'+
								'<strong style="margin-left:5px">'+data.text+'</strong>';
					}
				}
			}
      $('#nik1').select2(select2Options());
      $('#nik2').select2(select2Options());
    });
	});
	</script>
@endsection
