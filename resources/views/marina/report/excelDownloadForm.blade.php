    <div class="panel panel-default" id="info">
        <div class="panel-heading">Data Maintenance</div>
         <div id="fixed-table-container-demo" class="fixed-table-container">
            <table class="table table-bordered table-fixed">
              <tr>
                <th>#</th>
                <th>Id</th>
                <th>no_tiket</th>
                <th>no inet</th>
                <th>headline</th>
                <th>pic</th>
                <th>koordinat</th>
                <th>info</th>
                <th>nama</th>
                <th>pic</th>
                <th>jenis_order</th>
                <th>nama_order</th>
                <th>created_by</th>
                <th>created_at</th>
                <th>modified_at</th>
                <th>modified_by</th>
                <th>program_id</th>
                <th>program_name</th>
                <th>no_tiket_temp</th>
                <th>dispatch_regu_id</th>
                <th>dispatch_regu_name</th>
                <th>status</th>
                <th>status_name</th>
                <th>hasil_by_user</th>
                <th>hasil_by_qc</th>
                <th>hasil_by_rekon</th>
                <th>tgl_selesai</th>
                <th>username1</th>
                <th>username2</th>
                <th>username3</th>
                <th>username4</th>
                <th>tiket_by</th>
                <th>psb_laporan_id</th>
                <th>action</th>
                <th>order_from</th>
                <th>kandatel</th>
                <th>sto</th>
                <th>nama_odp</th>
                <th>refer</th>
                <th>action_cause</th>
                <th>verify</th>
                <th>mtrcount</th>
                <th>splitter</th>
                <th>alamat</th>
                <th>verifHd</th>
                <th>port used</th>
                <th>port idle</th>
              </tr>
            @foreach($data as $no => $list) 
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $list->id }}</td>
                    <td>{{ $list->no_tiket }}</td>
                    <td>{{ $list->no_inet }}</td>
                    <td>{{ $list->headline }}</td>
                    <td>{{ $list->pic }}</td>
                    <td>{{ $list->koordinat }}</td>
                    <td>{{ $list->info }}</td>
                    <td>{{ $list->customerName }}</td>
                    <td>{{ $list->myirpic }}</td>
                    <td>{{ $list->jenis_order }}</td>
                    <td>{{ $list->nama_order }}</td>
                    <td>{{ $list->created_by }}</td>
                    <td>{{ $list->created_at }}</td>
                    <td>{{ $list->modified_at }}</td>
                    <td>{{ $list->modified_by }}</td>
                    <td>{{ $list->program_id }}</td>
                    <td>{{ $list->program_name }}</td>
                    <td>{{ $list->no_tiket_temp }}</td>
                    <td>{{ $list->dispatch_regu_id }}</td>
                    <td>{{ $list->dispatch_regu_name }}</td>
                    <td>{{ $list->status }}</td>
                    <td>{{ $list->status_name }}</td>
                    <td>{{ $list->hasil_by_user }}</td>
                    <td>{{ $list->hasil_by_qc }}</td>
                    <td>{{ $list->hasil_by_rekon }}</td>
                    <td>{{ $list->tgl_selesai }}</td>
                    <td>{{ $list->username1 }}</td>
                    <td>{{ $list->username2 }}</td>
                    <td>{{ $list->username3 }}</td>
                    <td>{{ $list->username4 }}</td>
                    <td>{{ $list->tiket_by }}</td>
                    <td>{{ $list->psb_laporan_id }}</td>
                    <td>{{ $list->action }}</td>
                    <td>{{ $list->order_from }}</td>
                    <td>{{ $list->kandatel }}</td>
                    <td>{{ $list->sto }}</td>
                    <td>{{ $list->nama_odp }}</td>
                    <td>{{ $list->refer }}</td>
                    <td>{{ $list->action_cause }}</td>
                    <td>{{ $list->verify }}</td>
                    <td>{{ $list->mtrcount }}</td>
                    <td>{{ $list->splitter }}</td>
                    <td>{{ $list->alamat }}</td>
                    <td>{{ $list->verifHd }}</td>
                    <td>{{ $list->port_used }}</td>
                    <td>{{ $list->port_idle }}</td>
                </tr>
            @endforeach
          </table>
      </div>
    </div>