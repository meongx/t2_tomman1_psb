<table>
<tr><th rowspan="2">NO</th><th rowspan="2">URAIAN / ITEM</th><th rowspan="2">STO</th><th>TOTAL HARGA</th><th rowspan="2">KETERANGAN</th></tr>
<tr><th>( Rp )</th></tr>
<?php
	$total = 0;
?>
@foreach($list as $no => $data)
	<?php
		$total += $data->total_boq;
	?>
	<tr><td>{{ ++$no }}</td><td>{{ $data->info }} / {{ $data->headline }}</td><td></td><td>{{ $data->total_boq }}</td><td>{{ $data->no_tiket }}</td></tr>	
@endforeach
<tr><td colspan="2">TOTAL</td><td></td><td>{{ $total }}</td><td>Tidak Termasuk PPN</td></tr>
</table> 
