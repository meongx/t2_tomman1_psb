<ul class="nav nav-tabs">
  <li class="{{ (Request::segment(1) == 'home') ? 'active' : '' }}"><a href="/home">Active Team</a></li>
  <li class="{{ (Request::segment(1) == 'performansiAsr') ? 'active' : '' }}"><a href="/performansiAsr/{{ date('Y-m') }}">Performansi Asr</a></li>
  <li class="{{ (Request::segment(1) == 'performansiProv') ? 'active' : '' }}"><a href="/performansiProv/{{ date('Y-m') }}">Performansi Prov</a></li>
  <li class="{{ (Request::segment(1) == 'alkersarker') ? 'active' : '' }}"><a href="/alkersarker/QC">Alker & Sarker</a></li>
  <li class="{{ (Request::segment(1) == 'returnDropcore') ? 'active' : '' }}"><a href="/returnDropcore/{{ date('Y-m') }}">Pengembalian Dropcore</a></li>
  <li class="{{ (Request::segment(1) == 'QCOndeskAssurance') ? 'active' : '' }}"><a href="/QCOndesk/{{ date('Y-m') }}/1">QC Ondesk Assurance</a></li>
  <li class="{{ (Request::segment(1) == 'QCOndeskProvisioning') ? 'active' : '' }}"><a href="/QCOndesk/{{ date('Y-m') }}/2">QC Ondesk Provisioning</a></li>
  <li class="{{ (Request::segment(1) == 'material') ? 'active' : '' }}"><a href="/material/{{ date('Y-m') }}">Monitoring Material</a></li>
  <li class="{{ (Request::segment(1) == 'scheduleTech') ? 'active' : '' }}"><a href="/scheduleTech/{{ date('Y-m') }}">Schedule</a></li>
  <li class="{{ (Request::segment(1) == 'alpro') ? 'active' : '' }}"><a href="/alpro">Alpro</a></li>

  @if(session('auth')->level==2 || session('auth')->level==46 || session('auth')->level==15)
    <!-- <li class="{{ (Request::segment(1) == 'Dashboard TL') ? 'active' : '' }}"><a href="/dashboard/teritori/{{ date('Y-m-d') }}">Dashboard TL</a></li> -->
    <li class="{{ (Request::segment(1) == 'briefing') ? 'active' : '' }}"><a href="/briefing/list/{{ date('Y-m') }}">Briefing Report</a></li>
  @endif

  <!-- <li class="{{ (Request::segment(1) == 'split') ? 'active' : '' }}"><a href="/split/konfirmasi-dc/form/">Konfirmasi DC</a></li> -->
  
</ul>
