@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    <a href="/team/input" class="btn btn-info">
      <span class="glyphicon glyphicon-plus"></span>
    </a>
    Team
  </h3>

  <div class="list-group">
    @foreach($getregu as $data)
      <a href="/team/{{ $data->id_regu }}" class="list-group-item">
        <strong>{{ $data->uraian }}</strong>
        <br />
        <span class="label label-default">{{ $data->mitra }}</span>
        <span class="label label-warning">{{ $data->spesialis }}</span>
        <span class="label label-info">{{ $data->mainsector }}</span>
        <br />
        <span>{{ $data->nik1 ? : 'NIK1' }} {{ $data->nama_nik1 }}</span> /
        <span>{{ $data->nik2 ? : 'NIK2' }} {{ $data->nama_nik2 }}</span>

      </a>
    @endforeach
  </div>

@endsection
