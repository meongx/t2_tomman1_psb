@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      text-align: center;
      vertical-align: middle;
    }
    td {
      padding :0px;
    }
  </style>
  <div style="padding-bottom: 10px;">
    <h3>Produktifitas </h3>
    <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong>
  </div>

  <div class="panel panel-primary" id="produktifitas">
    <div class="panel-heading">PRODUKTIFITAS {{ $area }} - {{ date('Y-m-d') }} {{ $konstanta }}</div>
    <div class="panel-body">
      <div class="list-group">
	    <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <tr>
            <th width="10" rowspan="2">No</th>
            <th width="200" rowspan="2">Nama Tim</th>
            <th width="60" rowspan="2">Target MH</th>
            <th width="60" colspan=4 align="center"><center>WO</center></th>
            <th width="60" rowspan="2" align="center"><center>NO UPDATE</center></th>
            <th width="60" rowspan="2" align="center"><center>OGP</center></th>
            <th width="60" rowspan="2" align="center"><center>KEND.</center></th>
            <th width="60" colspan="7" align="center"><center>UP</center></th>
            <th width="60" rowspan="2" align="center"><center>MH ACH</center></th>
            <th width="60" rowspan="2" align="center"><center>PROD MH</center></th>
            <th width="60" rowspan="2" align="center"><center>PROD WO</center></th>
          </tr>
          <tr>
            <th width=15>Jumlah</th>
            <th width=15>PSB</th>
            <th width=15>ASR</th>
            <th width=30>MH</th>
            <th width=30>Asr (D)</th>
            <th width=30>Asr (C)</th>
            <th width=30>Asr (F)</th>
            <th width=30>Prov</th>
            <th width=30>Add on</th>
            <th width=30>As Is</th>
            <th width=30>Jumlah</th>
          </tr>
            <?php
              $number = 1;
              $total_target_MH = 0;
              $total_WO = 0;
              $total_MH = 0;
              $total_Progress = 0;
              $total_NO_UPDATE = 0;
              $total_kendala = 0;
              $total_UP_D = 0;
              $total_UP_A = 0;
              $total_UP_C = 0;
              $total_UP_Prov = 0;
              $total_UP_asis = 0;
              $total_UP_addon = 0;
              $total_MH_ACH = 0;
              $total_prod = 0;
              $totalpsb = 0;
              $totalggn = 0;
              $PROD_WO = 0;
            ?>
              @foreach ($getTeamMatrix as $data)
              <?php
                $jumlah_target_MH = $data->target_MH*$data->jumlah_regu*$konstanta;
                $total_target_MH = $total_target_MH + $jumlah_target_MH;

              ?>
              <tr>
                <td>{{ $number }}</td>
                <td>{{ $data->uraian }}<br /><span class="label label-primary">{{ $data->TL }}</span></td>
                <td><center>{{ $jumlah_target_MH }}</center></td>
                <?php
                  $MH = $data->gangguan_Copper_MH +
                        $data->gangguan_GPON_MH +
                        $data->gangguan_DES_MH +
                        $data->asis_MH +
                        $data->prov_MH;
                ?>
                <td><center>{{ $data->jumlah }}</center></td>
                <td><center>{{ $data->jlhpsb }}</center></td>
                <td><center>{{ $data->jlhggn }}</center></td>
                <td><center>{{ $MH }}</center></td>
                <?php
                  $total_WO = $total_WO + $data->jumlah;
                  $total_MH = $total_MH + $MH;
                  $NO_UPDATE = $data->jumlah-$data->PROGRESS-$data->KENDALA-$data->UP;
                  $total_NO_UPDATE = $total_NO_UPDATE + $NO_UPDATE;
                  $total_Progress = $total_Progress + $data->PROGRESS;
                  $total_kendala = $total_kendala + $data->KENDALA;
                  $total_UP_D += $data->jumlah_gangguan_UP_DES;
                  $total_UP_A = $total_UP_A + $data->jumlah_gangguan_UP_GPON;
                  $total_UP_C = $total_UP_C + $data->jumlah_gangguan_UP_Copper;
                  $total_UP_Prov = $total_UP_Prov + $data->jumlah_prov_UP;
                  $total_UP_asis += $data->jumlah_asis_UP;
                  $total_UP_addon += $data->jumlah_addon_UP;
                  $totalpsb += $data->jlhpsb;
                  $totalggn += $data->jlhggn;
                  $MH_UP = $data->gangguan_UP_Copper +
                           $data->gangguan_UP_GPON +
                           $data->gangguan_UP_DES +
                           $data->asis_UP +
                           $data->prov_UP;
                  $total_MH_ACH = $total_MH_ACH + $MH_UP;
                  $PROD = ($MH_UP / ($data->kemampuan*$data->jumlah_regu*$konstanta))*100;
                  $PROD_WO = ($MH_UP / $MH) * 100;
                ?>
                <td><center>{{ $NO_UPDATE }}</center></td>
                <td><center>{{ $data->PROGRESS }}</center></td>
                <td><center>{{ $data->KENDALA }}</center></td>
                <td><center>{{ $data->jumlah_gangguan_UP_DES }}</center></td>
                <td><center>{{ $data->jumlah_gangguan_UP_Copper }}</center></td>
                <td><center>{{ $data->jumlah_gangguan_UP_GPON }}</center></td>
                <td><center>{{ $data->jumlah_prov_UP }}</center></td>
                <td><center>{{ $data->jumlah_addon_UP }}</center></td>
                <td><center>{{ $data->jumlah_asis_UP }}</center></td>
                <td><center>{{ $data->UP }}</center></td>
                <td><center>{{ $MH_UP }}</center></td>
                <td><center>{{ round($PROD,2) }}%</center></td>
                <td><center>{{ round($PROD_WO,2) }}%</center></td>
              </tr>
              <?php $number++; ?>
            @endforeach
            <tr>
              <td colspan="2">Total</td>
              <td><center>{{ $total_target_MH }}</center></td>
              <td><center>{{ $total_WO }}</center></td>
              <td><center>{{ $totalpsb }}</center></td>
              <td><center>{{ $totalggn }}</center></td>
              <td><center>{{ $total_MH }}</center></td>
              <td><center>{{ $total_NO_UPDATE }}</center></td>
              <td><center>{{ $total_Progress }}</center></td>
              <td><center>{{ $total_kendala }}</center></td>
              <td><center>{{ $total_UP_D }}</center></td>
              <td><center>{{ $total_UP_C }}</center></td>
              <td><center>{{ $total_UP_A }}</center></td>
              <td><center>{{ $total_UP_Prov }}</center></td>
              <td><center>{{ $total_UP_addon }}</center></td>
              <td><center>{{ $total_UP_asis }}</center></td>
              <td><center>{{ $total_UP_Prov+$total_UP_A+$total_UP_C }}</center></td>
              <td><center>{{ $total_MH_ACH }}</center></td>
              <?php
                @$total_prod = ($total_MH_ACH/$total_target_MH)*100;
                @$total_prod_wo = ($total_MH_ACH/$total_MH)*100;
              ?>
              <td><center>{{ round($total_prod,2) }}%</center></td>
              <td><center>{{ round($total_prod_wo,2) }}%</center></td>
            </tr>
        </table>


      </div>
    </div>
  </div>
</div>

<div id="timexcount">0 ms</div>
		<div class="panel-body">
		<script>
        $(document).ready(function(){
          $("[data-toggle='popover']").popover({html:true});
        })
				var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
        var c=0;
        var minutes= 0;
        var t;
        var timer_is_on=0;

        function timedCount(element)
        {
        document.getElementById(element).innerHTML = minutes+' min '+c+' sec';
        c=c+1;
        if (c%60==0){
          minutes+=1;
          c=0;
        }
        t=setTimeout("timedCount()",1000);
        }

        function showElement(element){
          document.getElementById(element).innerHTML = "Detected";
        }


    </script>

							<div id="bar-10" style="height: 450px; width: 100%;"></div>
						</div>
					</div>
  </div>
@endsection
