<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>Tomman Ver 1.5az</title>
  @yield('header')
  <script src="https://psb.tomman.info/js/jquery.min.js"></script>
  <script src="https://psb.tomman.info/bower_components/vue/dist/vue.min.js"></script>
  <script src="https://psb.tomman.info/bower_components/bootstrap-list-filter/bootstrap-list-filter.min.js"></script>
  <link rel="stylesheet" href="https://psb.tomman.info/bower_components/bootswatch-dist/css/bootstrap.min.css" />
  <script src="https://psb.tomman.info/bower_components/bootswatch-dist/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="https://psb.tomman.info/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="https://psb.tomman.info/bower_components/select2-bootstrap/select2-bootstrap.css" />
  <link rel="stylesheet" href="https://psb.tomman.info/bower_components/datepicker/css/datepicker.css" />

  <script type="text/javascript" src="https://psb.tomman.info/bower_components/dx/globalize.min.js"></script>

  {{-- time picker --}}
  <link rel="stylesheet" href="https://psb.tomman.info/bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
  <script src="https://psb.tomman.info/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>

  <style media="screen">
  .table-responsive {
    background-color: #FFF;
  }
  .table {
    background-color: #FFF;
  }
  body {
    background-color : #e2e2e2;
  }
  h3 {
    font-size: 24px;
  }
  .footer {
    padding: 10px 20px;
    background-color: #eee;
  }

  .list-group-item span, .form-group p.form-control-static {
    word-wrap: break-word;
  }

  .navbar-brand {
    font-size: 14px;
    font-weight: bold;
  }
  .navbar {
    margin-bottom: 10px;

  }
  .navbar-nav {
    float: left;
    margin: 0;
    font-size: 11px;
  }
  .navbar-nav > li > a {

  }
  .navbar-brand {

  }
  .navbar-toggle {

  }
  .redxxx {
    color : #e74c3c;
  }

  .input-photos img {
    width: 100px;
    height: 150px;
    margin-bottom: 5px;
  }
  @media (max-width: 900px) {
    #menu-main .navbar-right li {
      border-top: 1px solid #E7E7E7;
    }
  }
  .navbar {
    box-shadow: 0 1px 10px rgba(0,0,0,0.3);
  }
  .nav-tabs {
   border : 1px solid #D6D6D6;
 }
 .btn-info {
   background-color: #FF5442;
 }
 .label-info {
  background-color: #2196f3;
}
.footer {
 position: fixed;
 bottom: 0px;
 width: 100%;
 background-color: #404040;
 color : #FFF;
}
.color_UP {
 color: #FFF;
 background-color : #1cd43e;
}
.color_OGP {
 color: #FFF;
 background-color : #d4841c;
}
.color_KENDALA {
 color: #FFF;
 background-color : #ff0000;
}
.color_BELUM {
 color: #FFF;
 background-color : #527ee6;
}
.report {
}
#tanggal {
 padding-left: 10px;
}
.pengumuman {
  padding: 2px;
  font-weight: bold;
  background-color: #e74c3c;
  position: fixed;
  bottom : 48px;
  width: 100%;
}

.gold {
  color : #f1c40f;
}

.dropdown-submenu {
  position: relative;
}

.dropdown-submenu .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -1px;
}

</style>
</head>
<body>
<span class="mar alert alert-danger"><marquee>WASPADA CORONA VIRUS (COVID-19) # 1. Gunakan Masker dan Bawa Hand Sanitizer ketika ke Rumah Pelanggan # 2. Tuntaskan Order dengan Cepat dan tidak berlama-lama di Rumah Pelanggan # 3. Mintalah izin ke Pelanggan untuk tetap menggunakan masker selama melakukan pekerjaan # 4. Tuntaskan pekerjaan dg Berkualitas sesuai SOP # 5. Semoga kita selalu diberikan lindungan dan kesehatan dari Allah SWT. # Tomman</marquee></span>
       
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-main">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="navbar-brand" href="/"><span class="redxxx">TOM</span>MAN</a>
      </div>
      <div id="menu-main" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
         @if (session('auth')->level <> 59 && session('auth')->level <> 60 && session('auth')->level <> 61  )
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ADMIN<span class="caret"></span></a>
          <ul class="dropdown-menu">
          @if(session('auth')->level == 2 || session('auth')->level == 62 || (session('auth')->id_user == '89150015' && session('auth')->level == 46 ))
            <li><a href="/employee">Employee</a></li>
            <li><a href="/user/v2">User</a></li>
          @endif

          @if(session('auth')->level == 46 || session('auth')->level == 2 || session('auth')->level == 62)
            <li><a href="/team">Team</a></li>
          @endif
           @if(session('auth')->level == 2 || session('auth')->level == 62 || (session('auth')->id_user == '89150015' && session('auth')->level == 46 ))
            <li><a href="/datel">Datel</a></li>
            @endif
          </ul>
        </li>

        @if (session('auth')->level=='10')
        <li><a href="/home">HOME</a></li>
        <li><a href="/">WORKORDER</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DSHR<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('dt') }}">TRANSAKSI</a></li>
            <li><a href="{{ route('dc') }}">DSHR CLUSTER</a></li>
          </ul>
          <li>
          @endif

          @if (session('auth')->level==2 || session('auth')->level == 12 || session('auth')->level==50 || session('auth')->level==15 || session('auth')->level==51 || session('auth')->level==37 || session('auth')->level==46 || session('auth')->level == 62)
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DSHR<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <!-- <li><a href="{{ route('dt') }}">TRANSAKSI</a></li> -->
                <li><a href="/dshr/plasa-sales/list-wo-by-sales/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">TRANSAKSI</a></li>
              </ul>
              <li>

          @endif

          @if(session('auth')->level == 19 || session('auth')->level == 2 || session('auth')->level == 40 || session('auth')->level == 15 || session('auth')->level == 31 || session('auth')->level == 45 || session('auth')->level == 8 || session('auth')->level == 46 || session('auth')->level == 62 || session('auth')->level=65)

                @if(session('psb_reg')<>1)
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CCAN<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/orderCcan/input">Input Order</a></li>
                    <li><a href="{{ route('ccan') }}">Monitor</a></li>
                  </ul>
                </li>

                <li><a href="{{ route('assurance') }}">ASSURANCE</a></li>
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NTE<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/nte">Dashboard</a></li>
                    <li><a href="/nte/input">Input Stock Gudang</a></li>
                    <li><a href="/nte/input/teknisi">Input Stock Teknisi</a></li>
                  </ul>
                </li>


                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROVISIONING<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/hdfallout">WO HD FALLOUT</a></li>
                    <li><a href="/bon/Installed/<?= date('Y-m-d') ?>">MONITORING LAPORAN</a></li>
                    <li><a href="/dispatch/search">WORK ORDER</a></li>
                    <li><a href="/manja/list/{{ date('Y-m-d') }}">MANJA DISPATCH</a></li>
                    <li><a href="/sms/prov/{{ date('Y-m-d') }}">SMS MANJA</a></li>
                    <li><a href="/laporan-material">LAPORAN MATERIAL SHVA</a></li>
                    <li><a href="/laporan-material-migrasi">LAPORAN MATERIAL MIGRASI</a></li>
                    <li><a href="/customGrab">CUSTOM GRAB SC</a></li>
                    <li><a href="/scbe/search">WORK ORDER SC BE</a></li>
                    <li><a href="/odpfull/search">ODP FULL</a></li>
                    <li><a href="/reboundary/list/{{ date('Y-m-d') }}">REBOUNDARY</a></li>
                  </ul>
                </li>
                @else
<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROVISIONING<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/bon/Installed/<?= date('Y-m-d') ?>">MONITORING LAPORAN</a></li>
                    <li><a href="/dispatch/search">WORKORDER BY SC</a></li>
                    <li><a href="/scbe/search">WORKORDER BY SCBE</a></li>
                    <li><a href="/manja/list/{{ date('Y-m-d') }}">MANJA DISPATCH</a></li>
                    <li><a href="/produktifitastek/{{ date('Y-m-d') }}/1/ALL">PRODUKTIFITAS</a></li>
                    <li><a href="/dispatch/matrix?date=<?php echo date('Y-m-d') ?>&sektor=PROV">MATRIX ORDER</a></li>
                    <li><a href="/customGrab">CUSTOM GRAB SC</a></li>
                    <li><a href="/dashboard/rekon/<?php echo date('Y-m') ?>">DASHBOARD REKON</a></li>
                    <li><a href="/dashboard/scbe/{{ date('Y-m-d') }}">DASHBOARD SCBE</a></li>

                  </ul>
                </li>
                @endif
            @endif

            @if (session('auth')->level==58)
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROVISIONING<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    @if (session('auth')->level==2 || session('auth')->level==58)
                        <li><a href="/reboundary/list">REBOUNDARY</a></li>
                    @endif
                  </ul>
                </li>
            @endif

            @if(session('auth')->level == 19 || session('auth')->level == 2 || session('auth')->level == 40 || session('auth')->level == 15 || session('auth')->level == 31 || session('auth')->level == 45 || session('auth')->level == 8 || session('auth')->level == 46 || session('auth')->level == 62)
              @if(session('psb_reg')<>1)
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><MAP>MATRIX</MAP><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="/dispatch/matrix?date=<?php echo date('Y-m-d') ?>&sektor=ALL">ALL SEKTOR</a></li>
                      <li><a href="/new_matrix/{{ date('Y-m-d') }}">SEKTOR ASR</a></li>
                      <li><a href="/dispatch/matrix?date=<?php echo date('Y-m-d') ?>&sektor=PROV">SEKTOR PROV</a></li>
                      <li><a href="/dispatch/matrix-reboundary/{{ date('Y-m-d') }}">SEKTOR REBOUNDARY</a></li>
                      <li><a href="/dispatch/matrixHD/<?php echo date('Y-m-d') ?>">Matrix HD</a></li>
                      <li><a href="/ranking/no-update">Ranking No Update</a></li>
                      <li><a href="/matrikMt/none/none/none">Matrik order Maintenance</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><MAP>MIGRASI</MAP><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="/migrasi/search">Workorder</a></li>
                      <li><a href="/ranking/no-update">Ranking No Update</a></li>
                      {{-- <li><a href="/migrasi/monitoring">Monitoring</a></li> --}}
                    </ul>
                  </li>
                @endif
              @endif

              @if(session('auth')->level<>56 and session('auth')->level<>55 and session('auth')->level<>59 )
              @if(session('psb_reg')<>1)
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DASHBOARD<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/gaulbysebab?sektor=&date=<?php echo date('Y-m') ?>">PENYEBAB GAUL</a></li>
                        <li><a href="/gaulbyaction?sektor=&date=<?php echo date('Y-m') ?>">ACTION GAUL</a></li>
                        <li><a href="/trendgaul">TREND GAUL</a></li>
                        <li><a href="/dashboard/unspec">UNDERSPEC</a></li>
                        <li><a href="/kehadiran/{{ date('Y-m-d') }}">KEHADIRAN</a></li>
                        <li><a href="/manja/<?php echo date('Y-m-d') ?>">MANJA ASR</a></li>
                        <li><a href="/dashboard/rekon/<?php echo date('Y-m') ?>">REKON</a></li>
                        <li><a href="/panjangtarikan/<?php echo date('Y-m') ?>">PANJANG TARIKAN</a></li>
                        <li><a href="/dashboard/migrasi/<?php echo date('Y-m-d') ?>/ALL">MIGRASI</a></li>
                        <li><a href="/ReportIbooster/<?php echo date('Y-m-d') ?>">REPORT IBOOSTER</a></li>
                        <li><a href="/dashboard/assurance/<?php echo date('Y-m-d') ?>">ASSURANCE</a></li>
                        <!-- <li><a href="/kpi/assurance?periode=<?php echo date('Ym') ?>&mitra=ALL">ASSURANCE (KPI SEKTOR)</a></li> -->
                        <!-- <li><a href="/kpi/assuranceMitra?periode=<?php echo date('Ym') ?>&mitra=ALL">ASSURANCE (KPI MITRA)</a></li> -->
                        <li><a href="/dashboard/provisioning/<?php echo date('Y-m-d') ?>/ALL">PROVISIONING</a></li>
                        <!-- <li><a href="/dashboard/PI?dateStart=<?php echo date('Y-m-d') ?>&dateEnd=<?php echo date('Y-m-d') ?>">PROVISIONING (PI)</a></li> -->
                        <li><a href="/telco/<?php echo date('Y-m-d') ?>">TELCO</a></li>
                        <!-- <li><a href="/dashboard/guarante/{{ date('Y-m-d') }}">FULFILLMENT GUARANTE</a></li> -->
                        <li><a href="/dashboard/scbe/{{ date('Y-m-d') }}">SCBE</a></li>
                        <li><a href="/dasboard/dispatch-reboundary/{{ date('Y-m-d') }}">REBOUNDARY</a></li>
                        <li><a href="/dashboard/dashboad-wifi/{{ date('Y-m-d') }}">CCAN</a></li>
                        <li><a href="/dashboard/wo-ps/qrcode/{{ date('Y-m-d') }}">PS QRCODE</a></li>
                      </ul>
                    </li>
              @else
              <!-- <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DASHBOARD<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/dashboard/scbe/{{ date('Y-m-d') }}">SCBE</a></li>
                      <li><a href="/dashboard/rekon/<?php echo date('Y-m') ?>">REKON</a></li>
                        </ul>
              </li> -->
              @endif
              @endif

              @if(session('auth')->level<>55 and session('auth')->level<>59)
               @if(session('psb_reg')<>1)

                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">LEADERBOARDS<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/leaderboards/ALPHA.ASR/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/UPATEK">ALPHA ASR UPATEK</a></li>
                        <li><a href="/leaderboards/ALPHA.ASR/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/CUI">ALPHA ASR CUI</a></li>
                        {{-- <li><a href="/leaderboards/ALPHA.PROV/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/TA">ALPHA PROV</a></li> --}}
                        <li><a href="/produktifitastek/{{ date('Y-m-d') }}/ALL">ALPHA PROV</a></li>
                        <li><a href="/leaderboards/DELTA.A/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/ALL">DELTA A</a></li>
                        <li><a href="/produktifitastek/{{ date('Y-m-d') }}/1/ALL">PRODUKTIFITAS PROV</a></li>
                        <li><a href="/produktifitasteksewa/{{ date('Y-m-d') }}/1/ALL">PRODUKTIFITAS PROV (SEWA ALKER)</a></li>
                        <li><a href="/produktifitastekA/{{ date('Y-m-d') }}/ALL/ALL">PRODUKTIFITAS TEK ASR ALL</a></li>
                        <li><a href="/produktifitastekA/{{ date('Y-m-d') }}/INNER/ALL">PRODUKTIFITAS TEK ASR INNER</a></li>
                        <li><a href="/produktifitastekA/{{ date('Y-m-d') }}/OUTER/ALL">PRODUKTIFITAS TEK ASR OUTER</a></li>
                        <li><a href="/produktifitastekA/{{ date('Y-m-d') }}/UP0/ALL">PRODUKTIFITAS TEK ASR NON PRODUKTIF</a></li>
                        <li><a href="/produktifitassektor/{{ date('Y-m-d') }}/ALL">PRODUKTIFITAS SEKTOR ASR ALL</a></li>
                        <li><a href="/produktifitassektor/{{ date('Y-m-d') }}/INNER">PRODUKTIFITAS SEKTOR ASR INNER</a></li>
                        <li><a href="/produktifitassektor/{{ date('Y-m-d') }}/OUTER">PRODUKTIFITAS SEKTOR ASR OUTER</a></li>
                        <li><a href="/produktifitassektor/{{ date('Y-m-d') }}/UP0">PRODUKTIFITAS SEKTOR ASR NON PRODUKTIF</a></li>
                      </ul>
                    </li>
                    @endif
              @endif


              @if(session('auth')->level == 54 || session('auth')->level == 2 || session('auth')->level == 62)
              @if(session('psb_reg')<>1)
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">REVIEW<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="/dashboardReview/{{ date('Y-m') }}">Dashboard</a></li>
                      <li><a href="/review/followup/{{ date('Y-m-d') }}">Follow Up</a></li>
                    </ul>
              </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SMS<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/new/sms">NEW SMS</a></li>
                    <li><a href="/sms/inbox">INBOX</a></li>
                    <li><a href="/sms/outbox">OUTBOX</a></li>
                    <li><a href="/sent/items">SENT ITEM</a></li>
                    <li class="dropdown-submenu submenu-sms">
                     <a tabindex="-1" class="test" href="#">CONTACT<span class="caret"></span></a>
                     <ul class="dropdown-menu">
                       <li><a href="/group/sms">GROUP CONTACT</a></li>
                     </ul>
                   </li>
                 </ul>
               </li>
               @endif
               @endif

               @if(session('auth')->level<>55 and session('auth')->level<>59)
                @if(session('psb_reg')<>1)
                   <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NEWS<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          @if(session('auth')->level == 2)
                              <li><a href="/news">Input News</a></li>
                          @endif
                          <li><a href="/seenews">See News</a></li>
                        </ul>
                  </li>
                @endif
              @endif

              @if (session('auth')->level == 2 || session('auth')->level == 10 || session('auth')->level == 46 || session('auth')->level == 62 )
              @if(session('psb_reg')<>1)
                 <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">RFC<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/rfc/input">Input</a></li>
                        <li><a href="/rfc/sisamaterial">Saldo Material</a></li>
                      </ul>
                </li>
              @endif
              @endif

              @if (session('auth')->level == 10 )
                <li><a href="/teknisi/listwo">LIST WO</a></li>
              @endif

              @if(session('auth')->level==10 || session('auth')->level==2 || session('auth')->level==46 || session('auth')->level==15 || session('auth')->level == 62)
               @if(session('psb_reg')<>1)
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TOOLS<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="/tool/logistik">Logistik</a></li>
                      <!-- <li><a href="/tool/logistik-gudang">Logistik Gudang</a></li> -->
                      <!-- <li><a href="/tool/logistik/pakai">Logistik Pemakaian</a></li> -->
                      <li><a href="/tool/logistik/kembali">Logistik Kembali</a></li>
                      <li><a href="/tool/listOrderTarikan/{{ date('Y-m-d') }}">List Order Tarikan</a></li>
                      <li><a href="/tool/listOrderTarikanGangguan/{{ date('Y-m-d') }}">List Order Tarikan Gangguan</a></li>
                      <li><a href="/tool/rekapodp">Rekap ODP</a></li>
                      <li><a href="/tool/listmaterialstok">Stok Material</a></li>
                      <li><a href="/tool/rekap-material-rfc/{{ date('Y-m') }}">Rekap Material RFC</a></li>
                      <li><a href="/tool/rekap-material-rfc-tl/{{ date('Y-m') }}">Rekap Material RFC 2</a></li>
                      <li><a href="/cekwo-teknisi/dashboard">WO UP Teknisi</a></li>

                      <!-- <li><a href="/tool/listhadir/{{ date('Y-m-d') }}">List Hadir Teknisi</a></li> -->
                    </ul>
              </li>
              @endif
              @endif
              @endif
              @if (session('auth')->level == 59 )
               @if(session('psb_reg')<>1)
              <li><a href="{{ route('assurance') }}">ASSURANCE</a></li>
              @endif
              @endif

              @if (session('auth')->level==61 || session('auth')->level==59)
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DSHR<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="/dshr/plasa-sales/list-wo-by-sales">TRANSAKSI</a></li>
                    </ul>
                </li>
              @endif
               @if(session('psb_reg')<>1 && session('auth')->level <> 62)
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TICKETING<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="/ticketing">TICKETING</a></li>

                      @if(session('auth')->level<>61)
                        <li><a href="/ticketing/dashboard">DASHBOARD OPEN</a></li>
                        <li><a href="/ticketing/dashboardClose/{{ date('Y-m-d') }}">DASHBOARD PROGRESS</a></li>
                      @endif

                      <li><a href="/listTicketPersonal">YOUR OPEN PROGRESS</a></li>
                    </ul>
              </li>


              @if (session('auth')->level==2)
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DC<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="/split/saldo-dc">SPLIT DC</a></li>
                      <li><a href="/split/dashboard/mitra">DASHBOARD</a></li>
                    </ul>
                </li>
              @endif
              @endif
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">MARINA<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                      <a class="test" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Register<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="/marina/order/input">Manual</a></li>
                          <li><a href="/marina/listtommanpsb/24">Kendala ODP FULL</a></li>
                          <li><a href="/marina/listtommanpsb/2">Kendala ODP LOSS</a></li>
                          <li><a href="/marina/listtommanpsb/11">Kendala INSERT TIANG</a></li>
                        </ul>
                    </li>
                    <li><a href="/marina/tech">Index Teknisi</a></li>
                    <li><a href="/marina/saldo">Saldo RFC</a></li>
                    <li><a href="/marina/order/none/none/{{date('Y-m')}}/all">Matrik Program</a></li>
                    <li class="dropdown-submenu">
                      <a class="test" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Matrix Order Per Tim<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="/marina/matrix/{{date('Y-m-d')}}/0">ALL</a></li>
                          <li><a href="/marina/matrix/{{date('Y-m-d')}}/1">Benjar</a></li>
                          <li><a href="/marina/matrix/{{date('Y-m-d')}}/2">Gamas</a></li>
                          <li><a href="/marina/matrix/{{date('Y-m-d')}}/3">Odp Loss</a></li>
                          <li><a href="/marina/matrix/{{date('Y-m-d')}}/4">Remo</a></li>
                          <li><a href="/marina/matrix/{{date('Y-m-d')}}/5">Utilitas</a></li>
                          <li><a href="/marina/matrix/{{date('Y-m-d')}}/6">Insert Tiang</a></li>
                          <li><a href="/marina/matrix/{{date('Y-m-d')}}/7">Revitalisasi FTM</a></li>
                          <li><a href="/marina/matrix/{{date('Y-m-d')}}/8">ODP SEHAT</a></li>
                          <li><a href="/marina/matrix/{{date('Y-m-d')}}/9">ODP Full</a></li>
                        </ul>
                    </li>
                    <li><a href="/marina/khs">Designator</a></li>
                    <li class="dropdown-submenu">
                      <a class="test" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Setting Designator Program<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="/marina/setting/1">Benjar</a></li>
                          <li><a href="/marina/setting/2">Gamas</a></li>
                          <li><a href="/marina/setting/3">Odp Loss</a></li>
                          <li><a href="/marina/setting/4">Remo</a></li>
                          <li><a href="/marina/setting/5">Utilitas</a></li>
                          <li><a href="/marina/setting/6">Insert Tiang</a></li>
                          <li><a href="/marina/setting/7">Revitalisasi FTM</a></li>
                          <li><a href="/marina/setting/8">Benjar ODP</a></li>
                          <li><a href="/marina/setting/9">ODP Full</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                      <a class="test" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Rev<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="/marina/revenuev2/TA/{{date('Y-m')}}">TA</a></li>
                          <li><a href="/marina/revenuev2/MITRA/{{date('Y-m')}}">Mitra</a></li>
                        </ul>
                    </li>
                    <li><a href="/marina/download">Download</a></li>
                  </ul>
              </li>


              <li><a href="/loginstarclick">SCNCX</a></li>
              <li><a href="/logout">LOGOUT</a></li>

             </ul>
           </div>
         </div>
       </nav>
       <div class="container-fluid" style="margin-bottom : 50px">
        @yield('content')
      </div>


      <script>
        $(document).ready(function(){
          $('.dropdown-submenu a.test').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
          });
        });
      </script>
      <script src="https://psb.tomman.info/bower_components/select2/select2.min.js"></script>
      <script src="/js/marquee3k.min.js"></script>
        <script>
Marquee3k.init()
</script>
      <!-- <div class="pengumuman">
      </div>
      <div class="footer">

        <a href="/logout" class="btn btn-sm btn-warning pull-right">Logout</a>
        <small><span class="gold">{{ session('auth')->id_user }}</span> {{ session('auth')->nama }}</small> <br />

      </div> -->

      @yield('plugins')
    </body>
    </html>
