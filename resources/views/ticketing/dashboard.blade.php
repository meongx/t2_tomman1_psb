@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    td {
      padding : 5px;
      border:1px solid #ecf0f1;
    }
    .pdnol {
      padding : 0px;
    }
    .center {
      text-align: center;
    }
    th {
      text-align: center;
      vertical-align: middle;
      background-color: #0073b7;
      padding : 5px;
      color : #FFF;
      border:1px solid #ecf0f1;
    }
    td {
      font-size: 14px;
      padding : 3px !important;

    }
    .center a {
      color : #666666 !important;
    }
    .green {
      background-color: #2ecc71;
      font-weight: bold;
      color : #FFF;
    }
    .green a:link{
      color : #FFF;
    }
    .red {
      background-color: #e67e22;
      font-weight: bold;
      color : #FFF;
    }
    .redx {
      background-color: #e74c3c;
      font-weight: bold;
      color : #FFF;
    }
    .red a:link{
      color : #FFF;
    }
    .gray {
      background-color : #f4f4f4;
    }
  </style>  
  <center><h3>Dashboard Ticket Tomman</h3></center>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading"></div>
  			<div class="panel-body">
  				<table width="100%">
		  			<tr>
		              <th rowspan="2">SEKTOR</th>
		              <th colspan="7">DURASI (Jam)</th>
		              <th colspan="2">SUM</th>
		              <th rowspan="2">TTL</th>
		            </tr>
		            <tr>
		              <th class="green">0-2</th>
		              <th class="green">2-4</th>
		              <th class="green">4-6</th>
		              <th class="red">6-8</th>
		              <th class="red">8-10</th>
		              <th class="red">10-12</th>
		              <th class="redx">12-24</th>
		              <th class="redx">>24</th>
		              <th><24</th>
		            </tr>
		            <?php
		            	$total_jumlah = 0;
		            	$total_x0dan2 = 0;
		            	$total_x2dan4 = 0;
		            	$total_x4dan6 = 0;
		            	$total_x6dan8 = 0;
		            	$total_x8dan10 = 0;
		            	$total_x10dan12 = 0;
		            	$total_x12dan24 = 0;
		            	$total_lebih24 = 0;
		            	$total_kurang24 = 0;
		            ?>
		            @foreach ($query as $num => $result)
		            <?php
		            	$total_jumlah += $result->jumlah;
		            	$total_x0dan2 += $result->x0dan2;
		            	$total_x2dan4 += $result->x2dan4;
		            	$total_x4dan6 += $result->x4dan6;
		            	$total_x6dan8 += $result->x6dan8;
		            	$total_x8dan10 += $result->x8dan10;
		            	$total_x10dan12 += $result->x10dan12;
		            	$total_x12dan24 += $result->x12dan24;
		            	$total_lebih24 += $result->lebih24;
		            	$total_kurang24 += $result->kurang24;
		            ?>
		            <tr>
		            	<td>{{ $result->sektor_asr }}</td>
		            	<td align="center"><a href="/ticketing/dashboardList/{{ $result->sektor_asr }}/x0dan2">{{ $result->x0dan2 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/{{ $result->sektor_asr }}/x2dan4">{{ $result->x2dan4 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/{{ $result->sektor_asr }}/x4dan6">{{ $result->x4dan6 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/{{ $result->sektor_asr }}/x6dan8">{{ $result->x6dan8 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/{{ $result->sektor_asr }}/x8dan10">{{ $result->x8dan10 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/{{ $result->sektor_asr }}/x10dan12">{{ $result->x10dan12 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/{{ $result->sektor_asr }}/x12dan24">{{ $result->x12dan24 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/{{ $result->sektor_asr }}/lebih24">{{ $result->lebih24 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/{{ $result->sektor_asr }}/kurang24">{{ $result->kurang24 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/{{ $result->sektor_asr }}/ALL">{{ $result->jumlah }}</a></td>
		            </tr>
		            @endforeach

		            <tr>
		            	<td>UNDISPATCH</td>
		            	@foreach ($undispatch as $un)
			            	<td align="center"><a href="/ticketing/dashboardList/undispatch/list/x0dan2">{{ $un->x0dan2 ?: 0 }}</a></td>
			            	<td align="center"><a href="/ticketing/dashboardList/undispatch/list/x2dan4">{{ $un->x2dan4 ?: 0 }}</a></td>
			            	<td align="center"><a href="/ticketing/dashboardList/undispatch/list/x4dan6">{{ $un->x4dan6 ?: 0 }}</a></td>
			            	<td align="center"><a href="/ticketing/dashboardList/undispatch/list/x6dan8">{{ $un->x6dan8 ?: 0 }}</a></td>
			            	<td align="center"><a href="/ticketing/dashboardList/undispatch/list/x8dan10">{{ $un->x8dan10 ?: 0 }}</a></td>
			            	<td align="center"><a href="/ticketing/dashboardList/undispatch/list/x10dan12">{{ $un->x10dan12 ?: 0 }}</a></td>
			            	<td align="center"><a href="/ticketing/dashboardList/undispatch/list/x12dan24">{{ $un->x12dan24 ?: 0 }}</a></td>
			            	<td align="center"><a href="/ticketing/dashboardList/undispatch/list/lebih24">{{ $un->lebih24 ?: 0 }}</a></td>
			            	<td align="center"><a href="/ticketing/dashboardList/undispatch/list/kurang24">{{ $un->kurang24 ?: 0 }}</a></td>
			            	<td align="center"><a href="/ticketing/dashboardList/undispatch/list/ALL">{{ $un->jumlah }}</a></td>
		            	@endforeach
		            </tr>

		            <tr>
		            	<td>TOTAL</td>
		            	<td align="center"><a href="/ticketing/dashboardList/ALL/x0dan2">{{ $total_x0dan2 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/ALL/x2dan4">{{ $total_x2dan4 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/ALL/x4dan6">{{ $total_x4dan6 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/ALL/x6dan8">{{ $total_x6dan8 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/ALL/x8dan10">{{ $total_x8dan10 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/ALL/x10dan12">{{ $total_x10dan12 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/ALL/x12dan24">{{ $total_x12dan24 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/ALL/lebih24">{{ $total_lebih24 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/ALL/kurang24">{{ $total_kurang24 }}</a></td>
		            	<td align="center"><a href="/ticketing/dashboardList/ALL/ALL">{{ $total_jumlah }}</a></td>
		            </tr>
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection