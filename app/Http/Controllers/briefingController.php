<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\HomeModel;
use App\DA\briefingModel;
use Telegram;

class briefingController extends Controller
{
    public function input(){
    	date_default_timezone_set("Asia/Makassar");
    	$nik = session('auth')->id_karyawan;
		$group_telegram = HomeModel::group_telegram($nik);

		// simpan data
		$exist = briefingModel::getDataByTglNik(date('Y-m-d'), $nik);
		if (!$exist){
			$data = briefingModel::simpanData($nik);
			$id   = $data; 	
		}
		else{
			$id = $exist->id;
			// cek keterangan 
			if ($exist->status==0){
				$path=public_path().'/upload/briefing/'.$id;
				 \File::deleteDirectory($path);
				briefingModel::hapusSemuaFoto($id);
			}
		}

		$getData = briefingModel::getDataById($id);
		$sektor  = briefingModel::getSektor();
		
		return view('briefing.briefingInput',compact('group_telegram','nik', 'id', 'sektor', 'getData'));
    }

    public static function simpanFoto(Request $req, $id){
		if ($req->hasFile('file')){
			$path = public_path().'/upload/briefing/'.$id.'/';
			if (!file_exists($path)) {
				if (!mkdir($path, 0770, true))
					return 'gagal menyiapkan folder foto evidence';
			}

			try {
				$image = $req->file('file');
				$imageName = $image->getClientOriginalName();
				$image->move("$path", $imageName);   

				briefingModel::simpanFoto($imageName, $id);
			}
			catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
				return 'gagal menyimpan foto evidence ';
			}	
		}
	}

	public static function hapusFoto(Request $req, $id){
		date_default_timezone_set("Asia/Makassar");
		$filename =  $req->get('filename');
		$path=public_path().'/upload/briefing/'.$id.'/'.$filename;
		echo $path;
		if (file_exists($path)) {
			unlink($path);
		};

		briefingModel::hapusFotoByFilename($filename, $id);
	}

	public function simpanLaporan (Request $req){
		date_default_timezone_set("Asia/Makassar");
		$nik = session('auth')->id_karyawan;
		$this->validate($req,[
			'sektor'	=> 'required',
			'catatan'	=> 'required'
		],[
			'sektor.required'	=> 'Sektor Jangan Kosong',
			'catatan.required'	=> 'Catatan Diisi'
		]);

		//update
		briefingModel::updateData($req, $req->idLaporan);

		// kirim sektor
		$dataTl = briefingModel::getDataTl($nik);
		$dataBriefing = briefingModel::getDataById($req->idLaporan);

		if ($nik==$dataTl->TL_NIK){
			$nikPelapor = $dataTl->TL_NIK; 
			$pelapor    = $dataTl->TL;
		}
		else if ($nik==$dataTl->Nik_Atl){
			$nikPelapor = $dataTl->Nik_Atl;
			$pelapor    = $dataTl->Atl;
		}
		else{
			$nikPelapor = $dataTl->Nik_Atl2;
			$pelapor    = $dataTl->Atl2;	
		};
	
		$pesan = "BRIEFING REPORT ".date('Y-m-d'). "\n";
		$pesan .= "=================================\n";
		$pesan .= "Dilaporkan oleh : ".$nikPelapor."[ ".$pelapor." ]\n";
		$pesan .= "catatan Briefing : ".$dataBriefing->catatan;
		$chatID = $req->sektor;
	
		Telegram::sendMessage([
      		'chat_id' => $chatID,
      		'text' => $pesan
    	]);

    	// kirim foto
    	$dataFoto = briefingModel::getFoto($req->idLaporan);
    	if (count($dataFoto)<>0){
    		foreach($dataFoto as $foto){
    			$file = public_path()."/upload/briefing/".$req->idLaporan."/".$foto->foto;
				if (file_exists($file)){
					Telegram::sendPhoto([
						'chat_id' => $chatID,
						'photo' => $file
					]);
				}
    		}
    	}
	
    	// kirim teknisi aproval
    	// assurance
    	$pesan = "Teknisi Sudah Diaprov Hadir \n";
    	$pesan .= "================================= \n";
    	$pesan .= "Teknisi Assurance \n\n";
    	$dataTeknisi = HomeModel::active_team_Briefing($nik,0);
    	$num = 1;
    	
    	foreach($dataTeknisi as $no=>$teknisi){
    		if($teknisi->approval=='1'){
    			$pesan .= $num++.'. '. $teknisi->nik." [ ".$teknisi->nama." ]"." [".$teknisi->mitra."] \n";
    		};
    	};

    	Telegram::sendMessage([
      		'chat_id' => $chatID,
      		'text' => $pesan
    	]);

    	// provisionig
    	$pesan = "Teknisi Sudah Diaprov Hadir \n";
    	$pesan .= "================================= \n";
    	$pesan .= "Teknisi Provisioning \n\n";
    	$dataTeknisi = HomeModel::active_team_Briefing($nik,1);
    	$num = 1;
    	
    	foreach($dataTeknisi as $no=>$teknisi){
    		if($teknisi->approval=='1'){
    			$pesan .= $num++.'. '. $teknisi->nik." [ ".$teknisi->nama." ]"." [".$teknisi->mitra."] \n";
    		};
    	};

    	Telegram::sendMessage([
      		'chat_id' => $chatID,
      		'text' => $pesan
    	]);

    	return redirect('/briefing/list/'.date('Y-m'))->with('alerts',[['type' => 'success', 'text' => 'Laporan Berhasil Disimpan']]);
	}

	public function list($tgl){
		date_default_timezone_set("Asia/Makassar");
		$nik = session('auth')->id_karyawan;
		$group_telegram = HomeModel::group_telegram($nik);
		$listData = briefingModel::getAllBriefingTgl($tgl, $nik);
		$getFoto  = briefingModel::getFotoByNik($nik);
		$dataNow  = briefingModel::getDataByTglNik(date('Y-m-d'),$nik);

		return view('briefing.briefingList',compact('group_telegram','nik', 'id', 'listData', 'getFoto', 'dataNow'));
	}

}
