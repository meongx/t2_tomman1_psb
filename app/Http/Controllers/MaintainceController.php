<?php

namespace App\Http\Controllers;
use Excel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use File;
use DB;
use Telegram;

class MaintainceController extends Controller
{
  protected $photoInputs = [
    'Sebelum','Progress', 'Sesudah'
  ];
  protected static $photoInputst = [
    'Sebelum','Progress', 'Sesudah'
  ];
  public function search(Request $req)
  {
    $list = null;
    if ($req->has('q')) {
      $list = DB::table('maintaince')->where('no_tiket', $req->input('q'))->get();
    }
    //dd($list);
    return view('maintaince.search', compact('list'));
  }
  public function index($id)
  {
    $auth = session('auth');
    //dd($auth);
    $status = " and (status is NULL OR status = '' OR status = 'pending')";
    $condition = "username1 = '".$auth->id_karyawan."' OR username2 = '".$auth->id_karyawan."'";
    if($auth->level == 53)
      $condition = "1";
    if($id != 'np')
      $status = " and status='".$id."'";
    $list = DB::select('
      SELECT *
      FROM maintaince
      WHERE '.$condition.' '.$status.' order by id desc
    ');
    //dd($list);
    /*
    $list = DB::select('
      SELECT m.*, m2.status, m2.team, 
      (SELECT sum((qty*hmbjm)) FROM maintaince_mtr mm left join item i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq,
      (SELECT sum((qty*hjbjm)) FROM maintaince_mtr mm left join item i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_jasa
      FROM maintaince m left join maintaince_progres m2 on m.id = m2.maintaince_id
      WHERE 1 order by id desc
    ');*/
    return view('maintaince.list', compact('list'));
  }
  public function input($id)
  {
    $exists = DB::select('
      SELECT *
      FROM maintaince
      WHERE id = ?
    ', [
      $id
    ]);
    if (count($exists)) {
      $data = $exists[0];
    }
    else{
      $data = new \stdClass();
    }
    $regu = DB::select('
      SELECT id_regu as id,uraian as text, telp, job
      FROM regu
      WHERE
      ACTIVE = 1
    ');
    return view('maintaince.input', compact('data', 'regu'));
  }

  //to form progress
  public function update($id)
  {
    $foto = $this->photoInputs;
    $exists = DB::select('
      SELECT *
      FROM maintaince
      WHERE id = ?
    ', [
      $id
    ]);
    if (count($exists)) {
      $data = $exists[0];
    }
    else{
      $data = new \stdClass();
    }
    $materials = DB::select('
        SELECT
          i.*, i.uraian as nama_item,
          COALESCE(m.qty, 0) AS qty
        FROM khs_maintenance i
        LEFT JOIN
          maintaince_mtr m
          ON i.id_item = m.id_item
          AND m.maintaince_id = ? 
        ORDER BY m.id_item
      ', [
        $id
      ]);
    //print_r($materials);
    return view('maintaince.progress', compact('data', 'materials', 'foto'));
  }
  private function handleFileUpload($request, $id)
  {
    foreach($this->photoInputs as $name) {
      $input = 'photo-'.$name;
      if ($request->hasFile($input)) {
        //dd($input);
        $path = public_path().'/upload/maintenance/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true))
            return 'gagal menyiapkan folder foto evidence';
        }
        $file = $request->file($input);
        $ext = 'jpg';
        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");
          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence '.$name;
        }
      }
    }
  }
  public function save(Request $request, $id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM maintaince
      WHERE id = ?
    ',[
      $id
    ]);
    $realId = $id;

    if (count($exists)) {
      $data = $exists[0];

      DB::transaction(function() use($request, $data, $auth) {
        DB::table('maintaince')
          ->where('id', $data->id)
          ->update([
            'no_tiket'    => $request->input('tiket'),
            'headline'    => $request->input('headline'),
            'info'        => $request->input('info'),
            'program_id'  => $request->input('program'),
            'modified_at' => DB::raw('NOW()'),
            'modified_by' => $auth->id_karyawan ,
            'dispatch_regu_id'    => $request->input('tim'),
          ]);
        if($request->input('tim') != $data->dispatch_regu_id){
          //insert log dispatch
          $regu = DB::table('regu')->select('uraian', 'nik1', 'nik2')->where('id_regu', $request->input('tim'))->first();
          DB::table('maintaince')
          ->where('id', $data->id)
          ->update([
            'dispatch_regu_name'    => $regu->uraian,
            'username1'             => $regu->nik1,
            'username2'             => $regu->nik2
          ]);
          DB::table('maintenance_dispatch_log')->insert([
            'maintenance_id' => $data->id,
            'regu_id' => $request->input('tim'),
            'regu_name' => $regu->uraian,
            'dispatch_by' => $auth->id_karyawan,
          ]);
        }
      });
      
    }
    else {
      DB::transaction(function() use($request, $id, $auth) {
        $tiket_by = "USER";
        if($auth->level == 53){
          $tiket_by = "ADMIN";
        }
        $realId = DB::table('maintaince')->insertGetId([
          'no_tiket'    => $request->input('tiket'),
          'headline'    => $request->input('headline'),
          'info'        => $request->input('info'),
          'program_id'  => $request->input('program'),
          'created_at'  => DB::raw('NOW()'),
          'created_by'  => $auth->id_karyawan,
          'tiket_by'    => $tiket_by,
          
        ]);
        if($request->input('tim')!='' || $tiket_by == 'USER'){
          //insert regu info to maintenance table
          if($tiket_by == 'USER'){
            $regu = DB::table('regu')->select('id_regu', 'uraian', 'nik1', 'nik2')->where('nik1', $auth->id_karyawan)->orWhere('nik2', $auth->id_karyawan)->first();
            $query = DB::table('maintenance_tiket_dummy')->orderBy('id', 'desc')->first();
            $tiket_dummy = $query->no_tiket;
          }
          else{
            $regu = DB::table('regu')->select('id_regu', 'uraian', 'nik1', 'nik2')->where('id_regu', $request->input('tim'))->first();
            $tiket_dummy = $request->input('tiket');
          }
          DB::table('maintaince')
          ->where('id', $realId)
          ->update([
            'dispatch_regu_id'      => $regu->id_regu,
            'dispatch_regu_name'    => $regu->uraian,
            'username1'             => $regu->nik1,
            'username2'             => $regu->nik2
          ]);
          DB::table('maintaince')
          ->where('id', $realId)
          ->update([
            'no_tiket'    => $tiket_dummy
          ]);
          //insert log dispatch
          DB::table('maintenance_dispatch_log')->insert([
            'maintenance_id' => $realId,
            'regu_id' => $regu->id_regu,
            'regu_name' => $regu->uraian,
            'dispatch_by' => $auth->id_karyawan,
          ]);
        }
        if($tiket_by == "USER"){
          exec('cd ..;php artisan sendCreateTiket '.$id.' > /dev/null &');
          
        }
      });
      
    }
    return redirect('/maintaince/status/np')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
    ]);
  }

  public function saveProgres(Request $request, $id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM maintaince
      WHERE id = ?
    ',[
      $id
    ]);
    $this->handleFileUpload($request, $id);
    $materials = json_decode($request->input('materials'));
    if (count($exists)) {
      $data = $exists[0];

      DB::transaction(function() use($request, $data, $auth, $materials, $id) {
        DB::table('maintaince_mtr')
          ->where('maintaince_id', $data->id)
          ->delete();

        foreach($materials as $material) {
          DB::table('maintaince_mtr')->insert([
            'maintaince_id' => $data->id,
            'id_item'       => $material->id_item,
            'qty'           => $material->qty,
          ]);
        }
        $total = DB::select('
          SELECT 
          (SELECT sum((qty*material_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as material,
          (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as jasa
          FROM maintaince m
          WHERE id = ?
        ', [$id])[0];
        $hasil_by_qc = $data->hasil_by_qc;
        $hasil_by_user = $data->hasil_by_user;
        if($request->input('status')=="close"){
          $hasil_by_user = $total->jasa + $total->material;
        }else if($request->input('status')=="qqc"){
          $hasil_by_qc = $total->jasa + $total->material;
        }
        DB::table('maintaince')
          ->where('id', $data->id)
          ->update([
            'status_name'    => $request->input('status'),
            'status'    => $request->input('status'),
            'tgl_selesai'    => $request->input('tgl_selesai'),
            'modified_at'     => DB::raw('NOW()'),
            'modified_by'     => $auth->id_karyawan,
            'hasil_by_user'  => $hasil_by_user,
            'hasil_by_qc'    => $hasil_by_qc,
          ]);
      });
    }
    if($request->input('status') == "close" || $request->input('status') == "pending"){
      exec('cd ..;php artisan sendProgressTiket '.$id.' > /dev/null &');
    }
    return redirect('/maintaince/status/np')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
    ]);
  }

  public function destroy($id){
    DB::table('maintaince')
      ->where('id', [$id])->delete();

    return redirect('/maintaince')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Data']
    ]);
  }
  public function upload($id){
    $path2 = public_path().'/upload/maintenance/'.$id.'th';
    $th = array();
    if (file_exists($path2)) {
      $photos = File::allFiles($path2);
      foreach ($photos as $photo){
        array_push($th,(string) $photo);
      }
    }
    return view('maintaince.photos', compact('id','th'));
  }
  public function uploadSave(Request $request, $id){
    $files = $request->file('photo');
    $path = public_path().'/upload/maintenance/'.$id.'/';
    $th = public_path().'/upload/maintenance/'.$id.'th/';
    if (!file_exists($path)) {
      if (!mkdir($path, 0755, true))
        return 'gagal menyiapkan folder foto evidence';
    }
    if (!file_exists($th)) {
      if (!mkdir($th, 0755, true))
        return 'gagal menyiapkan folder foto evidence';
    }
    foreach ($files as $file) {
        $name = $file->getClientOriginalName();
        try {
          $moved = $file->move("$path", "$name");
          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$th/$name");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence '.$name;
        }
    }
    return redirect('/maintaince')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Upload Photo']
    ]);
  }
  public function getRekapBulanan($id){
    $maintenance = DB::select("
      SELECT m.*,m2.*,i.material_ta,i.jasa_ta,(SELECT sum((qty*material_ta)+(qty*jasa_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
      FROM maintaince m 
      right join maintaince_mtr m2 on m.id = m2.maintaince_id
      LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
      WHERE m.tgl_selesai like '".$id."%'
    ");
    $data = array();
    $lastNama = '';
    $no=0;
    $head = array();
    foreach ($maintenance as $no => $m){
      if(!empty($m->id_item)){
        $head[] = $m->id_item;
      } 
    }
    $head = array_unique($head);
    foreach ($maintenance as $no => $m){
      if($lastNama == $m->no_tiket){
        $data[count($data)-1]['Material'][$m->id_item] = $m->qty;  
        $no++;
      }else{
        $data[] = array("no_tiket" => $m->no_tiket,"total_boq" => $m->total_boq, "Material" => array());
        foreach($head as $h){
          if($h == $m->id_item){
            $data[count($data)-1]['Material'][$h] = $m->qty;
          }else{
            $data[count($data)-1]['Material'][$h] = 0;
          }
        }
        $no=0;
      }
      $lastNama = $m->no_tiket;
    }
    //var_dump($data);
    return view("laporan.ajax-maintenance", compact("data", "head"));
  }
  public function getMtRegu($id){
    $maintenance = DB::select("
      SELECT m.*,m2.*,i.material_ta,i.jasa_ta,(SELECT sum((qty*material_ta)+(qty*jasa_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
      FROM maintaince m 
      right join maintaince_mtr m2 on m.id = m2.maintaince_id
      LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
      WHERE m.tgl_selesai like '".$id."%'
    ");
    $data = array();
    $lastNama = '';
    $no=0;
    $head = array();
    foreach ($maintenance as $no => $m){
      if(!empty($m->id_item)){
        $head[] = $m->id_item;
      } 
    }
    $head = array_unique($head);
    foreach ($maintenance as $no => $m){
      if($lastNama == $m->dispatch_regu_name){
        $data[count($data)-1]['Material'][$m->id_item] = $m->qty;  
        $no++;
      }else{
        $data[] = array("no_tiket" => $m->dispatch_regu_name,"total_boq" => $m->total_boq, "Material" => array());
        foreach($head as $h){
          if($h == $m->id_item){
            $data[count($data)-1]['Material'][$h] = $m->qty;
          }else{
            $data[count($data)-1]['Material'][$h] = 0;
          }
        }
        $no=0;
      }
      $lastNama = $m->dispatch_regu_name;
    }
    //var_dump($data);
    return view("laporan.ajax-maintenance2", compact("data", "head"));
  }
  public function cekExcel(){
    $list = DB::select('
      SELECT m.*, m2.status, m2.team, (SELECT sum((qty*hmbjm)+(qty*hjbjm)) FROM maintaince_mtr mm left join item i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
      FROM maintaince m left join maintaince_progres m2 on m.id = m2.maintaince_id
      WHERE 1
    ');
    Excel::create('QE', function($excel) use($list) {
      $excel->sheet('TOTAL', function($sheet) use($list) {
          $sheet->loadView('maintaince.report.akses', compact('list'));
      });
      $excel->sheet('RAB', function($sheet) use($list) {
          $sheet->loadView('maintaince.report.akses', compact('list'));
      });
    })->export('xls');
  }
  public static function sendCreateTiket($id){
    $data = DB::table('maintaince')->where('id', $id)->first();
    if($data){
      $message = "<b>Insert Ticket by USER </b>\n";
      $message .= "No Ticket : #".$data->no_tiket."\n";
      $message .= "HeadLine  : ".$data->headline."\n";
      $message .= "Info      : ".$data->info."\n\n";
      $message .= "<i>Created By</i> \n";
      $message .= "Regu      : ".$data->dispatch_regu_name."\n";
      $message .= "Nik1      : ".$data->username1."\n";
      $message .= "Nik2      : ".$data->username2."\n";
      Telegram::sendMessage([
        'chat_id' => "-192742760",
        //'chat_id' => "52369916",
        'parse_mode' => 'html',
        'text' => $message
      ]);
    }
  }
  public static function sendProgressTiket($id){
    echo $id; 
    $data = DB::table('maintaince')->where('id', $id)->first();
    if($data){
      $message = "<b>Update Progress Ticket</b>\n";
      $message .= "No Ticket : #".$data->no_tiket."\n";
      $message .= "HeadLine  : ".$data->headline."\n";
      $message .= "Info      : ".$data->info."\n";
      $message .= "Status    : #".$data->status."\n";
      $message .= "Team      : ".$data->dispatch_regu_name."\n";
      $message .= "Material  : \n";
      $queryMtr = DB::table('maintaince_mtr')->where('maintaince_id', $id)->get();
      foreach($queryMtr as $mtr){
        $message .= " --- <i>".$mtr->id_item."</i> ( <i>".$mtr->qty."</i> )\n";
      }
      $chat_id = "-192742760";
      //$chat_id = "52369916";
      Telegram::sendMessage([
        'chat_id' => $chat_id,
        'parse_mode' => 'html',
        'text' => $message
      ]);
      $photoInputs = self::$photoInputst;
      for($i=0;$i<count($photoInputs);$i++) {
        $file = public_path()."/upload/maintenance/".$id."/".$photoInputs[$i].".jpg";
        if (file_exists($file)){
          Telegram::sendPhoto([
            'chat_id' => $chat_id,
            'caption' => $photoInputs[$i]." ".$data->no_tiket,
            'photo' => $file
          ]);
        }
      }
    }
  }
  public function listDummy(){
    $list = DB::table('maintenance_tiket_dummy')->orderBy('id', 'desc')->get();
    return view('maintaince.listdummy', compact('list'));
  }
  public function insertDummyForm($id){
    return view('maintaince.formdummy');
  }
  public function insertDummy(Request $req, $id){
    $auth = session('auth');
    DB::table('maintenance_tiket_dummy')->insert([
      'no_tiket' => $req->input('tiket'),
      'updated_by' => $auth->id_karyawan
    ]);
    return back();
  }
}
