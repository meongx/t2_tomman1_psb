<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class CcanController extends Controller
{
  public function listCcan()
  {
    $list = DB::select('
      SELECT
		m.*,
		d.*,
		case e.status_laporan
			when "1" then "UP"
			when "2" then "KEND. TEKNIS"
			when "3" then "KEND. PELANGGAN"
			when "4" then "HR"
			when "5" then "OGP"
			else "BELUM SURVEY" end as status_laporan_teknisi,
		id_regu AS id_r, (
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian
      FROM micc_wo m
      left join dispatch_teknisi d on m.ND=d.Ndem
	  left join psb_laporan e on d.id = e.id_tbl_mj
      WHERE loker="CCAN" order by d.id desc
    ');
    return view('migrasi.list-ccan', compact('list'));
  }

  public function dispatch($id){
    $data = DB::select('
      SELECT m.*,d.*,id_regu AS id_r, (
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian
      FROM micc_wo m left join dispatch_teknisi d on m.ND = d.Ndem
      WHERE m.ND = ?
    ',[
      $id
    ])[0];
    $regu = DB::select('
      SELECT id_regu as id,uraian as text, telp
      FROM regu
      WHERE job = "CCAN"
    ');

    return view('migrasi.dispatch', compact('data','regu'));
  }
  public function dispatchSave(Request $request, $id){
    $auth = session('auth');
    $exists = DB::select('select * from dispatch_teknisi where Ndem = ? and dispatch_by = 1',[
      $id
    ]);
    if(count($exists)){
      $data = $exists[0];
      DB::table('dispatch_teknisi')
          ->where('id', $data->id)
          ->update([
            'updated_at'   => DB::raw('NOW()'),
            'updated_by'   => $auth->id_karyawan,
            'tgl'          => $request->input('tgl'),
            'id_regu'      => $request->input('id_regu')
          ]);
    }else{
        DB::table('dispatch_teknisi')->insert([
          'updated_at' => DB::raw('NOW()'),
          'updated_by' => $auth->id_karyawan,
          'tgl'          => $request->input('tgl'),
          'id_regu'    => $request->input('id_regu'),
          'Ndem'       => $id,
          'dispatch_by'=> 1
        ]);
    }
    return redirect('/ccan')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> men-dispatch WO CCAN']
      ]);
  }
  public function reportCcan(){
    $auth = session('auth');
    $list = DB::select('
      SELECT ms2n.*, id_regu AS id_r, mdf.area,(
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian, Status_Indihome, status_laporan, ms2n_sebab_value, ms2n_sebab_pic, m.Kcontact as mKcontact,m.Nama as mNama,m.Alamat as mAlamat,m.ND as mND,m.ND_Speedy as mND_Speedy,
      case h.status_wo_by_hd
        when "1" then "UP"
        when "2" then "OGP"
        when "3" then "KENDALA"
        when "5" then "RESCHEDULE"
        else "BELUM SURVEY"
      end as status_wo_by_hd,
      dispatch_teknisi.id as id_dt,
      (select count(*) from psb_laporan_material plm where plm.psb_laporan_id = psb_laporan.id) as jumlah_material

      FROM micc_wo m
        LEFT JOIN dispatch_teknisi ON m.ND = dispatch_teknisi.Ndem
        LEFT JOIN psb_laporan on dispatch_teknisi.id=psb_laporan.id_tbl_mj
        LEFT JOIN mdf on m.MDF = mdf.mdf
        LEFT JOIN ms2n on m.ND = ms2n.ND
        LEFT join ms2n_sebab on ms2n.Sebab = ms2n_sebab.ms2n_sebab
        LEFT JOIN ms2n_status_hd h on m.ND = h.ndem
      WHERE dispatch_teknisi.updated_at like "'.date("Y-m-d").'%" and id_regu is not null order by dispatch_teknisi.id_regu
    ');



    $countWo = count($list);
    $countOuter = 0;
    $countInner = 0;
    $outer = array();

    $lastArea = '';
    foreach($list as $row) {
      if($row->area != 'BJM'){
        if ($lastArea == $row->area) {
          $outer[count($outer)-1]['WO'][] = array('ND_Speedy'=>$row->ND,'NDEM'=> $row->Ndem,'ID_DT'=> $row->id_dt,  'STATUS' => $row->status_wo_by_hd ,'ND' => $row->mND, 'detil'=>$row->mND.' ~ '.$row->mNama);
        }
        else {
          $outer[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->ND, 'NDEM'=> $row->Ndem, 'ID_DT'=> $row->id_dt, 'STATUS' => $row->status_wo_by_hd, 'ND'=>$row->mND, 'detil' => $row->mND.' ~ '.$row->mNama)));
          $lastArea = $row->area;
        }
        $countOuter++;
      }
    }
//     $lastArea = '';
    $tim = array();
    $lastTim = "";
    foreach($list as $row) {
      if ($row->jumlah_material>0 && $row->status_wo_by_hd=='UP') {
        $status_material = "MATERIAL OK";
      } else {
        $status_material = "";
      }
      if($row->area == 'BJM'){
        if ($lastTim == $row->uraian) {
          $tim[count($tim)-1]['WO'][] = array('ND_Speedy'=>$row->ND,'ID_DT'=> $row->id_dt, 'MATERIAL' => $status_material, 'STATUS' => $row->status_wo_by_hd ,'ND' => $row->mND, 'detil'=>$row->mND.' ~ '.$row->mNama);
          $countInner++;
        }
        else {
          $countInner++;
          $tim[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->ND,'ID_DT'=> $row->id_dt,'MATERIAL' => $status_material, 'STATUS' => $row->status_wo_by_hd, 'ND'=>$row->mND, 'detil' => $row->mND.' ~ '.$row->mNama)));
          $lastTim = $row->uraian;
        }
      }
    }
    usort($list, function($x, $y) {
      return strcasecmp($x->uraian , $y->uraian);
    });

    if($auth->id_user =='wandiy99'){
      //return $outer;
    }

    $kendala = array();
    $lastKendala = '';
    $bs = 0;
    $up = 0;
    $ogp = 0;
    $kdl = 0;
    foreach($list as $row) {
      if($row->ms2n_sebab_value == 'Kendala'){
        if ($lastKendala == $row->ms2n_sebab_pic) {
          $kendala[count($kendala)-1]['WO'][] = $row->ND.' : '.$row->Nama.' ('.$row->Sebab.')';
        }
        else {
          $kendala[] = array('head' => $row->ms2n_sebab_pic, 'WO' => array( $row->ND.' : '.$row->Nama.' ('.$row->Sebab.')'));
          $lastKendala = $row->ms2n_sebab_pic;
        }

      }
      if($row->status_wo_by_hd == 'KENDALA'){
        $kdl++;
      }if($row->status_wo_by_hd == 'BELUM SURVEY'){
        $bs++;
      }if($row->status_wo_by_hd == 'OGP'){
        $ogp++;
      }if($row->status_wo_by_hd == 'UP'){
        $up++;
      }
    }
    $counting = array('KENDALA' => $kdl, 'BELUM SURVEY' => $bs, 'OGP' => $ogp, 'UP' => $up, 'INNER' => $countInner, 'OUTER' => $countOuter, 'WO' => $countWo);
    //var_dump($outer);
    //return $kendala;
    return view('migrasi.report-ccan', compact('tim', 'outer', 'kendala', 'counting'));
  }
  public function updateStatus($id)
  {
    $data = DB::select('
      SELECT m.ND, Nama, h.status_wo_by_hd, h.id
      FROM micc_wo m
      LEFT JOIN ms2n_status_hd h on m.ND = h.ndem
      WHERE m.ND = ?
    ',[
      $id
    ])[0];
    return view('migrasi.status', compact('data'));
  }
  
  public function saveStatus(Request $request, $id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM ms2n_status_hd
      WHERE ndem = ?
    ',[
      $id
    ]);
    if (count($exists)) {
      $data = $exists[0];
        DB::table('ms2n_status_hd')
          ->where('id', $data->id)
          ->update([
            'modified_at'     => DB::raw('NOW()'),
            'modified_by'     => $auth->id_karyawan,
            'status_wo_by_hd' => $request->input('status')
          ]);
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> update status']
      ]);
    }
    else {
        DB::table('ms2n_status_hd')->insert([
          'created_at'      => DB::raw('NOW()'),
          'created_by'      => $auth->id_karyawan,
          'status_wo_by_hd' => $request->input('status'),
          'ndem'            => $id
        ]);
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> insert status']
      ]);
    }
    return $exists;
  }
  public function saveCcan(Request $request)
  {
    $wos = json_decode($request->input('wos'));
    $insertMigrasi = "insert ignore into micc_wo(ND,MDF,
            Nama,Alamat,jenis_layanan,layanan,service,sid,pic,loker) values";
    foreach($wos as $no => $wo){
      $sparator = ", ";
      if($no == 0){
        $sparator = "";
      }
      $insertMigrasi .= $sparator."('".($wo->ao)."','".($wo->mdf?:'-')."',".DB::connection()->getPdo()->quote($wo->nama).",
        ".DB::connection()->getPdo()->quote($wo->alamat).",'".($wo->jenis_layanan?:'-')."','".($wo->layanan?:'-')."','".($wo->service?:'-')."',
        '".($wo->sid?:'-')."',".DB::connection()->getPdo()->quote($wo->pic).",'CCAN')";
    }
    DB::statement($insertMigrasi);
    return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Upload Data']
    ]);
  }
  public function uploadCcan()
  {
    return view('migrasi.ccan');
  }
  public function inputOrder($id)
  {
    $data = DB::table('micc_wo')->where('ND', $id)->first();
    return view('migrasi.inputorder', compact('data'));
  }
  public function saveorder(Request $request)
  {
    $exists = DB::table('micc_wo')->where('id', $request->id)->first();
    if(count($exists)){
      DB::table('micc_wo')->where('id', $request->id)->update([
        "ND"            => $request->ao,
        "MDF"           => $request->sto,
        "Nama"          => $request->nama,
        "Alamat"        => $request->alamat,
        "jenis_layanan" => $request->jenis_layanan,
        "layanan"       => $request->layanan,
        "sid"           => $request->vlan,
        "ODP"           => $request->odp,
        "pic"           => $request->pic,
        "loker"         => "CCAN",
        "jenis_tiket"         => $request->jenis_tiket,
        ]);
    }else{
      DB::table('micc_wo')->insert([
        "ND"            => $request->ao,
        "MDF"           => $request->sto,
        "Nama"          => $request->nama,
        "Alamat"        => $request->alamat,
        "jenis_layanan" => $request->jenis_layanan,
        "layanan"       => $request->layanan,
        "sid"           => $request->vlan,
        "ODP"           => $request->odp,
        "pic"           => $request->pic,
        "loker"         => "CCAN",
        "jenis_tiket"         => $request->jenis_tiket,
        ]);
    }
    return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Submit']
    ]);
  }
}
