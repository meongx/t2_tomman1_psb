@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>
   MONITORING WORK ORDER MIGRASI <a href="/ms2n/sync/VA" class="btn btn-info">
      <span class="glyphicon glyphicon-refresh"> SyncNow</span>
    </a>
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <li class="{{ (Request::path() == 'migrasi') ? 'active' : '' }}"><a href="/migrasi">ALL MIGRASI</a></li>
    <li class="{{ (Request::segment(2) == 'report') ? 'active' : '' }}"><a href="/migrasi/report">REPORT</a></li>
    <li class="{{ (Request::segment(2) == 'upload') ? 'active' : '' }}"><a href="/migrasi/upload">UPLOAD</a></li>
  </ul>
  <div style="padding-bottom: 10px;">
    <strong>Mapping Order MICC</strong><br/>
    <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong>
  </div>
  <div class="panel panel-primary">
    <div class="panel-heading">INNER ({{ $counting['INNER'] }}) : </div>
    <div class="panel-body">
      <div class="list-group">
        @foreach($tim as $data)
          <div class="list-group-item">
            <strong>{{ $data['head'] }}</strong><br/>
            @foreach ($data['WO'] as $no => $wo)
							<span class="label label-info">{{ ++$no }}</span>
							<span><a href="/{{ $wo['ID_DT'] }}">{{ $wo['detil'] }}</a></span>
              <a class="report label label-default color_{{ $wo['STATUS'] }}"  href="/migrasi/status/{{ $wo['ND'] }}">{{ $wo['STATUS'] }}</a>
              <span class="label label-default">{{ $wo['MATERIAL'] }}</span>
              <br/>
            @endforeach
          </div>
        @endforeach
      </div>
    </div>
  </div>

  <div class="panel panel-primary">
    <div class="panel-heading">KENDALA : </div>
    <div class="panel-body">
      <div class="list-group">
        @foreach($kendala as $data)
          <div class="list-group-item">
            <strong>{{ $data['head'] }}</strong><br/>
            @foreach ($data['WO'] as $no => $wo)
              <span class="label label-info">{{ ++$no }}</span>
              {{ $wo }}<br />
            @endforeach
          </div>
        @endforeach
      </div>
    </div>
  </div>

  <div class="panel panel-primary">
    <div class="panel-heading">OUTER ({{ $counting['OUTER'] }}) : </div>
    <div class="panel-body">
      <div class="list-group">
        @foreach($outer as $data)
          <div class="list-group-item">
            @foreach ($data['WO'] as $no => $wo)
              <span class="label label-info">{{ ++$no }}</span>
              <a class="report"  href="/migrasi/status/{{ $wo['NDEM'] }}">{{ $wo['detil'] }}</a>
              <span class="label label-info color_{{ $wo['STATUS'] }}" >{{ $wo['STATUS'] }}</span>
              <span><a href="http://mydashboard.telkom.co.id/ms2/update_demand_useetv2.php?ndem={{ $wo['NDEM'] }}&amp;etat=VA" class="label label-warning">MS2N</a></span><br/>
           @endforeach
          </div>
        @endforeach
      </div>
    </div>
  </div>

  <div class="panel panel-primary">
    <div class="panel-heading">REKAP : </div>
    <div class="panel-body">
      <div class="list-group">
        <div class="list-group-item">
          <strong>WO MICC</strong> : <strong>{{ $counting['WO'] }}</strong><br/>
        </div>
        <div class="list-group-item">
          <strong>UP</strong> : <strong>{{ $counting['UP'] }}</strong><br/>
        </div>
        <div class="list-group-item">
          <strong>OGP</strong> : <strong>{{ $counting['OGP'] }}</strong><br/>
        </div>
        <div class="list-group-item">
          <strong>BELUM SURVEY</strong> : <strong>{{ $counting['BELUM SURVEY'] }}</strong><br/>
        </div>
        <div class="list-group-item">
          <strong>KENDALA</strong> : <strong>{{ $counting['KENDALA'] }}</strong><br/>
        </div>
      </div>
    </div>
    <div class="panel-body">  
    <script>
        var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
      </script>
              <script type="text/javascript">
                jQuery(document).ready(function($)
                {
                  var dataSource = [
                    {region: "UP", val: {{ $counting['UP'] }}},
                    {region: "OGP", val: {{ $counting['OGP'] }}},
                    {region: "BELUM SURVEY", val: {{ $counting['BELUM SURVEY'] }}},
                    {region: "KENDALA", val: {{ $counting['KENDALA'] }}},
                  ], timer;
                  
                  $("#bar-10").dxPieChart({
                    dataSource: dataSource,
                    title: "Mapping Progress WO",
                    tooltip: {
                      enabled: false,
                      customizeText: function() { 
                        return this.argumentText + "<br/>" + this.valueText;
                      }
                    },
                    size: {
                      height: 420
                    },
                    pointClick: function(point) {
                      point.showTooltip();
                      clearTimeout(timer);
                      timer = setTimeout(function() { point.hideTooltip(); }, 2000);
                      $("select option:contains(" + point.argument + ")").prop("selected", true);
                    },
                    legend: {
                      visible: false
                    },  
                    series: [{
                      type: "doughnut",
                      argumentField: "region"
                    }],
                    palette: xenonPalette
                  });
                  
                });
              </script>
              <div id="bar-10" style="height: 450px; width: 100%;"></div>
            </div>
          </div>
  </div>
@endsection
