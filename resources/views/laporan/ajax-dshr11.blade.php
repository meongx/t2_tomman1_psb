      <div class="panel panel-primary">
        <div class="panel-heading">Laporan Inputan DSHR HI {{ date('d M Y') }}</div>
        <div class="panel-body">
          <div class="list-group">
            <div class="list-group-item">
              <span class="label label-info">PSB</span><br/>
              <span>Sales : Total HI / VA / UNSC</span><br/>
              @foreach ($witel as $w)
                <span>{{ $w->m or 0 }} : {{ $w->totalpsb or 0 }} / {{ $w->va or 0 }} / {{ $w->unsc_psb or 0 }}</span><br/>
              @endforeach
            </div>
            <div class="list-group-item">
              <span class="label label-info">MICC</span><br/>
              <span>Sales : Total HI / TIKET CREATED / UNSC</span><br/>
              @foreach ($witel as $w)
                <span>{{ $w->m or 0 }} : {{ $w->totalmicc or 0 }} / {{ $w->tc or 0 }} / {{ $w->unsc_micc or 0 }}</span><br/>
              @endforeach
            </div>
          </div>
        </div>
      </div>