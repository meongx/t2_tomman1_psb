@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id_item }}" />
    @endif
    <h3>
      <a href="/item" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Register Item
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-id">Id</label>
      <input name="id_item" type="text" id="input-id" class="form-control" value="<?= $data->id_item ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-nama">Nama Item</label>
      <input name="nama_item" type="text" id="input-nama" class="form-control" value="<?= $data->nama_item ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-unit">Unit</label>
      <input name="unit_item" type="text" id="input-unit" class="form-control" value="<?= $data->unit_item ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-status">Status</label>
      <input name="status" type="hidden" id="input-status" class="form-control" value="<?= $data->status ?>" />
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>

  <script>
    $(function() {
      var data1 = [{"id":"0","text":"Active"},{"id":"1","text":"Non-Active"}]
      var status = function() {
        return {
          data: data1,
          placeholder: 'Input status'
        }
      }
      $('#input-status').select2(status());
    })
  </script>
@endsection
