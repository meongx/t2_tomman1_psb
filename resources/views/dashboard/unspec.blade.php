@extends('layout')

@section('content')
  @include('partial.alerts')
  <center>
  	<h3>Dashboard Underspec</h3>
  </center>
  <div class="row">
  	<div class="col-sm-5">
  		<div class="panel panel-default">
  			<div class="panel-heading">
  				Table
  			</div>
  			<div class="panel-body">
  				<table class="table table-stripped">
  					<tr>
  						<th>Rank</th>
  						<th>STO</th>
  						<th>JUMLAH</th>
  					</tr>
  					<?php
  						$total = 0;
  					?>
  					@foreach ($query as $rank => $result)
  					<?php
  						$total += $result->Jumlah;
  					?>
  					<tr>
  						<td>{{ ++$rank }}</td>
  						<td>{{ $result->Workzone }}</td>
  						<td><a href="/dashboard/unspeclist/{{ $result->Workzone }}">{{ $result->Jumlah }}</a></td>
  					</tr>
  					@endforeach
  					<tr>
  						<th colspan="2">Total</th>
  						<th><a href="/dashboard/unspeclist/ALL">{{ $total }}</a></th>
  					</tr>
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection