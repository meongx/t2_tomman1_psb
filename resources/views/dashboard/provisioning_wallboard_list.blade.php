@extends('public_layout')

@section('content')
  @include('partial.alerts') 
  <a href="/provisioning" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a>
  <br />
  <br />
  <div class="table-responsive">
    <table class="table" border="1">
      <tr>
        <td>NO.</td>
        <td>SC</td>
        <td>NAMA</td>
        <td>ORDER DATE</td>
        <td>KCONTACT</td>
        <td>ORDER</td>
        <td>STO</td>
      </tr>
      @foreach ($query as $num => $datax)
      <tr>
        <td>{{ ++$num }}</td>
        <td>SC.{{ $datax->ORDER_ID }}</td>
        <td>{{ $datax->CUSTOMER_NAME }}</td>
        <td>{{ $datax->ORDER_DATE }}</td>
        <td>{{ $datax->KCONTACT }}</td>
        <td>{{ $datax->JENISPSB }}</td>
        <td>{{ $datax->XS2 }}</td>
      </tr>
      @endforeach
    </table>
  </div>
@endsection