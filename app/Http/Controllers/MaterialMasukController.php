<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class MaterialMasukController extends Controller
{
  
  public function index()
  {
    $auth = session('auth');

    $list = DB::select('
      SELECT *
      FROM gudang_masuk
    ');

    return view('materialMasuk.list', compact('list'));
  }
  
  public function input($id)
  {
    $exists = DB::select('
      SELECT *
      FROM gudang_masuk
      WHERE id = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];

      // TODO: order by most used
      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item,
          COALESCE(m.qty, 0) AS qty
        FROM item i
        LEFT JOIN
          gudang_masuk_material m
          ON i.id_item = m.id_item
          AND m.gudang_masuk_id = ?
        WHERE i.kat = 9
        ORDER BY id_item
      ', [
        $data->id
      ]);
    }
    $gudangs = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang where 1
    ');
    return view('materialMasuk.input', compact('data', 'materials', 'gudangs'));
  }
  
  public function create()
  {
    $data = new \StdClass;
    $data->id = null;
    $data->gudang_id = null;
    $data->tgl = null;
    // TODO: order by most used
    $materials = DB::select('
      SELECT id_item, nama_item, 0 AS qty
      FROM item
      WHERE kat = 9
      ORDER BY id_item
    ');
    $gudangs = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang where 1
    ');
    return view('materialMasuk.input', compact('data', 'materials', 'gudangs'));
  }
  
  public function save(Request $request, $id)
  {
    $auth = session('auth');
    $materials = json_decode($request->input('materials'));
    $gudang_id = $request->input('gudang');
    
    $exists = DB::select('
      SELECT *
      FROM gudang_masuk
      WHERE id = ?
    ',[
      $id
    ]);
    
    if (count($exists)) {
      $data = $exists[0];
      DB::transaction(function() use($request, $data, $auth, $materials,$gudang_id,$id) {
        $this->decrementStokGudang($id);
        DB::table('gudang_masuk')
          ->where('id', $data->id)
          ->update([
            'ts'   => DB::raw('NOW()'),
            'updater'   => $auth->id_karyawan,
            'tgl'       => $request->input('tanggal'),
            'gudang_id' => $gudang_id
          ]);

        DB::table('gudang_masuk_material')
          ->where('gudang_masuk_id', $data->id)
          ->delete();
        foreach($materials as $material) {
          DB::table('gudang_masuk_material')->insert([
            'gudang_masuk_id' => $data->id,
            'id_item' => $material->id_item,
            'qty' => $material->qty
          ]);
        }
        $this->incrementStokGudang($data->gudang_id, $materials);

      });
      return redirect('/material-masuk')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan material Masuk']
      ]);
    }
    else {
      DB::transaction(function() use($request, $id, $auth, $materials) {
        $insertId = DB::table('gudang_masuk')->insertGetId([
          'ts'        => DB::raw('NOW()'),
          'updater'   => $auth->id_karyawan,
          'gudang_id' => $request->input('gudang'),
          'tgl'       => $request->input('tanggal')
        ]);
        foreach($materials as $material) {
          DB::table('gudang_masuk_material')->insert([
            'gudang_masuk_id' => $insertId,
            'id_item' => $material->id_item,
            'qty' => $material->qty
          ]);
        }
        $this->incrementStokGudang($request->input('gudang'), $materials);
        //$this->incrementStokTeknisi($pj, $materials);
      });
      return redirect('/material-masuk')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan Material Masuk']
      ]);
    }
    
  }
  public function destroy($id)
  {
    //$this->decrementStokTeknisi($id);
    DB::transaction(function() use($id) {
      $this->decrementStokGudang($id);
      DB::table('gudang_masuk')
          ->where('id', [$id])->delete();
      DB::table('gudang_masuk_material')
          ->where('gudang_masuk_id', [$id])->delete();
    });
    return redirect('/material-masuk')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Data']
    ]);
  }

  private function incrementStokGudang($id, $materials){
    foreach($materials as $material) {
      $exists = DB::select('
        SELECT *
        FROM stok_material_gudang
        WHERE id_item = ? and id_gudang = ?
      ',[
        $material->id_item, $id
      ]);
      if(count($exists)){
        $data = $exists[0];
        DB::table('stok_material_gudang')
          ->where('id', $data->id)
          ->increment('stok', $material->qty);
      }else{
        DB::table('stok_material_gudang')->insert([
          'id_item'   => $material->id_item,
          'id_gudang' => $id,
          'stok'      => $material->qty
        ]);
      }
    }
  }

  private function decrementStokGudang($id){
    $gudang = DB::select('
        SELECT *
        FROM gudang_masuk
        WHERE id = ?
    ',[
      $id
    ]);
    $data=$gudang[0];
    $materials = DB::select('
        SELECT *
        FROM gudang_masuk_material
        WHERE gudang_masuk_id = ?
    ',[
      $id
    ]);
    foreach($materials as $material) {
      DB::table('stok_material_gudang')
      ->where('id_item', $material->id_item)
      ->where('id_gudang', $data->gudang_id)
      ->decrement('stok', $material->qty);
    }
  }
}
