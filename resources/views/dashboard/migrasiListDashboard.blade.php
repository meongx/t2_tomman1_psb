@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
  </style>
  <a href="/dashboard/migrasi/{{ $tgl }}/ALL" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List {{ $title }} {{ $jenis }} {{ $so }} {{ $tgl }} </h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th rowspan="2">No.</th>
          <th rowspan="2">SM</th>
          <th rowspan="2">TL</th>
          <th rowspan="2">TEKNISI</th>
          <th colspan="4">WO / TIKET</th>
          <th colspan="3">NOMOR LAYANAN</th>
          <th colspan="4">DATA PELANGGAN</th>
          <th colspan="3">TRANSAKSI / MUTASI</th>
          <th colspan="3">DATA PERANGKAT TERPASANG</th>
          <th colspan="7">MATERIAL PENUNJANG</th>
          
          @if ($status=='INSERT TIANG' || $status=='ALL')
              <th colspan="8">LAIN-LAIN</th>
          @else
              <th colspan="9">LAIN-LAIN</th>
          @endif

        </tr>
        <tr>

          <th>NOMOR SC</th>
          <th>MYIR</th>
          <TH>TGL Dispatch</TH>
          <th>STO</th>
          <th>TELEPON</th>
          <th>INET / DATIN</th>
          <th>NCLI</th>
          <th>NAMA</th>
          <th>ALAMAT</th>
          <th>JENIS MUTASI</th>
          <th>LAYANAN</th>
          <th>NAMA MODEM</th>
          <th>TYPE / SERIES</th>
          <th>KET. ORDER</th>
          <th>S/N or MAC ADDRESS</th>
          <th>MERK</th>
          <th>MODE GELARAN</th>
          <th>DC</th>
          <th>PATCHCORE</th>
          <th>UTP CAT6</th>
          <th>TIANG</th>
          <th>PULL STRAP</th>
          <th>CONDUIT</th>
          <th>TRAY CABLE</th>
          <th>ODP</th>
          <th>KORD. PEL </th>
          <th>AREA</th>
          <th>NAMA MITRA</th>
          <th>STATUS SC</th>

          <th>STATUS TEK.</th>

          @if ($status=='INSERT TIANG' || $status=='ALL')
              <th>TAMBAH TIANG</th>
          @endif

          <th>CATATAN TEK 1</th>
          <th>CATATAN TEK 2</th>
          <th>CATATAN TEK 3</th>
        </tr>

        @foreach ($data as $num => $result)
        <?php
          if ($result->dc_preconn>0 || $result->dc_roll>0){
            $mode_gelaran = "KU";
          } else {
            $mode_gelaran = "UTP";
          }
        ?>
        <tr>
          <td>{{ ++$num }}</td>
          <td>MUHAMMAD NOR RIFANI</td>
          @if ($result->mode==1 && $mode_gelaran == "UTP")
          <td># {{ $result->TL }}</td>
          @else
          <td>{{ $result->TL }}</td>
          @endif
          <td>{{ $result->uraian }}</td>
          <td><a href="/{{ $result->id_dt }}">{{ $result->orderId }}</a></td>
  
          @php
            $myir = '-';
            if (substr($result->jenisPsb,0,2)=='AO'){
                    $dataMyir = $result->kcontact;
                    $pisah    = explode(';',$dataMyir);
                    $myir = $pisah[1];
            };
          @endphp

          <td>{{ $myir }}</td>
          <td>{{ $result->tanggal_dispatch }}</td>
          <td>{{ $result->sto }}</td> 
          <td>{{ $result->ndemPots }}</td>
          <td>{{ $result->ndemSpeedy }}</td>
          <td>{{ $result->orderNcli }}</td>
          <td>{{ $result->orderName }}</td>
          <td>{{ $result->orderAddr }} {{ $result->kordinat_pelanggan }}</td>

          <td>{{ $result->jenisPsb }}</td>
          <td>{{ $result->jenis_layanan }}</td>
          <td>~</td>  
          <td>{{ $result->typeont }} ~ {{ $result->typestb }}</td>
          <td>{{ $result->ketOrder}}</td>
          <td>{{ $result->snont }} ~ {{ $result->snstb }}</td>
          <td>~</td>
          <td>
            {{ $mode_gelaran }}
          </td>

          <td>{{ $result->dc_preconn ? : $result->dc_roll ? : "~" }}</td>
          <td>{{ $result->patchcore }}</td>
          <td>{{ $result->utp }}</td>
          <td>{{ $result->tiang }}</td>
          <td>{{ $result->pullstrap }}</td>
          <td>{{ $result->conduit }}</td>
          <td>{{ $result->tray_cable }}</td>
          <td>{{ strtoupper($result->nama_odp) }}</td>
          <td>{{ $result->kordinat_pelanggan }}</td>
          <td>{{ $result->area_migrasi }}</td>
          <td>{{ $result->mitra }}</td>
          <td>{{ $result->orderStatus }}</td>
          <td>{{ $result->laporan_status }}</td>
        
          @if ($status=='INSERT TIANG' || $status=='ALL')
                <td>{{ $result->ketTiang }}</td>
          @endif
          
          @php 
            if ($result->catatan==NULL){
               echo "<td></td>";
               echo "<td></td>";
               echo "<td></td>";
            }
            else{
                $cat = explode('#',$result->catatan);
                $jml = count($cat);

                $jmlCat = $jml;
                $awal = 0;
                if ($jml >= 3){
                    $jmlCat = 3;
                }

                if (!empty($jmlCat)){
                    for ($a=$awal;$a<=($jmlCat-1);$a++){
                        echo "<td>".$cat[$a]."</td>";
                    }

                    $bbb = 3 - $jmlCat;
                    for ($iii=0;$iii<=$bbb-1;$iii++){
                        echo "<td></td>";       
                    }
                }
            }
          @endphp
        </tr>
        @endforeach
      </table>
    </div>
    </div>

  </div>    <br />
      <br />
@endsection     
