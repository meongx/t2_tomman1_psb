<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class EmployeeModel
{
	public static function getMitraAmija(){
		return DB::table('mitra_amija')->get();
	}
	public static function getAll(){
		return DB::SELECT('SELECT *, nik as id, nama as text FROM 1_2_employee WHERE Witel_New = "'.session('witel').'"');
	}
	public static function getById($id){
		$data =  DB::SELECT('
			SELECT
				a.*,
				b.mitra_amija_pt
			FROM
				1_2_employee a
			LEFT JOIN mitra_amija b ON a.mitra_amija = b.mitra_amija
			WHERE
				a.id_people = "'.$id.'"
		');

		if(!empty($data)){
			return DB::SELECT('
						SELECT
							a.*,
							b.mitra_amija_pt
						FROM
							1_2_employee a
						LEFT JOIN mitra_amija b ON a.mitra_amija = b.mitra_amija
						WHERE
							a.id_people = "'.$id.'"
					')[0];
		}
		else{
			return DB::SELECT('
					SELECT
						a.*,
						b.mitra_amija_pt
					FROM
						1_2_employee a
					LEFT JOIN mitra_amija b ON a.mitra_amija = b.mitra_amija
					WHERE
						a.id_people = "'.$id.'"
				');
		}
		// return DB::table('1_2_employee')->where('id_people', $id)->first();
	}
	public static function save($req){
		return DB::table('1_2_employee')->insert([
			'nik' => $req->input('nik')
		]);
	}

	public static function getSQUAD(){
		return DB::table('employee_squad')->get();
	}

	public static function update($data,$nikUpdate){
		return DB::table('1_2_employee')
								->where('id_people', $data->input('id_people'))
								->update([
									'nik' => $data->input('nik'),
									'nama' => $data->input('nama'),
									'sub_directorat' => "Consumer Service",
									'sub_sub_unit' => "MULTISKILL",
									'position' => $data->input('position'),
									'atasan_1' => $data->input('atasan_1'),
									'status' => "PKS",
									'status_konfirmasi' => "Valid",
									'CAT_JOB' => $data->input('cat_job'),
									'laborcode' => $data->input('laborcode'),
									'ACTIVE' => 1,
									'Witel_New' => $data->input('witel'),
									'mitra_amija' => $data->input('mitra_amija'),
									'nik_amija' => $data->input('nik_amija'),
									'squad' => $data->input('squad'),
									'updateBy'	=> $nikUpdate,
									'dateUpdate'	=> date('Y-m-d H:i:s')
								]);
	}
	public static function store($data,$nikUpdate){
		return DB::table('1_2_employee')
								->insert([
									'nik' => $data->input('nik'),
									'nama' => $data->input('nama'),
									'sub_directorat' => "Consumer Service",
									'sub_sub_unit' => "MULTISKILL",
									'position' => $data->input('position'),
									'atasan_1' => $data->input('atasan_1'),
									'status' => "PKS",
									'status_konfirmasi' => "Valid",
									'CAT_JOB' => $data->input('cat_job'),
									'laborcode' => $data->input('laborcode'),
									'ACTIVE' => 1,
									'Witel_New' => $data->input('witel'),
									'mitra_amija' => $data->input('mitra_amija'),
									'nik_amija' => $data->input('nik_amija'),
									'squad' => $data->input('squad'),
									'updateBy'	=> $nikUpdate,
									'dateUpdate'	=> date('Y-m-d H:i:s')
								]);
	}
	public static function distinctPSA(){
		return DB::table('1_3_witel')->select('City as id', 'City as text')->get();
	}

	public static function distinctCATJOB(){
		return DB::table('1_2_employee')->select('CAT_JOB as id', 'CAT_JOB as text')->groupBy('CAT_JOB')->orderBy('CAT_JOB')->get();
	}
	public static function distinctSubDirectorat(){
		return DB::table('1_2_employee')->select('sub_directorat as id', 'sub_directorat as text')->groupBy('sub_directorat')->orderBy('sub_directorat')->get();
	}
	public static function distinctSubSubUnit(){
		return DB::table('1_2_employee')->select('sub_sub_unit as id', 'sub_sub_unit as text')->groupBy('sub_sub_unit')->orderBy('sub_sub_unit')->get();
	}
	public static function distinctPosition(){
		return DB::table('1_2_employee')->select('position as id', 'position as text')->groupBy('position')->orderBy('position')->get();
	}


}
