<?php

use Illuminate\Foundation\Inspiring;
use App\DA\Tele;
use App\Http\Controllers\DispatchController;
use App\Http\Controllers\MaintainceController;
use App\Http\Controllers\AssuranceController;
use App\Http\Controllers\GrabController;
use App\Http\Controllers\TicketingController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SaldoController;
use App\Http\Controllers\ApiController;


/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/
Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');
Artisan::command('grabAlistaKembali', function () {
	GrabController::grabAlistaKembalian();
});
Artisan::command('sendReport {id}', function ($id) {
	Tele::sendReportTelegram($id);
});
Artisan::command('sendReportDSHR {id}', function ($id) {
	Tele::send_to_telegram_dshr($id);
});

Artisan::command('sendMt {id}', function ($id) {
	Tele::sendOrderMaintenance($id);
});
Artisan::command('kpro {id} {jenis}', function ($id, $jenis) {
	DispatchController::update_kpro($id, $jenis);
});
Artisan::command('akpro {id}', function ($id) {
	DispatchController::assign_kpro($id);
});
Artisan::command('sendCreateTiket {id}', function ($id) {
	MaintainceController::sendCreateTiket($id);
});
Artisan::command('sendProgressTiket {id}', function ($id) {
	MaintainceController::sendProgressTiket($id);
});
Artisan::command('sendReportUndispacth', function(){
	Tele::sendReportUndispacth();
});
Artisan::command('sendReportUndispacthLagi', function(){
	Tele::sendReportUndispacthLagi();
});
Artisan::command('sendSMSPagi', function(){
	Tele::sendSms();
});
Artisan::command('sendTelegramSSC1 {id} {nik}', function($id, $nik){
	Tele::sendTelegramSSC1($id, $nik);
});
Artisan::command('matrikSector', function(){
	Tele::matrikSector();
});
Artisan::command('sendReportUndispacthAo', function(){
	Tele::sendReportUndispacthAo();
});
Artisan::command('grabAlistaDate {start}', function($start){
	GrabController::grabAlistaDate($start);
});
Artisan::command('grabAlistaKeluar {tgl}', function($tgl){
	GrabController::grabAlistaVersi2($tgl);
});
Artisan::command('sendTelegramSSC1SyncSc {id} {nik}', function($id, $nik){
	Tele::sendTelegramSSC1SyncSc($id, $nik);
});
Artisan::command('reportAbsenProv', function(){
	Tele::reportAbsenProv();
});
Artisan::command('sendReportMaterial {id}', function ($id) {
	Tele::sendReportTelegramMaterial($id);
});
Artisan::command('grabAlistaStokNasionalBanjarmasinArea', function () {
	GrabController::grabAlistaStokNasionalBanjarmasinArea();
});
Artisan::command('sendReportKp {id}', function ($id) {
	Tele::sendReportTelegramKp($id);
});
Artisan::command('starclickmin', function () {
	GrabController::starclickMin();
});
Artisan::command('grabstarclickncx {witel}', function ($witel) {
	GrabController::grabstarclickncx($witel);
});

Artisan::command('idosier', function () {
	GrabController::idosier();
});
Artisan::command('baDigital {tgl}', function ($tgl) {
	GrabController::baDigital($tgl);
});
Artisan::command('byRfc {rfc}', function ($rfc) {
	GrabController::grabAlistaByRfc($rfc);
});
Artisan::command('sendReportReboundary {id}', function ($id) {
	Tele::sendReportTelegramReboundary($id);
});
Artisan::command('senDWoReporOrderMaintenance {bulan}', function ($bulan) {
	AssuranceController::senDWoReporOrderMaintenance($bulan);
});
Artisan::command('ticketingUndispatch', function () {
	TicketingController::undispatch();
});
Artisan::command('matrikTimTidakHadir', function () {
	DispatchController::matrikTidakHadir();
});
Artisan::command('senDWoReportKirimTeknisi {bulan}', function ($bulan) {
	AssuranceController::senDWoReportKirimTeknisi($bulan);
});
Artisan::command('getPsStatusHr {bulan}', function($bulan){
	DashboardController::getPsStatusHr($bulan);
});
Artisan::command('showManjaBot', function(){
	AssuranceController::showManjaBot();
});
Artisan::command('grabNossaByReportDate {tgl}', function($tgl){
	GrabController::grabNossaByDb($tgl);
});
Artisan::command('starclickminKalbar', function () {
	GrabController::starclickMinKalbar();
});
Artisan::command('starclickminBalikpapan', function () {
	GrabController::starclickMinBalikpapan();
});
Artisan::command('starclickminKaltara', function () {
	GrabController::starclickMinKaltara();
});
Artisan::command('starclickminSamarinda', function () {
	GrabController::starclickMinSamarinda();
});
Artisan::command('starclickminKalteng', function () {
	GrabController::starclickMinKalteng();
});
Artisan::command('plasaNotSc {tgl}', function($tgl) {
	Tele::plasaNotSc($tgl);
});

//marina
// Artisan::command('sendReport {id}', function ($id) {
// 	OrderController::sendtotelegram($id);
// });
// Artisan::command('sendDispatch {id}', function ($id) {
// 	OrderController::senddispatchtotelegram($id);
// });
// Artisan::command('pleaseUpdate', function () {
// 	OrderController::pleaseUpdate();
// });
// Artisan::command('pleaseVerif', function () {
// 	OrderController::pleaseVerif();
// });
// Artisan::command('getQuestionPortal', function () {
// 	GraberController::getQuestionPortal();
// });
// Artisan::command('alistaStocknocssSend', function () {
// 	ToolsController::alistaStocknocssSend();
// });
Artisan::command('grabAlistaByRfc {rfc}', function ($rfc) {
	SaldoController::grabAlistaByRfc($rfc);
});

Artisan::command('check_sc_not_sync', function(){
  $tgl = date('Y-m');
  GrabController::check_sc_not_sync($tgl);
});

Artisan::command('batch_send_to_dalapa', function(){
  $tgl = date('Y-m-d');
  ApiController::batch_send_to_dalapa($tgl);
});
