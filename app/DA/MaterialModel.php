<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class MaterialModel{
  public static function list_TL($periode,$nik){
    $tglPisah = explode('-', $periode);
    $tgl      = $tglPisah[1].'/'.$tglPisah[0];

    if ($nik == "88159353" || $nik == "850056" || $nik == "94150248"){
        $where = '';
    }
    else if ($nik == "MITRA_UPATEK"){
        $where = 'b.mitra in ("UPATEK","UPATEKDELTA") AND';
    }
    else if ($nik == "MITRA_PTAGB"){
        $where = 'b.mitra = "AGB DELTA" AND';
    }
    else if ($nik == "MITRA_SPM"){
        $where = 'b.mitra = "SPM" AND';
    }
    else if ($nik == "MITRA_CUI"){
        $where = 'b.mitra in ("CUI","CUI DELTA") AND';
    }
    else
    {
        $where = 'c.TL_NIK = "'.$nik.'" AND'; 
    };
  
    return DB::SELECT('
      SELECT
        *
      FROM
        rfc_sal a
      LEFT JOIN regu b ON (a.nik = b.nik1 OR a.nik = b.nik2)
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN 1_2_employee d ON a.nik = d.nik
        WHERE
      '.$where.'
      a.jmlTerpakai < a.jumlah AND
      b.ACTIVE = "1" AND
      c.chat_id <> "-254857170" AND
      a.rfc LIKE "%'.$tgl.'%"
    ');
  }

  public static function listMaterial($periode,$sektor){
    $tglPisah = explode('-', $periode);
    $tgl      = $tglPisah[1].'/'.$tglPisah[0];

    $where_sektor = '';
    if ($sektor<>''){
        $where_sektor = 'c.chat_id="'.$sektor.'" AND ';
    };

    $sql = '
      SELECT
        *
      FROM
        rfc_sal a
      LEFT JOIN regu b ON (a.nik = b.nik1 OR a.nik = b.nik2)
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN 1_2_employee d ON a.nik = d.nik
        WHERE
      '.$where_sektor.'
      a.jmlTerpakai < a.jumlah AND
      b.ACTIVE = "1" AND
      a.rfc LIKE "%'.$tgl.'%"
    ';
    
    $data = DB::select($sql);
    return $data;
  }
}
