<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class ReturnDropcoreModel {

  public static function list_by_TL($periode){
    return DB::SELECT('
      SELECT
        f.nik,
        f.nama,
        count(*) as jumlah,
        SUM(CASE WHEN d.returnDropcore_panjang > 0 THEN 1 ELSE 0 END) as returnDropcore
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN returnDropcore d ON a.Ndem = d.Incident
      LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
      LEFT JOIN 1_2_employee f ON c.TL_NIK = f.nik
        WHERE
          a.tgl LIKE "'.$periode.'%" AND
          e.action = 1 AND
          f.nik <> ""
      GROUP BY
        c.TL_NIK
    ');
  }

  public static function get_tiket($in){
    return DB::SELECT('
      SELECT
      *,
      a.id as id_dt
      FROM
        dispatch_teknisi a
      LEFT JOIN data_nossa_1_log b ON a.Ndem = b.Incident
      LEFT JOIN returnDropcore c ON a.Ndem = c.Incident
      WHERE
        a.Ndem = "'.$in.'"
    ');
  }
  public static function list_TL($periode,$TL_NIK){
    $query = DB::SELECT('
      SELECT
      *
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_action e ON d.action = e.laporan_action_id
      LEFT JOIN data_nossa_1_log f ON a.Ndem = f.Incident
      LEFT JOIN psb_laporan_penyebab g ON d.penyebabId = g.idPenyebab
      LEFT JOIN returnDropcore h ON a.Ndem = h.Incident
      WHERE
        (c.TL_NIK = "'.$TL_NIK.'" OR c.Nik_Atl = "'.$TL_NIK.'" OR c.Nik_Atl2 = "'.$TL_NIK.'") AND
        d.status_laporan = 1 AND
        e.statusqc = 1 AND
        a.TGL LIKE "'.$periode.'%"
    ');
    return $query;
  }
  public static function save($request){
    $in = $request->Input('Incident');
    $auth = session('auth');
    $cek_in = DB::table('returnDropcore')->where('Incident',$in)->get();
    if (count($cek_in)>0){
      $result = DB::table('returnDropcore')->where('Incident',$in)->update([
        'returnDropcore_panjang' => $request->Input('panjang'),
        'date_created' => date('Y-m-d H:i:s'),
        'user_created' => $auth->id_user
      ]);
    } else {
      $result = DB::table('returnDropcore')->insert([
        'Incident' => $in,
        'returnDropcore_panjang' => $request->Input('panjang'),
        'date_created' => date('Y-m-d H:i:s'),
        'user_created' => $auth->id_user
      ]);
    }
    return $result;
  }
}

?>
