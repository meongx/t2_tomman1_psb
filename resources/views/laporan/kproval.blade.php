@extends('layout')
@section('content')
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading"> Jam 8</div>

            <div class="panel-body table-responsive">
                <table class="table table-bordered">
                    <th>STO</th>
                    {{-- <th>Datel</th> --}}
                    <th>Prov.Issued (Val)</th>

                    @foreach($dataVal as $val)
                        <tr>
                            <td>{{ $val->sto }}</td>
                            {{-- <td>{{ $val->datel }}</td> --}}
                            <td><a href="/kproval/{{ date('Y-m-d') }}/{{ $jam }}/{{ $val->sto }}"> {{ $val->jumlah }} </a></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td >Total </td>
                        <td>{{ $jmlData[0]->total }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading"> Jam {{ $jam }} </div>

            <div class="panel-body table-responsive">
                <table class="table table-bordered">
                    <th>STO</th>
                    {{-- <th>Datel</th> --}}
                    <th>Prov.Issued (Val)</th>
                    <th>Fallout</th>

                    @foreach($dataTot as $tot)
                        <tr>
                            <td>{{ $tot->sto }}</td>
                            {{-- <td>{{ $tot->datel }}</td> --}}
                            <td><a href="/kproval/{{ date('Y-m-d') }}/issued/{{ $tot->sto }}/{{ $jam }}"> {{ $tot->issued }} </a></td>
                            <td><a href="/kproval/{{ date('Y-m-d') }}/fall/{{ $tot->sto }}/{{ $jam }}"> {{ $tot->fallout }} </a></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td>Total </td>
                        <td>{{ $jml[0]->issued }}</td>
                        <td>{{ $jml[0]->fallout }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

@endsection