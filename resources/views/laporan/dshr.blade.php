@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .strong{
      font-weight: bold;
      
    }​
    .stronger{
      font-weight: bold;
      font-size: 9px;
    }​
  </style>
  <h3>
    REPORT HARIAN DSHR
  </h3>
  <ul class="nav nav-tabs ajax">
    <li class="active"><a href="#" id="1">1</a></li>
    <li><a href="#dua" id="2">2</a></li>
    <li><a href="#tiga" id="3">3</a></li>
    <li><a href="#empat" id="4">4</a></li>
    <li><a href="#lima" id="5">5</a></li>
    <li><a href="#enam" id="6">6</a></li>
    <li><a href="#tujuh" id="7">7</a></li>
    <li><a href="#delapan" id="8">8</a></li>
    <li><a href="#sembilan" id="9">9</a></li>
    <li><a href="#sepuluh" id="10">10</a></li>
    <li><a href="#witel" id="11">witel</a></li>
    <li><a href="#visual" id="12">visual</a></li>
  </ul>
  <br/>
  <div id="txtHint"></div>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />
  <script>
  $(document).ready(function(){
    $(".loading").css({"visibility":"visible"});
    $.get("/laporan/dshr-report1", function(text) {
      $("#txtHint").html(text);
    }).done(function(){
      $(".loading").css({"visibility":"hidden"});
    });
    $(".ajax a").on("click", function(){
      $(".loading").css({"visibility":"visible"});
      $(".nav").find(".active").removeClass("active");
      $(this).parent().addClass("active");
      $.get("/laporan/dshr-report"+$(this).attr('id'), function(text) {
        $("#txtHint").html(text);
      }).done(function(){
        $(".loading").css({"visibility":"hidden"});
      });
    });
  });
  </script>
@endsection
