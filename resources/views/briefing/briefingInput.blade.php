@extends('layout')

@section('content')
  <script src="/js/dropzone.js"></script>
  <link rel="stylesheet" href="/css/dropzone.min.css">
  @include('partial.alerts')
  <div class="row">
    <div class="col-md-12" id="bio_tl">
      <div class="panel panel-default">
          <div class="panel-heading">
          Profile
          </div>
          <div class="panel-body table-responsive">
            <table style="padding:10px;">
              <tr>
                <td style="padding:15px">
                  <img src="/image/people.png" width="100" />
                </td>
                <td style="padding:10px">

                  {{ session('auth')->nama }}<br />
                  @foreach ($group_telegram as $gt)
                  <span class="label label-info">{{ $gt->title }}</span>
                  @endforeach
                  <br />
                  <small>
                    Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                  </small>
                </td>
                <td style="padding : 10px" valign=top>

                </td>
              </tr>

            </table>
            @include ('partial.home')
            <br>
            
            <div class="panel panel-default">
                <div class="panel-heading">Input Foto</div>
                <div class="panel-body">

                    <form method="post" action="/uploadFoto/{{ $id }}/upload" enctype="multipart/form-data" class="dropzone" id="dropzone">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    </form>   

                </div>
            </div>

            <form method="post">
                <div class="form-group">
                    <label>Send To Grub Sektor</label> 
                    <input name="sektor" type="hidden" id="sektor" class="form-control" />
                     {!! $errors->first('sektor', '<span class="label label-danger">:message</span>') !!}
                </div>

                <div class="form-group">
                    <label>Catatan TL</label> 
                    <textarea class="form-control" name="catatan" id="catatan" rows="5"></textarea>
                    {!! $errors->first('catatan', '<span class="label label-danger">:message</span>') !!}
                </div>

                <input type="hidden" name="idLaporan" id="idLaporan" value="{{ $id }}">
                <input type="submit" value="Simpan Laporan" class="btn btn-primary">
            </form>

          </div>
        </div>
      </div>
    </div>

 <script type="text/javascript">
    $(function() {
      var data_sektor= <?= json_encode($sektor) ?>;
      var sektor = function() {
        return {
          data: data_sektor,
          placeholder: 'Pilih Sektor',
          formatResult: function(data) {
          return  data.text;
          }
        }
      }
      $('#sektor').select2(sektor());
    }); 


    Dropzone.options.dropzone =
     {
        maxFilesize: 12,
        renameFile: function(file) {
            var dt = new Date();
            var time = dt.getTime();
           return time+file.name;
        },
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        addRemoveLinks: true,
        timeout: 50000,
        removedfile: function(file) 
        {
            var name = file.upload.filename;
            var id   = document.getElementById('idLaporan').value;

            console.log(id);
            $.ajax({
                headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                type: 'POST',
                url: '/uploadFoto/'+id+'/removed',
                data: {filename: name},
                success: function (data){
                    console.log("File has been successfully removed!!");
                },
                error: function(e) {
                    console.log(e);
                }});
                var fileRef;
                return (fileRef = file.previewElement) != null ? 
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
   
        success: function(file, response) 
        {
            console.log(response);
        },
        error: function(file, response)
        {
           return false;
        }
    };
</script>
@endsection
