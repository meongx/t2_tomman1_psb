@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="row">
    <div class="col-sm-12">
      <a href="/alpro" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      <br />
      <br />
      <div class="panel panel-default">
        <div class="panel-heading">
         ALPRO TO SEKTOR
        </div>
        <div class="panel-body">
          <form method="post">
            <input type="hidden" name="nik" value="{{ $nik }}" />
            <input name="param" type="text" id="param" class="form-control" value="" /><br />
            <input type="submit" name="submit" class="btn btn-success form-control" value="Cari">
          </form>
        </div>

      </div>

      @if (count($get_alproname)>0)
      <div class="panel panel-default">
        <div class="panel-heading">
          Result
        </div>
        <div class="panel-body">
          <form method="post" action="/alpro/book">
            <input type="hidden" name="nik" value="{{ $nik }}">
            <input type="submit" name="submit" class="btn btn-success form-control" value="Booking" />
            <table class="table">
              <tr>
                <th>#</th>
                <th>ODP NAME</th>
                <th>Status</th> 
                <th><a class="checkall">SELECT ALL</a> / <a class="uncheckall">CLEAR</a></th>
              </tr>
              @foreach ($get_alproname as $num => $alproname)
              <tr>
                <td>{{ ++$num }}</td>
                <td>{{ $alproname->text }}</td>
                <td>
                  {{ $alproname->status ? : 'AVAILABLE' }}
                  @if ($alproname->status<>"")
                  <br />
                  by {{ $alproname->nik }}
                  @endif
                </td>
                <td>
                  @if ($alproname->status=="")
                  <input type="checkbox" name="book[]" class="checkbox1" value="{{ $alproname->text }}" />
                  @endif
                </td>
              </tr>
              @endforeach
            </table>
          </form>
        </div>
      </div>
      @endif
    </div>
  </div>
  <script>
  $(function() {
      $('.checkall').click(function() {
          $('.checkbox1').attr('checked',true);
      });

      $('.uncheckall').click(function() {
          $('.checkbox1').attr('checked',false);
      });

  });
  </script>
@endsection
