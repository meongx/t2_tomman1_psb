@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>Ubah Progress Mitra </h3><hr>

  <form method="post" autocomplete enctype="multipart/form-data">
      <div class="row">
        <div class="form-group">
            <div class="col-md-12">
                Cluster
                <input type="text" name="nmCluster" readonly class="form-control" value="{{ $dataProgress->nama_cluster }}">
            </div>

            <div class="col-md-12">
                Regu
                <input type="text" name="regu" readonly class="form-control" value="{{ $dataProgress->uraian }}">
            </div>

            <div class="col-md-4 {{ $errors->has('progress') ? 'has-error' : '' }}">
                Progress (%)
                <input type="text" name="progress" class="form-control" value="{{ $dataProgress->progress }}"> 
                {!! $errors->first('progress','<p class=help-block>:message</p>') !!}
            </div>

        <br><br><br><br><br><br><br><br>
        </div>

        <div class="col-md-12">
            <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
            <a href="/migrasi/monitoring/progress" class="btn btn-default btn-sm">Batal</a>
        </div>
      </div>
  </form>
@endsection