@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>
   UPLOAD WORK ORDER CCAN
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <li class="{{ (Request::path() == 'ccan') ? 'active' : '' }}"><a href="/ccan">CCAN</a></li>
    <li class="{{ (Request::segment(2) == 'report-ccan') ? 'active' : '' }}"><a href="/migrasi/report-ccan">REPORT</a></li>
    <li class="{{ (Request::segment(2) == 'upload-ccan') ? 'active' : '' }}"><a href="/migrasi/upload-ccan">UPLOAD</a></li>
  </ul>
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    <input type="hidden" name="wos" value="[]" />
    <div class="form-group">
      <label class="control-label" for="tes">Import Excel</label>
      <input type="file" id="tes" class="form-control"/>
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
    <div id="jumlah"></div>
    <div class="list-group"></div>

  </form>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />

  <script src="/bower_components/excel/zip/WebContent/zip.js"></script>
  <script src="/bower_components/excel/async-master/dist/async.js"></script>
  <script src="/bower_components/excel/underscore.js"></script>
  <script src="/bower_components/excel/xlsxParser.js"></script>
  <script>
    $(function() {
      var migrasi = [];
      $("#tes").change(function(e){
        migrasi = [];
        $(".loading").css({"visibility":"visible"});
        $(".removable").remove();
        var file = document.getElementById('tes').files[0];
        zip.workerScriptsPath = "/bower_components/excel/zip/WebContent/";
        xlsxParser.parse(file).then(function(data) {
          var count = 0;
          for(var i=1;i<data.length;i++){
            var e_sto             = data[i][1],
                e_ao              = data[i][2],
                e_nama            = data[i][3],
                e_alamat          = data[i][4],
                e_jenis_layanan   = data[i][5],
                e_odp             = data[i][6],
                e_pic             = data[i][7],
                e_vlan            = data[i][8],
                e_layanan         = data[i][9];
            if(e_ao){
              migrasi.push({
                mdf: e_sto,
                ao : e_ao,
                layanan: e_layanan,
                nama : e_nama,
                alamat : e_alamat,
                jenis_layanan : e_jenis_layanan,
                service : e_odp,
                pic : e_pic,
                sid : e_vlan
              });
              $(".list-group").append("<div class='list-group-item removable'><span>" + e_ao + "<span> <span>" + e_layanan + "<span> <br /><span>" + e_jenis_layanan + "<span><br/><span>" + e_nama + "<span> <span>" + e_alamat + "<span> <span>" + e_odp + "<span><br/><span>" + e_pic + "<span><br/><span>" + e_vlan + "<span><span>" + e_vlan + "<span><span>" + e_sto + "<span><br/></span>");
              count++;
            }
          }
          $("#jumlah").append("<span class=removable>Ticked Found : " + count + "</span>");
          $(".loading").css({"visibility":"hidden"});
        }, function(err) {
          alert('error', err);
        });
      })
      $('#submit-form').submit(function() {
        $('input[name=wos]').val(JSON.stringify(migrasi));
      });
    })
  </script>
@endsection
