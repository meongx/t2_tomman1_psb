@extends('layout')

@section('content')
  @include('partial.alerts')
  <script src="/bower_components/devexpress-web-14.1/js/dx.chartjs.js"></script>
  <link rel="stylesheet" href="/bower_components/devexpress-web-14.1/css/dx.dark.css" />
  <center>
    <h3>Dashboard GAUL</h3>
  </center>
  <br />
<div class="row">
  <div class="col-sm-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        Dashboard GAUL by Sequence {{ $periode }}
      </div>
      <div class="panel-body">
        <table class="table">
          @foreach ($data1 as $num=>$result)
          <tr>
            <td>{{ str_replace("Â","",$result->SEQ) }}</td>
            <td><a href="/gaulv2list/{{ str_replace("Â","",$result->SEQ) }}/{{ $periode }}">{{ $result->jumlah }}</a></td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <div class="panel panel-default">
      <div class="panel-heading">
        Grafik
      </div>
      <div class="panel-body">
        <script type="text/javascript">
          jQuery(document).ready(function($)
          {
            $("#graph").dxChart({
              dataSource: [
                @foreach ($data as $result)
                {reporting : "{{ $result->SEQ }}", val: {{ $result->jumlah }}},
                @endforeach
              ],
              AutoLayout : false,
              title: "Dashboard GAUL by Sequence {{ $periode }} ",
              tooltip: {
                  enabled: true,
                customizeText: function() {
                  return this.argumentText + "<br/>" + this.valueText;
                }
              },
              size: {
                height: 650
              },
              legend: {
                visible: true
              },

              rotated : true,
              series: [{
                label: {
                    visible: true,
                    customizeText: function (segment) {
                        return segment.percentText;
                    },
                    connector: {
                        visible: true
                    }
                },
                name : "SEQ",
                type: "bar",
                color : "#74b9ff",
                argumentField: "reporting"
              }]
            });
            });
            </script>
        <div id="graph"></div>
      </div>
    </div>
  </div>
</div>

@endsection
