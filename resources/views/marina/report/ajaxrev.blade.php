@foreach($data as $no => $d)
    <tr class="ajax">
        <td colspan="2">{{ $d->uraian }}</td>
        <td align="right"><a href="/marina/ajaxdetilrev/{{ $d->id }}/1/{{ $tgl }}" target="_BLANK">{{ number_format($d->benjar) }}</a></td>
        <td align="right"><a href="/marina/ajaxdetilrev/{{ $d->id }}/2/{{ $tgl }}" target="_BLANK">{{ number_format($d->gamas) }}</a></td>
        <td align="right"><a href="/marina/ajaxdetilrev/{{ $d->id }}/3/{{ $tgl }}" target="_BLANK">{{ number_format($d->odp_loss) }}</a></td>
        <td align="right"><a href="/marina/ajaxdetilrev/{{ $d->id }}/4/{{ $tgl }}" target="_BLANK">{{ number_format($d->remo) }}</a></td>
        <td align="right"><a href="/marina/ajaxdetilrev/{{ $d->id }}/5/{{ $tgl }}" target="_BLANK">{{ number_format($d->utilitas) }}</a></td>
        <td align="right"><a href="/marina/ajaxdetilrev/{{ $d->id }}/6/{{ $tgl }}" target="_BLANK">{{ number_format($d->insert_tiang) }}</a></td>
        <td align="right"><a href="/marina/ajaxdetilrev/{{ $d->id }}/10/{{ $tgl }}" target="_BLANK">{{ number_format($d->val_odp) }}</a></td>
        <td align="right"><a href="/marina/ajaxdetilrev/{{ $d->id }}/9/{{ $tgl }}" target="_BLANK">{{ number_format($d->odp_full) }}</a></td>
        <td align="right">{!! $d->tiket_odp_sehat?'('.$d->tiket_odp_sehat.' Tiket) ':'' !!}<a href="/marina/ajaxdetilrev/{{ $d->id }}/8/{{ $tgl }}" target="_BLANK">{{ number_format($d->odp_sehat) }}</a></td>
        <td align="right">{{ number_format($d->total) }}</td>
    </tr>
@endforeach