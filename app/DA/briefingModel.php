<?php

namespace App\DA;

use Illuminate\Database\Eloquent\Model;
use DB;

class briefingModel extends Model
{
    public static function simpanData($nik){
        date_default_timezone_set("Asia/Makassar");
    	return DB::table('briefing')->insertGetId([
    		'createdBy'	=> $nik
    	]);
    }

    public static function getDataById($id){
    	return DB::table('briefing')->where('id',$id)->first();
    }

    public static function getDataByTglNik($tgl,$nik){
    	return DB::table('briefing')->whereDate('createdAt', $tgl)->where('createdBy',$nik)->first();
    }

    public static function updateData($req, $id){
        date_default_timezone_set("Asia/Makassar");
    	return DB::table('briefing')->where('id',$id)->update([
    		'catatan'	=> $req->catatan,
    		'sektor'	=> $req->sektor,
    		'status'	=> 1
    	]);
    }

    public static function getSektor(){
    	return DB::SELECT('
	      SELECT
	      a.chat_id as id,
	      a.title as text
	      FROM
	        group_telegram a
	      where title like "%TERRITORY%" OR title like "RING%" OR title like "FIBERZONE%"
	    ');
    }

    public static function getDataTl($nik){
    	return DB::table('group_telegram')->where('TL_NIK',$nik)->orwhere('Nik_Atl',$nik)->orwhere('Nik_Atl2',$nik)->first();
    }

    public static function simpanFoto($filename, $id){
    	return DB::table('briefing_foto')->insert([
    		'idFoto'	=> $id,
    		'foto'		=> $filename
    	]);
    }

    public static function hapusFotoByFilename($filename, $id){
    	return DB::table('briefing_foto')->where('idFoto',$id)->where('foto',$filename)->delete();
    }

    public static function hapusSemuaFoto($id){
    	return DB::table('briefing_foto')->where('idFoto',$id)->delete();
    }

    public static function getFoto($id){
    	return DB::table('briefing_foto')->where('idFoto',$id)->get();
    }

    public static function getAllBriefingTgl($tgl, $nik){
    	return DB::table('briefing')->where('createdAt', 'LIKE', '%'.$tgl.'%')->where('createdBy',$nik)->orderBy('createdAt', 'DESC')->get();
    }

    public static function getFotoByNik($nik){
    	return DB::table('briefing')
    		->leftJoin('briefing_foto','briefing.id','=','briefing_foto.idFoto')
    		->select('briefing_foto.*')
    		->where('briefing.createdBy',$nik)
    		->get();
    }

}
