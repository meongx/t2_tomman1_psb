@extends('layout')

@section('content')
  @include('partial.alerts')
  @include('workorderlist')

  <!-- dialog !-->
  <div>
    <form class="form-group" method="post" id="submit-form" >
      <div class="form-group {{ $errors->has('Search') ? 'has-error' : '' }}">
          <label for="Search">Search</label>
          <input type="text" name="Search" class="form-control" id="Search" value="">
          {!! $errors->first('Search', '<p class=help-block>:message</p>') !!}
          <button class="btn form-control btn-primary">Search</button>
      </div>
    </form>
  </div>


  @if ($data <> NULL)
  <br />
  <div >
  <div class="form-control">
    {{ $data }}. <br />
    Result ({{ $jumlah }}) Found.
    <br />
    <br />
    @if ($jumlah > 0)
    <div class="table-responsive">
    <table class="table table-striped table-bordered dataTable">
      <thead>
        <tr>
          <th>#</th>
  		    <th width="75" colspan=2>Work Order</th>
        </tr>
      </thead>
      <tbody>
        @foreach($list as $no => $data)
        <tr>
          <td rowspan=3>{{ ++$no }}.</td>
  		@if(session('auth')->level <> 19)
          <td colspan=2><span>
            <form id="delete-form" method="post" autocomplete="off" action="/dispatch/{{ $id }}">
  		@if(!empty($data->id_r))
          <span class="label label-warning">{{ $data->uraian }}</span>
                @if(session('auth')->level == 2 || session('auth')->level == 15 || session('auth')->level == 46 )
                  <a href="/dispatch/{{ $data->orderId }}" class="label label-info" data-toggle="tooltip" title="{{ $data->uraian }}">Re-Dispatch</a>
      				  @endif
                <span class="label label-warning">{{ $data->hd }}</span>
                <span class="label label-warning">{{ $data->updated_at }}</span>
              @else
                <a href="/dispatch/{{ $data->orderId }}" class="label label-info">Dispatch</a>
              @endif
  		    </span>

          <span><a href="http://mydashboard.telkom.co.id/ms2/update_demand_useetv2.php?ndem={{ $data->Ndem }}&etat=VA" class="label label-info">Update MS2N</a></span>
          <span><a href="/sendsms/{{ $data->orderId }}" class="label label-warning">Send SMS</a></span>
        
          @if(session('auth')->level == 2 || session('auth')->level == 15 || session('auth')->level == 46 )
            @if($data->status_laporan=="1" || $data->status_laporan=="37" || $data->status_laporan=="38")
                <span><label class="label label-success">{{ $data->laporan_status }}</label></span>
                <a href="/sendkpro/manual/{{ $data->id_dt }}" class="label label-primary"> Send To KPRO</a>
            @endif
          @endif
          
          @if ($data->dispatch_by==4)
              <span><label class="label label-success">Fulfillment Guarante</label></span>    
          @endif

         {{--  @if(session('auth')->level == 2 || session('auth')->level == 30)
              <a href="/dispatch/manja/{{$data->orderId }}" class="label label-info" data-toggle="tooltip" title="{{ $data->manja_status }}" >M a n j a</a>
          @endif --}}


          @if (session('auth')->id_user=="94132168" || session('auth')->level==2)
            @if($data->uraian<>'')
              <input name="_method" type="hidden" value="DELETE">
              <a href="#" class="label label-danger" id="hapus-wo" data-toggle="tooltip" >H a p u s</a>
            @endif
          @endif

          </form>
        </td>
  		@endif
  		</tr>
  		<tr>
          <td>

  	        {{ $data->orderStatus }}
            @if ($data->flagging<>'')
            WO Cluster {{ $data->flagging }}
            @endif
            <br />

  			@if ($data->id_dt<>'')
  			<a href="/{{ $data->id_dt }}">SC{{ $data->orderId ? : 'Tidak Ada SC' }}</a>
  			@else
  			SC{{ $data->orderId ? : 'Tidak Ada SC' }}
  			@endif

  			/ {{ $data->orderDate ? : 'Tidak Ada SC' }}<br />
  	        {{ $data->ND ? : $data->ndemPots }}~{{ $data->ND_Speedy ? : $data->ndemSpeedy }}<br />
  	        {{ $data->Nama  ? : $data->orderName }}<br />
            {{ $data->ndemPots ?: '-' }}<br />
  	        {{ $data->sto }} /
  	        {{ $data->jenisPsb }} <br />
            NCLI : {{ $data->orderNcli ? : '' }} <br />

            {{ $data->Deskripsi }} <br />
            {{ $data->alproname ? : 'Tidak Ada Informasi ODP' }} <br />
        @if ($data->LOCN_X <> '')
        ({{ substr($data->LOCN_X,0,2) }}.{{ substr($data->LOCN_X,2,10) }}, {{ substr($data->LOCN_Y,0,3) }}.{{ substr($data->LOCN_Y,3,10) }}) / {{ $data->OLT_IP }} / {{ $data->OLT }} <br />
        Lokasi ODP : http://maps.google.com/?q={{ substr($data->LOCN_X,0,2) }}.{{ substr($data->LOCN_X,2,10) }},{{ substr($data->LOCN_Y,0,3) }}.{{ substr($data->LOCN_Y,3,10) }} <br />
        @endif
  	        {{ $data->Kcontact ? : $data->kcontact }}<br />
  	        {{ $data->Jalan ? : '' }} {{ $data->No_Jalan ? : '' }} {{ $data->Distrik ? : '' }}<br />
  	        {{ $data->Kota ? : '' }}
            {{ $data->orderAddr ? : '' }} <br />

             <!-- catatan kordinat pelanggan dan alamat sales -->
             Koordinat Pelanggan : <b>{{ $data->kordinatPel ? : ' - ' }}</b><br>
             Alamat Sales        : <b>{{ $data->alamatSales ? : ' - ' }}</b><br><br>

            

             {{-- catatan unsc sdi --}}
             @if (!empty($dataUnsc))
                CATATAN UNSC SDI<br />
                {{ $dataUnsc->catatan ?: $dataUnsc->catatan ?: '-'}}
             @endif



          </td>
       <td>
            @if ($data->nik_sales <> '')
            {{ $data->nik_sales }}<br />
            Korlap : {{ $data->korlap }}<br />
            {{ $data->nama_pelanggan }}<br />
            {{ $data->pic }}<br />
            {{ $data->alamat }}<br />
            {{ $data->nama_odp }}<br />
            Kordinat ODP : {{ $data->koor_odp }}<br />
            Kordinat Pelanggan : {{ $data->koor_pel }}<br />
            {{ $data->keterangan }}<br />
            <a href="/upload/dshr/{{ $data->id_dshr }}/Foto_Rumah_Pelanggan.jpg" ><img src="/upload/dshr/{{ $data->id_dshr }}/Foto_Rumah_Pelanggan-th.jpg" /></a>
            <a href="/upload/dshr/{{ $data->id_dshr }}/Foto_ODP.jpg" ><img src="/upload/dshr/{{ $data->id_dshr }}/Foto_ODP-th.jpg" /></a>
          @endif
        </td>
  	     </tr>
  			<tr>
          <td>
  	        <span>Sebab : <br />{{ $data->Sebab }}</span><br/>
  	        <span>{{ $data->Ket_Sebab }}</span><br />
  	        <span>Solusi : <br />{{ $data->Ket_Solusi }}</span><br />
  		</td>
  		<td valign="top">
  	        Catatan Teknisi : <br />
  	        {{ $data->catatan ? : 'Tidak ada' }}
  		<br />{{ $data->Due_Date_Solusi }}
  		</td>

        </tr>
        @endforeach
      </tbody>
    </table>
    </div>
    @endif

  </div>
  </div>

  @endif

</div>

<script>
  $(document).ready(function(){
    $('#submit-form').submit(function() {

    });
    $('[data-toggle="tooltip"]').tooltip();

    $('#hapus-wo').click(function(e){
        e.preventDefault();
        var sure = confirm('Yakin Data Akan Dihapus ?');
        if (sure){
            $('#delete-form').submit();
        }
    })
  });
</script>
@endsection
