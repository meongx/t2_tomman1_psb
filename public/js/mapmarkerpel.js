(function() {
    var $mapModal = $('#mapModalPel'),
        $koorInput = $('#kordinat_pelanggan'),
        $idDisp = $('#dispatch_id')
    var ghostEl = document.createElement('div');
    ghostEl.style.position = 'absolute';
    ghostEl.style.top = '-500px';
    document.body.appendChild(ghostEl);
    var infoWindow = new google.maps.InfoWindow({ maxWidth: 600 });
    function showInfoMarker(marker){
        var bbox = ghostEl.getBoundingClientRect(),
            width = Math.ceil(bbox.right - bbox.left),
            height = Math.ceil(bbox.bottom - bbox.top);
            console.log(marker.data);
        var content = '<div style="width:100px;height:90px;"><span class="label label-success">'+marker.data.odp_name+'</span><br/><br/><span class="label label-info">Kap:'+marker.data.is_total+'</span><br/><br/><span class="label label-info">Terpakai:'+marker.data.is_service+'</span><br/><br/><span class="label label-info">Sisa:'+marker.data.is_avai+'</span></div>';

        if (!marker.getMap()) {
            marker.setMap(map);
            infoWindow.previousMarker = marker;
        }

        infoWindow.setContent(content);
        infoWindow.open(map,marker);
    }
    var map = null, marker = null,odp = [];
    function setOdpOnAll(map) {
      for (var i = 0; i < odp.length; i++) {
        odp[i].setMap(map);
      }
    }
    var showMarker = function(latLng) {
        // if (!marker) {
        //     marker = new google.maps.Marker({
        //         position: latLng,
        //         map: map,
        //         draggable: true,
        //         animation: google.maps.Animation.DROP
        //     });
        //     posisiSekrang = navigator.geolocation.getCurrentPosition;
        //     console.log(posisiSekrang);
        // }
        // else {
        //     marker.setVisible(true);
        //     marker.setPosition(latLng);
        // }


        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                map.setCenter(pos);
            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }



        
    };

    var initMap = function(lat, lon) {
        var center = {
                lat: lat || -3.332081,
                lng: lon || 114.67323
            },
            withMarker = (!isNaN(lat) && !isNaN(lon)) && (lat != 0 && lon != 0),
            zoom = withMarker ? 18 : 12;

        if (!map) {
            map = new google.maps.Map(
                document.getElementById('mapViewPel'),
                {
                    center: center,
                    zoom: zoom,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            );
            google.maps.event.addListener(map, 'click', function(event) {
                setOdpOnAll(null);
                
                
                $.getJSON("/getOdpByPoint/"+event.latLng.lat()+"/"+event.latLng.lng(), function(data){
                $.each(data, function (index, item) {

                        var point = {
                            lat:  Number(item.latitude),
                            lng:  Number(item.longitude)
                        }
                        var marker = new google.maps.Marker({
                            position: point,
                            map: map,
                            icon : "https://starclick.telkom.co.id/noss_prod/resources/images/icon-gphone-blue.png"
                        });
                        marker.data = item;
                        google.maps.event.addListener(marker, 'click', function() {
                            showMarker(point);
                            showInfoMarker(marker);
                            $namaOdp.val(item.odp_name);
                        });
                        odp.push(marker);
                    })
                    console.log(data);
                });
                showMarker(event.latLng);
            });
        }
        else {
            map.setZoom(zoom);
            map.panTo(center);
        }

        if (withMarker) {
            showMarker(center);
        }
        else if (marker)
            marker.setVisible(false);

        $.getJSON("/getOdp/"+$idDisp.val(), function(data){
            $.each(data, function (index, item) {
                var point = {
                    lat:  Number(item.latitude),
                    lng:  Number(item.longitude)
                }
                var marker = new google.maps.Marker({
                    position: point,
                    map: map,
                    icon : "https://starclick.telkom.co.id/noss_prod/resources/images/icon-gphone-blue.png"
                });
                marker.data = item;
                google.maps.event.addListener(marker, 'click', function() {
                    showMarker(point);
                    showInfoMarker(marker);
                    $namaOdp.val(item.odp_name);
                });
                odp.push(marker);
            })
            console.log(data);
        });
    };

    $mapModal.on('shown.bs.modal', function() {
        var koor = $koorInput.val();
        var myArr = koor.split(",");
        initMap(
            Number(myArr[0]),
            Number(myArr[1])
        );
    });

    $('#btnLoadMapPel').click(function() {
        $mapModal.modal();
    });
    $('#btnGetMarkerPel').click(function() {
        var latLng = marker ? marker.getPosition() : new google.maps.LatLng(0,0);
        $koorInput.val(latLng.lat()+", "+latLng.lng());

        $mapModal.modal('hide');
    });
})();