@extends('layout')
@section('content')
	<div class="panel panel-primary">
		<div class="panel-heading">Input Tiket</div>
		<div class="panel-body">
			<form>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="tiket">No Tiket</label>
							<input type="text" name="tiket" class="form-control">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label for="keluhan">Keluhan</label>
							<input type="hidden" name="keluhan" class="form-control" id="keluhan">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label for="catatan">Catatan</label>
							<input type="text" name="catatan" class="form-control" id="catatan">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label for="noInet">No. Inet</label>
							<input type="text" name="noInet" class="form-control" id="noInet">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label for="odp">ODP</label>
							<input type="text" name="odp" class="form-control" id="odp">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label for="sn">SN</label>
							<input type="text" name="sn" class="form-control" id="sn">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<input type="submit" name="tiket" class="btn btn-primary" value="Simpan">
						</div>
					</div>

				</div>
			</form>
		</div>
	</div>
@endsection

@section('plugins')
	<script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
	<script>
		$(function(){
     		$("#odp").inputmask("AAA-AAA-A{2,3}/999");
     		
     		var data = <?= json_encode($getKeluhan); ?>;
      		$('#keluhan').select2({
      			data: data,
      			placeholder: 'Input Keluhan',
      			formatSelection: function(data){
      				return data.text;
      			},
      			formatResult: function(data){
      				return data.text;
      			}
      		});

      		
		})
	</script>
@endsection
