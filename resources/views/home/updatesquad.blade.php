@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>UPDATE SQUAD {{ $nik }}</h3>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading"></div>
  			<div class="panel-body">
  				<form method="post">
	  				<table>
	  					<tr>
	  						<td>NIK</td>
	  						<td>:</td>
	  						<td><input type="text" name="nik" value="{{ $nik }}" /></td>
	  					</tr>
	  					<tr>
	  						<td>NAMA</td>
	  						<td>:</td>
	  						<td><input type="text" readonly="true" value="{{ $query[0]->nama }}" /></td>
	  					</tr>
	  					<tr>
	  						<td>SQUAD</td>
	  						<td>:</td>
	  						<td>
	  							<select name="squad">
	  								<option value=""> -- </option>
	  								@foreach ($query_squad as $squad)
	  								<?php 
	  									if ($query[0]->squad_id == $squad->squad_id) {
	  										$selected = 'selected="selected"';
	  									} else {
	  										$selected = '';
	  									}
	  								?>
	  								<option value="{{ $squad->squad_id }}" {{ $selected }}>{{ $squad->squad }}</option>
	  								@endforeach
	  							</select>
	  						</td>
	  					</tr>
	  					<tr>
	  						<td>
	  							<input type="submit" name="submit" value="Submit">
	  						</td>
	  					</tr>
	  				</table>
  				</form>
  			</div>
  		</div>
  	</div>
  </div>
@endsection