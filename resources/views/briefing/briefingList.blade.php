@extends('layout')

@section('content')
  <script src="/js/dropzone.js"></script>
  <link rel="stylesheet" href="/css/dropzone.min.css">
  @include('partial.alerts')
  <div class="row">
    <div class="col-md-12" id="bio_tl">
      <div class="panel panel-default">
          <div class="panel-heading">
          Profile
          </div>
          <div class="panel-body table-responsive">
            <table style="padding:10px;">
              <tr>
                <td style="padding:15px">
                  <img src="/image/people.png" width="100" />
                </td>
                <td style="padding:10px">

                  {{ session('auth')->nama }}<br />
                  @foreach ($group_telegram as $gt)
                  <span class="label label-info">{{ $gt->title }}</span>
                  @endforeach
                  <br />
                  <small>
                    Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                  </small>
                </td>
                <td style="padding : 10px" valign=top>

                </td>
              </tr>

            </table>
            @include ('partial.home')
            <br>
            <div class="row">
                <div class="col-md-12">
                    @if(count($dataNow)==0 || $dataNow->status==0)
                      <a href="/briefing" class="btn btn-primary">Inputkan Laporan</a>
                    @else
                      <input type="button" class="btn btn-success" value="Sudah Laporan Hari ini">
                    @endif
                </div>
            </div><br>

            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Catatan</th>
                        <th>Foto</th>
                    </tr>

                    @foreach($listData as $no=>$data)
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $data->createdAt }}</td>
                            <td>{{ $data->catatan }}</td>
                            <td>
                                @foreach($getFoto as $foto)
                                    @if($data->id==$foto->idFoto)
                                      <a href="/upload/briefing/{{ $data->id }}/{{ $foto->foto }}"><img src="/upload/briefing/{{ $data->id }}/{{ $foto->foto }}" width="80px" height="120px"></a>
                                    @endif
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
          
          </div>
        </div>
      </div>
    </div>
@endsection
