@extends('layout')

@section('content')
  @include('partial.alerts')
  
  <div class="row">
      <div class="col-md-12">
          <h3>
      <div class="col-sm-6">    
        <a href="/sales/proses/input" class="btn btn-info">
          <span class="glyphicon glyphicon-plus"></span>
        </a>

      </div>
      
      <div class="input-group col-sm-6">
          <input type="text" class="form-control inputSearch" name="q" value="{{ date('Y-m-d') }}" />
          <span class="input-group-btn">
              <button class="btn btn-default search">
                  <span class="glyphicon glyphicon-search"></span>
              </button>
          </span>
      </div>  
  </h3>      
      </div>
    
  </div>  

  <ul class="nav nav-tabs" style="margin-bottom:20px;">
    <li class="{{ (Request::segment(2) == 'listhi') ? 'active' : '' }}"><a href="/sales/listhi">P.HI <span class="label label-primary"> {{ $jmlHi }}</span></a></li>
    <li class="{{ (Request::segment(2) == 'listkp') ? 'active' : '' }}"><a href="/sales/listkp">KP <span class="label label-primary"> {{ $jmlKp }}</span></a></li>
    <li class="{{ (Request::segment(2) == 'listkt') ? 'active' : '' }}"><a href="/sales/listkt">KT <span class="label label-primary"> {{ $jmlKt }}</span></a></li>
    <li class="{{ (Request::segment(2) == 'listup') ? 'active' : '' }}"><a href="/sales/listup">UP <span class="label label-primary"> {{ $jmlUp }}</span></a></li>
    <li class="{{ (Request::segment(2) == 'list') ? 'active' : '' }}"><a href="/sales/list">LIST WO <span class="label label-success"> {{ $jmlList }}</span></a></li>
  </ul>

  <div class="panel panel-primary">
      <div class="panel-heading">List WO</div>
      <div class="panel-body">
          <table class="table table-bordered">
              @if($ket=='list')
                @foreach ($data as $dt)
                  <tr>
                      <td>
                        <b>Tgl Registrasi : </b> {{ $dt->orderDate }} <br> 
                        <b>TRACKER ID : </b> {{ $dt->myir }}<br>
                        <b>Pelanggan : </b> {{ $dt->customer }} <br>
                        <b>Alamat : </b> {{ $dt->alamatLengkap }} <br>
                        <b>Koor. Pel : </b> {{ $dt->koorPelanggan }} <br>
                        <b>Pic Pelanggan : </b> {{ $dt->picPelanggan }} <br><br>

                        <b>Channel : </b> {{ $dt->channel }} <br>
                        <b>Jenis Layanana : </b> {{ $dt->layanan }} <br>
                        <b>Tansakai PSB : </b> {{ $dt->psb }} <br>
                      </td>
                  </tr>
                @endforeach
              @elseif($ket=='hi' || $ket=='kp' || $ket=='kt')
                @foreach ($data as $dt)
                  <tr>
                      <td>
                        <b>Tgl Registrasi : </b> {{ $dt->orderDate }} <br> 
                        <b>TRACKER ID : </b> {{ $dt->myir }}<br>
                        <b>Pelanggan : </b> {{ $dt->customer }} <br>
                        <b>Alamat : </b> {{ $dt->alamatLengkap }} <br>
                        <b>Koor. Pel : </b> {{ $dt->koorPelanggan }} <br>
                        <b>Pic Pelanggan : </b> {{ $dt->picPelanggan }} <br><br>

                        <b>Channel : </b> {{ $dt->channel }} <br>
                        <b>Jenis Layanana : </b> {{ $dt->layanan }} <br>
                        <b>Tansakai PSB : </b> {{ $dt->psb }} <br><br>

                        <b>Status Laporan : </b> {{ $dt->laporan_status }} <br>
                        <b>Tanggal Laporan :</b> {{ $dt->updated_at }} <br>
                        <b>Regu : </b> {{ $dt->uraian }}
                      </td>
                  </tr>
                @endforeach
              @elseif($ket=='up')
                @foreach ($data as $dt)
                  <tr>
                      <td>
                        <b>Tgl Registrasi : </b> {{ $dt->orderDate }} <br> 
                        <b>TRACKER ID : </b> {{ $dt->myir }}<br>
                        <b>Pelanggan : </b> {{ $dt->customer }} <br>
                        <b>Alamat : </b> {{ $dt->alamatLengkap }} <br>
                        <b>Koor. Pel : </b> {{ $dt->koorPelanggan }} <br>
                        <b>Pic Pelanggan : </b> {{ $dt->picPelanggan }} <br><br>

                        <b>Channel : </b> {{ $dt->channel }} <br>
                        <b>Jenis Layanana : </b> {{ $dt->layanan }} <br>
                        <b>Tansakai PSB : </b> {{ $dt->psb }} <br><br>

                        <b>SC : </b> {{ $dt->sc }}<br>
                        <b>Status Laporan : </b> {{ $dt->laporan_status }} <br>
                        <b>Tanggal Laporan :</b> {{ $dt->updated_at }} <br>
                        <b>Regu : </b> {{ $dt->uraian }}
                      </td>
                  </tr>
                @endforeach
              @endif
          </table>
      </div>
    
  </div>

@endsection