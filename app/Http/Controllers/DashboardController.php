<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\DA\DashboardModel;
use App\DA\TicketingModel;
use App\DA\WitelModel;
use App\Http\Requests;
use App\DA\HomeModel;
use App\DA\Tele;
use Telegram;
use Excel;
use DB;
use TG;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    protected $periode = [
      'HOLD','x0dan2jam','x2dan4jam','x4dan6jam','x6dan8jam','x8dan10jam','x10dan12jam','x12jam'
    ];

    protected $photokpro = [
    'Berita_Acara',
    'Speedtest',
    'SN_ONT',
    'ODP',
    'Lokasi',
    'Telephone_Incoming',
    'Foto_Pelanggan_dan_Teknisi',
    'LABEL',
    'KLEM_S',
    'K3',
    'Redaman_ODP',
    'SN_STB',
    'Live_TV'
  ];

  public function sendkprobydate($sektor,$date){
    $query = DB::SELECT('
      select
        *
      from
        Data_Pelanggan_Starclick a
      LEFT JOIN mdf b ON a.sto = b.mdf
      LEFT JOIN dispatch_teknisi c ON a.orderId = c.Ndem
      WHERE b.sektor_asr = "'.$sektor.'" AND DATE(a.orderDatePs)="'.$date.'" AND c.Ndem <> ""');
    foreach ($query as $num => $result){
      echo ++$num.". ";
      echo $result->orderId." | ".$result->id."<br />";
      $this->sendtobotkpro($result->orderId,$num);
      sleep(5);
    }
  }

  public function sendkprodaily($sektor){
    $query = DB::SELECT('
      select
        *
      from
        Data_Pelanggan_Starclick a
      LEFT JOIN mdf b ON a.sto = b.mdf
      LEFT JOIN dispatch_teknisi c ON a.orderId = c.Ndem
      WHERE b.sektor_asr = "'.$sektor.'" AND DATE(a.orderDatePs)="'.date('Y-m-d').'" AND a.jenisPsb="AO|BUNDLING"');
    foreach ($query as $num => $result){
      echo ++$num.". ";
      echo $result->orderId." | ".$result->id."<br />";
      $this->sendtobotkpro($result->orderId,$num);
    }
  }

    public function kehadiran($date){
      $data = DashboardModel::kehadiran2($date);
      if (count($data)<>0){
        foreach($data as $dt){
           $detail = DashboardModel::detailJumlahAbsen($dt->mainsector, $date);
           $detail = $detail[0];
           $list[] = array(
                      'chat_id'       => $dt->mainsector,
                      'sektor'        => $dt->title,
                      'jumlahTim'     => $detail->requestAsr+$detail->requestProv,
                      'requestAsr'    => $detail->requestAsr,
                      'approvalAsr'   => $detail->approveAsr,
                      'declineAsr'    => $detail->declineAsr,
                      'requestProv'   => $detail->requestProv,
                      'approvalProv'  => $detail->approveProv,
                      'declineProv'   => $detail->declineProv
                  );

        }
      }
      else{
        $list[]='';
      }


      // dd(count($list));
      return view('dashboard.kehadiran',compact('date','data', 'list'));
    }

    public function public_kehadiran(){
      $date = date('Y-m-d');
      $data = DashboardModel::kehadiran2($date);
      if (count($data)<>0){
        foreach($data as $dt){
           $detail = DashboardModel::detailJumlahAbsen($dt->mainsector, $date);
           $detail = $detail[0];
           $list[] = array(
                      'chat_id'       => $dt->mainsector,
                      'sektor'        => $dt->title,
                      'jumlahTim'     => $detail->requestAsr+$detail->requestProv,
                      'requestAsr'    => $detail->requestAsr,
                      'approvalAsr'   => $detail->approveAsr,
                      'declineAsr'    => $detail->declineAsr,
                      'requestProv'   => $detail->requestProv,
                      'approvalProv'  => $detail->approveProv,
                      'declineProv'   => $detail->declineProv
                  );

        }
      }
      else{
        $list[]='';
      }


      // dd(count($list));
      return view('dashboard.kehadiran_public',compact('date','data', 'list'));
    }

    public function autosendtobotkpro($tgl,$jenis,$status,$so,$ket){
      $data = dashboardModel::scbeList($tgl,$jenis,$status,$so,$ket);
      foreach ($data as $num => $result){
        $this->sendtobotkpro($result->orderId,++$num);
      }
      // return redirect('/dashboard/scbeList/'.$tgl.'/'.$jenis.'/'.$status.'/'.$so.'/'.$ket)->with('alerts', [
      //   ['type' => 'success', 'text' => '<strong>SUKSES</strong> sync kpro']
      // ]);
    }

    public function autosendtobotkprofix(){
      ini_set('max_execution_time', '3000');
      $data = DB::SELECT('SELECT * FROM dispatch_teknisi WHERE Ndem IN ("16612140",
"16613547",
"16615818",
"16616359",
"16617553",
"16619878",
"16619914",
"16620366",
"16621818",
"16619242",
"16623991",
"16634123",
"16636107",
"16636626",
"16637019",
"16641856",
"16642766",
"16642880",
"16647287",
"16647964",
"16648526",
"16649242",
"16650604",
"16650627",
"16662987",
"16666092",
"16666577",
"16666677",
"16667739",
"16668513",
"16668701",
"16670196",
"16671867",
"16672623",
"16672724",
"16673427",
"16673949",
"16675316",
"16675904",
"16677654",
"16687513",
"16687679",
"16687937",
"16688618",
"16688793",
"16690370",
"16690394",
"16690443",
"16690663",
"16690736",
"16691144",
"16691587",
"16692972",
"16693021",
"16693124",
"16693305",
"16693501",
"16694166",
"16695701",
"16696247",
"16706185",
"16707087",
"16707502",
"16707942",
"16709238",
"16710877",
"16712152",
"16712363",
"16713827",
"16714420",
"16714796",
"16716566",
"16717092",
"16719008",
"16719585",
"16719629",
"16719654",
"16724137",
"16725173",
"16726903",
"16727172",
"16728327",
"16728516",
"16729014",
"16634144",
"16709739",
"16719206",
"16725462",
"16730055",
"16739782",
"16740681",
"16741438",
"16742709",
"16743843",
"16743973",
"16746732",
"16747924",
"16748241",
"16748761",
"16749257",
"16749510",
"16749663",
"16752072",
"16752843",
"16752938",
"16753497",
"16754832",
"16756873",
"16759060",
"16762931",
"16764956",
"16765934",
"16768136",
"16688883",
"16719190",
"16753709",
"16780556",
"16780972",
"16781387",
"16781564",
"16783294",
"16784069",
"16785217",
"16786071",
"16786432",
"16786407",
"16787105",
"13022080",
"16788835",
"16788936",
"16791786",
"16791877",
"16792506",
"16792646",
"16792744",
"16792861",
"16793698",
"16795076",
"16797140",
"16798954",
"16799061",
"16799507",
"16799820",
"16800536",
"16801342",
"16802076",
"16802338",
"16803793",
"16804490",
"16785178",
"16790343",
"16816703",
"16817024",
"16818934",
"16819562",
"16819706",
"16819761",
"16821188",
"16821366",
"16822762",
"16822904",
"16823309",
"16824251",
"16825467",
"16826334",
"16828039",
"16828117",
"16828824",
"16830274",
"16830728",
"16831841",
"16832154",
"16832327",
"16832700",
"16837201",
"16837295",
"16841444",
"16865008",
"16947496",
"16861156",
"16949880",
"16921354",
"16893245",
"16875955",
"16963864",
"16954310",
"16929165",
"16298927",
"16898780",
"16894534",
"16864802",
"16853566",
"16865011",
"16861444",
"16842254",
"16852677",
"16956906",
"16957565",
"16955242",
"16952809",
"16926480",
"16867519",
"16321924",
"16864047",
"16820672",
"16854257",
"16897261",
"16921571",
"16892311",
"16902112",
"16962617",
"16950094",
"16928557",
"16897014",
"16956320",
"16860058",
"16922881",
"16924804",
"16928671",
"16407803",
"16852552",
"16854525",
"16852635",
"16850992",
"16860356",
"16959901",
"16903090",
"16891318",
"16855873",
"16874221",
"16852100",
"16944941",
"16904158",
"16890497",
"16856339",
"16899087",
"16875199",
"16872126",
"16901641",
"16875110",
"16889762",
"16376008",
"16898679",
"16901950",
"16868095",
"16861908",
"16955232",
"16891194",
"16959155",
"16924780",
"16534968",
"16855644",
"16893818",
"16954383",
"16947842",
"16890313",
"16925978",
"16858270",
"16834995",
"16941953",
"16947763",
"16922912",
"16948581",
"16928935",
"16966921",
"16861759",
"16893170",
"16888347",
"16956476",
"16958381",
"16961295",
"16983095",
"16983611",
"16983895",
"16985683",
"16986020",
"16986982",
"16987027",
"16988664",
"16988889",
"16989466",
"16990035",
"16992815",
"16992788",
"16993938",
"16993862",
"16994145",
"6679945",
"16994759",
"16994763",
"16995538",
"16995910",
"16999328",
"17000861",
"17001760",
"17004742",
"17006121",
"17006516",
"17006590",
"17006791",
"17008992",
"16907415",
"16949709",
"16951164",
"16960474",
"16961014",
"16985976",
"16990642",
"16994048",
"17002953",
"17006588",
"17017240",
"17017956",
"17018500",
"17018740",
"17019923",
"17020351",
"17020433",
"17021312",
"17021679",
"17022725",
"17023685",
"17024034",
"17024110",
"17024628",
"17024757",
"17024824",
"17027595",
"17028061",
"17028085",
"17028102",
"17028329",
"17028331",
"17028536",
"17028557",
"17028962",
"17029337",
"17030786",
"17030911",
"17031629",
"17032324",
"17033262",
"17033574",
"17034814",
"17036116",
"17037032",
"17037416",
"17038293",
"17038384",
"17038738",
"17039608",
"16941421",
"16959186",
"16983937",
"16984555",
"16988142",
"16995466",
"16998712",
"17000106",
"17002825",
"17017813",
"17018518",
"17021333",
"17021999",
"17023001",
"17024889",
"17029696",
"17034743",
"17035690",
"17038346",
"17038490",
"17038499",
"17052637",
"17053024",
"17053107",
"17053211",
"17053727",
"17054239",
"17054726",
"17054801",
"17055637",
"17055670",
"17056012",
"17056051",
"17056338",
"17056856",
"17056881",
"17057159",
"17057107",
"17057753",
"17057670",
"17057700",
"17058009",
"17058217",
"17058245",
"17059099",
"17059100",
"17060285",
"17060712",
"17061290",
"17061512",
"17061547",
"17062372",
"17062299",
"17063644",
"17064918",
"17064883",
"17066023",
"17066134",
"17066546",
"17066496",
"17067956",
"17069824",
"17069800",
"17070232",
"17070364",
"17073070",
"17073428",
"17073792",
"17074321",
"17075817",
"17087416",
"17088615",
"17088617",
"17088635",
"17088777",
"17088885",
"17088923",
"17088946",
"17089027",
"17088963",
"17088989",
"17089097",
"17089230",
"17089157",
"17089255",
"17089265",
"17089421",
"17089567",
"17089674",
"17089728",
"17089846",
"17089899",
"17089907",
"17089940",
"17089970",
"17090532",
"17090588",
"17091123",
"17091135",
"17091227",
"17091337",
"17091285",
"17091765",
"17091841",
"17092416",
"17093002",
"17093056",
"17093134",
"17093075",
"17093175",
"17093444",
"17093949",
"17094669",
"17094770",
"17094938",
"17094949",
"17095111",
"17095153",
"17095422",
"17095523",
"17095596",
"17095861",
"17095694",
"17096522",
"17097459",
"17098026",
"17098269",
"17098995",
"17099561",
"17100193",
"17100421",
"17100434",
"17100615",
"17100564",
"17101983",
"17102076",
"17102482",
"17102664",
"17102675",
"17102854",
"17102862",
"17102904",
"17103286",
"17103656",
"17103693",
"17104026",
"17103946",
"17103991",
"17104690",
"17104774",
"17106142",
"17106357",
"17106895",
"17107185",
"17107555",
"17107902",
"17107998",
"17108524",
"17109258",
"17109407",
"17109525",
"17112060",
"17112445",
"17125217",
"17125157",
"17125204",
"17125349",
"17126039",
"17126118",
"17126829",
"17127016",
"17127045",
"17127052",
"17126995",
"17127600",
"17127713",
"17128235",
"17128403",
"17128452",
"17128606",
"17128812",
"17128813",
"17128816",
"17128935",
"17129010",
"17129142",
"17129748",
"17129775",
"17130020",
"17130521",
"17132783",
"17133996",
"17135454",
"17135501",
"17135597",
"17135645",
"17135738",
"17136028",
"17136515",
"17136556",
"17136817",
"17136958",
"17137735",
"17137836",
"17137958",
"17138622",
"17138867",
"17139579",
"17139680",
"17140371",
"17140628",
"17141758",
"17143048",
"17129185")');
    foreach ($data as $result){
        $this->sendtobotkpro($result->id);
      }
    }

   public function sendtobotkpro($id,$urutan){

      ini_set('max_execution_time', '300000');
    $get_sc = DB::SELECT('
      SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.id = "'.$id.'"
    ');

    if (count($get_sc)==0){
      $get_sc = DB::SELECT('
        SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.Ndem = "'.$id.'"
      ');

      if($get_sc){
          $id = $get_sc[0]->id;
      };
    };

    $result = $get_sc[0];
    foreach($this->photokpro as $num => $name) {
      if (count($get_sc)>0){
        echo "num.".$num." ".date('Y-m-d H:i:s');
        $path = '/mnt/hdd3/upload/evidence/'.$id.'/'.$name.'.jpg';
        $path2 = '/mnt/hdd2/upload/evidence/'.$id.'/'.$name.'.jpg';

        if (file_exists($path)) {
          $path_kpro = "/mnt/hdd3/upload/evidence/".$id."/".$name."-kpro.jpg";
          // if(!file_exists($path_kpro)){
            $img = new \Imagick($path);
            $img->scaleImage(600, 800, true);

             $watermark = new \Imagick('/mnt/hdd3/upload/watermark/watermark-1.png');
             $x = 0;
             $y = 0;
            $img->compositeImage($watermark,\Imagick::COMPOSITE_OVER, $x, $y);
            $img->writeImage();
          // }
          $foto = 'https://psb.tomman.info/upload3/evidence/'.$id.'/'.$name.'-kpro.jpg';
          #$foto = public_path().'/upload/evidence/'.$id.'/'.$name.'-kpro.jpg';
          echo $foto;
      try {
        //TG::sendMsg('@kprofoto_bot', 'order '.$urutan.' foto '.$num.' : '.$result->orderId);
        TG::sendPhoto('@kprofoto_bot', $foto,true,$result->orderId);
        # echo exec('/usr/share/tg/bin/telegram-cli -WR -e "send_photo @mnorrifani /mnt/hdd1/upload/evidence/'.$id.'/'.$name.'-kpro.jpg ['.$result->orderId.'] "');

      } catch(Exception $e) {
        echo "gagal";
      TG::sendMessage('@kprofoto_bot', 'failed');
      }
  }
      #echo exec('/usr/share/tg/bin/telegram-cli -WR -e "send_photo @kprofoto_bot /mnt/hdd2/upload/evidence/'.$id.'/Lokasi.jpg ['.$result->orderId.'] "');
        }elseif (file_exists($path2)){
          #$img = new \Imagick($path2);
          #$img->scaleImage(600, 800, true);

          // $watermark = new \Imagick('/mnt/hdd2/upload/watermark/watermark-1.png');
          // $x = 0;
          // $y = 0;
          // $img->compositeImage($watermark,\Imagick::COMPOSITE_OVER, $x, $y);
          #$img->writeImage("/mnt/hdd2/upload/evidence/".$id."/".$name."-kpro.jpg");
          #$foto = 'https://psb.tomman.info/upload2/evidence/'.$id.'/'.$name.'-kpro.jpg';
          echo $foto;
          try {
          TG::sendPhoto('@kprofoto_bot', $foto,true,$result->orderId);
      } catch(Exception $e) {
        echo "gagal";
      TG::sendMessage('@kprofoto_bot', 'failed');
      }

      }
      echo "<br />";
    }
  // return redirect('/'.$id)->with('alerts', [
    //     ['type' => 'success', 'text' => '<strong>SUKSES</strong> sync kpro']
    //   ]);
  }

    public function kpisektor(){
      $get_sektor = DB::SELECT('
          SELECT * FROM group_telegram a
          WHERE
            a.ket_sektor = 1
          ORDER BY a.urut ASC
        ');
      $data = array();
      foreach ($get_sektor as $sektor){
        $get_gaul_sektor = DB::SELECT('select count(*) as jumlah from detail_gangguan_close a left join dispatch_teknisi b ON a.ticket_id = b.Ndem left join regu c on b.id_regu = c.id_regu where c.mainsector = "'.$sektor->chat_id.'"');
        $get_1ds = DB::SELECT('select count(*) as jumlah from detail_gangguan_close_3on3 a left join dispatch_teknisi b ON a.ticket_id = b.Ndem left join regu c on b.id_regu = c.id_regu where c.mainsector = "'.$sektor->chat_id.'" and a.ttr12_jam = 0');

        $get_saldo = DB::SELECT('select count(*) as jumlah from rock_excel a left join dispatch_teknisi b ON a.trouble_no = b.Ndem left join regu c on b.id_regu = c.id_regu where c.mainsector = "'.$sektor->chat_id.'"');
        $data[$sektor->chat_id]['gaul']  = $get_gaul_sektor[0];
        $data[$sektor->chat_id]['saldo']  = $get_saldo[0];
        $data[$sektor->chat_id]['1ds']  = $get_1ds[0];
      }
      return view('dashboard.kpisektor',compact('get_sektor','data'));

    }

    public function panjangtarikan($periode){
      $query = DashboardModel::panjangtarikan($periode);
      return view('dashboard.panjangtarikan',compact('periode','query'));
    }

    public function index()
    {
      echo "apa cenganng2";
    }

    public function unspec(){
      $query = DashboardModel::unspec();
      return view('dashboard.unspec',compact('query'));
    }

    public function unspeclist($sto){
      $query = DashboardModel::unspeclist($sto);
      return view('dashboard.unspeclist',compact('query','sto'));
    }

    public function assuranceStatusOpen($witel){
      $query = DashboardModel::get_assuranceStatusOpen($witel);
      return view('assurance.dashboardStatus',compact('query'));
    }

    public function nossa($witel){
      if ($witel=="ALL"){
        $title = "MONITORING 1DS FZ KALIMANTAN 2";
        $org = 'a.Witel';
      } else {
        $title = "MONITORING 1DS WITEL ".strtoupper($witel);
        $org = 'b.sektor_asr';
      }
      $query = DashboardModel::getNossa('ALL',$witel,$org);
      $query_myi = DashboardModel::getNossa('6',$witel,$org);
      $lastsync = DashboardModel::get_last_sync_nossa();
      return view('dashboard.assuranceNossa',compact('query','query_myi','lastsync','title'));
    }
    public function nossasektor($witel){
      if ($witel=="ALL"){
        $title = "MONITORING 1DS FZ KALIMANTAN 2";
        $org = 'a.Witel';
      } else {
        $title = "MONITORING 1DS WITEL ".strtoupper($witel);
        $org = 'b.sektor_asr';
      }
      $query = DashboardModel::getNossa('ALL',$witel,$org);
      $query_myi = DashboardModel::getNossa('6',$witel,$org);
      $lastsync = DashboardModel::get_last_sync_nossa();
      return view('dashboard.assuranceNossa',compact('query','query_myi','lastsync','title'));
    }

    public function nossaNew($witel){
      if ($witel=="ALL"){
        $title = "MONITORING 1DS FZ KALIMANTAN 2";
        $org = "a.Witel";
      } else {
        $title = "MONITORING 1DS WITEL ".strtoupper($witel);
        $org = "b.sektor_asr";
      }
      $query = DashboardModel::getNossa('ALL',$witel,$org);
      $query_myi = DashboardModel::getNossa('6',$witel,$org);
      $lastsync = DashboardModel::get_last_sync_nossa();
      $data = array();
      foreach ($query as $result){
        foreach ($this->periode as $per) {
          $data[$result->ORG][$per] = DashboardModel::getNossaList('ALL',$per,' h.sektor_asr = "'.$result->ORG.'"');
        }
      }
      $periode = $this->periode;
      return view('dashboard.assuranceNossaNew',compact('query','query_myi','lastsync','title','data','periode'));
    }


    public function nossaList($channel,$periode,$witel){
      $query = DashboardModel::getNossaList($channel,$periode,$witel);
      return view('dashboard.assuranceNossaList',compact('query','periode','witel'));
    }

    public function nossaListText($channel,$periode,$witel){
      $query = DashboardModel::getNossaList($channel,$periode,$witel);
      return view('dashboard.assuranceNossaListText',compact('query','periode','witel'));
    }


    public function pbs($type){
      switch ($type) {
        case 'teamleader':
          $title = "PBS TEAM LEADER";
          $link = 'https://analytics.telkomakses.co.id/t/Tableau/views/PBS-TL/DASHBOARD_TL?:embed=y&amp;:showVizHome=no&amp;:host_url=https%3A%2F%2Fanalytics.telkomakses.co.id%2F&amp;:embed_code_version=2&amp;:tabs=no&amp;:toolbar=yes&amp;:showAppBanner=false&amp;:showShareOptions=true&amp;:display_spinner=no&amp;:loadOrderID=0';
          break;
        case 'pbs_hd' :
          $title = "PBS HELPDESK";
          $link = 'https://analytics.telkomakses.co.id/t/Tableau/views/PBS_HELPDESK/Dashboard1?:embed=y&amp;:showVizHome=no&amp;:host_url=https%3A%2F%2Fanalytics.telkomakses.co.id%2F&amp;:embed_code_version=2&amp;:tabs=no&amp;:toolbar=yes&amp;:showAppBanner=false&amp;:showShareOptions=true&amp;:display_spinner=no&amp;:loadOrderID=0';
          break;
        case 'sm' :
          $title = 'PBS SITE MANAGER';
          $link = 'https://analytics.telkomakses.co.id/t/Tableau/views/m-PBS-SM/Dashboard2?:embed=y&amp;:showAppBanner=false&amp;:showShareOptions=true&amp;:display_count=no&amp;:showVizHome=no';
          break;
        default:
          $title = "PBS TEAM LEADER";
          $link = 'https://analytics.telkomakses.co.id/t/Tableau/views/PBS-TL/DASHBOARD_TL?:embed=y&amp;:showVizHome=no&amp;:host_url=https%3A%2F%2Fanalytics.telkomakses.co.id%2F&amp;:embed_code_version=2&amp;:tabs=no&amp;:toolbar=yes&amp;:showAppBanner=false&amp;:showShareOptions=true&amp;:display_spinner=no&amp;:loadOrderID=0';
          break;
      }
      return view('dashboard.pbs',compact('link','title'));
    }

    public function PI(){
      $dateStart = Input::get('dateStart');
      $dateEnd = Input::get('dateEnd');
      $transaksi = Input::get('transaksi');
      $query = DashboardModel::PI($transaksi,$dateStart,$dateEnd);
      return view('dashboard.PI',compact('transaksi','dateStart','dateEnd','query'));
    }

    public function PIlist($witel,$jam){
      $dateStart = Input::get('dateStart');
      $dateEnd = Input::get('dateEnd');
      $query = DashboardModel::PIlist($witel,$jam,$dateStart,$dateEnd);
      return view('dashboard.PIlist',compact('witel','jam','dateStart','dateEnd','query'));
    }

    public function kpi_assurance_mitra(){
      $periode = Input::get('periode');
      $mitrax = Input::get('mitra');
      if ($periode=="") { $periode = date('Ym'); }
      if ($mitrax=="") { $mitrax = "ALL"; }
      $get_by_actual_solution = DashboardModel::by_actual_solution('SEGMENTASI',$periode,$mitrax);
      $get_by_jenis_ggn = DashboardModel::by_actual_solution('JENIS_GGN',$periode,$mitrax);
      $get_kpi_assurance = DashboardModel::kpi_assurance_mitrax($periode,$mitrax);
      $get_periodez = DashboardModel::kpi_assurance_periode();
      $get_mitra = DashboardModel::kpi_assurance_mitra($periode);
      $last_update = DashboardModel::get_last_update_kpi_assurance();
      return view('dashboard.kpi_assurance',compact('get_by_jenis_ggn','mitrax','get_mitra','get_kpi_assurance','periode','last_update','get_periodez','get_by_actual_solution'));
    }

    public function kpi_assurance_mitra_tech(){
      $periode = Input::get('periode');
      $mitrax = Input::get('mitra');
      if ($periode=="") { $periode = date('Ym'); }
      if ($mitrax=="") { $mitrax = "ALL"; }
      $get_by_actual_solution = DashboardModel::by_actual_solution('SEGMENTASI',$periode,$mitrax);
      $get_by_jenis_ggn = DashboardModel::by_actual_solution('JENIS_GGN',$periode,$mitrax);
      $get_kpi_assurance = DashboardModel::kpi_assurance_mitra_tech($periode,$mitrax);
      $get_periodez = DashboardModel::kpi_assurance_periode();
      $get_mitra = DashboardModel::kpi_assurance_mitra($periode);
      $last_update = DashboardModel::get_last_update_kpi_assurance();
      return view('dashboard.kpi_assurance',compact('get_by_jenis_ggn','mitrax','get_mitra','get_kpi_assurance','periode','last_update','get_periodez','get_by_actual_solution'));
    }

    public function kpi_assurance(){
      $periode = Input::get('periode');
      $mitrax = Input::get('mitra');
      if ($periode=="") { $periode = date('Ym'); }
      if ($mitrax=="") { $mitrax = "ALL"; }
      $get_by_actual_solution = DashboardModel::nonatero_group_by('b.SUB_KATEGORI_PERBAIKAN',$periode,$mitrax);
      $get_by_segmentasi = DashboardModel::nonatero_group_by('a.SEGMENTASI',$periode,$mitrax);
      $get_by_jenis_ggn = DashboardModel::nonatero_group_by('a.JENIS_GGN',$periode,$mitrax);
      $get_kpi_assurance = DashboardModel::kpi_assurance($periode,$mitrax);
      $get_periodez = DashboardModel::kpi_assurance_periode();
      $get_mitra = DashboardModel::kpi_assurance_mitra($periode);
      $last_update = DashboardModel::get_last_update_kpi_assurance();
      return view('dashboard.kpi_assurance',compact('get_by_actual_solution','get_by_jenis_ggn','get_by_actual_solution','mitrax','get_mitra','get_kpi_assurance','periode','last_update','get_periodez'));
    }

    public function kpi_assuranceList($tipe,$sektor,$periode){
      $query = DashboardModel::kpi_assuranceList($tipe,$sektor,$periode);
      return view('dashboard.kpi_assuranceList',compact('tipe','sektor','periode','query'));
    }
    public function kpi_assuranceListGaul($sektor,$periode){
      $tipe = 'GAUL';
      $query = DashboardModel::kpi_assuranceListGaul($sektor,$periode);
      return view('dashboard.kpi_assuranceListGAUL',compact('tipe','sektor','periode','query'));
    }

    public function provisioning_wallboard(){
      $query = DashboardModel::provisioning_wallboard_byjam();
      $query_hari = DashboardModel::provisioning_wallboard_byhari();
      $query2_hari = DashboardModel::provisioning2_wallboard_byhari();
       $lastsync = DB::connection('mysql4')->table('4_0_master_order_1_lastsync')->get();

      return view('dashboard.provisioning_wallboard',compact('lastsync','query','query_hari','query2_hari'));
    }

    public function provisioningbyjam($id,$witel){
      $query = DashboardModel::provisioning_wallboard_list($id,$witel);
      return view('dashboard.provisioning_wallboard_list',compact('query'));
    }

    public function provisioningbyhari($id,$witel){
      $query = DashboardModel::provisioning_wallboard_list_by_hari($id,$witel);
      return view('dashboard.provisioning_wallboard_list',compact('query'));
    }
    public function provisioning2byhari($id,$witel){
      $query = DashboardModel::provisioning2_wallboard_list_by_hari($id,$witel);
      return view('dashboard.provisioning_wallboard_list',compact('query'));
    }
    public function listbyumur($sektor,$durasi,$channel){
      $getAssuranceList = DashboardModel::listbyumur($sektor,$durasi,$channel);
      return view('dashboard.listbyumur',compact('getAssuranceList','sektor','durasi'));
    }

    public function umurTiket(){
      $getumurTiket = DashboardModel::umurTiket();
      return view('dashboard.umurTiket',compact('getumurTiket'));
    }

    public function rekon($date){
      $rekonMitraData = DashboardModel::rekonMitra($date);
      $datams2n = DashboardModel::ms2n($date);
      $datams2nmigrasi = DashboardModel::ms2nmigrasi($date);
      $dataRekonMintraDC = DashboardModel::rekonMitraDc($date);
      $rekonMitraProv    = DashboardModel::rekonMitrProv($date);
			$get_qc_borneo 		 = DashboardModel::qc_borneo($date);
      $rekonMitra = array();
      foreach($rekonMitraData as $rekon){
          foreach($dataRekonMintraDC as $rekonDc){
              if ($rekon->mitra == $rekonDc->mitra){
                  $rekonMitra[] = [
                      'mitra'               => $rekon->mitra,
                      'jumlah2'             => $rekon->jumlah,
                      'jumlah_prov2'        => $rekon->jumlah_prov,
                      'jumlah_prov_anomali' => $rekon->jumlah_prov_anomali,
                      'jumlah_migrasi'      => $rekon->jumlah_migrasi,
                      'jumlah_asr'          => $rekon->jumlah_asr,
                      'jumlahDc'            => $rekonDc->dc_preconn
                  ];
              };
          };
      }

      $data = array();
      foreach($rekonMitra as $mit){
          foreach($rekonMitraProv as $prov){
              if($prov->mitra==$mit['mitra']){
                  $mit    = array_merge($mit, array("jumlah_prov"=>$prov->jumlah_prov,"jumlah"=>$mit['jumlah2'] + $prov->jumlah_prov));
                  $data[] = $mit;
              };
          }

          if ($mit['jumlah_prov2']==0){
              $mit    = array_merge($mit, array("jumlah_prov"=>0,"jumlah"=>$mit['jumlah2'] + $prov->jumlah_prov));
              $data[] = $mit;
          }
      };

      $rekonMitra = $data;
      return view('dashboard.rekon',compact('get_qc_borneo','date','data','rekonMitra','datams2n','datams2nmigrasi'));
    }

    public function listJumlahDc($mitra, $tgl){
        $getData = DashboardModel::rekonMitraDcList($mitra, $tgl);
        return view('dashboard.RekonDcList',compact('getData', 'mitra', 'tgl'));
    }

    public function listms2n($tgl,$so,$jenis,$status){
      $title = "MS2N NAL";
      $query = DashboardModel::listms2n($tgl,$so,$jenis,$status);
      return view('dashboard.listms2n',compact('tgl','so','jenis','title','query'));
    }

    public function rekonList($jenis,$date){
      $data = DashboardModel::getRekonList($jenis,$date);
      return view('dashboard.rekonList',compact('jenis','date','data'));
    }

    public function listTimMitra($jenis,$date){
      $data = DashboardModel::listTimMitra($jenis,$date);
      return view('dashboard.listTimMitra',compact('jenis','date','data'));
    }

    public function mitra($tgl){
      $query = DashboardModel::getMitra($tgl);
      return view('dashboard.mitra',compact('tgl','query'));
    }

    public function migrasiList($tgl,$jenis,$status,$so,$order){
      $data = DashboardModel::getMigrasiStatusList($tgl,$jenis,$status,$so,$order);
      $title = "MIGRASI";

      // return view('dashboard.provisioningList',compact('title','data','tgl','jenis','status','so'));
      return view('dashboard.migrasiListDashboard',compact('title','data','tgl','jenis','status','so'));
    }

    public function migrasi($tgl,$jenis){
      // $query = DashboardModel::getMigrasi($tgl);
      $datax = DashboardModel::getMigrasiStatus($tgl,$jenis);

      $order = array();
      foreach($datax as $data){
          $order[] = $data->ketOrder;
      };

      $ketOrder = array_unique($order);
      return view('dashboard.migrasi',compact('tgl','datax','jenis', 'ketOrder'));
    }

    public function absenkanbosku(){
      date_default_timezone_set('Asia/Makassar');
      $absenkanbosku = DashboardModel::absenkanbosku();
      $content = "ABSENKANBOSKU<br />";
        $content .= "
          <table>
            <tr>
              <td>No</td>
              <td>Laborcode</td>
              <td>Nama</td>
              <td>Status</td>
              <td>Autoabsen</td>
            </tr>
        ";
      $number = 0;
      echo " (".count($absenkanbosku).") ";
      foreach ($absenkanbosku as $data){
        $autoabsen = $this->autoabsen($data->laborcode);
        $content .= "
            <tr>
              <td>".++$number."</td>
              <td>".$data->laborcode."</td>
              <td>".$data->teknisi."</td>
              <td>".$data->hadir."</td>
              <td>".$autoabsen."</td>
            </tr>
        ";
      }
        $content .= "
          </table>
        ";
        echo $content;
    }

    public function autoabsen($id){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/assurance/rta_update_absen.php');
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"laborcode=".$id."&reasoncode=HADIR&run=SUBMIT");
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $c = curl_exec($ch);
      return "DONE";
    }

    public function provisioning($tgl,$jenis, Request $req){
      // dashboard potensi undispatch
      $area   = array('INNER','BBR','KDG','BLC','TJL');
      $jumlah = count($area);
      for($aa=0; $aa<=$jumlah-1; $aa++){
          $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
          $enddate = date('Y-m-d');
          $datas = DB::SELECT('
          SELECT
              m.area_migrasi,
              SUM(CASE WHEN dt.id is NULL THEN 1 ELSE 0 END) as jumlah_undispatch
          FROM
              Data_Pelanggan_Starclick dps
              LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE
              (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
              AND dps.orderStatusId in (16,40,14,5,4,2,0,49) AND m.mdf<>""
              And m.area_migrasi="'.$area[$aa].'"
          ');

          // simpan data ke table psb_dashboard_prov
          DB::table('psb_dashboard_prov')->where('area',$area[$aa])->update(['undispatch' => $datas[0]->jumlah_undispatch]);

          $kendalas = DB::SELECT('
          SELECT
              m.area_migrasi,
              SUM(CASE WHEN dt.id is not NULL AND dps.orderStatusId=16 THEN 1 ELSE 0 END) as jumlah_kendala
          FROM
              Data_Pelanggan_Starclick dps
              LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE
              (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
              AND pl.status_laporan not in (1,5,6,8,28,29,30,31,32,37,38,4,7) and m.mdf<>""
              And m.area_migrasi="'.$area[$aa].'"
          ');
          DB::table('psb_dashboard_prov')->where('area',$area[$aa])->update(['kendala' => $kendalas[0]->jumlah_kendala]);
              // (DATE(dps.orderDate) = "'.$enddate.'")
      }

      $reportPotensi = DB::table('psb_dashboard_prov')->get();
      $listStatus    = DB::table('psb_laporan_status')->get();

      return view('dashboard.provisioning',compact('jenis','tgl','reportPotensi','listStatus'));
      // return view('dashboard.provisioning',compact('data','jenis','tgl','listStatus'));
    }

    public function provisioning2($tgl,$jenis, Request $req){
      $data = DashboardModel::provisioning($jenis,$tgl);
      $sqlWhere2 = '';

      if ($req->has('status')){
        $tes = $req->input('status');
        $jml = count($tes);

        $sqlWhere = '(';
        for($a=0;$a<$jml;$a++){
            $sqlWhere .= "'$tes[$a]'".',';
            $sqlWhere2 .= $tes[$a].'-';
        }
        $sqlWhere = substr($sqlWhere, 0, -1);
        $sqlWhere .= ')';

        $data = dashboardModel::provisioningCari($tgl,$sqlWhere);
      };

      // $reportPotensi = DashboardModel::reportPotensi();

      // dashboard potensi jumlah_undispatch
      $area   = array('INNER','BBR','KDG','BLC','TJL');
      $jumlah = count($area);
      for($aa=0; $aa<=$jumlah-1; $aa++){
          $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
          $enddate = date('Y-m-d');
          $datas = DB::SELECT('
          SELECT
              m.area_migrasi,
              SUM(CASE WHEN dt.id is NULL THEN 1 ELSE 0 END) as jumlah_undispatch
          FROM
              Data_Pelanggan_Starclick dps
              LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE
              (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
              AND dps.orderStatusId in (16,40,14,5,4,2,0,49) AND m.mdf<>""
              And m.area_migrasi="'.$area[$aa].'"
          ');

          // simpan data ke table psb_dashboard_prov
          DB::table('psb_dashboard_prov')->where('area',$area[$aa])->update(['undispatch' => $datas[0]->jumlah_undispatch]);

          $kendalas = DB::SELECT('
          SELECT
              m.area_migrasi,
              SUM(CASE WHEN dt.id is not NULL AND dps.orderStatusId=16 THEN 1 ELSE 0 END) as jumlah_kendala
          FROM
              Data_Pelanggan_Starclick dps
              LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE
              (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
              AND pl.status_laporan not in (1,5,6,8,28,29,30,31,32,37,38,4,7) and m.mdf<>""
              And m.area_migrasi="'.$area[$aa].'"
          ');
          DB::table('psb_dashboard_prov')->where('area',$area[$aa])->update(['kendala' => $kendalas[0]->jumlah_kendala]);
              // (DATE(dps.orderDate) = "'.$enddate.'")
      }

      $reportPotensi = DB::table('psb_dashboard_prov')->get();
      $listStatus    = DB::table('psb_laporan_status')->get();

      return view('dashboard.provisioning',compact('data','jenis','tgl','reportPotensi','listStatus','sqlWhere2'));
      // return view('dashboard.provisioning',compact('data','jenis','tgl','listStatus'));
    }

    public function listpotensi($date, $area, $jenis){
      $data = DashboardModel::listpotensi($jenis,$area);
      return view('dashboard.potensiList',compact('date','jenis','data'));
    }

    public function provisioningList($tgl,$jenis,$status,$so,$order,Request $req){
      ini_set('memory_limit','10G');
      $q = $req->q;
      if ($q==NULL){
          $data = DashboardModel::provisioningList($tgl,$jenis,$status,$so);
      }
      else{
          $q   = substr($q, 0, -1);
          $tes = explode('-', $q);
          $jml = count($tes);

          $sqlWhere = '(';
          for($a=0;$a<$jml;$a++){
              $sqlWhere .= "'$tes[$a]'".',';
          }
          $sqlWhere = substr($sqlWhere, 0, -1);
          $sqlWhere .= ')';

          $data = dashboardModel::provisioningListFilter($tgl,$jenis,$status,$so,$sqlWhere);
          // dd($data);
      }

      if ($order=='PROV'){
          $title = "PROVISIONING";
      }
      else{
          $title = "SCBE";
      };

      // Excel::create("List Material Keluar", function($excel) use ($datas) {
      //       //  set property
      //       $excel->setTitle("Data Project");

      //       $excel->sheet('List Material Keluar', function($sheet) use ($datas){
      //           $sheet->mergeCells('A1:J1');
      //           $sheet->cells('A1', function ($cells) use ($datas){
      //               $cells->setFontWeight('bold');
      //               $cells->setValue('List Material Keluar');
      //           });

      //           $row = 3;
      //           $sheet->row($row, [
      //               'No',
      //               'SM',
      //               'TL',
      //               'SC',
      //               'Tanggal WO',
      //               'Tanggal Pasang',
      //               'Tanggal PS',
      //               'STO',
      //               'Telepon',
      //               'Inet / Datin',
      //               'Nama',
      //               'Alamat',
      //               'Jenis Mutasi',
      //               'Layanan',
      //               'Nama Modem',
      //               'Type / Series',
      //               'S/N or Mac Address',
      //               'merk',
      //               'Mode Gelaran',
      //               'DC',
      //               'Patchcore',
      //               'UTP Car 6',
      //               'Tiang',
      //               'Pullstrap',
      //               'Conduit',
      //               'Tray Cable',
      //               'ODP',
      //               'Kord. Pelanggan',
      //               'Area',
      //               'Mitra',
      //               'Status SC',
      //               'Status Tek',
      //               'Tambah Tiang',
      //               'Catatan Tek'
      //             ]);

      //           foreach($datas as $no=>$data){
      //             $sheet->row(++$row,[
      //                 ++$no,
      //                 'MUHAMMAD NOR RIFANI',
      //                 $data->TL,
      //                 $data->uraian,
      //                 $data->uraian,
      //                 $data->orderId,
      //                 $data->tanggal_dispatch,
      //                 $data->modified_at,
      //                 $data->orderDatePs,
      //                 $data->sto,
      //                 $data->ndemPots,
      //                 $data->ndemSpeedy,
      //                 $data->orderName,
      //                 $data->orderAddr.' '.$data->kordinat_pelanggan,
      //                 $data->jenisPsb,
      //                 $data->jenis_layanan,
      //                 '~',
      //                 $data->typeont.' ~ '.$data->typestb,
      //                 $data->snont.' ~ '.$data->snstb.
      //                 '~',
      //                 $data->dc_preconn ? : $data->dc_roll ? : '~',
      //                 $data->patchcore,
      //                 $data->utp,
      //                 $data->tiang,
      //                 $data->pullstrap,
      //                 $data->conduit,
      //                 $data->tray_cable,
      //                 strtoupper($data->nama_odp),
      //                 $data->kordinat_pelanggan,
      //                 $data->area_migrasi,
      //                 $data->mitra,
      //                 $data->orderStatus,
      //                 $data->laporan_status
      //               ]);
      //           }
      //       });
      //   })->export('xlsx');

      return view('dashboard.provisioningList',compact('title','data','tgl','jenis','status','so'));
    }

    public function provisioningListMitra($jenis,$tgl){
      $data = DashboardModel::provisioningListMitra($jenis,$tgl);
      $title = "PROVISIONING MITRA";
      $so = "NULL";

      return view('dashboard.provisioningList2',compact('title','data','tgl','jenis','status','so'));
    }

    public function migrasiListMitra($jenis,$tgl){
      $data = DashboardModel::migrasiListMitra($jenis,$tgl);
      $title = "MIGRASI MITRA";
      $so = "NULL";
      return view('dashboard.provisioningList2',compact('title','data','tgl','jenis','status','so'));
    }

    public function migrasiListMitraMs2n($jenis,$tgl){
      $data = DashboardModel::migrasiListMitraMs2n($jenis,$tgl);
      $title = "MIGRASI MITRA MS2N";
      $so = "NULL";
      return view('dashboard.provisioningList2',compact('title','data','tgl','jenis','status','so'));
    }


    public function assuranceListMitra($jenis,$tgl){
      $data = DashboardModel::assuranceListMitra($jenis,$tgl);
      $title = "ASSURANCE MITRA";
      $so = "NULL";
      return view('dashboard.assuranceListMitra',compact('title','data','tgl','jenis','status','so'));
    }

    public function provisioning1($jenis){
      $query = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-07%" GROUP BY datex');
      $juni = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-06%" GROUP BY datex');
      $mei = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-05%" GROUP BY datex');
      $april = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-04%" GROUP BY datex');
      $maret = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-03%" GROUP BY datex');
      $februari = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-02%" GROUP BY datex');
      $januari = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a LEFT JOIN mdf b ON a.sto = b.mdf WHERE b.area <> "" AND  a.orderDatePs LIKE "2017-01%" GROUP BY datex');
      $ytd = DB::SELECT('select date_format(orderDatePs,"%M") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a LEFT JOIN mdf b ON a.sto = b.mdf Where b.area <> "" AND a.orderDatePs LIKE "2017%" group by datex ORDER BY a.orderDatePs');
      return view('dashboard.provisioning1',compact('jenis','query','juni','mei','april','maret','februari','januari','ytd'));
    }

    public function assurance($date){
      $getRocActive = DashboardModel::RocActive();
      $getAction = DashboardModel::Action($date);
      $getStatus = DashboardModel::Status($date);
      $getTiket = DashboardModel::Tiket();
      $getTiket2 = DashboardModel::Tiket2();
      $getTiket_INT = DashboardModel::Tiket_INT($date);
      $getTiket_OpenINT = TicketingModel::dashboard();
      $getNonaterobyPic = DashboardModel::nonatero_active_by_pic();
      $getRockbyUmur = DashboardModel::rock_active_by_umur();
      $getRockbyUmur2 = DashboardModel::rock_active_by_umur_myi();
      $dataNonaterobyPic = array();

      foreach ($getNonaterobyPic as $data){
        $dataNonaterobyPic[$data->PIC]["jumlah"]            = $data->jumlah;
        $dataNonaterobyPic[$data->PIC]["isGamas_before"]    = $data->isGamas_before;
        $dataNonaterobyPic[$data->PIC]["isReguler_before"]  = $data->isReguler_before;
        $dataNonaterobyPic[$data->PIC]["jumlah_before"]     = $data->jumlah_before;
        $dataNonaterobyPic[$data->PIC]["isGamas_after"]     = $data->isGamas_after;
        $dataNonaterobyPic[$data->PIC]["isReguler_after"]   = $data->isReguler_after;
        $dataNonaterobyPic[$data->PIC]["jumlah_after"]      = $data->jumlah_after;
      }
      $get_sektor = DB::SELECT('
      select
      e.chat_id,
      e.sektor,
      sum(case when f.source="TOMMAN" then 1 else 0 end) as jumlah_INT,
      sum(case when f.source<>"TOMMAN" then 1 else 0 end) as jumlah_IN
      from
      psb_laporan a
      left join psb_laporan_action b ON a.action = b.laporan_action_id
      left join dispatch_teknisi c ON a.id_tbl_mj = c.id
      left join regu d ON c.id_regu = d.id_regu
      left join group_telegram e ON d.mainsector = e.chat_id
      left join data_nossa_1_log f ON c.Ndem = f.Incident
      where
      c.tgl LIKE "%'.$date.'%" AND b.action is not NULL and c.dispatch_by<>4 AND
      e.ket_sektor = 1 group by e.sektor order by e.urut');
      return view('dashboard.assurance',compact('getTiket_OpenINT','getTiket_INT','get_sektor','getRockbyUmur','getRockbyUmur2','dataNonaterobyPic','getNonatero','date','getTiket2','getTiket','getStatus','jenis','getRocActive','getAction'));
    }

    public function assranceListRoc($sektor, $status){
        $getData = DashboardModel::Tiket2List($sektor, $status);

        return view('dashboard.ListActiveRock',compact('getData'));

    }

    public function assuranceList($date,$status){
      ini_set('memory_limit', '2048M');

      $getAssuranceList = DashboardModel::getAssuranceList($date,$status);
      return view('dashboard.assuranceList',compact('date','status','getAssuranceList'));
    }

    public function assuranceList2($date,$status,$area){
      $getAssuranceList = DashboardModel::getAssuranceList2($date,$status,$area);
      return view('dashboard.assuranceList',compact('date','status','getAssuranceList'));
    }
    public function assuranceListbyAction($date,$status,$source,$sektor){
      $getAssuranceList = DashboardModel::getAssuranceListbyAction($date,$status,$source,$sektor);
      return view('dashboard.assuranceList',compact('date','status','getAssuranceList'));
    }

    public function MBSP(){
      $GetMBSP = DashboardModel::GetQuery("MBSP",NULL);
      return view('dashboard.mbsp',compact('GetMBSP'));
    }

    public function GetList($jenis, $status, $area){
      $GetList = DashboardModel::GetQueryList($jenis,$status,$area);
      return view('dashboard.list',compact('GetList'));
    }

    public function dashboardTelco($id){
      $tgl = date("Y-m-d");
      $kehadiran = DB::table('a2s_kehadiran')->where('tgl', $id)->orderBy('div', 'asc')->get();
      $dash = "";
      $lastDiv = '';
      foreach($kehadiran as $row) {
        if($row->div == $lastDiv){
          $dash[count($dash)-1]['total'] += 1;
          if($row->jadwal == 'MASUK')
            $dash[count($dash)-1]['masuk'] += 1;
          if($row->hadir == 'ABSEN')
            $dash[count($dash)-1]['absen'] += 1;
          else
            $dash[count($dash)-1]['tdk_absen'] += 1;
        }
        else {
          $dash[] = array("div"=>$row->div,"total"=>0, "masuk"=>0,"absen"=>0,"tdk_absen"=>0);
          $dash[count($dash)-1]['total'] += 1;
          if($row->jadwal == 'MASUK')
            $dash[count($dash)-1]['masuk'] += 1;
          if($row->hadir == 'ABSEN')
            $dash[count($dash)-1]['absen'] += 1;
          else
            $dash[count($dash)-1]['tdk_absen'] += 1;
          $lastDiv = $row->div;
        }
      }
      $prov = DB::TABLE('time_by_time_kpro')->orderBy('id', 'desc')->first();
      $asr = DB::TABLE('time_by_time_mdashboard')->orderBy('id', 'desc')->first();
      $pi = DB::TABLE('Data_Pelanggan_Starclick_Pi')->count();
      //dd($pi);
      $ndstb = DB::table('dispatch_teknisi')
        ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
        ->leftJoin('Data_Pelanggan_Starclick', 'dispatch_teknisi.Ndem', '=', 'Data_Pelanggan_Starclick.orderId')
        ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
        ->where('dispatch_teknisi.tgl', date("Y-m-d"))
        ->where('Data_Pelanggan_Starclick.jenisPsb', "MO|IPTV")
        ->orderBy('laporan_status', 'desc')
        ->get();
      $ogp = DB::table('dispatch_teknisi')
        ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
        ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
        ->where('dispatch_teknisi.tgl', date("Y-m-d"))
        ->where('dispatch_teknisi.dispatch_by', 2)
        ->where('psb_laporan.status_laporan', 5)
        ->count();
      $tc = DB::table('dispatch_teknisi')
        ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
        ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
        ->where('dispatch_teknisi.tgl', date("Y-m-d"))
        ->where('dispatch_teknisi.dispatch_by', 2)
        ->where('psb_laporan.status_laporan', 1)
        ->count();
      //dd($ndstb);
      $nd = array();
      $lastNama = 'asd';
      $no=0;
      $head = array();
      foreach ($ndstb as $no => $m){
        if(!empty($m->sto)){
          $head[] = $m->sto;
        }
      }
      $head = array_unique($head);
      foreach ($ndstb as $no => $m){
        if($lastNama == $m->laporan_status){
          $nd[count($nd)-1]['STO'][$m->sto] += 1;
          $no++;
        }else{
          $nd[] = array("status" => $m->laporan_status,"STO" => array());
          foreach($head as $h){
            if($h == $m->sto){
              $nd[count($nd)-1]['STO'][$h] = 1;
            }else{
              $nd[count($nd)-1]['STO'][$h] = 0;
            }
          }
          $no=0;
        }
        $lastNama = $m->laporan_status;
      }
      //dd($data);
      $sync = DB::table('f_sync')->orderBy('id', 'desc')->first();

      $da = DB::select('SELECT *,b.source_name,
        (select value from f_master where kategori_id = a.id and f_witel_id=3 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as KALSEL,
        (select value from f_master where kategori_id = a.id and f_witel_id=2 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as KALTENG,
        (select value from f_master where kategori_id = a.id and f_witel_id=1 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as KALBAR,
        (select value from f_master where kategori_id = a.id and f_witel_id=4 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as BALIKPAPAN,
        (select value from f_master where kategori_id = a.id and f_witel_id=5 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as SAMARINDA,
        (select value from f_master where kategori_id = a.id and f_witel_id=6 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as KALTARA
        FROM  f_kategori a left join f_source b on a.f_source_id = b.id
        WHERE urutan !=0 order by urutan asc');
      //dd($da);
      return view('laporan.dashboardTelco', compact('dash', 'prov', 'asr', 'pi', 'nd', 'head', 'ogp', 'tc', 'da', 'sync'));
    }
    public function detilTelco($id, $div ,$jadwal, $hadir){
      $detil = DB::table('a2s_kehadiran')->where('tgl', $id)
        ->when($div, function ($query) use ($div) {
            return $query->where('div', $div);
        })
        ->when($jadwal, function ($query) use ($jadwal) {
            return $query->where('jadwal', $jadwal);
        })
        ->when($hadir, function ($query) use ($hadir) {
            return $query->where('hadir', $hadir);
        })
        ->get();
      return view('laporan.kehadiranDetil', compact('detil'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function provisioningDua($tgl,$jenis, DashboardModel $dashboardModel){
      $data = DashboardModel::provisioning($jenis,$tgl);
      $reportPotensi = $dashboardModel->reportPotensiDua();

      return view('dashboard.provisioning2',compact('data','jenis','tgl','reportPotensi'));
    }

    public function reportUndispatch()
    {
      exec('cd ..;php artisan sendReportUndispacth> /dev/null &');
      // $data = Tele::sendReportUndispacth();
      // dd($data);
    }

    public function dashboardGuarante($tgl)
    {
       $getAction = DashboardModel::ActionGuarante($tgl);
       return view('dashboard.fulfillmentGuarante',compact('getAction', 'tgl'));
    }

    public function guaranteListbyAction($date,$status){
      $getGuaranteList = DashboardModel::getGuaranteListbyAction($date,$status);
      return view('dashboard.guarantelist',compact('date','status','getGuaranteList'));
    }

    public function rekonSebelumRfc($date){
      $data = DashboardModel::getRekon($date);
      $rekonMitra = DashboardModel::rekonMitra($date);
      $datams2n = DashboardModel::ms2n($date);
      $datams2nmigrasi = DashboardModel::ms2nmigrasi($date);
      return view('dashboard.rekonSebelumRfc',compact('date','data','rekonMitra','datams2n','datams2nmigrasi'));
    }

    public function provisioningListMitraSebelumRfc($jenis,$tgl){
      $data = DashboardModel::provisioningListMitraSebelumRfc($jenis,$tgl);
      $title = "PROVISIONING MITRA";
      $so = "NULL";
      return view('dashboard.provisioningList3',compact('title','data','tgl','jenis','status','so'));
    }

    public function migrasiListMitraSebelumRfc($jenis,$tgl){
      $data = DashboardModel::migrasiListMitraSebelumRfc($jenis,$tgl);
      $title = "MIGRASI MITRA";
      $so = "NULL";
      return view('dashboard.provisioningList3',compact('title','data','tgl','jenis','status','so'));
    }

    public function dashboardScbe($tgl){
      $witel = session('witel');
      $datel = DB::table('maintenance_datel')->where('witel',session('witel'));
      if ($witel=="KALSEL"){
          $data   = dashboardModel::scbe($tgl);
          $dataUp = dashboardModel::scbeUp($tgl);
          $dataReportPsStar     = dashboardModel::reportPotensiPsStar($tgl);
          $dataReporPsStarMo    = dashboardModel::reportPotensiPsStarMo($tgl);
          $dataReportTransaksi  = dashboardModel::dashboardTransaksi($tgl);
          $dataReportPsStarUp   = dashboardModel::reportPotensiPsStarUp($tgl);
          $dataKendala = dashboardModel::scbeKendala(date('Y-m',strtotime($tgl)));
          $bulan = $this->bulanIndo(date('m',strtotime($tgl))).' '.date('Y',strtotime($tgl));
          $dataSmartIndome      = dashboardModel::dashboardTransaksiSmartIndihome($tgl);
          $dataKendalaPt1 = dashboardModel::scbeKendalaPt1(date('Y',strtotime($tgl)));
          if (count($dataKendalaPt1)<>0){
              array_push($dataKendala,$dataKendalaPt1[0]);
          }

          // grafik
          $total = 0;
          $label = [];
          foreach($data as $dt){
                array_push($label, $dt->laporan_status);
                $total += $dt->jumlah;
          };

          $nilai = [];
          foreach($data as $dt){
            array_push($nilai, $dt->jumlah);
          };

          return view('scbe.dashboardScbe',compact('datel','data', 'tgl', 'dataReportPsStar', 'dataReporPsStarMo', 'dataReportTransaksi', 'dataReportPsStarUp', 'dataUp', 'dataKendala', 'bulan', 'dataSmartIndome', 'label','nilai'));
      }
      else{
          $data               = dashboardModel::scbeWitel($tgl,null,session('witel'));
          // $data_no_update     = dashboardModel::scbeWitel_no_update($tgl,null,session('witel'));
          $get_datel          = WitelModel::get_datel(session('witel'));
          $get_potensi_ps_up  = dashboardModel::get_potensi_ps_up($tgl,null,session('witel'));
          $get_potensi_ps     = dashboardModel::get_potensi_ps($tgl,null,session('witel'));
          $dataKendala = dashboardModel::scbeKendalaWitel(date('Y-m'));
          // grafik
          $total = 0;
          $label = [];
          foreach($data as $dt){
                array_push($label, $dt->laporan_status);
                $total += $dt->jumlah;
          };

          $nilai = [];
          foreach($data as $dt){
            array_push($nilai, $dt->jumlah);
          };

          return view('scbe.dashboardScbeBalikpapan',compact('dataKendala','get_potensi_ps','get_potensi_ps_up','get_datel','data', 'label', 'nilai', 'tgl'));

      }
    }

     public function dashboardScbeNew($tgl){
      $data   = dashboardModel::scbeNew($tgl);
      $dataUp = dashboardModel::scbeUp($tgl);
      $dataReportPsStar     = dashboardModel::reportPotensiPsStar($tgl);
      $dataReportPsScbe     = dashboardModel::reportPotensiPsScbe($tgl);
      $dataReporPsStarMo    = dashboardModel::reportPotensiPsStarMo($tgl);
      $dataReportTransaksi  = dashboardModel::dashboardTransaksi($tgl);
      $dataReportPsStarUp   = dashboardModel::reportPotensiPsStarUp($tgl);

      return view('scbe.dashboardScbe2',compact('data', 'tgl', 'dataReportPsStar', 'dataReportPsScbe', 'dataReporPsStarMo', 'dataReportTransaksi', 'dataReportPsStarUp', 'dataUp'));
    }

    public function scbeList($tgl,$jenis,$status,$so,$ket){
        $dataUp = '';
        // if ($status=='ALL' && $ket=='PROV'){
        //     $dataUp = DashboardModel::provisioningList($tgl,$jenis,'UP',$so);
        // };

        $data = dashboardModel::scbeList($tgl,$jenis,$status,$so,$ket);
        $title = "SCBE";

        return view('scbe.dashboardScbeList',compact('data','tgl','jenis','status','so','title','dataUp','ket'));
    }

    public function potensiPsList($tgl,$jenis,$status,$so){
        $data   = dashboardModel::potensiPs($tgl,$jenis,$status,$so);
        $title = "Report Potensi PS AO";

        // dd($data);
        return view('scbe.potensiPs',compact('data','tgl','jenis','status','so','title'));
    }

    public function potensiPsListPs($tgl,$jenis,$status,$so){
        $data   = dashboardModel::potensiPsUp($tgl,$jenis,$status,$so);
        $title = "Report Potensi PS AO";

        // dd($data);
        return view('scbe.potensiPs',compact('data','tgl','jenis','status','so','title'));
    }

    public function potensiPsScbeList($tgl,$jenis,$status,$so){
        $data = dashboardModel::potensiPsScbe($tgl,$jenis,$status,$so);
        $title = "Report Potensi PS";
        // dd($data);
        return view('scbe.potensiPsScbe',compact('data','tgl','jenis','status','so','title'));
    }

    public function potensiPsListMo($tgl,$jenis,$status,$so,$jnsPsb){
        $data   = dashboardModel::potensiPsMo($tgl,$jenis,$status,$so);
        $title = "Report Potensi PS MO";

        // dd($data);
        return view('scbe.potensiPsMo',compact('data','tgl','jenis','status','so','title'));
    }

    public function transaksiScbeList($tgl,$jenis,$status,$so){
        $data = dashboardModel::reportDashboardTransaksi($tgl,$jenis,$status,$so);
        $title = "Report Dashboard transaksi";
        // dd($data);
        return view('scbe.transaksiScbe',compact('data','tgl','jenis','status','so','title'));
    }

    public function transaksiScbeListIndihome($tgl,$jenis,$status,$so){
        $data = dashboardModel::reportDashboardTransaksiSmartIndhome($tgl,$jenis,$status,$so);
        $title = "Report Dashboard Transaksi SmartIndihome";
        // dd($data);
        return view('scbe.transaksiSmartIndihome',compact('data','tgl','jenis','status','so','title'));
    }

    public function dashboardTeritori($tgl, Request $req){
        $nik = session('auth')->id_user;
        $dataKetDashboard = dashboardModel::getKetDashboard($nik);

        $data = '';
        $group_telegram = HomeModel::group_telegram($nik);
        $ketDashboard = '';

        if (count($dataKetDashboard)<>0){
            $ketDashboard = $dataKetDashboard->ket_dashboard;
            $data = dashboardModel::dashboardTeritory($tgl,$ketDashboard);

        }

        if ($req->has('sektor')){
            $ketDashboard = $req->input('sektor');
            $data = dashboardModel::dashboardTeritory($tgl,$ketDashboard);
        }

        return view('dashboard.dashboardTl',compact('data', 'nik', 'group_telegram','ketDashboard', 'tgl'));

    }

    public function dashboardTeritoriList($tgl,$jenis,$status,$so){
        $data = dashboardModel::listDashboardTeritori($tgl,$jenis,$status,$so);
        $title = 'Dashboard TL ';

        return view('dashboard.dashboardTlList',compact('data', 'title', 'tgl', 'jenis', 'status', 'so'));
    }

    public function scbeListTotal($tgl, $so){
        $dataUp       = dashboardModel::potensiPsUp($tgl,'ALL','ALL',$so);
        $dataComp     = dashboardModel::potensiPs($tgl,'ALL','ALL',$so);
        $title        = "Report Potensi PS AO TOTAL";
        $status       = '';
        // $dataScbe     = dashboardModel::potensiPsScbe($tgl,'ALL','potensiScbe',$so);

        return view('scbe.potensiPsTotal',compact('title', 'dataUp', 'dataComp', 'tgl', 'status'));
    }

    private function bulanIndo($bulan){
        if($bulan==1){
            $blnIndo = 'Januari';
        }
        elseif($bulan==2){
            $blnIndo = 'Februari';
        }
        elseif($bulan==3){
            $blnIndo = 'Maret';
        }
        elseif($bulan==4){
            $blnIndo = 'April';
        }
        elseif($bulan==5){
            $blnIndo = 'Mei';
        }
        elseif($bulan==6){
            $blnIndo = 'Juni';
        }
        elseif($bulan==7){
            $blnIndo = 'Juli';
        }
        elseif($bulan==8){
            $blnIndo = 'Agustus';
        }
        elseif($bulan==9){
            $blnIndo = 'September';
        }
        elseif($bulan==10){
            $blnIndo = 'Oktober';
        }
        elseif($bulan==11){
            $blnIndo = 'November';
        }
        elseif($bulan==12){
            $blnIndo = 'Desember';
        }
        else{
          $blnIndo = '';
        }

        return $blnIndo;
    }

    public function dashDispatchReboundary($tgl){
        $dataDispatch = dashboardModel::dashReboundaryDispatch($tgl);
        $progressReboundary = DashboardModel:: progressReboundary($tgl);

        return view ('reboundary.dashboard',compact('dataDispatch', 'tgl', 'progressReboundary'));
    }

    public function listDispatchReboundary($area, $tgl, $status){
        $data = dashboardModel::listDispatchReboundary($area, $tgl, $status);
        // dd($data);
        return view('reboundary.listDispatch',compact('data'));
    }

     public function reboundaryList($tgl,$jenis,$status,$so){
        $data = dashboardModel::reboundaryList($tgl,$jenis,$status,$so);
        $title = "Reboundary";

        return view('reboundary.dashboardReboundaryList',compact('data','tgl','jenis','status','so','title','dataUp','ket'));
    }

     public function dashboardScbeTw($tgl, $ket){
      $ketTw = null;
      $title = '';
      if ($ket=='tw1'){
        $awal  = date_create($tgl."/1/1");
        $akhir = date_create($tgl."/3/31");

        $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
        $title = $tgl. ' TRIWULAN 1';
      }
      elseif ($ket=='tw2'){
        $awal  = date_create($tgl."/4/1");
        $akhir = date_create($tgl."/6/31");

        $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
        $title = $tgl. ' TRIWULAN 2';
      }
      elseif ($ket=='tw3'){
        $awal  = date_create($tgl."/7/1");
        $akhir = date_create($tgl."/9/31");

        $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
        $title = $tgl. ' TRIWULAN 3';
      }
      elseif ($ket=='tw4'){
        $awal  = date_create($tgl."/10/1");
        $akhir = date_create($tgl."/12/31");

        $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
        $title = $tgl. ' TRIWULAN 4';
      }

      $data   = dashboardModel::scbe($tgl, $ketTw);
      $dataUp = dashboardModel::scbeUp($tgl, $ketTw);
      $dataReportPsStar     = dashboardModel::reportPotensiPsStar($tgl, $ketTw);
      $dataReportPsStarUp   = dashboardModel::reportPotensiPsStarUp($tgl, $ketTw);
      $dataReporPsStarMo    = dashboardModel::reportPotensiPsStarMo(date('Y-m-d'));
      $dataReportTransaksi  = dashboardModel::dashboardTransaksi(date('Y-m-d'));
      $dataKendala = dashboardModel::scbeKendala(date('Y-m'));
      $bulan = $this->bulanIndo(date('m')).' '.date('Y');
      $tgl2  = date('Y-m-d');

      // $dataReportPsScbe     = dashboardModel::reportPotensiPsScbe($tgl);
      // $dataTes = dashboardModel::reportPotensiPsStarFalloutWfm($tgl);
      // dd($dataTes);

      return view('scbe.dashboardScbeTw',compact('data', 'tgl', 'dataReportPsStar', 'dataReporPsStarMo', 'dataReportTransaksi', 'dataReportPsStarUp', 'dataUp', 'dataKendala', 'bulan', 'title', 'tgl2', 'ket'));
    }

     public function scbeListTw($tgl,$jenis,$status,$so,$ket,$ketTww){
        $ketTw = null;
        if ($ketTww=='tw1'){
          $awal  = date_create($tgl."/1/1");
          $akhir = date_create($tgl."/3/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 1';
        }
        elseif ($ketTww=='tw2'){
          $awal  = date_create($tgl."/4/1");
          $akhir = date_create($tgl."/6/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 2';
        }
        elseif ($ketTww=='tw3'){
          $awal  = date_create($tgl."/7/1");
          $akhir = date_create($tgl."/9/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 3';
        }
        elseif ($ketTww=='tw4'){
          $awal  = date_create($tgl."/10/1");
          $akhir = date_create($tgl."/12/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 4';
        }

        $dataUp = '';
        if ($status=='ALL' && $ket=='PROV'){
            $dataUp = DashboardModel::provisioningList($tgl,$jenis,'UP',$so,$ketTw);
        };

        $data = dashboardModel::scbeList($tgl,$jenis,$status,$so,$ket,$ketTw);
        $title = "SCBE PERIODE ".$tgl.' '.$tw;

        return view('scbe.dashboardScbeList',compact('data','tgl','jenis','status','so','title','dataUp','ket'));
    }

    public function potensiPsListPsTw($tgl,$jenis,$status,$so,$ketTww){
        $ketTw = null;
        if ($ketTww=='tw1'){
          $awal  = date_create($tgl."/1/1");
          $akhir = date_create($tgl."/3/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 1';
        }
        elseif ($ketTww=='tw2'){
          $awal  = date_create($tgl."/4/1");
          $akhir = date_create($tgl."/6/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 2';
        }
        elseif ($ketTww=='tw3'){
          $awal  = date_create($tgl."/7/1");
          $akhir = date_create($tgl."/9/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 3';
        }
        elseif ($ketTww=='tw4'){
          $awal  = date_create($tgl."/10/1");
          $akhir = date_create($tgl."/12/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 4';
        };

        $data   = dashboardModel::potensiPsUp($tgl,$jenis,$status,$so,$ketTw);
        $title = "Report Potensi PS AO PERIODE ".$tgl." ".$tw;

        // dd($data);
        return view('scbe.potensiPs',compact('data','tgl','jenis','status','so','title'));
    }

    public function potensiPsListTw($tgl,$jenis,$status,$so,$ketTww){
        $ketTw = null;
        if ($ketTww=='tw1'){
          $awal  = date_create($tgl."/1/1");
          $akhir = date_create($tgl."/3/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 1';
        }
        elseif ($ketTww=='tw2'){
          $awal  = date_create($tgl."/4/1");
          $akhir = date_create($tgl."/6/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 2';
        }
        elseif ($ketTww=='tw3'){
          $awal  = date_create($tgl."/7/1");
          $akhir = date_create($tgl."/9/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 3';
        }
        elseif ($ketTww=='tw4'){
          $awal  = date_create($tgl."/10/1");
          $akhir = date_create($tgl."/12/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 4';
        };

        $data   = dashboardModel::potensiPs($tgl,$jenis,$status,$so);
        $title = "Report Potensi PS AO PERIODE ".$tgl." ".$tw;

        // dd($data);
        return view('scbe.potensiPs',compact('data','tgl','jenis','status','so','title'));
    }

    public function scbeListTotalTw($tgl, $so, $ketTww){
        $ketTw = null;
        if ($ketTww=='tw1'){
          $awal  = date_create($tgl."/1/1");
          $akhir = date_create($tgl."/3/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 1';
        }
        elseif ($ketTww=='tw2'){
          $awal  = date_create($tgl."/4/1");
          $akhir = date_create($tgl."/6/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 2';
        }
        elseif ($ketTww=='tw3'){
          $awal  = date_create($tgl."/7/1");
          $akhir = date_create($tgl."/9/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 3';
        }
        elseif ($ketTww=='tw4'){
          $awal  = date_create($tgl."/10/1");
          $akhir = date_create($tgl."/12/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 4';
        };

        $dataUp       = dashboardModel::potensiPsUp($tgl,'ALL','ALL',$so, $ketTw);
        $dataComp     = dashboardModel::potensiPs($tgl,'ALL','ALL',$so, $ketTw);
        $title        = "Report Potensi PS AO TOTAL PERIODE ".$tgl." ".$tw;
        $status       = '';
        // $dataScbe     = dashboardModel::potensiPsScbe($tgl,'ALL','potensiScbe',$so);

        return view('scbe.potensiPsTotal',compact('title', 'dataUp', 'dataComp', 'tgl', 'status'));
    }

    public function dashboadWifi($tgl){
        $data = DashboardModel::dashboadWifiId($tgl);
        $dataDatin = DashboardModel::dashboadDatin($tgl);

        $area   = array('INNER','BBR','KDG','BLC','TJL');
        $jumlah = count($area);
        for($aa=0; $aa<=$jumlah-1; $aa++){
            $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
            $enddate = date('Y-m-d');
            $datas = DB::SELECT('
            SELECT
                m.area_migrasi,
                SUM(CASE WHEN dt.id is NULL THEN 1 ELSE 0 END) as jumlah_undispatch
            FROM
                micc_wo dps
                LEFT JOIN dispatch_teknisi dt ON dps.ND = dt.Ndem
                LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
                LEFT JOIN mdf m ON dps.MDF = m.mdf
            WHERE
                m.mdf<>""
                AND m.area_migrasi="'.$area[$aa].'"
            ');
            // simpan data ke table psb_dashboard_prov
            DB::table('psb_dashboard_ccan')->where('area',$area[$aa])->update(['undispatch' => $datas[0]->jumlah_undispatch]);
        }

        // matrix hd ccan

        $user = ['92153701','96154577','95151848','92153706','92157603'];
        $matrik = array();
        $list   = array();

        foreach ($user as $u){
            $list = [];
            $jml  = 0;
            $getData = DashboardModel::getMatrixCcan($tgl, $u);
            if ($getData){
                foreach ($getData as $d){
                    $list[] = [
                        'Ndem'  => $d->Ndem
                    ];
                };

                $jml = count($getData);
            };

            $nama = '';
            $getNama = DB::table('1_2_employee')->where('nik',$u)->first();
            if($getNama){
                $nama = $getNama->nama;
            };

            $matrik[] = [
                'nik'     => $u,
                'nama'    => $nama,
                'jumlah'  => $jml,
                'dataWo'  => $list
            ];

        };

        $dataundispatch = DB::table('psb_dashboard_ccan')->get();

        return view('dashboard.DashboardCcanWifiId',compact('data', 'tgl', 'dataDatin','dataundispatch', 'matrik'));
    }

     public function dashboadWifiAjax($ket){
        $where_ket = '';
        if ($ket<>'ALL'){
            $where_ket = 'AND dps.layanan LIKE "%'.$ket.'%"';
        };

        $area   = array('INNER','BBR','KDG','BLC','TJL');
        $jumlah = count($area);
        for($aa=0; $aa<=$jumlah-1; $aa++){
            $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
            $enddate = date('Y-m-d');
            $datas = DB::SELECT('
            SELECT
                m.area_migrasi,
                SUM(CASE WHEN dt.id is NULL THEN 1 ELSE 0 END) as jumlah_undispatch
            FROM
                micc_wo dps
                LEFT JOIN dispatch_teknisi dt ON dps.ND = dt.Ndem
                LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
                LEFT JOIN mdf m ON dps.MDF = m.mdf
            WHERE
                m.mdf<>""
                AND m.area_migrasi="'.$area[$aa].'"
                '.$where_ket.'
            ');

            // simpan data ke table psb_dashboard_prov
            DB::table('psb_dashboard_ccan')->where('area',$area[$aa])->update(['undispatch' => $datas[0]->jumlah_undispatch]);
        }

        $dataundispatch = DB::table('psb_dashboard_ccan')->get();

        if ($ket=="" || $ket=="ALL"){
            $jenisPilihan = "ALL";
        }
        else{
          $jenisPilihan = $ket;
        };

        return view('dashboard.DashboardCcanWifiIdAjax',compact('dataundispatch','jenisPilihan'));
    }

    public function listDetailWifiId($tgl, $status, $area){
        $data = DashboardModel::listWifiId($area, $tgl, $status);
        $title = 'List Dashboard WIFI.ID';

        return view('dashboard.listDashboardCcan',compact('area', 'tgl', 'status', 'data', 'title'));
    }

    public function listDetailDatin($tgl, $status, $area){
        $data = DashboardModel::listDatin($area, $tgl, $status);
        $title = 'List Dashboard DATIN';

        return view('dashboard.listDashboardCcan',compact('area', 'tgl', 'status', 'data', 'title'));
    }

    public function listundispatchCcan($tgl,$area,$ket){
        $data = dashboardModel::getListUndispatchCcan($tgl,$area,$ket);
        return view('dashboard.listUndispatchCcan',compact('data'));
    }


    public function chartKendalUp($tgl, Request $req){
       DB::table('psb_kendala_up')->truncate();

       $area = 'ALL';
       if ($req->has('area')){
          $area = $req->input('area');
       };

       $where_area = ' AND m.area_migrasi="'.$area.'"';
       if ($area=='ALL'){
          $where_area = '';
       };

       $sql = 'SELECT
              c.laporan_status_id, c.laporan_status, b.status_kendala,
              SUM(CASE WHEN f.orderStatus="Completed (PS)" THEN 1 ELSE 0 END) as jumlahPs,
              SUM(CASE WHEN f.orderStatus<>"Completed (PS)" THEN 1 ELSE 0 END) as jumlahBelumPs
            FROM
              dispatch_teknisi a
            LEFT JOIN Data_Pelanggan_Starclick f ON a.Ndem=f.orderId
            LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
            LEFT JOIN psb_laporan_status c ON b.status_laporan=c.laporan_status_id
            LEFT JOIN mdf m ON f.sto=m.mdf
            WHERE
              a.dispatch_by IS NULL  AND
              f.orderId<>"" AND
              b.status_kendala IS NOT NULL AND
              a.tgl like "%'.$tgl.'%"'
              .$where_area.'
            GROUP BY
              b.status_kendala ASC
            ';

      $data = DB::select($sql);
      foreach ($data as $dt){
          $simpan = [
              'status_laporan' => $dt->status_kendala,
              'jumlah'         => $dt->jumlahPs + $dt->jumlahBelumPs,
              'jumlahPs'       => $dt->jumlahPs,
              'jumlahBelumPs'  => $dt->jumlahBelumPs,
          ];

          DB::table('psb_kendala_up')->insert($simpan);
      }

      $sql2 = 'SELECT
              c.laporan_status_id, c.laporan_status, b.status_kendala,
              count(*) as jumlahBelumPs,
              0 as jumlahPs
            FROM
              dispatch_teknisi a
            LEFT JOIN psb_myir_wo f ON a.Ndem=f.myir
            LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
            LEFT JOIN psb_laporan_status c ON b.status_laporan=c.laporan_status_id
            LEFT JOIN mdf m ON f.sto=m.mdf
            WHERE
              a.dispatch_by = 5  AND
              b.status_kendala IS NOT NULL AND
              a.tgl like "%'.$tgl.'%"'
              .$where_area.'
            GROUP BY
              b.status_kendala ASC
            ';

      $dataMyir = DB::select($sql2);
      foreach($dataMyir as $myir){
        $exist = DB::table('psb_kendala_up')->where('status_laporan',$myir->status_kendala)->first();
        if ($exist){
            DB::table('psb_kendala_up')->where('status_laporan',$myir->status_kendala)->update([
                'jumlah'    => $exist->jumlah + ($myir->jumlahPs + $myir->jumlahBelumPs),
                'jumlahPs'  => $exist->jumlahPs + $myir->jumlahPs,
                'jumlahBelumPs'  => $exist->jumlahBelumPs + $myir->jumlahBelumPs,

            ]);
        }
        else{
          $simpan = [
              'status_laporan' => $myir->status_kendala,
              'jumlah'         => $myir->jumlahPs + $myir->jumlahBelumPs,
              'jumlahPs'       => $myir->jumlahPs,
              'jumlahBelumPs'  => $myir->jumlahBelumPs,
          ];

          DB::table('psb_kendala_up')->insert($simpan);
        }
      }

      $status = [];
      $jumlah = [];
      $ps     = [];
      $belumPs= [];

      $getData = DB::table('psb_kendala_up')->orderBy('jumlah','DESC')->get();
      foreach($getData as $data){
          array_push($status, $data->status_laporan);
          array_push($jumlah, $data->jumlah);
          array_push($ps, $data->jumlahPs);
          array_push($belumPs, $data->jumlahBelumPs);
      };

     //  foreach($data as $dt){
     //      array_push($status, $dt->laporan_status);
     //      // array_push($jumlah , ['status'=>$dt->laporan_status, 'jumlah'=> $dt->jumlahPs + $dt->jumlahBelumPs]);
     //      array_push($jumlah , $dt->jumlahPs + $dt->jumlahBelumPs);
     //      array_push($ps,$dt->jumlahPs );
     //      array_push($belumPs,$dt->jumlahBelumPs);

     //  };

      return view('dashboard.DashboardKendalaUpChart',compact('status', 'jumlah', 'ps', 'belumPs', 'area'));
    }

    public function listPsDownload($tgl){
        $sql = 'SELECT
                  a.Ndem,
                  b.orderId,
                  b.internet,
                  b.alproname,
                  c.nama_odp
                FROM
                  dispatch_teknisi a
                LEFT JOIN Data_Pelanggan_Starclick b ON a.Ndem = b.orderId
                LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
                WHERE
                  a.dispatch_by IS NULL AND
                  b.orderId<>"" AND
                  b.orderStatusId IN (7,52) AND
                  a.created_at="'.$tgl.'"
                ';
        $data = DB::select($sql);
        return view('dashboard.ListPsDownload',compact('data'));
    }

    public function showData($id){
        $data = DB::table('psb_laporan')->where('id_tbl_mj',$id)->first();
        return response()->json($data);
    }

    public function saveData(Request $req, $id){
        $this->validate($req,[
            'sc' => 'numeric'
        ],[
            'sc.numeric' => 'Diisi Angka'
        ]);


        DB::table('psb_laporan')->where('id_tbl_mj',$id)->update([
            'sc_unsc'       => $req->sc,
            'kcontact_unsc' => $req->kcontact
        ]);

        $getMyir = DB::table('dispatch_teknisi')->where('id',$id)->first();
        DB::table('Data_Pelanggan_UNSC')->where('MYIR_ID',$getMyir->Ndem)->update([
            'orderId' => $req->sc
        ]);

        return 'sukses';
    }

    public static function getPsStatusHr($bulan){
        if ($bulan=='today'){
            $bulan = date('Y-m');
        }

        $data = DashboardModel::getHrPs($bulan);
        $pesan = "LIST HR COMPLETED (PS)\n";
        $pesan .= "===========================\n\n";

        $head = array();
        foreach($data as $dt){
          if (!in_array($dt->title, $head)){
            array_push($head, $dt->title);
          }
        };

        foreach($head as $h){
            $pesan .= $h."\n";
            foreach ($data as $dt){
                if ($h==$dt->title){
                    $pesan .= $dt->Ndem.' '.$dt->uraian.' '.$dt->tglStatus."\n";
                };
            };
          $pesan .= "\n";
        };

        $pesan .= 'Total '.count($data);

        if(count($data)<>0){
          Telegram::sendMessage([
              'chat_id' => '-1001309386322',
              'text' => $pesan
          ]);
        };

        echo $pesan;
    }

    public function listPsbMo(){
        // $getData = DB::table('Data_Pelanggan_Starclick')
        //             ->leftJoin('dispatch_teknisi','Data_Pelanggan_Starclick.orderId','=','dispatch_teknisi.Ndem')
        //             ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
        //             ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
        //             ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
        //             ->leftJoin('group_telegram','regu.mainsector','=','group_telegram.chat_id')
        //             ->select('dispatch_teknisi.Ndem','group_telegram.title','Data_Pelanggan_Starclick.sto','regu.uraian','dispatch_teknisi.jenis_layanan','Data_Pelanggan_Starclick.jenisPsb','Data_Pelanggan_Starclick.orderStatus','psb_laporan_status.laporan_status','dispatch_teknisi.tgl')
        //             ->where('dispatch_teknisi.tgl','LIKE','%2019%')
        //             ->orderBy('dispatch_teknisi.tgl','ASC')
        //             ->orderBy('psb_laporan.status_laporan','ASC')
        //             ->get();

        // $getData = DB::table('psb_myir_wo')
        //             ->leftJoin('dispatch_teknisi','psb_myir_wo.myir','=','dispatch_teknisi.Ndem')
        //             ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
        //             ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
        //             ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
        //             ->leftJoin('group_telegram','regu.mainsector','=','group_telegram.chat_id')
        //             ->select('dispatch_teknisi.Ndem','group_telegram.title','psb_myir_wo.sto','regu.uraian','dispatch_teknisi.jenis_layanan','psb_laporan_status.laporan_status','dispatch_teknisi.tgl')
        //             ->where('dispatch_teknisi.tgl','LIKE','%2019%')
        //             ->where('psb_myir_wo.ket','0')
        //             ->where('dispatch_teknisi.dispatch_by','5')
        //             ->orderBy('dispatch_teknisi.tgl','ASC')
        //             ->orderBy('psb_laporan.status_laporan','ASC')
        //             ->get();

        $sql = 'select dispatch_teknisi.Ndem, maintaince.no_tiket, dispatch_teknisi.tgl, psb_laporan.status_laporan, psb_laporan_status.laporan_status, maintaince.status, regu.uraian, group_telegram.title, Data_Pelanggan_Starclick.sto as stoStarclick, psb_myir_wo.sto as stoMyir
                from dispatch_teknisi
                LEFT JOIN Data_Pelanggan_Starclick ON dispatch_teknisi.Ndem = Data_Pelanggan_Starclick.orderId
                LEFT JOIN psb_myir_wo ON dispatch_teknisi.Ndem = psb_myir_wo.myir
                LEFT JOIN maintaince ON maintaince.no_tiket = dispatch_teknisi.Ndem
                LEFT JOIN psb_laporan ON dispatch_teknisi.id = psb_laporan.id_tbl_mj
                LEFT JOIN regu ON dispatch_teknisi.id_regu = regu.id_regu
                LEFT JOIN group_telegram ON regu.mainsector = group_telegram.chat_id
                LEFT JOIN psb_laporan_status ON psb_laporan.status_laporan = psb_laporan_status.laporan_status_id
                WHERE
                maintaince.status="close" AND
                dispatch_teknisi.tgl LIKE "%2019%" AND
                psb_laporan.status_laporan NOT IN ("1","37","38") AND
                maintaince.order_from="PSB" AND
                (dispatch_teknisi.dispatch_by IS NULL OR dispatch_teknisi.dispatch_by=5) AND
                (Data_Pelanggan_Starclick.orderId IS NULL OR psb_myir_wo.myir IS NULL)
                ORDER BY `dispatch_teknisi`.`Ndem`  DESC';

        $getData = DB::select($sql);

        return view('tool.listPsbMo',compact('getData'));
    }

    public function woPsQrcode($tgl){
        $data = DashboardModel::getWOPs($tgl);
        return view('dashboard.woPsQr',compact('data', 'tgl'));
    }

    public function woPsQrcodeDetail($tgl, $chatid, $status){
        $data = DashboardModel::getWoPsDetail($tgl, $chatid, $status);
        return view('dashboard.woPsQrDetail',compact('data'));
    }

}
