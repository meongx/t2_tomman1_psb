@extends('tech_layout')

@section('content')
  @include('marina.partial.alert')
  <h3>
    <!-- <a href="/order/input" class="btn btn-sm btn-info">
        <span class="glyphicon glyphicon-plus"></span>
      </a>
    Tambah Order -->
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
  <li class="{{ (Request::segment(2) == 'tech') ? 'active' : '' }}"><a href="/marina/tech">Need Progress</a></li>
  <li class="{{ (Request::segment(2) == 'monitor') ? 'active' : '' }}"><a href="/marina/monitor">Monitor Tiket</a></li>
  </ul>
  <div class="list-group">
    @foreach($list as $no => $data)
      <div class="list-group-item">
        <span class="label label-primary">{{ ++$no }}. {{ $data->nama_order }}</span>
        <!-- @if($data->jenis_order==1)
          <strong><a href="/marina/order/{{ $data->id }}" >{{ $data->no_tiket }}</a></strong>
        @else -->
          <strong>{{ $data->no_tiket }}</strong>
        <!-- @endif -->
        <br />
        <span>{{ $data->headline }}</span>
        <br />
        <span class="label label-default">PIC:{{ $data->pic }}</span><br />
        <span class="label label-default">CP:{{ $data->orderNotel }}/{{ $data->orderName }}</span><br />
        <span class="label label-default">ALAMAT:</span>{{ $data->orderAddr }}<br />
        <span class="label label-default">NDEM SPEEDY:</span>{{ $data->ndemSpeedy }}<br />
        <span class="label label-default">NDEM POTS:</span>{{ $data->ndemPots }}<br />
        <span class="label label-default">MANJA:</span>{{ $data->manja }}<br />
        @if($data->jenis_order == 3)
          <strong>{{ $data->nama_odp_m or $data->nama_odp }}( {{ $data->koordinat or $data->kordinat_odp}} )</a></strong><br/>
        @endif
        
        <a href="/marina/tech/{{ $data->id }}" class="btn btn-xs btn-info">
          {{ $data->status or 'Progress' }}
        </a>
        <!-- <a href="/tech-photos/{{ $data->id }}" class="btn btn-xs btn-info">
          Photos
        </a> -->
        @if($data->psb_laporan_id)
          <a href="#"  class="btn btn-xs btn-info button_info" id-psb="{{ $data->psb_laporan_id }}">
            Info
          </a>
        @endif
        <span class="label label-info">
          {{ $data->dispatch_regu_name or '---' }}
        </span>
        &nbsp
        <!-- 
        <span class="label label-info">
          Total Rp. {{ number_format($data->hasil_by_user) }}
        </span>
        -->
      </div>

    @endforeach
  </div>
  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #eee">
          <strong style="color:black;">Detail PSB</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 450px">
          <div class="form-group">
            <label for="nama_odp" class="col-sm-4 col-md-3 control-label">Nama ODP</label>
            <input name="nama_odp" id="nama_odp" class="form-control input-sm" rows="1" value="" readonly/>
          </div>
          <div class="form-group">
            <label for="koordinat_odp" class="col-sm-4 col-md-3 control-label">Koordinat ODP</label>
            <input name="koordinat_odp" id="koordinat_odp" class="form-control input-sm" rows="1" value="" readonly/>
          </div>
          <div class="form-group">
            <label for="koordinat_pelanggan" class="col-sm-4 col-md-3 control-label">Koordinat Pelanggan</label>
            <input name="koordinat_pelanggan" id="koordinat_pelanggan" class="form-control input-sm" rows="1" value="" readonly/>
          </div>
          <div class="form-group">
            <label for="catatan" class="col-sm-4 col-md-3 control-label">Catatan</label>
            <input name="catatan" id="catatan" class="form-control input-sm" rows="1" value="" readonly/>
          </div>
        </div>
        <div class="modal-footer" style="background: #eee">
          
        </div>
      </div>
    </div>
  </div>
@endsection
@section('plugins')
  <script>
  $(function() {
    $(".button_info").click(function(event) {
      $(".button_info").hide();
      psb = this.getAttribute("id-psb");
      $.getJSON( "/marina/jsonPsb/"+psb, function(data) {
        $("#nama_odp").val(data.nama_odp);
        $("#koordinat_odp").val(data.kordinat_odp);
        $("#koordinat_pelanggan").val(data.kordinat_pelanggan);
        $("#catatan").val(data.catatan);
      })
      .done(function(data) {
        $(".button_info").show();
      });
      $('#material-modal').modal({show:true});
    });
  });
  </script>
@endsection