@extends('layout')

@section('content')
  @include('partial.alerts')

      <div class="panel panel-primary" id="info">
          <div class="panel-heading">{{ $title }}</div>
          <div class="panel-body table-responsive">
              <table class="table table-bordered table-fixed">
                  <tr>
                      <th>#</th>
                      <th>Nomor Tiket</th>
                      <th>Uraian</th>
                      <th>Mitra</th>
                      <th>Sto</th>
                      <th>DC ROLL</th>
                  </tr>
         
                  @foreach($data as $no => $d)
                          <td>{{ ++$no }}</td>
                          <td><a href="/tiket/{{ $d->id_tbl_mj }}">{{ $d->Ndem }}</a></td>
                          <td>{{ $d->uraian }}</td>
                          <td>{{ $d->mitra }}</td>
                          <td>{{ $d->sto }}</td>
                          <td>{{ $d->dc_roll }}</td>
                      </tr>
                  @endforeach
              </table>
          </div>
      </div>
@endsection
  