<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DA\HomeModel;
use App\DA\AlkersarkerModel;
use App\DA\AlproModel;
use DB;

class HomeController extends Controller
{
	public function	index(){
		date_default_timezone_set('Asia/Makassar');
		
		$this->home();
	}

	public function updatesquad($nik){
		$query = DB::SELECT('select * from 1_2_employee a left join employee_squad b ON a.squad = b.squad_id WHERE nik = "'.$nik.'"');
		$query_squad = DB::table('employee_squad')->get();
		return view('home.updatesquad',compact('nik','query','query_squad'));
	}

	public function updatesquad_save(Request $request){
		$simpan = DB::table('1_2_employee')->where('nik',$request->input('nik'))->update([
			'squad' => $request->input('squad')
		]);
		return redirect('/updatesquad/'.$request->input('nik'))->with('alerts',[
				['type'=>'success','text'=> 'UPDATE SQUAD SUCCESS']
			]);
	}

	public function alpro(){
		$nik = session('auth')->id_karyawan;
		$group_telegram = HomeModel::group_telegram($nik);
		$get_alpro = AlproModel::get_alpro();
		return view('alpro.list',compact('group_telegram','get_alpro','nik'));
	}

	public function absen_decline($id){
		date_default_timezone_set('Asia/Makassar');

		$status_absen_by_id = HomeModel::status_absen_by_id($id)[0];
		return view('home.absen_decline',compact('status_absen_by_id'));
	}

	public function absen_decline_save(Request $req){
		date_default_timezone_set('Asia/Makassar');

		$update_absen = HomeModel::update_absen_by_id($req->input('absen_id'),$req->input('keterangan'),3);
		if ($update_absen){
			return redirect('/home')->with('alerts',[
				['type'=>'success','text'=> 'ABSEN DECLINE SUCCESS']
			]);
		} else {
			return redirect()->back()->with('alerts',[
				['type'=>'danger','text'=> 'ABSEN DECLINE FAILED. TRY AGAIN IN A MINUTE']
			]);
		}
		// $decline =
	}


	public function absen_approval($nik,$date){
		date_default_timezone_set('Asia/Makassar');

		$approve_absen = HomeModel::approve_absen($nik,$date);
		return redirect()->back()->with('alerts',[
			['type'=>'success','text'=> $nik.' ABSEN APPROVED']
		]);
	}

	public function home(){
		date_default_timezone_set('Asia/Makassar');
    	$datex = date('Y-m-d H:i:s');
		$nik = session('auth')->id_user;
		$level = session('auth')->level;

		$jumlah_hadir = 0;
		if ($level == 10) {
			$status_absen = HomeModel::status_absen($nik);
			$alkersarker = AlkersarkerModel::list_teknisi(session('auth')->id_user);
			$statusProv = HomeModel::cekStatusTekProv($nik);
			$status_absenEsok = HomeModel::status_absenEsok($nik);

			return view('home.home_teknisi',compact('status_absen','datex','alkersarker','statusProv', 'status_absenEsok'));
		} else {
			$active_team = HomeModel::active_team($nik);
			if ($nik=='88159353' || $nik == "720428"){
				$active_team = HomeModel::active_team_all();
			};

			if ($nik=='96158953'){
				$active_team = HomeModel::active_team_all_atasan();
			}

			foreach($active_team as $at){
				if ($at->status=="HADIR") $jumlah_hadir++;
			}
			$group_telegram = HomeModel::group_telegram($nik);
			$absen_approval = HomeModel::absen_approval(session('auth')->id_user);
			$inbox_xcheck_material_tl = HomeModel::inbox_xcheck_material_tl($nik);

			$data_alker = array();
			foreach ($absen_approval as $absen){
				$get_alker = AlkersarkerModel::list_teknisi_avai($absen->nik);
				foreach ($get_alker as $alker):
				$data_alker[$absen->nik][] = $alker->alkersarker;
				endforeach;
			}

			$data_team = array();
			foreach ($active_team as $query_active_team) :
				if ($query_active_team->NIK<>NULL)
				$data_team[$query_active_team->NIK] = HomeModel::active_team_p($query_active_team->NIK,$nik);
			endforeach;

			return view('home.home',compact('data_alker','jumlah_hadir','data_team','inbox_xcheck_material_tl','absen_approval','group_telegram','active_team'));
		}
	}

	public function absen(){
		date_default_timezone_set('Asia/Makassar');

		$nik = session('auth')->id_user;
		$absen = HomeModel::absen($nik);
		return redirect()->back()->with('alerts', [
			['type' => $absen['status'], 'text' => $absen['message']]
		]);
	}

	public function absenBesok(){
		date_default_timezone_set('Asia/Makassar');
		$nik = session('auth')->id_user;
		$esok    = mktime(date("H",strtotime("08")),0,0,date("m"),date("d")+1,date("Y"));
		$tglEsok = date('Y-m-d H:i:s',$esok);

		$absen = HomeModel::simpanAbsenBesok($nik,$tglEsok);
		return redirect()->back()->with('alerts', [
			['type' => $absen['status'], 'text' => $absen['message']]
		]);
	}

	public function absenManualProv($nik){
        date_default_timezone_set('Asia/Makassar');
        $tglAbsen  = mktime(date("H",strtotime("08")),0,0,date("m"),date("d"),date("Y"));
        DB::table('absen')->insert([
            'nik'            => $nik,
            'date_created'    => date('Y-m-d H:i:s',$tglAbsen),
            'status'        => 'HADIR',
            'approval'        => '1',
            'date_approval'    => date('Y-m-d H:i:s',$tglAbsen),
            'tglAbsen'         => date('Y-m-d H:i:s',$tglAbsen),
            'absenEsok'        => '2'
        ]);

        echo 'sukses absen';
    }

    public function approveMassal(Request $req){
    	$getData 	= $req->approveMassal;
        $tglAbsen  	= date('Y-m-d');
        $auth 		= session('auth');

    	if (count($getData)==0){
    		return back()->with('alerts',[['type' => 'danger', 'text' => 'Nik Belum Dichecklist']]);
    	};

    	foreach($getData as $data){
    		DB::table('absen')->where('nik',$data)->whereDate('date_created',$tglAbsen)->update([
    			'approval'		=> 1,
    			'date_approval'	=> DB::raw('now()'),
    			'approve_by'	=> $auth->id_user
    		]);
    	}

    	return back()->with('alerts',[['type' => 'success', 'text' => 'Approve Massal Sukses']]);
    }

}
