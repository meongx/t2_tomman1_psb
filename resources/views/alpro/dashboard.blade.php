@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="row">
    <div class="col-sm-12">
      <h3>PROGRESS MAPPING ALPRO TERRITORY</h3>
    </div>
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <center>
            <?php
              $percent = (count($all_alpro_booked)/count($all_alpro))*100;
            ?>
            <h3>JUMLAH ODP BOOKED {{ number_format(count($all_alpro_booked)) }} ({{ round($percent,1) }}%)</h3>

            TOTAL ODP {{ number_format(count($all_alpro)) }}
          </center>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <table class="table">
        <tr>
          <th>#</th>
          <th>TL_NIK</th>
          <th>TL</th>
          <th>JUMLAH</th>
        </tr>
        @foreach ($data as $num => $result)
        <tr>
          <td>{{ ++$num }}</td>
          <td>{{ $result->nik_al }}</td>
          <td>{{ strtoupper($result->nama) }}</td>
          <td>{{ $result->jumlah }}</td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
@endsection
