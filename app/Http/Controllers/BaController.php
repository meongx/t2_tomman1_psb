<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use Telegram;
class BaController extends Controller
{
  public function index(){
    $data = DB::table('data_2028')->where('folder_ftp','not like','%UPLOAD %')->orderBy('status','asc')->get();
    $yours = DB::table('data_2028')->where('updated_by',session('auth')->id_karyawan)->get();
    $uploaded = DB::table('data_2028')->where('status','uploaded')->get();
    $booked = DB::table('data_2028')->where('status','booked')->get();
    $revisi = DB::table('data_2028')->where('status','revisi')->get();
    $folder_ftp = DB::SELECT('SELECT folder_ftp,count(*) as jumlah FROM data_2028 WHERE status = "uploaded" GROUP BY folder_ftp');
    $booker = DB::SELECT('SELECT b.nama,count(*) as jumlah FROM data_2028 a LEFT JOIN 1_2_employee b ON a.updated_by = b.nik WHERE a.status = "booked" GROUP BY a.updated_by ORDER BY  jumlah,b.nama DESC');
    $yours = count($yours);
    $uploaded = count($uploaded);
    $booked = count($booked);
    $revisi = count($revisi);
    $all = count($data);
    return view('bainstalasi.home',compact('booker','data','revisi','yours','uploaded','all','booked','folder_ftp'));
  }
  public function upload_form($no_internet){
    return view('bainstalasi.upload',compact('no_internet'));
  }
  public function save(Request $request){
    $id = $request->input('id');
    $save = DB::table('data_2028')->where('no_internet',$id)
                ->update([
                  'updated_by' => session('auth')->id_karyawan,
                  'modified_at' => date('Y-m-d h:i:s'),
                  'status' => 'uploaded'
                ]);
    echo $this->handleFileUpload($request, $id);
    return redirect('/ba')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan BA']
    ]);
  }

  public function book($id){
    $save = DB::table('data_2028')->where('no_internet',$id)
                ->update([
                  'status' => 'booked',
                  'updated_by' => session('auth')->id_karyawan,
                  'modified_at' => date('Y-m-d h:i:s')
                ]);
                return redirect('/ba')->with('alerts', [
                  ['type' => 'success', 'text' => '<strong>SUKSES</strong> booking order BA']
                ]);
  }

  public function handleFileUpload($request, $id){
    $input = 'userfile';
    if ($request->hasFile($input)) {
      //dd($input);
      $path = public_path().'/upload/bainstalasinew/';

      $file = $request->file($input);
      // $ext = $file->guessExtension();
      $ext = 'pdf';
      //TODO: path, move, resize
      try {
        $moved = $file->move("$path", "$id.$ext");
        return 'Berhasil upload file';

      }
      catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
        return 'gagal menyimpan foto evidence '.$id.' '.$e;
      }
    }
  }

  public function checkftp(){
    $get_file = DB::SELECT('SELECT * FROM data_2028 WHERE status = "uploaded" AND folder_ftp = ""');
    foreach ($get_file as $result) :
      $file = 'upload/bainstalasinew/'.$result->no_internet.'.pdf';
      if (file_exists($file)) :
        $copy = copy($file, 'upload/bainstalasinew/11/'.$result->no_internet.'.pdf');
        $update = DB::table('data_2028')->where('no_internet',$result->no_internet)->update(['status'=>'uploaded','folder_ftp'=>'UPLOAD BA INSTALASI 11']);
      endif;
    endforeach;
  }

  public function sendtoftp($folder_name){
    $folder_name = urldecode($folder_name);
    $get_file = DB::SELECT('SELECT * FROM data_2028 WHERE status = "uploaded" AND folder_ftp = ""');

    foreach ($get_file as $result) :
    $file = 'upload/bainstalasinew/'.$result->no_internet.'.pdf';
    if (file_exists($file)) :
    $update = DB::table('data_2028')->where('no_internet',$result->no_internet)->update(['folder_ftp'=>$folder_name]);

    $remote_file  = '/disk3/BA_INSTALASI_NOUSAGE/kalimantan/BA_2017/KALSEL/'.$folder_name.'/'.$result->no_internet.'.pdf';
    $ftp_server = '10.39.1.46';
    $conn_id = ftp_connect($ftp_server) or die("Couldn't connect to $ftp_server") ;
    $login_result = @ftp_login($conn_id, 'kalimantan', 'kalimantan01')  or die("Cannot login");
    ftp_pasv($conn_id, true) or die("Cannot switch to passive mode");
    if (ftp_put($conn_id, $remote_file, $file, FTP_BINARY)) {
     echo "successfully uploaded $file\n";
         ftp_close($conn_id);
    } else {
     echo "There was a problem while uploading $file\n";
    }
    else :
      echo "file not found $file\n";
      $update = DB::table('data_2028')->where('no_internet',$result->no_internet)->update(['folder_ftp'=>'FILE NOT FOUND','status'=> 'revisi']);
    endif;
    endforeach;

    // close the connection
  }

}
?>
