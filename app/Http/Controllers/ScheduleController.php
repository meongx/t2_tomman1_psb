<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
use App\DA\DashboardModel;
use App\DA\DispatchModel;
use App\DA\HomeModel;
use Illuminate\Support\Facades\Input;

class ScheduleController extends Controller
{
  public function home($periode){
    $auth =   $nik = session('auth');
    $nik = $auth->id_user;
    $TL_NIK = $nik;
    $group_telegram = HomeModel::group_telegram($nik);
    return view('schedule.home',compact('periode','group_telegram'));
  }
}
