<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use cURL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Telegram;
use DB;
use DateTime;
use Illuminate\Support\Facades\Input;
use App\DA\DispatchModel;

class DispatchController extends Controller
{

  public function gaul($periode){
      $query = DB::SELECT('
        SELECT
        *
        FROM
          nonatero_detil_gaul ngd
        LEFT JOIN dispatch_teknisi a ON ngd.TROUBLE_NO = a.Ndem
        LEFT JOIN regu b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        WHERE
          ngd.THNBLN = "'.$periode.'"
        ORDER by ngd.TROUBLE_NUMBER
      ');
      $query1 = DB::SELECT('
        SELECT
        *,
        d.JENIS as action,
        count(*) as jumlah
        FROM
          nonatero_detil_gaul ngd
        LEFT JOIN dispatch_teknisi a ON ngd.TROUBLE_NO = a.Ndem
        LEFT JOIN regu b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        WHERE
          ngd.THNBLN = "'.$periode.'" AND
          ngd.GAUL = 1
        GROUP BY d.JENIS
      ');
      $query2 = DB::SELECT('
        SELECT
        *,
        d.JENIS as action,
        count(*) as jumlah
        FROM
          nonatero_detil_gaul ngd
        LEFT JOIN dispatch_teknisi a ON ngd.TROUBLE_NO = a.Ndem
        LEFT JOIN regu b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        WHERE
          ngd.THNBLN = "'.$periode.'" AND
          ngd.GAUL = 2
        GROUP BY d.JENIS
      ');
      $query3 = DB::SELECT('
        SELECT
        *,
        d.JENIS as action,
        count(*) as jumlah
        FROM
          nonatero_detil_gaul ngd
        LEFT JOIN dispatch_teknisi a ON ngd.TROUBLE_NO = a.Ndem
        LEFT JOIN regu b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        WHERE
          ngd.THNBLN = "'.$periode.'" AND
          ngd.GAUL = 3
        GROUP BY d.JENIS
      ');
      $num = 1;
      $title = '';
      $action = '';
      $action_num = 0;
      foreach ($query as $result){

        if ($title <> $result->NCLI){
          $title = $result->NCLI;
          $num = 1;
        } else {
          $num++;
        }
       DB::table('nonatero_detil_gaul')->where('TROUBLE_NO',$result->TROUBLE_NO)
            ->update(['GAUL'=>$num]);
      }
      return view('dashboard.gaul',compact('query','query1','query2','query3'));
    }

    public function matrixText(){
      $date = Input::get('date');
      $area = Input::get('sektor');
      if ($area==""){ $area = 'ALL'; }
      if ($date==""){ $area = date('Y-m-d'); }
      date_default_timezone_set('Asia/Makassar');
      $get_sektor = DispatchModel::get_sektor();
      $get_area = DispatchModel::group_telegram($area);
      $getTeamMatrix = DispatchModel::getTeamMatrix($date,$area);
      echo "CREW | TOTAL TIKET | CLOSE | SISA<br />";
      foreach ($getTeamMatrix as $result){
        $sisa = $result->jumlah-$result->UP;
        echo $result->crewid." | ".$result->jumlah." | ".$result->UP." | ".$sisa."<br />";
      }
    }

  public function checksc(){
    $title = "Check SC";
    return view('dispatch.checkscform',compact('title'));
  }

  public function resultchecksc(){
      $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1513823387084&ScNoss=true&SearchText=7143775&Field=ORDER_ID&Fieldstatus=&Fieldwitel=&StartDate=&EndDate=&page=1&start=0&limit=10&sort=%5B%7B%22property%22%3A%22WITEL%22%2C%22direction%22%3A%22DESC%22%7D%5D";
      $result = json_decode(@file_get_contents($link));
  }

  public function nousage(){
    $query = DB::SELECT('SELECT * FROM nousage1 a LEFT JOIN Data_Pelanggan_Starclick b ON a.NOINET LIKE CONCAT("%",b.ndemSpeedy,"%")');
    foreach ($query as $data){
      echo $data->NOINET."<br />";
    }
  }

  public function index()
  {
    $this->WorkOrder('ALL');
  }

  public function psb_laporan(){
    $get_laporan = DB::SELECT('
      SELECT
        b.Ndem,
        a.kordinat_pelanggan,
        a.kordinat_odp,
        a.nama_odp
      FROM
        psb_laporan a
      LEFT JOIN dispatch_teknisi b ON a.id_tbl_mj = b.id
    ');
    $number = 1;
    $table = '
    <table>
      <tr>
        <td>No</td>
        <td>Ndem</td>
        <td>Kordinat Pelanggan</td>
        <td>Kordinat ODP</td>
        <td>Nama ODP</td>
      </tr>
    ';
    foreach ($get_laporan as $laporan) :
      $result = substr_replace($laporan->kordinat_pelanggan, '', 0, strpos($laporan->kordinat_pelanggan, "q=")+2);

      $table .= '
        <tr>
          <td>'.$number++.'</td>
          <td>'.$laporan->Ndem.'</td>
          <td>'.$laporan->kordinat_pelanggan.'</td>
          <td>'.$laporan->kordinat_odp.'</td>
          <td>'.$laporan->nama_odp.'</td>
        </tr>
      ';
    endforeach;
    $table .= '</table>';
    echo $table;
  }

  public function dashboard(){
    $list = 5;
    $data = DispatchModel::CountWO('READY');
    $READY = $data[0]->jumlah;
    $data = DispatchModel::CountWO('NAL');
    $NAL = $data[0]->jumlah;
    $data = DispatchModel::CountWO('MBSP');
    $MBSP = $data[0]->jumlah;
    $data = DispatchModel::CountWO('MBSP_CLUSTER');

    $MBSP_CLUSTER = $data[0]->jumlah;
    $data = DispatchModel::CountWO('MBSP_NONCLUSTER');
    $MBSP_NONCLUSTER = $data[0]->jumlah;
    $data = DispatchModel::CountWO('REGULER');
    $REGULER = $data[0]->jumlah;
    $data = DispatchModel::CountWO('REGULER_CLUSTER');
    $REGULER_CLUSTER = $data[0]->jumlah;
    $data = DispatchModel::CountWO('REGULER_NONCLUSTER');
    $REGULER_NONCLUSTER = $data[0]->jumlah;

    $data = DispatchModel::CountWO('NONNAL');
    $NONNAL = $data[0]->jumlah;
    $data = DispatchModel::CountWO('NONNAL_CLUSTER');
    $NONNAL_CLUSTER = $data[0]->jumlah;
    $data = DispatchModel::CountWO('NONNAL_NONCLUSTER');
    $NONNAL_NONCLUSTER = $data[0]->jumlah;


    return view('dispatch.dashboard',compact('NONNAL','NONNAL_CLUSTER','NONNAL_NONCLUSTER','READY','NAL','MBSP','MBSP_CLUSTER','MBSP_NONCLUSTER','REGULER','REGULER_CLUSTER','REGULER_NONCLUSTER'));
  }

  public function WorkOrder($id)
  {
    $list = DispatchModel::WorkOrder($id,NULL);
    $statusProv = null;
    return view('dispatch.list', compact('list'));
  }

  public function Transaksi(){
    $list = DispatchModel::Transaksi();
    return view('dispatch.list3', compact('list'));
  }

  public function FollowUp($id)
  {
    $list = DispatchModel::WorkOrder($id,NULL);
    return view('dispatch.list2', compact('list'));
  }

  public function QueryCheck($id){
    $list = DispatchModel::WorkOrder($id,NULL);
    echo $list;
  }

  public function matrix(){
    $date = Input::get('date');
    $area = Input::get('sektor');
    if ($area==""){ $area = 'ALL'; }
    if ($date==""){ $area = date('Y-m-d'); }
    date_default_timezone_set('Asia/Makassar');
    $get_sektor = DispatchModel::get_sektor();
    $get_area = DispatchModel::group_telegram($area);
    $getTeamMatrix = DispatchModel::getTeamMatrix($date,$area);
    $data = array();
    $lastRegu = '';
 
    foreach($getTeamMatrix as $team) :
      $listWO = array(); 
      $listWOData = DispatchModel::listWO($date,$team->id_regu);
      
      foreach ($listWOData as $listData) : 
        if ($listData->status_laporan<>52 && $listData->status_laporan<>57 && $listData->status_laporan<>58 && $listData->status_laporan<>59 && $listData->status_laporan<>60 && $listData->status_laporan<>69 && $listData->status_laporan<>68 && $listData->status_laporan<>61 ){
            $listWO[] = $listData;
        };
      endforeach;  

      $ketNik1='-';
      $ketNik2='-';
    
      if ($team->nik1<>NULL){
          $statusNik1 = DispatchModel::statusAbsen($team->nik1, $date);
          if(count($statusNik1)<>NULL){
              $ketNik1 = "HADIR";
          };
      }

      if($team->nik2<>NULL){
        $statusNik2 = DispatchModel::statusAbsen($team->nik2, $date);
        if(count($statusNik2)<>NULL){
            $ketNik2 = "HADIR";
        };
      }


      // echo $team->nik1."\n";
      // echo $statusNik2->nik.' '.$ketNik2."\n";

      $ketRegu = '';
      if ($ketNik1=="HADIR" || $ketNik2=="HADIR"){
          $ketRegu = "HADIR";
      };

      $data[$team->mainsector][] = array(
        "mitra" => $team->mitra,  
        "uraian"=>$team->uraian,
        "kemampuan"=>$team->kemampuan,
        "nik1"=>$team->nik1,
        "nik2"=>$team->nik2,
        "ketNik1" =>$ketNik1,
        "ketNik2" =>$ketNik2,
        "jns"       => "-",
        "ketRegu"   => $ketRegu,
        "listWO" =>$listWO
      );
    endforeach;

    if ($area=='ALL'){
        $ketSektor = 'ALL';
    }
    else{
        $ketSektor = 'PROVISIONING';
    }
      
    return view('dispatch.matrix2',compact('get_area','data','get_sektor','date','area','ketSektor'));
  }

  public function matrixPublic(){
    $date = Input::get('date');
    $area = Input::get('sektor');
    if ($area==""){ $area = 'ALL'; }
    if ($date==""){ $area = date('Y-m-d'); }
    date_default_timezone_set('Asia/Makassar');
    $get_sektor = DispatchModel::get_sektor();
    $get_area = DispatchModel::group_telegram($area);
    $getTeamMatrix = DispatchModel::getTeamMatrix($date,$area);
    $data = array();
    $lastRegu = '';

    $reguNotWo = [];
    if ($area=="PROV"){
        $reguNotWo = DispatchModel::getReguNotWo($date);
    }

    foreach($getTeamMatrix as $team) :
      if(substr($team->sektor, 0, 4)=="PROV"){
        if ($team->nik1_absen==1 || $team->nik2_absen==1){
            $listWO = DispatchModel::listWO($date,$team->id_regu);
            $data[$team->mainsector][] = array(
              "close_status" => $team->close_status,
              "PROGRESS" => $team->PROGRESS,
              "mitra" => $team->mitra,
              "KENDALA" => $team->KENDALA,
              "uraian"=>$team->uraian,
              "jumlah"=>$team->jumlah,
              "gangguan"=>$team->gangguan,
              "prov"=>$team->prov,
              "UP"=>$team->UP,
              "gangguan_UP_Copper"=>$team->gangguan_UP_Copper,
              "gangguan_UP_GPON"=>$team->gangguan_UP_GPON,
              "gangguan_UP_DES"=>$team->gangguan_UP_DES,
              "addon_UP"=>$team->addon_UP,
              "ccan_UP"=>$team->ccan_UP,
              "prov_UP"=>$team->prov_UP,
              "asis_UP"=>$team->asis_UP,
              "asis_MH"=>$team->asis_MH,
              "ccan_MH"=>$team->ccan_MH,
              "addon_MH"=>$team->addon_MH,
              "gangguan_Copper_MH"=>$team->gangguan_Copper_MH,
              "gangguan_GPON_MH"=>$team->gangguan_GPON_MH,
              "gangguan_DES_MH"=>$team->gangguan_DES_MH,
              "prov_MH"=>$team->prov_MH,
              "target_MH"=>$team->kemampuan,
              "kemampuan"=>$team->kemampuan,
              "nik1_absen"=>$team->nik1_absen,
              "nik2_absen"=>$team->nik2_absen,
              "listWO" =>$listWO
            );
        }
      }
      else{
            $listWO = DispatchModel::listWO($date,$team->id_regu);
            $data[$team->mainsector][] = array(
              "close_status" => $team->close_status,
              "PROGRESS" => $team->PROGRESS,
              "mitra" => $team->mitra,
              "KENDALA" => $team->KENDALA,
              "uraian"=>$team->uraian,
              "jumlah"=>$team->jumlah,
              "gangguan"=>$team->gangguan,
              "prov"=>$team->prov,
              "UP"=>$team->UP,
              "gangguan_UP_Copper"=>$team->gangguan_UP_Copper,
              "gangguan_UP_GPON"=>$team->gangguan_UP_GPON,
              "gangguan_UP_DES"=>$team->gangguan_UP_DES,
              "addon_UP"=>$team->addon_UP,
              "ccan_UP"=>$team->ccan_UP,
              "prov_UP"=>$team->prov_UP,
              "asis_UP"=>$team->asis_UP,
              "asis_MH"=>$team->asis_MH,
              "ccan_MH"=>$team->ccan_MH,
              "addon_MH"=>$team->addon_MH,
              "gangguan_Copper_MH"=>$team->gangguan_Copper_MH,
              "gangguan_GPON_MH"=>$team->gangguan_GPON_MH,
              "gangguan_DES_MH"=>$team->gangguan_DES_MH,
              "prov_MH"=>$team->prov_MH,
              "target_MH"=>$team->kemampuan,
              "kemampuan"=>$team->kemampuan,
              "nik1_absen"=>$team->nik1_absen,
              "nik2_absen"=>$team->nik2_absen,
              "listWO" =>$listWO
            );
     }
    endforeach;

    return view('dispatch.matrixPublic',compact('get_area','data','get_sektor','date','area', 'reguNotWo'));
  }

  public function matrixHD($date, DispatchModel $dispatchModel){
    date_default_timezone_set('Asia/Makassar');
    $getHdMatrix = DispatchModel::getHdMatrix($date);
    $hdFallout = DispatchModel::getHdFalloutMatrix($date);
  
    $data = array();
    $lastHD = '';
    $count = 0;
    foreach ($getHdMatrix as $hd){
      if($lastHD == $hd->updated_by){
        $data[count($data)-1]['jumlahWO'] += 1;
        $data[count($data)-1]['WO'][] = array("sc"=>$hd->Ndem, "status_laporan"=>$hd->status_laporan);
        //status
        if ($hd->status_laporan==6 || $hd->status_laporan==NULL){
          $data[count($data)-1]['NO_UPDATE'] += 1;
        } else if ($hd->status_laporan<>5 && $hd->status_laporan<>6 && $hd->status_laporan<>1 && $hd->status_laporan<>4) {
          $data[count($data)-1]['KENDALA'] += 1;
        } else if ( $hd->status_laporan==5){
          $data[count($data)-1]['PROGRESS'] += 1;
        } else if ($hd->status_laporan==1){
          $data[count($data)-1]['UP'] += 1;
        }
      }else{
        $data[] = array('nama' => $hd->nama,'jumlahWO'=>1,'PROGRESS'=>0,'KENDALA'=>0,'NO_UPDATE'=>0,'UP'=>0, 'WO' => array(array("sc"=>$hd->Ndem, "status_laporan"=>$hd->status_laporan)));
        if ($hd->status_laporan==6 || $hd->status_laporan==NULL){
          $data[count($data)-1]['NO_UPDATE'] = 1;
        } else if ($hd->status_laporan<>5 && $hd->status_laporan<>6 && $hd->status_laporan<>1 && $hd->status_laporan<>4) {
          $data[count($data)-1]['KENDALA'] = 1;
        } else if ( $hd->status_laporan==5){
          $data[count($data)-1]['PROGRESS'] = 1;
        } else if ($hd->status_laporan==1){
          $data[count($data)-1]['UP'] = 1;
        }

      }
      $lastHD = $hd->updated_by;
    }

    $fout = array();
    $lastHD = '';

    foreach ($hdFallout as $hd){
      if($lastHD == $hd->workerId){
          $fout[count($fout)-1]['jumlahWO'] += 1;
          $fout[count($fout)-1]['keterangan'] = $hd->ket; 
          $fout[count($fout)-1]['WO'][] = array("sc"=>$hd->orderId, "status_laporan"=>$hd->orderStatus, "keteranganWo"=>$hd->ket);
          //status

          if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="0" ){
            $fout[count($fout)-1]['UP'] += 1;
          }
          else if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="1" ){
            $fout[count($fout)-1]['UP_PDA'] += 1;
          }
           else{
            $fout[count($fout)-1]['PROGRESS'] += 1;
          }

          
      }else{
          $fout[] = array('nama' => $hd->workerName,'jumlahWO'=>1,'PROGRESS'=>0,'UP'=>0, 'UP_PDA'=>0,'keterangan'=>$hd->ket,'WO' => array(array("sc"=>$hd->orderId, "status_laporan"=>$hd->orderStatus, "keteranganWo"=>$hd->ket)));
          if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="0"){
            $fout[count($fout)-1]['UP'] += 1;
          } 
          else if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="1" ){
            $fout[count($fout)-1]['UP_PDA'] += 1;
          }else{
            $fout[count($fout)-1]['PROGRESS'] += 1;
          }      
      }
      $lastHD = $hd->workerId;
    }

    $periode = date('Y-m',strtotime($date));
    $manjas  = $dispatchModel->jumlahManjaByUser($periode);

    return view('dispatch.matrixHd',compact('data', 'fout', 'manjas'));

  }
  public function produktifitastek2($date,$area){
    if (strlen($date)<8) {
      if (date('Y-m')==$date) {
        if (date('d')<=22){
          $konstanta = date('d');
        } else {
          $konstanta = 22;
        }
      } else {
        $konstanta = 22;
      }
    } else {
      $konstanta = 1;
    }
    date_default_timezone_set('Asia/Makassar');
    $get_area = DispatchModel::group_telegram($area);
    $getTeamMatrix = DispatchModel::produktifitastek2($date,$area);
    $get_hari_lembur = DB::table('hari_lembur')->where('lembur', $date)->first();
    $data = array();
    $lastRegu = '';
    echo "1";
    foreach ($getTeamMatrix as $data){
      echo $data->nik." - ".$data->nama." (".$data->jumlah_regu.")<br />";
    }
  //  return view('dispatch.produktifitas2',compact('get_hari_lembur','date','konstanta','area','get_area','getTeamMatrix','area'));
  }

  public function produktifitastek($date,$area,$mitra){

    date_default_timezone_set('Asia/Makassar');
    // $get_area = DispatchModel::group_telegram($area);
    $get_area = DispatchModel::getGroupTelegramProv();
    $getTeamMatrix = DispatchModel::produktifitastek3($date,$area,$mitra);
    $get_hari_lembur = DB::table('hari_lembur')->where('lembur', $date)->first();
    $data = array();
    $lastRegu = '';
    $dataLastUpdate = array();
    foreach ($getTeamMatrix as $team) { 
    $getLastUpdate = DispatchModel::lastupdate($team->id_regu);
    if (count($getLastUpdate)>0){
      $dataLastUpdate[$team->id_regu] = $getLastUpdate;
    } else {
      $dataLastUpdate[$team->id_regu] = "Starting Progress at 09.00 AM";
    }
    }
    $layout = 'layout';
    return view('dispatch.produktifitastek2',compact('layout','dataLastUpdate','get_hari_lembur','date','konstanta','area','get_area','getTeamMatrix','area'));
  }

  public function tempQ(){
    $query = DB::SELECT('SELECT a.internet,b.manja,b.alamatSales FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem 
      WHERE a.orderStatus = "Completed (PS)" AND a.internet IN ("161214200316",
"162226200994",
"161201207185",
"162219202021",
"162206903716",
"161201202235",
"162204301845",
"161203207723",
"162204212542",
"161201218696",
"162222302864",
"162204213968",
"161201209043",
"161231202688",
"161231200507",
"162229203455",
"161201212067",
"161231201939",
"161214200725",
"162206903844",
"161203204698",
"162229303745",
"162222302791",
"162206901048",
"161201212682",
"162211300064",
"162204212801",
"162222303604",
"161203213664",
"161203212113",
"162204214425",
"162206900639",
"161201205102",
"161231200716",
"162204900703",
"162204214387",
"162228900581",
"161203207932",
"162204212001",
"161201214972",
"162204302178",
"162228900928",
"161203205732",
"161231202878",
"161201214121",
"161201214034",
"161214200108",
"161628500362",
"161203213015",
"161203209112",
"162204212046",
"162204213010",
"161201218137",
"162214302304",
"161201204954",
"161628500540",
"162204209646",
"161203207901",
"161212200147",
"162204214421",
"162204218203",
"162214302232",
"162222200828",
"162222303395",
"162204212583",
"161201215131",
"162203202692",
"162223202483",
"161203213550",
"162206900882",
"161201210573",
"161203209109",
"162206903655",
"162204216224",
"161201205934",
"161201207904",
"162223200786",
"161231200365",
"161221202140",
"161212202569",
"161628501815",
"162204217331",
"161203209267",
"161201215583",
"162204217769",
"162228800012",
"161201208789",
"162218202368",
"161201207112",
"161201304166",
"162204206864",
"162206900348",
"162206301690",
"162204212393",
"162204217607",
"162211300367",
"162204217054",
"161201213960",
"161201212612",
"162204212619",
"162222303590",
"161203213863",
"162222201661",
"162204217317",
"161201209524",
"161201208402",
"161203213315",
"162222303402",
"162229303508",
"162204211291",
"162222303962",
"162204220167",
"162204217703",
"162229202837",
"162224200971",
"161203213391",
"162203800327",
"162204217058",
"162206900458",
"162201300701",
"162228900583",
"162206903676",
"162229303783",
"162204214188",
"161231202551",
"161201211346",
"161201208443",
"161203204783",
"162218203453",
"161201214944",
"162204217948",
"162229203433",
"162219203688",
"162204200582",
"161203204866",
"161221200943",
"161201215117",
"162228900174",
"162204213183",
"162228800664",
"161203208013",
"161201204839",
"161221200793",
"162222302641",
"161202207730",
"161212202053",
"162206904145",
"162229202608",
"161201218250",
"162223202659",
"162228900139",
"161201212173",
"161212201001",
"161214200019",
"161201213494",
"162218202848",
"162202800433",
"161201208220",
"161201203237",
"162203902108",
"162218200794",
"162204216952",
"161203212363",
"162204206229",
"161209200041",
"162218202585",
"161201213750",
"162205203530",
"161203213518",
"162204213630",
"162219203626",
"161201210636",
"161201212639",
"162204213107",
"162204212884",
"162222201974",
"162204800513",
"162204217259",
"162204212293",
"162204208489",
"162229203020",
"161201211037",
"162204211432",
"161209200038",
"161201217867",
"161201213235",
"162222203038",
"162204212382",
"162204209521",
"162204220226",
"162204211513",
"162219203273",
"161628501289",
"162222302243",
"162214302484",
"161203203533",
"162204212125",
"162222202775",
"161201214800",
"161628500683",
"162229203173",
"161201212156",
"162204209960",
"161203208030",
"161201217915",
"162229202859",
"162204213870",
"161201219231",
"162211300038",
"162204209481",
"162203200099",
"161201210411",
"161628501214",
"161203208937",
"162222303472",
"161201217218",
"161201219372",
"162204301201",
"162204208454",
"162203300836",
"162222203018",
"161201211541",
"162204213029",
"161203208274",
"161203208139",
"162204212849",
"162204302910",
"162204208977",
"161201217153",
"162226200692",
"161202201229",
"162224200835",
"162229201646",
"161203206971",
"161628501694",
"162204211427",
"161203208250",
"161203207333",
"162229201732",
"162219201538",
"162219203318",
"161201211188",
"161201206276",
"161203204244",
"161201218626",
"162219201235",
"161221200950",
"162222201908",
"161201217023",
"162204218150",
"162231200190",
"162204214450",
"161203213660",
"162229203411",
"162229202542",
"161201214634",
"162222202037",
"162204217755",
"161201214186",
"162204216432",
"162222201024",
"162226200803",
"162206903602",
"162204212219",
"162229203571",
"161628500162",
"162229301003",
"162204214771",
"161201214124",
"162229301391",
"162204211460",
"162222302741",
"162204206180",
"162204212512",
"162229201985",
"161203213204",
"161201218893",
"161203205750",
"162219203615",
"162206904981",
"161203204904",
"161201201554",
"162229201600",
"162204220284",
"161221201118",
"161203207880",
"162204214344",
"162229302167",
"162222202091",
"162204220458",
"162204211960",
"162229202236",
"161201211953",
"162204211207",
"161201219205",
"161201209171",
"162204217707",
"162204217647",
"161203208755",
"162204213665",
"162229201483",
"161203213634",
"161203207876",
"161203209023",
"162229300617",
"162204212322",
"162226200819",
"162228900404",
"161201217444",
"162229203379",
"161201215443",
"161203208676",
"161201207001",
"162206903649",
"161203209353",
"161201215176",
"162222201017",
"162223900169",
"161201217807",
"161203212191",
"162229301892",
"161201205066",
"162204214578",
"162214302272",
"162204213989",
"161203204713",
"161201217874",
"162222303795",
"162219203496",
"162206903670",
"162229302163",
"162204800710",
"161231202485",
"162204216424",
"162204211174",
"161203209943",
"162204213437",
"162204217687",
"161201217415",
"161201217328",
"162222201288",
"161203205903",
"162203900353",
"162214300301",
"162204212241",
"161201200837",
"162204208189",
"162204204869",
"161201218906",
"161203212262",
"162204211857",
"162206900406",
"161203208323",
"162229202788",
"162231900165",
"161212202245",
"162204211970",
"162222202790",
"161202204964",
"162206904374",
"161628500340",
"161201203546",
"161201208413",
"161201206534",
"162218203326",
"161221200966",
"161201208405",
"161201208753",
"162204209339",
"161203208458",
"161212202208",
"162222201006",
"161203204303",
"162205203010",
"161202203854",
"162228800512",
"162206900397",
"162204212066",
"162223300380",
"161201215516",
"161201212490",
"162204217746",
"161201210192",
"162204212523",
"161203213093",
"162222201978",
"161203209064",
"161201206024",
"161201213229",
"161201213111",
"162204216782",
"161201218370",
"161203213319",
"161203209840",
"162204213799",
"161231202088",
"161203212719",
"162222303150",
"162204217543",
"161628500681",
"162229201931",
"162204220151",
"161212202465",
"162222201239",
"161201212424",
"161203209970",
"162206904300",
"162218202413",
"162204213701",
"162222303459",
"162204216042",
"161203205100",
"162214302336",
"162204217830",
"162204217750",
"162222202075",
"161221200972",
"162229203710",
"162222303995",
"162222303078",
"161202204422",
"161201214778",
"161201203362",
"161201211797",
"161203213301",
"162229201613",
"161201211215",
"162222302949",
"161201214081",
"162206903997",
"161628501270",
"161203207729",
"161201213029",
"162222301218",
"162206901045",
"162222900199",
"162222202089",
"162204217265",
"161201203512",
"162204212092",
"162204211205",
"161628501756",
"161203209256",
"162229202514",
"161201211301",
"162204212975",
"161201209530",
"161201209928",
"162204208919",
"161201211747",
"161203204520",
"161203207663",
"162218203576",
"162222302631",
"161203209663",
"161201217774",
"162222200641",
"162218202489",
"162214302318",
"161201218548",
"161203208503",
"161203204752",
"161231201043",
"161203205266",
"162204204499",
"162229202461",
"162223202680",
"162222301621",
"161201203780",
"162203902190",
"162223900665",
"161201212167",
"162204213209",
"161201303966",
"162229303788",
"162214302518",
"161203205489",
"162204216257",
"162228900828",
"162204206192",
"161212200821",
"162229203343",
"161202204092",
"161201205982",
"161203207765",
"162226201180",
"162204204455",
"162205203528",
"162222300232",
"162229301433",
"162226201009",
"162226200877",
"161201207274",
"162204209299",
"162204206037",
"162204213348",
"162204206550",
"162218202061",
"162204212061",
"161212202179",
"162204217694",
"162203800529",
"162222302878",
"162222202201",
"162204212186",
"162204900273",
"161628500135",
"162222302279",
"161201211076",
"162204212867",
"162219203788",
"162219203140",
"162214302665",
"162206904131",
"162206903506",
"162218202981",
"161221201042",
"161203213964",
"162203903006",
"161221200313",
"162204212725",
"161201218431",
"161201209990",
"161201210709",
"162204217751",
"162204301603",
"161221200174",
"161201208759",
"162229201576",
"162201203636",
"162204213035",
"162204213077",
"162204212254",
"161201211044",
"162204212164",
"161203208242",
"161201303327",
"161203213245",
"161203204766",
"162214302389",
"162204211418",
"162222202247",
"161221200586",
"161203203547",
"162222201313",
"161201214341",
"162206301882",
"162204208115",
"162226200703",
"162219203716",
"162229203312",
"162222202514",
"162219203660",
"161203209205",
"161203206832",
"161201212235",
"161628501015",
"162223300124",
"162214302451",
"162204211882",
"162204211673",
"162204206916",
"162204217658",
"162222201964",
"162222303235",
"161201208638",
"162222303137",
"162206903832",
"162229301290",
"162214302201",
"161203207900",
"161201205757",
"161212200315",
"162219203532",
"161212200500",
"162214302683",
"162204212115",
"162219200401",
"162222201945",
"162204206071",
"162204213407",
"162204214650",
"162228900830",
"162206301461",
"162204212256",
"162222203023",
"161201201688",
"161201217093",
"162205203529",
"162222201080",
"161203209174",
"161628501011",
"162229202866",
"161203206483",
"162222302644",
"162204213574",
"161201214017",
"162218203603",
"162204212618",
"161203209266",
"162204302224",
"161201219365",
"162218201413",
"161201213056",
"162201301500",
"162218202280",
"162222202097",
"161628501740",
"161201204483",
"162228900561",
"162225200084",
"161212202995",
"161212202884",
"162228900053",
"161201206268",
"161201217928",
"161203207131",
"161201213103",
"161203207260",
"162228800562",
"162222303363",
"162203902176",
"161201206919",
"162214302255",
"161203204940",
"162219203541",
"162203902506",
"161201218333",
"162222201720",
"162204216401",
"161201204638",
"162222300828",
"162224200753",
"162203901713",
"162204217973",
"162204211922",
"162218203651",
"162214302083",
"161203213917",
"162222201783",
"162204204283",
"162204211073",
"162204209707",
"162222301601",
"161221200734",
"161201218770",
"161201207500",
"162204211682",
"161628501088",
"162204800172",
"162204216635",
"161201215691",
"161212202996",
"162222202242",
"161201212128",
"162214302322",
"162204216101",
"162222201956",
"161201210427",
"162204800328",
"162222203041",
"162222202772",
"162204213246",
"161201215603",
"162204212213",
"162204214031",
"161203204213",
"161201217405",
"161201209310",
"162204214416",
"162204213111",
"162218202782",
"161201304763",
"161203212497",
"161201208101",
"162202902430",
"162204217728",
"162204211501",
"161628501662",
"162214302513",
"162224200414",
"162228800498",
"161203204815",
"162204213552",
"161201214230",
"161203207949",
"162219203702",
"161203208862",
"162203200419",
"162204206172",
"161203209588",
"162204800839",
"162228800612",
"162222303573",
"162229301737",
"161201211341",
"161203208810",
"162204211015",
"161202208314",
"162203902189",
"162202202368",
"162206301346",
"162222202077",
"162206300206",
"161628500439",
"161201208424",
"162203800273",
"162229303260",
"162204213969",
"162229203280",
"161201210685",
"161201303156",
"161201217097",
"161201212759",
"162222300444",
"162204212818",
"162206904043",
"162204217700",
"161628501267",
"161201303821",
"162229301787",
"162228900632",
"162218205011",
"162222301430",
"162204213105",
"161203205778",
"162222302675",
"162226200631",
"162226200196",
"161201304790",
"161201209128",
"161203208983",
"161214200786",
"161201212960",
"162229203614",
"161212202939",
"162218203216",
"162206904722",
"162204213678",
"162226200870",
"162222201287",
"162206903136",
"162229202975",
"162204212843",
"161201211228",
"162222303004",
"162214302079",
"162204301111",
"161203213361",
"162204214775",
"161203212243",
"161214200790",
"161203205779",
"162229303510",
"162224200752",
"161203213449",
"162222202895",
"162204212533",
"162203901955",
"162229303567",
"161201204191",
"161221202111",
"161212202295",
"161201215112",
"162204216174",
"162204212614",
"162229303363",
"162222303187",
"162204213058",
"161201203142",
"162204209879",
"162231900064",
"161203209190",
"161203208512",
"161203212995",
"161201215265",
"161203212636",
"162204216106",
"161201209856",
"161201202308",
"162204217218",
"162218203411",
"161212202674",
"162206903277",
"162222302074",
"162204209296",
"162222202167",
"161201213232",
"161201217992",
"161203208243",
"162204212332",
"162218202832",
"162222202652",
"162228900933",
"162220200464",
"162204214657",
"162204212597",
"162228900895",
"161201212155",
"161203206662",
"161201212111",
"162206904453",
"161201210154",
"162214302217",
"162204211769",
"161202208349",
"162204213951",
"162218203169",
"162203902220",
"162204209607",
"162204209216",
"162228800526",
"161203208068",
"161201210035",
"161203207455",
"162203901897",
"162202800524",
"161201303301",
"161231202453",
"161203212179",
"162204213402",
"162204211481",
"162222303755",
"161201212115",
"161201211222",
"162204211349",
"162222201653",
"162204220140",
"161201203865",
"162218203522",
"162219203515",
"162204212559",
"162204204026",
"162219201335",
"161201205784",
"161201206770",
"162229303243",
"161203207843",
"161202204268",
"162204216167",
"161201209729",
"161203212304",
"161203204299",
"161201214660",
"162204209673",
"161201218817",
"162222202042",
"161201300364",
"162218202828",
"162206904769",
"162218203289",
"162226201105",
"162204204565",
"162229203349",
"161231202511",
"161201207624",
"162228900761",
"162222202027",
"162218203750",
"161628501242",
"161201214356",
"162204206067",
"162204900547",
"162224200961",
"162204217512",
"161203205817",
"162204212947",
"161201217066",
"162222301273",
"161201215507",
"162228900143",
"161201214775",
"161203203432",
"161212202073",
"162204217129",
"161201212974",
"162214300034",
"162222201885",
"162222303821",
"161203207501",
"162206903062",
"161203212369",
"161203204375",
"162204217420",
"161628501050",
"161201214346",
"162204211095",
"161201206405",
"162211200041",
"162204209221",
"161201303372",
"162214302271",
"161201212069",
"162204202327",
"161214200995",
"162222302091",
"161203207607",
"161203212391",
"162220200413",
"161214200524",
"162211300422",
"162224200362",
"161201215165",
"162204211023",
"161201211851",
"161203212999",
"162204212551",
"161201214218",
"161201217916",
"161203209658",
"161201215546",
"161201219169",
"161231202889",
"162228900756",
"162204302824",
"161201300001",
"161628500997",
"162219203491",
"161231202729",
"162206900652",
"161201212916",
"161203214004",
"161203209578",
"161209200003",
"162204220452",
"161201215225",
"162204216926",
"162204216916",
"161201205783",
"161201206408",
"162222202305",
"162204212172",
"161201204533",
"161201213631",
"162205203512",
"162204217685",
"161201213233",
"162222202191",
"161201219064",
"161203213057",
"162204212911",
"161212202779",
"162229202288",
"161212202268",
"162206900922",
"162222301415",
"161203212900",
"162219203850",
"161221202141",
"161201217492",
"161201302044",
"162204209168",
"161201202040",
"161201208562",
"162222303346",
"162204206743",
"162204212755",
"162204208317",
"161201207019",
"161203213086",
"162204801424",
"162228900633",
"162204214694")');
  echo '<table><tr><td>Internet</td><td>Alamat 1</td><td>Alamat 2</td></tr>';
    foreach ($query as $result){
      echo "<tr><td>".$result->internet."</td><td>".$result->manja."</td><td>".$result->alamatSales."</td>";
    }
  }

public function produktifitasteksewa($date,$area,$mitra){

    date_default_timezone_set('Asia/Makassar');
    // $get_area = DispatchModel::group_telegram($area);
    $get_area = DispatchModel::getGroupTelegramProv();
    $getTeamMatrix = DispatchModel::produktifitastek3($date,$area,$mitra);
    $get_hari_lembur = DB::table('hari_lembur')->where('lembur', $date)->first();
    $data = array();
    $lastRegu = '';
    $dataLastUpdate = array();
    foreach ($getTeamMatrix as $team) { 
    $getLastUpdate = DispatchModel::lastupdate($team->id_regu);
    if (count($getLastUpdate)>0){
      $dataLastUpdate[$team->id_regu] = $getLastUpdate;
    } else {
      $dataLastUpdate[$team->id_regu] = "Starting Progress at 09.00 AM";
    }
    }
    $layout = 'layout';
    return view('dispatch.produktifitastek2',compact('layout','dataLastUpdate','get_hari_lembur','date','konstanta','area','get_area','getTeamMatrix','area'));
  }
  

  public function produktifitastekA($date,$area,$mitra){

      date_default_timezone_set('Asia/Makassar');
      // $get_area = DispatchModel::group_telegram($area);
      $get_area = DispatchModel::getGroupTelegramProv();
      $getTeamMatrix = DispatchModel::produktifitastekA($date,$area,$mitra);
      $get_hari_lembur = DB::table('hari_lembur')->where('lembur', $date)->first();
      $data = array();
      $lastRegu = '';
      $dataLastUpdate = array();
      foreach ($getTeamMatrix as $team) { 
      $getLastUpdate = DispatchModel::lastupdate($team->id_regu);
      if (count($getLastUpdate)>0){
        $dataLastUpdate[$team->id_regu] = $getLastUpdate;
      } else {
        $dataLastUpdate[$team->id_regu] = "Starting Progress at 09.00 AM";
      }
      } 
      $layout = 'layout';
      return view('dispatch.produktifitastek2',compact('layout','dataLastUpdate','get_hari_lembur','date','konstanta','area','get_area','getTeamMatrix','area'));
    }

    public function produktifitassektor($date,$mode){
      date_default_timezone_set('Asia/Makassar');
      $getTeamMatrix = DispatchModel::produktifitassektor($date,$mode);
      $layout = 'layout';
      return view('dispatch.produktifitassektor',compact('getTeamMatrix','date','layout'));
    }
    public function produktifitassektorlist($date,$sektor,$mode){
      date_default_timezone_set('Asia/Makassar');
      $get_produktifitasteklist = DispatchModel::produktifitassektorlist($date,$sektor,$mode);
      $title = "LIST ORDER ".$sektor." ".$date." ".$mode;
      $layout = 'layout';
      return view('dispatch.produktifitasteklist2',compact('get_produktifitasteklist','sektor','mode','date','layout','title'));
    }


    public function produktifitastek_public($date,$area,$mitra){

    date_default_timezone_set('Asia/Makassar');
    // $get_area = DispatchModel::group_telegram($area);
    $get_area = DispatchModel::getGroupTelegramProv();
    $getTeamMatrix = DispatchModel::produktifitastek3($date,$area,$mitra);
    $get_hari_lembur = DB::table('hari_lembur')->where('lembur', $date)->first();
    $data = array();
    $lastRegu = '';
    $dataLastUpdate = array();
    foreach ($getTeamMatrix as $team) { 
    $getLastUpdate = DispatchModel::lastupdate($team->id_regu);
    if (count($getLastUpdate)>0){
      $dataLastUpdate[$team->id_regu] = $getLastUpdate;
    } else {
      $dataLastUpdate[$team->id_regu] = "Starting Progress at 09.00 AM";
    }
    }
    $layout = "public_layout_2"; 
    return view('dispatch.produktifitastek2',compact('layout','dataLastUpdate','get_hari_lembur','date','konstanta','area','get_area','getTeamMatrix','area'));
  }

  public function produktifitasteklist($date,$id_regu,$status){
    $title = "LIST ORDER ".$status." ".$date;
    $get_produktifitasteklist = DispatchModel::produktifitasteklist($date,$id_regu,$status);
    return view('dispatch.produktifitasteklist',compact('get_produktifitasteklist','title','date','id_regu','status'));
  }

  public function produktifitas($date,$area){
    if (strlen($date)<8) {
      if (date('Y-m')==$date) {
        if (date('d')<=22){
          $konstanta = date('d');
        } else {
          $konstanta = 22;
        }
      } else {
        $konstanta = 22;
      }
    } else {
      $konstanta = 1;
    }
    date_default_timezone_set('Asia/Makassar');
    $get_area = DispatchModel::group_telegram($area);
    $getTeamMatrix = DispatchModel::getTeamMatrix1($date,$area);
    $data = array();
    $lastRegu = '';

    return view('dispatch.produktifitas',compact('get_area','getTeamMatrix','area','konstanta'));
  }

  public function timeslot($date,$area){
    date_default_timezone_set('Asia/Makassar');
    $timeslot = DB::SELECT('
      SELECT * FROM dispatch_teknisi_timeslot
    ');
    $dispatch_timeslot = DB::SELECT('
      SELECT
        a.*,
        b.*,
        e.*,
        c.status_laporan,
        c.catatan,
        c.modified_at,
        c.created_at,
        d.uraian,
        d.id_regu,
        e.orderName,
        g.laporan_status,
        c.id_tbl_mj,
        f.title as area
      FROM
        dispatch_teknisi a
      LEFT JOIN dispatch_teknisi_timeslot b ON a.updated_at_timeslot = b.dispatch_teknisi_timeslot_id
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN regu d ON a.id_regu = d.id_regu
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN roc ee ON a.Ndem = ee.no_tiket
      LEFT JOIN group_telegram f ON d.mainsector = f.chat_id
      LEFT JOIN psb_laporan_status g ON c.status_laporan = g.laporan_status_id
      WHERE
        a.tgl LIKE "'.$date.'%"
      ORDER BY d.id_regu,f.title
    ');
    $data       = array();
    $lastRegu   = '';
    $lastArea   = '';
    $listTim    = array();

    foreach ($dispatch_timeslot as $data_timeslot) :
        $timeCount = 0;
        $timetoCount = '';
        if ($data_timeslot->status_laporan==5){
          if (empty($data_timeslot->modified_at))
          $timetoCount = $data_timeslot->created_at;
          else
          $timetoCount = $data_timeslot->modified_at;
          $timeCount = $this->timeCount($timetoCount);
        }
        if (empty($data_timeslot->modified_at))
        $timetoCount = $data_timeslot->created_at;
        else
        $timetoCount = $data_timeslot->modified_at;
        if ($data_timeslot->status_laporan==6 || empty($data_timeslot->status_laporan)){

          $timeslot_h = new DateTime(date('Y-m-d ').$data_timeslot->dispatch_teknisi_timestart);
          $current = new DateTime(date('Y-m-d H:i:s'));
          if ($current>$timeslot_h)
          $timeCount = $this->timeCount(date('Y-m-d ').$data_timeslot->dispatch_teknisi_timestart);
        }
        $manja = "<b>Keterangan Manja</b> : <br />".html_entity_decode($data_timeslot->manja)."<br /><b>Status Teknisi</b> : <br />$data_timeslot->laporan_status / <small>$timetoCount</small><br /><b>Catatan Teknisi</b> : <br />".$data_timeslot->catatan;
        $manja .= "<br /><a class='label btn-info' href='/$data_timeslot->id_tbl_mj'>Detail</a>";
        if ($lastRegu <> $data_timeslot->id_regu) {
          if ($data_timeslot->dispatch_teknisi_timeslot_id <> NULL){
            $data[$data_timeslot->area][$data_timeslot->id_regu][$data_timeslot->dispatch_teknisi_timeslot_id][] = array("manja"=>$manja,"orderName"=>$data_timeslot->orderName,"timeCount" => $timeCount,"SC" => $data_timeslot->Ndem, "status_laporan" => $data_timeslot->status_laporan);
          }
        } else {
          $data[$data_timeslot->area][$data_timeslot->id_regu][$data_timeslot->dispatch_teknisi_timeslot_id] = array("manja"=>$manja,"orderName"=>$data_timeslot->orderName,"timeCount" => $timeCount, "SC" => $data_timeslot->Ndem, "status_laporan" => $data_timeslot->status_laporan);
          $lastRegu = $data_timeslot->id_regu;
        }
    endforeach;

    $listTim = DB::SELECT('
      SELECT
        a.id_regu,
        b.uraian,
        e.title as area,
        count(*) as jumlah_wo,
        SUM(CASE WHEN a.manja_status = 1 THEN 1 ELSE 0 END) as jumlah_manja,
        SUM(CASE WHEN a.manja_status = 2 THEN 1 ELSE 0 END) as jumlah_manja_nok
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN roc cc ON a.Ndem = cc.no_tiket
      LEFT JOIN mdf d ON c.sto = d.mdf
      LEFT JOIN group_telegram e ON b.mainsector = e.chat_id
      WHERE
        a.tgl LIKE "'.$date.'%"
      GROUP BY b.id_regu
    ');

    if ($area=="ALL") {
      $get_where_area = "";
    } else {
      $get_where_area = "AND a.area = '".$area."'";
    }
    $get_area = DB::SELECT('
      SELECT
        a.title as area,
        a.title as DESKRIPSI
      FROM
        group_telegram a

      GROUP BY a.title
    ');

    // print_r($data);

    return view('dispatch.timeslot',compact('timeslot','data','listTim','get_area'));
  }

  public function timeCount($startDatex){
    date_default_timezone_set('Asia/Makassar');

    // $endDate = date("Y-m-d H:i:s");
    // $result = round(abs($endDate - $startDate) / 60,2);
    $startDate = new DateTime($startDatex);
    $endDate = new DateTime();
    $since_start = $startDate->diff($endDate);
    if ($since_start->h>0) $h = $since_start->h."h "; else $h = '';
    if ($since_start->i>0) $i = $since_start->i."min "; else $i = '';
    $result = $h. $since_start->i."min";
    // return $endDate->format('Y-m-d H:i:s')."<br />".$startDatex."<br /> ".$result;

    return $result;
  }

  public function reportPsb2($tgl)
  {
  if (empty($tgl)){
    $tgl = date('Y-m-d');
  }

    $auth = session('auth');

    $get_list = DB::select('
      SELECT
        dps.*,
        i.*,
        emp.nama as HD,
        d.id as id_dt,
        d.updated_at,
        d.updated_at_timeslot,
        j.uraian,
        i.status_laporan,
        ii.laporan_status as status_wo_by_hd,
        ii.laporan_status as Sebab,
        i.catatan as Ket_Sebab,
        dps.orderId,
        cc.area as area,
        dps.orderNcli as Ncli,
        dps.ndemSpeedy as ND_Speedy,
        dps.orderName as Nama,
        (select count(*) from psb_laporan_material plm where plm.psb_laporan_id = i.id) as jumlah_material,
        cc.mdf,
        k.dispatch_teknisi_timeslot
      FROM
        dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick dps ON d.Ndem = dps.orderId
      LEFT JOIN mdf cc ON dps.sto = cc.mdf
      LEFT JOIN psb_laporan i ON d.id = i.id_tbl_mj
      LEFT JOIN psb_laporan_status ii ON i.status_laporan = ii.laporan_status_id
      LEFT JOIN regu j ON d.id_regu = j.id_regu
      LEFT JOIN dispatch_teknisi_timeslot k ON d.updated_at_timeslot = k.dispatch_teknisi_timeslot_id
      LEFT JOIN 1_2_employee emp ON d.updated_by = emp.nik
        WHERE
        d.tgl like "'.$tgl.'%" and
        j.id_regu is not null
        ORDER BY j.id_regu
    ');
    $get_report = DB::select('
    SELECT
SUM(
          CASE WHEN i.status_laporan = 6 OR i.status_laporan is NULL
          THEN 1
          ELSE 0
          END ) AS BELUM_SURVEY,
          SUM(
          CASE WHEN i.status_laporan = 8
          THEN 1
          ELSE 0
          END ) AS SURVEY_OK,

          SUM(
          CASE WHEN i.status_laporan = 2
          THEN 1
          ELSE 0
          END ) AS ODP_LOS,

          SUM(
          CASE WHEN i.status_laporan = 9
          THEN 1
          ELSE 0
          END ) AS FOLLOW_UP_SALES,
          SUM(
          CASE WHEN i.status_laporan = 5
          THEN 1
          ELSE 0
          END ) AS OGP,
          SUM(
          CASE WHEN i.status_laporan =3
          THEN 1
          ELSE 0
          END ) AS KEND_PELANGGAN,

          SUM(
          CASE WHEN i.status_laporan =10
          THEN 1
          ELSE 0
          END ) AS PROGRESS_PT23,


          SUM(
          CASE WHEN i.status_laporan =12
          THEN 1
          ELSE 0
          END ) AS UP_DG_SC_LAIN,

          SUM(
          CASE WHEN i.status_laporan =13
          THEN 1
          ELSE 0
          END ) AS INPUT_ULANG,

          SUM(
          CASE WHEN i.status_laporan =14
          THEN 1
          ELSE 0
          END ) AS ANOMALI,

          SUM(
          CASE WHEN i.status_laporan =15
          THEN 1
          ELSE 0
          END ) AS KEND_SISTEM,


          SUM(
          CASE WHEN i.status_laporan =16
          THEN 1
          ELSE 0
          END ) AS PERMINTAAN_BATAL,


          SUM(
          CASE WHEN i.status_laporan =11
          THEN 1
          ELSE 0
          END ) AS INSERT_TIANG,
          SUM(
          CASE WHEN i.status_laporan =4
          THEN 1
          ELSE 0
          END ) AS HR,
          SUM(
          CASE WHEN i.status_laporan = 1
          THEN 1
          ELSE 0
          END ) AS UP,
          SUM(
          CASE WHEN i.status_laporan = 7
          THEN 1
          ELSE 0
          END ) AS RESCHEDULE,
      count(*) as jumlah,
      cc.area
      FROM
        dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick dps ON d.Ndem = dps.orderId
      LEFT JOIN ms2n b ON d.Ndem = b.Ndem
      LEFT JOIN Data_Pelanggan_Starclick a ON a.orderNcli = b.Ncli
      LEFT JOIN mdf c ON b.mdf = c.mdf
      LEFT JOIN mdf cc ON cc.mdf = dps.sto
      LEFT JOIN psb_laporan i ON d.id = i.id_tbl_mj
      LEFT JOIN ms2n_sebab e ON e.ms2n_sebab = b.Sebab
      LEFT JOIN ms2n_status_hd h on b.Ndem = h.Ndem
      LEFT JOIN regu j ON d.id_regu = j.id_regu
      LEFT JOIN dispatch_teknisi_timeslot k ON d.updated_at_timeslot = k.dispatch_teknisi_timeslot_id
        WHERE
        d.tgl like "'.$tgl.'%" AND
        j.id_regu is not null AND
        cc.area is not null
        GROUP BY cc.area,c.area

    ');

    return view('dispatch.report2', compact('get_list','get_report'));
  }

  public function searchform(){
    $data = "";
    return view('dispatch.searchform',compact('data'));
  }

  public function search(Request $request){
    $this->validate($request,[
        'Search' => 'numeric',
    ]);

    $id     = $request->input('Search');
    if ($id==""){ $list = NULL; } else {
      $list   = DispatchModel::WorkOrder('SEARCH',$id);
    };

    // if (!empty($list)){
    //     $list = DispatchModel::QueryBuilderTambahan($list[0]->orderKontak);
    // };

    // dd($list);
    $data   = "Trying to search '".$id."'";
    $jumlah = count($list);

    $dataUnsc = dispatchModel::getUnscByUnsc($id);
    return view('dispatch.searchform',compact('data','list','jumlah','dataUnsc','id'));
  }

  public function reportCFC()
  {
    $auth = session('auth');

    $get_list = DB::select('
      SELECT
        dps.*,
        b.*,
        e.*,
        i.*,
        d.id as id_dt,
        j.uraian,
        i.status_laporan,
        case i.status_laporan
          when "1" then "UP"
          when "2" then "KENDALA TEKNIS"
          when "3" then "KENDALA PELANGGAN"
          when "4" then "HR"
          when "5" then "OGP"
        else "BELUM SURVEY"
        end as status_wo_by_hd,
        dps.orderId,
        c.area,
        cc.area as areaSC,
        (select count(*) from psb_laporan_material plm where plm.psb_laporan_id = i.id) as jumlah_material,
        b.mdf,
        a.orderId as orderIdMS2N,
        a.jenisPsb as jenisPsbMS2N
      FROM
        dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick dps ON d.Ndem = dps.orderId
      LEFT JOIN ms2n b ON d.Ndem = b.Ndem
      LEFT JOIN Data_Pelanggan_Starclick a ON a.orderNcli = b.Ncli
      LEFT JOIN mdf c ON b.mdf = c.mdf
      LEFT JOIN mdf cc ON cc.mdf = dps.sto
      LEFT JOIN psb_laporan i ON d.id = i.id_tbl_mj
      LEFT JOIN ms2n_sebab e ON e.ms2n_sebab = b.Sebab
      LEFT JOIN ms2n_status_hd h on b.Ndem = h.Ndem
      LEFT JOIN regu j ON d.id_regu = j.id_regu
        WHERE
        d.updated_at like "'.date('Y-m-d').'%" and
        j.id_regu is not null AND
        (dps.kcontact LIKE "%(CFC)%" OR a.kcontact LIKE "%(CFC)%") AND
        j.uraian NOT LIKE "%SHVA LAN%"
        GROUP BY b.ND_Speedy,dps.ndemSpeedy
        ORDER BY j.id_regu
    ');
    $get_report = DB::select('
    SELECT
        case i.status_laporan
          when "1" then "UP"
          when "2" then "KENDALA TEKNIS"
          when "3" then "KENDALA PELANGGAN"
          when "4" then "HR"
          when "5" then "OGP"
        else "BELUM SURVEY"
        end as status_wo_by_hd,
      count(*) as jumlah
      FROM
        dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick dps ON d.Ndem = dps.orderId
      LEFT JOIN ms2n b ON d.Ndem = b.Ndem
      LEFT JOIN Data_Pelanggan_Starclick a ON a.orderNcli = b.Ncli
      LEFT JOIN mdf c ON b.mdf = c.mdf
      LEFT JOIN mdf cc ON cc.mdf = dps.sto
      LEFT JOIN psb_laporan i ON d.id = i.id_tbl_mj
      LEFT JOIN ms2n_sebab e ON e.ms2n_sebab = b.Sebab
      LEFT JOIN ms2n_status_hd h on b.Ndem = h.Ndem
      LEFT JOIN regu j ON d.id_regu = j.id_regu
        WHERE
        d.updated_at like "2016-07-25%" AND
        (dps.kcontact LIKE "%(CFC)%" OR a.kcontact LIKE "%(CFC)%") AND
        j.id_regu is not null
        GROUP BY i.status_laporan
        ORDER BY j.id_regu

    ');

    return view('dispatch.report2', compact('get_list'));
  }

  public function reportPsb()
  {
    $auth = session('auth');
    $list = DB::select('
      SELECT ms2n.*, id_regu AS id_r, mdf.area,(
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian, Status_Indihome, status_laporan, ms2n_sebab_value, ms2n_sebab_pic,
      case psb_laporan.status_laporan
        when "1" then "UP"
        when "2" then "ODP LOS"
        when "11" then "INSERT TIANG"
        when "10" then "PROGRESS PT23"
        when "3" then "KENDALA PELANGGAN"
        when "4" then "HR"
        when "5" then "OGP"
        when "7" then "RESCHEDULE"
        when "6" then "BELUM SURVEY"
        when "8" then "SURVEY OK"
        when "9" then "FOLLOW UP SALES"
        when "12" then "UP DG SC LAIN"
        when "13" then "INPUT ULANG"
        when "14" then "ANOMALI"
        when "15" then "KENDALA SISTEM"
        when "16" then "PERMINTAAN BATAL"
        else "BELUM SURVEY"
      end as status_wo_by_hd,
      dispatch_teknisi.id as id_dt,
      (select count(*) from psb_laporan_material plm where plm.psb_laporan_id = psb_laporan.id) as jumlah_material

      FROM ms2n
        LEFT JOIN dispatch_teknisi ON ms2n.Ndem = dispatch_teknisi.Ndem
        LEFT JOIN psb_laporan on dispatch_teknisi.id=psb_laporan.id_tbl_mj
        LEFT JOIN mdf on ms2n.MDF = mdf.mdf
        LEFT join ms2n_sebab on ms2n.Sebab = ms2n_sebab.ms2n_sebab
        LEFT JOIN ms2n_status_hd h on ms2n.Ndem = h.ndem
      WHERE  dispatch_teknisi.updated_at like "'.date('Y-m-d').'%" and id_regu is not null order by ms2n.mdf
    ');

    $countWo = count($list);
    $countOuter = 0;
    $countInner = 0;
    $outer = array();
    $lastArea = '';
    foreach($list as $row) {

      if($row->area != 'BJM'){
        if ($lastArea == $row->area) {
          $outer[count($outer)-1]['WO'][] = array('ND_Speedy'=>$row->ND_Speedy,'ID_DT'=> $row->id_dt,  'STATUS' => $row->status_wo_by_hd ,'NDEM' => $row->Ndem, 'detil'=>$row->ND.' ~ '.$row->ND_Speedy.' : '.$row->Nama);
        }
        else {
          if($row->ms2n_sebab_value <> 'Kendala'){
          $outer[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->ND_Speedy, 'ID_DT'=> $row->id_dt, 'STATUS' => $row->status_wo_by_hd, 'NDEM'=>$row->Ndem, 'detil' => $row->ND.' ~ '.$row->ND_Speedy.' : '.$row->Nama)));
          $lastArea = $row->area;
          }
        }
        $countOuter++;
      }
    }
    usort($list, function($x, $y) {
      return strcasecmp($x->uraian , $y->uraian);
    });
    $tim = array();
    $lastTim = '';
    if($auth->id_user =='wandiy99'){
      //return $list;
    }
    $i=0;
    foreach($list as $row) {
      if ($row->jumlah_material>0 && $row->status_wo_by_hd=='UP') {
        $status_material = "MATERIAL OK";
      } else {
        $status_material = "";
      }
      if($row->area == 'BJM'){
        if ($lastTim == $row->uraian) {
          $tim[count($tim)-1]['WO'][] = array('ND_Speedy'=>$row->ND_Speedy, 'ID_DT'=> $row->id_dt, 'MATERIAL' => $status_material,'STATUS' => $row->status_wo_by_hd ,'NDEM' => $row->Ndem, 'detil'=>$row->ND.' ~ '.$row->ND_Speedy.' : '.$row->Nama);
          //$tim[count($tim)-1]['WO'][]['ND'] = $row->ND;
        }
        else {
          $tim[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->ND_Speedy,'ID_DT'=> $row->id_dt, 'MATERIAL' => $status_material, 'STATUS' => $row->status_wo_by_hd, 'NDEM'=>$row->Ndem, 'detil' => $row->ND.' ~ '.$row->ND_Speedy.' : '.$row->Nama)));
//           $tim[] = array('head' => $row->uraian, 'WO' => array( 'ND' => $row->ND));
          $lastTim = $row->uraian;
        }
        $countInner++;
      }
    }
    usort($list, function($x, $y) {
      return strcasecmp($x->ms2n_sebab_pic , $y->ms2n_sebab_pic);
    });
    $kendala = array();
    $lastKendala = '';
    $bs = 0;
    $up = 0;
    $ogp = 0;
    $kdl = 0;
    foreach($list as $row) {
      if($row->ms2n_sebab_value == 'Kendala'){
        if ($lastKendala == $row->ms2n_sebab_pic) {
          $kendala[count($kendala)-1]['WO'][] = $row->ND.' : '.$row->Nama.' ('.$row->Sebab.')';
        }
        else {
          $kendala[] = array('head' => $row->ms2n_sebab_pic, 'WO' => array( $row->ND.' : '.$row->Nama.' ('.$row->Sebab.')'));
          $lastKendala = $row->ms2n_sebab_pic;
        }

      }
      if($row->status_wo_by_hd == 'KENDALA'){
        $kdl++;
      }if($row->status_wo_by_hd == 'BELUM SURVEY'){
        $bs++;
      }if($row->status_wo_by_hd == 'OGP'){
        $ogp++;
      }if($row->status_wo_by_hd == 'UP'){
        $up++;
      }
    }
    $counting = array('KENDALA' => $kdl, 'BELUM SURVEY' => $bs, 'OGP' => $ogp, 'UP' => $up, 'INNER' => $countInner, 'OUTER' => $countOuter, 'WO' => $countWo);
    //return $kendala;
    //return ($outer);
    return view('dispatch.report', compact('tim', 'outer', 'kendala', 'counting'));
  }
  private function array_remval($val, &$arr)
    {
          $array_remval = $arr;
          for($x=0;$x<count($array_remval);$x++)
          {
              $i=array_search($val,$array_remval);
              if (is_numeric($i)) {
                  $array_temp  = array_slice($array_remval, 0, $i );
                $array_temp2 = array_slice($array_remval, $i+1, count($array_remval)-1 );
                $array_remval = array_merge($array_temp, $array_temp2);
              }
          }

          return $array_remval;
    }
  public function input($id)
  {
    $ketDispatch = '';
    $exists = DB::select('
      SELECT dispatch_teknisi.*,dispatch_teknisi.kpro_timid,dispatch_teknisi.manja_status,a.*,id_regu,id,tgl,manja,a.orderName as Nama, c.*
      FROM Data_Pelanggan_Starclick a
      LEFT JOIN dispatch_teknisi ON a.orderId = dispatch_teknisi.Ndem
      LEFT JOIN dispatch_teknisi_timeslot c ON dispatch_teknisi.updated_at_timeslot = c.dispatch_teknisi_timeslot_id
      WHERE a.orderId = ?
    ',[
      $id
    ]);
    if (count($exists)==0){
      $exists = DB::select('
        SELECT dispatch_teknisi.*,dispatch_teknisi.kpro_timid,dispatch_teknisi.manja_status, dispatch_teknisi.manja, dispatch_teknisi.updated_at_timeslot,a.orderName as Nama,id_regu,id,tgl
        FROM Data_Pelanggan_Starclick a left join dispatch_teknisi
        on a.orderId=dispatch_teknisi.Ndem
        WHERE a.orderId = ?
      ',[
        $id
      ]);
    }
    if (count($exists)==0){
      $exists = DB::select('
        SELECT
        b.*,
         b.kpro_timid,
        b.manja_status,b.manja,b.updated_at_timeslot,a.NAMA as Nama,b.id_regu,b.id,b.tgl
        FROM dossier_master a left join dispatch_teknisi b
        on a.ND=b.Ndem
        WHERE a.ND = ?
      ',[
        $id
      ]);

      $ketDispatch = 'migrasi';
    }
    $data = $exists[0];
    //dd($data);

    $regu = DB::select('
      SELECT id_regu as id,uraian as text, telp, job
      FROM regu
      WHERE
      ACTIVE = 1
    ');
    $timeslot = DB::select('
      SELECT *,dispatch_teknisi_timeslot_id as id, dispatch_teknisi_timeslot as text FROM dispatch_teknisi_timeslot
    ');
    $kpro_tim = DB::select('
      SELECT *, timid as id, nama_tim as text FROM kpro_tim
    ');
    $get_manja_status = DB::SELECT('
      SELECT
        a.manja_status_id as id,
        a.manja_status as text
      FROM
        manja_status a
    ');

    $dataUnscc = dispatchModel::getUnscByUnsc($id);
    if (!empty($dataUnscc)){
        $dataUnsc = $dataUnscc;
    }
    else{
        $dataUnsc = [];
    }

    return view('dispatch.input', compact('data', 'regu', 'timeslot','get_manja_status', 'kpro_tim', 'dataUnsc', 'ketDispatch'));

  }

  public function save(Request $request, $id)
  {
    date_default_timezone_set('Asia/Makassar');
    echo "Trial Integrasi K-Pro<br />";

    // validasi
    $this->validate($request,[
        'jenis_layanan' => 'required',
        'id_regu'       => 'required',
        'tgl'           => 'required',
        'kordinatPel'   => 'required',
        'alamatSales'   => 'required'
    ]);

    if ($request->ketDispatch=="migrasi"){
        $this->validate($request,[
            'ketOrder'  => 'required'
        ],
        [
            'ketOrder.required' => 'Harus Diisi Jika WO Migrasi',
        ]);
    }

    $auth = session('auth');
    if ($request->input('manja')<>""){
      $manja = $request->input('manja');
    } else {
      $manja = 'EMPTY';
    };

    $exists = DB::select('
      SELECT a.*,b.status_laporan, c.mainsector
        FROM dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN regu c ON a.id_regu = c.id_regu
      WHERE a.Ndem = ?
    ',[
      $id
    ]);

    // insert into log status
    $status_laporan = 6;
    if (count($exists)>0){
      if ($exists[0]->status_laporan==4){
        $status_laporan = 4;
      }
    };
    
    DB::transaction(function() use($request, $id, $auth, $status_laporan) {
       DB::table('psb_laporan_log')->insert([
         'created_at'      => DB::raw('NOW()'),
         'created_by'      => $auth->id_karyawan,
         'status_laporan'  => $status_laporan,
         'id_regu'         => $request->input('id_regu'),
         'catatan'         => "DISPATCH at ".date('Y-m-d'),
         'Ndem'            => $id
       ]);
     });
     DB::transaction(function() use($request, $id, $auth) {
       DB::table('dispatch_teknisi_log')->insert([
         'updated_at'          => DB::raw('NOW()'),
         'updated_by'          => $auth->id_karyawan,
         'tgl'                 => $request->input('tgl'),
         'id_regu'              => $request->input('id_regu'),
         'manja'               => $request->input('manja'),
         'manja_status'        => $request->input('manja_status'),
         'updated_at_timeslot' => $request->input('timeslot'),
         'Ndem'                => $id
       ]);
     });
    // reset status if redispatch
    if (count($exists)) {
      $data = $exists[0];
      DB::transaction(function() use($request, $data, $auth, $id,$status_laporan) {
        DB::table('psb_laporan')
          ->where('id_tbl_mj', $data->id)
          ->update([
            'status_laporan' => $status_laporan
          ]);
          DB::table('psb_laporan_log')->insert([
             'created_at'      => DB::raw('NOW()'),
             'created_by'      => $auth->id_karyawan,
             'status_laporan'  => $status_laporan,
             'id_regu'         => $request->input('id_regu'),
             'catatan'         => "REDISPATCH",
             'Ndem'            => $id,
             'psb_laporan_id'  => $data->id
           ]);
      });
      DB::transaction(function() use($request, $data, $auth, $id) {
        DB::table('dispatch_teknisi')
          ->where('id', $data->id)
          ->update([
            'updated_at'            => DB::raw('NOW()'),
            'updated_by'            => $auth->id_karyawan,
            'tgl'                   => $request->input('tgl'),
            'manja'                 => $request->input('manja'),
            'manja_status'          => $request->input('manja_status'),
            'updated_at_timeslot'   => $request->input('timeslot'),
            'id_regu'               => $request->input('id_regu'),
            'jenis_layanan'         => $request->input('jenis_layanan'),
            'kpro_timid'            => $request->input('kpro_timid'),
            'step_id'               => "1.0",
            'kordinatPel'           => $request->input('kordinatPel'),
            'alamatSales'           => $request->alamatSales,
            'ketOrder'            => $request->input('ketOrder')
          ]);
      });
    }
    else {
      DB::transaction(function() use($request, $id, $auth) {
        DB::table('dispatch_teknisi')->insert([
          'updated_at'          => DB::raw('NOW()'),
          'updated_by'          => $auth->id_karyawan,
          'tgl'                 => $request->input('tgl'),
          'id_regu'             => $request->input('id_regu'),
          'manja'               => $request->input('manja'),
          'created_at'          => DB::raw('NOW()'),
          'manja_status'        => $request->input('manja_status'),
          'updated_at_timeslot' => $request->input('timeslot'),
          'jenis_layanan'       => $request->input('jenis_layanan'),
          'Ndem'                => $id,
          'kpro_timid'          => $request->input('kpro_timid'),
          'step_id'             => "1.0",
          'kordinatPel'         => $request->kordinatPel,
          'alamatSales'         => $request->alamatSales,
          'ketOrder'            => $request->input('ketOrder')
        ]);
      });
      echo "Sukses";
    }

    // update data_pelanggan_startclick ketDis
    DB::table('Data_Pelanggan_Starclick')->where('orderId',$id)->update(['ketDis' => '1']);

    if ($manja<>"EMPTY"){
        exec('cd ..;php artisan sendTelegramSSC1 '.$id.' '.$auth->id_user.' > /dev/null &');
    }

    // redirect
    // return redirect('/dispatch/reportPsb2/'.date('Y-m-d'))->with('alerts', [
    return redirect('/dispatch/search')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> men-dispatch']
    ]);
  }

  public function starclick(){
    ini_set('xdebug.var_display_max_depth', 5);
    ini_set('xdebug.var_display_max_children', 1024);
    ini_set('xdebug.var_display_max_data', 1024);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://starclick.telkom.co.id/backend/public/user/authenticate');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $fields = array(
        'username'=> "84153707",
        'password'=> "telkom135"
    );
    $fields_string = http_build_query($fields);
    echo $fields_string;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $result = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($result, 0, $header['header_size']);
    $request_content = substr($result, 0, $header['http_code']);
    var_dump($header_content);
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0];
    var_dump($header_content);
    echo "<br />";
    curl_setopt($ch, CURLOPT_URL, 'https://starclick.telkom.co.id/backend/public/order/load-inbox');
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    curl_setopt($ch, CURLOPT_POST, true);
    $fields_string1 = "draw=1&columns%5B0%5D%5Bdata%5D=IDENTITY&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=ORDER_DTM&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=NAME&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=ALPRONAME&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=TNNUMBER&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=STATUSNAME&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=XS7&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=APPOINTMENTTIME&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=XN6&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&columns%5B8%5D%5Borderable%5D=true&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=ACTION&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=false&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false";
    echo $fields_string1;
    echo "<br />";
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string1);
    $result = curl_exec($ch);
    print_r($result);

  }

  //update from console : php artisan kpro id regu
  public static function update_kpro($id, $jenis){
    try{
      echo "Order Id : ".$id."<br />";
      $url = "http://prov.rockal.id:7777/";
      //$url = "http://prov.rockal.id";
      $number = 1;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url.'/login.php');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      $fields = array(
          'usr'=> "97158960",
          'pwd'=> "telkomakses",
          'b_submit' => ""
      );
      $fields_string = http_build_query($fields);
      echo $fields_string;
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      $result = curl_exec($ch);
      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);
      $header_content = substr($result, 0, $header['header_size']);
      $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      preg_match_all ($pattern, $header_content, $matches);
      $cookiesOut = $matches['cookie'][0];
      echo "<br />";
      $query = DB::select('
        SELECT
          *
        FROM dispatch_teknisi a
        LEFT JOIN dispatch_teknisi_timeslot b ON a.updated_at_timeslot = b.dispatch_teknisi_timeslot_id
        LEFT JOIN regu c ON a.id_regu = c.id_Regu
        LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
        LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
        LEFT JOIN regu f ON a.id_regu = f.id_regu
        WHERE ndem = "'.$id.'"
      ')[0];
      if($jenis == "Manja"){
        $manja_content = $query->manja_status." | ".$query->tgl." | ".$query->uraian. " | ".$query->manja;
        //manja ok
        if ($query->manja_status==1){
          $tl = "Management Janji";
        }
        //manja no ok
        else if ($query->manja_status==2){
          $tl = "Kendala Pelanggan";
        }
      }else{
        $manja_content = "[".$query->laporan_status."]
                           ".$query->uraian." |
                           ".$query->catatan." |
                           Koor.Pel : ".$query->kordinat_pelanggan. " |
                           Koor.ODP : ".$query->kordinat_odp;
        $tl = $query->status_kpro;
      }
      if($tl){
        // Telegram::sendMessage(["chat_id" => 52369916,
        //   "text" => "update kpro!!"
        // ]);
        //do manja/kendala kpro
        curl_setopt($ch, CURLOPT_URL, $url.'/ajax_tindaklanjut.php?jenis=savetdkljt');
        curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
        curl_setopt($ch, CURLOPT_POST, true);
        $fields = array(
            'order_id'=> $id,
            'pilih_tl'=> $tl,
            'ket_tdk_ljt' => $manja_content
        );
        $fields_string = http_build_query($fields);
        echo $fields_string;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result1 = curl_exec($ch);
        curl_close($ch);
        // Telegram::sendMessage(["chat_id" => 52369916,
        //   "text" => $result1
        // ]);
        echo "<br />";
      }
    }catch (\Exception $e) {
      // Telegram::sendMessage(["chat_id" => 52369916,
      //           "text" => $e->getMessage()
      //       ]);
    }

  }
  public static function assign_kpro($id){
    try{
      echo "Order Id : ".$id."<br />";
      $url = "http://prov.rockal.id:7777/";
      //$url = "http://prov.rockal.id";
      $number = 1;
      $query = DB::select('
        SELECT
          *
        FROM dispatch_teknisi a
        LEFT JOIN dispatch_teknisi_timeslot b ON a.updated_at_timeslot = b.dispatch_teknisi_timeslot_id
        LEFT JOIN regu c ON a.id_regu = c.id_Regu
        WHERE ndem = "'.$id.'"
      ')[0];
        $manja_content = $query->manja_status." | ".$query->tgl." | ".$query->uraian." | ".$query->manja;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url.'/login.php');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      $fields = array(
          'usr'=> "97158960",
          'pwd'=> "telkomakses",
          'b_submit' => ""
      );
      $fields_string = http_build_query($fields);
      echo $fields_string;
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      $result = curl_exec($ch);
      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);
      $header_content = substr($result, 0, $header['header_size']);
      $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      preg_match_all ($pattern, $header_content, $matches);
      $cookiesOut = $matches['cookie'][0];
      echo "<br />";
      //do assign kpro
      curl_setopt($ch, CURLOPT_URL, $url.'/ajax_timeschedule.php?jenis=saveassign');
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      curl_setopt($ch, CURLOPT_POST, true);
      //manja ok
      $fields = array(
          'pilih_order'=> $id,
          'asg_timid'=> $query->kpro_timid,
          'asg_slot' => $query->timeslot_kpro,
          'asg_tgl' => $query->tgl
      );
      $fields_string = http_build_query($fields);
      echo $fields_string;
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result1 = curl_exec($ch);
      curl_close($ch);
      // Telegram::sendMessage(["chat_id" => 52369916,
      //     "text" => $result1
      // ]);
      echo "<br />";
    }catch (\Exception $e) {
      // Telegram::sendMessage(["chat_id" => 52369916,
      //     "text" => $e->getMessage()
      // ]);
    }

  }
  public function kpro($id,$regu,$jenis,$text){
    echo "Order Id : ".$id."<br />";
    $url = "http://prov.rockal.id:7777/";
    // $url = "http://prov.rockal.id";
    $number = 1;
    $query = DB::select('
      SELECT
        *
      FROM karyawan
      WHERE id_regu = "'.$regu.'"
    ');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url.'/login.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $fields = array(
        'usr'=> "97158960",
        'pwd'=> "telkomakses",
        'b_submit' => ""
    );
    $fields_string = http_build_query($fields);
    echo $fields_string;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $result = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($result, 0, $header['header_size']);
    var_dump($header_content);
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0];
    // print_r($header['header_size']);
    echo "<br />";
    if ($jenis=="dispatch") {
      foreach ($query as $result){
        if ($result->idkpro<>0) {
          curl_setopt($ch, CURLOPT_URL, $url.'/ajax_teknisi.php?jenis=savetek');
          curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
          curl_setopt($ch, CURLOPT_POST, true);

          $fields = array(
              'tek'=> $number,
              'order_id'=> $id,
              'daftartech' => $result->idkpro
          );
          $fields_string = http_build_query($fields);
          echo $fields_string;
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $result1 = curl_exec($ch);
          print_r($result1);
          echo "Tek ".$number." | ".$result->idkpro." | ".$result->id_karyawan."<br />";
          echo "<br />";

          $number++;
        }
      }
    } else if ($jenis=="manja"){
      curl_setopt($ch, CURLOPT_URL, $url.'/ajax_tindaklanjut.php?jenis=savetdkljt');
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      curl_setopt($ch, CURLOPT_POST, true);
      if ($regu==1){
        $tl = "Management Janji";
      } else if ($regu==2){
        $tl = "Kendala Pelanggan";
      }
      $fields = array(
          'order_id'=> $id,
          'pilih_tl'=> $tl,
          'ket_tdk_ljt' => $text
      );
      $fields_string = http_build_query($fields);
      echo $fields_string;
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result1 = curl_exec($ch);
      print_r($result1);
      // echo "Tek ".$number." | ".$result->idkpro." | ".$result->id_karyawan."<br />";
      echo "<br />";
    }

}
public function getTimKpro(){
    $url = "http://prov.rockal.id:7777/";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url.'/login.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $fields = array(
        'usr'=> "97158960",
        'pwd'=> "telkomakses",
        'b_submit' => ""
    );
    $fields_string = http_build_query($fields);
    echo $fields_string;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $result = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($result, 0, $header['header_size']);
    var_dump($header_content);
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0];
    // print_r($header['header_size']);
    echo "<br />";
    curl_setopt($ch, CURLOPT_URL, $url.'/timeschedule.php?tgl='.date("Y-m-d").'&asg=all&witel=KALSEL');
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $result1 = curl_exec($ch);
    curl_close($ch);
    $columns = array(
            0 => 'no',
            'action',
            'timid',
            'nama_tim',
            'sto',
            'delapan',
            'sepuluh',
            'duabelas',
            'empatbelas',
            'enambelas',
            'delapanbelas',
            'tot',
            'reassign'
        );

    $result1 = str_replace("<a class='badge bg-blue' style='margin-top: 3px; margin-bottom: 3px;' onclick='status_tim(","",$result1);

    $result1 = str_replace(")'>Sta<br>Tus</a>","",$result1);

    $dom = @\DOMDocument::loadHTML(trim($result1));

    $table = $dom->getElementsByTagName('tbody')->item(0);

    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 0, $count = $rows->length-1; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        //print_r($cells);
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }

        $result[] = $data;

    }
    $data = $result;
    DB::table('kpro_tim')->truncate();
    DB::table('kpro_tim')->insert($data);
}

  public function destroy($id)
  {
    //$this->decrementStokTeknisi($id);
    // cari filenya
  
    $data = DB::table('dispatch_teknisi')->where('Ndem',$id)->first();

    DB::table('dispatch_teknisi_log')->insert([
       'updated_at'          => DB::raw('NOW()'),
       'updated_by'          => session('auth')->id_karyawan,
       'tgl'                 => date('Y-m-d'),
       'id_regu'             => $data->id_regu,
       'manja'               => 'Dispatch Dihapus',
       'manja_status'        => $data->manja_status,
       'updated_at_timeslot' => $data->updated_at_timeslot,
       'Ndem'                => $id
    ]);

    DB::transaction(function() use($id) {
      DB::table('dispatch_teknisi')
          ->where('Ndem', [$id])->delete();
    });

    return redirect('/dispatch/workorder/READY')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Dispatch']
    ]);
  }

  //status by hd
  public function updateStatus($id)
  {
    $data = DB::select('
      SELECT m.Ndem, Nama, h.status_wo_by_hd, h.id
      FROM ms2n m
      LEFT JOIN ms2n_status_hd h on m.Ndem = h.ndem
      WHERE m.Ndem = ?
    ',[
      $id
    ])[0];
    return view('dispatch.status', compact('data'));
  }

  public function sendMessages(){


    /*
    echo "sending message to " . $chatID . "\n";
    $token = "bot187041523:AAGL_P6YmZiUryD2dVDpMasx1zGWgxke3YM";

    $url = "https://api.telegram.org/" . $token . "/sendMessage?chat_id=" . $chatID;
    $url = $url . "&text=" . urlencode($messaggio);
    $ch = curl_init();
    $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
    );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    curl_close($ch);
    */
    //send
    $messaggio = "Test send Message via Tomman PSB";
    $chatID = "-137029837";
    Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messaggio
    ]);
  }

  public function saveStatus(Request $request, $id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM ms2n_status_hd
      WHERE ndem = ?
    ',[
      $id
    ]);
    if (count($exists)) {
      $data = $exists[0];
        DB::table('ms2n_status_hd')
          ->where('id', $data->id)
          ->update([
            'modified_at'     => DB::raw('NOW()'),
            'modified_by'     => $auth->id_karyawan,
            'status_wo_by_hd' => $request->input('status')
          ]);
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> update status']
      ]);
    }
    else {
        DB::table('ms2n_status_hd')->insert([
          'created_at'      => DB::raw('NOW()'),
          'created_by'      => $auth->id_karyawan,
          'status_wo_by_hd' => $request->input('status'),
          'ndem'            => $id
        ]);
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> update status']
      ]);
    }
    return $exists;
  }

  public function sendsms($orderId){
    $auth = session('auth');
    $get_data = DB::select('
      SELECT
        a.*
      FROM
        Data_Pelanggan_Starclick a
      WHERE
        a.orderId = "'.$orderId.'"
      ')[0];
    $text = "Pelanggan Telkom Yth.Kami berencana melakukan intalasi Fiber Optik di lokasi Anda. Mhn infrmasi waktu plaksanaannya dg me-reply sms ini. Kami Siap melayani Anda.";
    if (substr($get_data->orderNotel,0,2)=="08") {
      $notel = $get_data->orderNotel;
    } else if (substr($get_data->orderKontak,0,2)=="08"){
      $notel = $get_data->orderKontak;
    } else {
      $notel = NULL;
    }

    if ($notel<>NULL){
      $id_dshr = DB::connection('mysql2')->table('outbox')->insertGetId([
          'DestinationNumber' => $notel,
          'TextDecoded' => $text
      ]);
    }

    $messaggio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";

    if ($notel<>NULL){
      $messaggio .= "SENT SMS to ".$get_data->orderKontak."\nFrom Work Order : \nSC".$get_data->orderId."\n".$get_data->orderName."\n".$get_data->kcontact;
    } else {
      $messaggio .= "FAILED SMS to ".$get_data->orderKontak."\nFrom Work Order : \nSC".$get_data->orderId."\n".$get_data->orderName."\n".$get_data->kcontact;
      $messageio .= "\nPHONE NUMBER NOT FOUND\n";
    }
    $chatID = "-184620664";

    Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messaggio
    ]);

    return redirect('/dispatch/workorder/READY')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengirim SMS']
      ]);
  }

  public function manja ($id)
  {
      // dd($id);
      $ketSc = '';
      $exists = DB::select('
      SELECT dispatch_teknisi.*,dispatch_teknisi.kpro_timid,dispatch_teknisi.manja_status,a.*,id_regu,id,tgl,manja,a.orderName as Nama, c.*
      FROM Data_Pelanggan_Starclick a
      LEFT JOIN dispatch_teknisi ON a.orderId = dispatch_teknisi.Ndem
      LEFT JOIN dispatch_teknisi_timeslot c ON dispatch_teknisi.updated_at_timeslot = c.dispatch_teknisi_timeslot_id
      WHERE a.orderId = ?
      ',[
        $id
      ]);

      if (count($exists)==0){
        $exists = DB::select('
          SELECT dispatch_teknisi.*,dispatch_teknisi.kpro_timid,dispatch_teknisi.manja_status, dispatch_teknisi.manja, dispatch_teknisi.updated_at_timeslot,a.orderName as Nama,id_regu,id,tgl
          FROM Data_Pelanggan_Starclick a left join dispatch_teknisi
          on a.orderId=dispatch_teknisi.Ndem
          WHERE a.orderId = ?
        ',[
          $id
        ]);
      }

      if (count($exists)==0){
        $exists = DB::select('
          SELECT
          b.*,
           b.kpro_timid,
          b.manja_status,b.manja,b.updated_at_timeslot,a.NAMA as Nama,b.id_regu,b.id,b.tgl
          FROM dossier_master a left join dispatch_teknisi b
          on a.ND=b.Ndem
          WHERE a.ND = ?
        ',[
          $id
        ]);
      }

      if (count($exists)==0){
          $exists = dispatchModel::manjaSearchByMyir($id);
          $ketSc = 'myir';
      }

      $data = $exists[0];

      $get_manja_status = DB::SELECT('
        SELECT
          a.manja_status_id as id,
          a.manja_status as text
        FROM
          manja_status a
      ');

      return view('dispatch.manja', compact('data', 'get_manja_status', 'id', 'ketSc'));
  }

  public function saveManja(Request $req, $id)
  {
      // dd($req);
      // dd(session('auth'));

      $this->validate($req,[
          'manja_status' => 'required',
          'manja'        => 'required',
      ]);

      $exist = DispatchModel::getData($id);
      if ($exist){
          DispatchModel::updateManja($req, $exist->id, session('auth')->id_karyawan);

          // simpan log
          DB::table('dispatch_teknisi_log')->insert([
             'updated_at'          => DB::raw('NOW()'),
             'updated_by'          => session('auth')->id_karyawan,
             'tgl'                 => $exist->tgl,
             'id_regu'             => $exist->id_regu,
             'manja'               => $req->input('manja'),
             'manja_status'        => $req->input('manja_status'),
             'updated_at_timeslot' => $req->input('timeslot'),
             'Ndem'                => $id
          ]);

          // $auth = session('auth');
          $dataRegu = DB::select('SELECT * FROM dispatch_teknisi WHERE Ndem = ?',[$id]);
          $namaDispatch = DB::select('SELECT * FROM 1_2_employee where nik= ?',[$dataRegu[0]->updated_by]);

          if (!empty($dataRegu[0]->id_regu)){
              // get regu
              $get_regu = DB::SELECT('
                SELECT a.uraian,a.mainsector FROM regu a WHERE a.id_regu = ?
              ',[ $dataRegu[0]->id_regu ]);
              $get_regu = $get_regu[0];

              // get manja
              $get_manja_status = DB::SELECT('
                SELECT a.manja_status FROM manja_status a WHERE a.manja_status_id = ?
              ',[ $dataRegu[0]->manja_status ]);
              $get_manja_status = $get_manja_status[0];

              // get data
              $ketSc = '';
              $get_data = DB::SELECT('
                SELECT
                  a.*,
                  b.mdf_grup_id
                FROM
                  Data_Pelanggan_Starclick a
                  LEFT JOIN mdf b ON a.sto = b.mdf
                WHERE a.orderId = ?
              ',[ $id ]);

              if (count($get_data)==0) {
                $get_data = DB::SELECT('
                  SELECT
                   a.*,
                   b.mdf_grup_id,
                   a.ND as orderId,
                   a.NAMA as orderName,
                   a.LQUARTIER as kcontact,
                   0 as lat,
                   0 as lon
                  FROM
                    dossier_master a
                  LEFT JOIN mdf b ON a.STO = b.mdf
                  WHERE
                    a.ND = ?
                ', [ $id ]);
              };

              if (count($get_data)==0){
                  $get_data = dispatchModel::manjaSearchByMyir($id);
                  $ketSc = 'myir';
              };


              // send to Telegram SSC1
              // $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";
              if (!empty($namaDispatch)){
                  $messageio = "Pengirim : ".$namaDispatch[0]->nik."-".$namaDispatch[0]->nama."\n\n";
              }
              else{
                  $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";
              };

              $messageio .= "Manajemen Janji\n";
              $messageio .= "Regu : ".$get_regu->uraian."\n";
              $messageio .= "Tanggal : ".$dataRegu[0]->tgl."\n";
              $messageio .= "Status Manja : ".$get_manja_status->manja_status."\n";
              // $messageio .= "Timeslot : ".$request_timeslot."\n";
              $messageio .= "Keterangan : \n".$dataRegu[0]->manja."\n\n";
              if ($ketSc=='myir'){
                  $messageio .= "Related to MYIR".$get_data[0]->myir."\n";
                  $messageio .= $get_data[0]->customer;
              }
              else{
                  $messageio .= "Related to SC".$get_data[0]->orderId."\n".$get_data[0]->orderName."\n".$get_data[0]->kcontact;
              };

              $get_data[0];
              $chatID = "-199400365";
              // $chatID = "192915232";
              Telegram::sendMessage([
                'chat_id' => $chatID,
                'text' => $messageio
              ]);

              if ($ketSc<>'myir'){
                  if ($get_data[0]->lat<>NULL){
                    Telegram::sendMessage([
                      'chat_id' => $chatID,
                      'text' => "Lokasi SC".$get_data[0]->orderId." ".$get_data[0]->orderName." 👇🏻"
                    ]);
                    Telegram::sendLocation([
                      'chat_id' => $chatID,
                      'latitude' => $get_data[0]->lat,
                      'longitude' => $get_data[0]->lon
                    ]);
                  }
                  if ($get_regu->mainsector<>NULL){
                    $chatID = $get_regu->mainsector;
                  } else {
                    $chatID = "-".$get_data[0]->mdf_grup_id;
                  }

                  Telegram::sendMessage([
                    'chat_id' => $chatID,
                    'text' => $messageio
                  ]);
                  Telegram::sendMessage([
                    'chat_id' => $chatID,
                    'text' => "Lokasi SC".$get_data[0]->orderId." ".$get_data[0]->orderName." 👇🏻"
                  ]);
                  Telegram::sendLocation([
                    'chat_id' => $chatID,
                    'latitude' => $get_data[0]->lat,
                    'longitude' => $get_data[0]->lon
                  ]);
              }
            };

          return redirect('/manja/list/'.date('Y-m-d'))->with('alerts',[['type' => 'success', 'text' => 'MANJA Sudah <strong>Disimpan</strong>']]);
      }
      else {
           DispatchModel::simpanManja($req, session('auth')->id_karyawan, $id);

          // simpan log
          DB::table('dispatch_teknisi_log')->insert([
             'updated_at'          => DB::raw('NOW()'),
             'updated_by'          => session('auth')->id_karyawan,
             'tgl'                 => $exist->tgl,
             'id_regu'             => $exist->id_regu,
             'manja'               => $req->input('manja'),
             'manja_status'        => $req->input('manja_status'),
             'updated_at_timeslot' => $req->input('timeslot'),
             'Ndem'                => $id
          ]);

          $auth = session('auth');
          $dataRegu = DB::select('SELECT * FROM dispatch_teknisi WHERE Ndem = ?',[$id]);
          if (!empty($dataRegu->id_regu)){

              // get regu
              $get_regu = DB::SELECT('
                SELECT a.uraian,a.mainsector FROM regu a WHERE a.id_regu = ?
              ',[ $dataRegu->id_regu ]);
              $get_regu = $get_regu[0];

              // get manja
              $get_manja_status = DB::SELECT('
                SELECT a.manja_status FROM manja_status a WHERE a.manja_status_id = ?
              ',[ $dataRegu->manja_status ]);
              $get_manja_status = $get_manja_status[0];

              // get data
              $get_data = DB::SELECT('
                SELECT
                  a.*,
                  b.mdf_grup_id
                FROM
                  Data_Pelanggan_Starclick a
                  LEFT JOIN mdf b ON a.sto = b.mdf
                WHERE a.orderId = ?
              ',[ $id ]);

              if (count($get_data)==0) {
                $get_data = DB::SELECT('
                  SELECT
                   a.*,
                   b.mdf_grup_id,
                   a.ND as orderId,
                   a.NAMA as orderName,
                   a.LQUARTIER as kcontact,
                   0 as lat,
                   0 as lon
                  FROM
                    dossier_master a
                  LEFT JOIN mdf b ON a.STO = b.mdf
                  WHERE
                    a.ND = ?
                ', [ $id ]);
              };

              // send to Telegram SSC1
              $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";
              $messageio .= "Manajemen Janji\n";
              $messageio .= "Regu : ".$get_regu->uraian."\n";
              $messageio .= "Tanggal : ".$dataRegu[0]->tgl."\n";
              $messageio .= "Status Manja : ".$get_manja_status->manja_status."\n";
              // $messageio .= "Timeslot : ".$request_timeslot."\n";
              $messageio .= "Keterangan : \n".$dataRegu->manja."\n\n";
              $messageio .= "Related to SC".$get_data[0]->orderId."\n".$get_data[0]->orderName."\n".$get_data[0]->kcontact;

              $chatID = "-199400365";
              // $chatID = "192915232";
              Telegram::sendMessage([
                'chat_id' => $chatID,
                'text' => $messageio
              ]);
              if ($get_data[0]->lat<>NULL){
                Telegram::sendMessage([
                  'chat_id' => $chatID,
                  'text' => "Lokasi SC".$get_data[0]->orderId." ".$get_data[0]->orderName." 👇🏻"
                ]);
                Telegram::sendLocation([
                  'chat_id' => $chatID,
                  'latitude' => $get_data[0]->lat,
                  'longitude' => $get_data[0]->lon
                ]);
              }
              if ($get_regu->mainsector<>NULL){
                $chatID = $get_regu->mainsector;
              } else {
                $chatID = "-".$get_data[0]->mdf_grup_id;
              }

              Telegram::sendMessage([
                'chat_id' => $chatID,
                'text' => $messageio
              ]);
              Telegram::sendMessage([
                'chat_id' => $chatID,
                'text' => "Lokasi SC".$get_data[0]->orderId." ".$get_data[0]->orderName." 👇🏻"
              ]);
              Telegram::sendLocation([
                'chat_id' => $chatID,
                'latitude' => $get_data[0]->lat,
                'longitude' => $get_data[0]->lon
              ]);
            };
      }

      return redirect('/manja/list/'.date('Y-m-d'))->with('alerts',[['type' => 'success', 'text' => 'MANJA sudah <strong>Disimpan</strong>']]);
  }

  public function getJmlWo($tgl)
  {
      $getWoSektors = DispatchModel::getJmlWoBySektor($tgl);
      return view('dashboard.listWoSektor', compact('getWoSektors', 'tgl'));
  }

  public function getJmlWoSingle($tgl, $sto, $idRegu)
  {
      $data = DispatchModel::getJmlWoBySektorIdRegu($tgl, $sto, $idRegu);
      dd($data);
  }

  public function listManja($tgl, DispatchModel $modelDispatch, Request $req)
  {
      $this->validate($req,[
          'q' => 'numeric',
      ]);
      date_default_timezone_set("Asia/Makassar");
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
      $enddate = date('Y-m-d');

      $listManjas = []; //$modelDispatch->listManja($startdate,$enddate);
      $myir       = '';

      if ($req->has('q')){
          $cari = $req->input('q');
          $listManjas = $modelDispatch->manjaSearchBySc(trim($cari));
          $ketSc = 'sc';

          if (count($listManjas)==0){
              $listManjas = $modelDispatch->manjaSearchByMyir(trim($cari));
              $ketSc = 'myir';

              if (count($listManjas)==0){
                  $dataMyir = dispatchModel::cariMyir($cari);
                  if ($dataMyir->ket==1){
                      $scMyir = $dataMyir->sc;

                      $listManjas = $modelDispatch->manjaSearchBySc(trim($scMyir));
                      $ketSc = 'sc';
                      $myir  = $cari;
                  }
              }
          };

          // dd($listManjas);
      };

      return view('dispatch.listManja',compact('listManjas','ketSc','myir'));
  }

  public function jamBerangkatProv($tgl)
  {
      $sql = 'SELECT a.Ndem, b.status_laporan
              from
                  dispatch_teknisi a
              LEFT JOIN
                  psb_laporan_log b on a.Ndem=b.Ndem
              where
                  a.Ndem is not null and a.tgl like "%'.$tgl.'%" LIMIT 5';
      $hasil = DB::select($sql);
      dd($hasil);
  }

  public static function matrikTidakHadir(){
      date_default_timezone_set('Asia/Makassar');
      $date = date('Y-m-d');
      $getTeamMatrix = DispatchModel::getTeamMatrixNot($date);
      $data = array();
      $lastRegu = '';
      $jmlProv = 0; $jmlAssr = 0;

      foreach($getTeamMatrix as $team) :
        $listWO = array(); 
        $listWOData = DispatchModel::listWO($date,$team->id_regu);
        
        foreach ($listWOData as $listData) : 
          if ($listData->status_laporan<>52 && $listData->status_laporan<>57 && $listData->status_laporan<>58 && $listData->status_laporan<>59 && $listData->status_laporan<>60){
              $listWO[] = $listData;
          };
        endforeach;  

        $ketNik1='-';
        $ketNik2='-';
      
        if ($team->nik1<>NULL){
            $statusNik1 = DispatchModel::statusAbsen($team->nik1, $date);
            if(count($statusNik1)<>NULL){
                $ketNik1 = "HADIR";
            };
        }

        if($team->nik2<>NULL){
          $statusNik2 = DispatchModel::statusAbsen($team->nik2, $date);
          if(count($statusNik2)<>NULL){
              $ketNik2 = "HADIR";
          };
        }


        // echo $team->nik1."\n";
        // echo $statusNik2->nik.' '.$ketNik2."\n";

        $ketRegu = '';
        if ($ketNik1=="-" && $ketNik2=="-"){
            $data[$team->mainsector][] = array(
              "mitra"     => $team->mitra,  
              "uraian"    => $team->uraian,
              "kemampuan" => $team->kemampuan,
              "nik1"      => $team->nik1,
              "nik2"      => $team->nik2,
              "ketNik1"   => $ketNik1,
              "ketNik2"   => $ketNik2,
              "jns"       => "-",
              "ketRegu"   => 'TIDAK HADIR',
              "chat_id"   => $team->chat_id,
              "title"     => $team->title,
              'statusWo'  => $team->sttsWo,
              "listWO"    => $listWO
            );
        };

      endforeach;
      
      $pesan = "List TIm Tidak Hadir Ada Wo ".date('Y-m-d')."\n";
      $pesan .= "====================================\n";
      $pesan .= "Provisioning\n";    
      foreach ($data as $key=>$dt){
        if ($key<>'-1001133390186'){
          foreach($dt as $d){
            if ($d['statusWo']==1){
              if (count($d['listWO'])<>0){
                  $jmlProv += 1;
                  $pesan .= $d['title'].' - '.$d['uraian']."\n";
                  foreach($d['listWO'] as $wo){
                      $status = "NEED PROGRESS";
                      if ($wo->laporan_status<>''){
                          $status = $wo->laporan_status;
                      };

                      $pesan .= '-'.$wo->Ndem.' '.$status."\n";
                  }
                  $pesan .= "\n";   
              }
            }
          }
        }
      }

      // $chatId = "102050936";
      $chatId = "192915232";
      if ($jmlProv<>0){
        Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => $pesan
        ]);
      }

      $pesan = '';
      $pesan = "List TIm Tidak Hadir Ada Wo ".date('Y-m-d')."\n";
      $pesan .= "====================================\n";
      $pesan .= "Assurance\n";    
      foreach ($data as $key=>$dt){
        if ($key<>'-1001133390186'){
          foreach($dt as $d){
            if ($d['statusWo']==0){
              $jmlAssr += 1;
              if (count($d['listWO'])<>0){
                  $pesan .= $d['title'].' - '.$d['uraian']."\n";
                  foreach($d['listWO'] as $wo){
                      $status = "NEED PROGRESS";
                      if ($wo->laporan_status<>''){
                          $status = $wo->laporan_status;
                      };

                      $pesan .= '-'.$wo->Ndem.' '.$status."\n";
                  }
                  $pesan .= "\n";   
              }
            }
          }
        }
      }
     
      // $chatId = "102050936";
      $chatId = "192915232";
      if ($jmlAssr<>0){
        Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => $pesan
        ]);
      }
  }

  public function matrixZainal($date){
    date_default_timezone_set('Asia/Makassar');
    $area = 'PROV';
    $getTeamMatrix = DispatchModel::getTeamMatrix($date,$area);
    $get_area = DispatchModel::group_telegram($area);
    $data = array();
    $lastRegu = '';

    foreach($getTeamMatrix as $team) :
      $listWO = array(); 
      $listWOData = DispatchModel::listWO($date,$team->id_regu);
      
      foreach ($listWOData as $listData) : 
        if ($listData->status_laporan<>52 && $listData->status_laporan<>57 && $listData->status_laporan<>58 && $listData->status_laporan<>59 && $listData->status_laporan<>60){
            $listWO[] = $listData;
        };
      endforeach;  

      $ketNik1='';
      $ketNik2='';
    
      if ($team->nik1<>NULL){
          $statusNik1 = DispatchModel::statusAbsen($team->nik1, $date);
          if(count($statusNik1)<>NULL){
              $ketNik1 = "HADIR";
          };
      }

      if($team->nik2<>NULL){
        $statusNik2 = DispatchModel::statusAbsen($team->nik2, $date);
        if(count($statusNik2)<>NULL){
            $ketNik2 = "HADIR";
        };
      }


      // echo $team->nik1."\n";
      // echo $statusNik2->nik.' '.$ketNik2."\n";

      $ketRegu = '';
      if ($ketNik1=="HADIR" || $ketNik2=="HADIR"){
          $ketRegu = "HADIR";
      };

      $data[] = array(
        "mitra" => $team->mitra,  
        "uraian"=>$team->uraian,
        "kemampuan"=>$team->kemampuan,
        "nik1"=>$team->nik1,
        "nik2"=>$team->nik2,
        "jns"       => "-",
        "ketRegu"   => $ketRegu,
        "listWO" =>$listWO
      );
    endforeach;

    if ($area=='ALL'){
        $ketSektor = 'ALL';
    }
    else{
        $ketSektor = 'PROVISIONING';
    }
 
    return view('dispatch.matrixZainal',compact('get_area','data','date','area','ketSektor'));
  }

  public function matrixReboundary($tgl){
    date_default_timezone_set('Asia/Makassar');
    $date = $tgl;
    $area = Input::get('sektor');
    if ($area==""){ $area = 'ALL'; }
    $get_sektor = DispatchModel::get_sektor();
    $get_area = DispatchModel::group_telegram($area);
    $getTeamMatrix = DispatchModel::getTeamMatrixReboundary($date);
    $data = array();
    $lastRegu = '';
 
    foreach($getTeamMatrix as $team) :
      $listWO = array(); 
      $listWOData = DispatchModel::listWOReboundary($date,$team->id_regu);
      
      foreach ($listWOData as $listData) : 
        if ($listData->status_laporan<>52 && $listData->status_laporan<>57 && $listData->status_laporan<>58 && $listData->status_laporan<>59 && $listData->status_laporan<>60){
            $listWO[] = $listData;
        };
      endforeach;  

      $ketNik1='-';
      $ketNik2='-';
    
      if ($team->nik1<>NULL){
          $statusNik1 = DispatchModel::statusAbsen($team->nik1, $date);
          if(count($statusNik1)<>NULL){
              $ketNik1 = "HADIR";
          };
      }

      if($team->nik2<>NULL){
        $statusNik2 = DispatchModel::statusAbsen($team->nik2, $date);
        if(count($statusNik2)<>NULL){
            $ketNik2 = "HADIR";
        };
      }


      // echo $team->nik1."\n";
      // echo $statusNik2->nik.' '.$ketNik2."\n";

      $ketRegu = '';
      if ($ketNik1=="HADIR" || $ketNik2=="HADIR"){
          $ketRegu = "HADIR";
      };

      $data[$team->mainsector][] = array(
        "mitra" => $team->mitra,  
        "uraian"=>$team->uraian,
        "kemampuan"=>$team->kemampuan,
        "nik1"=>$team->nik1,
        "nik2"=>$team->nik2,
        "ketNik1" =>$ketNik1,
        "ketNik2" =>$ketNik2,
        "jns"       => "-",
        "ketRegu"   => $ketRegu,
        "listWO" =>$listWO
      );
    endforeach;
    $ketSektor = 'REBOUNDARY';
      
    return view('dispatch.matrixReboundary',compact('get_area','data','get_sektor','date','area','ketSektor'));
  }

  public function matrixHDFoto($date, DispatchModel $dispatchModel){
    date_default_timezone_set('Asia/Makassar');
    $hdFallout = DispatchModel::getHdFalloutMatrix($date);
    //dd($hdFallout);
    $fout = array();
    $lastHD = '';

    foreach ($hdFallout as $hd){
      if($lastHD == $hd->workerId){
          $fout[count($fout)-1]['jumlahWO'] += 1;
          $fout[count($fout)-1]['keterangan'] = $hd->ket; 
          $fout[count($fout)-1]['WO'][] = array("sc"=>$hd->orderId, "status_laporan"=>$hd->orderStatus, "keteranganWo"=>$hd->ket);
          //status

          if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="0" ){
            $fout[count($fout)-1]['UP'] += 1;
          }
          else if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="1" ){
            $fout[count($fout)-1]['UP_PDA'] += 1;
          }
           else{
            $fout[count($fout)-1]['PROGRESS'] += 1;
          }

          
      }else{
          $fout[] = array('nama' => $hd->workerName,'jumlahWO'=>1,'PROGRESS'=>0,'UP'=>0, 'UP_PDA'=>0,'keterangan'=>$hd->ket,'WO' => array(array("sc"=>$hd->orderId, "status_laporan"=>$hd->orderStatus, "keteranganWo"=>$hd->ket)));
          if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="0"){
            $fout[count($fout)-1]['UP'] += 1;
          } 
          else if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="1" ){
            $fout[count($fout)-1]['UP_PDA'] += 1;
          }else{
            $fout[count($fout)-1]['PROGRESS'] += 1;
          }      
      }
      $lastHD = $hd->workerId;
    }


    //dd($data);

    $periode = date('Y-m',strtotime($date));
    $manjas  = $dispatchModel->jumlahManjaByUser($periode);
      
    return view('dispatch.matrixHdFoto',compact('fout', 'manjas'));
  }

  public function listKendalaUp($tgl){
      $sql = 'SELECT 
                *,
              a.created_at as tglDispatch,
              a.tgl as tglReDispatch,
              a.id as id_dt,
              f.sto as area
              FROM 
                dispatch_teknisi a
              LEFT JOIN Data_Pelanggan_Starclick f ON a.Ndem=f.orderId
              LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
              LEFT JOIN psb_laporan_status c ON b.status_laporan=c.laporan_status_id
              LEFT JOIN regu d ON a.id_regu=d.id_regu
              LEFT JOIN group_telegram g ON d.mainsector=g.chat_id
              LEFT JOIN mdf h ON f.sto = h.mdf
              WHERE
                a.dispatch_by IS NULL AND
                f.orderId<>"" AND 
                b.status_kendala IS NOT NULL AND 
                a.tgl like "%'.$tgl.'%"
              ORDER BY 
                b.status_laporan ASC
              ';

      $data = DB::select($sql);

      $sql2 = 'SELECT 
                *,
              a.created_at as tglDispatch,
              a.tgl as tglReDispatch,
              a.id as id_dt,
              f.sto as area
              FROM 
                dispatch_teknisi a
              LEFT JOIN psb_myir_wo f ON a.Ndem=f.myir
              LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
              LEFT JOIN psb_laporan_status c ON b.status_laporan=c.laporan_status_id
              LEFT JOIN regu d ON a.id_regu=d.id_regu
              LEFT JOIN group_telegram g ON d.mainsector=g.chat_id
              LEFT JOIN mdf h ON f.sto = h.mdf
              WHERE
                a.dispatch_by=5 AND
                f.myir<>"" AND 
                b.status_kendala IS NOT NULL AND 
                a.tgl like "%'.$tgl.'%"
              ORDER BY 
                b.status_laporan ASC
              ';

      $dataMyir = DB::select($sql2);
      $noMyir   = count($data)+1;
      return view('dispatch.listKendalaUp',compact('data', 'dataMyir', 'noMyir'));

  }

}
