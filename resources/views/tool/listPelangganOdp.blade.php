@extends('layout')

@section('content')
  @include('partial.alerts')

      <div class="panel panel-primary" id="info">
          <div class="panel-heading">List Pelanggan <b>{{ $odp }}</b></div>
          <div class="panel-body table-responsive">
              <table class="table table-bordered table-fixed">
                  <tr>
                      <th>#</th>
                      <th>Nama ODP</th>
                      <th>Kordinat ODP</th>
                      <th>Update Terakhir</th>
                      <th>WO</th>
                      <th>Nama Pelanggan</th>
                      <th>Notel</th>
                      <th>NDEM Speedy</th>
                      <th>Alamat</th>
                  </tr>
                 
                  @foreach($data as $no => $d)
                          <td>{{ ++$no }}</td>
                          <td>{{ $d->nama_odp }}</td>
                          <td>{{ $d->kordinat_odp }}</td>
                          <td>{{ $d->modifiedPsbLap ?: $d->createdPsbLap }}</td>
                          <td>{{ $d->Ndem }}</td>
                          <td>{{ $d->orderName ?: $d->NAMA ?: $d->nama_micc ?: $d->customer ?: '-'}}</td>
                          <td>{{ $d->orderNotel }}</td>
                          <td>{{ $d->ndemSpeedy }}</td>
                          <td>{{ $d->orderAddr ?: $d->alamat_micc ?: '-' }}</td>
                      </tr>
                  @endforeach
              </table>
          </div>
      </div>
@endsection
