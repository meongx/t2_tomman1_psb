<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class AmijaModel{
  public static function list($mitra){
    $query = DB::SELECT('
      SELECT
        a.*,
        b.*,
        c.*,
        d.*,
        e.jadwal,
        e.hadir
      FROM
        1_2_employee a
      LEFT JOIN regu b ON (a.nik = b.nik1 OR a.nik = b.nik2) AND b.ACTIVE=1
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN employee_squad d ON a.squad = d.squad_id
      LEFT JOIN a2s_kehadiran e ON (a.nik_amija = e.nik AND e.tgl = "'.date('Y-m-d').'")
        WHERE
          a.nama <> "" AND  
          a.squad IN (2,6)
      GROUP BY a.nik
    ');
    return $query;
  }
}

?>
