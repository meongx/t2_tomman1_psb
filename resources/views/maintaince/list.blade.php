@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    <a href="/maintaince/input" class="btn btn-sm btn-info">
        <span class="glyphicon glyphicon-plus"></span>
      </a>
    Maintenance
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
  <li class="{{ (Request::segment(3) == 'np') ? 'active' : '' }}"><a href="/maintaince/status/np">Need Progress</a></li>
  <li class="{{ (Request::segment(3) == 'close') ? 'active' : '' }}"><a href="/maintaince/status/close">Quality and Quantity Control</a></li>
  <li class="{{ (Request::segment(3) == 'rekon') ? 'active' : '' }}"><a href="/maintaince/status/rekon">Rekon</a></li>
  <li class="{{ (Request::segment(3) == 'invoice') ? 'active' : '' }}"><a href="/maintaince/status/invoice">Invoice</a></li>
  @if(session('auth')->level ==53)
    <li class="{{ (Request::segment(2) == 'search') ? 'active' : '' }}"><a href="/maintaince/search">Cari Tiket</a></li>
    <li class="{{ (Request::segment(2) == 'dummy') ? 'active' : '' }}"><a href="/maintaince/dummy">Tiket Dummy</a></li>
  @endif
  </ul>
  <div class="list-group">
    @foreach($list as $data)
      <div class="list-group-item">
        <strong>{{ $data->no_tiket }}</strong>
        <br />
        <span>{{ $data->headline }}</span>
        <br />
        <span>{{ $data->info }}</span><br />
        <a href="/maintaince/{{ $data->id }}" class="btn btn-sm btn-info">
          Ticket
        </a>
        <a href="/maintaince-progres/{{ $data->id }}" class="btn btn-sm btn-info">
          {{ $data->status or 'Progress' }}
        </a>
        <a href="/maintaince-photos/{{ $data->id }}" class="btn btn-sm btn-info">
          Photos
        </a>
        <span class="label label-info">
          {{ $data->dispatch_regu_name or '---' }}
        </span>
        &nbsp 
        <span class="label label-info">
          Total Rp. {{ number_format($data->hasil_by_user) }}
        </span>
      </div>

    @endforeach
  </div>
@endsection
