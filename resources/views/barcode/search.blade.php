@extends('tech_layout')

@section('content')
  @include('partial.alerts')
  <style>
    .red {
      background-color: #FF0000;

    }
    .warning1 {
      background-color: #f39c12;
    }
    .warning2 {
      background-color: #e67e22;
    }
    .warning3 {
      background-color: #e74c3c;
    }


    .default {
      background-color: #888888;
    }
    .line {
      border-radius: 5px;
      padding : 3px;
    }
  </style>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
  <li class="{{ (Request::path() == '/') ? 'active' : '' }}"><a href="/">NEED PROGRESS</a></li>
  <li class="{{ (Request::segment(2) == 'KENDALA') ? 'active' : '' }}"><a href="/workorder/KENDALA">KENDALA</a></li>
  <li class="{{ (Request::segment(2) == 'UP') ? 'active' : '' }}"><a href="/workorder/UP">UP</a></li>
  <li class="{{ (Request::segment(1) == 'ibooster') ? 'active' : '' }}"><a href="/ibooster">IBOOSTER</a></li>
  <li class="{{ (Request::segment(1) == 'searchbarcode') ? 'active' : '' }}"><a href="/searchbarcode">BARCODE</a></li>
    <!-- <li><a href="/stok">Materials/NTE</a></li> -->
  </ul>
  <?php if (count($checkONGOING)>0) : ?>
  <div class="content">
    <?php foreach($checkONGOING as $ONGOING) : ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        ONGOING PROGRESS {{ $ONGOING->Ndem }}
      </div>
      <div class="panel-content" style="padding:20px;">

        <center>
        @if(strtoupper(substr($ONGOING->Ndem,0,2)) <> "IN")
        <a href="/{{ $ONGOING->id_dt }}" class="responsive btn btn-sm btn-warning">CLICK HERE FOR REPORT</a>
        @else
        <a href="/tiket/{{ $ONGOING->id_dt }}" class="responsive btn btn-sm btn-warning">CLICK HERE FOR REPORT</a>
        @endif
        <center>

      </div>
    </div>
      <?php endforeach; ?>
  </div>
  <?php endif; ?>

    <div class="panel panel-default" style="padding:20px">
      <div class="panel-heading">
        Pencarian Barcode
      </div>
      <div class="panel-content">
        <center>
          <form method="post">
          <table class="table">
            <tr>
              <td><label for="barcode">Barcode</label></td>
              <td><input type="text" name="barcode" class="form-control" value="{{ @$input }}" /></td>
              <td><input type="submit" name="submit" value="Cari" class="form-control btn btn-warning " /></td>
            </tr>
          </table>
        </form>
        </center>
        <div class="panel panel-default">
          <div class="panel-body">
            {!! $result !!}
          </div>
        </div>
      </div>
    </div>

  </div>
@endsection
