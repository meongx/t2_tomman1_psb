@extends('layout')

@section('content')
      <div class="panel panel-primary">
        <div class="panel-heading">.....</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>NIK</th>
                <th>NAMA</th>
                <th>JADWAL</th>
                <th>HADIR</th>
                </tr>
              </thead>
              <tbody>
                @foreach($detil as $no => $t)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $t->nik }}</td>
                  <td>{{ $t->teknisi }}</td> 
                  <td>{{ $t->jadwal }}</td> 
                  <td>{{ $t->hadir }}</td> 
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

@endsection