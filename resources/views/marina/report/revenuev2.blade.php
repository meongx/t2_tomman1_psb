@extends('layout')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
    }
  </style>
@endsection
@section('content')
<input name="tglnav" id="tglnav" type="hidden" value="{{Request::segment(4)}}"/>
    <div class="panel panel-default" id="info" class="align-middle">
        <div class="panel-heading"> 
        <div class="row">
  <div class="col-lg-6">
    <h4>REVENUE {{Request::segment(3)}} {{Request::segment(4)}}</h4>
  </div>
  <div class="col-lg-6 pull-right">
    <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
        Tahun
        <input type="text" id="tahun" class="form-control input-sm" value="{{ substr(Request::segment(4),0,4) }}" placeholder="Tahun">
      </div>
      <div class="form-group">
        Bulan
        <input type="text" id="bulan" class="form-control input-sm" value="{{ substr(Request::segment(4),5,2) }}" placeholder="Bulan">
      </div>
      <button type="button" id="goto" class="btn btn-success btn-sm">Go!</button>
    </form>
  </div><!-- /.col-lg-6 -->
</div></div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-fixed">
                <tr>
                    <th style="vertical-align:middle" rowspan=1>#</th>
                    <th style="vertical-align:middle" rowspan=1>MITRA</th>
                    <th style="vertical-align:middle" rowspan=1>BENJAR</th>
                    <th style="vertical-align:middle" rowspan=1>GAMAS</th>
                    <th style="vertical-align:middle" rowspan=1>ODP LOSS</th>
                    <th style="vertical-align:middle" rowspan=1>REMO</th>
                    <th style="vertical-align:middle" rowspan=1>UTILITAS</th>
                    <th style="vertical-align:middle" rowspan=1>INSERT TIANG</th>
                    <th style="vertical-align:middle" rowspan=1>VALIDASI ODP</th>
                    <th style="vertical-align:middle" rowspan=1>ODP FULL</th>
                    <th style="vertical-align:middle" rowspan=1>ODP SEHAT</th>
                    <th style="vertical-align:middle" rowspan=1>TOTAL</th>
                </tr>
                <?php
                    $benjar=0;$gamas=0;$odp_loss=0;$remo=0;$utilitas=0;$insert_tiang=0;$odp_full=0;$odp_sehat=0;$val_odp=0;$total=0;
                ?>
                @foreach($data as $no => $d)

                    <?php
                        $benjar+=$d->benjar;$gamas+=$d->gamas;$odp_loss+=$d->odp_loss;$remo+=$d->remo;$utilitas+=$d->utilitas;$insert_tiang+=$d->insert_tiang;$odp_full+=$d->odp_full;$odp_sehat+=$d->odp_sehat;$val_odp+=$d->val_odp;$total+=$d->total;
                    ?>
                    
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td><a href="#" class="btnnewrow" data-mitra="{{ $d->mitra }}" data-sgmt2="{{ Request::segment(2) }}">{{ $d->mitra }}</a>
                        <a href="#" class="btnrmrow">{{ $d->mitra }}</a></td>
                        <td align="right">{{ number_format($d->benjar) }}</td>
                        <td align="right">{{ number_format($d->gamas) }}</td>
                        <td align="right">{{ number_format($d->odp_loss) }}</td>
                        <td align="right">{{ number_format($d->remo) }}</td>
                        <td align="right">{{ number_format($d->utilitas) }}</td>
                        <td align="right">{{ number_format($d->insert_tiang) }}</td>
                        <td align="right">{{ number_format($d->val_odp) }}</td>
                        <td align="right">{{ number_format($d->odp_full) }}</td>
                        <td align="right"><b style="color:yellow;">{!! $d->tiket_odp_sehat?'('.$d->tiket_odp_sehat.' Tiket) ':'' !!}</b>{{ number_format($d->odp_sehat) }}
                        </td>
                        <td align="right">{{ number_format($d->total) }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2">TOTAL</td>
                    <td align="right">{{ number_format($benjar) }}</td>
                    <td align="right">{{ number_format($gamas) }}</td>
                    <td align="right">{{ number_format($odp_loss) }}</td>
                    <td align="right">{{ number_format($remo) }}</td>
                    <td align="right">{{ number_format($utilitas) }}</td>
                    <td align="right">{{ number_format($insert_tiang) }}</td>
                    <td align="right">{{ number_format($val_odp) }}</td>
                    <td align="right">{{ number_format($odp_full) }}</td>
                    <td align="right">{{ number_format($odp_sehat) }}</td>
                    <td align="right">{{ number_format($total) }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div id="detil-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #eee">
          <strong style="color:black;">Detail Material</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 450px">

        </div>
      </div>
    </div>
  </div>
@endsection
@section('plugins')
    <script type="text/javascript">
        $(function() {
            $('.btnrmrow').hide();
            $(".btnnewrow").click(function(e){
                var $this = $(this);
                var mt = this.getAttribute("data-mitra");
                var dd = this.getAttribute("data-sgmt2");
                var tglnav = $('#tglnav').val();
                $.ajax({
                    type: "GET",
                    url: "/marina/ajaxrev/"+dd+"/"+mt+"/"+tglnav,
                    dataType: "html"
                }).done(function(data) {
                    $('.btnrmrow').show();
                    $('.btnnewrow').hide();
                    $this.closest('tr').after(data);
                });
            });
            $(".btnrmrow").click(function(e){
                $('.ajax').remove();
                $('.btnrmrow').hide();
                $('.btnnewrow').show();
            });
            $("#goto").click(function(e){
                var segments = $(location).attr('href').split('/');
                var goto='marina/'+segments[4]+'/'+segments[5]+'/'+$('#tahun').val()+'-'+$('#bulan').val();
                window.location = '/'+goto;
            });
        })
    </script>
@endsection