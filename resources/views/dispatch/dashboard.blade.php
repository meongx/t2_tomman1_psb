@extends('layout')

@section('content')
  @include('partial.alerts')


  <h3>Dashboard WorkOrder</h3>

  <style>
    td {
      padding : 10px;
      font-weight: bold;
    }
  </style>
  <table border="0">
    <tr>
      <td colspan=4>WorkOrder untuk di Dispatch</td>
      <td colspan=4>Priority</td>
    </tr>
    <tr>
      <td rowspan="6" width="150" align="center"><a href="/dispatch/workorder/READY">WO READY ({{ $READY }})</a></td>
      <td rowspan="4" width="150" align="center"><a href="/dispatch/workorder/NAL">NAL ({{ $NAL }})</a></td>
      <td rowspan="2" width="100" align="center"><a href="/dispatch/workorder/MBSP">MBSP ({{ $MBSP }})</a></td>
      <td><a href="/dispatch/workorder/MBSP_CLUSTER">Cluster ({{ $MBSP_CLUSTER }})</a></td>
      <td align=center>1</td>
    </tr>
    <tr>
      <td><a href="/dispatch/workorder/MBSP_NONCLUSTER">Non Cluster ({{ $MBSP_NONCLUSTER }})</a></td>
      <td align=center>2</td>
    </tr>
    <tr>
      <td rowspan="2" align="center"><a href="/dispatch/workorder/REGULER">Reguler ({{ $REGULER }})</a></td>
      <td ><a href="/dispatch/workorder/REGULER_CLUSTER">Cluster ({{ $REGULER_CLUSTER }})</a></td>
      <td align=center>3</td>
    </tr>
    <tr>
      <td><a href="/dispatch/workorder/REGULER_CLUSTER">Non Cluster ({{ $REGULER_NONCLUSTER }})</a></td>
      <td align=center>4</td>
    </tr>
    <tr>
      <td rowspan="2" align="center"><a href="/dispatch/workorder/NONNAL">Non NAL ({{ $NONNAL }})</a></td>
      <td colspan="2"><a href="/dispatch/workorder/NONNAL_CLUSTER">Cluster ({{ $NONNAL_CLUSTER }})</a></td>
      <td align=center>5</td>
    </tr>
    <tr>
      <td colspan="2"><a href="/dispatch/workorder/NONNAL_NONCLUSTER">Non Cluster ({{ $NONNAL_NONCLUSTER }})</a></td>
      <td align=center>6</td>
    </tr>
  </table>

  <script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
@endsection
