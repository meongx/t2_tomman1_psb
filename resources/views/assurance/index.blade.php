
@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .strong{
      font-weight: bold;
    }​
    .stronger{
      font-weight: bold;
      font-size: 9px;
    }
  </style>
  <h3>
    ASSURANCE
  </h3>

  <ul class="nav nav-tabs ajax">
    <li class="active"><a href="" id="search">Search</a></li>
    <li><a href="/assurance/dispatch_manual" id="1">Dispatch Manual</a></li>
    <!-- /* <li><a href="/assurance/close_list/{{date('Y-m')}}">close</a></li> */ -->

  </ul>
  <br/>
  <div id="txtHint">


  <div>
    <form class="form-group" method="post" id="submit-form" action="/assurance/search" >
      <label for="Search">Search</label>
      <input type="text" name="search" class="form-control" id="Search" value="">
      <button class="btn form-control btn-primary">Search</button>
    </form>
  </div>


  @if ($data<>NULL)
    <br />
    {{ $data }}
      @if (count($query)>0)
      <div class="table-responsive">
        <table class="table table-striped table-bordered dataTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Workorder</th>
              <th>Hasil Ukur</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($query as $result)
            <tr>
              <td>
                @if(!empty($result->uraian))
                    @if (session('auth')->level == 2 || session('auth')->level == 15 || session('auth')->level == 46)
                      @if ($result->dispatch_by=='4')
                          <a href="/fulfillment/hapusfg/{{ $result->no_tiket }}" class="label label-danger" data-toggle="tooltip">Hapus WO FG</a>
                          <a href="/fulfillment/redispatch/{{ $result->no_tiket }}" class="label label-info" data-toggle="tooltip" title="{{ $result->uraian }}">Re-Dispatch</a>
                      @else
                          <a href="/assurance/dispatch/{{ $result->no_tiket }}" class="label label-info" data-toggle="tooltip" title="{{ $result->uraian }}">Re-Dispatch</a>
                      @endif
                    @endif
                    <span class="label label-warning">{{ $result->uraian }}</span>
                  @else
                    <a href="/assurance/dispatch/{{ $result->no_tiket }}" class="label label-info">Dispatch</a>
                  @endif  

                  @if(session('auth')->level == 2)
                     <a href="{{ route('ticket.hapus',[$result->no_tiket]) }}" class="label label-danger">Hapus</a>
                  @endif
                </td>
              <td>
                @if ($result->dispatch_by=='4')
                  <?php
                      $noTiket = substr($result->no_tiket, 0, 2);
                      if ($noTiket=='IN'){
                          $tiket = $result->no_tiket;
                      }
                      else{
                          $tiket = 'FG '.$result->no_tiket;
                      }
                   ?>

                  <a href="/guarantee/{{ $result->id_dt }}">{{ $tiket ? : 0 }}</a> -
                  {{ $result->no_telp ? : 0 }} -
                  {{ $result->no_speedy ? : 0 }}<br />
                @else
                  <a href="/tiket/{{ $result->id_dt }}">{{ $result->no_tiket ? : 0 }}</a> -
                  {{ $result->no_telp ? : 0 }} -
                  {{ $result->no_speedy ? : 0 }}<br />
                @endif
              </td>
              <td>[]
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      @else
      <br />
      Data Tidak ditemukan.<br />
      <a href="/nossaIn/{{ $id }}" class="btn btn-success">Re-sync from Nossa</a>
      @endif
    <br />
    <br />
  @endif

  </div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />

@endsection
