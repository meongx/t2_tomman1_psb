@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id }}" />
    @endif
    <h3>
      <a href="../dispatch" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Dispatch WO {{ $data->Nama ? : $data->orderName }}
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-regu">Regu</label>
      <input name="id_regu" type="hidden" id="input-regu" class="form-control" value="{{ old('id_regu') ?: @$data->id_regu ?: '' }}" />
      <div class="col-xs-10">{!! $errors->first('id_regu', '<span class="label label-danger">:message</span>') !!}</div>
    </div>
    <div class="form-group">
      <label class="control-label" for="input-timid">Kpro select tim</label>
      <input name="kpro_timid" type="hidden" id="input-timid" class="form-control" value="{{ old('kpro_timid') ?: @$data->kpro_timid ?: '' }}"  />
      <div class="col-xs-10">{!! $errors->first('kpro_timid', '<span class="label label-danger">:message</span>') !!}</div>
    </div>
    <div class="form-group">
      <label class="control-label" for="input-tgl">Tanggal</label>
      <input name="tgl" type="tgl" id="input-tgl" class="form-control" value="{{ old('tgl') ?: @$data->tgl ?: '' }}" />
      <div class="col-xs-10">{!! $errors->first('tgl', '<span class="label label-danger">:message</span>') !!}</div>
    </div>
    <div class="form-group">
      <label class="control-label" for="input-timeslot">Timeslot</label>
		  <input name="timeslot" type="hidden" id="input-timeslot" class="form-control" value="{{ old('dispatch_teknisi_timeslot_id') ?: @$data->dispatch_teknisi_timeslot_id ?: '' }}"  />
      <div class="col-xs-10">{!! $errors->first('timeslot', '<span class="label label-danger">:message</span>') !!}</div>
    </div>
    <div class="form-group">
      <label class="control-label" for="input-manja-status">Status Manja</label>
		  <input name="manja_status" type="hidden" id="input-manja-status" class="form-control" value="<?= $data->manja_status ?>"  />
    </div>

    <div class="form-group">
      <label class="control-label" for="input-timeslot">Manja</label>
		  <textarea name="manja" id="input-manja" class="form-control"><?= $data->manja ?></textarea>
    </div>


    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>
  @if (isset($data->id))
  <form id="delete-form" method="post" autocomplete="off">
    <input name="_method" type="hidden" value="DELETE">
      <div style="margin:40px 0 20px">
        <button class="btn btn-danger">Hapus</button>
      </div>
  </form>
  @endif
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script>
    $(function() {
      $('.btn-danger').click(function() {
        var sure = confirm('Yakin hapus data ?');
        if (sure) {
          $('#delete-form').submit();
        }
      })

      var state = <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Input Regu',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.job+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-regu').select2(regu());

      var timeslot_data = <?= json_encode($timeslot) ?>;
      var timeslot = function() {
        return {
          data: timeslot_data,
          placeholder: 'Input Timeslot',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.dispatch_teknisi_timeslot_id+'</span>'+
                '<strong style="margin-left:5px">'+data.dispatch_teknisi_timeslot+'</strong>';
          }
        }
      }
      $('#input-timeslot').select2(timeslot());

      var manja_data = <?= json_encode($get_manja_status) ?>;
      var manja = function() {
        return {
          data: manja_data,
          placeholder: 'Status Manja',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-manja-status').select2(manja());

      var tim_id = <?= json_encode($kpro_tim) ?>;
      var tim = function() {
        return {
          data: tim_id,
          placeholder: 'Pilih tim kpro',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-timid').select2(tim());

      var day = {
        format: 'yyyy-mm-dd',
        viewMode: 0,
        minViewMode: 0
      };
      $('#input-tgl').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });;
    })
  </script>
@endsection
