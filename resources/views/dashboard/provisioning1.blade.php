@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }
  </style>
  <h3>Dashboard Provisioning TA KALSEL</h3>
  <div class="row">
    <div class="col-sm-12">
      <h6>Report Potensi PS Witel Kalsel {{ date('d M Y') }}</h6>
      <div class="table-responsive">

      </div>
    </div>

  </div>
  <div class="row">
    <div class="col-sm-12 ">
      <div class="panel-body">
        <div class="row">
                          <script type="text/javascript">
                            jQuery(document).ready(function($)
                            {
                              $("#bar-1").dxChart({
                                title: 'Trend PS Witel Kalsel',
                                label : {
                                  visible : true
                                },
                                argumentAxis: {
                                    valueMarginsEnabled: false,
                                    discreteAxisDivisionMode: "crossLabels",
                                    grid: {
                                        visible: true
                                    }
                                },
                                tooltip: {
                                    enabled: true,
                                    customizeTooltip: function (arg) {
                                        return {
                                            text: arg.valueText
                                        };
                                    }
                                },
                                dataSource: [
                                  @foreach ($query as $result)
                                  { day : {{ $result->datex }}, Juli : {{ $result->jumlah }}},
                                  @endforeach
                                  @foreach ($juni as $result)
                                  { day : {{ $result->datex }}, Juni : {{ $result->jumlah }}},
                                  @endforeach
                                  @foreach ($mei as $result)
                                  { day : {{ $result->datex }}, Mei : {{ $result->jumlah }}},
                                  @endforeach
                                  @foreach ($april as $result)
                                  { day : {{ $result->datex }}, April : {{ $result->jumlah }}},
                                  @endforeach
                                  @foreach ($maret as $result)
                                  { day : {{ $result->datex }}, Maret : {{ $result->jumlah }}},
                                  @endforeach
                                  @foreach ($februari as $result)
                                  { day : {{ $result->datex }}, Februari : {{ $result->jumlah }}},
                                  @endforeach
                                  @foreach ($januari as $result)
                                  { day : {{ $result->datex }}, Januari : {{ $result->jumlah }}},
                                  @endforeach

                                ],
                                series: [{
                                  argumentField : "day",
                                  valueField: "Juli",
                                  name : "Juli",
                                  type : "area",
                                  color: '#2ecc71'
            										},{
                                  argumentField : "day",
                                  valueField: "Juni",
                                  name : "Juni",
                                  type : "area",
                                  color: '#ff0000'
            										}
                                ,{
                                  argumentField : "day",
                                  valueField: "Mei",
                                  name : "Mei",
                                  type : "area",
                                  color: '#ffff00'
            										}
                                ,{
                                  argumentField : "day",
                                  valueField: "April",
                                  name : "April",
                                  type : "area",
                                  color: '#bdc3c7'
            										}
                                ,{
                                  argumentField : "day",
                                  valueField: "Maret",
                                  name : "Maret",
                                  type : "area",
                                  color: '#888888'
            										}
                                ,{
                                  argumentField : "day",
                                  valueField: "Februari",
                                  name : "Februari",
                                  type : "area",
                                  color: '#bdc3c7'
            										}
                                ,{
                                  argumentField : "day",
                                  valueField: "Januari",
                                  name : "Januari",
                                  type : "area",
                                  color: '#e74c3c'
            										}
                              ]
                              });
                            });
                          </script>
                          		<div id="bar-1" style="height: 290px; width: 100%;"></div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="panel-body">
        <script type="text/javascript">
          jQuery(document).ready(function($)
          {
            $("#bar-2").dxChart({
              title: 'PS Witel YTD Kalsel',
              label : {
                visible : true
              },
              argumentAxis: {
                  valueMarginsEnabled: false,
                  discreteAxisDivisionMode: "crossLabels",
                  grid: {
                      visible: true
                  }
              },
              tooltip: {
                  enabled: true,
                  customizeTooltip: function (arg) {
                      return {
                          text: arg.valueText
                      };
                  }
              },
              dataSource : [
                @foreach ($ytd as $result)
                { month : "{{ $result->datex }}", Jumlah : {{ $result->jumlah }}},
                @endforeach
              ],
              series: [{
                argumentField : "month",
                valueField: "Jumlah",
                name : "Jumlah",
                type : "line",
                color: '#2ecc71'
              }]
            });
          });
          </script>
          	<div id="bar-2" style="height: 290px; width: 100%;"></div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel-body">
      </div>
    </div>
  </div>
  <div class="row">
  </div>
@endsection
