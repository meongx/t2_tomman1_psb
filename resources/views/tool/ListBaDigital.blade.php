@extends('tech_layout')

@section('content')
	<div class="panel panel-primary">
		<div class="panel panel-heading">Search Ba Digital</div>

		<div class="panel panel-body table-responsive">
			<form class="row" style="margin-bottom: 20px">
	          <div class="col-md-12">
	              <div class="input-group">
	                <input type="text" class="form-control" placeholder="Masukkan No. WO" name="sc"/>
	                  <span class="input-group-btn">
	                    <button class="btn btn-primary" type="submit">
	                      <span class="glyphicon glyphicon-search"></span>
	                    </button>
	                  </span>
	              </div>
	          </div>
	        </form>

			@if($ket==0)
				<table class="table table-bordered">
					<tr>
						<th>STO</th>
						<th>Regional</th>
						<th>Fiberzone</th>
						<th>Witel</th>
						<th>Tgl</th>
						<th>Mitra</th>
						<th>Pelanggan</th>
						<th>No. Wo</th>
						<th>No. Telp 1</th>
						<th>No. Telp 2</th>
						<th>No. Inet</th>
						<th>Layanan</th>
						<th>Nik Teknisi</th>
					</tr>
		
					@foreach($getData as $data)
						<tr>
							<td>{{ $data->sto }}</td>
							<td>{{ $data->regional }}</td>
							<td>{{ $data->fiberzone }}</td>
							<td>{{ $data->witel }}</td>
							<td>{{ $data->tgl }}</td>
							<td>{{ $data->mitra }}</td>
							<td>{{ $data->pelanggan }}</td>
							<td>{{ $data->no_wo }}</td>
							<td>{{ $data->noTelp_pelanggan1 }}</td>
							<td>{{ $data->noTelp_pelanggan2 }}</td>
							<td>{{ $data->no_inet }}</td>
							<td>{{ $data->layanan }}</td>
							<td>{{ $data->nikTeknisi }}</td>
						</tr>
					@endforeach
				</table>
			@elseif($ket==1)
				<p>Data Tidak Ditemukan</p>
			@endif

		</div>
	</div>
@endsection
