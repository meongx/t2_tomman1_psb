@extends('layout')

@section('content')
  @include('partial.alerts')
  <a href="/dashboard/assurance/{{ $date }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List Assurance {{ $status }} ({{ $date }})</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>No.</th>
          <th>No Tiket</th>
          <th>Hasil Ukur</th>
          <th>Tiket</th>
          <th>Tim</th>
          <th>Sektor</th>
          <th>Order</th>
          <th>Headline</th>
          <th>Tgl_Open</th>
          <th>Tgl Close</th>
          <th>Status Laporan</th>
          <th>Segment Status</th>
          <th>Status Nossa</th>
          <th>STO</th>
          <th>Umur</th>
          <th>Sebab</th>
          <th>Action</th>
          <th>Catatan Tek.</th>
          <th>HD</th>
          <th>Teknisi</th>
        </tr>
        @foreach ($getAssuranceList as $num => $AssuranceList)
        <tr>
          <td>{{ ++$num }}</td>
          <td><a href="/tiket/{{ $AssuranceList->id_dt }}">{{ $AssuranceList->Ndem }}</a></td>
          <td>{{ $AssuranceList->status_ukur }}</td>
          <td>{{ $AssuranceList->updated_at }}</td>
          <td>
            {{ $AssuranceList->uraian }}
          </td>
          <td>
            {{ $AssuranceList->sektor }}
          </td>
          <td>
            {{ $AssuranceList->Service_No }}
          </td>
          <td>{{ $AssuranceList->headline }}</td>
          <td>{{ $AssuranceList->tgl_open }}</td>

          @if ($AssuranceList->tglClose<>'' || $AssuranceList->tglCloseCreate<>'' )
            @if ($AssuranceList->tglClose=='')
              <td>{{ date('Y-m-d',strtotime($AssuranceList->tglCloseCreate)) }}</td>
            @else
              <td>{{ date('Y-m-d',strtotime($AssuranceList->tglClose)) }}</td>
            @endif
          @else
            <td></td>
          @endif
          <td>{{ $AssuranceList->laporan_status ? : 'NO UPDATE' }}</td>
          <td>{{ $AssuranceList->Segment_Status ? : '-' }}</td>
          <td>{{ $AssuranceList->aaIncident ? : 'CLOSED' }}</td>
          <td>{{ $AssuranceList->sto }}</td>
          <td>{{ $AssuranceList->umur }}</td>
          <td>{{ $AssuranceList->penyebab }}</td>
          <td>{{ $AssuranceList->action }}</td>
          <td>{{ $AssuranceList->catatan }}</td>
          <td>{{ $AssuranceList->jenis_ont ? : '-' }}</td>
          <td>{{ $AssuranceList->typeont ? : '-' }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    </div>
  </div>
@endsection
