@extends('layout')

@section('content')
  @include('partial.alerts')
<style>
.verticalTableHeader {
    text-align:center;
    white-space:nowrap;
    transform: rotate(270deg);

}
.verticalTableHeader p {
    margin:0 -100% ;
    display:inline-block;
}
.verticalTableHeader p:before{
    content:'';
    width:0;
    padding-top:110%;/* takes width as reference, + 10% for faking some extra padding */
    display:inline-block;
    vertical-align:middle;
    text-align : right;
}

</style>
<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<div class="row">
    <div class="col-md-12" id="bio_tl">
      <div class="panel panel-default">
          <div class="panel-heading">
          Profile
          </div>
          <div class="panel-body table-responsive">
            <table style="padding:10px;">
              <tr>
                <td style="padding:15px">
                  <img src="/image/people.png" width="100" />
                </td>
                <td style="padding:10px">

                  {{ session('auth')->nama }}<br />
                  @foreach ($group_telegram as $gt)
                  <span class="label label-info">{{ $gt->title }}</span>
                  @endforeach
                  <br />
                  <small>
                    Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                  </small>
                </td>
                <td style="padding : 10px" valign=top>

                </td>
              </tr>

            </table>
            @include ('partial.home')

            <div class="col-sm-6">
              <h3>Dashboard TL </h3>

              @if (session('auth')->level==2 || session('auth')->level==15 )
                <form method="get">
                    <div class="row">
                      <div class="col-md-2">
                          <select class="form-control" name="sektor">
                            <option value="1" {{ $ketDashboard==1 ? 'selected' : ''}}>BBR</option>
                            <option value="2" {{ $ketDashboard==2 ? 'selected' : ''}}>BJM</option>
                            <option value="5" {{ $ketDashboard==5 ? 'selected' : ''}}>ULI</option>
                            <option value="4" {{ $ketDashboard==4 ? 'selected' : ''}}>BLC</option>
                            <option value="3" {{ $ketDashboard==3 ? 'selected' : ''}}>TJL</option>
                          </select>
                      </div>
                          <input type="submit" class="btn btn-primary" value="Filter">
                    </div>
                </form>
              @endif
              
                @if (count($data)>1)
                <table class="table table-striped table-bordered dataTable">
                  <tr></tr>
                    <th>HDESK</th>
                    <th>Status</th>

                    @if ($ketDashboard==1)
                        <th>WAHYU [BBR1]</th>
                        <th>ROKHMAN [BBR2]</th>
                        <th>ARIE [BBR3]</th>
                    @elseif ($ketDashboard==2)
                        <th>JUNAIDI [BJM1]</th>
                        <th>HASAN [BJM2]</th>
                        <th>TAMAMI [BJM3]</th>
                        <th>IQBAL [BJM4]</th>
                    @elseif ($ketDashboard==3)
                        <th>SARPANI [KDG]</th>
                        <th>HIFNI [TJL]</th>
                    @elseif ($ketDashboard==4)
                        <th>SAMSUDIN [BLC]</th>
                    @elseif ($ketDashboard==5)
                        <th>ARIF [ULI1]</th>
                        <th>VHERDA [ULI2]</th>
                        <th>RESTU [ULI3]</th>
                        <th>VHERDA [ULI4]</th>
                        <th>HASBULLAH [ULI5]</th>
                        <th>IRWAN [ULI6]</th>     
                    @endif

                    <th>JUMLAH</th>
                  </tr>
                  <?php
                    $no_update_jumlah = 0;
                    $total = 0;
                    $totalAll = 0;
                    
                    if ($ketDashboard==1){
                       $no_update_wahyu = 0;
                       $no_update_rokhman = 0;
                       $no_update_arie = 0;
                        
                       $total_wahyu = 0;
                       $total_rokhman = 0;
                       $total_arie = 0;
                    }
                    elseif($ketDashboard==2){
                       $no_update_junaidi = 0;
                       $no_update_hasan = 0;
                       $no_update_tamami = 0;
                       $no_update_iqbal = 0;

                       $total_junaidi = 0;
                       $total_hasan = 0;
                       $total_tamami = 0;
                       $total_iqbal = 0;  
                    }
                    elseif($ketDashboard==3){
                       $no_update_sarpani = 0;
                       $total_sarpani = 0;

                       $no_update_hifni = 0;
                       $total_hifni = 0;
                    }
                    elseif($ketDashboard==4){
                        $no_update_samsudin = 0;
                        $total_samsudin = 0;                        
                    }
                    elseif($ketDashboard==5){
                       $no_update_arif = 0;
                       $no_update_vherda = 0;
                       $no_update_restu = 0;
                       $no_update_vherda2 = 0;
                       $no_update_hasbullah = 0;
                       $no_update_irwan = 0;

                       $total_arif = 0;
                       $total_vherda = 0;
                       $total_restu = 0;
                       $total_vherda2 = 0;
                       $total_hasbullah = 0;
                       $total_irwan = 0;
                      
                    };
                  ?>
                  
                  @if($ketDashboard==2)
                    <tr><td rowspan="{{ count($data)+3}}" valign='middle' align='center'>RINA & NAJIB</td></tr>
                  @elseif($ketDashboard==5)
                    <tr><td rowspan="{{ count($data)+3}}" valign='middle' align='center'>GILANG & ALVIAN</td></tr>
                  @elseif($ketDashboard==1)
                    <tr><td rowspan="{{ count($data)+3}}" valign='middle' align='center'>DAYAT & FAHKRUL</td></tr>
                  @elseif($ketDashboard==4)
                    <tr><td rowspan="{{ count($data)+3}}" valign='middle' align='center'>ETTY & ALIF</td></tr>
                  @elseif($ketDashboard==3)
                    <tr><td rowspan="{{ count($data)+3}}" valign='middle' align='center'>PRAS & HAPIP</td></tr>
                  @endif 

                  @foreach ($data as $result)
                    @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                    <tr>
                      <td>{{ $result->laporan_status }}</td>
                        @if ($ketDashboard==1)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-253578253">{{ $result->woTlWahyu }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-1001405220793">{{ $result->woTlRokhman }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-1001188642735">{{ $result->woTlArie }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/1">{{ $result->jumlah }}</a></td>
                            <?php
                                $total_wahyu += $result->woTlWahyu;
                                $total_rokhman += $result->woTlRokhman;
                                $total_arie += $result->woTlArie;
                                $total += $result->jumlah;
                             ?>
                        @elseif($ketDashboard==2)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-283767249">{{ $result->woTlJunai }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-348628322">{{ $result->woTlHasan }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-1001155521550">{{ $result->woTlTamami }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-1001318576095">{{ $result->woTlIqbal }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/2">{{ $result->jumlah }}</a></td>
                            <?php
                                $total_junaidi += $result->woTlJunai;
                                $total_hasan += $result->woTlHasan;
                                $total_tamami += $result->woTlTamami;
                                $total_iqbal += $result->woTlIqbal;
                                $total += $result->jumlah;
                             ?>
                        @elseif($ketDashboard==3)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-114836111">{{ $result->woTlSarpani }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-1001077578882">{{ $result->woTlHifni }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/3">{{ $result->jumlah }}</a></td>
                            <?php
                                $total_sarpani += $result->woTlSarpani;
                                $total_hifni += $result->woTlHifni;
                                $total += $result->jumlah;
                             ?>
                        @elseif($ketDashboard==4)       
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-1001186288135">{{ $result->woTlSamsudin }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/4">{{ $result->jumlah }}</a></td>
                            <?php
                                $total_samsudin += $result->woTlSamsudin;     
                                $total += $result->jumlah;
                             ?>
                        @elseif($ketDashboard==5)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-1001499053925">{{ $result->woTlArif }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-1001439986938">{{ $result->woTlVherda }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-307695867">{{ $result->woTlRestu }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-1001194255648">{{ $result->woTlVherda2 }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-316682210">{{ $result->woTlHasbullah }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/-242478590">{{ $result->woTlIrwan }}</a></td>                            
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/{{ $result->laporan_status }}/5">{{ $result->jumlah }}</a></td>
                            <?php
                                $total_arif += $result->woTlArif;
                                $total_vherda += $result->woTlVherda;
                                $total_restu += $result->woTlRestu;
                                $total_vherda2 += $result->woTlVherda2;
                                $total_hasbullah += $result->woTlHasbullah;
                                $total_irwan += $result->woTlIrwan;
                                $total += $result->jumlah;
                             ?>
                        @endif
                    </tr>
                    @else
                         @if ($ketDashboard==1)
                            <?php
                                $no_update_wahyu += $result->woTlWahyu;
                                $no_update_rokhman += $result->woTlRokhman;
                                $no_update_arie += $result->woTlArie;
                                $no_update_jumlah += $result->jumlah;

                                $totalAll = $total + $no_update_jumlah;
                             ?>
                        @elseif($ketDashboard==2)
                            <?php
                                $no_update_junaidi += $result->woTlJunai;
                                $no_update_hasan += $result->woTlHasan;
                                $no_update_tamami += $result->woTlTamami;
                                $no_update_iqbal += $result->woTlIqbal;
                                $no_update_jumlah += $result->jumlah;

                                $totalAll = $total + $no_update_jumlah;
                             ?>
                        @elseif($ketDashboard==3)
                            <?php
                                $no_update_sarpani += $result->woTlSarpani;
                                $no_update_hifni += $result->woTlHifni;
                                $no_update_jumlah += $result->jumlah;

                                $totalAll = $total + $no_update_jumlah;
                             ?>
                        @elseif($ketDashboard==4)
                            <?php
                                $no_update_samsudin += $result->woTlSamsudin;
                                $no_update_jumlah += $result->jumlah;

                                $totalAll = $total + $no_update_jumlah;
                             ?>
                        @elseif($ketDashboard==5)
                            <?php
                                $no_update_arif += $result->woTlArif;
                                $no_update_vherda += $result->woTlVherda;
                                $no_update_restu += $result->woTlRestu;
                                $no_update_vherda2 += $result->woTlVherda2;
                                $no_update_hasbullah += $result->woTlHasbullah;
                                $no_update_irwan += $result->woTlIrwan;
                                $no_update_jumlah += $result->jumlah;

                                $totalAll = $total + $no_update_jumlah;
                             ?>
                        @endif
                    @endif
                  @endforeach
        
                  <tr>
                    <td>NEED PROGRESS / NO UPDATE</td>
                    @if($ketDashboard==1)      
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-253578253">{{ $no_update_wahyu }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-1001405220793">{{ $no_update_rokhman }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-1001188642735">{{ $no_update_arie }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/1">{{ $no_update_jumlah }}</a></td>
                      @elseif($ketDashboard==2)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-283767249">{{ $no_update_junaidi }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-348628322">{{ $no_update_hasan }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-1001155521550">{{ $no_update_tamami }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-1001318576095">{{ $no_update_iqbal }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/2">{{ $no_update_jumlah }}</a></td>
                      @elseif($ketDashboard==3)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-114836111">{{ $no_update_sarpani }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-1001077578882">{{ $no_update_hifni }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/3">{{ $no_update_jumlah }}</a></td>
                      @elseif($ketDashboard==4)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-1001186288135">{{ $no_update_samsudin }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/4">{{ $no_update_jumlah }}</a></td>
                      @elseif($ketDashboard==5)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-1001499053925">{{ $no_update_arif }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-1001439986938">{{ $no_update_vherda }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-307695867">{{ $no_update_restu }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-1001194255648">{{ $no_update_vherda2 }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-316682210">{{ $no_update_hasbullah }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/-242478590">{{ $no_update_irwan }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/NOUPDATE/5">{{ $no_update_jumlah }}</a></td>
                      @endif
                  </tr>

                  <tr>
                      <td>Total</td>
                      @if($ketDashboard==1)      
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-253578253">{{ $total_wahyu + $no_update_wahyu }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-1001405220793">{{ $total_rokhman + $no_update_rokhman }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-1001188642735">{{ $total_arie + $no_update_arie }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/1">{{ $total + $no_update_wahyu + $no_update_rokhman + $no_update_arie }}</a></td>
                      @elseif($ketDashboard==2)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-283767249">{{ $total_junaidi + $no_update_junaidi }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-348628322">{{ $total_hasan + $no_update_hasan }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-1001155521550">{{ $total_tamami + $no_update_tamami }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-1001318576095">{{ $total_iqbal + $no_update_iqbal }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/2">{{ $total + $no_update_junaidi + $no_update_hasan + $no_update_tamami + $no_update_iqbal }}</a></td>
                      @elseif($ketDashboard==3)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-283767249">{{ $total_sarpani + $no_update_sarpani}}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-1001077578882">{{ $total_hifni + $no_update_hifni}}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/3">{{ $total + $no_update_sarpani + $no_update_hifni }}</a></td>
                      @elseif($ketDashboard==4)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-1001186288135">{{ $total_samsudin + $no_update_samsudin }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/4">{{ $total + $no_update_samsudin }}</a></td>
                      @elseif($ketDashboard==5)
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-1001499053925">{{ $total_arif + $no_update_arif }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-1001439986938">{{ $total_vherda + $no_update_vherda }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-307695867">{{ $total_restu + $no_update_restu }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-1001194255648">{{ $total_vherda2 + $no_update_vherda2 }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-316682210">{{ $total_hasbullah + $no_update_hasbullah  }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/-242478590">{{ $total_irwan }}</a></td>
                            <td><a href="/dashboard/teritorilist/{{ $tgl }}/ALL/ALL/5">{{ $total + $no_update_arif + $no_update_vherda + $no_update_restu + $no_update_vherda2 + $no_update_hasbullah }}</a></td>
                      @endif

                  </tr>
                </table>
                @endif
             </div> 

        </div>
    </div>
@endsection