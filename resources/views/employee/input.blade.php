@extends('layout')

@section('content')
@if ($auth->level==2 || (session('auth')->id_user == '89150015' && session('auth')->level == 46 ))
  @include('partial.alerts')
  <style>
	  .panel-content {
		  padding : 10px;
	  }
	</style>
	<link rel="stylesheet" href="/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
  <script src="/bower_components/select2/select2.min.js"></script>

	<h3><a href="/employee" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Employee</h3>
	<div class="panel panel-primary">
		<div class="panel-heading">Input</div>
		<div class="panel-content">
			<form method="post" action="/employee/input">
				<input type="hidden" name="id_people" id="id_people" class="form-control" value="{{ @$data->id_people }}" />
				<div class="form-group">
					<label for="nik">NIK</label>
					<input type="text" name="nik" id="nik" class="form-control" value="{{ @$data->nik }}" />
				</div>
				<div class="form-group">
					<label for="nama">NAMA</label>
					<input type="text" name="nama" class="form-control" value="{{ @$data->nama }}" />
				</div>
				<div class="form-group">
					<label for="laborcode">LABORCODE</label>
					<input type="text" name="laborcode" class="form-control" value="{{ @$data->laborcode }}" />
				</div>
				<div class="form-group">
					<label for="atasan_1">ATASAN 1</label>
					<input type="text" name="atasan_1" class="form-control" value="{{ @$data->atasan_1 }}" />
				</div>
        <div class="form-group">
					<label for="squad">SQUAD</label>
					<select name="squad" class="form-control">
						<option value=""> -- </option>
						@foreach ($getSQUAD as $SQUAD)
            <?php
              if ($SQUAD->squad_id==@$data->squad) { $selected = 'selected = "selected"'; } else { $selected = ''; }
            ?>
						<option value="{{ $SQUAD->squad_id }}" {{ $selected }}>{{ $SQUAD->squad }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="cat_job">CAT JOB</label>
					<select name="cat_job" class="form-control">
						<option value=""> -- </option>
						@foreach ($getDistinctCATJOB as $CATJOB)
						<?php
							if ($CATJOB->id==@$data->CAT_JOB){
								$selected = 'selected = "selected"';
	            } else {
		            $selected = '';
	            }	
						?>
						<option value="{{ $CATJOB->id }}" {{ $selected }}>{{ $CATJOB->text }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="cat_job">Sub Directorat</label>
					<select name="cat_job" class="form-control">
						<option value=""> -- </option>
						@foreach ($getDistinctSubDirectorat as $SubDirectorat)
						<?php
							if ($SubDirectorat->id==@$data->sub_directorat){
								$selected = 'selected = "selected"';
	            } else {
		            $selected = '';
	            }	
						?>
						<option value="{{ $SubDirectorat->id }}" {{ $selected }}>{{ $SubDirectorat->text }}</option>
						@endforeach
					</select>
				</div>
        <div class="form-group">
          <label for="mitra_amija">Mitra Amija</label>
          <select class="form-control" name="mitra_amija">
            <option value=""> -- </option>
            @foreach ($getMitraAmija as $MitraAmija)
            <?php
	            if ($MitraAmija->mitra_amija == @$data->mitra_amija){
		            $selected = 'selected = "selected"';
	            } else {
		            $selected = '';
	            }	
	          ?>
            <option value="{{ $MitraAmija->mitra_amija }}" {{ $selected }}>{{ $MitraAmija->mitra_amija_pt }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="nik_amija">NIK Amija</label>
          <input type="text" name="nik_amija" class="form-control" value="{{ @$data->nik_amija }}">
        </div>
        <div class="form-group">
          <label for="nik_amija">Witel</label>
					<select class="form-control" name="witel">
						<option value=""> -- </option>
						@foreach ($getWitel as $Witel)
						<?php
	            if ($Witel->witel == @$data->Witel_New){
		            $selected = 'selected = "selected"';
	            } else {
		            $selected = '';
	            }	
	          ?>
						<option value="{{ $Witel->witel }}" {{ $selected }}>{{ $Witel->witel }}</option>
						@endforeach
					</select>
        </div>
        
				<div class="form-group">
					<input type="submit" name="submit" class="btn btn-s btn-success" />
				</div>
			</form>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			var psa = <?= json_encode($getDistinctPSA) ?>;
			$('#psa').select2(select2Options(psa));
		});
	</script>
@else
    <h1>Banyaki Bawa Beibadah Ja Parak Kiamat !!!</h1>
@endif
@endsection
