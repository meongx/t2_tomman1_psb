<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class ItemController extends Controller
{

  public function index()
  {
    $list = DB::select('
      SELECT *
      FROM item
      WHERE kat=9 and status=0
    ');

    return view('item.list', compact('list'));
  }
  public function nonaktif()
  {
    $list = DB::select('
      SELECT *
      FROM item
      WHERE kat=9 and status!=0
    ');

    return view('item.list', compact('list'));
  }
  public function create()
  {
    $data = new \StdClass;
    $data->id_item = null;
    $data->nama_item = null;
    $data->unit_item = null;
    $data->status = null;
    return view('item.input', compact('data'));
  }
  public function input($id)
  {
    $exists = DB::select('
      SELECT *
      FROM item
      WHERE id_item = ?
    ',[
      $id
    ]);
    $data = $exists[0];
    return view('item.input', compact('data'));
  }

  public function save(Request $request, $id)
  {
    $exists = DB::select('
      SELECT *
      FROM item
      WHERE id_item = ?
    ',[
      $id
    ]);
    
    if (count($exists)) {
      $data = $exists[0];
      DB::transaction(function() use($request, $data) {
        DB::table('item')
          ->where('id_item', $data->id_item)
          ->update([
            'nama_item'  => $request->input('nama_item'),
            'unit_item' => $request->input('unit_item'),
            'status'    => $request->input('status')
          ]);
      });
      return redirect('/item')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }
    else {
      DB::transaction(function() use($request) {
        DB::table('item')->insert([
          'id_item'   => $request->input('id_item'),
          'nama_item' => $request->input('nama_item'),
          'unit_item' => $request->input('unit_item'),
          'status' => $request->input('status'),
          'kat'       => 9
        ]);
      });
      return redirect('/item')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }
  }
}
