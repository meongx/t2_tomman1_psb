@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>Tambah Progress Mitra </h3><hr>

  <form method="post" autocomplete enctype="multipart/form-data">
      <div class="row">
        <div class="form-group">
            <div class="col-md-12 {{ $errors->has('nmCluster') ? 'has-error' : '' }}">
                Cluster
                <select name="nmCluster" class="form-control" id="nmCluster">
                    <option value=""></option>
                    @foreach($listCluster as $cluster)
                        <option value="{{ $cluster->id_cluster }}">{{ $cluster->nama_cluster }}</option>
                    @endforeach
                </select>
                {!! $errors->first('nmCluster','<p class=help-block>:message</p>') !!}
            </div>

            <div class="col-md-12 {{ $errors->has('regu') ? 'has-error' : '' }}">
                Regu
                <select name="regu" class="form-control" id="regu">
                    <option value=""></option>
                    @foreach($listRegu as $regu)
                        <option value="{{ $regu->id_regu }}">{{ $regu->uraian }}</option>
                    @endforeach
                </select>
                {!! $errors->first('regu','<p class=help-block>:message</p>') !!}
            </div>

            <div class="col-md-4 {{ $errors->has('progress') ? 'has-error' : '' }}">
                Progress (%)
                <input type="text" name="progress" class="form-control"> 
                {!! $errors->first('progress','<p class=help-block>:message</p>') !!}
            </div>

        <br><br><br><br><br><br><br><br>
        </div>

        <div class="col-md-12">
            <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
            <a href="/migrasi/monitoring/progress" class="btn btn-default btn-sm">Batal</a>
        </div>
      </div>
  </form>
@endsection

@section('plugins')
  <script type="text/javascript">
      $(document).ready(function(){
          $('#nmCluster').select2();
          $('#regu').select2();
      })
  </script>
@endsection