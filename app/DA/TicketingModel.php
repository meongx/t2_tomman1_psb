<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class TicketingModel{

  public static function get_keluhan($id){
  $keluhan = DB::SELECT('select * from keluhan where keluhan = "'.$id.'"');
  return $keluhan;
  }

  public static function dashboardCloseList($status,$sektor,$date){
    $where_sektor = '';
    if ($sektor=="UNDEFINED"){
      $where_sektor = ' AND f.sektor_asr IS NULL';
    } else if ($sektor<>"ALL"){
      $where_sektor = ' AND f.sektor_asr = "'.$sektor.'"';
    }

    $where_status = '';
    if ($status<>"ALL"){
      SWITCH ($status){
        case "UNDISPATCH" : 
          $where_status = 'AND a.id IS NULL';
        break;
        case "SISA" : 
          $where_status = 'AND (c.grup = "SISA" OR (a.id IS NOT NULL AND b.status_laporan IS NULL))';
        break;
        case "UP" : 
          $where_status = 'AND (b.status_laporan = 1 AND a.tgl like "'.$date.'%")';
        break;
        default : 
          $where_status = 'AND c.grup = "'.$status.'"';
        break;
      }
    }
    $query = DB::SELECT('SELECT *,a.id as id_dt, e.* FROM  
       data_nossa_1_log e 
      LEFT JOIN  dispatch_teknisi a ON a.Ndem = e.Incident
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
      LEFT JOIN psb_laporan_action d ON b.action = d.laporan_action_id
      LEFT JOIN psb_laporan_penyebab h ON b.penyebabId = h.idPenyebab
      LEFT JOIN mdf f ON e.Workzone = f.mdf
      LEFT JOIN regu g ON a.id_regu = g.id_regu
      WHERE 
        e.Source = "TOMMAN" 
        '.$where_sektor.'
        '.$where_status.'
      ');
    return $query;
  }

  public static function dashboardClose($date){
    return DB::SELECT('
      SELECT 
      f.sektor_asr,
      SUM(CASE WHEN  a.id IS NULL THEN 1 ELSE 0 END) as UNDISPATCH,
      SUM(CASE WHEN (c.grup = "SISA" OR (a.id IS NOT NULL AND b.status_laporan IS NULL)) THEN 1 ELSE 0 END) as SISA,
      SUM(CASE WHEN c.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
      SUM(CASE WHEN c.grup = "KT" THEN 1 ELSE 0 END) as KT,
      SUM(CASE WHEN c.grup = "KP" THEN 1 ELSE 0 END) as KP,
      SUM(CASE WHEN c.grup = "UP" THEN 1 ELSE 0 END) as UP 
      FROM 
        data_nossa_1_log e 
      LEFT JOIN dispatch_teknisi a ON a.Ndem = e.Incident
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
      LEFT JOIN psb_laporan_action d ON b.action = d.laporan_action_id
      LEFT JOIN mdf f ON e.Workzone = f.mdf
      LEFT JOIN regu g ON a.id_regu = g.id_regu
      WHERE 
        e.Source = "TOMMAN" AND (
        (b.status_laporan = 1 AND date(a.tgl) LIKE "'.$date.'%") OR 
        (c.grup IN ("KP","KT","OGP")) OR
        (b.status_laporan IS NULL) OR 
        a.id IS NULL
      ) 

      GROUP BY f.sektor_asr
      ');
  }

  public static function get_history_order($id){
    $sql = 'SELECT 
    *,b.id as id_dt FROM data_nossa_1_log a 
    LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
    LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
    LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
    LEFT JOIN psb_laporan_action e ON c.action = e.laporan_action_id
    LEFT JOIN psb_laporan_penyebab f ON c.penyebabId = f.idPenyebab
    LEFT JOIN regu g ON b.id_regu = g.id_regu
    WHERE a.Service_No = "'.$id.'"
    ORDER BY a.Reported_Date DESC
    ';
    return DB::SELECT($sql);
  }

  public static function get_customer_info($id){
      $data = new \StdClass;
      $data->orderName = null;
      $data->ndemPots = null;
      $data->ndemSpeedy = null;
      $data->orderAddr = null;
      $data->kcontact = null;
      $data->alproname = null;
      $data->sto = null;
      $data->internet = null;
      $data->orderDatePs = null;
      $data->orderId = null;
      $data->id_dt = null;

      $sql = 'SELECT 
                *,
                b.id as id_dt
              FROM 
                Data_Pelanggan_Starclick a
              LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem 
              WHERE 
                a.orderId IS NOT NULL AND 
                a.orderStatus="Completed (PS)" AND 
                a.jenisPsb = "AO|BUNDLING" AND 
                (a.ndemSpeedy LIKE "%'.$id.'%" OR a.ndemPots LIKE "%'.$id.'%")
              ORDER BY 
                a.orderDate DESC 
              ';

      $getData = DB::SELECT($sql);
      if (count($getData)<>0){
          $data->orderName  = $getData[0]->orderName;
          $data->ndemPots   = $getData[0]->ndemPots;
          $data->ndemSpeedy = $getData[0]->ndemSpeedy;
          $data->orderAddr  = $getData[0]->orderAddr;
          $data->kcontact   = $getData[0]->kcontact;
          $data->alproname  = $getData[0]->alproname;
          $data->sto        = $getData[0]->sto;
          $data->internet   = $getData[0]->internet;
          $data->orderDatePs   = $getData[0]->orderDatePs;
          $data->orderId   = $getData[0]->orderId;
      }
      else{
          $getData = DB::select('SELECT * FROM dossier_master WHERE ND LIKE "%'.$id.'%" OR ND_REFERENCE LIKE "%'.$id.'%"');
          if (count($getData)<>0){
              $data->orderName  = $getData[0]->NAMA;
              $data->ndemPots   = $getData[0]->ND;
              $data->ndemSpeedy = $getData[0]->ND_REFERENCE;
              $data->orderAddr  = $getData[0]->LCOM;
              $data->kcontact   = $getData[0]->NCLI.'-'.$getData[0]->ND.'-'.$getData[0]->ND_REFERENCE;     
              $data->alproname  = null; 
              $data->sto        = $getData[0]->STO;
              $data->internet   = $getData[0]->ND_REFERENCE;
          }
      }

      return $data;
  }

  public static function dashboard(){
  	date_default_timezone_set('Asia/Makassar');
  	$date_now = date('Y-m-d H:i:s');
  	return DB::SELECT('
  		SELECT 
  		  d.sektor_asr,
  		  count(*) as jumlah,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=0 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 2 THEN 1 ELSE 0 END) as x0dan2,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=2 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 4 THEN 1 ELSE 0 END) as x2dan4,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=4 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 6 THEN 1 ELSE 0 END) as x4dan6,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=6 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 8 THEN 1 ELSE 0 END) as x6dan8,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=8 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 10 THEN 1 ELSE 0 END) as x8dan10,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=10 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 12 THEN 1 ELSE 0 END) as x10dan12,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=12 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 24 THEN 1 ELSE 0 END) as x12dan24,
  		  SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=24 THEN 1 ELSE 0 END) as lebih24,
  		  SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") <24 THEN 1 ELSE 0 END) as kurang24
  		FROM 
  			data_nossa_1_log a
  			LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
  			LEFT JOIN regu c ON b.id_regu = c.id_regu
  			LEFT JOIN mdf d ON a.Workzone = d.mdf  
  			LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
  		WHERE 
        a.Incident <> "" AND
  			a.Source = "TOMMAN" AND 
  			(e.status_laporan <> 1 OR e.id_tbl_mj is NULL)
  		GROUP BY 
  		d.sektor_asr
  		');
  }

  public static function listPersonal($auth){
  	if ($auth->id_karyawan=="91153709") {
  		$where_id_karyawan = ' AND a.Source = "TOMMAN"';
  	} else {
  		$where_id_karyawan = ' AND a.user_created = "'.$auth->id_karyawan.'"';
  	}
  	return DB::SELECT('
  		SELECT * FROM data_nossa_1_log a 
  		LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
  		LEFT JOIN regu c ON b.id_regu = c.id_regu 
  		LEFT JOIN psb_laporan d ON b.id = d.id_tbl_mj
  		LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
  		WHERE d.status_laporan <> 1 
  		'.$where_id_karyawan.'
  		');
  }

  public static function dashboardDetail($sektor, $status){
    date_default_timezone_set('Asia/Makassar');
    $date_now = date('Y-m-d H:i:s');

    $where_status = '';
    if ($status=='x0dan2'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=0 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 2)';
    }
    elseif($status=='x2dan4'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=2 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 4)';
    }
    elseif($status=='x4dan6'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=4 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 6)';
    }
    elseif($status=='x6dan8'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=6 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 8)';
    }
    elseif($status=='x8dan10'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=8 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 10)';
    }
    elseif($status=='x10dan12'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=10 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 12)';
    }
    elseif($status=='x12dan24'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=12 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 24)';
    }
    elseif($status=='lebih24'){
       $where_status = ' AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=24';
    }
    elseif($status=='kurang24'){
       $where_status = ' AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 24';
    };

    $where_sektor = '';
    if($sektor<>'ALL'){
        $where_sektor = ' AND d.sektor_asr="'.$sektor.'"';
    };    

    $sql = 'SELECT 
              *,
              b.id as id_dt
            FROM 
              data_nossa_1_log a
              LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
              LEFT JOIN regu c ON b.id_regu = c.id_regu
              LEFT JOIN mdf d ON a.Workzone = d.mdf  
              LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
              LEFT JOIN psb_laporan_status f ON e.status_laporan=f.laporan_status_id
            WHERE 
              a.Source = "TOMMAN" AND 
              (e.status_laporan <> 1 OR e.id_tbl_mj is NULL)
              '.$where_status.'
              '.$where_sektor.'
            ';
    $data = DB::select($sql);
    return $data;
  }

  public static function getUndispatchList(){
      return DB::select('SELECT * FROM data_nossa_1_log WHERE Incident NOT IN (SELECT Ndem FROM dispatch_teknisi) AND Incident LIKE "%INT%"');
  }

   public static function getStoUndispatchList(){
      return DB::select('SELECT DISTINCT Workzone FROM data_nossa_1_log WHERE Incident NOT IN (SELECT Ndem FROM dispatch_teknisi) AND Incident LIKE "%INT%"');
  }

  public static function getJumlahUndispatch(){
    date_default_timezone_set('Asia/Makassar');
    $date_now = date('Y-m-d H:i:s');
    return DB::SELECT('
      SELECT 
        count(*) as jumlah,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=0 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 2 THEN 1 ELSE 0 END) as x0dan2,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=2 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 4 THEN 1 ELSE 0 END) as x2dan4,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=4 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 6 THEN 1 ELSE 0 END) as x4dan6,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=6 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 8 THEN 1 ELSE 0 END) as x6dan8,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=8 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 10 THEN 1 ELSE 0 END) as x8dan10,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=10 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 12 THEN 1 ELSE 0 END) as x10dan12,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=12 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 24 THEN 1 ELSE 0 END) as x12dan24,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=24 THEN 1 ELSE 0 END) as lebih24,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") <24 THEN 1 ELSE 0 END) as kurang24
      FROM 
        data_nossa_1_log a
        LEFT JOIN mdf d ON a.Workzone = d.mdf  
      WHERE 
        a.Incident NOT IN (SELECT Ndem FROM dispatch_teknisi) AND 
        a.Incident LIKE "%INT%"
      ');
  }

  public static function dashboardDetailUndispatch($status){
    date_default_timezone_set('Asia/Makassar');
    $date_now = date('Y-m-d H:i:s');

    $where_status = '';
    if ($status=='x0dan2'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=0 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 2)';
    }
    elseif($status=='x2dan4'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=2 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 4)';
    }
    elseif($status=='x4dan6'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=4 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 6)';
    }
    elseif($status=='x6dan8'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=6 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 8)';
    }
    elseif($status=='x8dan10'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=8 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 10)';
    }
    elseif($status=='x10dan12'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=10 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 12)';
    }
    elseif($status=='x12dan24'){
       $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=12 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 24)';
    }
    elseif($status=='lebih24'){
       $where_status = ' AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=24';
    }
    elseif($status=='kurang24'){
       $where_status = ' AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 24';
    };

    $sql = 'SELECT 
              *
            FROM 
              data_nossa_1_log a
              LEFT JOIN mdf d ON a.Workzone = d.mdf  
            WHERE 
              a.Incident NOT IN (SELECT Ndem FROM dispatch_teknisi) AND 
              a.Incident LIKE "%INT%"
              '.$where_status.'
            ';
    $data = DB::select($sql);
    return $data;
  }

}
