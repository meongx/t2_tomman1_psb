@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
      th {
        background-color: #FF0000;
        color : #FFF;
        text-align: center;
        vertical-align: middle;
      }
      td {
        color : #000;
      }
    </style>

  <a href="/dashboard/assurance/{{ date('Y-m-d') }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List Tiket Active By Rock</h3>
  <div class="row">
    <div class="col-sm-12">
      <div>
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>No.</th>
          <th width=100>No Tiket</th>
          <th>Inet</th>
          <th>Hasil Ukur</th>
          <th>IP OLT</th>
          <th>Calling Station Id</th>
          <th>Status</th>
          <th>Tim</th>
          <th>Sektor</th>
          <th>Tgl_Open</th>
          <th>Status Laporan</th>
          <th>STO</th>
          <th>Loker Dispatch</th>
          <th>Channel</th>
          <th>Sebab</th>
          <th>Action</th>
          <th width="300">Catatan Tek.</th>
        </tr>

        @foreach($getData as $no=>$data)
          <tr>
              <td>{{ ++$no }}</td>
              <td>{{ $data->trouble_no }}</td>
              <td>{{ $data->nd_int }}</td>
              <td>{{ $data->ONU_Rx }}</td>
              <td>{{ $data->IP_NE }}</td>
              <td>{{ $data->Calling_Station_Id }}</td>
              <td>{{ $data->ONU_Link_Status }}</td>
              <td>{{ $data->uraian }}</td>
              <td>{{ $data->title }}</td>
              <td>{{ $data->trouble_opentime }}</td>
              <td>{{ $data->laporan_status }}</td>
              <td>{{ $data->mdf ?: $data->sto }}</td>
              <td>{{ $data->loker_dispatch }}</td>
              <td>{{ $data->channel }}</td>
              <td>{{ $data->penyebab }}</td>
              <td>{{ $data->action }}</td>
              <td>{{ $data->catatan }}</td>
          </tr>
        @endforeach
       
      </table>
    </div>
    </div>
  </div>
@endsection
