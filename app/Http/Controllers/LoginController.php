<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

use DB;

class LoginController extends Controller
{
  public function loginPage()
  {
    return view('login');
  }

  public function starclick(){
    $starclick = DB::table('cookie_starclick')->get();
    $starclick = $starclick[0];
    return view('login.starclick',compact('starclick'));
  }

  public function starclick_save(Request $req){
    $cookie_starclick = $req->input('cookie_starclick');
    $save = DB::table('cookie_starclick')->update([
      'cookie' => $cookie_starclick
    ]);

      return redirect('/loginstarclick')->with('alerts', [
          ['type' => 'success', 'text' => 'Session Berhasil disimpan.']
      ]);
    
  }

  public function login(Request $request)
  {
    $username = $request->input('login');

    // cek for suspend
    $cek_suspend = DB::SELECT('SELECT * FROM regu WHERE (nik1 = "'.$username.'" OR nik2 = "'.$username.'") AND status_team = "SUSPEND_3_DAY"');
    if (count($cek_suspend)>0){
      return redirect('/login')->with('alerts', [
          ['type' => 'danger', 'text' => 'Login Gagal, User Anda dalam Status SUSPEND.']
      ]);
    }

    $password = $request->input('password');

    $result = DB::select('
      SELECT w.nama_witel,u.id_user, u.password, u.id_karyawan, k.nama, r.id_regu, u.level, u.psb_remember_token, r.mainsector, u.maintenance_level, u.witel,
      u.psb_reg
      FROM user u
      LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
      LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
      LEFT JOIN witel w ON u.witel = w.id_witel
      WHERE id_user = ? AND password = MD5(?) AND u.level<>0
    ',[$username,$password]);

    if (count($result)>0) {
      $save_log = DB::table('user_log')->insert([
        'id_karyawan' => $username,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'status' => 1
      ]);

      $session = $request->session();
      $session->put('auth', $result[0]);
      $session->put('psb_reg',$result[0]->psb_reg);
      $session->put('witel',$result[0]->nama_witel);

      $this->ensureLocalUserHasRememberToken($result[0]);
      return $this->successResponse($result[0]->psb_remember_token);
    } else {
      //echo "error";
      $save_log = DB::table('user_log')->insert([
        'id_karyawan' => $username,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'status' => 2
      ]);
      return redirect('/login')->with('alerts', [
          ['type' => 'danger', 'text' => 'Login Gagal']
      ]);
    }
  }

  public function logout()
  {
    Session::forget('auth');
    return redirect('/login')->withCookie(cookie()->forever('presistent-token', ''));
  }

  private function ensureLocalUserHasRememberToken($localUser)
  {
    $token = $localUser->psb_remember_token;

    if (!$localUser->psb_remember_token) {
      $token = $this->generateRememberToken($localUser->id_user);
      DB::table('user')
      ->where('id_user', $localUser->id_user)
      ->update([
        'psb_remember_token' => $token
      ]);
      $localUser->psb_remember_token = $token;
    }

    return $token;
  }

  private function generateRememberToken($nik)
  {
    return md5($nik . microtime());
  }

  private function successResponse($rememberToken)
  {
    if (Session::has('auth-originalUrl')) {
      $url = Session::pull('auth-originalUrl');
    } else {
      $url = '/';
    }

    $response = redirect($url);
    if ($rememberToken) {
      $response->withCookie(cookie()->forever('presistent-token', $rememberToken));
    }

    return $response;
  }

  private function failureResponse()
  {
    // flash old input

  }
}
