@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
  </style>
  <h3>Data Prospek MBSP</h3>
  <div class="row">
    <div class="col-sm-6">
      <h6>MBSP Witel Kalsel {{ date('d M Y') }}</h6>
      <div class="table-responsive">
        <table align=center class="table table-striped table-bordered dataTable">
          <tr>
            <th rowspan="2">Area</th>
            <th colspan=2>BELUM WEBCARE</th>
          </tr>
          <tr>
            <td>F2F</td>
            <td>C2F</td>
          </tr>
          @foreach ($GetMBSP as $MBSP)
          <tr>
            <td>{{ $MBSP->area }}</td>
            <td><a href="/dashboard/list/MBSP/F2F/{{ $MBSP->area }}" class="label label-default">{{ $MBSP->F2F }}</a></td>
            <td><a href="/dashboard/list/MBSP/C2F/{{ $MBSP->area }}" class="label label-default">{{ $MBSP->C2F }}</a></td>
          </tr>

          @endforeach
        </table>
      </div>
    </div>
  </div>

@endsection
