<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class PsbModel
{
  public static function psb_laporan_status(){
    return DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status ORDER BY laporan_status');
  }
  public static function batch_get_data_for_dalapa($id){
    $get_sc = DB::SELECT('
      SELECT
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ3aXRlbCI6IkJKTSIsInZhbHVlIjp7IjEiOjIwMzcsIjIiOjIwMzgsIjMiOjIwMzksIjQiOjIwNDAsIjUiOjIwNDEsIjYiOjIwNDIsIjciOjIwNDN9fQ.GF0HcedknFaFQ5h-Dll93F6MLRmVpj2sBYrpgoytDC0" as Authorization,
      b.nama_odp as ODP_LOCATION,
      b.port_number as PORT_NUMBER,
      b.kordinat_pelanggan as KORDINAT,
      b.snont as SN_ONT,
      c.orderName as NAMA_PELANGGAN,
      c.noTelp as VOICE_NUMBER,
      c.internet as INTERNET_NUMBER,
      DATE(b.modified_at) as DATE_VALIDATION,
      b.odp_label_code as QR_PORT_ODP,
      b.dropcore_label_code as QR_DROPCORE,
      d.alamatLengkap as NAMA_JALAN,
      d.picPelanggan as CONTACT,
      1 as TYPE,
      c.orderId,
      a.Ndem,
      a.id as id_dt
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN psb_myir_wo d ON a.Ndem = d.sc
      WHERE
        a.tgl LIKE "'.$id.'%" AND
        b.status_laporan = 1 AND
        b.dalapa_status IS NULL AND
        c.orderId IS NOT NULL
    ');
    return $get_sc;
  }

  public static function get_data_for_dalapa($id){
    $get_sc = DB::SELECT('
      SELECT
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ3aXRlbCI6IkJKTSIsInZhbHVlIjp7IjEiOjIwMzcsIjIiOjIwMzgsIjMiOjIwMzksIjQiOjIwNDAsIjUiOjIwNDEsIjYiOjIwNDIsIjciOjIwNDN9fQ.GF0HcedknFaFQ5h-Dll93F6MLRmVpj2sBYrpgoytDC0" as Authorization,
      b.nama_odp as ODP_LOCATION,
      b.port_number as PORT_NUMBER,
      b.kordinat_pelanggan as KORDINAT,
      b.snont as SN_ONT,
      c.orderName as NAMA_PELANGGAN,
      c.noTelp as VOICE_NUMBER,
      c.internet as INTERNET_NUMBER,
      DATE(b.modified_at) as DATE_VALIDATION,
      b.odp_label_code as QR_PORT_ODP,
      b.dropcore_label_code as QR_DROPCORE,
      d.alamatLengkap as NAMA_JALAN,
      d.picPelanggan as CONTACT,
      1 as TYPE
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN psb_myir_wo d ON a.Ndem = d.sc
      WHERE
        a.id = "'.$id.'" AND
        c.orderId IS NOT NULL
    ');
    return $get_sc;
  }

  public static function updatenote(){
    return DB::table('updatenote')->get()->first();
  }

  public static function checkONGOING(){
    $auth = session('auth');
    $query = DB::SELECT('
      SELECT
      *,
      a.id as id_dt
      FROM
        dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN regu c ON a.id_regu = c.id_regu
        LEFT JOIN data_nossa_1_log d ON a.Ndem = d.Incident
        LEFT JOIN Data_Pelanggan_Starclick e ON (a.Ndem = e.orderId)
        LEFT JOIN Data_Pelanggan_Starclick ee ON ((ee.ndemPots LIKE concat("%",d.Service_No,"%") OR ee.ndemSpeedy LIKE concat("%",d.Service_No,"%")))  AND ee.orderStatusId = 7
        LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      WHERE
        (c.nik1 = "'.$auth->id_karyawan.'" OR c.nik2 = "'.$auth->id_karyawan.'") AND
        b.status_laporan IN (28,29,5,31) AND
        a.tgl LIKE "%'.date('Y-m').'%"
      GROUP BY a.id
    ');
    return $query;
  }

  public static function WorkOrder($id,$bulan){
    $auth = session('auth');
    if ($auth->id_karyawan=="91153709x"){

      $id_regu = '';
    } else {
      $id_regu = '(aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND';
    }

      switch ($id) {
        case 'NEED_PROGRESS':
          $Where = '
          WHERE
            a.tgl LIKE "'.$bulan.'%" AND
            '.$id_regu.'
    				(b.id IS NULL OR b.status_laporan IN (5,6,4,3,7) OR b.status_laporan IS NULL) ';
          break;

        case 'NEED_PROGRESS_FFG':
          $Where = '
          WHERE
    				a.tgl LIKE "'.$bulan.'%" AND
            dn1log.Owner_Group = "WOC FULFILLMENT KALSEL (BANJARMASIN)" AND
    				'.$id_regu.'
    				(b.id IS NULL OR b.status_laporan IN (5,6,4,7) OR b.status_laporan IS NULL) ';
          break;

        case 'KENDALA':
          $Where = '
          WHERE
            a.tgl LIKE "%'.$bulan.'%" AND
            '.$id_regu.'
            b.status_laporan NOT IN (5,6,1,4,29,28,37,38)';
          break;

        case 'ONGOING':
          $Where = '
          WHERE
            a.tgl LIKE "%'.$bulan.'%" AND
            '.$id_regu.'
            b.status_laporan IN (28,29)';
          break;

        case 'UP':
          $Where = '
          WHERE
            a.tgl LIKE "'.$bulan.'%" AND
            '.$id_regu.'
            b.status_laporan IN (1,37,38)';
          break;

        default:
          $Where = '
          WHERE
            a.tgl LIKE "'.$bulan.'%" AND

            (b.id IS NULL OR b.status_laporan > 7 OR b.status_laporan = "")';
          break;
      }

      $result = PsbModel::QueryBuilder($Where, $bulan);
      // dd($result);
      return $result;
  }

  private static function QueryBuilder($id, $bulan){
    date_default_timezone_set('Asia/Makassar');
    $auth = session('auth');
    if ($auth->id_karyawan=="91153709z"){
      $id_regu = '';
    } else {
      $id_regu = '(aa.nik1 = "'.$id.'" OR aa.nik2 = "'.$id.'") AND';
    }
    $now = date('Y-m-d H:n:s');
    $SQL = '
    select t1.* from(
      SELECT
        a.*,
        a.id as id_dt,
        b.id as id_pl,
        c.orderName,
        b.status_laporan as status_laporan,
        0 as jumlah_material,
        mg3.mdf_grup as grup_starclick,
        mg3.mdf_grup_id as grup_id_starclick,
        c.ndemPots,
        c.ndemSpeedy,
        c.Kcontact as KcontactSC,
        c.jenisPsb,
        c.orderId,
        c.orderCity,
        c.sto as sto_dps,
        c.alproname,
        c.orderNcli,
        c.orderKontak,
        c.orderDate,
        c.lat,
        c.lon,
        c.email,
        0 as ND,
        0 as duration,
        bb.laporan_status_id,
        bb.laporan_status,
        bb.laporan_status as status_wo_by_teknisi,
        dn1log.Workzone as neSTO,
        dn1log.Datek as neRK,
        dn1log.Service_ID as neND_TELP,
        dn1log.Service_No as neND_INT,
        dn1log.Service_Type as nePRODUK_GGN,
        dn1log.Summary as neHEADLINE,
        dn1log.Owner_Group as neLOKER_DISPATCH,
        dn1log.Customer_Name as neKANDATEL,
        dn1log.Booking_Date as Booking_Date,
        0 as rebUmur,
        dn1log.Reported_Date as TROUBLE_OPENTIME,
        qc.*,
        qc_by.nik as qcNik,
        qc_by.nama as qcNama,
        qc_by.position as qcPosition,
        0 as alamatGGN,
        0 as historyPSB,
        dn1log.Service_No as ND_TELP,
        dn1log.Contact_Phone,
        dn1log.Customer_Name,
        dn1log.Reported_Date,
        dn1log.Contact_Name,
        0 as datek,
        dn1log.Incident as no_tiket,
        c.noTelp,
        c.internet,
        dn1log.Incident as status_gangguan,
        ps.kode_sales,
        ps.nama_sales,
        my.myir as myir,
        my.picPelanggan,
        my.customer,
        my.namaOdp,
        my.sto as stoMy,
        my.no_internet,
        my.no_telp
      FROM
        dispatch_teknisi a
        LEFT JOIN regu aa ON a.id_regu = aa.id_regu
        LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
        LEFT JOIN mdf m3 ON c.sto = m3.mdf
        LEFT JOIN mdf_grup mg3 ON m3.mdf_grup_id = mg3.mdf_grup_id
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status bb ON b.status_laporan = bb.laporan_status_id
        LEFT JOIN data_nossa_1_log dn1log ON dn1log.Incident = a.Ndem
        LEFT JOIN psb_myir_wo my on (a.Ndem=my.myir or a.Ndem=my.sc)
        LEFT JOIN psb_sales ps ON ps.kode_sales = my.sales_id
        LEFT JOIN qcondesk qc ON a.id = qc.qcondesk_order
        LEFT JOIN 1_2_employee qc_by ON qc.qcondesk_by = qc_by.nik
        '.$id.'
        ORDER BY a.created_at ASC
        ) as t1 group by Ndem
     ';
        // ra.no_tiket as status_gangguan,
        // LEFT JOIN roc_active ra ON roc.no_tiket = ra.no_tiket

     $query = DB::select($SQL);
     // dd($query);
     return $query;
 }

 public static function woProv($ket){
    $now  = date('Y-m-d H:n:s');
    $id_regu = '(aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND';

    // 1 = need progress, 2 = kendala, 3 = up
    if ($ket==1){
        $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    (b.id IS NULL OR b.status_laporan IN (5,6,4,3,7) OR b.status_laporan IS NULL) ';
    }
    elseif ($ket==2){
      $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan NOT IN (5,6,1,4,29,28,37,38)';
    }
    elseif($ket==3){
       $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan IN (1,37,38)';
    }

    $sql = ' SELECT
        a.*,
        a.id as id_dt,
        b.id as id_pl,
        c.orderName,
        b.status_laporan as status_laporan,
        mg3.mdf_grup as grup_starclick,
        mg3.mdf_grup_id as grup_id_starclick,
        c.ndemPots,
        c.ndemSpeedy,
        c.Kcontact as KcontactSC,
        c.jenisPsb,
        c.orderId,
        c.orderCity,
        c.sto as sto_dps,
        c.alproname,
        c.orderNcli,
        c.orderKontak,
        c.orderDate,
        c.lat,
        c.lon,
        c.email,
        bb.laporan_status_id,
        bb.laporan_status,
        bb.laporan_status as status_wo_by_teknisi,
        my.customer,
        my.myir,
        my.picPelanggan,
        c.noTelp,
        c.internet
      FROM
        dispatch_teknisi a
        LEFT JOIN regu aa ON a.id_regu = aa.id_regu
        LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
        LEFT JOIN mdf m3 ON c.sto = m3.mdf
        LEFT JOIN mdf_grup mg3 ON m3.mdf_grup_id = mg3.mdf_grup_id
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status bb ON b.status_laporan = bb.laporan_status_id
        LEFT JOIN psb_myir_wo my on a.Ndem=my.myir
        '.$Where.'
      ';

    $query = DB::select($sql);
    dd($query);
 }

 public static function woReboundary($ket){
    $auth = session('auth');
    $now  = date('Y-m-d H:n:s');
    $id_regu = '(aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND';

    // 1 = need progress, 2 = kendala, 3 = up
    if ($ket==1){
        $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    (b.id IS NULL OR b.status_laporan IN (5,6,4,3,7) OR b.status_laporan IS NULL) ';
    }
    elseif ($ket==2){
      $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan NOT IN (5,6,1,4,29,28,37,38)';
    }
    elseif($ket==3){
       $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan IN (1,37,38)';
    }

    $sql = ' SELECT
        a.*,
        a.id as id_dt,
        b.id as id_pl,
        c.orderName,
        b.status_laporan as status_laporan,
        mg3.mdf_grup as grup_starclick,
        mg3.mdf_grup_id as grup_id_starclick,
        c.ndemPots,
        c.ndemSpeedy,
        c.Kcontact as KcontactSC,
        c.jenisPsb,
        c.orderId,
        c.orderCity,
        c.sto as sto_dps,
        c.alproname,
        c.orderNcli,
        c.orderKontak,
        c.orderDate,
        c.lat,
        c.lon,
        c.email,
        bb.laporan_status_id,
        bb.laporan_status,
        bb.laporan_status as status_wo_by_teknisi,
        my.*,
        c.noTelp,
        c.internet
      FROM
        dispatch_teknisi a
        LEFT JOIN regu aa ON a.id_regu = aa.id_regu
        LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
        LEFT JOIN mdf m3 ON c.sto = m3.mdf
        LEFT JOIN mdf_grup mg3 ON m3.mdf_grup_id = mg3.mdf_grup_id
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status bb ON b.status_laporan = bb.laporan_status_id
        LEFT JOIN reboundary_survey my on a.Ndem=my.myir
        '.$Where.'
      ';

    $query = DB::select($sql);
    dd($query);
 }

 public static function woMigrasi($ket){
    $auth = session('auth');
    $now  = date('Y-m-d H:n:s');
    $id_regu = '(aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND';

    // 1 = need progress, 2 = kendala, 3 = up
    if ($ket==1){
        $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    (b.id IS NULL OR b.status_laporan IN (5,6,4,3,7) OR b.status_laporan IS NULL) ';
    }
    elseif ($ket==2){
      $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan NOT IN (5,6,1,4,29,28,37,38)';
    }
    elseif($ket==3){
       $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan IN (1,37,38)';
    }

    $sql = ' SELECT
        a.*,
        a.id as id_dt,
        b.id as id_pl,
        b.status_laporan as status_laporan,
        mg3.mdf_grup as grup_starclick,
        mg3.mdf_grup_id as grup_id_starclick,
        bb.laporan_status_id,
        bb.laporan_status,
        bb.laporan_status as status_wo_by_teknisi,
        mw.nd as ndmw,
        mw.pic,
        mw.nama as namamw,
        mw.alamat as alamatmw,
        mw.jenis_layanan as jnslayananmw,
      FROM
        dispatch_teknisi a
        LEFT JOIN regu aa ON a.id_regu = aa.id_regu
        LEFT JOIN micc_wo mw on a.Ndem = mw.ND
        LEFT JOIN mdf m3 ON c.sto = m3.mdf
        LEFT JOIN mdf_grup mg3 ON m3.mdf_grup_id = mg3.mdf_grup_id
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status bb ON b.status_laporan = bb.laporan_status_id
        '.$Where.'
      ';

    $query = DB::select($sql);
    dd($query);
 }

 public static function orderMaintenance($order){
    // nik uji coba 98170106

    $auth = session('auth');
    if ($order == 'maintenance'){
        $status = " and (m.status is NULL OR m.status != 'close')";
        $condition = "(m.username1 = '".$auth->id_karyawan."' OR m.username2 = '".$auth->id_karyawan."' OR m.username3 = '".$auth->id_karyawan."' OR m.username4 = '".$auth->id_karyawan."')";

        $result = DB::select('
          SELECT m.*, pl.nama_odp, pl.kordinat_odp, m.nama_odp as nama_odp_m
          FROM maintaince m left join psb_laporan pl on m.psb_laporan_id = pl.id
          WHERE '.$condition.' '.$status.' order by m.id desc
        ');
    }
   return $result;
  }

  public static function getTambahTiang()
  {
      return DB::SELECT('SELECT id, ketTiang as text FROM tambahTiang ORDER BY id');
  }

  public static function value_search($search){
     return DB::table('Data_Pelanggan_Starclick')->where('orderId', 'like', '%' .$search. '%')->limit(5)->get();
  }

  public static function cekDataMaterial($id){
      return DB::table('psb_laporan_material')
          ->where('psb_laporan_id',$id)
          ->where('type','1')
          ->get();
  }

  public static function getIdWoFrormPsbid($psbId){
      return DB::table('dispatch_teknisi')
            ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
            ->select('dispatch_teknisi.id')
            ->where('psb_laporan.id',$psbId)
            ->first();
  }

  public static function cekDataMyirIfSc($id){
      return DB::table('dispatch_teknisi')
                ->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.myir')
                ->select('psb_myir_wo.*')
                ->where('dispatch_teknisi.id',$id)
                ->first();
  }

  public static function listReboundary($tgl){
      return DB::table('reboundary_survey')
                ->leftJoin('dispatch_teknisi','reboundary_survey.no_tiket_reboundary','=','dispatch_teknisi.Ndem')
                ->leftJoin('Data_Pelanggan_Starclick','reboundary_survey.scid','=','Data_Pelanggan_Starclick.orderId')
                ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
                ->leftJoin('group_telegram','reboundary_survey.chat_sektor','=','group_telegram.chat_id')
                ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
                ->select('reboundary_survey.*','Data_Pelanggan_Starclick.*', 'regu.id_regu', 'regu.uraian', 'group_telegram.chat_id', 'group_telegram.title', 'psb_laporan.status_laporan','psb_laporan_status.laporan_status')
                ->whereDate('reboundary_survey.updated_at',$tgl)
                ->get();
  }

  public static function listReboundaryCari($scid){
      return DB::table('reboundary_survey')
                ->leftJoin('dispatch_teknisi','reboundary_survey.no_tiket_reboundary','=','dispatch_teknisi.Ndem')
                ->leftJoin('Data_Pelanggan_Starclick','reboundary_survey.scid','=','Data_Pelanggan_Starclick.orderId')
                ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
                ->leftJoin('group_telegram','reboundary_survey.chat_sektor','=','group_telegram.chat_id')
                ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
                ->select('reboundary_survey.*','Data_Pelanggan_Starclick.*', 'regu.id_regu', 'regu.uraian', 'group_telegram.chat_id', 'group_telegram.title', 'psb_laporan.status_laporan','psb_laporan_status.laporan_status')
                ->where('reboundary_survey.scid',$scid)
                ->get();
  }

  public static function simpanReboundary($req, $nik, $status, $id=null){
      $timLama = DB::table('dispatch_teknisi')
                  ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
                  ->select('dispatch_teknisi.Ndem','regu.id_regu','regu.uraian')
                  ->where('dispatch_teknisi.Ndem',$req->scid)
                  ->first();

      $idTimLama = NULL;
      $uraianLama = NULL;
      if (count($timLama)<>0){
          $idTimLama  = $timLama->id_regu;
          $uraianLama = $timLama->uraian;
      };

      if ($status==1){
          return DB::table('reboundary_survey')->insert([
              'scid'            => $req->scid,
              'tarikan_lama'    => $req->tarikanLama,
              'koor_pelanggan'  => $req->koorPel,
              'koor_odp_lama'   => $req->koorOdpLama,
              'koor_odp_baru'   => $req->koorOdpBaru,
              'odp_lama'        => $req->odpLama,
              'odp_baru'        => $req->odpBaru,
              'estimasi_tarikan'=> $req->tarikanEstimasi,
              'created_by'      => $nik,
              'catatan'         => $req->catatan,
              'chat_sektor'     => $req->sektor,
              'idTimLama'       => $idTimLama,
              'uraianTimLama'   => $uraianLama
          ]);
      }
      else{
          return DB::table('reboundary_survey')->where('id',$id)->update([
              'scid'            => $req->scid,
              'tarikan_lama'    => $req->tarikanLama,
              'koor_pelanggan'  => $req->koorPel,
              'koor_odp_lama'   => $req->koorOdpLama,
              'koor_odp_baru'   => $req->koorOdpBaru,
              'odp_lama'        => $req->odpLama,
              'odp_baru'        => $req->odpBaru,
              'estimasi_tarikan'=> $req->tarikanEstimasi,
              'created_by'      => $nik,
              'catatan'         => $req->catatan,
              'chat_sektor'     => $req->sektor,
              'idTimLama'       => $idTimLama,
              'uraianTimLama'   => $uraianLama
          ]);
      }
  }

  public static function cekSCInDataStarclick($scid){
      return DB::table('Data_Pelanggan_Starclick')->where('orderId',$scid)->first();
  }

  public static function getDataReboundaryById($id){
      return DB::table('reboundary_survey')->where('id',$id)->first();
  }

  public static function getDataReboundaryByScid($scid){
      return DB::table('reboundary_survey')->where('scid',$scid)->first();
  }

  public static function updateNoTiket($scid, $notiket){
      return DB::table('reboundary_survey')->where('scid',$scid)->update(['no_tiket_reboundary' => $notiket]);
  }

  public static function getAkses($nik, $akses){
      return DB::table('psb_nik_scbe')->where('nik',$nik)->where('akses',$akses)->first();
  }
}
