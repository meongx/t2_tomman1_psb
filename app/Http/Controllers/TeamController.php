<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\DA\TeamModel;
use App\DA\EmployeeModel;

class TeamController extends Controller
{

	public static function index(){
		$auth = session('auth');
		if ($auth->level==2 || $auth->level==46 ){
			$getregu = TeamModel::getRegu(session('witel'));
			return view('team.index',compact('getregu'));
		}
		else{
			echo "Banyaki Bawa Beibadah Ja Parak Kiamat !!!" ;
		}
	}

	public static function input(){
		$auth = session('auth');
		if ($auth->level==2 || $auth->level==46){
			$data = new \stdClass();
	    	$data->id_regu = null;
		    $data->uraian = null;
		    $data->mitra = null;
		    $data->nik1 = null;
		    $data->nik2 = null;
		    $data->mainsector = null;
		    $data->crewid = null;
		    $data->kemampuan = null;
		    $data->status_team = NULL;
		    $data->status_alker = null;
			$karyawan = EmployeeModel::getAll();
			$getStatus = TeamModel::distinctStatus();
			$mainsector = TeamModel::mainsector();
			$get_status_team = DB::table('status_team')->get();
			$get_status_alker = DB::table('status_alker')->get();
			return view('team.input',compact('data','karyawan','getStatus','mainsector','get_status_team','get_status_alker'));
		}
		else{
			echo "Banyaki Bawa Beibadah Ja Parak Kiamat !!!" ;
		}
	}

	public static function store(Request $r){
		if ($r->input('uraian')!=""){
			//data tidak kosong
			$check = TeamModel::checkRegu($r->input('uraian'));

			if (count($check)>0){
				$type = "danger";
				$message = "FAILED ! GROUP NAME WAS USED";
			} else {
				$save = TeamModel::store($r);
				$type = "success";
				$message = "SUCCESS !";
			}
		} else {
				$type = "danger";
				$message = "FAILED ! GROUP NAME IS EMPTY";
		}
		return redirect('/team')->with('alerts', [
			['type' => $type, 'text' => '<strong>'.$message.'</strong>']
		]);
	}

	public static function edit($id){
		$auth = session('auth');
		if ($auth->level==2 || $auth->level==46){
			$data = TeamModel::detailRegu($id);
			$karyawan = EmployeeModel::getAll();
			$getStatus = TeamModel::distinctStatus();
			$mainsector = TeamModel::mainsector();
			$get_status_team = DB::table('status_team')->get();
			$get_status_alker = DB::table('status_alker')->get();
			return view('team.input',compact('data','karyawan','getStatus','mainsector','get_status_team','get_status_alker'));
		}
		else{
			echo "Banyaki Bawa Beibadah Ja Parak Kiamat !!!" ;
		}
	}

	public static function update(Request $r){
		$auth = session('auth');
		if ($r->input('uraian')==""){
			$type = "danger";
			$message = "FAILED ! GROUP NAME IS EMPTY";
		} else {
			if ($auth->level==2) {
				$update = TeamModel::update_2($r);
			} else {
				$update = TeamModel::update($r);
			}
			$type = "success";
			$message = "SUCCESS !";
		}
		return redirect('/team')->with('alerts', [
			['type' => $type, 'text' => '<strong>'.$message.'</strong>']
		]);
	}

	public static function disable($id){
		TeamModel::disable($id);
		return redirect('/team')->with('alerts',[
		 ['type' => 'success','text' => '<strong>SUCCESS ! '.$id.' DISABLED</strong>']
		]);
	}

}
