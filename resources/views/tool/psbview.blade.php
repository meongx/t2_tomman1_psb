@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
  </style>
  <div style="padding-bottom: 10px;">
<!--     <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong> -->
  </div>
  <h3>PSB VIEW</h3>
  <div class="panel panel-primary" id="timeslot">
    {{-- <div class="panel-heading"></div> --}}
    <div class="panel-body">
      <div class="list-group">
	    <div class="table-responsive">
				<table class="table table-striped table-bordered">
          <tr>
            <th>No</th>
            <th>SC</th>
            <th>Tagging Rumah Pelanggan</th>
            <th>Order Status</th>
            <th>Foto</th>
            
            @foreach($datas as $no => $data)
              <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $data->orderId }}</td>
                  <td>{{ $data->lat }}, {{ $data->lon }}</td>
                  <td>{{ $data->orderStatus }}</td>
                  <td>
                    @if ($data->orderStatusId==7)
                        @foreach($photoInputsPs as $input)
                          <?php
                              $path = "/upload/evidence/".$data->id_tbl_mj."/$input";
                              $th   = "$path-th.jpg";
                              $img  = "$path.jpg";
                        
                              if (file_exists(public_path().$th)){
                           ?>

                              <a href="/upload/evidence/{{ $data->id_tbl_mj }}/{{$input}}.jpg">
                                  <img src="/upload/evidence/{{ $data->id_tbl_mj }}/{{ $input }}-th.jpg" alt="{{ $input }}">
                              </a>
                          <?php

                            }
                            else{
                          ?>
                              <img src="/image/placeholder.gif" alt="" />   
                          <?PHP
                            }
                          ?>
                        @endforeach
                    @else
                        @foreach($photoInputs as $input)
                          <?php
                              $path = "/upload/evidence/".$data->id_tbl_mj."/$input";
                              $th   = "$path-th.jpg";
                              $img  = "$path.jpg";
                          
                              if (file_exists(public_path().$th)){
                          ?>

                              <a href="/upload/evidence/{{ $data->id_tbl_mj }}/{{$input}}.jpg">
                                  <img src="/upload/evidence/{{ $data->id_tbl_mj }}/{{ $input }}-th.jpg" alt="{{ $input }}">
                              </a>
                          <?php
                            }
                            else{
                          ?>
                              <img src="/image/placeholder.gif" alt="" />   
                          <?PHP
                            }
                          ?>
                        @endforeach
                    @endif
                  </td>
              </tr>
            @endforeach
          </tr>
        </table>
        </div>
      </div>
    </div>
  </div>


@endsection
