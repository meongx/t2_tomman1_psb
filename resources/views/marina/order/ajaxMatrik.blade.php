@foreach($data as $no => $d)
    <?php
        $total = $d->undisp+$d->no_update+$d->kendala_pelanggan+$d->kendala_teknis+$d->close;
    ?>
    <tr class="ajax">
        <td>{{ $d->datel }}</td>
        <td></td>
        <td><a href="/marina/order/{{ $id }}/undisp/all/{{ $d->datel }}">{{ $d->undisp }}</td>
        <td><a href="/marina/order/{{ $id }}/null/all/{{ $d->datel }}">{{ $d->no_update }}</td>
        <td><a href="/marina/order/{{ $id }}/kendala pelanggan/all/{{ $d->datel }}">{{ $d->kendala_pelanggan }}</td>
        <td><a href="/marina/order/{{ $id }}/kendala teknis/all/{{ $d->datel }}">{{ $d->kendala_teknis }}</td>
        <td><a href="/marina/order/{{ $id }}/close/{{date('Y-m')}}/{{ $d->datel }}">{{ $d->close }}</td>
    </tr>
@endforeach