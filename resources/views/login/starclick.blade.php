@extends('layout')

@section('content')
  @include('partial.alerts')
  <center><h3>Starclick NCX Session</h3></center>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Last Sync : {{ $starclick->last_sync }}
        </div>
        <div class="panel-body">
          <form method="post">
            <textarea name="cookie_starclick" cols=50 row=20>{{ $starclick->cookie }}</textarea><br />
            <input type="submit" name="submit" class="btn btn-success" value="save" />
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
