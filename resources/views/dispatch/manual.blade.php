@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <div class="col-md-12">
        <h3>
          <a href="../" class="btn btn-sm btn-default">
            <span class="glyphicon glyphicon-arrow-left"></span>
          </a>
          Dispatch WO [Manual Assurance]
        </h3>
    </div>

    <div class="form-group col-md-12">
      <label for="no_tiket">No Tiket</label>
      <input type="text" class="form-control" name="no_tiket">
    </div>
    <div class="form-group col-md-12 {{ $errors->has('refSc') ? 'has-error' : '' }}" id="hilang2">
      <label for="refSc">References SC</label>
      <input type="text" class="form-control" name="refSc">
      {!! $errors->first('refSc','<p class=help-block>:message</p>') !!}
    </div>

    {{-- <div class="form-group col-md-12"> --}}
      {{-- <label for="no_tiket">No WO</label> --}}
      {{-- <input class="form-control" name="noSC" type="hidden" id="noSC" /> --}}
      {{-- <input class="form-control" name="noSC" type="text" /> --}}
    {{-- </div> --}}

    <div class="form-group col-md-12">
      <label class="control-label" for="input-regu" >Loker</label>
      <input name="loker" type="hidden" id="input-loker" class="form-control" />
    </div>

    <div class="form-group col-md-12">
      <label class="control-label" for="input-regu">Alpro</label>
      <input name="alpro" type="hidden" id="input-alpro" class="form-control" />
    </div>

    <div class="form-group col-md-12">
      <label class="control-label" for="input-regu">Regu</label>
      <input name="id_regu" type="hidden" id="input-regu" class="form-control" />
    </div>

    <div class="form-group col-md-12">
      <label class="control-label" for="jenis_ont" >Jenis ONT</label>
      <input name="jenis_ont" type="hidden" id="jenis_ont" class="form-control" />
      {!! $errors->first('jenis_ont','<p class="label label-danger">:message</p>') !!}
    </div>
    
    <div class="form-group col-md-12">
      <label class="control-label" for="input-tgl">Tanggal</label>
      <input name="tgl" type="tgl" id="input-tgl" class="form-control" value="{{ date('Y-m-d') }}"/>
    </div>

    <div class="form-group col-md-2 {{ $errors->has('tglOpen') ? 'has-error' : '' }}">
        <label class="control-label" for="tglOpen">Tanggal Open</label>
        <input name="tglOpen" type="tgl" id="tglOpen" class="form-control" value="{{ date('Y-m-d') }}" />  
        {!! $errors->first('tglOpen','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group col-md-2 {{ $errors->has('jamOpen') ? 'has-error' : '' }}">
         <div class="input-group bootstrap-timepicker timepicker">
            <label class="label-control">Jam Open</label>
            <input id="jamOpen" name="jamOpen" type="text" class="form-control input-small" readonly>
            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
            {!! $errors->first('jamOpen','<p class="label label-danger">:message</p>') !!}
        </div>
    </div>

     <div class="form-group col-md-12" id="hilang">
          <label class="control-label" for="ket"><b>Keterangan Fullfillment Guarante</b></label>
          <textarea name="ket" class="form-control" rows="5"></textarea>
      </div>

    <div style="margin:40px 0 20px" class="col-md-12">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  
  {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> --}}
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script>
    $(function() {
      var dataloker = [
        {"id":"1", "text":"CONSUMER"},
        {"id":"2", "text":"CORPORATE"},
        {"id":"3", "text":"Fulfillment Guarantee"},
        {"id":"4", "text":"REBOUNDARY"}
        ];

    $('#hilang').hide();
    $('#hilang2').hide();
    $('#input-loker').change(function(){
        console.log(this.value);
        if(this.value==4){
          $('#hilang2').show();
          $('#hilang').hide();
        }
        else if (this.value==3){
            $('#hilang').show();
            $('#hilang2').show();
        }
        else{
          $('#hilang').hide();
          $('#hilang2').hide();
        };

    });

    var loker = function() {
      return {
        data: dataloker,
        placeholder: 'Input Loker',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }

    $('#input-loker').select2(loker());

    var dataloker = [
      {"id":"1", "text":"CONSUMER"},
      {"id":"2", "text":"CORPORATE"},
      {"id":"3", "text":"FULFILLMENT GUARANTEE"},
      {"id":"4", "text":"REBOUNDARY"}
      ];

    var loker = function() {
      return {
        data: dataloker,
        placeholder: 'Input Loker',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#input-loker').select2(loker());

    var dataalpro = [
      {"id":"COPPER", "text":"COPPER"},
      {"id":"GPON", "text":"GPON"}
      ];

    var alpro = function() {
      return {
        data: dataalpro,
        placeholder: 'Input Alpro',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#input-alpro').select2(alpro());

    var dataOnt = [
        {"id":"ZTE", "text":"ZTE"},
        {"id":"HUAWEI", "text":"HUAWEI"},
        {"id":"ALU", "text":"ALU"}
    ];

    var ont = function() {
      return {
        data: dataOnt,
        placeholder: 'Input Jenis ONT',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#jenis_ont').select2(ont());

      $('.btn-danger').click(function() {
        var sure = confirm('Yakin hapus data ?');
        if (sure) {
          $('#delete-form').submit();
        }
      })
      var state= <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Input Regu',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.title+'</span> '+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-regu').select2(regu());
      
      var day = {
        format : 'yyyy-mm-dd',
        viewMode: 0,
        minViewMode: 0
      };
 
      $('#input-tgl').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });

      $('#tglOpen').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });

      $('#jamOpen').timepicker({
          showMeridian : false,
          disableMousewheel : false,
          minuteStep : 1
      });

      $('#noSC').select2({
        placeholder: "Masukkan No. SC",
        minimumInputLength: 2,
        ajax: { 
          url: "{{route('live_SearchSC')}}",
          dataType: 'json',
          data: function (params) {
            return {
              q: $.trim(params.term)
              // q:'11314277'
            };
          },
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });

    });

  </script>
@endsection
