@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <a href="/trendPI?dateStart={{ $dateStart }}&dateEnd={{ $dateEnd }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a>
  <h3>PI LIST JAM {{ $jam }} {{ $witel }}</h3>
  <div class="row">
  	<div class="col-sm-12">
      <div class="table-responsive">
  		<table class="table">
  			<tr>
  				<th>NO</th>
  				<th>SC</th>
  				<th>ORDER DATE</th>
  				<th>POTS</th>
  				<th>INET</th>
  				<th>NAMA</th>
  				<th>ALAMAT</th>
  				<th>KCONTACT</th>
  				<th>ORDER</th>
  				<th>STO</th>
  			</tr>
  			@foreach ($query as $num => $result)
  			<tr>
  				<td>{{ ++$num }}</td>
  				<td>SC.{{ $result->ORDER_ID }}</td>
  				<td>{{ $result->ORDER_DATE }}</td>
  				<td>{{ $result->POTS }}</td>
  				<td>{{ $result->SPEEDY }}</td>
  				<td>{{ $result->CUSTOMER_NAME }}</td>
  				<td>{{ $result->INS_ADDRESS }}</td>
  				<td>{{ $result->KCONTACT }}</td>
  				<td>{{ $result->JENISPSB }}</td>
  				<td>{{ $result->XS2 }}</td>
  			</tr>
  			@endforeach
  		</table>
    </div>
  	</div>
  </div>
@endsection
