<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class NteController extends Controller
{
  
  public function index()
  {
    $stock_nte = DB::SELECT('
      SELECT 
      b.nama_gudang,
      SUM(CASE WHEN a.jenis_nte LIKE "ONT_ZTE-F609%" THEN 1 ELSE 0 END) as ztef609,
      SUM(CASE WHEN a.jenis_nte LIKE "ONT_ZTE-F670%" THEN 1 ELSE 0 END) as ztef670,
      SUM(CASE WHEN a.jenis_nte = "ONT_HUAWEI-HG8245H" THEN 1 ELSE 0 END) as hwiHG8245H,
      SUM(CASE WHEN a.jenis_nte = "ONT_HUAWEI-HG8245H5" THEN 1 ELSE 0 END) as hwiHG8245H5,
      SUM(CASE WHEN a.jenis_nte = "ONT_HUAWEI-HG8245U" THEN 1 ELSE 0 END) as hwiHG8245U,
      SUM(CASE WHEN a.jenis_nte = "ONT_NOKIA-G240WF" THEN 1 ELSE 0 END) as nokiaG240WF,
      SUM(CASE WHEN a.jenis_nte = "STB-FIBERHOME-HG680P" THEN 1 ELSE 0 END) as fiberhomeHG680P,
      SUM(CASE WHEN a.jenis_nte = "STB-HUAWEI-EC6108V9" THEN 1 ELSE 0 END) as hwiEC6108V9,
      SUM(CASE WHEN a.jenis_nte = "STB-ZTE B860H" THEN 1 ELSE 0 END) as zteB860H,
      SUM(CASE WHEN a.jenis_nte = "STB-ZTE B760H" THEN 1 ELSE 0 END) as zteB760H,
      SUM(CASE WHEN a.jenis_nte = "STB-ZTE B700" THEN 1 ELSE 0 END) as zteB700,
      SUM(CASE WHEN a.jenis_nte = "D-LINK DHP-W310AV" THEN 1 ELSE 0 END) as dhpW310AV,
      SUM(CASE WHEN a.jenis_nte = "WIFI EXTENDER EW-7438RPN" THEN 1 ELSE 0 END) as smartN300,
      SUM(CASE WHEN a.jenis_nte = "MODEM_TP LINK-TDW8961N" THEN 1 ELSE 0 END) as tdW8961N,
      count(*) as jumlah,
      0 as id_tbl_mj
      FROM nte a 
      LEFT JOIN gudang b ON a.position_type = b.id_gudang  
      WHERE 
        a.position_key = 1
      GROUP BY b.id_gudang
      ');
    $stock_nte_teknisi = DB::SELECT('
      SELECT 
      b.nama_gudang,
      SUM(CASE WHEN a.jenis_nte LIKE "ONT_ZTE-F609%" THEN 1 ELSE 0 END) as ztef609,
      SUM(CASE WHEN a.jenis_nte LIKE "ONT_ZTE-F670%" THEN 1 ELSE 0 END) as ztef670,
      SUM(CASE WHEN a.jenis_nte = "ONT_HUAWEI-HG8245H" THEN 1 ELSE 0 END) as hwiHG8245H,
      SUM(CASE WHEN a.jenis_nte = "ONT_HUAWEI-HG8245H5" THEN 1 ELSE 0 END) as hwiHG8245H5,
      SUM(CASE WHEN a.jenis_nte = "ONT_HUAWEI-HG8245U" THEN 1 ELSE 0 END) as hwiHG8245U,
      SUM(CASE WHEN a.jenis_nte = "ONT_NOKIA-G240WF" THEN 1 ELSE 0 END) as nokiaG240WF,
      SUM(CASE WHEN a.jenis_nte = "STB-FIBERHOME-HG680P" THEN 1 ELSE 0 END) as fiberhomeHG680P,
      SUM(CASE WHEN a.jenis_nte = "STB-HUAWEI-EC6108V9" THEN 1 ELSE 0 END) as hwiEC6108V9,
      SUM(CASE WHEN a.jenis_nte = "STB-ZTE B860H" THEN 1 ELSE 0 END) as zteB860H,
      SUM(CASE WHEN a.jenis_nte = "STB-ZTE B760H" THEN 1 ELSE 0 END) as zteB760H,
      SUM(CASE WHEN a.jenis_nte = "STB-ZTE B700" THEN 1 ELSE 0 END) as zteB700,
      SUM(CASE WHEN a.jenis_nte = "D-LINK DHP-W310AV" THEN 1 ELSE 0 END) as dhpW310AV,
      SUM(CASE WHEN a.jenis_nte = "WIFI EXTENDER EW-7438RPN" THEN 1 ELSE 0 END) as smartN300,
      SUM(CASE WHEN a.jenis_nte = "MODEM_TP LINK-TDW8961N" THEN 1 ELSE 0 END) as tdW8961N,
      count(*) as jumlah
      FROM nte a 
      LEFT JOIN gudang b ON a.position_type = b.id_gudang  
      LEFT JOIN psb_laporan c ON (a.sn = c.snont OR a.sn = c.snstb)
     
      WHERE 
      a.position_key <> 1 AND
      c.id IS NULL
      GROUP BY b.id_gudang
      ');
    $stock_nte_used = DB::SELECT('
      SELECT 
      b.nama_gudang,
      SUM(CASE WHEN a.jenis_nte LIKE "ONT_ZTE-F609%" THEN 1 ELSE 0 END) as ztef609,
      SUM(CASE WHEN a.jenis_nte LIKE "ONT_ZTE-F670%" THEN 1 ELSE 0 END) as ztef670,
      SUM(CASE WHEN a.jenis_nte = "ONT_HUAWEI-HG8245H" THEN 1 ELSE 0 END) as hwiHG8245H,
      SUM(CASE WHEN a.jenis_nte = "ONT_HUAWEI-HG8245H5" THEN 1 ELSE 0 END) as hwiHG8245H5,
      SUM(CASE WHEN a.jenis_nte = "ONT_HUAWEI-HG8245U" THEN 1 ELSE 0 END) as hwiHG8245U,
      SUM(CASE WHEN a.jenis_nte = "ONT_NOKIA-G240WF" THEN 1 ELSE 0 END) as nokiaG240WF,
      SUM(CASE WHEN a.jenis_nte = "STB-FIBERHOME-HG680P" THEN 1 ELSE 0 END) as fiberhomeHG680P,
      SUM(CASE WHEN a.jenis_nte = "STB-HUAWEI-EC6108V9" THEN 1 ELSE 0 END) as hwiEC6108V9,
      SUM(CASE WHEN a.jenis_nte = "STB-ZTE B860H" THEN 1 ELSE 0 END) as zteB860H,
      SUM(CASE WHEN a.jenis_nte = "STB-ZTE B760H" THEN 1 ELSE 0 END) as zteB760H,
      SUM(CASE WHEN a.jenis_nte = "STB-ZTE B700" THEN 1 ELSE 0 END) as zteB700,
      SUM(CASE WHEN a.jenis_nte = "D-LINK DHP-W310AV" THEN 1 ELSE 0 END) as dhpW310AV,
      SUM(CASE WHEN a.jenis_nte = "WIFI EXTENDER EW-7438RPN" THEN 1 ELSE 0 END) as smartN300,
      SUM(CASE WHEN a.jenis_nte = "MODEM_TP LINK-TDW8961N" THEN 1 ELSE 0 END) as tdW8961N,
      count(*) as jumlah
      FROM nte a 
      LEFT JOIN gudang b ON a.position_type = b.id_gudang  
      LEFT JOIN psb_laporan c ON (a.sn = c.snont OR a.sn = c.snstb)
     
      WHERE 
      a.position_key <> 1 AND
      c.id IS NOT NULL
      GROUP BY b.id_gudang
      ');
    
    
    $transaksi = DB::select('
      SELECT gt.*, g.nama_gudang, k.nama, IF(position_type = "1", "IN", "OUT") as trx
      FROM gudang_transaksi gt 
      left join gudang g on gt.gudang_id=g.id_gudang
      LEFT JOIN karyawan k on gt.created_by=k.id_karyawan where 1 AND YEAR(created_at)>=2019
    ');
    
    return view('nte.list', compact('transaksi','stock_nte','stock_nte_teknisi','stock_nte_used'));
  }

  public function listGudang($nte,$gudang){

    if ($nte=="ALL") {
      $where_nte = '';
    } else {
      $where_nte = 'AND b.nte_jenis_code LIKE "%'.$nte.'%"';
      
    }
    $query = DB::SELECT('
      SELECT *,0 as id_tbl_mj, 0 as jenis_layanan, 0 as Ndem, 0 as internet, 0 as Service_ID, 0 as orderName, 0 as Customer_Name, 0 as orderAddr FROM 
        nte a 
      LEFT JOIN gudang c ON a.position_type = c.id_gudang  
      LEFT JOIN nte_jenis b ON a.jenis_nte = b.nte_jenis
      WHERE 
        a.position_key = 1 AND 
        c.nama_gudang = "'.$gudang.'" 
        '.$where_nte.'
      ');
    return view('nte.listTeknisi',compact('nte','gudang','query'));    
  }

  public function listTeknisi($nte,$gudang){
    if ($nte=="ALL") {
      $where_nte = '';
    } else {
      $where_nte = 'AND e.nte_jenis_code LIKE "%'.$nte.'%"';
      
    }
    $query = DB::SELECT('
      SELECT *,a.sn, 0 as jenis_layanan, 0 as Ndem, 0 as internet, 0 as Service_ID, 0 as orderName, 0 as Customer_Name, 0 as orderAddr  FROM 
        nte a 
      LEFT JOIN 1_2_employee b ON a.position_key = b.nik 
      LEFT JOIN psb_laporan c ON (a.sn = c.snont OR a.sn = c.snstb)
      LEFT JOIN gudang d ON a.position_type = d.id_gudang  
      LEFT JOIN nte_jenis e ON a.jenis_nte = e.nte_jenis
     WHERE 
        a.position_key <> 1 AND 
        d.nama_gudang = "'.$gudang.'" AND 
        c.id IS NULL 
        '.$where_nte.'
      ');
    return view('nte.listTeknisi',compact('nte','gudang','query'));
  }

public function listUsed($nte,$gudang){
    if ($nte=="ALL") {
      $where_nte = '';
    } else {
      $where_nte = 'AND e.nte_jenis_code LIKE "%'.$nte.'%"';
      
    }
    $query = DB::SELECT('
      SELECT *,a.sn, dd.* FROM 
        nte a 
      LEFT JOIN 1_2_employee b ON a.position_key = b.nik 
      LEFT JOIN psb_laporan c ON (a.sn = c.snont OR a.sn = c.snstb)
      LEFT JOIN dispatch_teknisi dd ON c.id_tbl_mj = dd.id
      LEFT JOIN gudang d ON a.position_type = d.id_gudang  
      LEFT JOIN nte_jenis e ON a.jenis_nte = e.nte_jenis
      LEFT JOIN Data_Pelanggan_Starclick dps ON dd.Ndem = dps.orderId
      LEFT JOIN data_nossa_1_log dn ON dd.Ndem = dn.Incident

     WHERE 
        a.position_key <> 1 AND 
        d.nama_gudang = "'.$gudang.'" AND 
        c.id IS NOT NULL 
        '.$where_nte.'
      ');
    // dd($query);
    return view('nte.listTeknisi',compact('nte','gudang','query'));
  }
  
  public function input($id){
    $exist = DB::select('select * from gudang_transaksi where id = ?',[$id]);
    if (count($exist)) {
      $data=$exist[0];
      
    }else{
      $data = new \StdClass;
    }
    $sn = DB::select('
      SELECT *
      FROM gudang_transaksi_barang g left join nte n on g.key = n.id
       where g.gudang_transaksi_id = ?
      ',[$id]);
    $gudangs = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang where 1
    ');
    return view('nte.nteMasuk', compact('gudangs', 'data', 'sn'));
  }
  public function save(Request $request, $id){
    $auth = session('auth');
    $sns = json_decode($request->input('sns'));
    if($request->input('transaksi') == '1'){
      $key = $request->input('gudang');
      $type = 1;
    }else{
      $key = 0;
      $type = 0;
    }
    $exist = DB::select('select * from gudang_transaksi where id = ?',[$id]);
    if (count($exist)) {
      $data = $exist[0];
      DB::transaction(function() use($request, $data, $auth, $key, $type) {
        DB::table('gudang_transaksi')
          ->where('id', $data->id)
          ->update([
            'modified_at'   => DB::raw('NOW()'),
            'modified_by'   => $auth->id_karyawan,
            'catatan'       => $request->input('catatan'),
            'position_type' => $request->input('transaksi'),
            'gudang_id'     => $request->input('gudang'),
            'status'     => $request->input('status')
          ]);
        $ntes = DB::select('select * from gudang_transaksi_barang where gudang_transaksi_id = ?',[$data->id]);
        foreach($ntes as $nte) {
          DB::table('nte')
            ->where('id', $nte->key)
            ->update([
              'position_type' => $type,
              'position_key'  => $key,
              'status'        => $request->input('status')
            ]);
        }
      });
      
    }else{
      DB::transaction(function() use($request, $auth , $sns , $key , $type) {
        $insertId = DB::table('gudang_transaksi')->insertGetId([
          'created_at'      => DB::raw('NOW()'),
          'created_by'   => $auth->id_karyawan,
          'gudang_id'      => $request->input('gudang'),
          'position_type' => $request->input('transaksi'),
          'catatan' => $request->input('catatan'),
          'status' => $request->input('status')
        ]);

        foreach($sns as $sn) {
          $ada = DB::select('select * from nte where sn = ?',[$sn->sn]);
          if(count($ada)){
            $nte = $ada[0];
            $idNte = $nte->id;
            DB::table('nte')
            ->where('id', $nte->id)
            ->update([
              'position_type' => $type,
              'position_key'  => $key,
              'status'        => $request->input('status')
            ]);
          }else{
            $idNte = DB::table('nte')->insertGetId([
            'sn' => $sn->sn,
            'jenis_nte' => $sn->type,
            'position_type' => $key,
            'position_key' => $type,
            'status'        => $request->input('status')
            ]);
          }
          DB::table('gudang_transaksi_barang')->insert([
            'gudang_transaksi_id' => $insertId,
            'type' => 2,
            'qty' => 1,
            'key' => $idNte
          ]);
        }
        
      });
    }
    return redirect('/nte')->with('alerts', [
       ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan Nte']
    ]);
  }

  public function destroy($id)
  {
    $exists = DB::select('select * from gudang_transaksi where id = ?',[$id]);
    $data = $exists[0];
    if($data->position_type == 1){
      $key = 0;$type = 0;    
    }else{
      $key = $data->gudang_id;
      $type = 1;
    }
    DB::transaction(function() use($id,$key, $type) {
      $ntes = DB::select('select * from gudang_transaksi_barang where gudang_transaksi_id = ?',[$id]);
      foreach($ntes as $nte){
        DB::table('nte')
          ->where('id', $nte->key)
          ->update([
            'position_type'   => $type,
            'position_key'   => $key,
            'status'       => 1
            
          ]);
      }
      DB::table('gudang_transaksi_barang')
          ->where('gudang_transaksi_id', [$id])->delete();
      DB::table('gudang_transaksi')
          ->where('id', [$id])->delete();
    });
    return redirect('/nte')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Data']
    ]);
  }
  public function stok(){
    $gudang = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang
    ');
    return view('nte.stok', compact('gudang'));
  }
  public function getAllNte(){
    $nte = DB::select('
      SELECT id, sn as text
      FROM nte
    ');
    return json_encode($nte);
  }
  public function getGudangNte($id){
    $nte = DB::select('
      SELECT id, sn as text, jenis_nte
      FROM nte where position_type = 1 and position_key = ?
    ',[
    $id
    ]);
    return json_encode($nte);
  }
  public function getSumNteType($id){
    $nte = DB::select('
      SELECT jenis_nte as jn,(select count(id) from nte where position_type = 1 and position_key = ? and jenis_nte=jn) as totalType
      FROM nte where 1 group by jenis_nte
    ',[
    $id
    ]);
    return json_encode($nte);
  }

  public function inputTeknisi(){
    $regu = DB::select('
        SELECT a.id_regu as id,a.uraian as text, b.title
        FROM regu a
          LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
        WHERE
          ACTIVE = 1
      ');

    $gudang    = DB::select('SELECT id_gudang as id, nama_gudang as text FROM gudang');
    // $materials = DB::select('SELECT *, sn as id, sn as text, null as vueSelected FROM nte WHERE position_key=1 AND position_type=2 ');
    // dd($materials);
    $materials = [];
    return view('nte.inputTeknisi',compact('regu', 'gudang', 'materials'));

  }

  public function saveTeknisi(Request $req){
      $this->validate($req,[
          'nama_gudang'   => 'required',
          'nama_regu'     => 'required'
      ],[
          'nama_gudang.required'    => 'Gudang Diplih !',
          'nama_regu.required'      => 'Regu Dipilih !'
      ]);

      if($req->sn_nte[0]==null){
          return back()->with('alerts',[['type' => 'danger', 'text' => 'NTE Jangan Dikosongi']]);
      };

      $auth = session('auth');
      $dataRegu = DB::table('regu')->where('id_regu',$req->nama_regu)->first();
      
      $dataNte = explode(',', $req->sn_nte[0]);
      foreach($dataNte as $nte){
          DB::table('nte')->where('sn',$nte)->update([
              'id_regu'   => $dataRegu->id_regu,
              'nik1'      => $dataRegu->nik1,
              'nik2'      => $dataRegu->nik2,
              'position_key'    => $dataRegu->nik1,
              'tanggal_keluar'  => DB::RAW('NOW()'),
              'petugas'         => $auth->id_user
          ]);
      };

      return back()->with('alerts',[['type' => 'success', 'text' => 'Sukses Menyimpan Data']]);
  }

  public function getListGudang($gudang){
      $materials = DB::select('SELECT *, sn as id, sn as text, null as vueSelected FROM nte WHERE position_key=1 AND position_type="'.$gudang.'" ');
      return $materials;
  }
}
