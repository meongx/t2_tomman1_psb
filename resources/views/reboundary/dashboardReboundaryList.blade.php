@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }

  </style>
  <a href="/dashboard/scbe/{{ $tgl }}/" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List {{ $title }} {{ $jenis }} {{ $so }} {{ $tgl }} </h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th rowspan="2">No.</th>
          <th rowspan="2">SM</th>
          <th rowspan="2">TL</th>
          <th rowspan="2">TEKNISI</th>
          
          @if ($status=='ALL' || $status=="UP")
            <th colspan="9">WO / TIKET</th>
          @else
            <th colspan="7">WO / TIKET</th>
          @endif
          
          <th colspan="3">NOMOR LAYANAN</th>
          <th colspan="3">DATA PELANGGAN</th>
          <th colspan="2">TRANSAKSI / MUTASI</th>
          
          @if ($status=='INSERT TIANG' || $status=='ALL')
              <th colspan="10">LAIN-LAIN</th>
          @else
              <th colspan="11">LAIN-LAIN</th>
          @endif

        </tr>

        <tr>
          <th>TIKET</th>
          <th>NOMOR SC</th> 
          <th>MYIR</th>

          <!-- <th>TGL WO</th> -->
          <th>TGL DISPATCH</th>
          
          @if($status=='ALL' || $status=="UP")
            <th>TGL PS</th>
          @endif

          <th>TGL STATUS</th>

          <th>STO</th>
          <th>TELEPON</th>
          <th>INET / DATIN</th>
          <th>NCLI</th>
          <th>NAMA</th>
          <th>ALAMAT</th>
          <th>No. HP</th>
          <th>JENIS MUTASI</th>
          <th>LAYANAN</th>
          <th>KORD. PEL </th>
          <th>ODP</th>
          <th>AREA</th>
          <th>NAMA MITRA</th>
          <th>STATUS SC</th>

          <th>STATUS TEK.</th>

          @if ($status=='INSERT TIANG' || $status=='ALL')
              <th>TAMBAH TIANG</th>
          @endif

          <th>CATATAN TEKNISI </th>
        </tr>

      
        <!-- data -->
        @foreach ($data as $num => $result)
        <tr>
          <td>{{ ++$num }}</td>
          <td>MUHAMMAD NOR RIFANI</td>
          <td>{{ $result->TL }}</td>
          <td>{{ $result->uraian }}</td>
          <td><a href="/reboundary/{{ $result->id_dt }}">INX{{ $result->Ndem }}</a></td>
          <td>{{ $result->orderId }}</td>
          
          @if($result->orderId<>"")
            @php
              $myir = '-';
              if (substr($result->jenisPsb,0,2)=='AO'){
                      $dataMyir = $result->kcontact;
                      $pisah    = explode(';',$dataMyir);
                      $myir = $pisah[1];
              };
            @endphp
            <td>{{ $myir }}</td>
          @else
            <td>-</td>
          @endif

          <td>{{ $result->tanggal_dispatch }}</td>

          @if($status=='ALL' || $status=="UP")
            <td>{{ $result->orderDatePs ?: '~' }}</td>
          @endif

          <td>{{ $result->modified_at }}</td>

          <td>{{ $result->sto OR $result->stoMy}}</td> 
          <td>{{ $result->noTelp ?: '~' }}</td>
          <td>{{ $result->ndemSpeedy ?: $result->internet ?: '~' }}</td>
          <td>{{ $result->orderNcli ?: '~' }}</td>
          <td>{{ $result->customer or $result->orderName}}</td>
          <td>{{ $result->kordinat_pelanggan or $result->orderAddr }}</td>
          <td>{{ $result->noPelangganAktif }}</td>
          <td>{{ $result->jenisPsb ?: '~' }}</td>
          <td>{{ $result->jenis_layanan ?: '~'}}</td>
         
          <td>{{ $result->kordinat_pelanggan ?: $result->kordinatPel }}</td>
          <td>{{ strtoupper($result->nama_odp) ?: $result->alproname}}</td>
          <td>{{ $result->area_migrasi }}</td>
          <td>{{ $result->mitra }}</td>
          <td>{{ $result->orderStatus ?: '~'}}</td>
          <td>{{ $result->laporan_status }}</td>
        
          @if ($status=='INSERT TIANG' || $status=='ALL')
                <td>{{ $result->ketTiang }}</td>
          @endif
          
          <td>{{ $result->catatan_teknisi ?: '~'}}</td>
        </tr>
        @endforeach
      </table>
    </div>
    </div>

  </div>    <br />
      <br />
@endsection     
