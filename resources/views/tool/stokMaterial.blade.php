@extends('layout')

@section('content')
  @include('partial.alerts')

      <div class="panel panel-primary" id="info">
          <div class="panel-heading">{{ $title }}</div>
          <div class="panel-body table-responsive">
              <table class="table table-bordered table-fixed">
                  <tr>
                      <th>#</th>
                      <th>GUDANG</th>
                      <th>WITEL</th>
                      <th>ID BARANG</th>
                      <th>BARANG</th>
                      <th>STOK</th>
                      <th>SATUAN</th>
                  </tr>
        
                  @foreach($data as $no => $d)
                          <td>{{ ++$no }}</td>
                          <td>{{ $d->nama_gudang }}</td>
                          <td>{{ $d->nama_witel }}</td>
                          <td>{{ $d->id_barang }}</td>
                          <td>{{ $d->nama_barang }}</td>
                          <td>{{ $d->total_stok }}</td>
                          <td>{{ $d->nama_satuan }}</td>
                      </tr>
                  @endforeach
              </table>
          </div>
      </div>
@endsection