<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class BackupModel{
  public static function get_backup($periode){
    $query =  DB::SELECT('SELECT * FROM dispatch_teknisi a WHERE a.tgl LIKE "'.$periode.'%"');
    return $query;
  }
  public static function get_delete($periode){
    $query = DB::SELECT('SELECT * FROM dispatch_teknisi a WHERE a.backup = 1 AND a.tgl LIKE "'.$periode.'%"');
    return $query;
  }
}
