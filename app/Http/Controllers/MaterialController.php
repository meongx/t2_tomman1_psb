<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
use App\DA\DashboardModel;
use App\DA\DispatchModel;
use App\DA\HomeModel;
use App\DA\MaterialModel;
class MaterialController extends Controller
{
  public function home($periode, Request $req){
    $auth = session('auth');
    $nik = $auth->id_user;
    $group_telegram = HomeModel::group_telegram($nik);
    $get_list_TL = MaterialModel::list_TL($periode,$nik);

    $periode = '';
    $sektorPilih = '-1001318576095';
    if ($req->has('date')){
    	$periode = $req->input('date');
        $sektorPilih  = $req->input('sektor');

    	$get_list_TL = MaterialModel::listMaterial($periode,$sektorPilih);
    };

    $sektor = DB::select('SELECT chat_id as id, title as text FROM group_telegram WHERE ket_sektor=1');
    return view('material.home',compact('group_telegram','nik','get_list_TL', 'nik', 'sektor', 'auth', 'periode', 'sektorPilih'));
  }
}
?>
