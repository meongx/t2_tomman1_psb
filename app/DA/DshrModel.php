<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class DshrModel
{
  public function QueryBuilder($Where){

  }

  public function report_Cluster(){

  }

  public static function getCloseDispatch($witel,$tglAwal,$tglAkhir){
    return DB::SELECT('
    SELECT
    a.sto,
    count(*) as jumlah
    FROM
    psb_myir_wo a
    LEFT JOIN dispatch_teknisi b ON a.sc = b.Ndem
    LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
    WHERE
      (date(a.orderDate) BETWEEN "'.$tglAwal.'" AND "'.$tglAkhir.'") AND
      c.status_laporan = 1
    GROUP BY a.sto
    ');
  }

  public static function getSudahDispatch($witel,$tglAwal,$tglAkhir){
    return DB::SELECT('
      SELECT
      a.sto,count(*) as jumlah FROM
      psb_myir_wo a
      LEFT JOIN maintenance_datel b ON a.sto = b.sto
      WHERE a.dispatch = 1 AND b.witel = "'.$witel.'" AND
      (date(a.orderDate) BETWEEN "'.$tglAwal.'" AND "'.$tglAkhir.'")
      GROUP BY a.sto
    ');
  }

  public static function simpanData($req, $noTiket, $user){
  		// cari data
  		$data = DB::table('dshr_splitter')->where('noTiket',$noTiket)->first();

  		if(count($data)==0){
  			return DB::table('dshr_splitter')->insert([
  				'noTiket'		     => $req->noTiket,
  				'nm_pelanggan'	 => $req->nmPelanggan,
  				'no_telp'		     => $req->noTelp,
  				'splitter'	   	 => $req->splitter,
  				'nm_odp'		     => $req->nmOdp,
  				'koor_odp'		   => $req->koordinatOdp,
  				'redaman_before'    => $req->redamanBefore,
  				'redaman_after'	    => $req->redamanAfter,
  				'estimasi_tarikan'	=> $req->tarikan,
  				'created_by'	      => $user,
  				'id_regu'		        => $req->idRegu,
          'source'            => $req->source
  			]);
  		}
      else{
        return back()->with('alerts',[['type' => 'danger', 'text' => 'Sudah Ada Datanya Mang !']]);
      }
  }

  public static function ubahSimpanData($req, $id, $user){
      return DB::table('dshr_splitter')->where('id',$id)->update([
          'noTiket'       => $req->noTiket,
          'nm_pelanggan'  => $req->nmPelanggan,
          'no_telp'       => $req->noTelp,
          'splitter'      => $req->splitter,
          'nm_odp'        => $req->nmOdp,
          'koor_odp'      => $req->koordinatOdp,
          'redaman_before'=> $req->redamanBefore,
          'redaman_after' => $req->redamanAfter,
          'estimasi_tarikan'  => $req->tarikan,
          'created_by'        => $user,
          'id_regu'           => $req->idRegu,
          'source'            => $req->source
        ]);
  }

  public static function listSplitter(){
  		return DB::table('dshr_splitter')
  				->leftJoin('regu','dshr_splitter.id_regu','=','regu.id_regu')
  				->select('dshr_splitter.*','regu.*')
          ->orderBy('id','DESC')
  				->get();
  }

  public static function listSplitterByTelp($cari){
  		return DB::table('dshr_splitter')
  				->leftJoin('regu','dshr_splitter.id_regu','=','regu.id_regu')
  				->select('dshr_splitter.*','regu.*')
  				->where('dshr_splitter.no_telp',$cari)
  				->orwhere('dshr_splitter.noTiket',$cari)
  				->get();
  }

  public static function getDataDshrSplitterByNoTiket($noTiket){
  		return DB::table('dshr_splitter')->where('id',$noTiket)->first();
  }

  public static function cekDshrByTelpOrInet($cari){
      return DB::table('dshr')
          ->where('no_telp','LIKE','%'.$cari.'%')
          ->orwhere('no_internet','LIKE', '%'.$cari.'%')
          ->first();
  }

  public static function simpanInputSales($req){
      return DB::table('psb_myir_wo')->insert([
          'orderDate'   => $req->tglRegistrasi,
          'myir'        => $req->trackerId,
          'customer'    => $req->nmPelanggan,
          'namaOdp'     => $req->odp,
          'lokerJns'    => 'myir',
          'picPelanggan'  => $req->picPelanggan,
          'koorPelanggan' => $req->koorPelanggan,
          'picSales'      => $req->kodeSales,
          'alamatLengkap' => $req->alamat,
          'channel'       => $req->channel,
          'layanan'       => $req->jnsLayanan,
          'psb'           => $req->transaksiPsb
      ]);
  }

  public static function getListMyirByKodeSales($kodeSales){
      return DB::table('psb_myir_wo')->where('picSales',$kodeSales)->where('dispatch',0)->get();
  }

  public static function getListMyirProses($tgl, $kodeSales){
      $sql = 'SELECT pmw.*, dt.*, pls.laporan_status_id, pls.laporan_status, r.id_regu, r.uraian
              FROM
                psb_myir_wo pmw
              LEFT JOIN dispatch_teknisi dt ON pmw.myir=dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
              LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
              LEFT JOIN regu r ON r.id_regu=dt.id_regu
              Where
                pmw.picSales = "'.$kodeSales.'" AND
                DATE(dt.updated_at) LIKE "%'.$tgl.'%" AND
                (pls.grup in ("OGP","SISA") or pl.id IS NULL)
             ';

      return DB::select($sql);
  }

  public static function getListMyirByKtKp($kodeSales, $tgl, $status){
      if($status=='listkp'){
          $ket = 'KP';
      }
      elseif($status=='listkt'){
          $ket = 'KT';
      };

      $sql = 'SELECT pmw.*, dt.*, pls.laporan_status_id, pls.laporan_status, r.id_regu, r.uraian
              FROM
                psb_myir_wo pmw
              LEFT JOIN dispatch_teknisi dt ON pmw.myir=dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
              LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
              LEFT JOIN regu r ON r.id_regu=dt.id_regu
              Where
                pmw.picSales = "'.$kodeSales.'" AND
                DATE(dt.updated_at) LIKE "%'.$tgl.'%" AND
                pls.grup = "'.$ket.'"
             ';

      return DB::select($sql);
  }

  public static function getListMyirUP($kodeSales, $tgl){
      $sql = 'SELECT pmw.*, dt.*, pls.laporan_status_id, pls.laporan_status, r.id_regu, r.uraian
              FROM
                myirSc pmw
              LEFT JOIN dispatch_teknisi dt ON pmw.sc=dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
              LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
              LEFT JOIN regu r ON r.id_regu=dt.id_regu
              Where
                pmw.picSales = "'.$kodeSales.'" AND
                DATE(dt.updated_at) LIKE "%'.$tgl.'%" AND
                pls.grup = "UP"
             ';

      return DB::select($sql);
  }

  public static function getPaketHarga(){
      return DB::select('SELECT id, paket_harga as text FROM psb_paket_harga');
  }

  public static function simpanPlasa($req){
      date_default_timezone_set('Asia/Makassar');
      $simpan =  DB::table('psb_myir_wo')->insertGetId([
          'orderDate'       => date('Y-m-d H:i:S'),
          'myir'            => $req->orderNo,
          'customer'        => strtoupper($req->nama_pemohon),
          'namaOdp'         => $req->namaOdp,
          'sc'              => NULL,
          'ket'             => 0,
          'lokerJns'        => 'plasa',
          'sto'             => $req->sto,
          'picPelanggan'    => $req->picPelanggan,
          'koorPelanggan'   => $req->koordinatPelanggan,
          'picSales'        => NULL,
          'alamatLengkap'   => $req->alamat,
          'channel'         => NULL,
          'layanan'         => $req->layanan,
          'sales_id'        => NULL,
          'psb'             => NULL,
          'dispatch'        => 0,
          'ketMyirGangguan' => 0,
          'koorOdp'         => $req->koordinatOdp,
          'nama_tagihan'    => $req->nama_tagihan,
          'no_ktp'          => $req->orderNo,
          'nama_ibu'        => $req->ibuKandung,
          'npwp'            => $req->npwp,
          'paket_harga_id'  => $req->paketHarga,
          'ket_input'       => 0,
          'email'           => $req->email,
          'kelurahan'       => $req->kelurahan,
          'terbit_ktp'      => $req->terbitKtp,
          'masa_ktp'        => $req->masaKtp,
          'no_internet'     => $req->no_internet,
          'kcontack'        => $req->kcontack,
          'created_by'      => session('auth')->id_user,
          'tgl_lahir'        => $req->tglLahir
      ]);

      return $simpan;
  }

  public static function ubahPlasa($req, $id){
      return DB::table('psb_myir_wo')->where('id', $id)->update([
          'myir'            => $req->orderNo,
          'customer'        => strtoupper($req->nama_pemohon),
          'namaOdp'         => $req->namaOdp,
          'sto'             => $req->sto,
          'koorPelanggan'   => $req->koordinatPelanggan,
          'alamatLengkap'   => $req->alamat,
          'layanan'         => $req->layanan,
          'koorOdp'         => $req->koordinatOdp,
          'nama_tagihan'    => $req->nama_tagihan,
          'no_ktp'          => $req->orderNo,
          'nama_ibu'        => $req->ibuKandung,
          'npwp'            => $req->npwp,
          'paket_harga_id'  => $req->paketHarga,
          'picPelanggan'    => $req->picPelanggan,
          'kelurahan'       => $req->kelurahan,
          'terbit_ktp'      => $req->terbitKtp,
          'masa_ktp'        => $req->masaKtp
      ]);
  }

  public static function simpanSales($req, $sales){
      date_default_timezone_set('Asia/Makassar');
      $simpan =  DB::table('psb_myir_wo')->insertGetId([
          'orderDate'       => date('Y-m-d H:i:S'),
          'myir'            => $req->myir,
          'customer'        => strtoupper($req->nama_pelanggan),
          'namaOdp'         => $req->namaOdp,
          'sc'              => NULL,
          'ket'             => 0,
          'lokerJns'        => 'myir',
          'sto'             => $req->sto,
          'picPelanggan'    => $req->pic_pelanggan,
          'koorPelanggan'   => $req->koor_pelanggan,
          'picSales'        => NULL,
          'alamatLengkap'   => $req->alamat_detail,
          'channel'         => NULL,
          'layanan'         => $req->layanan,
          'sales_id'        => $sales,
          'psb'             => $req->psb,
          'dispatch'        => 0,
          'ketMyirGangguan' => 0,
          'koorOdp'         => $req->koordinatOdp,
          'nama_tagihan'    => NULL,
          'no_ktp'          => NULL,
          'nama_ibu'        => NULL,
          'npwp'            => NULL,
          'paket_harga_id'  => NULL,
          'ket_input'       => 1,
          'email'           => $req->email,
          'paket_sales_id'  => $req->paket,
          'created_by'      => session('auth')->id_user,

      ]);

      return $simpan;
  }

  public static function simpanSalesQC($req, $sales){
      date_default_timezone_set('Asia/Makassar');
      $simpan =  DB::table('psb_myir_wo')->insertGetId([
          'orderDate'       => date('Y-m-d H:i:S'),
          'myir'            => $req->myir,
          'customer'        => strtoupper($req->nama_pelanggan),
          'namaOdp'         => $req->namaOdp,
          'sc'              => NULL,
          'ket'             => 0,
          'lokerJns'        => 'myir',
          'sto'             => $req->sto,
          'picPelanggan'    => $req->pic_pelanggan,
          'koorPelanggan'   => $req->koor_pelanggan,
          'picSales'        => NULL,
          'alamatLengkap'   => $req->alamat_detail,
          'channel'         => NULL,
          'layanan'         => $req->layanan,
          'sales_id'        => $sales,
          'psb'             => $req->psb,
          'dispatch'        => 0,
          'ketMyirGangguan' => 0,
          'koorOdp'         => $req->koordinatOdp,
          'nama_tagihan'    => NULL,
          'no_ktp'          => NULL,
          'nama_ibu'        => NULL,
          'npwp'            => NULL,
          'paket_harga_id'  => NULL,
          'ket_input'       => 1,
          'email'           => $req->email,
          'paket_sales_id'  => $req->paket,
          'created_by'      => session('auth')->id_user,
          'status_approval' => 1,
      'approve_by' => session('auth')->id_karyawan,
      'approve_date' => date('Y-m-d H:i:s')
      ]);

      return $simpan;
  }

  public static function getAllSalesPlasa($ket, $tgl, $nik){
      if ($ket=="ALL"){
          $where_ket = ' AND b.ket_input IN (0,1)';
      }
      elseif($ket=="plasa"){
          $where_ket = ' AND b.ket_input IN (0)';
      }
      elseif($ket=="sales"){
          $where_ket = ' AND b.ket_input IN (1)';
      };

      if (session('auth')->level==2 || session('auth')->level==46 || session('auth')->level==15){
          $where_nik = '';
      }
      else{
          $where_nik = ' AND b.sales_id="'.$nik.'"';
      };


      $sql = 'SELECT
                *, a.id as id_wo, b.sto as sto_wo, a.updated_at as tglDispatch, b.myir as myirInput, h.paket_sales, b.id as id_psbWo, b.email, b.created_by, b.tgl_lahir
              FROM
                dispatch_teknisi a
              LEFT JOIN psb_myir_wo b ON (a.Ndem = b.myir)
              LEFT JOIN regu c ON a.id_regu = c.id_regu
              LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
              LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
              LEFT JOIN psb_paket_harga f ON b.paket_harga_id = f.id
              LEFT JOIN group_telegram g ON c.mainsector = g.chat_id
              LEFT JOIN psb_paket_sales h ON b.paket_sales_id = h.id
              LEFT JOIN maintenance_datel i ON b.sto = i.sto
              WHERE
                a.Ndem <> "" AND
                (a.dispatch_by = 5) AND
                b.dispatch = 1 AND
                a.tgl LIKE "%'.$tgl.'%"
                '.$where_nik.'
                '.$where_ket.'
              ';

      $data = DB::select($sql);
      return $data;
  }

  public static function getAllSalesPlasaBelumDispatch_qc1($sto, $ket, $tglAwal, $tglAkhir, $nik){
      if (session('auth')->level==2 || session('auth')->level==46 || session('auth')->level==15){
          $where_nik = '';
      }
      else{
          $where_nik = ' AND b.sales_id="'.$nik.'"';
      };

      if ($sto=="ALL"){
        $where_sto = '';
      } else {
        $where_sto = ' AND b.sto = "'.$sto.'"';
      }
      $where_ket = '';
      if ($ket=="ALL"){
          $where_ket = ' AND b.ket_input IN (0,1)';
      }
      elseif($ket=="plasa"){
          $where_ket = ' AND b.ket_input IN (0)';
      }
      elseif($ket=="sales"){
          $where_ket = ' AND b.ket_input IN (1)';
      };

      $sql = 'SELECT
                b.*, b.id as id_wo, f.*, g.paket_sales
              FROM
                psb_myir_wo b
              LEFT JOIN psb_paket_harga f ON b.paket_harga_id = f.id
              LEFT JOIN psb_paket_sales g ON b.paket_sales_id = g.id
              WHERE
                b.status_approval = 0 AND
                b.ket = 0 AND
                b.dispatch = 0 AND
                (b.orderDate BETWEEN "'.$tglAwal.'" AND "'.$tglAkhir.'")
                '.$where_sto.'
                '.$where_nik.'
                '.$where_ket.'
              ';

      $data = DB::select($sql);

      return $data;
  }

  public static function getAllSalesPlasaBelumDispatch($ket, $sto, $tglAwal,$tglAkhir, $nik){
      if (session('auth')->level==2 || session('auth')->level==46 || session('auth')->level==15){
          $where_nik = '';
      }
      else{
          $where_nik = ' AND b.sales_id="'.$nik.'"';
      };

      if ($ket=="ALL"){
          $where_ket = ' AND b.ket_input IN (0,1)';
      }
      elseif($ket=="plasa"){
          $where_ket = ' AND b.ket_input IN (0)';
      }
      elseif($ket=="sales"){
          $where_ket = ' AND b.ket_input IN (1)';
      };

      $where_sto = '';
      if ($sto <> "ALL"){
        $where_sto = ' AND b.namaOdp LIKE "%'.$sto.'%"';
      }


      $sql = 'SELECT
                b.*, b.id as id_wo, f.*, g.paket_sales
              FROM
                psb_myir_wo b
              LEFT JOIN psb_paket_harga f ON b.paket_harga_id = f.id
              LEFT JOIN psb_paket_sales g ON b.paket_sales_id = g.id
              WHERE
                b.status_approval = 1 AND
                b.ket = 0 AND
                b.dispatch = 0 AND
                (b.orderDate BETWEEN "'.$tglAwal.'" AND "'.$tglAkhir.'" )
                '.$where_sto.'
                '.$where_nik.'
                '.$where_ket.'
              ';

      $data = DB::select($sql);

      return $data;
  }

  public static function getPlasaById($id){
       $getData =  DB::select('
                      SELECT *, d.id as id_wo, g.paket_sales
                      FROM psb_myir_wo d
                      left join 1_2_employee b on d.sales_id = b.nik
                      left join psb_paket_harga pkh on d.paket_harga_id = pkh.id
                      LEFT JOIN psb_paket_sales g ON d.paket_sales_id = g.id
                      WHERE d.id = ?
                      order by d.id desc
                    ',[ $id ])[0];

      return $getData;
  }

  public static function getMyirBelumDispatch($id){
      return DB::table('psb_myir_wo')->where('myir',$id)->first();
  }

  public static function getAllByTgl($tgl,$nik){
     if (session('auth')->level==2 || session('auth')->level==46 || session('auth')->level==15){
          $where_nik = '';
      }
      else{
          $where_nik = ' AND b.sales_id="'.$nik.'"';
      };

      $sql = 'SELECT
                SUM(CASE WHEN (f.area_migrasi="INNER" AND e.grup="UP") THEN 1 ELSE 0 END) as inner_up,
                SUM(CASE WHEN (f.area_migrasi="INNER" AND e.grup="KP") THEN 1 ELSE 0 END) as inner_kp,
                SUM(CASE WHEN (f.area_migrasi="INNER" AND e.grup="KT") THEN 1 ELSE 0 END) as inner_kt,
                SUM(CASE WHEN (f.area_migrasi="INNER" AND d.status_laporan=6 ) THEN 1 ELSE 0 END) as inner_Need,
                SUM(CASE WHEN (f.area_migrasi="BBR" AND e.grup="UP") THEN 1 ELSE 0 END) as bbr_up,
                SUM(CASE WHEN (f.area_migrasi="BBR" AND e.grup="KP") THEN 1 ELSE 0 END) as bbr_kp,
                SUM(CASE WHEN (f.area_migrasi="BBR" AND e.grup="KT") THEN 1 ELSE 0 END) as bbr_kt,
                SUM(CASE WHEN (f.area_migrasi="BBR" AND d.status_laporan=6) THEN 1 ELSE 0 END) as bbr_Need,
                SUM(CASE WHEN (f.area_migrasi="KDG" AND e.grup="UP") THEN 1 ELSE 0 END) as kdg_up,
                SUM(CASE WHEN (f.area_migrasi="KDG" AND e.grup="KP") THEN 1 ELSE 0 END) as kdg_kp,
                SUM(CASE WHEN (f.area_migrasi="KDG" AND e.grup="KT") THEN 1 ELSE 0 END) as kdg_kt,
                SUM(CASE WHEN (f.area_migrasi="KDG" AND d.status_laporan=6 ) THEN 1 ELSE 0 END) as kdg_Need,
                SUM(CASE WHEN (f.area_migrasi="TJL" AND e.grup="UP") THEN 1 ELSE 0 END) as tjl_up,
                SUM(CASE WHEN (f.area_migrasi="TJL" AND e.grup="KP") THEN 1 ELSE 0 END) as tjl_kp,
                SUM(CASE WHEN (f.area_migrasi="TJL" AND e.grup="KT") THEN 1 ELSE 0 END) as tjl_kt,
                SUM(CASE WHEN (f.area_migrasi="TJL" AND d.status_laporan=6 ) THEN 1 ELSE 0 END) as tjl_Need,
                SUM(CASE WHEN (f.area_migrasi="BLC" AND e.grup="UP") THEN 1 ELSE 0 END) as blc_up,
                SUM(CASE WHEN (f.area_migrasi="BLC" AND e.grup="KP") THEN 1 ELSE 0 END) as blc_kp,
                SUM(CASE WHEN (f.area_migrasi="BLC" AND e.grup="KT") THEN 1 ELSE 0 END) as blc_kt,
                SUM(CASE WHEN (f.area_migrasi="BLC" AND d.status_laporan=6 ) THEN 1 ELSE 0 END) as blc_Need
              FROM
                dispatch_teknisi a
              LEFT JOIN psb_myir_wo b ON (a.Ndem = b.myir OR a.Ndem = b.sc)
              LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
              LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
              LEFT JOIN mdf f ON b.sto = f.mdf
              WHERE
                a.Ndem <> "" AND
                (a.dispatch_by = 5 OR a.dispatch_by IS NULL) AND
                b.orderDate LIKE "%'.$tgl.'%" AND
                b.ket_input is not null
                '.$where_nik.'
              ';
      $data = DB::select($sql);

      return $data;
  }

  public static function getWoDispatchBelum($tgl, $nik){
      if (session('auth')->level==2 || session('auth')->level==46 || session('auth')->level==15){
          $where_nik = '';
      }
      else{
          $where_nik = ' AND b.sales_id="'.$nik.'"';
      };

      $sql = 'SELECT
            SUM(CASE WHEN (f.area_migrasi="INNER" AND b.dispatch=1) THEN 1 ELSE 0 END) as inner_dispatch,
            SUM(CASE WHEN (f.area_migrasi="INNER" AND b.dispatch=0) THEN 1 ELSE 0 END) as inner_undispatch,
            SUM(CASE WHEN (f.area_migrasi="BBR" AND b.dispatch=1) THEN 1 ELSE 0 END) as bbr_dispatch,
            SUM(CASE WHEN (f.area_migrasi="BBR" AND b.dispatch=0) THEN 1 ELSE 0 END) as bbr_undispatch,
            SUM(CASE WHEN (f.area_migrasi="KDG" AND b.dispatch=1) THEN 1 ELSE 0 END) as kdg_dispatch,
            SUM(CASE WHEN (f.area_migrasi="KDG" AND b.dispatch=0) THEN 1 ELSE 0 END) as kdg_undispatch,
            SUM(CASE WHEN (f.area_migrasi="TJL" AND b.dispatch=1) THEN 1 ELSE 0 END) as tjl_dispatch,
            SUM(CASE WHEN (f.area_migrasi="TJL" AND b.dispatch=0) THEN 1 ELSE 0 END) as tjl_undispatch,
            SUM(CASE WHEN (f.area_migrasi="BLC" AND b.dispatch=1) THEN 1 ELSE 0 END) as blc_dispatch,
            SUM(CASE WHEN (f.area_migrasi="BLC" AND b.dispatch=0) THEN 1 ELSE 0 END) as blc_undispatch
          FROM
              psb_myir_wo b
          LEFT JOIN mdf f ON b.sto = f.mdf
          WHERE
            b.ket_input is not null AND
            b.orderDate LIKE "%'.$tgl.'%"
            '.$where_nik.'
          ';
      $data = DB::select($sql);

      return $data;
  }

  public static function getAllWoBySales($tgl, $nik){
      $data = DB::table('psb_myir_wo')->whereDate('orderDate',$tgl)->where('sales_id',$nik)->whereNotNull('ket_input')->get();
      return count($data);
  }

  public static function getAllWoBySalesProses($tgl, $nik){
      $sql = 'SELECT
                SUM(CASE WHEN e.grup="UP" THEN 1 ELSE 0 END) as jmlUp,
                SUM(CASE WHEN e.grup="KT" THEN 1 ELSE 0 END) as jmlKt,
                SUM(CASE WHEN e.grup="KP" THEN 1 ELSE 0 END) as jmlKp,
                SUM(CASE WHEN (d.status_laporan=6 OR d.id_tbl_mj is null) THEN 1 ELSE 0 END) as jmlNeed
              FROM
                dispatch_teknisi a
              LEFT JOIN psb_myir_wo b ON (a.Ndem = b.myir)
              LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
              LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
              WHERE
                a.Ndem <> "" AND
                (a.dispatch_by = 5) AND
                a.updated_at LIKE "%'.$tgl.'%" AND
                b.ket_input is not null AND
                b.sales_id="'.$nik.'"
              ';

      return DB::select($sql);
  }

   public static function getAllSalesPlasaProgress($ket, $tgl, $nik){
      $tgl = date('Y-m',strtotime($tgl));
      if ($ket=="TOTAL"){
          $where_ket = ' ';
      }
      elseif($ket=="NEED"){
          $where_ket = ' AND (d.status_laporan=6 OR d.id_tbl_mj is null)';
      }
      else{
          $where_ket = ' AND e.grup="'.$ket.'"';
      };

      if (session('auth')->level==2 || session('auth')->level==46 || session('auth')->level==15){
          $where_nik = '';
      }
      else{
          $where_nik = ' AND b.sales_id="'.$nik.'"';
      };


      $sql = 'SELECT
                *, a.id as id_wo, b.sto as sto_wo, a.updated_at as tglDispatch, b.myir as myirInput
              FROM
                dispatch_teknisi a
              LEFT JOIN psb_myir_wo b ON (a.Ndem = b.myir)
              LEFT JOIN regu c ON a.id_regu = c.id_regu
              LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
              LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
              LEFT JOIN psb_paket_harga f ON b.paket_harga_id = f.id
              LEFT JOIN group_telegram g ON c.mainsector = g.chat_id
              WHERE
                a.Ndem <> "" AND
                (a.dispatch_by = 5) AND
                b.dispatch = 1 AND
                b.ket_input IN (0,1) AND
                a.updated_at LIKE "%'.$tgl.'%"
                '.$where_nik.'
                '.$where_ket.'
              ';

      $data = DB::select($sql);
      return $data;
  }

  public static function getAllSalesPlasaCariUndispacth($cari, $nik){
        $sql = 'SELECT
                b.*, b.id as id_wo, f.*
              FROM
                psb_myir_wo b
              LEFT JOIN psb_paket_harga f ON b.paket_harga_id = f.id
              WHERE
                b.ket = 0 AND
                b.dispatch = 0 AND
                b.ket_input IN (0,1) AND
                b.sales_id="'.$nik.'" AND
                b.myir="'.$cari.'"
              ';

      $data = DB::select($sql);
      return $data;
  }

  public static function getAllSalesPlasaCariDispatch($cari, $nik){
      $sql = 'SELECT
                *, a.id as id_wo b.sto as sto_wo, a.updated_at as tglDispatch, b.myir as myirInput
              FROM
                dispatch_teknisi a
              LEFT JOIN psb_myir_wo b ON (a.Ndem = b.myir OR a.Ndem = b.sc)
              LEFT JOIN regu c ON a.id_regu = c.id_regu
              LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
              LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
              LEFT JOIN psb_paket_harga f ON b.paket_harga_id = f.id
              LEFT JOIN group_telegram g ON c.mainsector = g.chat_id
              WHERE
                a.Ndem <> "" AND
                (a.dispatch_by = 5) AND
                b.dispatch = 1 AND
                b.ket_input IN (0,1) AND
                b.sales_id="'.$nik.'" AND
                b.myir="'.$cari.'"
              ';
      $data = DB::select($sql);
      return $data;
  }

  public static function getAllWoBySalesProsesWitek($tgl, $witel){
      $sql = 'SELECT
                SUM(CASE WHEN e.grup="UP" THEN 1 ELSE 0 END) as jmlUp,
                SUM(CASE WHEN e.grup="KT" THEN 1 ELSE 0 END) as jmlKt,
                SUM(CASE WHEN e.grup="KP" THEN 1 ELSE 0 END) as jmlKp,
                SUM(CASE WHEN (d.status_laporan=6 OR d.id_tbl_mj is null) THEN 1 ELSE 0 END) as jmlNeed,
                f.datel,
                f.witel
              FROM
                dispatch_teknisi a
              LEFT JOIN psb_myir_wo b ON (a.Ndem = b.myir)
              LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
              LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
              LEFT JOIN maintenance_datel f ON b.sto = f.sto
              WHERE
                a.Ndem <> "" AND
                (a.dispatch_by = 5) AND
                a.updated_at LIKE "%'.$tgl.'%" AND
                b.ket_input is not null AND
                f.witel = "'.$witel.'"
              GROUP BY f.datel
              ORDER BY f.id
              ';

      return DB::select($sql);
  }

  public static function getListDetailDispatcundispatch($datel, $ket, $tgl){
        if ($ket==0){
            $sql = 'SELECT
                  *, a.id as id_wo, b.sto as sto_wo, a.updated_at as tglDispatch, b.myir as myirInput, h.paket_sales, b.id as id_psbWo, b.email, b.created_by, b.tgl_lahir
                FROM
                  dispatch_teknisi a
                LEFT JOIN psb_myir_wo b ON (a.Ndem = b.myir OR a.Ndem = b.sc)
                LEFT JOIN regu c ON a.id_regu = c.id_regu
                LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
                LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
                LEFT JOIN psb_paket_harga f ON b.paket_harga_id = f.id
                LEFT JOIN group_telegram g ON c.mainsector = g.chat_id
                LEFT JOIN psb_paket_sales h ON b.paket_sales_id = h.id
                LEFT JOIN maintenance_datel i ON b.sto = i.sto
                WHERE
                  a.Ndem <> "" AND
                  (a.dispatch_by = 5 OR a.dispatch_by IS NULL) AND
                  b.dispatch = 1 AND
                  a.tgl LIKE "%'.$tgl.'%" AND
                  i.datel = "'.$datel.'"
                ';

        $data = DB::select($sql);
      };

      return $data;
  }


}
?>
