<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage;
use DB;
class NTnewsController extends Controller
{
	public function index()
	{
		return view('news.Index');
	}

	public function submit(Request $req)
	{
		$thumb = $req->file('thumb-img');
		$judul = $req->judul;
		$namathumb =$judul.'_'.time().'_'.$thumb->getClientOriginalName();
		// Storage::put('Thumbnails/'. $namathumb, file_get_contents($req->file('thumb-img')->getRealPath()));
		$path = public_path().'/upload/Thumbnails/';
		$thumb->move("$path", "$namathumb");
		// $thumb->move(public_path().'/Thumbnails/',$namathumb);
		$konten = $req->content;
		$news = "INSERT into NT_news set judul= '$judul', konten ='$konten', thumbnails ='$namathumb'";
		$result = DB::insert($news);
		return redirect('/seenews');
	}

	public function see(Request $req)
	{
		/*$hitung = DB::table('NT_news')->count();
		$pn = ceil($hitung/5);*/
		$news= DB::table('NT_news')->orderBy('create_at', 'desc')->paginate(10);
		return view('news.konten', ['berita' => $news]);
	}

	public function update(Request $req, $id_berita)
	{
		$view_content = "SELECT * FROM NT_news WHERE id_berita ='$id_berita'";
		$show_news = DB::select($view_content);
		return view('news.update', compact('show_news'));
	}

	public function view_you(Request $req, $id_berita)
	{
		// $show_news = DB::table('NT_news')->where('id_berita','=',$id_berita)->first();
		$view_content = "SELECT * FROM NT_news WHERE id_berita ='$id_berita'";
		$show_news = DB::select($view_content);
		$value=$show_news[0]->view + 1;
		$updateview= "UPDATE NT_news SET view ='$value' WHERE id_berita ='$id_berita'";
		$update_view = DB::update($updateview);
		/*$topten = "SELECT * FROM NT_news ORDER BY view DESC";
		$show_top = DB::select($topten);*/
		$news_show= DB::table('NT_news')->orderBy('view', 'desc')->limit(10)->get();
		return view('news.isi_konten', ['show' => $news_show], compact('show_news'));
	}

	public function hapus(Request $req, $id_berita)
	{
		$view_content = "SELECT * FROM NT_news WHERE id_berita ='$id_berita'";
		$show_news = DB::select($view_content);
		DB::table('NT_news')->where('id_berita','=',$id_berita)->delete();
		\File::delete(storage_path('/public/Thumbnails/'.$show_news[0]->thumbnails));
		return redirect('/seenews');
	}

	public function edit(Request $req, $id_berita){
		$thumb = $req->file('thumb-img');
		$judul = $req->judul;
		$konten = $req->content;
		$lastup= date ('Y-m-d H:i:s');
		if(!empty($thumb)){
			$view_content = "SELECT * FROM NT_news WHERE id_berita ='$id_berita'";
			$show_news = DB::select($view_content);
			\File::delete(storage_path('/public/Thumbnails/'.$show_news[0]->thumbnails));
			$namathumb =$judul.'_'.time().'_'.$thumb->getClientOriginalName();
			// Storage::put('Thumbnails/'. $namathumb, file_get_contents($req->file('thumb-img')->getRealPath()));
			$thumb->storeAs('public/Thumbnails/', $namathumb);
			// $thumb->move(public_path().'/Thumbnails/',$namathumb);
			DB::table('NT_news')->where('id_berita','=',$id_berita)->update(['thumbnails' => $namathumb, 'judul' => $judul , 'konten' => $konten , 'last_update' => $lastup ]);
			return redirect('/seenews');
		}else{
			DB::table('NT_news')->where('id_berita','=',$id_berita)->update(['judul' => $judul , 'konten' => $konten , 'last_update' => $lastup ]);
			return redirect('/seenews');
		}
		return redirect('/seenews');
	}
}
?>
