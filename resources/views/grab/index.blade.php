@extends('layout')
@section('content')
  @include('partial.alerts')
  
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <h3>
      Custom Grab SC By Order ID
    </h3>
    <div class="form-group">
      <label class="control-label" for="SC">Please Type or Paste Order ID</label>
			<input name="sc" type="text" id="SC" class="form-control" />
		</div>
    
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Sync</button>
    </div>
  </form>
@endsection