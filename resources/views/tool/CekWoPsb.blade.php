@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="panel panel-primary" id="info">
      <div class="panel-heading">WO {{ count($getData)<>0 ? $getData[0]->uraian : '' }}</div>
      <div class="panel-body table-responsive">
          <table class="table table-bordered table-fixed">
              <tr>
                  <th>#</th>
                  <th>Regu</th>
                  <th>Ndem</th>
                  <th>Jenis Transaksi</th>
                  <th>Tgl</th>
                  <th>Status</th>
              </tr>

              @foreach($getData as $no=>$dt)
              		<tr>
              			<td>{{ ++$no }}</td>
              			<td>{{ $dt->uraian }}</td>
              			
              			@if($jenis=="AO" || $jenis=="MO")
                      <?php
                          $tiket = $dt->Ndem;
                          $jenisTransaksi = 'WIFI ID';
                          if ($dt->dispatch_by==NULL){
                              $tiket = 'SC'.$dt->Ndem;
                              $jenisTransaksi = $dt->jenisPsb;
                              if($dt->ketMigrasi==1){
                              	$tiket = $dt->Ndem;
                              	$jenisTransaksi = '-';	
                              }

                          };
                       ?>

              				<td><a href="/{{ $dt->id_dt }}">{{ $tiket}}</a></td>
              				<td>{{ $jenisTransaksi }}</td>
              			@else
              				<td><a href="/tiket/{{ $dt->id_dt }}">{{ $dt->Ndem }}</a></td>
              				<td>-</td>
              			@endif

              			<td>{{ $dt->tgl }}</td>
              			<td>{{ $dt->laporan_status }}</td>
              		</tr>		
              @endforeach    
          </table>
      </div>
  </div>
@endsection