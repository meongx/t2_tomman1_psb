<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class MaterialKembaliController extends Controller
{
  public function index()
  {
    $auth = session('auth');

    $list = DB::select('
      SELECT *
      FROM gudang_kembali
      WHERE 1
    ');

    return view('materialKembali.list', compact('list'));
  }
  public function create()
  {
    $data = new \StdClass;
    $data->id = null;
    $data->id_gudang = null;
    $data->id_karyawan = null;
    $data->catatan = null;
    $data->status_material = null;
    $gudangs = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang where 1
    ');
    $karyawans = DB::select('
      SELECT id_karyawan as id,nama as text
      FROM karyawan
      WHERE witel="KALSEL" and (status_kerja!="MITRA" OR status_kerja!="RESIGN")
    ');
    $materials = DB::select('
      SELECT id_item, nama_item, 0 AS qty
      FROM item
      WHERE kat = 9
      ORDER BY id_item
    ');
    return view('materialKembali.input', compact('data', 'gudangs', 'karyawans', 'materials'));
  }
  public function input($id)
  {
    $exists = DB::select('
      SELECT *
      FROM gudang_kembali
      WHERE id = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];

      // TODO: order by most used
      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item,
          COALESCE(m.qty, 0) AS qty
        FROM item i
        LEFT JOIN
          gudang_kembali_material m
          ON i.id_item = m.id_item
          AND m.gudang_kembali_id = ?

        WHERE i.kat = 9 
        ORDER BY i.id_item
      ', [
        $data->id
      ]);
    }
    $gudangs = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang where 1
    ');
    $karyawans = DB::select('
      SELECT id_karyawan as id,nama as text
      FROM karyawan
      WHERE witel="KALSEL" and (status_kerja!="MITRA" OR status_kerja!="RESIGN")
    ');
    return view('materialKembali.input', compact('data', 'materials', 'gudangs', 'karyawans'));
  }
  public function save(Request $request, $id)
  {
    $auth = session('auth');
    $materials = json_decode($request->input('materials'));
    $karyawan = $request->input('karyawan');
    $exists = DB::select('
      SELECT *
      FROM gudang_kembali
      WHERE id = ?
    ',[
      $id
    ]);
   
    if (count($exists)) {
      $data = $exists[0];
      DB::transaction(function() use($request, $data, $auth, $materials,$karyawan) {
        //if ($data->status_material == 0){
        //$this->decrementStokGudang($data->id);
        //}else{
        //$this->decrementStokNouse($data->id);
        //}
        DB::table('gudang_kembali')
          ->where('id', $data->id)
          ->update([
            'ts'   => DB::raw('NOW()'),
            'updater'   => $auth->id_karyawan,
            'id_karyawan'       => $karyawan,
            'id_gudang' => $request->input('gudang'),
            'status_material' => $request->input('status')
          ]);

        DB::table('gudang_kembali_material')
          ->where('gudang_kembali_id', $data->id)
          ->delete();

        foreach($materials as $material) {
          DB::table('gudang_kembali_material')->insert([
            'gudang_kembali_id' => $data->id,
            'id_item' => $material->id_item,
            'qty' => $material->qty
          ]);
        }
        //if ($request->status == 0){
        //$this->incrementStokTeknisi($pj, $materials, $request->input('gudang'));
        //}
      });
      return redirect('/material-kembali')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan Data']
      ]);
    }
    else {
      DB::transaction(function() use($request, $auth, $materials) {
        $insertId = DB::table('gudang_kembali')->insertGetId([
          'ts'      => DB::raw('NOW()'),
          'updater' => $auth->id_karyawan,
          'id_karyawan'      => $request->input('karyawan'),
          'id_gudang' => $request->input('gudang'),
          'status_material' => $request->input('status')
        ]);
        foreach($materials as $material) {
          DB::table('gudang_kembali_material')->insert([
            'gudang_kembali_id' => $insertId,
            'id_item' => $material->id_item,
            'qty' => $material->qty
          ]);
        }
        //if ($request->status == 0){
        //$this->incrementStokGudang($pj, $materials, $request->input('gudang'));
        //}else{
        //$this->incrementStokNouse($pj, $materials, $request->input('gudang'));
        //}
      });
      return redirect('/material-kembali')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan Data']
      ]);
    }
  }
  public function destroy($id)
  {
    DB::transaction(function() use($id) {
      $data = DB::select('
        SELECT *
        FROM gudang_kembali
        WHERE id = ?
      ',[
        $id
      ]);
      //if($data->status_material == 0){
      //$this->decrementStokGudang($id);
      //}else{
      //$this->decrementStokNouse($id);
      //}
      DB::table('gudang_kembali')
          ->where('id', [$id])->delete();
      DB::table('gudang_kembali_material')
          ->where('gudang_kembali_id', [$id])->delete();
    });
    return redirect('/material-kembali')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Data']
    ]);
  }
}
