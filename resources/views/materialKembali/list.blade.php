@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    List Pengembalian Material
  </h3>
  
  <div class="list-group">
    @foreach($list as $no => $data)
      <a href="material-kembali/{{ $data->id }}" class="list-group-item">
        <strong>{{ $data->id_karyawan }}</strong>
        <br />
        <span>{{ $data->catatan }}</span>
        <br />
        <span>{{ $data->updater }}</span><span>{{ $data->ts }}</span>
      </a>
    @endforeach
  </div>
@endsection
