@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      text-align: center;
    }
    td {
      text-align: center;
      padding : 5px;
    }
    .center {
      text-align: center;
    }
  </style>
  <center>
  </center>
  <div class="row">
      <div class="col-sm-6">
        <h3>Dashboard Migrasi TA KALSEL</h3>
            <table class="table table-striped table-bordered dataTable">
              <!-- <tr> -->
                <th>STATUS</th>
                <th>INNER</th>
                <th>BBR</th>
                <th>KDG</th>
                <th>TJL</th>
                <th>BLC</th>
                <th>JUMLAH</th>
              <!-- </tr> -->
              <?php
                  $subTotalInner = 0;
                  $subTotalBbr = 0;
                  $subTotalKdg = 0;
                  $subTotalTjl = 0;
                  $subTotalBlc = 0;
                  $subTotal = 0;
               ?>

          @foreach($ketOrder as $order)
              <?php
                $no_update_jumlah = 0;
                $no_update_inner = 0;
                $no_update_bbr = 0;
                $no_update_kdg = 0;
                $no_update_tjl = 0;
                $no_update_blc = 0;
                $total_inner = 0;
                $total_bbr = 0;
                $total_kdg = 0;
                $total_tjl = 0;
                $total_blc = 0;
                $total = 0;
              ?>
              <tr>
                  @if($order=="orderMigrasi")
                    <td colspan="7">ORDER MIGRASI</td>
                  @elseif($order=="Shutdown")
                    <td colspan="7">SHUTDOWN</td>
                  @elseif($order=="sekunderkritis")
                    <td colspan="7">SEKUNDER KRITIS</td>
                  @elseif($order==null)
                    <td colspan="7">Data Startclick</td>
                  @else
                    <td colspan="7">EMPTY</td>
                  @endif

                  <?php
                      if ($order==''){
                          $kOrder = 'EMPTY';
                      }
                      else{
                          $kOrder = $order;
                      };
                   ?>
              </tr>

              @foreach ($datax as $result)
                @if ($order == $result->ketOrder)
                    @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                    <tr>
                      <td>{{ $result->laporan_status }}</td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/INNER/{{ $kOrder }}">{{ $result->WO_INNER }}</a></td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/BBR/{{ $kOrder }}">{{ $result->WO_BBR }}</a></td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/KDG/{{ $kOrder }}">{{ $result->WO_KDG }}</a></td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/TJL/{{ $kOrder }}">{{ $result->WO_TJL }}</a></td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/BLC/{{ $kOrder }}">{{ $result->WO_BLC }}</a></td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/ALL/{{ $kOrder }}">{{ $result->jumlah }}</a></td>
                    </tr>
                    @else
                    <?php
                      $no_update_jumlah += $result->jumlah;
                      $no_update_inner += $result->WO_INNER;
                      $no_update_bbr += $result->WO_BBR;
                      $no_update_kdg += $result->WO_KDG;
                      $no_update_tjl += $result->WO_TJL;
                      $no_update_blc += $result->WO_BLC;
                    ?>

                    @endif
                
                    <?php
                        $total_inner += $result->WO_INNER;
                        $total_bbr   += $result->WO_BBR;
                        $total_kdg   += $result->WO_KDG;
                        $total_tjl   += $result->WO_TJL;
                        $total_blc   += $result->WO_BLC;

                        $total = $total_inner + $total_bbr + $total_kdg + $total_tjl + $total_blc;
                     ?>
                @endif

              @endforeach
                    <tr>
                      <td>NO UPDATE</td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/NO UPDATE/INNER/{{ $kOrder }}">{{ $no_update_inner }}</a></td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/NO UPDATE/BBR/{{ $kOrder }}">{{ $no_update_bbr }}</a></td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/NO UPDATE/KDG/{{ $kOrder }}">{{ $no_update_kdg }}</a></td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/NO UPDATE/TJL/{{ $kOrder }}">{{ $no_update_tjl }}</a></td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/NO UPDATE/BLC/{{ $kOrder }}">{{ $no_update_blc }}</a></td>
                      <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/NO UPDATE/ALL/{{ $kOrder }}">{{ $no_update_jumlah }}</a></td>
                    </tr>

                    <tr>
                        <td>TOTAL</td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/INNER/{{ $kOrder }}">{{ $total_inner }}</a></td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/BBR/{{ $kOrder }}">{{ $total_bbr }}</a></td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/KDG/{{ $kOrder }}">{{ $total_kdg }}</a></td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/TJL/{{ $kOrder }}">{{ $total_tjl }}</a></td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/BLC/{{ $kOrder }}">{{ $total_blc }}</a></td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/ALL/{{ $kOrder }}">{{ $total }}</a></td>
                    </tr>

                    <?php
                        $subTotalInner  += $total_inner;
                        $subTotalBbr    += $total_bbr;
                        $subTotalKdg    += $total_kdg;
                        $subTotalTjl    += $total_tjl;
                        $subTotalBlc    += $total_blc;
                        $subTotal       += $total;
                     ?>
          @endforeach
                    <tr>
                        <td colspan="7"> TOTAL ALL</td>
                    </tr>

                    <tr>
                        <td>TOTAL ALL</td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/INNER/ALL">{{ $subTotalInner }}</a></td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/BBR/ALL">{{ $subTotalBbr }}</a></td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/KDG/ALL">{{ $subTotalKdg }}</a></td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/TJL/ALL">{{ $subTotalTjl }}</a></td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/BLC/ALL">{{ $subTotalBlc }}</a></td>
                        <td><a href="/dashboard/migrasiList/{{ $tgl }}/{{ $jenis }}/ALL/ALL/ALL">{{ $subTotal }}</a></td>
                    </tr>
            </table>      
      </div>
    </div>
  </div>
@endsection
