@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
  </style>
  <a href="/dashboard/rekon/{{ $tgl }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List {{ $title }} {{ $jenis }} {{ $so }} {{ $tgl }} </h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th rowspan="2">No.</th>
          <th rowspan="2">SM</th>
          <th rowspan="2">TL</th>
          <th rowspan="2">TEKNISI</th>
          <th colspan="6">WO / TIKET</th>
          <th colspan="2">NOMOR LAYANAN</th>
          <th colspan="3">DATA PELANGGAN</th>
          <th colspan="2">TRANSAKSI / MUTASI</th>
          <th colspan="4">DATA PERANGKAT TERPASANG</th>
          <th colspan="24">MATERIAL PENUNJANG</th>
          <th colspan="6">LAIN-LAIN</th>
        </tr>
        <tr>

          <th>NOMOR SC</th>
          <th>MYIR</th>
          <th>TGL WO</th>
          <th>TGL PASANG</th>
          <th>TGL PS</th>
          <th>STO</th>
          <th>TELEPON</th>
          <th>INET / DATIN</th>
          <th>NAMA</th>
          <th>ALAMAT</th>
          <th>ID SALES</th>
          <th>JENIS MUTASI</th>
          <th>LAYANAN</th>
          <th>NAMA MODEM</th>
          <th>TYPE / SERIES</th>
          <th>S/N or MAC ADDRESS</th>
          <th>MERK</th>
          <th>MODE GELARAN</th>

          <th>AD SC</th>
          <th>DC</th>
          <th>PRECON 100 M</th>
          <th>PRECON 80 M</th>
          <th>PRECON 75 M</th>
          <th>PRECON 50 M</th>
          <th>PRECON 150 Non Acc</th>
          <th>TOTAL PRECON</th>
          <th>PATCHCORE</th>
          {{-- <th>UTP CAT6</th> --}}
          <th>UTP CAT5</th>
          <th>TIANG</th>
          {{-- <th>PULL STRAP</th> --}}
          <th>CONDUIT</th>
          <th>TRAY CABLE</th>
          <th>ODP</th>
          <th>BREKET</th>
          <th>CLAMPS</th>
          <th>PULL STRAP</th>
          <th>RJ45</th>
          <th>SOC ILSINTEMTECH</th>
          <th>SOC SUMITOMO</th>
          <th>ROSET</th>
          <th>Tambah Tiang (btg)</th>

          <th>KORD. PEL </th>
          <th>KORD. ODP</th>
          <th>AREA</th>
          <th>NAMA MITRA</th>
          <th>STATUS SC</th>

          <th>STATUS TEK.</th>
          <th>CATATAN TEK.</th>
        </tr>

        {{-- {{dd($data)}} --}}

<?php
              $totalPrecon100 = 0;
              $totalPrecon80 = 0;
              $totalPrecon75 = 0;
              $totalPrecon50 = 0;
              $totalPrecon150nonAcc = 0;
              $totalPreconn = 0;
              $a = 100;
              $b = 80;
              $c = 75;
              $d = 50;
              $f = 150;
            ?>
        @foreach ($data as $num => $result)
<?php
              $totalPrecon100 = ($result->precon100 * $a);
              $totalPrecon80 = ($result->precon80 * $b);
              $totalPrecon75 = ($result->precon75 * $c);
              $totalPrecon50 = ($result->precon50 * $d);
              $totalPrecon150nonAcc = ($result->preconnonacc * $f);
              $totalPreconn = $totalPrecon100 + $totalPrecon80 + $totalPrecon75 + $totalPrecon50 + $totalPrecon150nonAcc + $result->dc_roll ;
              ?>
        <?php
          if ($result->dc_preconn>0 || $result->dc_roll>0){
            $mode_gelaran = "KU";
          } else {
            $mode_gelaran = "UTP";
          }
        ?>
        <tr>
          <td>{{ ++$num }}</td>
          <!-- <td>
            <?php
              $path = "/upload3/evidence/".$result->id_dt."/Berita_Acara.jpg";
              if (file_exists(public_path().$path)){
            ?>
            <a href="/upload/evidence/{{ $result->id_dt }}/Berita_Acara.jpg"><img src="/upload/evidence/{{ $result->id_dt }}/Berita_Acara-th.jpg" /></a>
            <?php
              } else {
                ?>
                <img src="/image/placeholder.gif" alt="" />
                <?php
              }
            ?>
          </td> -->
          <td>{{ $result->SM }}</td>
          @if ($result->mode==1 && $mode_gelaran == "UTP")
          <td bgcolor=red># {{ $result->TL }}</td>
          @else
          <td>{{ $result->TL }}</td>
          @endif
          <td>{{ $result->uraian }}</td>
          <td><a href="/{{ $result->id_dt}}">{{ $result->Ndem }}</a></td>
          <td>{{ $result->myir ?: '-' }}</td>

          {{-- <td>{{ $result->tanggal_dispatch }}</td> --}}
          <td>{{ $result->modified_at }}</td>

          <td>{{ $result->modified_at }}</td>
          <td>{{ $result->orderDatePs }}</td>
          <td>{{ $result->sto }}</td>
          <td>{{ $result->ndemPots }}</td>
          <td>{{ $result->ndemSpeedy }}</td>
          <td>{{ $result->orderName }}</td>
          <td>{{ $result->orderAddr }} {{ $result->kordinat_pelanggan }}</td>

          <!-- salas -->
          <?php
              $idSales = '-';
              if (substr($result->jenisPsb,0,2)=="AO"){
                $data = $result->kcontact;
                $dataNew = explode(';', $data);
                if ($dataNew[0] == 'MI'){
                  $idSales = $dataNew[2];
                }
                else{
                  $idSales = '-';
                }
              }
           ?>

          <td>{{ $idSales }}</td>
          <td>{{ $result->jenisPsb }}</td>
          <td>{{ strtoupper($result->jenis_layanan) }}</td>
          <td>~</td>
          <td>{{ $result->typeont }} ~ {{ $result->typestb }}</td>
          <td>{{ $result->snont }} ~ {{ $result->snstb }}</td>
          <td>~</td>
          <td>
            {{ $mode_gelaran }}
          </td>

          {{-- <td>{{ $result->dc_preconn ? : $result->dc_roll ? : "~" }}</td> --}}
          {{-- <td>{{ $result->preconnectorized }}</td> --}}
          <td>{{ $result->adsc }}</td>
          <td>{{ $result->dc_roll ? : "~" }}</td>
          <td>{{ $result->precon100 }}</td>
          <td>{{ $result->precon80 }}</td>
          <td>{{ $result->precon75 }}</td>
          <td>{{ $result->precon50 }}</td>
          <td>{{ $result->preconnonacc }}</td>
          <!-- <td>{{ $result->dc_preconn ? : '~'}}</td> -->
          <td>{{ $totalPreconn ? : '0'}}</td>
          <td>{{ $result->patchcore }}</td>
          <td>{{ $result->utp }}</td>
          <td>{{ $result->tiang }}</td>
          {{-- <td>{{ $result->pullstrap }}</td> --}}
          <td>{{ $result->conduit }}</td>
          <td>{{ $result->tray_cable }}</td>
          <td>{{ strtoupper($result->nama_odp) }}</td>

          <td>{{ $result->breket }}</td>
          <td>{{ $result->clamps }}</td>
          <td>{{ $result->pulstrap }}</td>
          <td>{{ $result->rj45 }}</td>
          <td>{{ $result->SOCIlsintentech }}</td>
          <td>{{ $result->SOCSumitomo}}</td>
          <td>{{ $result->roset }}</td>
          <td>{{ $result->jml_tiang }}</td>

          <td>{{ $result->kordinat_pelanggan }}</td>
          <td>{{ $result->kordinat_odp }}</td>
          <td>{{ $result->area_migrasi }}</td>
          <td>{{ $result->mitra }}</td>
          <td>{{ $result->orderStatus }}</td>
          <td>{{ $result->laporan_status }}</td>
          <td>{{ $result->catatan }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    </div>

  </div>    <br />
      <br />
@endsection
