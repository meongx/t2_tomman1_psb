  @extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    
    <h3>
      <a href="/maintaince/status/np" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Proggress {{ $data->no_tiket }}
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-team">Team</label>
      <input name="team" type="text" id="input-team" class="form-control" value="{{ $data->team or '' }}" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-tgl">Tanggal Selesai</label>
      <input name="tgl_selesai" type="text" id="input-tgl" class="form-control" value="{{ $data->tgl_selesai or '' }}" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-status">Status</label>
      <input name="status" type="text" id="input-status" class="form-control" value="{{ $data->status or '' }}" />
    </div>
    <input type="hidden" name="materials" value="[]" />
    <h3 style="margin-top:40px">
      Material
      <button data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info" type="button">
        <span class="glyphicon glyphicon-list"></span>
        Edit
      </button>
    </h3>

    <ul id="material-list" class="list-group">
      <li class="list-group-item" v-repeat="$data | hasQty ">
        <span class="badge" v-text="qty"></span>
        <strong v-text="id_item"></strong>
        <p v-text="nama_item"></p>
      </li>
    </ul>
    <div class="row text-center input-photos" style="margin: 20px 0">
     <label class="control-label"><b>Dokumentasi</b></label><br />
       <?php
        $number = 1;
       ?>
       @foreach($foto as $input)
        <div class="col-xs-6 col-sm-4">

          <?php
            $path = "/upload/maintenance/".Request::segment(2)."/$input";
            $th   = "$path-th.jpg";
            $img  = "$path.jpg";
            $flag = "";
            $name = "flag_".$input;
          ?>
          @if (file_exists(public_path().$th))
            <a href="{{ $img }}">
              <img src="{{ $th }}" alt="{{ $input }}" />
            </a>
            <?php
              $flag = 1;
            ?>
          @else
            <img src="/image/placeholder.gif" alt="" />
          @endif
          <br />
          <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
          <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
          <button type="button" class="btn btn-sm btn-info">
            <i class="glyphicon glyphicon-camera"></i>
          </button>
          <p>{{ str_replace('_',' ',$input) }}</p>
          {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
        </div>
      <?php
        $number++;
      ?>
      @endforeach
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>
  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Material Bon</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 300px">
          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">
              <strong v-text="id_item"></strong>
              <p v-text="nama_item"></p>
              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <!-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit" disabled></button> -->
                <input v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script>
  $(function() {
    $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);
            
          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });
    var materials = <?= json_encode($materials) ?>;

      Vue.filter('hasQty', function(value) {
        return value.filter(function(a) { return a.qty > 0 });
      });

      Vue.filter('doubleDigit', function(value) {
        var v = Number(value);
        if (v < 1) return '00';
        else if (String(v).length < 2) return '0' + v;
      });

      var listVm = new Vue({
        el: '#material-list',
        data: materials
      });

      var modalVm = new Vue({
        el: '#material-modal',
        data: materials,
        methods: {
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });
    $('#submit-form').submit(function() {
        var result = [];
        materials.forEach(function(item) {
          if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty});
        });
        $('input[name=materials]').val(JSON.stringify(result));
      });
    level = <?= session('auth')->level ?>;
    if(level == 53)
      var data = [{"id":"close", "text":"Close"},{"id":"pending", "text":"Pending"},{"id":"rekon", "text":"Rekon"},{"id":"invoice", "text":"invoice"}];
    else
      var data = [{"id":"close", "text":"Close"},{"id":"pending", "text":"Pending"}];
    var select2Options = function() {
      return {
        data: data,
        placeholder: 'Input Status',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#input-status').select2(select2Options());

    var day = {
      format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
    };
    $("#input-tgl").datepicker(day).on('changeDate', function (e) {
        $(this).datepicker('hide');
      });
  })
  </script>
@endsection
