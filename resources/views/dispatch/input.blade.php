@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    {{-- <input type="hidden" name="sto" value="{{ $data->sto }}"> --}}
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id }}" />
    @endif
    <h3>
      <a href="../dispatch/search" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Dispatch WO {{ $data->Nama ? : $data->orderName }}
    </h3>
    <div class="form-group">
      <label class="control-label" for="jenis_layanan">Jenis Layanan</label>
      <input name="jenis_layanan" type="hidden" id="jenis_layanan" class="form-control" value="<?= $data->jenis_layanan ?>"  />
      {!! $errors->first('jenis_layanan', '<span class="label label-danger">:message</span>') !!}
    </div>
    <div class="form-group">
      <label class="control-label" for="input-regu">Regu</label>
      <input name="id_regu" type="hidden" id="input-regu" class="form-control" value="{{ old('id_regu') ?: @$data->id_regu ?: '' }}" />
      {!! $errors->first('id_regu', '<span class="label label-danger">:message</span>') !!}
      {{-- <div class="col-xs-10">{!! $errors->first('id_regu', '<span class="label label-danger">:message</span>') !!}</div> --}}

    </div>
   {{--  <div class="form-group">
      <label class="control-label" for="input-timid">Kpro select tim</label>
      <input name="kpro_timid" type="hidden" id="input-timid" class="form-control" value="{{ old('kpro_timid') ?: @$data->kpro_timid ?: '' }}"  />
      <div class="col-xs-10">{!! $errors->first('kpro_timid', '<span class="label label-danger">:message</span>') !!}</div>
    </div> --}}
    <div class="form-group">
      <label class="control-label" for="input-tgl">Tanggal</label>
      <input name="tgl" type="tgl" id="input-tgl" class="form-control" value="{{ old('tgl') ?: @$data->tgl ?: '' }}" />
      {!! $errors->first('tgl', '<span class="label label-danger">:message</span>') !!}
      {{-- <div class="col-xs-10">{!! $errors->first('tgl', '<span class="label label-danger">:message</span>') !!}</div> --}}
    </div>

{{--     <div class="form-group">
      <label class="control-label" for="input-timeslot">Timeslot</label>
		  <input name="timeslot" type="hidden" id="input-timeslot" class="form-control" value="{{ old('dispatch_teknisi_timeslot_id') ?: @$data->dispatch_teknisi_timeslot_id ?: '' }}"  />
      <div class="col-xs-10">{!! $errors->first('timeslot', '<span class="label label-danger">:message</span>') !!}</div>
    </div> --}}

    {{-- {{dd($data->manja_status)}} --}}
    <div class="form-group">
      <label class="control-label" for="input-manja-status">Status Manja</label>
		  <input name="manja_status" type="hidden" id="input-manja-status" class="form-control" value="<?= $data->manja_status ?>"/>
      
     {{--  @if ($data->manja_status==0) 
          <input type="text" class="form-control" name="manjaStatus" value="Belum Manja" readonly>
      @elseif ($data->manja_status==1)
          <input type="text" class="form-control" name="manjaStatus" value="Manja Ok" readonly>
      @else
          <input type="text" class="form-control" name="manjaStatus" value="Manja NOK" readonly>
      @endif --}}

    </div>

    <div class="form-group">
      <label class="control-label" for="input-timeslot">Manja</label>
		  <textarea name="manja" id="input-manja" class="form-control" ><?= $data->manja ?></textarea>
    </div>

    <div class="form-group{{ $errors->has('kordinatPel') ? 'has-error' : '' }}">
      <label class="control-label" for="input-timeslot">Koordinat Pelanggan</label>
      <input type="text" name="kordinatPel" id="kordinatPel" value="{{ $data->kordinatPel }}" class="form-control">
      {!! $errors->first('kordinatPel','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group{{ $errors->has('alamatSales') ? 'has-error' :'' }} ">
      <label class="control-label" for="input-timeslot">Alamat Sales</label>
      <textarea name="alamatSales" id="alamatSales" class="form-control" >{{ $data->alamatSales }}</textarea>
      {!! $errors->first('alamatSales','<p class="label label-danger">:message</p>') !!}
    </div>

     <div class="form-group{{ $errors->has('ketOrder') ? 'has-error' :'' }} ">
      <label class="control-label" for="input-timeslot">Keterangan Order (Migrasi)</label>
      <input type="hidden" name="ketOrder" id="ketOrder" class="form-control"> 
      {!! $errors->first('ketOrder','<p class="label label-danger">:message</p>') !!}
    </div>

    <input type="hidden" name="ketDispatch" value="{{ $ketDispatch }}">
    


    <?php
        if (!empty($dataUnsc)){
            $catatan       = $dataUnsc->catatan;
            $koordinat_odp = $dataUnsc->koordinat_odp;
            $label_odp     = $dataUnsc->label_odp;
        }
        else{
            $catatan       = '-';
            $koordinat_odp = '-';
            $label_odp     = '-'; 
        }

     ?>

    <div class="form-group">
      <label class="control-label">Catatan UNSC SDI</label>
      <textarea name="cat_unsc" class="form-control" readonly>{{ $catatan }}</textarea>
    </div>

     <div class="form-group">
      <label class="control-label">ODP UNSC</label>
      <textarea name="odp" class="form-control" readonly>{{ $label_odp }}</textarea>
    </div>

     <div class="form-group">
      <label class="control-label">Koordinat ODP UNSC</label>
      <textarea name="koorOdp" class="form-control" readonly>{{ $koordinat_odp }}</textarea>
    </div>

    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>
  
  @if(session('auth')->level=='2')
    @if (isset($data->id))
    <form id="delete-form" method="post" autocomplete="off">
      <input name="_method" type="hidden" value="DELETE">
        <div style="margin:40px 0 20px">
          <button class="btn btn-danger">Hapus</button>
        </div>
    </form>
    @endif
  @endif

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script>
    $(function() {
      $('.btn-danger').click(function() {
        var sure = confirm('Yakin hapus data ?');
        if (sure) {
          $('#delete-form').submit();
        }
      })

      var state = <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Input Regu',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.job+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-regu').select2(regu());

      var datajenislayanan = [
        {"id":"CHANGESTB2NDSTB", "text":"<span class='label label-success'>CHANGESTB2ND</span> CHANGE STB + 2ND STB"},
        {"id":"CHANGESTBWIFIEXTENDER", "text":"<span class='label label-success'>CHANGESTBEXTENDER</span> CHANGE STB + WIFI EXTENDER"},
        {"id":"ADDSTBPLC", "text":"<span class='label label-success'>ADDSTBPLC</span> ADD STB + PLC"},
        {"id":"ADDSTBWIFIEXTENDER", "text":"<span class='label label-success'>ADDSTBWIFIEXTENDER</span> ADD STB + WIFI EXTENDER"},
        {"id":"2NDSTBPLC", "text":"<span class='label label-success'>2ND</span> 2ND STB + PLC"},
        {"id":"2NDSTBWIFIEXTENDER", "text":"<span class='label label-success'>2ND</span> 2ND STB + WIFI EXTENDER"},
        {"id":"3RDSTBPLC", "text":"<span class='label label-success'>3RD</span> 3RD STB + PLC"},
        {"id":"3RDSTBWIFIEXTENDER", "text":"<span class='label label-success'>3RD</span> 3RD STB + WIFI EXTENDER"},
        {"id":"1PVOICE", "text":"<span class='label label-success'>1P</span> VOICE"},
        {"id":"1PINET", "text":"<span class='label label-success'>1P</span> INET"},
        {"id":"1PTV", "text":"<span class='label label-success'>1P</span> TV"},
        {"id":"2PINETVOICE", "text":"<span class='label label-success'>2P</span> INET VOICE"},
        {"id":"2PINETUSEETV", "text":"<span class='label label-success'>2P</span> INET USEETV"},
        {"id":"3P", "text":"<span class='label label-success'>3P</span> VOICE INET USEETV"},
        {"id":"ADDONSTB", "text":"<span class='label label-success'>ADD ON</span> STB"},
        {"id":"CHANGESTB", "text":"<span class='label label-success'>CHANGE</span> STB"},
        {"id":"2ndSTB", "text":"<span class='label label-success'>2nd</span> STB"},
        {"id":"3ndSTB", "text":"<span class='label label-success'>3nd</span> STB"},
        {"id":"PDA", "text":"<span class='label label-success'>PDA</span> PDA"},
        {"id":"AddserviceInet", "text":"<span class='label label-success'>AddserviceInet</span> Addservice Inet"},
        {"id":"AddserviceVoice", "text":"<span class='label label-success'>AddserviceVoice</span> Addservice Voice"},
        {"id":"WifiExtender", "text":"<span class='label label-success'>WifiExtender</span> WIFI EXTENDER"},
        {"id":"plc", "text":"<span class='label label-success'>PLC</span> PLC"},
        {"id":"SMARTINDIHOME", "text":"<span class='label label-success'>SmartIndihome</span> SMART INDIHOME"},
        {"id":"2NDSTBPLCWIFI", "text":"2ND STB + PLC + WIFI EXTENDER"},
        {"id":"HOMECAMERA", "text":"HOME CAMERA"},
        {"id":"CHANGE2STBPLC", "text":"CHANGE + 2nd STB PLC"},
      ];

      var jenislayanan = function() {
        return {
          data: datajenislayanan,
          placeholder: 'Input Jenis Layanan',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }

      $('#jenis_layanan').select2(jenislayanan());

      var dataKetOrder = [
          {"id":"Shutdown", "text":"Shutdown"},
          {"id":"sukenderKritis", "text":"Sukender kritis"},
          {"id":"orderMigrasi", "text":"Order Migrasi"},
      ];

      var ketOrder = function (){
          return {
              data : dataKetOrder,
              placeholder : 'Input Keterangan Order',
              formatSelection : function (data) {return data.text},
              formatResult : function(data){return data.text}
          }
      }

      $('#ketOrder').select2(ketOrder());

      var timeslot_data = <?= json_encode($timeslot) ?>;
      var timeslot = function() {
        return {
          data: timeslot_data,
          placeholder: 'Input Timeslot',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.dispatch_teknisi_timeslot_id+'</span>'+
                '<strong style="margin-left:5px">'+data.dispatch_teknisi_timeslot+'</strong>';
          }
        }
      }
      $('#input-timeslot').select2(timeslot());

      var manja_data = <?= json_encode($get_manja_status) ?>;
      var manja = function() {
        return {
          data: manja_data,
          placeholder: 'Status Manja',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-manja-status').select2(manja());

      var tim_id = <?= json_encode($kpro_tim) ?>;
      var tim = function() {
        return {
          data: tim_id,
          placeholder: 'Pilih tim kpro',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-timid').select2(tim());

      var day = {
        format: 'yyyy-mm-dd',
        viewMode: 0,
        minViewMode: 0
      };
      $('#input-tgl').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });;
    })
  </script>
@endsection
