<!DOCTYPE html>
<html lang="en">
<head>
  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-1245774688828610",
    enable_page_level_ads: true
  });
</script>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

  <title>Tomman Ver 1.5x</title>
  {{-- tes login --}}

  <link rel="stylesheet" href="/bower_components/bootswatch-dist/css/bootstrap.min.css" />
     <style>
        @media(min-width: 769px) {
            form {
                width: 400px;
                margin: 100px auto;
            }
        }
        .new {
			    border: 1px solid #b7b7b7;
			  }
        .input-small,
				.input-medium {
				    border-color: #E56717;
				}
    </style>
</head>
<body>
  <div class="container" style="margin-top:40px">

    <form method="post">
    @include('partial.alerts')
	    <div class="form-group">
	    	<h5>Tomman Operation v1.5</h5>
	    </div>
      <div class="form-group">
        <label class="control-label" for="input-login">Login</label>
        <input name="login" type="text" id="input-login" class="form-control new" autofocus />
      </div>
      <div class="form-group">
        <label class="control-label" for="input-password">Password</label>
        <input name="password" type="password" id="input-password" class="form-control" />
      </div>
      <div>
        <button class="btn btn-primary">Login</button>
      </div>
    </form>
  </div>
</body>
</html>
