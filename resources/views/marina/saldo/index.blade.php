@extends('layout')
@section('content')
@include('marina.partial.alert')
    
    <div class="panel panel-default" id="info">
        <div class="panel-heading">LIST SALDO OUTSTANDING RFC
        	<a href="/marina/saldo/input" class="btn btn-info btn-xs pull-right" style="margin: 0 -15px;">
		        <span class="glyphicon glyphicon-plus"></span>
		        <span>Input RFC</span>
			    </a>
		  	</div>
        <div id="fixed-table-container-demo" class="table-responsive">
            <table class="table table-bordered table-fixed">
                <thead>
                  <tr>
                    <th width="20" class="head">#</th>
                    <th width="20" class="head">RFC</th>
                    <th width="20" class="head">Regu</th>
                    <th width="20" class="head">Nik1</th>
                    <th width="20" class="head">Nik2</th>
                    <th width="20" class="head">Niktl</th>
                    <th width="20" class="head">Date</th>
                    <th width="20" class="head">By</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($outstanding as $no => $list) 
                    <tr>
                      <td>{{ ++$no }}</td>
                      <td><a href="#" class="getlist" data-rfc="{{str_replace('/','*',$list->rfc)}}">{{ $list->rfc }}</a></td>
                      <td>{{ $list->regu_name }}</td>
                      <td>{{ $list->nik1 }}</td>
                      <td>{{ $list->nik2 }}</td>
                      <td>{{ $list->niktl }}</td>
                      <td>{{ $list->created_at }}</td>
                      <td>{{ $list->created_by }}</td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
   	<div id="modaldetail" class="modal fade">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button class="close" data-dismiss="modal">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      Detail
                  </h4>
              </div>
              <div class="modal-body table-responsive">
                  
              </div>
              <div class="modal-footer">
                  <button class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div>
@endsection
@section('plugins')
<script type="text/javascript">
$(function(){
	$('.getlist').click(function(event){
    var rfc = event.target.dataset.rfc;
		 	$.ajax({
        type: "GET",
        url: "/marina/modallist/"+rfc,
        dataType: "html"
	    }).done(function(data) {
	    	$('.modal-body').html(data);
	    	$('#modaldetail').modal({show:true});
	    });

	});
});
</script>
@endsection