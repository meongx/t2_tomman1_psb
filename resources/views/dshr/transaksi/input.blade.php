@extends('layout')

@section('header')
@endsection

@section('content')
  @include('partial.alerts')
  <style>
  textarea, textarea.form-control, input.form-control, input[type=text], input[type=password], input[type=email], input[type=number], [type=text].form-control, [type=password].form-control, [type=email].form-control, [type=tel].form-control, [contenteditable].form-control{
    font-size: 14px;
    background-color: #dfe6e9;
    padding-left: 10px;
    color : #000;
    border : 1px solid #636e72;
  }
  </style>

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    <input name="korlap" type="hidden" value="{{ $korlap }}">
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id }}" />
    @endif
    <h3>
      <a href="{{ route('dt') }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>

      <button class="btn btn-primary">Simpan</button>
      @if (isset($data->id))

  @endif

    </h3><br />
	@if(session('auth')->level <> 49)
		<span>Transaksi oleh {{ $data->nama ? : '' }}</span>
	@endif

    <div class="panel panel-default">
      <div class="panel-heading">Data Transaksi</div>
      <div class="panel-body">
        	<div class="row">
	      <div class="col-md-12">Tanggal Deal :
        <input name="tanggal_deal" type="text" id="input-deal" class="form-control" placeholder="Tanggal" value="{{ $data->tanggal_deal or '' }}" />
          <br />
	     </div>
	 @if(session('auth')->level == 1)
   <div class="col-md-6">Other :
        <input name="magang" type="hidden" id="input-magang" class="form-control" placeholder="Other" value="{{ $data->magang or '' }}" />
      </div>
	  @endif

      <div class="col-md-6">Status :
        <input name="status_hdesk" type="hidden" id="input-status-hdesk" class="form-control" placeholder="Status Hdesk" value="{{ $data->status_hdesk or '' }}" />
      </div>
      <div class="col-md-6">Jenis Transaksi :
        <input name="status_nal" type="hidden" id="input-status-nal" class="form-control" value="{{ $data->status_nal or '' }}" />
      </div>

      @if(session('auth')->level == 15 || session('auth')->level == 51 || session('auth')->level == 2)

	<div class="col-md-6">Kode SC :
        <input name="kode_sc" type="text" placeholder="Kode SC" id="input-kode-sc" class="form-control" value="{{ $data->kode_sc or '' }}" />
      </div>
	<div class="col-md-12">Keterangan Hdesk :
      <input name="keterangan_hdesk" placeholder="Keterangan Hdesk" type="text" id="input-keterangan-hdesk" class="form-control" value="{{ $data->keterangan_hdesk or '' }}" />
		<br />
	</div>
	 <div class="col-md-6">Jenis Layanan :
        <input name="jenis_layanan" type="hidden" id="input-jl" placeholder="Jenis Layanan" class="form-control" value="{{ $data->jenis_layanan or '' }}" />
      </div>
      <div class="col-md-6">Kodefikasi :
        <input name="kodefikasi" type="hidden" placeholder="KODEFIKASI" id="input-kode" class="form-control" value="{{ $data->kodefikasi or '' }}" />
      </div>

	@endif

      <div class="col-md-12">Flagging :
        <input name="flagging" type="hidden" id="input-flagging" class="form-control" placeholder="Flagging" value="{{ $data->flagging or '' }}" />
      </div>

      <div class="col-md-12">Jenis Transaksi :
        <input name="paperless" type="text" id="input-paperless" class="form-control" value="{{ $data->paperless OR 'MIGRASI' }}" readonly />
      </div>

	  <div class="col-md-12">Nama :
        <input name="nama" type="text" id="input-nama" class="form-control" placeholder="Nama Pelanggan" value="{{ $data->nama_pelanggan or '' }}" />
      </div>
      <div class="col-md-12">PIC Pelanggan :
        <input name="pic" type="text" id="input-pic" class="form-control" placeholder="PIC" value="{{ $data->pic or '' }}" />
      </div>
      <div class="col-md-12">Email :
        <input name="email" type="text" id="input-email" class="form-control" placeholder="PIC" value="{{ $data->email or '' }}" />
      </div>
    <div class="col-md-12">Alamat :
      <input name="alamat" type="text" placeholder="Alamat" id="input-alamat" class="form-control" value="{{ $data->alamat or '' }}" />
    <br />
	</div>

      <div class="col-md-6">MYIR :
        <input name="no_ktp" type="text" placeholder="MYIR" id="input-myir" class="form-control" value="{{ $data->no_ktp or '' }}" />
      </div>
      <div class="col-md-6">No Telp :
        <input name="no_telp" placeholder="Nomor Telpon" type="text" id="input-telp" class="form-control" value="{{ $data->no_telp or '' }}" />
      </div>
	   <div class="col-md-6">No. Internet :
        <input name="no_internet" placeholder="Nomor Internet" type="text" id="input-internet" class="form-control" value="{{ $data->no_internet or '' }}" />
      </div>


	  <br />

	     <div class="col-md-6">Nama ODP :
        <input name="odp" type="text" placeholder="Nama Odp" id="input-odp" class="form-control" value="{{ $data->nama_odp or '' }}" />
      </div>
      <div class="col-md-6">Kordinat Pelanggan :
        <input name="koor_pel" placeholder="Koordinat Pelanggan" type="text" id="input-kpel" class="form-control" value="{{ $data->koor_pel or '' }}" />
      </div>
      <div class="col-md-6">Kordinat ODP :
        <input name="koor_odp" placeholder="Koordinat ODP" type="text" id="input-kodp" class="form-control" value="{{ $data->koor_odp or '' }}" />
      </div>
	  <br />
    </div>
    <div class="form-group">Keterangan :
      <input name="keterangan" placeholder="Sertakan informasi jarak jika ada" type="text" id="input-ket" class="form-control" value="{{ $data->keterangan or '' }}" />
		<br />
	</div>

  <div class="row text-center input-photos" style="margin: 20px 0">
     <label class="control-label"><b>Dokumentasi</b></label><br />
       @foreach($photoInputs as $input)
        <div class="col-xs-6">
          <?php
            $path = "/upload/dshr/{$data->id}/$input";
            $th   = "$path-th.jpg";
            $img  = "$path.jpg";
          ?>
          @if (file_exists(public_path().$th))
            <a href="{{ $img }}">
              <img src="{{ $th }}" alt="{{ $input }}" />
            </a>
          @else
            <img src="/image/placeholder.gif" alt="" />
          @endif
          <br />
          <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
          <button type="button" class="btn btn-sm btn-info">
            <i class="glyphicon glyphicon-camera"></i>
          </button>
          <p>{{ str_replace('_',' ',$input) }}</p>
        </div>
      @endforeach
    </div>
  </div>
  </div>

    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>

  </form>
  <!--
  <div id="mapModal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Koordinat Pelanggan (<span id="lonText">0</span>, <span id="latText">0</span>)
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="mapView" style="height:450px;"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button id="btnGetMarker" class="btn btn-primary">OK</button>
                </div>
            </div>
        </div>
    </div>
  -->


  @if (isset($data->id))
    <form id="delete-form" method="post" autocomplete="off">
      <input name="_method" type="hidden" value="DELETE">
      <div style="margin:40px 0 20px">
        <button class="btn btn-danger">Hapus</button>
      </div>
    </form>
  @endif
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>

    $(document).ready(function () {
      $('#submit-form').submit(function() {
        var tgl = $("#input-deal").val();
        var jl = $("#input-jl").val();
        var harga = $("#input-harga").val();
        var paket = $("#input-paket").val();
        var nama = $("#input-nama").val();
        var pic = $("#input-pic").val();
		var tipe_transaksi = $("#input-tipe-transaksi").val();
        // if(tipe_transaksi=='' || tgl == '' || jl == '' || harga == '' || paket == '' || nama == '' || pic == ''){
        //   alert("Mohon Lengkapi lagi inputanya!!!");
        //   return false;
        // }

      });
      $('.btn-danger').click(function() {
      var sure = confirm('Yakin hapus data ?');
      if (sure) {
        $('#delete-form').submit();
      }
      });
      $("#input-jl").change(function(e) {
        var url = 'getStatus/' + e.val;
        $.getJSON(url, function(data) {
          statuschange.select2({data: data,
            placeholder: 'Input s'
          })
        })
      });
      var day = {
        format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0, autoclose: true
      };
      $("#input-deal").datepicker(day).on('changeDate', function (e) {
        $(this).datepicker('hide');
      });

    $("#input-odp").inputmask("AAA-AAA-A{2,3}/999");
	  // var paperless = [{"id":"PSB", "text":"PSB"},{"id":"MIGRASI", "text":"MIGRASI"}];
    // var paperless = [{"id":"MIGRASI", "text":"MIGRASI"}];
    //   $('#input-paperless').select2({
    //     data: paperless,
    //     placeholder: 'Jenis Transaksi'
    //   });

      var flagging = [
      {"id":"Gangguan Copper","text":"Gangguan Copper"},
        {"id":"Vandalisme", "text":"Vandalisme"},
        {"id":"Gamas", "text":"Gamas"},
        {"id":"Cluster HKSN B", "text":"Cluster HKSN B"},
        {"id":"PEMURUS 1P", "text":"PEMURUS 1P"},
        {"id":"BUMI MAS", "text":"BUMI MAS"},
        {"id":"KOMP MANDIRI LESTARI", "text":"KOMP MANDIRI LESTARI"},
        {"id":"SIMP GUSTI A", "text":"SIMP GUSTI A"},
        {"id":"SIMP GUSTI B", "text":"SIMP GUSTI B"},
        {"id":"PONDOK INDAH", "text":"PONDOK INDAH"},
        {"id":"SUTOYO S", "text":"SUTOYO S"},
        {"id":"DAHLIA KEBUN SAYUR", "text":"DAHLIA KEBUN SAYUR"},
        {"id":"DHARMA BAKTI", "text":"DHARMA BAKTI"},
        {"id":"ANTASARI", "text":"ANTASARI"},
        {"id":"PERDAGANGAN", "text":"PERDAGANGAN"},
        {"id":"CEMPAKA GUNUNG SARI", "text":"CEMPAKA GUNUNG SARI"},
        {"id":"ANDAI JAYA PERSADA", "text":"ANDAI JAYA PERSADA"},
        {"id":"ANDAI JAYA PERMAI", "text":"ANDAI JAYA PERMAI"},
        {"id":"JAHRI SALEH", "text":"JAHRI SALEH"},
        {"id":"BUMI GRAHA LESTARI", "text":"BUMI GRAHA LESTARI"}
      ];
        $('#input-flagging').select2({
          data: flagging,
          placeholder: 'Flagging'
        });

        var status_nal = [
          {"id":"1PVOICE", "text":"<span class='label label-success'>1P</span> VOICE"},
          {"id":"1PINET", "text":"<span class='label label-success'>1P</span> INET"},
          {"id":"2PINETVOICE", "text":"<span class='label label-success'>2P</span> INET VOICE"},
          {"id":"2PINETUSEETV", "text":"<span class='label label-success'>2P</span> INET USEETV"},
          {"id":"3P", "text":"<span class='label label-success'>3P</span> VOICE INET USEETV"},
          {"id":"ADDONSTB", "text":"<span class='label label-success'>ADD ON</span> STB"},
          {"id":"CHANGESTB", "text":"<span class='label label-success'>CHANGE</span> STB"},
        ];
        var jenislayanan = function() {
          return {
            data: status_nal,
            placeholder: 'Input Jenis Layanan',
            formatSelection: function(data) { return data.text },
            formatResult: function(data) {
              return  data.text;
            }
          }
        }

        $('#input-status-nal').select2(jenislayanan());

	  $('#input-status-hdesk').select2({
			initSelection: function (element, callback) {
	                var data = { "id": "{{ $data->status_hdesk_id ? : ''}}", "text": "{{ $data->status_hdesk_text ? : ''}}" };
	                callback(data);
	        },
	      ajax : {
				url : "/dshr-transaksi-status",
				dataType : "json",
				results : function (data){
					var myResults = [];
					$.each(data, function (index, item) {
	                myResults.push({
	                    'id': item.id,
	                    'text': item.text
	                });
	            });
	            return {
	                results: myResults
	            };
				}
			}
    });




	  var magang =  <?= json_encode($magang) ?>;
      $('#input-magang').select2({
        data: magang,
        placeholder: 'Input'
      });
	  var transaksi = [{"id":"PSB","text":"PSB"},{"id":"MIGRASI","text":"MIGRASI"}];
	  $('#input-tipe-transaksi').select2({
		  data: transaksi,
		  placeholder: 'Input Tipe Transaksi'
	  })
      var kode = [{"id":"CLS", "text":"CLS"},{"id":"PT2", "text":"PT2"},{"id":"PT3", "text":"PT3"},{"id":"MIG", "text":"MIG"},{"id":"IND", "text":"IND"}];
      $('#input-kode').select2({
        data: kode,
        placeholder: 'Input Kodefikasi'
      });
    })
    $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);
          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });

  </script>
  <!--
  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script>
  <script src="/script/mapmarker.js"></script>
  -->
@endsection
