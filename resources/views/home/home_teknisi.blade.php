@extends('tech_layout')

@section('content')
  @include('partial.alerts')
	<div class="row">
				<div class="col-md-12">

					<!-- Default panel -->
					<div class="panel panel-default">
						<div class="panel-heading">
							PROFILE
						</div>
						<div class="panel-body">

                @if (count($status_absen)==0)
                      <center>
                        <form method="post" action="/home/absen">                
                          <button class="btn btn-primary large">READY ?</button>
                        </form>
                        <br />
                        {{ $datex }}
                        <br />
                        <small>
                            Dengan ini Anda menyatakan "Ready" bekerja bersama tomman hari ini.
                        </small>
                      </center>
                @elseif ((count($status_absen)>0) && $status_absen[0]->approval==0)
                  <center>
                    <button class="btn btn-warning">Waiting Approval TL</button>
                  </center>
                @elseif ((count($status_absen)>0) && $status_absen[0]->approval==3)
                  <center>
                    <form method="post" action="/home/absen">
                      <button class="btn btn-primary large">READY ?</button>
                    </form>
                    <br />
                    <small>
                        Dengan ini Anda menyatakan "Ready" bekerja bersama tomman hari ini.
                    </small>
                  </center>
                @endif
            
            
              </center>
              <table style="padding:10px;">
              <tr>
                <td style="padding:15px">
                  <img src="/image/people.png" width="100" />
                </td>
                <td style="padding:10px">
                  NIK  {{ session('auth')->id_user }}<br />
                  NAMA {{ session('auth')->nama }}
                </td>
                </tr>
                </table>
						</div>
					</div>

				</div>
        <div class="col-sm-12">
        <form id="submit-form" method="post" action="/alkersarker/save" enctype="multipart/form-data" autocomplete="off">
            <div class="panel panel-default">
              <div class="panel-heading">
              ALKER & SARKER
              <div class="btn-group pull-right">
              <button type="submit" class="btn btn-success" id="submit-form">Submit</button>
              <button type="button" data-toggle="modal" data-target="#alkersarker-modal"  class="btn btn-default">Edit</button>
              </div>
              </div>
              <div class="panel-body">
              <input type="hidden" name="alkersarker" value="[]" />
              <ul id="searchlist" class="list-group">
              <ul id="alkersarker-list" class="list-group">
              <li class="list-group-item" v-repeat="$data">
                <span class="badge" v-text="qty || 0"></span>
                <p v-text="alkersarker"></p>
              </li>
            </ul>
              </ul>
              </div>
            </div>
          </div>
          </form>
			</div>
      </div>
      <div id="alkersarker-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Alker Sarker</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <div class="form-group">
            <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
          </div>

          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">
              <p v-text="alkersarker"></p>

              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                {{-- <button class="btn btn-default" type="button" v-text="0 | doubleDigit"></button> --}}
                <input v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
      <script>
      $(function() {
        var alkersarker = <?= json_encode($alkersarker) ?>;
        var listVm = new Vue({
        el: '#alkersarker-list',
        data: alkersarker
      });
        Vue.filter('doubleDigit', function(value) {
        var v = Number(value);
        if (v < 1) return '00';
        else if (String(v).length < 2) return '0' + v;
      });
      $('#submit-form').submit(function() {
        var result = [];
        alkersarker.forEach(function(item) {
          if (item.qty >= 0) result.push({alkersarker_id: item.alkersarker_id, qty: item.qty});
        });
        $('input[name=alkersarker]').val(JSON.stringify(result));
      });
       var modalVm = new Vue({
          el: '#alkersarker-modal',
          data: alkersarker,
          methods: {
            onPlus: function(item) {
              if (!item.qty) item.qty = 0;
              item.qty++;

              if (item.qty > item.saldo) item.qty = item.qty-1;
            },
            onMinus: function(item) {
              if (!item.qty) item.qty = 0;
              else item.qty--;
            }
          }
        });
      });
      </script>
@endsection
