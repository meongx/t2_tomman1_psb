@extends('layout')

@section('content')
@include('partial.alerts')
 <br>	
  <form class="row" style="margin-bottom: 20px">
  	 <div class="col-md-6">
  	 	  <a href="/dshr/splitter/input" class="btn btn-info">
		      <span class="glyphicon glyphicon-plus"></span>
		  </a>
  	 </div>	
 
     <div class="col-md-6">
          <div class="input-group {{ $errors->has('q') ? 'has-error' : '' }}">
            <input type="text" class="form-control" placeholder="Cari No. Telp / No Tiket" name="q"/>
              <span class="input-group-btn">
                <button class="btn btn-primary" type="submit">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
          </div>
          {!! $errors->first('q','<p class=help-block>:message</p>') !!}
      </div>
  </form>
 

    <div class="table-responsive">
	    <table class="table table-striped table-bordered">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>No Tiket</th>
	  		  <th>Pelanggan</th>
	          <th>No Telp</th>
	          <th>Regu</th>
	          <th>Splitter</th>
	          <th>ODP</th>
	          <th>Koordinat ODP</th>
	          <th>Redaman Before</th>
	          <th>Redaman After</th>
	          <th>Estimasi Tarikan</th>
	          <th>Source</th>
	          <th>Created By</th>
	        </tr>
	      </thead>
	      <tbody>
				@if (count($data)<>0)
					<?php $a = 1; ?>
					@foreach($data as $d)
						<tr>		
							<td>{{ $a++ }}</td>
							<td><a href="/dshr/splitter/{{ $d->id }}">{{ $d->noTiket }}</a></td>
							<td>{{ $d->nm_pelanggan }}</td>
							<td>{{ $d->no_telp }}</td>
							<td>{{ $d->uraian }}</td>
							<td>{{ $d->splitter }}</td>
							<td>{{ $d->nm_odp }}</td>
							<td>{{ $d->koor_odp }}</td>
							<td>{{ $d->redaman_before }}</td>
							<td>{{ $d->redaman_after }}</td>
							<td>{{ $d->estimasi_tarikan }}</td>
							<td>{{ $d->source ? $d->source : '-' }}</td>
							<td>{{ $d->created_by }}</td>
						</tr>
					@endforeach
				@endif	          
	      </tbody>
	    </table>
	</div>

@endsection
