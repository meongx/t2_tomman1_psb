@extends('layout')

@section('content')
  @include('partial.alerts')
<style>
.verticalTableHeader {
    text-align:center;
    white-space:nowrap;
    transform: rotate(270deg);

}
.verticalTableHeader p {
    margin:0 -100% ;
    display:inline-block;
}
.verticalTableHeader p:before{
    content:'';
    width:0;
    padding-top:110%;/* takes width as reference, + 10% for faking some extra padding */
    display:inline-block;
    vertical-align:middle;
    text-align : right;
}

</style>
<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<div class="row">
    <div class="col-md-12" id="bio_tl">
      <div class="panel panel-default">
          <div class="panel-heading">
          Profile
          </div>
          <div class="panel-body table-responsive">
            <table style="padding:10px;">
              <tr>
                <td style="padding:15px">
                  <img src="/image/people.png" width="100" />
                </td>
                <td style="padding:10px">

                  {{ session('auth')->nama }}<br />
                  @foreach ($group_telegram as $gt)
                  <span class="label label-info">{{ $gt->title }}</span>
                  @endforeach
                  <br />
                  <small>
                    Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                  </small>
                </td>
                <td style="padding : 10px" valign=top>

                </td>
              </tr>

            </table>
            @include ('partial.home')
            <br>
            <form method="GET">
                Pilih Bulan :
                <input type="text" name="date" id="input-tgl" value="{{ $periode }}">
                
                @if ($auth->id_user=="88159353" || $auth->id_user=="850056" || $auth->id_user=="94150248" || $auth->level=="2")
                  Sektor :
                  <input type="text" name="sektor" id="sektor" value="{{ $sektorPilih }}">
                @endif
                
                <input type="submit" value="Filter" class="btn btn-primary" />
            </form><br><br>

            <div class="table-responsive">
            <table class="table">
              <tr>
                <th>No</th>
                <th>NIK</th>
                <th>NAMA</th>
                <th>REGU</th>
                <th>SEKTOR</th>
                <th>ID ITEM</th>
                <th>ITEM</th>
                <th>RFC</th>
                <th>QTY</th>
                <th>TERPAKAI</th>
                <th>KEMBALI</th>
                <th>SISA</th>

                @if($nik == "MITRA_UPATEK" || $nik == "MITRA_PTAGB" || $nik == "MITRA_SPM" || $nik == "MITRA_CUI" ||$nik == "88159353" || $nik == "850056")
                  <th>TL</th>
                @endif

              </tr>
              @foreach ($get_list_TL as $num => $list_TL)
              <tr>
                <td>{{ ++$num }}</td>
                <td>{{ $list_TL->nik }}</td>
                <td>{{ $list_TL->nama }}</td>
                <td>
                    {{ $list_TL->uraian }} 
                    <label class="label label-primary">{{ $list_TL->mitra }}</label>
                </td>
                <td>{{ $list_TL->title }}</td>
                <td>{{ $list_TL->id_item }}</td>
                <td>{{ $list_TL->nama_item }}</td>
                <td>{{ $list_TL->rfc }}</td>
                <td>{{ $list_TL->jumlah }}</td>
                <td>{{ $list_TL->jmlTerpakai }}</td>
                <td>{{ $list_TL->jmlKembali }}</td>
                <td>{{ $list_TL->jumlah - $list_TL->jmlTerpakai - $list_TL->jmlKembali }}</td>

                @if($nik == "MITRA_UPATEK" || $nik == "MITRA_PTAGB" || $nik == "MITRA_SPM" || $nik == "MITRA_CUI" ||$nik == "88159353" || $nik == "850056")
                    <td>
                      <label class="label label-success">{{ $list_TL->TL_NIK }}</label>
                      {{ $list_TL->TL }}
                    </td>
                @endif
              </tr>
              @endforeach
            </table>
            </div>
        </div>
    </div>

<script>
    $(document).ready(function(){
          $("[data-toggle='popover']").popover({html:true});
          var day = {
            format : 'yyyy-mm',
            viewMode: "months",
            minViewMode: "months",
            autoClose: true
          };

          $('#input-tgl').datepicker(day).on('changeDate', function(e){
              $(this).datepicker('hide');
          });

          var data = <?=json_encode($sektor) ?>;
          $('#sektor').select2({data:data})
    })
</script>
@endsection