@extends('layout')

@section('header')
@endsection

@section('content')
  @include('partial.alerts')
  <h3>
    Send Message to Group
  </h3>
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <div class="form-group">
      <label class="control-label">Groups</label>
      <input name="group" type="hidden" id="groups" class="form-control" />

    </div>
    <div class="form-group">
      <label class="control-label">Messages</label>
      <textarea name="message" class="form-control"></textarea>
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Kirim</button>
    </div>

  </form>
  <script>

    $(document).ready(function () {
      var data = <?= json_encode($group_telegram) ?>;
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Pilih Group',
          allowClear: true,
          minimumInputLength: 1,
          escapeMarkup: function(m) { return m },
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#groups').select2(select2Options());
    })
  </script>
@endsection