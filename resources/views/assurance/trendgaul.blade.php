@extends($layout)

@section('content')
  @include('partial.alerts')
  <style>
    th, td {
      padding : 2px;
    }
  </style>
  <script src="/bower_components/devexpress-web-14.1/js/dx.chartjs.js"></script>
  <link rel="stylesheet" href="/bower_components/devexpress-web-14.1/css/dx.dark.css" />
  <h3>Trend GAUL Sektor</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel-body">
        <script type="text/javascript">
          jQuery(document).ready(function($)
          {
            $("#bar-11").dxChart({
              title: 'Trend GAUL Witel Kalsel',
              label : {
                visible : true
              },
              argumentAxis: {
                  valueMarginsEnabled: false,
                  discreteAxisDivisionMode: "crossLabels",
                  grid: {
                      visible: true
                  }
              },
              tooltip: {
                  enabled: true,
                  customizeTooltip: function (arg) {
                      return {
                          text: arg.valueText
                      };
                  }
              },
              dataSource: [
                @foreach ($get_trendgaul_date as $num => $trendgaul_date)
                { day : "{{ $trendgaul_date->date_close }}", Jumlah : {{ $total['tgl'.++$num] }}},
                @endforeach
              ],
              series: [
              {
                argumentField : "day",
                valueField: "Jumlah",
                name : "GAUL",
                type : "area",
                color: '#2ecc71'
              },
              ]
            });
          });
          </script>
          <div id="bar-11" style="height: 290px; width: 100%;"></div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post">

          <select name="sektor">
            <option value="ALL">== ALL SEKTOR ==</option>
            @foreach ($get_trendgaul_sektor as $sektor)
            <option value="{{ $sektor->mainsector ? : '' }}">{{ $sektor->title ? : 'NONTOMMAN' }}</option>
            @endforeach
          </select>
          OR
          <input name="orlike" type="text" />
          <input type="submit" name="submit" value="SUBMIT">
        </form>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Table
        </div>
        <div class="panel-body table-responsive">
          <table>
            <tr>
              <th>Sektor</th>
              @foreach ($get_trendgaul_date as $trendgaul_date)
              <th width=32 align=center>{{ $trendgaul_date->tgl_close }}</th>
              @endforeach
              <th>Jumlah</th>
            </tr>
            @foreach ($get_trendgaul_sektor as $trendgaul_sektor)
            <tr>
              <td>{{ $trendgaul_sektor->title ? : 'NONTOMMAN' }}</td>
              @foreach ($get_trendgaul_date as $num => $trendgaul_date)
              <td ><a href="trendgauldetil/{{ $trendgaul_sektor->title }}/{{ $trendgaul_date->date_close }}">{{ $data[$trendgaul_sektor->title]['tgl'.++$num] }}</a></td>
              @endforeach
              <td align=center><a href="trendgauldetil/{{ $trendgaul_sektor->title }}/ALL">{{ $trendgaul_sektor->jumlah }}</a></td>
            </tr>
            @endforeach
            <tr>
              <th>Total</th>
              <?php
                $jumlah = 0;
              ?>
              @foreach ($get_trendgaul_date as $num => $trendgaul_date)
              <?php
                $jumlah += $total['tgl'.++$num];
              ?>
              <th>{{ $total['tgl'.$num] }}</th>
              @endforeach
              <th>{{ $jumlah }}</th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
