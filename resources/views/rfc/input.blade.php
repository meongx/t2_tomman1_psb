@extends('tech_layout')

@section('content')
@include('partial.alerts')  
  <div class="panel panel-default">
      <div class="panel-heading">INPUT NO. RFC</div>
      <div class="panel-body">
           <form class="row" style="margin-bottom: 20px" method="POST" action="/rfc/input">
              <div class="col-md-12">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">No. RFC</label>
                        <input type="text" class="form-control" name="q" id="q" placeholder="Cari No. RFC" value="{{ old('q') }}">
                        {!! $errors->first('q', '<span class="label label-danger">:message</span>') !!}
                    </div>
                </div>

                <div class="col-md-3">              
                  <button class="btn btn-primary" type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                  </button>
                </div>
            </div>
          </form>
      </div>
  </div>

  @if(count($datas) <> 0)
    <div class="alert alert-success">Harap Dicek Kembali Jumlah Material RFC Sebelum Disimpan !!! </div>
    <div class="panel panel-default">
        <div class="panel-heading">No. RFC : {{ $datas[0]->no_rfc }} - {{ $existRfc }}</div>
        <div class="panel-body">
               <p></p>
               <form method="POST" action="/rfc/input/progress" enctype="multipart/form-data">
                  <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Tgl Pengambilan</th>
                              <th>Gudang</th>
                              <th>Pengambil</th>
                              <th>Mitra</th>
                              <th>ID Barang</th>
                              <th>Nama Barang</th>
                              <th>Jumlah</th>
                            </tr>
                          </thead>
                          <tbody>
                              @foreach ($datas as $no=>$data)
                                  <tr>
                                      <td>{{ ++$no }}</td>
                                      <td>{{ $data->tgl }}</td>
                                      <td>{{ $data->nama_gudang }}</td>
                                      <td>{{ $data->pengambil }}</td>
                                      <td>{{ $data->mitra }}</td>
                                      <td>{{ $data->id_barang }}</td>
                                      <td>{{ $data->nama_barang }}</td>
                                      <td>{{ $data->jumlah }}</td>
                                  </tr>
                              @endforeach
                          </tbody>

                          <div class="row text-center input-photos" style="margin: 20px 0">
                            <?php
                                $path = "/".$table->public."/rfc/";
                                $foto = str_replace('/','-', $datas[0]->no_rfc);
                                $exe  = ".jpg";
                                $th   = $path.$foto.$exe;
                              
                             ?>

                            @if (file_exists(public_path().$th))
                                <?php  $flag = 1 ?>
                                <a href="{{ $th }} ">
                                    <img src="{{ $th }}" width="100" alt="" / height="150"><br>
                                </a> 
                            @else
                                <?php  $flag = 0 ?>
                                <img src="/image/placeholder.gif" width="100" alt="" / height="150"><br>
                            @endif                         
                        
                            <input type="text" class="hidden" name="flag" value="{{ $flag }}"/>
                            <input type="file" class="hidden" name="photo" accept="image/jpeg" />
                            <button type="button" class="btn btn-sm btn-info">
                              <i class="glyphicon glyphicon-camera"></i>
                            </button>
                            <p>Foto Material</p>
                          </div> 

                        </table>
                  </div>

                  <input type="hidden" name="noRfc" value="{{ $datas[0]->no_rfc }}">
                  <input type="hidden" name="namaGudang" value="{{ $gudangRfc }}">

                  <input type="radio" name="aksi" id="aksi1" value="1">Jumlah Material Valid
                  <input type="radio" name="aksi" id="aksi2" value="2">Jumlah Material Tidak Valid

                  <br>
                  <input type="submit" class="btn btn-primary" id="hilangSimpan" value="Simpan Material">

                  <?php
                      $rfcNew = str_replace('/', '-', $datas[0]->no_rfc);
                   ?>

                  <a href="/reset-rfc/{{ $rfcNew }}/{{ $gudangRfc }}" class="btn btn-danger" id="hilangReset">Reset Jumlah</a>
                 
              </form>
        </div>
    </div>
  @endif

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
      $(document).ready(function(){
          $('input[type=file]').change(function() {
            console.log(this.name);
            var inputEl = this;
            if (inputEl.files && inputEl.files[0]) {
              $(inputEl).parent().find('input[type=text]').val(1);
              var reader = new FileReader();
              reader.onload = function(e) {
                $(inputEl).parent().find('img').attr('src', e.target.result);

              }
              reader.readAsDataURL(inputEl.files[0]);
            }
          });

          $('.input-photos').on('click', 'button', function() {
            $(this).parent().find('input[type=file]').click();
          });

          $('#tgl').datepicker({
            format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
          }).datepicker("setDate", "today");

          $('#hilangSimpan').hide();
          $('#hilangReset').hide();

          $('#aksi1').on('click',function(){
              $('#hilangSimpan').show();
              $('#hilangReset').hide();
          });

          $('#aksi2').on('click',function(){
              $('#hilangReset').show();
              $('#hilangSimpan').hide();
          });
      });
  </script>
@endsection
