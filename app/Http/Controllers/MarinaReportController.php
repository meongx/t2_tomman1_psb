<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use DB;
use Telegram;
use App\DA\User;
use Excel;

class MarinaReportController extends Controller
{
  protected $photoOdpSehat = [
      'GRID_TIANG', 'GRID_PROGRES_TIANG','GRID_AKSESORIS', 'GRID_PROGRES_AKSESORIS', 'GRID_ODP', 'GRID_PROGRES_ODP', 'GRID_REBOUNDARY', 'GRID_PROGRES_REBOUNDARY', 'GRID_DENAH'
    ];
  // public function reportDpTiang($date){

  //   $data = DB::select('select *, (select sum(qty) from maintaince_mtr mm left join maintaince m on mm.maintaince_id = m.id where m.kandatel=ms.datel and m.status = "close" and (mm.id_item = "TT-7" or mm.id_item = "TT-9") and tgl_selesai like "'.$date.'%") as tiang from maintenance_so ms');
  //   //dd($data);
  //   return view('report.dptiang', compact('data'));
  // }
  // public function reportDpTiangAjax($tgl, $dtl){

  //   $data = DB::select('SELECT sto as datel,(select sum(qty) from maintaince_mtr mm left join maintaince m on mm.maintaince_id = m.id where m.sto=maintaince.sto and m.status = "close" and (mm.id_item = "TT-7" or mm.id_item = "TT-9") and tgl_selesai like "'.$tgl.'%") as tiang FROM maintaince where kandatel = "'.$dtl.'" and tgl_selesai like "'.$tgl.'%" GROUP BY sto ORDER BY sto');
  //   //dd($data);
  //   return view('report.tiangAjax', compact('data'));
  // }
  // public function reportAbsen($date){
  //   $data = DB::select('select *,
  //   (select count(mal.id) from maintenance_regu mr left join maintenance_absen_log mal on mr.nik1 = mal.id_user where mal.status=2 and chat_id = mg.chat_id and created_at like "'.$date.'%") as nik1,
  //   (select count(mal.id) from maintenance_regu mr left join maintenance_absen_log mal on mr.nik2 = mal.id_user where mal.status=2 and chat_id = mg.chat_id and created_at like "'.$date.'%") as nik2,
  //   (select count(mal.id) from maintenance_regu mr left join maintenance_absen_log mal on mr.nik3 = mal.id_user where mal.status=2 and chat_id = mg.chat_id and created_at like "'.$date.'%") as nik3,
  //   (select count(mal.id) from maintenance_regu mr left join maintenance_absen_log mal on mr.nik4 = mal.id_user where mal.status=2 and chat_id = mg.chat_id and created_at like "'.$date.'%") as nik4
  //   from maintenance_grup mg where 1');
  //   return view('report.absen', compact('data'));
  // }
  // public function ajaxReportAbsen($id, $tgl){
  //   $query="";
  //   if($id)
  //     $query=" and mr.chat_id = ".$id;
  //   $data = DB::select("select mr.*, 
  //           (select count(status) from maintenance_absen_log mal where id_user = mr.nik1 and mal.created_at like '".$tgl."%' and mal.status=2) as status1,
  //           (select count(status) from maintenance_absen_log mal2 where mal2.id_user = mr.nik2 and mal2.created_at like '".$tgl."%' and mal2.status=2) as status2,
  //           (select count(status) from maintenance_absen_log mal3 where mal3.id_user = mr.nik3 and mal3.created_at like '".$tgl."%' and mal3.status=2) as status3,
  //           (select count(status) from maintenance_absen_log mal4 where mal4.id_user = mr.nik4 and mal4.created_at like '".$tgl."%' and mal4.status=2) as status4,
  //           (select nama from karyawan where id_karyawan=nik1) as nama1,
  //           (select nama from karyawan where id_karyawan=nik2) as nama2,
  //           (select nama from karyawan where id_karyawan=nik3) as nama3,
  //           (select nama from karyawan where id_karyawan=nik4) as nama4
  //           from maintenance_regu mr
  //           where mr.status_regu=1 ".$query);
  //   return view('report.ajaxAbsen', compact('data'));
  // }
  // public function grab_remo(){
  //   ini_set('max_execution_time', 60000);
  //   $filename = 'http://10.65.10.73:8081/pimcore/modules/3rdparty/mpdf/exportqefiberopticnew_2018.php?thnbln='.date('Ym').'&witel=KALSEL#level4';
  //   $serv = file_get_contents($filename);
  //   $clean = preg_replace('#\\x04\\x02..........#ms', "\t", substr(trim($serv), 119));
  //   $clean = preg_replace('#\\000\\000............#ms', "\n", $clean);
  //   $head = array();
  //   $result = array();
  //   $line = explode("\n", $clean);
  //   for($i = 1;$i<count($line);$i++){
  //     if($i==1){
  //       /*
  //       $heading = $line[$i];
  //       var_dump($line[0]);
  //       $heading = str_replace('ND', 'NO_SPEEDY', $heading);

  //       $data = explode("\t", $heading);
  //       $head = $data;
  //       array_pop($head);
  //       */
  //       $head = array("DIVRE", "WITEL", "CMDF", "RK", "DP", "NO_SPEEDY", "NODE_ID", "NODE_IP", "SLOT", "PORT", "ONU", "ONU_DESC", "ONU_TYPE", "ONU_SN", "FIBER_LENGTH", "OLT_RX_POWER", "OLT_RX_POWER_AKHIR", "ONU_RX_POWER", "ONU_RX_POWER_AKHIR", "TGL_UKUR_AKHIR", "STATUS", "ALAMAT", "STATUS_WARRANTY", "IS_CABUT", "IS_KW1");
  //       var_dump($head);
          
  //     }else{
  //       $data = explode("\t", $line[$i]);
  //       //dd($data);
  //       if($data[0] == '@'){
  //         foreach($head as $d => $h){
  //           $result[$i][$h] = trim(@$data[$d+1]);
  //         }
  //       }else{
  //         foreach($head as $d => $h){
  //           $result[$i][$h] = trim(@$data[$d]);
  //         }  
  //       }
  //     }
  //   }
  //   //var_dump($result);
  //   $records = array_chunk($result, 500);
  //   DB::table('remo_servo')->truncate();
  //   foreach ($records as $batch) {
  //     DB::table('remo_servo')->insert($batch); 
  //   }
  // }
    public function matrix($id, $jenis_order)
    {
      $auth = session('auth');
      $query = "";
      if($jenis_order)
        $query = " and jenis_order ='".$jenis_order."'";
      $list = array();
      $lastRegu = "a";
      $ls = DB::select("select * from maintaince where ((status = 'kendala teknis' and modified_at like '%".$id."%') 
        or (status = 'kendala pelanggan' and modified_at like '%".$id."%') 
        or (status = 'close' and tgl_selesai like '%".$id."%') 
        or (status is null)) and refer is null ".$query."
        order by dispatch_regu_id asc, created_at asc");
      foreach($ls as $l){
        if($lastRegu == $l->dispatch_regu_id){
          $list[$l->dispatch_regu_id][] = array("tiket"=>$l->no_tiket, "status_laporan"=>$l->status, "created_at" => $l->created_at, "id" => $l->id);
        }else{
          $list[$l->dispatch_regu_id][] = array("tiket"=>$l->no_tiket, "status_laporan"=>$l->status, "created_at" => $l->created_at, "id" => $l->id);
        }
        $lastRegu = $l->dispatch_regu_id;
      }
      // dd($list);
      // $regu = array();
      // $mitra = "";
      // foreach(DB::table('maintenance_grup')->orderBy('urutan', 'asc')->get() as $mr){
        $regu = DB::select("select *,
          (select count(*) from maintaince where dispatch_regu_id = mr.id_regu and refer is null ".$query.") as jumlah,
          (select count(*) from maintaince where dispatch_regu_id = mr.id_regu and status is null and refer is null ".$query.") as no_update,
          (select count(*) from maintaince where dispatch_regu_id = mr.id_regu and status = 'kendala teknis' and modified_at like '%".$id."%' and refer is null ".$query.") as kendala_teknis,
          (select count(*) from maintaince where dispatch_regu_id = mr.id_regu and status = 'kendala pelanggan' and modified_at like '%".$id."%' and refer is null ".$query.") as kendala_pelanggan,
          (select count(*) from maintaince where dispatch_regu_id = mr.id_regu and status = 'close' and tgl_selesai like '%".$id."%' and refer is null ".$query.") as close
          from regu mr where mr.status_team = 'ACTIVE' order by id_regu asc");
          // $regu[$mr->urutan]['data'] = $data;
          // $regu[$mr->urutan]['grup'] = $mr->grup;
      // }
      //dd($list);
      return view('marina.report.matrix', compact('regu', 'list'));
    }
  //   public function rekap(){
  //     $data = DB::select('select * from maintaince where 1');
  //     return view('report.rekap', [$data]);
  //   }
  //   public function reportUmur($tgl, $id)
  //   {
  //     $order = DB::table('maintenance_jenis_order')->where('id', $id)->first();
  //     $data = DB::select('select * from
  //       (select *, 
  //       (SELECT count(*)
  //         FROM maintaince m
  //         WHERE DATEDIFF( NOW( ) , created_at )<1 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as a,
  //       (SELECT count(*)
  //         FROM maintaince m
  //         WHERE DATEDIFF( NOW( ) , created_at )>=1 and DATEDIFF( NOW( ) , created_at )<=2 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as b,
  //       (SELECT count(*)
  //         FROM maintaince m
  //         WHERE DATEDIFF( NOW( ) , created_at )>2 and DATEDIFF( NOW( ) , created_at )<=3 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as c,
  //       (SELECT count(*)
  //         FROM maintaince m
  //         WHERE DATEDIFF( NOW( ) , created_at )>3 and DATEDIFF( NOW( ) , created_at )<=7 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as d,
  //       (SELECT count(*)
  //         FROM maintaince m
  //         WHERE DATEDIFF( NOW( ) , created_at )>7 and DATEDIFF( NOW( ) , created_at )<=30 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as e,
  //       (SELECT count(*)
  //         FROM maintaince m
  //         WHERE DATEDIFF( NOW( ) , created_at )>30 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as f,
  //       (SELECT count(*)
  //         FROM maintaince m
  //         WHERE jenis_order = '.$id.' and status is null and refer is null and m.sto=md.sto) as total
  //       from maintenance_datel md where 1 order by md.datel asc) asd where total>0');
  //     return view('report.odploss', compact('data', 'order'));
  //     /*
  //     day>30
  //     day>7 and day<=30
  //     day>3 and day<=7 
  //     day>2 and day<=3
  //     day>=1 and day<=2
  //     day<1
  //     */
  //   }
  //    public function detilReport($query)
  //   {
  //     $data = DB::select('select * from maintaince where '.$query.' and refer is null and status is null');
  //     return view('order.ajaxVerify', compact('data'));
  //   }
  //   public function produktifitasbulanan($id){
  //     $auth = session('auth');
  //     $mitra = "";
  //     if($auth->nama_instansi != "TELKOM AKSES")
  //       $mitra = " mr.mitra='".$auth->nama_instansi."' and ";
  //     $data = DB::select('select * from (select *, 
  //       (select nama from karyawan where id_karyawan = mr.nik1) as nama1, 
  //       (select nama from karyawan where id_karyawan = mr.nik2) as nama2,
  //       (select nama from karyawan where id_karyawan = mr.nik3) as nama3, 
  //       (select nama from karyawan where id_karyawan = mr.nik4) as nama4,
  //       (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id and refer is null) as close,
  //       (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id and m.jenis_order=3 and refer is null) as odp_loss,
  //       (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id and m.jenis_order=2 and refer is null) as gamas,
  //       (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id and m.jenis_order=1 and refer is null) as benjar,
  //       (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id and m.jenis_order=4 and refer is null) as remo,
  //       (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id and m.jenis_order=5 and refer is null) as utilitas,
  //       (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id and m.jenis_order=6 and refer is null) as insert_tiang,
  //       (SELECT sum(qty*jasa_ta) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item left join maintaince m on mm.maintaince_id=m.id WHERE m.status = "close" and m.tgl_selesai like "%'.$id.'%" and m.dispatch_regu_id = mr.id and m.refer is null) as jasa_ta,
  //       (SELECT sum(qty*material_ta) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item left join maintaince m on mm.maintaince_id=m.id WHERE m.status = "close" and m.tgl_selesai like "%'.$id.'%" and m.dispatch_regu_id = mr.id and m.refer is null) as material_ta,
  //       (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item left join maintaince m on mm.maintaince_id=m.id WHERE m.status = "close" and m.tgl_selesai like "%'.$id.'%" and m.dispatch_regu_id = mr.id and m.refer is null) as jasa_telkom,
  //       (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item left join maintaince m on mm.maintaince_id=m.id WHERE m.status = "close" and m.tgl_selesai like "%'.$id.'%" and m.dispatch_regu_id = mr.id and m.refer is null) as material_telkom
  //       from maintenance_regu mr where '.$mitra.' mr.status_regu =1) asd where 1 order by close desc');
  //     return view('report.produktifitas', compact('data'));
  //   }
  //   public function detil_produktifitasbulanan($tgl, $jenis, $id){
  //     $query = '';
  //     if($jenis)
  //       $query = ' m.jenis_order='.$jenis.' and ';
  //     $data = DB::select('select m.*,mr.mitra, 
  //       (select nama from karyawan where id_karyawan = m.username1) as nama1, 
  //       (select nama from karyawan where id_karyawan = m.username2) as nama2,
  //       (select nama from karyawan where id_karyawan = m.username3) as nama3, 
  //       (select nama from karyawan where id_karyawan = m.username4) as nama4,
  //       (SELECT sum(qty*jasa_ta) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as jasa_ta,
  //       (SELECT sum(qty*material_ta) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as material_ta,
  //       (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as jasa_telkom,
  //       (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as material_telkom
  //       from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where m.status = "close" and '.$query.' m.tgl_selesai like "%'.$tgl.'%" and m.refer is null and m.dispatch_regu_id='.$id);
  //     return view('report.detilproduktifitas', compact('data'));
  //   }
  //   public function cetak_excel($tgl, $jenis_rekon, $verif){
  //     Excel::create($jenis_rekon.'-'.$tgl, function($excel) use($tgl, $jenis_rekon, $verif){
  //       $khs = DB::table('jenis_khs')->get();
  //       foreach($khs as $k){
  //         $excel->sheet($k->jenis_khs, function($sheet) use($tgl, $jenis_rekon, $verif, $k){
  //           $q = "and verify = ".$verif;
  //           if($verif == 'null')
  //             $q = "and verify is ".$verif;
  //           if($verif == 'all')
  //             $q = "";
  //           $q2 = " and mjo.rekon = '".$jenis_rekon."' ";
  //           if($jenis_rekon == "all")
  //             $q2 = "";
  //           $maintenance = DB::select("
  //             SELECT m.*,m2.*,i.satuan,i.jasa_telkom as jasa, i.material_telkom as material, i.uraian,
  //             (SELECT sum((qty*jasa)+(qty*material)) FROM maintaince_mtr mm 
  //             left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
  //             FROM maintaince m 
  //             right join maintaince_mtr m2 on m.id = m2.maintaince_id
  //             LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
  //             LEFT JOIN maintenance_jenis_order mjo on m.jenis_order = mjo.id
  //             WHERE i.jenis=".$k->id." and m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m2.id_item asc"
  //           );
  //           $data = array();
  //           $lastNama = '';
  //           $no=0;
  //           $head = array();
  //           $title = array();
  //           foreach ($maintenance as $no => $m){
  //             if(!empty($m->no_tiket)){
  //               $head[] = $m->no_tiket;
  //               $title[$m->no_tiket] = array('tgl'=>$m->tgl_selesai,'no_tiket'=>$m->no_tiket,'sto'=>'STO-'.$m->sto,'action'=>substr($m->tgl_selesai, 0,10).' '.$m->nama_odp.' '.$m->action);
  //             } 
  //           }
  //           $head = array_unique($head);
            
  //           //sort head per tgl
  //           $sorthead = array();
  //           $maintenance2 = DB::select("
  //             SELECT m.no_tiket, m.tgl_selesai
  //             FROM maintaince m 
  //             right join maintaince_mtr m2 on m.id = m2.maintaince_id
  //             LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
  //             LEFT JOIN maintenance_jenis_order mjo on m.jenis_order = mjo.id
  //             WHERE i.jenis=".$k->id." and m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m.tgl_selesai asc
  //           ");
  //           foreach ($maintenance2 as $no => $m){
  //             if(!empty($m->no_tiket)){
  //               $sorthead[] = $m->no_tiket;
  //             } 
  //           }
  //           $sorthead = array_unique($sorthead);
  //           $head = $sorthead;
  //           //endsort head
  //           $total = array();
  //           $totalmaterial = array();
  //           $totaljasa = array();
  //           foreach($head as $h){
  //             $total[$h] = 0;
  //             $totalmaterial[$h] = 0;
  //             $totaljasa[$h] = 0;
  //           }
  //           foreach ($maintenance as $no => $m){
  //             if($lastNama == $m->id_item){
  //               $data[count($data)-1]['no_tiket'][$m->no_tiket] = $m->qty;
                
  //               $totalmaterial[$m->no_tiket] += ($m->qty*$m->material);
  //               if($m->jenis_order == 4){
  //                 $totaljasa[$m->no_tiket] += 0;
  //                 $total[$m->no_tiket] += ($m->qty*$m->material);
  //               }
  //               else{
  //                 $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
  //                 $total[$m->no_tiket] += ($m->qty*$m->jasa)+($m->qty*$m->material);
  //               }
  //               $no++;
  //             }else{
  //               $data[] = array("id_item" => $m->id_item, "satuan" => $m->satuan, "uraian" => $m->uraian, "jasa" => $m->jasa, "material" => $m->material, "total_boq" => $m->total_boq, "no_tiket" => array(), "total" => array());
  //               foreach($head as $h){
  //                 if($h == $m->no_tiket){
  //                   $data[count($data)-1]['no_tiket'][$h] = $m->qty;
  //                   $totalmaterial[$h] += ($m->qty*$m->material);
  //                   if($m->jenis_order == 4){
  //                     $total[$h] += ($m->qty*$m->material);
  //                     $totaljasa[$h] += 0;
  //                   }else{
  //                     $total[$h] += ($m->qty*$m->jasa)+($m->qty*$m->material);
  //                     $totaljasa[$h] += ($m->qty*$m->jasa);
  //                   }
                    
  //                 }else{
  //                   $data[count($data)-1]['no_tiket'][$h] = 0;
  //                   $data[count($data)-1]['total'][$h] = 0;
  //                 }
  //               }
  //               $no=0;
  //             }
  //             $lastNama = $m->id_item;
  //           }
  //           //return view('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
  //           $sheet->getStyle('B')->getAlignment()->setWrapText(true);
  //           $sheet->loadView('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
  //         });
  //       }
  //     })->download('xlsx');
      
  //   }
  //   public function cetak_excel_2($tgl, $jenis_rekon, $verif){
  //     Excel::create($jenis_rekon.'-'.$tgl, function($excel) use($tgl, $jenis_rekon, $verif){
  //       $khs = DB::table('jenis_khs')->get();
  //       foreach($khs as $k){
  //         $excel->sheet($k->jenis_khs, function($sheet) use($tgl, $jenis_rekon, $verif, $k){
  //           $q = "and verify = ".$verif;
  //           if($verif == 'null')
  //             $q = "and verify is ".$verif;
  //           if($verif == 'all')
  //             $q = "";
  //           $q2 = " and m.jenis_order = '".$jenis_rekon."' ";
  //           if($jenis_rekon == "all")
  //             $q2 = "";
  //           $maintenance = DB::select("
  //             SELECT m.*,m2.*,i.satuan,i.jasa_telkom as jasa, i.material_telkom as material, i.uraian,
  //             (SELECT sum((qty*jasa)+(qty*material)) FROM maintaince_mtr mm 
  //             left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
  //             FROM maintaince m 
  //             right join maintaince_mtr m2 on m.id = m2.maintaince_id
  //             LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
  //             WHERE i.jenis=".$k->id." and m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m2.id_item asc"
  //           );
  //           $data = array();
  //           $lastNama = '';
  //           $no=0;
  //           $head = array();
  //           $title = array();
  //           foreach ($maintenance as $no => $m){
  //             if(!empty($m->no_tiket)){
  //               $head[] = $m->no_tiket;
  //               $title[$m->no_tiket] = array('tgl'=>$m->tgl_selesai,'no_tiket'=>$m->no_tiket,'sto'=>'STO-'.$m->sto,'action'=>substr($m->tgl_selesai, 0,10).' '.$m->nama_odp.' '.$m->action);
  //             } 
  //           }
  //           $head = array_unique($head);
  //           //sort head per tgl
  //           $sorthead = array();
  //           $maintenance2 = DB::select("
  //             SELECT m.no_tiket, m.tgl_selesai
  //             FROM maintaince m 
  //             right join maintaince_mtr m2 on m.id = m2.maintaince_id
  //             LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
  //             WHERE i.jenis=".$k->id." and m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m.tgl_selesai asc
  //           ");
  //           foreach ($maintenance2 as $no => $m){
  //             if(!empty($m->no_tiket)){
  //               $sorthead[] = $m->no_tiket;
  //             } 
  //           }
  //           $sorthead = array_unique($sorthead);
  //           $head = $sorthead;
  //           //endsort head
  //           $total = array();
  //           $totalmaterial = array();
  //           $totaljasa = array();
  //           foreach($head as $h){
  //             $total[$h] = 0;
  //             $totalmaterial[$h] = 0;
  //             $totaljasa[$h] = 0;
  //           }
  //           foreach ($maintenance as $no => $m){
  //             if($lastNama == $m->id_item){
  //               $data[count($data)-1]['no_tiket'][$m->no_tiket] = $m->qty;
                
  //               $totalmaterial[$m->no_tiket] += ($m->qty*$m->material);
  //               if($m->jenis_order == 4){
  //                 $totaljasa[$m->no_tiket] += 0;
  //                 $total[$m->no_tiket] += ($m->qty*$m->material);
  //               }
  //               else{
  //                 $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
  //                 $total[$m->no_tiket] += ($m->qty*$m->jasa)+($m->qty*$m->material);
  //               }
  //               $no++;
  //             }else{
  //               $data[] = array("id_item" => $m->id_item, "satuan" => $m->satuan, "uraian" => $m->uraian, "jasa" => $m->jasa, "material" => $m->material, "total_boq" => $m->total_boq, "no_tiket" => array(), "total" => array());
  //               foreach($head as $h){
  //                 if($h == $m->no_tiket){
  //                   $data[count($data)-1]['no_tiket'][$h] = $m->qty;
  //                   $totalmaterial[$h] += ($m->qty*$m->material);
  //                   if($m->jenis_order == 4){
  //                     $total[$h] += ($m->qty*$m->material);
  //                     $totaljasa[$h] += 0;
  //                   }else{
  //                     $total[$h] += ($m->qty*$m->jasa)+($m->qty*$m->material);
  //                     $totaljasa[$h] += ($m->qty*$m->jasa);
  //                   }
                    
  //                 }else{
  //                   $data[count($data)-1]['no_tiket'][$h] = 0;
  //                   $data[count($data)-1]['total'][$h] = 0;
  //                 }
  //               }
  //               $no=0;
  //             }
  //             $lastNama = $m->id_item;
  //           }
  //           //dd($title);
  //           //return view('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
  //           $sheet->getStyle('B')->getAlignment()->setWrapText(true);

  //           $sheet->loadView('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
  //         });
  //       }
  //     })->download('xlsx');
      
  //   }
    public function downloadForm(){
      $data = DB::table('maintenance_jenis_order')->select('*', 'nama_order as text')->get();
      return view('marina.report.downloadForm', compact('data'));
    }
    public function downloadSubmit(Request $req){
      $jenis_order = $req->jenis_order;
      $tgl=$req->tgl;
      Excel::create('mt', function($excel) use($jenis_order, $tgl){
        $excel->sheet($jenis_order, function($sheet) use($jenis_order, $tgl){
          $data = DB::table('maintaince')->select('*', DB::raw('(select customer from psb_myir_wo where myir = no_tiket limit 0,1) as customerName,(select picPelanggan from psb_myir_wo where myir = no_tiket limit 0,1) as myirpic'))->where('jenis_order', $jenis_order)->where('created_at', 'like', $tgl.'%')->get();
          $sheet->loadView('marina.report.excelDownloadForm', compact('data'));
        });
      })->download('xlsx');
      return redirect()->back();
    }
    public function revmitrav2($mitra,$tgl){
      if($mitra=="MITRA"){
        $sql = "SELECT sum((qty*jasa_ta)+(qty*material_ta))";
      }else{
        $sql = "SELECT sum((qty*jasa_telkom)+(qty*material_telkom))";
      }
      // dd($sql);
      $data = DB::select('
        SELECT *,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order!=4) as total,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order=1) as benjar,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order=2) as gamas,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order=3) as odp_loss,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order=4) as remo,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order=5) as utilitas,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order=6) as insert_tiang,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order=9) as odp_full,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order=8) as odp_sehat,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order=10) as val_odp,
              (select count(*) FROM maintaince m
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and mr.mitra = mt.mitra and m.jenis_order=8) as tiket_odp_sehat
              FROM regu mt WHERE mitra is not null and mitra != "" GROUP BY mitra ORDER BY mitra
      ');
      // dd($data);
      return view('marina.report.revenuev2', compact('data'));
    }

    public function ajax_rev($mitra,$id,$tgl){
      if($mitra=="MITRA"){
        $sql = "SELECT sum((qty*jasa_ta)+(qty*material_ta))";
      }else{
        $sql = "SELECT sum((qty*jasa_telkom)+(qty*material_telkom))";
      }
      $data = DB::select('
        SELECT *,id_regu as id,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order!=4 and m.dispatch_regu_name=mt.uraian) as total,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order=1 and m.dispatch_regu_name=mt.uraian) as benjar,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order=2 and m.dispatch_regu_name=mt.uraian) as gamas,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order=3 and m.dispatch_regu_name=mt.uraian) as odp_loss,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order=4 and m.dispatch_regu_name=mt.uraian) as remo,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order=5 and m.dispatch_regu_name=mt.uraian) as utilitas,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order=6 and m.dispatch_regu_name=mt.uraian) as insert_tiang,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order=9 and m.dispatch_regu_name=mt.uraian) as odp_full,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order=8 and m.dispatch_regu_name=mt.uraian) as odp_sehat,
              ('.$sql.' FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order=10 and m.dispatch_regu_name=mt.uraian) as val_odp,
              (select count(*) FROM maintaince m
              left join regu mr on m.dispatch_regu_id = mr.id_regu 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order=8 and m.dispatch_regu_name=mt.uraian) as tiket_odp_sehat
              FROM regu mt
              WHERE mt.mitra= "'.$id.'" and mt.uraian!="" and mt.status_team = "ACTIVE"
      ');
      // dd($data);
      return view('marina.report.ajaxrev', compact('data', 'tgl'));
    }
    public function ajaxdetil_rev($regu,$order,$tgl){
      $data = DB::select('select *, (qty*jasa_ta) as hargajasa,(qty*material_ta) as hargamaterial,(select count(*) from maintaince_mtr where maintaince_id=m.id) as countmtr FROM maintaince_mtr mm 
              left join khs_maintenance i on mm.id_item=i.id_item 
              left join maintaince m on mm.maintaince_id = m.id 
              WHERE m.tgl_selesai like "'.$tgl.'%" and m.jenis_order="'.$order.'" and m.dispatch_regu_id="'.$regu.'" order by m.id');
      return view('marina.report.ajaxdetilrev', compact('data'));
    }
  //   public function qc($id,$date){
  //     $foto = $this->photoOdpSehat;
  //     $data = DB::table('maintaince')->where('jenis_order', $id)->where('tgl_selesai', 'like', $date.'%')->get();
  //     return view('report.qc', compact('data', 'foto'));
  //   }
}
