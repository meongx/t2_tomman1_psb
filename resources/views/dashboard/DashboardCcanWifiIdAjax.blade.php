<div id="undispatch">
  <table class="table table-striped table-bordered dataTable">
      <tr>
          <th>Area</th>
          <th>Undispatch</th>
      </tr>

      <?php $jumlah = 0; $jmlKendala = 0;?>
      @foreach($dataundispatch as $potensi)
        <tr>
            <td>{{ $potensi->area }}</td>
            <td><a href="/dashboard/undispatch-ccan/{{ date('Y-m-d') }}/{{ $potensi->area }}/{{ $jenisPilihan }}">{{ $potensi->undispatch ?: '0'}}</a></td>
            <?php 
                $jumlah += $potensi->undispatch;
            ?>
        </tr>
      @endforeach
      <tr>
          <td>Total</td>
          <td><a href="/dashboard/undispatch-ccan/{{ date('Y-m-d') }}/ALL/{{ $jenisPilihan }}">{{ $jumlah }}</a></td>
      </tr>
  </table>
</div>