@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .panel-info .panel-heading {
      background-color: #54a0ff;
    }
    .customer_info {
      border-color: #FFF;
    }
    .customer_info td {
      padding : 5px;
    }
    .customer_field {
      align: right !important;
    }
    input,textarea {
      background-color: #ecf0f1;
      padding : 5px;
    }
    table td {
      padding : 5px;
    }
  </style>
  <h3>Ticketing</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Input
        </div>
        <div class="panel-body">
          <form method="post">
            <input type="hidden" name="id" value="{{ $id }}">
            <table>
              <tr>
                <td>ND</td>
                <td>:</td>
                <td>
                  {{ $customer_info->ndemPots ? : "~" }}
                  <input type="hidden" name="ndemPots" value="{{ $customer_info->ndemPots }}">
                </td>
              </tr>

              <tr>
                <td>ND INTERNET</td>
                <td>:</td>
                <td>
                  @if ($customer_info->ndemSpeedy<>'')
                    {{ $customer_info->ndemSpeedy }}
                    <input type="hidden" name="ndemSpeedy" value="{{ $customer_info->ndemSpeedy }}">
                    <input type="hidden" name="internet" value="{{ $customer_info->internet }}">
                  @else
                    <input type="text" name="ndemSpeedy" id="ndemSpeedy">
                    {!! $errors->first('ndemSpeedy','<p><span class="label label-danger">:message</span></p>') !!}
                  @endif
                </td>
              </tr>
              <tr>
                <td>Segment Status</td>
                <td>:</td>
                <td>
                  <select name="segment_status" required>
                    <option value=""></option>
                    <option value="DBS"> DBS </option>
                    <option value="DCS"> DCS </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>Customer Name</td>
                <td>:</td>
                <td>
                  <input type="text" name="orderName" id="orderName" value="{{ $customer_info->orderName }}">
                  {!! $errors->first('orderName','<p><span class="label label-danger">:message</span></p>') !!}
                  </td>
              </tr>
              <tr>
                <td>ODP</td>
                <td>:</td>
                <td>
                  <input type="text" name="alproname" value="{{ $customer_info->alproname }}">
                {!! $errors->first('alproname','<p><span class="label label-danger">:message</span></p>') !!}
                  </td>
              </tr>
              <tr>
                <td>Workzone</td>
                <td>:</td>
                <td>
                  <input type="text" name="sto" value="{{ $customer_info->sto }}">
                 {!! $errors->first('sto','<p><span class="label label-danger">:message</span></p>') !!}
               </td>
              </tr>

              <tr>
                <td>Pelapor</td>
                <td>:</td>
                <td>
                  <input type="text" name="pelapor" value="">
                 {!! $errors->first('pelapor','<p><span class="label label-danger">:message</span></p>') !!}
                </td>
              </tr>
              <tr>
                <td>Nomor yang bisa dihubungi</td>
                <td>:</td>
                <td>
                  <input type="text" name="kontak" value="">
                  {!! $errors->first('kontak','<p><span class="label label-danger">:message</span></p>') !!}
               </td>
              </tr>
              <tr>
                <td>Kategori Gangguan</td>
                <td>:</td>
                <td>
                  <select name="kat_gangguan">
                    <option> -- </option>
                    <option value="FISIK"> FISIK </option>
                    <option value="LOGIK"> LOGIK </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>Kategori Layanan</td>
                <td>:</td>
                <td>
                  <select name="kat_layanan">
                    <option> -- </option>
                    <option value="INTERNET">INTERNET</option>
                    <option value="TELP">TELP</option>
                    <option value="USEETV">USEETV</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>Jam Manja</td>
                <td>:</td>
                <td>
                  <input type="tgl" name="tglManja" id="input-tgl">
                  <input name="jamManja" type="jamManja" id="jamManja" value="" />
   
                </td>
              </tr>
              <tr>
                <td>Kategori Keluhan (Prior)</td>
                <td>:</td>
                <td>
                  <select name="keluhan">
                    <option value=""> -- </option>
                    @foreach ($get_keluhan as $keluhan)
                    <option value="{{ $keluhan->keluhan }}">{{ $keluhan->keluhan }}</option>
                    @endforeach
                  </select>
                </td>
              </tr>
              <tr>
                <td valign=top>Deskripsi Keluhan</td>
                <td valign=top>:</td>
                <td>    
                  <textarea name="catatan" rows="8" cols="80"></textarea>
                </td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td>
                  <input type="submit" name="" value="Submit">
                </td>
              </tr>
            </table>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <script>
    $(function() {
 
    var day = {
        format: 'yyyy-mm-dd',
        viewMode: 0,
        minViewMode: 0
      };
      $('#input-tgl').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });
      $('#tglManja').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });

      $('#jamManja').timepicker({
        showMeridian : false,
        disableMousewheel : true,
        minuteStep : 1
      });
    })
    </script>
@endsection
