@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    td {
      padding : 5px;
      border:1px solid #ecf0f1;
    }
    .pdnol {
      padding : 0px;
    }
    .center {
      text-align: center;
    }
    th {
      text-align: center;
      vertical-align: middle;
      background-color: #0073b7;
      padding : 5px;
      color : #FFF;
      border:1px solid #ecf0f1;
    }
    td {
      font-size: 14px;
      padding : 3px !important;

    }
    .center a {
      color : #666666 !important;
    }
    .green {
      background-color: #2ecc71;
      font-weight: bold;
      color : #FFF;
    }
    .green a:link{
      color : #FFF;
    }
    .red {
      background-color: #e67e22;
      font-weight: bold;
      color : #FFF;
    }
    .redx {
      background-color: #e74c3c;
      font-weight: bold;
      color : #FFF;
    }
    .red a:link{
      color : #FFF;
    }
    .gray {
      background-color : #f4f4f4;
    }
  </style>
  <script src="/bower_components/devexpress-web-14.1/js/dx.chartjs.js"></script>
  <link rel="stylesheet" href="/bower_components/devexpress-web-14.1/css/dx.dark.css" />
  <center>
  <h3>Dashboard Assurance TA KALSEL</h3>
  <br />
  </center>
  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default" id="reportasrbyumur">
        <div class="panel-heading">
          Tiket Open by IN
        </div>
        <div class="panel-body pdnol">
          <table class="table table-responsive">
            <tr>
              <th rowspan="2">SEKTOR</th>
              <th colspan="7">DURASI (Jam)</th>
              <th colspan="2">SUM</th>
              <th rowspan="2">TTL</th>
            </tr>
            <tr>
              <th class="green">0-2</th>
              <th class="green">2-4</th>
              <th class="green">4-6</th>
              <th class="red">6-8</th>
              <th class="red">8-10</th>
              <th class="red">10-12</th>
              <th class="redx">12-24</th>
              <th class="redx">>24</th>
              <th><24</th>
            </tr>
            <?php
              $lebih24 = 0;
              $kurang24 = 0;
              $a0dan2 = 0;
              $a2dan4 = 0;
              $a4dan6 = 0;
              $a6dan8 = 0;
              $a8dan10 = 0;
              $a10dan12 = 0;
              $a12dan24 = 0;
              $jumlah = 0;
            ?>
            @foreach ($getRockbyUmur as $RockbyUmur)
            <?php
              $lebih24 += $RockbyUmur->lebih24;
              $a0dan2 += $RockbyUmur->a0dan2;
              $a2dan4 += $RockbyUmur->a2dan4;
              $a4dan6 += $RockbyUmur->a4dan6;
              $a6dan8 += $RockbyUmur->a6dan8;
              $a8dan10 += $RockbyUmur->a8dan10;
              $a10dan12 += $RockbyUmur->a10dan12;
              $a12dan24 += $RockbyUmur->a12dan24;
              $kurang24 += $RockbyUmur->kurang24;
              $jumlah += $RockbyUmur->jumlah;
            ?>
            <tr>
              <td>{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}</td>
              <td class="center"><a href="/listbyumur/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a0dan2/ALL">{{ $RockbyUmur->a0dan2 }}</a></td>
              <td class="center"><a href="/listbyumur/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a2dan4/ALL">{{ $RockbyUmur->a2dan4 }}</a></td>
              <td class="center"><a href="/listbyumur/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a4dan6/ALL">{{ $RockbyUmur->a4dan6 }}</a></td>
              <td class="center"><a href="/listbyumur/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a6dan8/ALL">{{ $RockbyUmur->a6dan8 }}</a></td>
              <td class="center"><a href="/listbyumur/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a8dan10/ALL">{{ $RockbyUmur->a8dan10 }}</a></td>
              <td class="center"><a href="/listbyumur/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a10dan12/ALL">{{ $RockbyUmur->a10dan12 }}</a></td>
              <td class="center"><a href="/listbyumur/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a12dan24/ALL">{{ $RockbyUmur->a12dan24 }}</a></td>
              <td class="center"><a href="/listbyumur/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/lebih24/ALL">{{ $RockbyUmur->lebih24 }}</a></td>
              <td class="center"><a href="/listbyumur/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/kurang24/ALL">{{ $RockbyUmur->kurang24 }}</a></td>
              <td class="center"><a href="/listbyumur/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/ALL/ALL">{{ $RockbyUmur->jumlah }}</a></td>
            </tr>
            @endforeach
            <tr>
              <td><b>TOTAL</b></td>
              <td class="center"><a href="/listbyumur/ALL/a0dan2/ALL">{{ $a0dan2 }}</td>
              <td class="center"><a href="/listbyumur/ALL/a2dan4/ALL">{{ $a2dan4 }}</td>
              <td class="center"><a href="/listbyumur/ALL/a4dan6/ALL">{{ $a4dan6 }}</td>
              <td class="center"><a href="/listbyumur/ALL/a6dan8/ALL">{{ $a6dan8 }}</td>
              <td class="center"><a href="/listbyumur/ALL/a8dan10/ALL">{{ $a8dan10 }}</td>
              <td class="center"><a href="/listbyumur/ALL/a10dan12/ALL">{{ $a10dan12 }}</td>
              <td class="center"><a href="/listbyumur/ALL/a12dan24/ALL">{{ $a12dan24 }}</td>
              <td class="center"><a href="/listbyumur/ALL/lebih24/ALL">{{ $lebih24 }}</td>
              <td class="center"><a href="/listbyumur/ALL/kurang24/ALL">{{ $kurang24 }}</td>
              <td class="center"><a href="/listbyumur/ALL/ALL/ALL">{{ $jumlah }}</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="panel panel-default" id="reportasrbyumur">
        <div class="panel-heading">
          Tiket Open by INT
        </div>
        <div class="panel-body pdnol">
          <table class="table table-responsive">
            <tr>
              <th rowspan="2">SEKTOR</th>
              <th colspan="7">DURASI (Jam)</th>
              <th colspan="2">SUM</th>
              <th rowspan="2">JML</th>
            </tr>
            <tr>
              <th class="green">0-2</th>
              <th class="green">2-4</th>
              <th class="green">4-6</th>
              <th class="red">6-8</th>
              <th class="red">8-10</th>
              <th class="red">10-12</th>
              <th class="redx">12-24</th>
              <th class="redx">>24</th>
              <th><24</th>
            </tr>
            <?php
              $lebih24 = 0;
              $kurang24 = 0;
              $a0dan2 = 0;
              $a2dan4 = 0;
              $a4dan6 = 0;
              $a6dan8 = 0;
              $a8dan10 = 0;
              $a10dan12 = 0;
              $a12dan24 = 0;
              $jumlah = 0;
            ?>
            @foreach ($getTiket_OpenINT as $RockbyUmur)
            <?php
              $lebih24 += $RockbyUmur->lebih24;
              $a0dan2 += $RockbyUmur->x0dan2;
              $a2dan4 += $RockbyUmur->x2dan4;
              $a4dan6 += $RockbyUmur->x4dan6;
              $a6dan8 += $RockbyUmur->x6dan8;
              $a8dan10 += $RockbyUmur->x8dan10;
              $a10dan12 += $RockbyUmur->x10dan12;
              $a12dan24 += $RockbyUmur->x12dan24;
              $kurang24 += $RockbyUmur->kurang24;
              $jumlah += $RockbyUmur->jumlah;
            ?>
            <tr>
              <td>{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}</td>
              <td class="center"><a href="/ticketing/dashboardList/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x0dan2">{{ $RockbyUmur->x0dan2 }}</a></td>
              <td class="center"><a href="/ticketing/dashboardList/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x2dan4">{{ $RockbyUmur->x2dan4 }}</a></td>
              <td class="center"><a href="/ticketing/dashboardList/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x4dan6">{{ $RockbyUmur->x4dan6 }}</a></td>
              <td class="center"><a href="/ticketing/dashboardList/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x6dan8">{{ $RockbyUmur->x6dan8 }}</a></td>
              <td class="center"><a href="/ticketing/dashboardList/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x8dan10">{{ $RockbyUmur->x8dan10 }}</a></td>
              <td class="center"><a href="/ticketing/dashboardList/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x10dan12">{{ $RockbyUmur->x10dan12 }}</a></td>
              <td class="center"><a href="/ticketing/dashboardList/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x12dan24">{{ $RockbyUmur->x12dan24 }}</a></td>
              <td class="center"><a href="/ticketing/dashboardList/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/lebih24">{{ $RockbyUmur->lebih24 }}</a></td>
              <td class="center"><a href="/ticketing/dashboardList/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/kurang24">{{ $RockbyUmur->kurang24 }}</a></td>
              <td class="center"><a href="/ticketing/dashboardList/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/ALL">{{ $RockbyUmur->jumlah }}</a></td>
            </tr>
            @endforeach
            <tr>
              <td><b>TOTAL</b></td>
              <td class="center"><a href="/ticketing/dashboardList/ALL/x0dan2">{{ $a0dan2 }}</td>
              <td class="center"><a href="/ticketing/dashboardList/ALL/x2dan4">{{ $a2dan4 }}</td>
              <td class="center"><a href="/ticketing/dashboardList/ALL/x4dan6">{{ $a4dan6 }}</td>
              <td class="center"><a href="/ticketing/dashboardList/ALL/x6dan8">{{ $a6dan8 }}</td>
              <td class="center"><a href="/ticketing/dashboardList/ALL/x8dan10">{{ $a8dan10 }}</td>
              <td class="center"><a href="/ticketing/dashboardList/ALL/x10dan12">{{ $a10dan12 }}</td>
              <td class="center"><a href="/ticketing/dashboardList/ALL/x10dan12">{{ $a12dan24 }}</td>
              <td class="center"><a href="/ticketing/dashboardList/ALL/lebih12">{{ $lebih24 }}</td>
              <td class="center"><a href="/ticketing/dashboardList/ALL/kurang12">{{ $kurang24 }}</td>
              <td class="center"><a href="/ticketing/dashboardList/ALL/ALL">{{ $jumlah }}</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="panel panel-default" id="reportpotensiasr">
        <div class="panel-heading">
          TIKET Progress IN
        </div>
        <div class="panel-body pdnol">
          <table class="table table-responsive" >
            <tr>
              <th>SEKTOR</th>
              <th>OPEN</th>
              <th>NP</th>
              <th>OGP</th>
              <th>KP</th>
              <th>KT</th>
              <th>CLOSING</th>
              <th class="red">SISA SALDO</th>
            </tr>
            <?php
              $TOTAL = 0;
              $NP = 0;
              $OGP = 0;
              $KT = 0;
              $KP = 0;
              $UP = 0;
            ?>
            @foreach ($getTiket2 as $number => $Tiket2)
            <?php
              $TOTAL += $Tiket2->jumlah;
              $NP += $Tiket2->NP;
              $OGP += $Tiket2->OGP;
              $KP += $Tiket2->KP;
              $KT += $Tiket2->KT;
              $UP += $Tiket2->UP;
            ?>
            <tr>
              <td>{{ $Tiket2->title ? : 'UNDISPATCH' }}</td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/open">{{ $Tiket2->jumlah }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/np">{{ $Tiket2->NP }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/ogp">{{ $Tiket2->OGP }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/kp">{{ $Tiket2->KP }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/kt">{{ $Tiket2->KT }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/up">{{ $Tiket2->UP }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/sisa">{{ $Tiket2->jumlah-$Tiket2->UP }}</a></td>
            </tr>
            <?php ++$number; ?>
            @endforeach
            <tr>
              <td class="center"><b>TOTAL</b></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/open"><b>{{ $TOTAL }}</b></a></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/np"><b>{{ $NP }}</b></a></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/ogp"><b>{{ $OGP }}</b></a></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/kp"><b>{{ $KP }}</b></a></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/kt"><b>{{ $KT }}</b></a></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/up"><b>{{ $UP }}</b></a></td>
              <td class="center red"><a href="/assurance/list-active-rock/ALL/sisa"><b>{{ $TOTAL-$UP }}</b></a></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          TIKET Progress INT
        </div>
        <div class="panel-body pdnol">
          <table class="table table-responsive" >
            <tr>
              <th>SEKTOR</th>
              <th>OPEN</th>
              <th>NP</th>
              <th>OGP</th>
              <th>KP</th>
              <th>KT</th>
              <th>CLOSING</th>
              <th class="red">SISA SALDO</th>
            </tr>
            <?php
              $TOTAL = 0;
              $NP = 0;
              $OGP = 0;
              $KT = 0;
              $KP = 0;
              $UP = 0;
            ?>
            @foreach ($getTiket_INT as $number => $Tiket2)
            <?php
              $TOTAL += $Tiket2->jumlah;
              $NP += $Tiket2->NP;
              $OGP += $Tiket2->OGP;
              $KP += $Tiket2->KP;
              $KT += $Tiket2->KT;
              $UP += $Tiket2->UP;
            ?>
            <tr>
              <td>{{ $Tiket2->title ? : 'UNDISPATCH' }}</td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/open">{{ $Tiket2->jumlah }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/np">{{ $Tiket2->NP }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/ogp">{{ $Tiket2->OGP }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/kp">{{ $Tiket2->KP }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/kt">{{ $Tiket2->KT }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/up">{{ $Tiket2->UP }}</a></td>
              <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/sisa">{{ $Tiket2->jumlah-$Tiket2->UP }}</a></td>
            </tr>
            <?php ++$number; ?>
            @endforeach
            <tr>
              <td class="center"><b>TOTAL</b></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/open"><b>{{ $TOTAL }}</b></a></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/np"><b>{{ $NP }}</b></a></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/ogp"><b>{{ $OGP }}</b></a></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/kp"><b>{{ $KP }}</b></a></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/kt"><b>{{ $KT }}</b></a></td>
              <td class="center"><a href="/assurance/list-active-rock/ALL/up"><b>{{ $UP }}</b></a></td>
              <td class="center red"><a href="/assurance/list-active-rock/ALL/sisa"><b>{{ $TOTAL-$UP }}</b></a></td>
            </tr>
          </table>

        </div>
      </div>
    </div>
</div>

  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Action Today Table
        </div>
        <div class="panel-body">
          <table width="100%">
            <tr>
              <th rowspan="2">Action</th>
              <!-- <th rowspan="2">Jumlah</th>
              <th rowspan="2">INT</th> -->
              <th rowspan="2">IN</th>
              <th colspan="{{ count($get_sektor) }}">IN</th>
              <th rowspan="2">INT</th>
              <th colspan="{{ count($get_sektor) }}">INT</th>

            </tr>
            <tr>
              @foreach ($get_sektor as $sektor)
              <th>{{ $sektor->sektor }}</th>
              @endforeach

              @foreach ($get_sektor as $sektor)
              <th>{{ $sektor->sektor }}</th>
              @endforeach
            </tr>
            <?php
              $total = 0;
              $totalIN = 0;
              $totalINT = 0;
            ?>
            @foreach ($getAction as $Action)
            <tr>
              <td>{{ $Action->action }}</td>
              <!-- <td><a href="/listbyaction/{{ $date }}/{{ $Action->action }}">{{ $Action->jumlah }}</a></td> -->
              <td><a href="/listbyaction/{{ $date }}/{{ $Action->action }}/NOSSA/ALL">{{ $Action->jumlah_IN }}</a></td>
              @foreach ($get_sektor as $sektor)
              <?php
                $sektorx=$sektor->sektor;
              ?>
              <td><a href="/listbyaction/{{ $date }}/{{ $Action->action }}/NOSSA/{{ $sektorx }}">{{ $Action->$sektorx }}</a></td>
              @endforeach
              <td><a href="/listbyaction/{{ $date }}/{{ $Action->action }}">{{ $Action->jumlah_INT }}</a></td>
              @foreach ($get_sektor as $sektor)
              <?php
                $sektorx="INT_".$sektor->sektor;
              ?>
              <td><a href="/listbyaction/{{ $date }}/{{ $Action->action }}/TOMMAN/{{ $sektorx }}">{{ $Action->$sektorx }}</a></td>
              @endforeach
            </tr>
            <?php
              $total = $total + $Action->jumlah;
              $totalINT = $totalINT + $Action->jumlah_INT;
              $totalIN = $totalIN + $Action->jumlah_IN;
            ?>
            @endforeach
            <tr>
              <td>Total</td>
              <td><a href="/listbyaction/{{ $date }}/ALL/NOSSA/ALL">{{ $totalIN }}</a></td>
              @foreach ($get_sektor as $sektor)
              <?php $sektorx = $sektor->sektor ?>
              <td><a href="/listbyaction/{{ $date }}/ALL/NOSSA/{{ $sektor->sektor }}">{{ $sektor->jumlah_IN }}</a></td>
              @endforeach
              <td><a href="/listbyaction/{{ $date }}/ALL/TOMMAN/ALL">{{ $totalINT }}</a></td>
              @foreach ($get_sektor as $sektor)
              <?php $sektorx = $sektor->sektor ?>
              <td><a href="/listbyaction/{{ $date }}/ALL/TOMMAN/{{ $sektor->sektor }}">{{ $sektor->jumlah_INT }}</a></td>
              @endforeach
            </tr>
          </table>
        </div>
      </div>
    </div>

    <div class="col-sm-12">
      <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <script type="text/javascript">
            jQuery(document).ready(function($)
            {
              $("#bar-3").dxChart({
                dataSource: [
                  @foreach ($getAction as $Action)
                  {reporting : "{{ $Action->action }}", val: {{ $Action->jumlah }}},
                  @endforeach
                ],
                AutoLayout : false,
                title: "Action Assurance {{ $date }} ",
                tooltip: {
                    enabled: true,
                  customizeText: function() {
                    return this.argumentText + "<br/>" + this.valueText;
                  }
                },
                size: {
                  height: 450
                },
                legend: {
                  visible: true
                },

                rotated : true,
                series: [{
                  label: {
                      visible: true,
                      customizeText: function (segment) {
                          return segment.percentText;
                      },
                      connector: {
                          visible: true
                      }
                  },
                  name : "Action",
                  type: "bar",
                  argumentField: "reporting"
                }]
              });
              });
              </script>

                <div id="bar-3" style="height: 290px; width: 100%;"></div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Action Today Table
          </div>
          <div class="panel-body">
            <table>
              <tr>
                <th rowspan="2">Action</th>
                <!-- <th rowspan="2">Jumlah</th>
                <th rowspan="2">INT</th> -->
                <th rowspan="2">IN</th>
                <th colspan="{{ count($get_sektor) }}">IN</th>
              </tr>
              <tr>
                @foreach ($get_sektor as $sektor)
                <th>{{ $sektor->sektor }}</th>
                @endforeach
              </tr>
              <?php
                $total = 0;
                $totalIN = 0;
              ?>
              @foreach ($getAction as $Action)
              <tr>
                <td>{{ $Action->action }}</td>
                <!-- <td><a href="/listbyaction/{{ $date }}/{{ $Action->action }}">{{ $Action->jumlah }}</a></td>
                <td><a href="/listbyaction/{{ $date }}/{{ $Action->action }}">{{ $Action->jumlah_INT }}</a></td> -->
                <td><a href="/listbyaction/{{ $date }}/{{ $Action->action }}/NOSSA/ALL">{{ $Action->jumlah_IN }}</a></td>
                @foreach ($get_sektor as $sektor)
                <?php
                  $sektorx=$sektor->sektor;
                ?>
                <td><a href="/listbyaction/{{ $date }}/{{ $Action->action }}/NOSSA/{{ $sektorx }}">{{ $Action->$sektorx }}</a></td>
                @endforeach
              </tr>
              <?php
                $total = $total + $Action->jumlah;
                $totalIN = $totalIN + $Action->jumlah_IN;
              ?>
              @endforeach
              <tr>
                <td>Total</td>
                <td><a href="/listbyaction/{{ $date }}/ALL/NOSSA/ALL">{{ $totalIN }}</a></td>
                @foreach ($get_sektor as $sektor)
                <?php $sektorx = $sektor->sektor ?>
                <td><a href="/listbyaction/{{ $date }}/ALL/NOSSA/{{ $sektor->sektor }}">{{ $sektor->jumlah_IN }}</a></td>
                @endforeach
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            Status Table
          </div>
          <div class="panel-body">
            <table>
              <tr>
                <th>Status</th>
                <th>Jumlah</th>
              </tr>
              <?php
                $jumlah_status = 0;
              ?>
              @foreach ($getStatus as $Status)
              <tr>
                <td>{{ $Status->laporan_status ? : 'NO UPDATE' }}</td>
                <td><a href="/dashboard/assurance/list/{{ $date }}/{{ $Status->laporan_status ? : 'NO UPDATE' }}">{{ $Status->jumlah }}</a></td>
              </tr>
              <?php
                $jumlah_status = $jumlah_status + $Status->jumlah;
              ?>
              @endforeach
              <tr>
                <td>Total</td>
                <td><a href="/dashboard/assurance/list/{{ $date }}/ALL">{{ $jumlah_status }}</a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-9">
    <script type="text/javascript">
      jQuery(document).ready(function($)
      {
        $("#bar-4").dxPieChart({
          dataSource: [
            @foreach ($getStatus as $Status)
            {reporting : "{{ $Status->laporan_status ? : 'NO UPDATE' }}", val: {{ $Status->jumlah }}},
            @endforeach
          ],
          AutoLayout : false,
          title: "Reporting Status Today ",
          tooltip: {
              enabled: true,
            customizeText: function() {
              return this.argumentText + "<br/>" + this.valueText;
            }
          },
          size: {
            height: 370
          },
          legend: {
            visible: true
          },
          series: [{
            label: {
                visible: true,
                connector: {
                    visible: true
                }
            },
            type: "doughnut",
            argumentField: "reporting"
          }]
        });
        });
        </script>
        <div id="bar-4" style="height: 290px; width: 100%;"></div>
      </div>
    </div>


  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
      <div class="panel-body">
        <script type="text/javascript">
          jQuery(document).ready(function($)
          {
            $("#bar-11").dxChart({
              title: 'Trend Open Gangguan Witel Kalsel',
              label : {
                visible : true
              },
              argumentAxis: {
                  valueMarginsEnabled: false,
                  discreteAxisDivisionMode: "crossLabels",
                  grid: {
                      visible: true
                  }
              },
              tooltip: {
                  enabled: true,
                  customizeTooltip: function (arg) {
                      return {
                          text: arg.valueText
                      };
                  }
              },
              dataSource: [
                @foreach ($getTiket as $Tiket)
                { day : "{{ $Tiket->day }}", Jumlah : {{ $Tiket->jumlah }}},
                @endforeach
                @foreach ($getTiket as $Tiket)
                { day : "{{ $Tiket->day }}", Jumlah_Close : {{ $Tiket->jumlah_open }}},
                @endforeach
              ],
              series: [
              {
                argumentField : "day",
                valueField: "Jumlah",
                name : "Jumlah_Tiket",
                type : "area",
                color: '#2ecc71'
              },
              {
                argumentField : "day",
                valueField: "Jumlah_Close",
                name : "Jumlah_Close_byTek",
                type : "area",
                color: '#ff0000'
              },
              ]
            });
          });
          </script>
          <div id="bar-11" style="height: 290px; width: 100%;"></div>
      </div>
    </div>
  </div>
  </div>



  <br />
  <br />
  <br />
  <br />
    </div>
@endsection
