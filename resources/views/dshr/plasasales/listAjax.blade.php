<table class="table table-bordered">
	<tr>	
		<th>No</th>
		<th>Tgl Order</th>
		<th>Myir / SC</th>
		<th>Pelanggaan</th>
		<th>Tanggal Lahir</th>
		<th>Alamat</th>
		<th>STO</th>
		<th>ODP</th>
		<th>Layanan</th>
		<th>PSB</th>
		<th>Paket Indihome</th>
		<th>Paket Sales</th>
		<th>Email</th>
		<th>Kcontack</th>	
		<th>Ket Dispatch</th>
		<th>Sales ID / Kode Plasa</th>				
		<th>Ket</th>

	</tr>

	@foreach($getData as $no=>$data)
		<tr>	
			<td>{{ ++$no }}</td>
			<td>{{ $data->orderDate }}</td>
			<td>{{ $data->myir }}</td>
			<td>{{ $data->customer }}</td>
			<td>{{ $data->tgl_lahir ?: ' - ' }}</td>		
			<td>{{ $data->alamatLengkap }}</td>
			<td>{{ $data->sto }}</td>
			<td>{{ $data->namaOdp }}</td>
			<td>{{ $data->layanan }}</td>
			<td>{{ $data->psb ?: '-'}}</td>
			<td>{{ $data->paket_harga ?: '-' }}</td>
			<td>{{ $data->paket_sales ?: '-' }}</td>
			<td>{{ $data->email ?: '-' }}</td>
			<td>{{ $data->kcontack ?: '-' }}</td>

			@if($data->ket_input==0)	
				<td><a href="/dshr/plasa-sales/dispatch/plasa/{{ $data->myir }}" class="btn btn-primary btn-sm">Dispatch</a></td>
			@elseif($data->ket_input==1)
				<td><a href="/dshr/plasa-sales/dispatch/plasa/{{ $data->myir }}" class="btn btn-primary btn-sm">Dispatch</a></td>
			@else
				@if($data->myir<>'')
					<td><a href="/dshr/plasa-sales/dispatch/sales-onecall/{{ $data->myir }}" class="btn btn-primary btn-sm">Dispatch</a></td>
				@else
					<td></td>
				@endif
			@endif

			<td>{{ $data->sales_id ?: $data->created_by}}</td>			

			@if($data->ket_input==0)
				<td>
					PLASA
					<a href="/dshr/plasa-sales/cetak-plasa/{{ $data->id_wo }}" target="_blank" class="btn btn-info btn-sm">
						<i class="glyphicon glyphicon-print" aria-hidden="true"></i>
					</a>
				</td>
			@elseif($data->ket_input==1)
				<td>SALES</td>
			@else
				<td>SALES ONECALL</td>
			@endif
		</tr>
	@endforeach
</table>