<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DateTime;
use DB;

class guaranteeController extends Controller
{
	protected $guaranteeInputs = [
    	'Foto_Action','ODP', 'Hasil_Ukur_OPM' ,'Berita_Acara', 'Rumah_Pelanggan', 'Test_Layanan_Internet', 'Test_Layanan_TV', 'Test_Layanan_Telepon', 'MODEM', 'SN_ONT_RUSAK'
  	];

  	private function insertMaterials($psbId, $materials)
	{
	    // foreach($materials as $material) {
		   //  DB::table('psb_laporan_material')->insert([
		   //     'id'				 => $psbId.'-'.$material->id_item,
		   //     'psb_laporan_id'	 => $psbId,
		   //     'id_item' 		 => $material->id_item,
		   //     'qty' 			 => $material->qty,
		   //     'type'      		=> 1
		   //  ]);
	    // }

	    foreach($materials as $material) {
	      $exis = DB::table('psb_laporan_material')
	                ->where('id',$psbId.'-'.$material->id_item.'_'.$material->rfc)
	                ->first();

	      if ($exis){
	        DB::table('psb_laporan_material')
	          ->where('id_item_bantu',$material->id_item.'_'.$material->rfc)
	          ->where('psb_laporan_id',$exis->psb_laporan_id)
	          ->update([
	              'qty' => $material->qty
	          ]);
	      }
	      else{
	        DB::table('psb_laporan_material')->insert([
	              'id' => $psbId.'-'.$material->id_item.'_'.$material->rfc,
	              'psb_laporan_id' => $psbId,
	              'id_item' => trim($material->id_item),
	              'id_item_bantu' => trim($material->id_item).'_'.$material->rfc,
	              'qty' => $material->qty,
	              'rfc' => $material->rfc,
	              'type' => 1
	          ]); 
	      }

	        // update saldo terpakai
	        $dataA = DB::table('psb_laporan_material')
	                  ->where('id_item_bantu',$material->id_item.'_'.$material->rfc)
	                  ->get();
	        $terpakai = 0;
	        foreach($dataA as $aaa){
	            $terpakai += $aaa->qty;
	        }

	        // update terpakai
	        DB::table('rfc_sal')->where('id_item_bantu',$dataA[0]->id_item_bantu)->update(['jmlTerpakai' => $terpakai]);
	    }
	}

	private function decrementStokTeknisi($id_karyawan, $materials)
	{
		foreach($materials as $material) {
			$exists = DB::select('
		        SELECT *
		        FROM stok_material_teknisi
		        WHERE id_karyawan = ? and id_item = ?
		      ',[
		        $id_karyawan, $material->id_item
		    ]);

		    if (count($exists)) {
		        DB::table('stok_material_teknisi')
		          ->where('id_item', $material->id_item)
		          ->where('id_karyawan', $id_karyawan)
		          ->decrement('stok', $material->qty);
		    }else {
		        $insertId=DB::table('stok_material_teknisi')->insertGetId([
		          'id_item' 	=> $material->id_item,
		          'id_karyawan' => $material->qty
		        ]);

		        DB::table('stok_material_teknisi')
		          ->where('id', $insertId)
		          ->decrement('stok', $material->qty);
		    }
	    }
	}

  	private function incrementStokTeknisi($id, $idkaryawan)
  	{
		$materials = DB::select('
	        SELECT *
	        FROM psb_laporan_material
	        WHERE psb_laporan_id = ? and type = 1
	      ',[
	        $id
	    ]);
   
	    foreach($materials as $material) {
	      DB::table('stok_material_teknisi')
	        ->where('id_item', $material->id_item)
	        ->where('id_karyawan', $idkaryawan)
	        ->increment('stok', $material->qty);
	    };

	    $ntes = DB::select('
	        SELECT *
	        FROM psb_laporan_material
	        WHERE psb_laporan_id = ? and type = 2
	      ',[
	        $id
	     ]);

	    foreach($ntes as $nte) {
	      DB::table('nte')
	        ->where('id', $nte->id_item)
	        ->update([
	            'position_type'  => 2,
	            'position_key'  => $idkaryawan,
	            'status' => 1
	        ]);
	    }
 	}

 	private function handleFileUpload($request, $id)
  	{
	  	foreach($this->guaranteeInputs as $name) {
	     	$input = 'photo-'.$name;
	      	if ($request->hasFile($input)) {
		        //dd($input);
		        $path = public_path().'/upload3/asurance/'.$id.'/';
		        if (!file_exists($path)) {
		          if (!mkdir($path, 0770, true))
		            return 'gagal menyiapkan folder foto evidence';
		        }
		        $file = $request->file($input);
		        // $ext = $file->guessExtension();
		        $ext = 'jpg';
		        //TODO: path, move, resize
		        try {
		          $moved = $file->move("$path", "$name.$ext");

		          $img = new \Imagick($moved->getRealPath());
		          $img->scaleImage(100, 150, true);
		          $img->writeImage("$path/$name-th.$ext");
		        }
		        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
		          return 'gagal menyimpan foto evidence '.$name;
		        }
	    	}
	    }
  	}

  	public function datediff($start,$end)
  	{
	    $start 		= new DateTime(($start));
	    $end 		= new DateTime(($end));
	    $interval 	= $start->diff($end);
	    return $interval->h + ($interval->days*24);
  	}

    public function form()
    {
    	$regu = DB::select('
	      SELECT a.id_regu as id,a.uraian as text, b.title
	      FROM regu a
	        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
	      WHERE
	        ACTIVE = 1
	    ');

   		return view('guarantee.form', compact('regu'));
    }

    public function saveForm(Request $request)
    {
    	$this->validate($request,[
	        'tglOpen' 	=> 'required',
	        'jamOpen' 	=> 'required',
	        'no_tiket'	=> 'required|numeric'
	    ]);

	    $now = $request->tglOpen.' '.$request->jamOpen.':00';
	    $openTanggal = date('Y-m-d H:i:s',strtotime($now));

	    $auth = session('auth');
	    $check = DB::table('psb_fullfillment_guarante')
	              ->where('sc',$request->input('no_tiket'))
	              ->get();

	    $sc = $request->no_tiket;
	    if (count($check)>0) {

	      DB::table('psb_fullfillment_guarante')->where('sc',$sc)->update([
	        'sc' 		=> $sc,
	        'loker' 	=> $request->input('loker'),
	        'alpro' 	=> $request->input('alpro'),
	        'tgl'		=> $request->tgl,
	        'tglOpen' 	=> $openTanggal,
	        'noTiket'	=> $request->no_tiket
	      ]);
	    } else {
	      DB::table('psb_fullfillment_guarante')->insert([
	        'sc' 		 => $sc,
	        'loker'		 => $request->input('loker'),
	        'alpro'		 => $request->input('alpro'),
	        'tgl'		 => $request->tgl,
	        'tglOpen' 	 => $openTanggal,
	        'noTiket'	=> $request->no_tiket
	      ]);
	    };

	    $exists = DB::select('select * from dispatch_teknisi where Ndem = ? and dispatch_by = 4',[
	      $request->input('no_tiket')
	    ]);

	    if(count($exists)){
	      $data = $exists[0];
	      // dd('ada');
	      // DB::table('dispatch_teknisi')
	      //     ->where('Ndem', $request->input('no_tiket'))
	      //     ->update([
	      //       'updated_at'   		   => DB::raw('NOW()'),
	      //       'update_byGuarante'    => $auth->id_karyawan,
	      //       'tglGuarante'          => $request->input('tgl'),
	      //       'reguGuarante'		   => $request->input('id_regu'),
	      //       'statusGuarante'	   => 1,
	      //       'dispatch_by'		   => 4
	      //     ]);
	          
	    }else{
	        DB::table('dispatch_teknisi')->insert([
	          'updated_at' => DB::raw('NOW()'),
	          'updated_by' => $auth->id_karyawan,
	          'tgl'        => $request->input('tgl'),
	          'id_regu'    => $request->input('id_regu'),
	          'Ndem'       => $sc,
	          'updated_by' => session('auth')->id_karyawan,
	          'dispatch_by'=> 4
	        ]);

	    }

	    return back()->with('alerts', [
	        ['type' => 'success', 'text' => '<strong>SUKSES</strong> men-dispatch SC Fulfillment Guarantee']
	    ]);
    }

    public function formLaporan ($id)
    {
	    $auth = session('auth');
	    $exists = DB::select('
	      SELECT *
	      FROM psb_laporan
	      WHERE id_tbl_mj = ?
	    ',[
	      $id
	    ]);

	    if (count($exists)) {
	      $data = $exists[0];

	      // TODO: order by most used
	      // $materials = DB::select('
	      //   SELECT
	      //     i.id_item, i.nama_item,
	      //     COALESCE(m.qty, 0) AS qty
	      //   FROM item i
	      //   LEFT JOIN
	      //     psb_laporan_material m
	      //     ON i.id_item = m.id_item
	      //     AND m.psb_laporan_id = ?
	      //   WHERE
	      //     i.grup = 1
	      //   ORDER BY id_item
	      // ', [
	      //   $data->id
	      // ]);

	      if ($auth->level==2){
	          $materials = DB::select('
	          SELECT
	            i.id_item, i.nama_item,
	            COALESCE(i.jmlTerpakai, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
	          FROM rfc_sal i
	          where
	            i.psb_laporan_id = ?
	          ORDER BY i.rfc
	        ', [
	          $data->id
	        ]);
	      }
	      else{
	        $materials = DB::select('
	          SELECT
	            i.id_item, i.nama_item,
	            COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
	          FROM rfc_sal i
	          LEFT JOIN
	            psb_laporan_material m
	            ON i.id_item_bantu = m.id_item_bantu
	            AND m.psb_laporan_id = ?
	          WHERE
	           i.nik="'.$auth->id_user.'"
	          ORDER BY id_item
	        ', [
	          $data->id
	        ]);
	      }

	    }
	    else {
	      $data = new \StdClass;
	      $data->id = null;
	      // TODO: order by most used

	      // $materials = DB::select('
	      //   SELECT a.id_item, a.nama_item, 0 AS qty
	      //   FROM item a,  psb_laporan_material b
	      //   WHERE a.id_item=b.id_item
	      //   GROUP BY a.id_item
	      //   ORDER BY a.id_item
	      // ');

	      $materials = DB::select('
	        SELECT id_item, nama_item, 0 AS qty, (jumlah - jmlTerpakai) as saldo, rfc
	        FROM rfc_sal where nik="'.$auth->id_user.'" AND jumlah<>0
	        ORDER BY rfc
	      ');
	    }
	    $nte1 = DB::select('select i.id as id, i.sn as text
	        FROM nte i
	        LEFT JOIN
	          psb_laporan_material m
	          ON i.id = m.id_item
	          AND m.psb_laporan_id = ?
	        WHERE m.type=2
	        ORDER BY id_item',[
	        $data->id
	      ]);
	    $no = 1;
	    $ntes = '';
	    foreach($nte1 as $n){
	      if($no == 1){
	        $ntes .= $n->id;
	        $no = 2;
	      }else{
	        $ntes .= ', '.$n->id;
	      }
	    }
	    $nte = DB::select('select  id as id, sn as text from nte
	      where position_key = ?',[
	        $auth->id_karyawan
	      ]);
	    // $project = DB::select('
	    //   SELECT
	    //     *,
	    //     d.id as id_dt,
	    //     roc.*,
	    //     ne.STO as neSTO,
	    //     ne.RK as neRK,
	    //     ne.ND_TELP as neND_TELP,
	    //     ne.ND_INT as neND_INT,
	    //     ne.PRODUK_GGN as nePRODUK_GGN,
	    //     ne.HEADLINE as neHEADLINE,
	    //     ne.LOKER_DISPATCH as neLOKER_DISPATCH,
	    //     ne.KANDATEL as neKANDATEL
	    //   FROM dispatch_teknisi d
	    //   LEFT JOIN roc on d.Ndem = roc.no_tiset
	    //   LEFT JOIN nonatero_excel_bank ne on d.Ndem = ne.TROUBLE_NO
	    //   WHERE
	    //   d.id = ?
	    // ',[
	    //   $id
	    // ])[0];

	     $project = DB::select('
	      SELECT
	        *,
	        d.id as id_dt,
	        dps.*,
	        roc.*,
	        pl.*
	      FROM dispatch_teknisi d
	      LEFT JOIN roc on d.Ndem = roc.no_tiket
	      LEFT JOIN Data_Pelanggan_Starclick dps on dps.orderId = roc.no_tiket
	      LEFT JOIN psb_laporan pl on d.id = pl.id_tbl_mj
	      WHERE
	      d.id = ?
	    ',[
	      $id
	    ])[0];

	    // dd($data); 
	    $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","ASR")');
	    $get_laporan_action = DB::SELECT('SELECT laporan_action_id as id, action as text FROM psb_laporan_action');
	    $get_laporan_penyebab = DB::select('SELECT idPenyebab as id, penyebab as text from psb_laporan_penyebab'); 
	    $photoInputs = $this->guaranteeInputs;

	    return view('guarantee.laporanTek', compact('data', 'project', 'materials', 'photoInputs', 'ntes', 'nte', 'nte1','get_laporan_status','get_laporan_action', 'get_laporan_penyebab'));
  	}

  	public function saveLaporan(Request $request, $id)
	{
	 	$auth = session('auth');
	    $disp = DB::table('dispatch_teknisi')
	      ->select('dispatch_teknisi.*', 'Data_Pelanggan_Starclick.jenisPsb')
	      ->leftJoin('Data_Pelanggan_Starclick', 'dispatch_teknisi.Ndem', '=', 'Data_Pelanggan_Starclick.orderId')
	      ->where('id', $id)
	      ->first();
	    
	    $materials = json_decode($request->input('materials'));
	    $ntes = explode(',', $request->input('nte'));
	    $exists = DB::select('
	      SELECT *
	      FROM psb_laporan
	      WHERE id_tbl_mj = ?
	    ',[
	      $id
	    ]);

	    $input = $request->only([
	      'status'
	    ]);
	    $rules = array(
	      'status' => 'required'
	    );
	    $messages = [
	      'status.required' => 'Silahkan isi kolom "Status" Bang!',
	      'penyebab.required' => 'Silahkan isi kolom "Penyebab" Bang!',
	      'action.required' => 'Silahkan isi kolom "action" Bang!',
	      'kordinat_pelanggan.required' => 'Silahkan isi kolom "kordinat_pelanggan" Bang!',
	      'nama_odp.required' => 'Silahkan isi kolom "nama_odp" Bang!',
	      'kordinat_odp.required' => 'Silahkan isi kolom "kordinat_odp" Bang!',
	      'noTelp.required' => 'Silahkan isi kolom "No. Telpon" Bang!',
	      'noTelp.numeric' => '"No. Telpon" Tidak Valid Bang!'
	    ];

	    $validator = Validator::make($request->all(), $rules, $messages);
	    $a = $request->input('status');
	    $b = $request->input('action');

	    if($a == 1){
	      $validator->sometimes('action', 'required', function ($input) {
	          return true;
	      });

	      $validator->sometimes('penyebab', 'required', function ($input) {
	          return true;
	      });

	    }
	    if($a == 2) {
	      $validator->sometimes('kordinat_odp', 'required', function ($input) {
	        return true;
	      });
	      $validator->sometimes('nama_odp', 'required', function ($input) {
	        return true;
	      });
	    }
	    if($a == 11) {
	      $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
	        return true;
	      });
	    }

	    if ($b==19){
	      $validator->sometimes('noTelp', 'required', function($input){
	        return true;
	      });
	      $validator->sometimes('noTelp', 'numeric', function($input){
	        return true;
	      });
	    }

	    if ($validator->fails()) {
	      return redirect()->back()
	          ->withInput($request->input())
	          ->withErrors($validator)
	          ->with('alerts', [
	            ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan, Silahkan scroll ke bawah untuk melihat field apa saja yg harus di isi, scroll kebawah!']
	          ]);
	    }

	    if ($request->input('alpro')<>"") :
	    DB::table('roc')
	        ->where('no_tiket',$request->input('no_tiket'))
	        ->update([
	          'alpro' => $request->input('alpro')
	        ]);
	    endif;

	    if (count($exists)) {
	      $data = $exists[0];
	      $sendReport = 0;
	      if($data->status_laporan!=$request->input('status')){
	        $sendReport = 1;
	      }

	    // update no.telp
	    DB::transaction(function() use($request, $disp, $auth) {
	        DB::table('roc')->where('no_tiket',$disp->Ndem)->update([
	            'no_telp' => $request->noTelp
	        ]);    
	    });
	     
	    DB::transaction(function() use($request, $disp, $auth) {
	        DB::table('psb_laporan_log')->insert([
	          'created_at'      => DB::raw('NOW()'),
	          'created_by'      => $auth->id_karyawan,
	          'status_laporan'  => $request->input('status'),
	          'id_regu'         => $disp->id_regu,
	          'catatan'         => $request->input('catatan'),
	          'Ndem'            => $disp->Ndem,
	          'mttr'            => $this->datediff($disp->updated_at,date('Y-m-d h:i:s')),
	          'psb_laporan_id'	 => $disp->id,
	          'penyebabId'      => $request->input('penyebab')
	        ]);
	    });

	      DB::transaction(function() use($request, $data, $auth, $materials, $ntes) {
	        DB::table('psb_laporan')
	          ->where('id', $data->id)
	          ->update([
	            'modified_at'   		=> DB::raw('NOW()'),
	            'modified_by'  	 		=> $auth->id_karyawan,
	            'catatan'       		=> $request->input('catatan'),
	            'status_laporan'		=> $request->input('status'),
	            'action'				=> $request->input('action'),
	            'kordinat_pelanggan'	=> $request->input('kordinat_pelanggan'),
	            'nama_odp'				=> $request->input('nama_odp'),
	            'kordinat_odp'			=> $request->input('kordinat_odp'),
	            'penyebabId'  			=> $request->input('penyebab'),
	            'typeont'             	=> $request->input('type_ont'),
	            'snont'               	=> $request->input('sn_ont'),
	            'typestb'             	=> $request->input('type_stb'),
	            'snstb'               	=> $request->input('sn_stb')
	        ]);
	        
	        $this->incrementStokTeknisi($data->id, $auth->id_karyawan);
	        
	        DB::table('psb_laporan_material')
	          ->where('psb_laporan_id', $data->id)
	          ->delete();
	        
	        foreach($ntes as $nte) {
	          	DB::table('psb_laporan_material')->insert([
	            	'id' => $data->id.'-'.$nte,
	            	'psb_laporan_id' => $data->id,
	            	'id_item' => $nte,
	            	'qty' => 1,
	            	'type' =>2
	          	]);

	          	DB::table('nte')
	           		->where('id', $nte)
	           	 	->update([
	             	 'position_type'  => 3,
	            	 'position_key'  => $data->id,
	             	 'status' => 1
	           	]);
	        }

	        $this->insertMaterials($data->id, $materials);
	        $this->decrementStokTeknisi($auth->id_karyawan, $materials);
	      });
	      $this->handleFileUpload($request, $id);
	      
	      if($sendReport){
	          exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
	      }

	      return back()->with('alerts', [
	        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
	      ]);
	    }
	    else {
	      // update no.telp
	      DB::transaction(function() use($request, $disp, $auth) {
	          DB::table('psb_fullfillment_guarante')->where('sc',$disp->Ndem)->update([
	              'noTelp' => $request->noTelp
	          ]);    
	      });

	      DB::transaction(function() use($request, $id, $auth, $materials, $disp, $ntes) {
	        $insertId = DB::table('psb_laporan')->insertGetId([
	          'created_at'    		=> DB::raw('NOW()'),
	          'created_by'    		=> $auth->id_karyawan,
	          'id_tbl_mj'     		=> $id,
	          'catatan'       		=> $request->input('catatan'),
	          'status_laporan'		=> $request->input('status'),
	          'action'        		=> $request->input('action'),
	          'kordinat_pelanggan'	=> $request->input('kordinat_pelanggan'),
	          'nama_odp'			=> $request->input('nama_odp'),
	          'kordinat_odp'		=> $request->input('kordinat_odp'),
	          'penyebabId'  		=> $request->input('penyebab'),
	          'typeont'             => $request->input('type_ont'),
	          'snont'               => $request->input('sn_ont'),
	          'typestb'             => $request->input('type_stb'),
	          'snstb'              	=> $request->input('sn_stb')  
	        ]);

	      foreach($ntes as $nte) {
	        DB::table('psb_laporan_material')->insert([
	          'id' => $insertId.'-'.$nte,
	          'psb_laporan_id' => $insertId,
	          'id_item' => $nte,
	          'qty' => 1,
	          'type' =>2
	        ]);

	        DB::table('nte')
	          ->where('id', $nte)
	          ->update([
	            'position_type'  => 3,
	            'position_key'  => $id,
	            'status' => 1
	        ]);
	      }

	      $this->insertMaterials($insertId, $materials);
	      $this->decrementStokTeknisi($auth->id_karyawan, $materials);

	      $dataDisp = $disp;
	      DB::transaction(function() use($request, $dataDisp, $auth, $insertId) {
	         DB::table('psb_laporan_log')->insert([
	           'created_at'      => DB::raw('NOW()'),
	           'created_by'      => $auth->id_karyawan,
	           'mttr'            => $this->datediff($dataDisp->updated_at,date('Y-m-d h:i:s')),
	           'status_laporan'  => $request->input('status'),
	           'id_regu'         => $dataDisp->id_regu,
	           'catatan'         => $request->input('catatan'),
	           'Ndem'            => $dataDisp->Ndem,
	           'psb_laporan_id'  => $insertId,
	           'penyebabId'      => $request->input('penyebab')
	         ]);
	       });
	    });
	    
	    $this->handleFileUpload($request, $id);
	    exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');

	      return redirect('/')->with('alerts', [
	        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
	      ]);
	    }
	}

	public function searchform(Request $req)
	{
		$this->validate($req,[
			'q'	=> 'numeric'
		]);

		$listFulls = [];
		if ($req->has('q')){
			$q 	  	   = $req->q;
			$listFulls = DB::table('dispatch_teknisi')
							->leftJoin('Data_Pelanggan_Starclick','dispatch_teknisi.Ndem','=','Data_Pelanggan_Starclick.orderId')
							->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
							->where('Ndem',$q)
							->where('dispatch_by','4')
							->get();
		}

		// dd($listFulls);
		return view('guarantee.search',compact('listFulls'));
	}

	public function formRedispatch ($id)
	{
		$data = DB::table('dispatch_teknisi')
				->leftJoin('Data_Pelanggan_Starclick','dispatch_teknisi.Ndem','=','Data_Pelanggan_Starclick.orderId')
				->leftJoin('roc','dispatch_teknisi.Ndem','=','roc.no_tiket')
				->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
				->where('Ndem',$id)
				->where('dispatch_by','4')
				->first();

		$regu = DB::select('
	      SELECT a.id_regu as id,a.uraian as text, b.title
	      FROM regu a
	        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
	      WHERE
	        ACTIVE = 1
	    ');

		// dd($data);
		return view('guarantee.form',compact('data', 'regu'));
	}

	public function saveRedispatch(Request $request, $id)
	{
		$this->validate($request,[
        	'tglOpen' => 'required',
        	'jamOpen' => 'required'
   		]);

   		$now = $request->tglOpen.' '.$request->jamOpen.':00';
        $openTanggal = date('Y-m-d H:i:s',strtotime($now));

	    DB::table('dispatch_teknisi')->where('Ndem',$id)->where('dispatch_by','4')
	    	->update([
	 	        'tgl'           => $request->input('tgl'),
	    	    'id_regu'       => $request->input('id_regu'),	
	    	]);

	    DB::table('roc')->where('no_tiket',$id)->update([
	        'tglOpen'  => $openTanggal
      	]);

	    return redirect('/assurance/search')->with('alerts', [
	        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Re-Dispatch Regu']
	    ]);
	}

	public function hapusWoFg($id)
	{
		DB::table('roc')->where('no_tiket',$id)->delete();
		DB::table('dispatch_teknisi')->where('Ndem',$id)->where('dispatch_by','4')->delete();

		return back()->with('alerts', [
	        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Hapus WO FG']
	    ]);
	}
}