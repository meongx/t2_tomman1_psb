@extends('layout')

@section('content')
  @include('partial.alerts')

      <div class="panel panel-primary" id="info">
          <div class="panel-heading">List Rekap ODP</div>
          <div class="panel-body table-responsive">
              {{-- pencarian --}}
              <form class="row" style="margin-bottom: 20px">
                  <div class="col-md-12">
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="Cari Nama ODP" name="q"/>
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit">
                              <span class="glyphicon glyphicon-search"></span>
                            </button>
                          </span>
                      </div>
                  </div>
              </form>

              <table class="table table-bordered table-fixed">
                  <tr>
                      <th>#</th>
                      <th>Nama ODP</th>
                      <th>Jumlah Pelanggan</th>
                  </tr>
                 
                  @foreach($data as $no => $d)
                          <td>{{ ++$no }}</td>
                          <td>{{ $d->nama_odp }}</td>
                          <td><a href="/tool/rekaodp/pelanggan?sort={{ $d->nama_odp }}">{{ $d->jml_pel }}</a></td>
                      </tr>
                  @endforeach
              </table>
          </div>
      </div>
@endsection
