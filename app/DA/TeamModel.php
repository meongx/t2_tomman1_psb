<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class TeamModel
{
	public static function mainsector(){
		return DB::table('group_telegram')->get();
	}
	public static function getRegu($id){
		return DB::SELECT('
								SELECT
									a.*,
									b.title as mainsector,
									(SELECT aa.nama FROM  1_2_employee aa WHERE aa.nik = a.nik1) as nama_nik1,
									(SELECT bb.nama FROM  1_2_employee bb WHERE bb.nik = a.nik2) as nama_nik2
								FROM
									regu a
									LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
								WHERE
									b.Witel_New = "'.$id.'" AND
									ACTIVE = 1
							');
	}
	public static function checkRegu($id){
		return DB::table('regu')->where('uraian',$id)->get();
	}
	public static function detailRegu($id){
		return DB::table('regu')->where('id_regu',$id)->first();
	}
	public static function distinctStatus(){
		return DB::SELECT('SELECT mitra as text,mitra as id FROM regu GROUP BY mitra');
	}
	public static function store($r){
		return DB::table('regu')
								->insert([
									'uraian' => $r->input('uraian'),
									'ACTIVE' => 1,
									'nik1' => $r->input('nik1'),
									'nik2' => $r->input('nik2'),
									'mitra' => $r->input('mitra'),
									'mainsector' => $r->input('mainsector'),
									'witel' => "KALSEL",
									'job' => "MULTISKILL",
									'spesialis' => "MULTISKILL",
									'kemampuan' => $r->input('manhours'),
									'crewid' => $r->input('crewid'),
									'sttsWo' => 1
								]);
	}
	public static function update($r){
		$stts = 0;
		if ($r->input('mitra')=="SPM" || $r->input('mitra')=="AGB DELTA" || $r->input('mitra')=="CUI DELTA" || $r->input('mitra')=="UPATEKDELTA"){
			$stts = 1;
		};
		return DB::table('regu')
								->where('id_regu',$r->input('id_regu'))
								->update([
									'uraian' => $r->input('uraian'),
									'ACTIVE' => 1,
									'nik1' => $r->input('nik1'),
									'nik2' => $r->input('nik2'),
									'mitra' => $r->input('mitra'),
									'mainsector' => $r->input('mainsector'),
									'witel' => "KALSEL",
									'job' => "MULTISKILL",
									'spesialis' => "MULTISKILL",
									'kemampuan' => $r->input('manhours'),
									'crewid' => $r->input('crewid'),
									'status_alker' => $r->input('status_alker'),
									'sttsWo' => $stts
								]);
	}
	public static function update_2($r){
		$stts = 0;
		if ($r->input('mitra')=="SPM" || $r->input('mitra')=="AGB DELTA" || $r->input('mitra')=="CUI DELTA" || $r->input('mitra')=="UPATEKDELTA"){
			$stts = 1;
		};
		return DB::table('regu')
								->where('id_regu',$r->input('id_regu'))
								->update([
									'uraian' => $r->input('uraian'),
									'ACTIVE' => 1,
									'nik1' => $r->input('nik1'),
									'nik2' => $r->input('nik2'),
									'mitra' => $r->input('mitra'),
									'mainsector' => $r->input('mainsector'),
									'witel' => "KALSEL",
									'job' => "MULTISKILL",
									'spesialis' => "MULTISKILL",
									'kemampuan' => $r->input('manhours'),
									'crewid' => $r->input('crewid'),
									'sttsWo' => $stts,
									'status_team' => $r->input('status_team'),
									'status_alker' => $r->input('status_alker')
								]);
	}

	public static function disable($id){
		return DB::table('regu')
								->where('id_regu',$id)
								->update([
									'ACTIVE' => 0
								]);
	}
}
