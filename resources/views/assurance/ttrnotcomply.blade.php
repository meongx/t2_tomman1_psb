@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>TTR 3 JAM NOT COMPLY MTD</h3>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading">Table</div>
  			<div class="panel-body">
  				<table class="table">
  					<tr>
  						<th>Sektor</th>
  						<?php for ($i=1;$i<=date('d');$i++) :  ?>
  						<th>{{ $i }}</th>
  						<?php endfor; ?>
  					</tr>
  					@foreach ($get_ttr3notcomply_sektor as $num => $ttr3notcomply) 
  					<tr>
  						<td>{{ $ttr3notcomply->title ? : 'NONTOMMAN' }}</td>
  						<?php for ($i=1;$i<=date('d');$i++) :  ?>
  						<td><a href="/ttr3notcomplylist/{{ $ttr3notcomply->title }}/{{ date('Y-m-'.$i) }}">{{ $data[$ttr3notcomply->title]['tgl'.$i] }}</a></td>
  						<?php endfor; ?>
  					</tr>
  					@endforeach
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection