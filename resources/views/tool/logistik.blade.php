@extends('layout')

@section('content')
  @include('partial.alerts')
      <div class="panel panel-default">
          <div class="panel-heading">
              Logistik
          </div>
          <div class="panel-body">
              <form method="post" class="form-horizontal">
                  <div class="form-group">
                      <div class="col-md-12">
                          <label for="noRfc" class="control-label">No RFC</label>
                            <input name="noRfc" type="text" id="noRfc" class="form-control" value="{{ old('noRfc') }}"/>
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <label for="proaktif_id" class="control-label">Project ID</label>
                            <input name="proaktif_id" type="text" id="proaktif_id" class="form-control" value="{{ old('proaktif_id') }}"/>
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <label for="nama_gudang" class="control-label">Nama Gudang</label>
                          <input name="nama_gudang[]" type="text" id="nama_gudang" class="form-control" />
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <label for="mitra" class="control-label">Peruntukan</label>
                          <input name="mitra[]" type="text" id="mitra" class="form-control" />
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <label for="tgl" class="control-label">Tgl Pengeluaran</label>
                          <input name="tgl" type="tgl" id="tgl" class="form-control" value="{{ old('tgl') }}"/>
                        
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <button class="btn btn-primary" type="submit">
                              <span class="glyphicon glyphicon-refresh"></span>
                              <span>Proses</span>
                          </button>
                      </div>
                  </div>
              </form>
          </div>
      </div>

        @if (!empty($data))
          <form method="get" action="/tool/logistik/excelmaterialkeluar" >
              <input type="submit" class="btn btn-primary" value="Export ke Excel">
          </form><br>
      @endif

      <div class="panel panel-default" id="info">
      <div class="panel-heading">List</div>
          <div class="panel-body table-responsive">
              <table class="table table-bordered table-fixed">
                  <tr>
                      <th>Alista Id</th>
                      <th>Tgl</th>
                      <th>Nama Gudang</th>
                      <th>Project</th>
                      <th>Pengambil</th>
                      <th>Mitra</th>
                      <th>ID Barang</th>
                      <th>Nama Barang</th>
                      <th>Jumlah</th>
                      <th>Registered</th>
                      <th>Terpakai</th>
                      <th>Kembali</th>
                      <th>No RFC</th>
                      
                  </tr>
                  @foreach($data as $no => $d)
                      <tr>
                          <td>{{ $d['alista_id']}}</td>
                          <td>{{ $d['tgl'] }}</td>
                          <td>{{ $d['nama_gudang'] }}</td>
                          <td>{{ $d['project'] }}</td>
                          <td>{{ $d['pengambil'] }}</td>
                          <td>{{ $d['mitra'] }}</td>
                          <td>{{ $d['id_barang'] }}</td>
                          <td>{{ $d['nama_barang'] }}</td>
                          <td>{{ $d['jumlah'] }}</td>
                          <td>{{ $d['register'] }}</td>
                          <td>{{ $d['terpakai'] }}</td>
                          <td>{{ $d['kembali'] }}</td>
                          <td><a href="/downloadrfc/{{$d['alista_id']}}" target="_BLANK">{{ $d['no_rfc'] }}</a></td>
                      </tr>
                  @endforeach
              </table>
          </div>
      </div>
@endsection

@section('plugins')
    <script src="/bower_components/select2/select2.min.js"></script>
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>

    <script>
        $(function() {
            var project = <?= json_encode($project) ?>;
            $('#proaktif_id').select2({
                data: project,
                placeholder: 'Pilih project',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
            var gudang = <?= json_encode($gudang) ?>;
            $('#nama_gudang').select2({
                data: gudang,
                placeholder: 'Pilih Gudang',
                allowClear: true,
                multiple: true
                // maximumSelectionSize: 
                // formatSelection: function(data) { return data.text },
                // formatResult: function(data) {
                //   return  data.text;
                // }
            });

            var mitras = <?= json_encode($mitra) ?>;
            $('#mitra').select2({
                data: mitras,
                placeholder: 'Pilih Peruntukan',
                allowClear: true,
                multiple: true
                // maximumSelectionSize: 0
                // formatSelection: function(data) { return data.text },
                // formatResult: function(data) {
                //   return  data.text;
                // }
            });

            var day = {
              format : 'yyyy-mm-dd',
              viewMode: 0,
              minViewMode: 0
            };
       
            $('#tgl').datepicker(day).on('changeDate', function(e){
              $(this).datepicker('hide');
            });
        });
    </script>
@endsection