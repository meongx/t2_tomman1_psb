@extends('layout')

@section('content')
  @if ($auth->level==2 || (session('auth')->id_user == '89150015' && session('auth')->level == 46 ))
  @include('partial.alerts')
      <style>
    	  .panel-content {
    		  padding : 10px;
    	  }
    	</style>
      <h3> 
    	  <a href="/employee/input" class="btn btn-info">
          <span class="glyphicon glyphicon-plus"></span>
        </a>
        Employee</h3>
      <div class="panel panel-primary">
      	<div class="panel-heading">List</div>
      	<div class="panel-content">
      		<div class="table table-responsive">
      			<table class="table table-responsive">
      				<tr>
    	  				<th>No</th>	
      					<th>NIK</th>	
      					<th>NAMA</th>	
      					<th>LABORCODE</th>	
      					<th>ATASAN 1</th>	
      					<th>CAT JOB</th>	
      					<th>SUB_DIRECTORAT</th>	
      					<th></th>	
      				</tr>
      				@foreach ($getAll as $num => $data)
      				<tr>
    	  				<td>{{ ++$num }}</td>
    	  				<td>{{ $data->nik }}</td>
    	  				<td>{{ $data->nama }}</td>
    	  				<td>{{ $data->laborcode }}</td>
    	  				<td>{{ $data->atasan_1 }}</td>
    	  				<td>{{ $data->CAT_JOB }}</td>
    	  				<td>{{ $data->sub_directorat }}</td>
    	  				<td><a href="/employee/{{ $data->id_people }}" class="btn btn-xs btn-warning">Edit</a></td>
    	  				
      				</tr>
      				@endforeach
      			</table>
      		</div>
      	</div>
      </div>
    @else
        <h1>Banyaki Bawa Beibadah Ja Parak Kiamat !!!</h1>
    @endif
  
@endsection