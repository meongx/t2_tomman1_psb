@extends('layout')

@section('content')
  @include('partial.alerts')
  <a href="/dashboard/rekon/{{ $tgl }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List DC > 75 {{ $mitra }} ({{ $tgl }})</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>No.</th>
          <th>No Tiket</th>
          <th>Tim</th>
          <th>Mitra</th>
          <th>Tgl_Open</th>
          <th>Tgl Close</th>
          <th>Status Laporan</th>
          <th>Panjang DC</th>
        </tr>

        @foreach($getData as $no=>$data)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $data->Ndem }}</td>
                <td>{{ $data->uraian }}</td>
                <td>{{ $data->mitra }}</td>
                <td>{{ $data->created_at }}</td>
                <td>{{ $data->modified_at }}</td>
                <td>{{ $data->statusTeknisi }}</td>
                <td>{{ $data->dc_preconn }}</td>
            </tr>
        @endforeach

      </table>
    </div>
    </div>
  </div>
@endsection
