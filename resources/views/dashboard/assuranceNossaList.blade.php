@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <a href="/assuranceNossa/ALL" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a>
  <br />
  <br />
  <div class="row">
  	<div class="col-sm-12">
      <div class="panel panel-tomman">
      <div class="panel-heading">LIST {{ $periode }} {{ $witel }}</div>
      <div class="panel-body"> 
        <div class="well"> 
  		    Tiket2nya : @foreach ($query as $result) {{ $result->Incident }}, @endforeach 
        </div>
      <div class="table-responsive"> 
      	<table class="table styledTable">
  			<tr>
  				<th>No</th>
  				<th>Workzone</th>
  				<th>Witel</th>
  				<th>Incident</th>
          @if ($witel=="KALSEL (BANJARMASIN)")
          <th>Team</th>
          <th>Status</th>
          <th>Action</th>
          <th>Sebab</th>
          @endif
  				<th nowrap>Customer Name</th>
  				<th>Summary</th>
  				<th>Owner Group</th>
  				<th>Assigned To</th>
  				<th>Reported Date</th>
  				<th>Umur (Jam)</th>
  				<th>Incident Symptom</th>
  			</tr>
  			@foreach ($query as $num => $result)
  			<tr>
  				<td>{{ ++$num }}</td>
  				<td>{{ $result->Workzone }}</td>
  				<td nowrap>{{ $result->Witel }}</td>
  				<td>{{ $result->Incident }}</td>
          @if ($witel=="KALSEL (BANJARMASIN)")
          <td nowrap>{{ $result->Team ? : 'NOT ASSIGN' }}</td>
          <td nowrap>{{ $result->Status ? : 'NO UPDATE' }}</td>
          <td nowrap>{{ $result->Action ? : '~' }}</td>
          <td nowrap>{{ $result->Sebab ? : '~' }}</td>
          @endif
  				<td nowrap>{{ $result->Customer_Name }}</td>
  				<td nowrap class="areatext">{{ $result->Summary }}</td>
  				<td nowrap>{{ $result->Owner_Group }}</td>
  				<td nowrap>{{ $result->Assigned_to }}</td>
  				<td nowrap>{{ $result->Reported_Date }}</td>
          @if ($result->Umur<0)
          <td>0</td>
          @else
  				<td>{{ $result->Umur }}</td>
          @endif
  				<td nowrap>{{ $result->Incident_Symptom }}</td>
  			</tr>
  			@endforeach
  		</table>
    </div>
    </div>
    </div>
  	</div>
  </div>
@endsection