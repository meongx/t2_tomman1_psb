@extends('tech_layout')

@section('content')
  @include('partial.alerts')
  <br />
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body">
          Today Productivity Ach
          @if (count($get_asr_close)>0)
            {{ $get_asr_close[0]->jumlah }}
            <?php
              $percent = ($get_asr_close[0]->jumlah/4)*100;
            ?>
          @else
            0
          @endif
          (%)<br />
          <div class="progress progress-striped active">
            <div class="progress-bar progress-bar-red" role="progressbar" aria-valuenow="{{ $percent }}" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
              <span class="sr-only">40% Complete (red)</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <?php if (count($checkONGOING)>0) : ?>
      <div class="content">
        <?php foreach($checkONGOING as $ONGOING) : ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            ONGOING PROGRESS {{ $ONGOING->Ndem }}
          </div>
          <div class="panel-content" style="padding:20px;">

            <center>
            @if(strtoupper(substr($ONGOING->Ndem,0,2)) <> "IN")
            <a href="/{{ $ONGOING->id_dt }}" class="responsive btn btn-sm btn-warning">CLICK HERE FOR REPORT</a>
            @else
            <a href="/tiket/{{ $ONGOING->id_dt }}" class="responsive btn btn-sm btn-warning">CLICK HERE FOR REPORT</a>
            @endif
            <center>

          </div>
        </div>
          <?php endforeach; ?>
      </div>
      <?php endif; ?>
    </div>
    <div class="col-sm-12">
          <ul class="nav nav-tabs" style="margin-bottom:20px">
          <li class="{{ (Request::segment(4) == 'ASR') ? 'active' : '' }}"><a href="/v1.5/workorder/{{ $catstatus }}/ASR">ASR</a></li>
          <li class="{{ (Request::segment(4) == 'PROV') ? 'active' : '' }}"><a href="/v1.5/workorder/{{ $catstatus }}/PROV">PROV</a></li>
          <li class="{{ (Request::segment(4) == 'ODPLOS') ? 'active' : '' }}"><a href="/v1.5/workorder/{{ $catstatus }}/ODPLOS">ODP LOS</a></li>
          <li class="{{ (Request::segment(4) == 'PT2') ? 'active' : '' }}"><a href="/v1.5/workorder/{{ $catstatus }}/PT2">PT2</a></li>
            <!-- <li><a href="/stok">Materials/NTE</a></li> -->
          </ul>
          <div class="list-group" style="margin-top:-20px;">
            @foreach ($query as $data)
            <span  class="list-group-item">
              <a href="/grabNossaIN/{{ $data->Ndem }}" class="label label-default">#</a>
              <a href="/tiket/{{ $data->id_dt }}" class="label label-info">{{ $data->Ndem }}</a><br />
              <b>{{ $data->Name }}</b><br />
              Contact : {{ $data->Contact }} ~ {{ $data->Contact_Phone }}<br />
              <div class="well">
                {{ $data->Summary }}
              </div>
            </span>
            @endforeach
          </div>
    </div>
  </div>
@endsection
