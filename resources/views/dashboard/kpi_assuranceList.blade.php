@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
      th {
        background-color: #FF0000;
        color : #FFF;
        text-align: center;
        vertical-align: middle;
      }
      td {
        color : #000;
      }
    </style>

  <a href="/kpi/assurance/{{ $periode }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a>
  <h3>LIST {{ $tipe }} {{ $sektor }} {{ $periode }}</h3>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-responsive">
        <tr>
          <th>No.</th>
          <th>TROUBLE_NO</th>
          <th>NCLI</th>
          <th>ND_REFERENCE</th>
          <th>TROUBLE_NUMBER</th>
          <th>TOMMAN</th>
          <th>MITRA</th>
          <th>ACTION</th>
          <th>SEBAB</th>
          <th>TROUBLE_OPENTIME</th>
          <th>TROUBLE_CLOSETIME</th>
          <th>TGL_TECH_CLOSE</th>
          <th>TTR</th>
          <th>TTR_CONS</th>
          <th>SYMPTOM_LV0</th>
          <th>SOLUTION1</th>
          <th>SOLUTION0</th>
          <th>WORKZONE</th>
          <th>AMCREW</th>
        </tr>
        @foreach($query as $num => $result)
        <tr>
          <td>{{ ++$num }}.</td>
          @if ($result->uraian<>'')
          <td><a href="/tiket/{{ $result->id }}">{{ $result->TROUBLE_NO  }}</a></td>
          @else
          <td>{{ $result->TROUBLE_NO  }}</td>
          @endif
          <td>{{ $result->NCLI }}</td>
          <td>{{ $result->ND_REFERENCE }}</td>
          <td>{{ $result->TROUBLE_NUMBER }}</td>
          <td>{{ $result->uraian ? : 'UNDISPACTH' }}</td>
          <td>{{ $result->mitra ? : 'UNKNOWN' }}</td>
          <td>{{ $result->action ? : 'UNKNOWN' }}</td>
          <td>{{ $result->sebab ? : 'UNKNOWN' }}</td>
          <td>{{ $result->TROUBLE_OPENTIME }}</td>
          <td>{{ $result->TROUBLE_CLOSETIME }}</td>
          <td>{{ $result->TGL_TECH_CLOSE }}</td>
          <td>{{ round($result->TTR,2) }}</td>
          <td>{{ round($result->TTR_CONS,2) }}</td>
          <td>{{ $result->SYMPTOM_LV0 }}</td>
          <td>{{ $result->SOLUTION1 }}</td>
          <td>{{ $result->SOLUTION0 }}</td>
          <td>{{ $result->TK_WORKZONE }}</td>
          <td>{{ $result->AMCREW }}</td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
@endsection
