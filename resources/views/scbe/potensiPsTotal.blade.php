@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
  </style>
  <a href="/dashboard/scbe/{{ $tgl }}/" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List {{ $title }} {{ $tgl }} </h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th rowspan="2">No.</th>
          <th rowspan="2">SM</th>
          <th rowspan="2">TL</th>
          <th rowspan="2">TEKNISI</th>
          <th colspan="6">WO / TIKET</th>
          <th colspan="3">NOMOR LAYANAN</th>
          <th colspan="4">DATA PELANGGAN</th>
          <th colspan="2">TRANSAKSI / MUTASI</th>
          
          @if ($status=='INSERT TIANG' || $status=='ALL')
              <th colspan="12">LAIN-LAIN</th>
          @else
              <th colspan="13">LAIN-LAIN</th>
          @endif

        </tr>
        <tr>

          <th>NOMOR SC</th>
          <th>MYIR</th>
          <th>TGL WO</th>
          <th>TGL PASANG</th>
          <th>TGL PS</th>
          <th>STO</th>
          <th>TELEPON</th>
          <th>INET / DATIN</th>
          <th>NCLI</th>
          <th>NAMA</th>
          <th>ALAMAT</th>
          <th>No. HP</th>
          <th>ID SALES </th>
          <!-- <th>JENIS MUTASI</th> -->
          <th>LAYANAN</th>
          <th>JENIS PSB</th>
          <th>KORD. PEL </th>
          <th>ODP</th>
          <th>AREA</th>
          <th>NAMA MITRA</th>
          <th>STATUS SC</th>

          <th>STATUS TEK.</th>
          <th>REDAMAN IBOOSTER</th>

          @if ($status=='INSERT TIANG' || $status=='ALL')
              <th>TAMBAH TIANG</th>
          @endif

          <th>CATATAN TEK 1</th>
          <th>CATATAN TEK 2</th>
          <th>CATATAN TEK 3</th>
        </tr>
    
        @foreach ($dataUp as $num => $result)
        <tr>
          <td>{{ ++$num }}</td>
          <td>MUHAMMAD NOR RIFANI</td>
          <td>{{ $result->TL }}</td>
          <td>{{ $result->uraian }}</td>
          <td><a href="/{{ $result->id_dt }}">{{ $result->orderId }}</a></td>

          @php
            if (!empty($result->kcontact)){
                $dataMyir = $result->kcontact;
                if($dataMyir){
                  $pisah    = explode(';',$dataMyir);
                  $myir = '';
                  if (count($pisah)>1){
                    $myir = $pisah[1];
                  }
                }
            }
          @endphp

          <td> {{ $myir }} </td>
          <td>{{ $result->tanggal_order }}</td>
          <td>{{ $result->modified_at }}</td>
          <td>{{ $result->orderDatePs }}</td>
          <td>{{ $result->sto }}</td> 
          <td>{{ $result->noTelp }}</td>
          <td>{{ $result->internet }}</td>
          <td>{{ $result->orderNcli }}</td>
          <td>{{ $result->orderName }}</td>
          <td>{{ $result->kordinat_pelanggan }}</td>
          <td>{{ $result->noPelangganAktif }}</td>

           <!-- salas -->
          <?php
              $idSales = '-';
              if (substr($result->jenisPsb,0,2)=="AO"){
                $data = $result->kcontact;
                $dataNew = explode(';', $data);
                if ($dataNew[0] == 'MI'){
                  $idSales = $dataNew[2];       
                }
                else{
                  $idSales = '-';
                }
              }
           ?>

          <td>{{ $idSales }}</td>
          <!-- <td></td> -->
          <td>{{ $result->jenis_layanan }}</td>
          <td>{{ $result->jenisPsb }}</td>
         
          <td>{{ $result->kordinat_pelanggan ?: $result->kordinatPel }}</td>
          <td>{{ strtoupper($result->nama_odp) ?: '~' }}</td>
          <td>{{ $result->area_migrasi }}</td>
          <td>{{ $result->mitra }}</td>
          <td>{{ $result->orderStatus }}</td>
          <td>{{ $result->laporan_status }}</td>
          <td>{{ $result->redaman_iboster }}</td>
        
          @if ($status=='INSERT TIANG' || $status=='ALL')
                <td>{{ $result->ketTiang }}</td>
          @endif
          
          @php 
            if ($result->catatan==NULL){
               echo "<td></td>";
               echo "<td></td>";
               echo "<td></td>";
            }
            else{
                $cat = explode('#',$result->catatan);
                $jml = count($cat);

                $jmlCat = $jml;
                $awal = 0;
                if ($jml >= 3){
                    $jmlCat = 3;
                }

                if (!empty($jmlCat)){
                    for ($a=$awal;$a<=($jmlCat-1);$a++){
                        echo "<td>".$cat[$a]."</td>";
                    }

                    $bbb = 3 - $jmlCat;
                    for ($iii=0;$iii<=$bbb-1;$iii++){
                        echo "<td></td>";       
                    }
                }
            }
          @endphp
        </tr>
        @endforeach

        <?php
          $noUp = count($dataUp) + 1;
         ?>

        @foreach ($dataComp as $num => $result)
        <tr>
          <td>{{ $noUp++ }}</td>
          <td>MUHAMMAD NOR RIFANI</td>
          <td>{{ $result->TL }}</td>
          <td>{{ $result->uraian }}</td>
          <td><a href="/{{ $result->id_dt }}">{{ $result->orderId }}</a></td>

          @php
            if (!empty($result->kcontact)){
                $dataMyir = $result->kcontact;
                $pisah    = explode(';',$dataMyir);
                $myir = $pisah[1];
            }
          @endphp

          <td> {{ $myir }} </td>
          <td>{{ $result->tanggal_order }}</td>
          <td>{{ $result->modified_at }}</td>
          <td>{{ $result->orderDatePs }}</td>
          <td>{{ $result->sto }}</td> 
          <td>{{ $result->noTelp }}</td>
          <td>{{ $result->internet }}</td>
          <td>{{ $result->orderNcli }}</td>
          <td>{{ $result->orderName }}</td>
          <td>{{ $result->kordinat_pelanggan }}</td>
          <td>{{ $result->noPelangganAktif }}</td>

           <!-- salas -->
          <?php
              $idSales = '-';
              if (substr($result->jenisPsb,0,2)=="AO"){
                $data = $result->kcontact;
                $dataNew = explode(';', $data);
                if ($dataNew[0] == 'MI'){
                  $idSales = $dataNew[2];       
                }
                else{
                  $idSales = '-';
                }
              }
           ?>

          <td>{{ $idSales }}</td>
          <!-- <td></td> -->
          <td>{{ $result->jenis_layanan }}</td>
          <td>{{ $result->jenisPsb }}</td>
         
          <td>{{ $result->kordinat_pelanggan ?: $result->kordinatPel }}</td>
          <td>{{ strtoupper($result->nama_odp) ?: '~' }}</td>
          <td>{{ $result->area_migrasi }}</td>
          <td>{{ $result->mitra }}</td>
          <td>{{ $result->orderStatus }}</td>
          <td>{{ $result->laporan_status }}</td>
          <td>{{ $result->redaman_iboster }}</td>

          @if ($status=='INSERT TIANG' || $status=='ALL')
                <td>{{ $result->ketTiang }}</td>
          @endif
          
          @php 
            if ($result->catatan==NULL){
               echo "<td></td>";
               echo "<td></td>";
               echo "<td></td>";
            }
            else{
                $cat = explode('#',$result->catatan);
                  $jml = count($cat);

                $jmlCat = $jml;
                $awal = 0;
                if ($jml >= 3){
                    $jmlCat = 3;
                }

                if (!empty($jmlCat)){
                    for ($a=$awal;$a<=($jmlCat-1);$a++){
                        echo "<td>".$cat[$a]."</td>";
                    }

                    $bbb = 3 - $jmlCat;
                    for ($iii=0;$iii<=$bbb-1;$iii++){
                        echo "<td></td>";       
                    }
                }
            }
          @endphp
        </tr>
        @endforeach
        
      </table>
    </div>
    </div>

  </div>    <br />
      <br />
@endsection     
