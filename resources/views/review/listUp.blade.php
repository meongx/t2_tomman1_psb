@extends('layout')

@section('content')
  @include('partial.alerts')  
 
  <div class="panel panel-primary">
      <div class="panel-heading">FollowUp Review</div>
      <div class="panel-body table-responsive">
        <table class="table table-bordered table-hover table-fixed ">
           <tr>
              <th>No</th>
              <th>Ndem</th>
              <th>TIM</th>
              <th>Tanggal</th>
              <th>Catatan</th>
              <th>Rating</th>
              <th>Cat. Follow Up</th>
              <th></th>
           </tr>

           @foreach($list as $no => $dt)
              <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $dt->Ndem }}</td>
                  <td>{{ $dt->uraian }}</td>
                  <td>{{ $dt->date_created }}</td>
                  <td>{{ $dt->Catatan }}</td>
                  <td>{{ $dt->Review }}</td>
                  <td>{{ $dt->followup }}</td>
                  <td><a class="btn btn-info" href="/review/followup/detail/{{ $dt->id_dt }}">FollowUp</a></td>
              </tr>
           @endforeach()

          
        </table>
      </div>
  </div>

 
@endsection
