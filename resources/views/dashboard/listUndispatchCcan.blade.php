@extends('layout')

@section('content')
  @include('partial.alerts')
  <a href="" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List Undispatch Ccan</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>No.</th>
          <th>Area</th>
          <th>STO</th>
          <th>ND</th>
          <th>Nama</th>
          <th>Alamat</th>
          <th>Layanan</th>
          <th>PIC</th>
          <th>Status</th>
          
        </tr>
        @foreach ($data as $num => $dt)
        <tr>
          <td>{{ ++$num }}</td>
          <td>{{ $dt->area_migrasi }}</td>
          <td>{{ $dt->MDF }}</td>
          <td>{{ $dt->ND }}</td>
          <td>{{ $dt->Nama }}</td>
          <td>{{ $dt->Alamat }}</td>
          <td>{{ $dt->layanan }}</td>
          <td>{{ $dt->pic }}</td>
          <td><label class="btn btn-primary btn-sm">Undispatch</label> </td>
        </tr>
        @endforeach
      </table>
    </div>
    </div>
  </div>
@endsection
