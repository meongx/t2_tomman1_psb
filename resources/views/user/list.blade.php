@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    <a href="/user/v2/input" class="btn btn-info">
      <span class="glyphicon glyphicon-plus"></span>
    </a>
    User Tomman
  </h3>

  <div class="list-group">
    @foreach($list as $data)
      <a href="/user/v2/edit/{{ $data->id_user }}" class="list-group-item">
        <strong>{{ $data->id_user }}</strong>
        <br />
        <span>{{ $data->nama }}</span> /
        <span>{{ $data->level }}</span>

      </a>
    @endforeach
  </div>

@endsection
