<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
Use App\DA\AbsensiModel;
class AbsensiController extends Controller
{
  function rekap_mtd_teknisi(){
    $nik = session('auth')->id_karyawan;
    $query = AbsensiModel::rekap_mtd_teknisi($nik);
    return view('absensi.list',compact('nik','query'));
  }
}
