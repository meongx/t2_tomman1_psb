<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Telegram;
use App\DA\Smsmodel;

class SmsgammaController extends Controller
{
    public function newsms(Smsmodel $smsmodel){
        $list_c = $smsmodel -> listcontact_n();
        return view('smsG.news_sms', ['list_c' => $list_c]);
    }

    public function inbox(Smsmodel $smsmodel){
        $list_in = $smsmodel -> inboxlist();
        return view('smsG.inbox', ['list_in' => $list_in]);
    }

    public function outbox(Smsmodel $smsmodel){
        $list_in = $smsmodel -> outboxlist();
        $count = $list_in->count();
        return view('smsG.outbox', ['list_in' => $list_in, 'count' => $count]);
    }

    public function sent(Smsmodel $smsmodel){
        $list_in = $smsmodel -> sentitemslist();
        return view('smsG.sent', ['list_in' => $list_in]);
    }

    public function group(Smsmodel $smsmodel){
        $list_c = $smsmodel -> listcontact();
        $list_g = $smsmodel -> listgroup();
        $count = $smsmodel -> countcont();
        return view('smsG.Group_C', ['list_c' => $list_c, 'list_g' => $list_g, 'comb' => $count]);
    }

    public function savecontact(Request $req, Smsmodel $smsmodel){
        $nomor = substr_replace($req->telp, '+62', 0, 1);
        $smsmodel -> savecontact($req, $nomor);
        return redirect('/group/sms')->with('alerts',[['type'=>'success','text'=>'Kontak Telah Disimpan!!']]);
    }

    public function send3006(Request $req, Smsmodel $smsmodel){
        // $tujuan =explode(" ",$req->to);
        // $pecahnomor = $tujuan[0];
        $jmlSMS = ceil(strlen($req->pesan)/153);
        $pecah  = str_split($req->pesan, 153);
        $auto_I = $smsmodel -> AI();
        $AI = $auto_I[0]->Auto_increment;
        // $pengirim = substr_replace($pecahnomor, '0', 0, 3);
        $raw_pengirim = $req->to;
        foreach ($raw_pengirim as $pengirim) {
            $smsmodel -> send3006single($req, $pengirim);
        }
        // for ($i=1; $i<=$jmlSMS; $i++)
        // {
        //     $udh = "050003A7".sprintf("%02s", $jmlSMS).sprintf("%02s", $i);
        //     $msg = $pecah[$i-1];
        //     if ($i == 1)
        //     {
        //         foreach ($raw_pengirim as $pengirim) {
        //             $smsmodel -> send3006single($req, $pengirim);
        //         }
        
        //     }
        //     else
        //     {
        //         $smsmodel -> send3006multi($req, $udh, $AI, $i);
        //     }
        // }

        return redirect('/new/sms')->with('alerts',[['type'=>'success','text'=>'Pesan Telah terkirim!!']]);
    }

    public function savegroup(Request $req, Smsmodel $smsmodel){
        $smsmodel -> savegroup($req);
        return redirect('/group/sms');
    }

    public function savegroupc(Request $req, Smsmodel $smsmodel){
        $kontak = $req -> input('kontak');
        $jumlah = count($kontak);
        if (0<$jumlah){
            foreach( $kontak as $key) {                
                $smsmodel->simpanGroupC($req, $key);
            }
        }else{}
    }

    public function detail_I($ID, Smsmodel $smsmodel){
        $detail = $smsmodel->detail_I($ID);
        return view('smsG.detail_I', compact('detail'));
    }

    public function detail_O($ID, Smsmodel $smsmodel){
        $detail = $smsmodel->detail_O($ID);
        return view('smsG.detail_O', compact('detail'));
    }

    public function detail_S($ID, Smsmodel $smsmodel){
        $detail = $smsmodel->detail_S($ID);
        return view('smsG.detail_S', compact('detail'));
    }

    public function delete_sms(Smsmodel $smsmodel, $id){
        $smsmodel->delete($id);
        return redirect('/group/sms')->with('alerts',[['type'=>'danger','text'=>'Data Telah Dihapus!!']]);
    }

    public function edit_sms(Smsmodel $smsmodel, $id){
        $edit = $smsmodel->edit($id);
        return view('smsG.edit', ['edit' => $edit]);
    }

    public function update(Request $req, Smsmodel $smsmodel, $id){
        $no = $req->telp;
        $nomor_check = substr($no,0,3);
        if($nomor_check == '+62'){
            $nomor = substr_replace($no, '0', 0, 3);
            $smsmodel->update_62($req, $id, $nomor);
            return redirect('/group/sms')->with('alerts',[['type'=>'info','text'=>'Data Telah Diperbaharui!!']]);
        }else{
            $nomor = substr_replace($no, '+62', 0, 1);
            $smsmodel->update_0($req,$id, $nomor);
            return redirect('/group/sms')->with('alerts',[['type'=>'info','text'=>'Data Telah Diperbaharui!!']]);
        }
    }

    public function formSmsProv($tgl, Smsmodel $modelSms)
    {
        $lists = $modelSms->listPesanMasuk($tgl);
        return view('smsG.smsProv',compact('lists'));
    }

    public function kirimSmsProv(Request $req, Smsmodel $modelSms)
    {
        $noSC  = explode(',', $req->noTelp);
        $datas = $modelSms->getNoTelpBySc($noSC);
        
        // ambil no.telp berdasarkan nosc
        foreach($datas as $data){
            if (!empty($data->noTelp)){
                $telp[] = $data->noTelp;
            };
        };
        $jml = count($telp);

        $noTelp = '';
        for ($a=0; $a<$jml; $a++){
            $noTelp .= '("'.$telp[$a].'","'.$req->pesan.'","true","3006"),';
        }

        $noTelp = substr($noTelp, 0, -1);
        $sql = 'INSERT INTO outbox (DestinationNumber,TextDecoded,MultiPart,CreatorID) values '.$noTelp;
        // dd($sql);

        DB::connection('mysql2')->insert($sql);
        return back()->with('alerts',[['type' => 'success', 'text' => 'Pesan Berhasil Dikirim']]);
    }

    public function livesearch(Request $req, Smsmodel $smsmodel){
        $term = trim($req->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $fetchData = $smsmodel->value_search($term);

        $formatted_tags = [];

        foreach ($fetchData as $row) {
            $formatted_tags[] = ["id"=> $row->telp_2, "text"=>$row->telp.'('.$row->nama.')'];
        }

        return \Response::json($formatted_tags);
    }
}

