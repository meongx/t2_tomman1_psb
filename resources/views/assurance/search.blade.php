<div>
  <form class="form-group" method="post" id="submit-form" action="/assurance/search" >
    <label for="Search">Search</label>
    <input type="text" name="Search" class="form-control" id="Search" value="">
    <button class="btn form-control btn-primary">Search</button>
  </form>
</div>
@if ($data<>NULL)
  <br />
  <div class="form-control">
    @if (count($query)>0)
    <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <thead>
          <tr>
            <th>#</th>
            <th></th>
          </tr>
        </thead>
      </table>
    </div>
    @endif
  </div>
@endif
