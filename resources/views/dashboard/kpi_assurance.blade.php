@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    td {
      padding : 5px;
      border:1px solid #ecf0f1;
    }
    .pdnol {
      padding : 0px;
    }
    .center {
      text-align: center;
    }
    th {
      text-align: center;
      vertical-align: middle;
      background-color: #0073b7;
      padding : 5px;
      color : #FFF;
      border:1px solid #ecf0f1;
    }
    td {
      font-size: 14px;
      padding : 5px !important;

    }
    .center a {
      color : #666666 !important;
    }
    .green {
      background-color: #2ecc71;
      font-weight: bold;
      color : #FFF;
    }
    .green a:link{
      color : #FFF;
    }
    .red {
      background-color: #e67e22;
      font-weight: bold;
      color : #FFF;
    }
    .redx {
      background-color: #e74c3c;
      font-weight: bold;
      color : #FFF;
    }
    .red a:link{
      color : #FFF;
    }
    .gray {
      background-color : #f4f4f4;
    }
    select {
      padding : 10px;
    }
  </style>
  <script src="/bower_components/devexpress-web-14.1/js/dx.chartjs.js"></script>
  <link rel="stylesheet" href="/bower_components/devexpress-web-14.1/css/dx.dark.css" />
  <center>
  <h3>Dashboard KPI Assurance TA KALSEL</h3>
  <br />
  </center>
  <div class="row">
    <div class="col-sm-12">
      <form method="get">
        PERIODE :
        <select name="periode">
          <option value="">--</option>
          @foreach ($get_periodez as $periodez)
          <option value="{{ $periodez->THNBLN }}" <?php if ($periodez->THNBLN==$periode) echo 'selected = "selected"'; ?> >{{ $periodez->THNBLN }}</option>
          @endforeach
        </select>
        MITRA :
        <select name="mitra">
          <option value="ALL" <?php if($mitrax=="ALL") echo 'selected = "selected"' ?>> ALL </option>
          @foreach ($get_mitra as $mitra)
          <option value="{{ $mitra->mitra }}" <?php if ($mitra->mitra==$mitrax) echo 'selected = "selected"'; ?>>{{ $mitra->mitra }}</option>
          @endforeach
        </select>
        <input type=submit />
      </form>
      <table class="table">
        <tr>
          <th rowspan="2">Sektor</th>
          <th rowspan="2">Jumlah<br />GGN</th>
          <th colspan="4">SLG 48 (30%)</th>
          <th colspan="4">SLG 1DS (50%)</th>
          <th colspan="4">GAUL (10%)</th>
          <th rowspan="2">SALDO<br />(10%)</th>
          <th rowspan="2">KPI</th>
        </tr>
        <tr>
          <th>Real</th>
          <th>Ach</th>
          <th>KPI</th>
          <th class="redx">NC</th>
          <th>Real</th>
          <th>Ach</th>
          <th>KPI</th>
          <th class="redx">JML</th>
          <th class="redx">NC</th>
          <th class="redx">NC</th>
          <th>Ach</th>
          <th>KPI</th>
        </tr>
        <?php

          $Total = 0;

          $TotalReal1DS = 0;
          $TotalAch1DS = 0;
          $TotalKPI1DS = 0;
          $TotalNC1DS = 0;

          $TotalReal48 = 0;
          $TotalAch48 = 0;
          $TotalKPI48 = 0;
          $TotalNC48 = 0;
          $jumlahGAUL = 0;
          $TotalRealGAUL = 0;
          $TotalAchGAUL = 0;
          $TotalKPIGAUL = 0;
          $TotalKPI = 0;
          $jumlahKPI = 0;
        ?>
        @foreach ($get_kpi_assurance as $num => $kpi_assurance)
        <?php

          $Total += $kpi_assurance->JUMLAH;

          $Real1DS = ($kpi_assurance->R1DS / $kpi_assurance->JUMLAH)*100;
          $Ach1DS = ($Real1DS / 95)*100;
          $KPI1DS = ($Real1DS / 95)*50;
          $NC1DS = 100-$Real1DS;
          $jumlahGAUL += $kpi_assurance->GAUL;
          $TotalReal1DS += $Real1DS;
          $TotalAch1DS += $Ach1DS;
          $TotalKPI1DS += $KPI1DS;
          $TotalNC1DS += $NC1DS;

          $Real48 = ($kpi_assurance->R48 / $kpi_assurance->JUMLAH)*100;
          $Ach48 = ($Real48 / 98)*100;
          $KPI48 = ($Real48 / 98)*30;
          $NC48 = 100-$Real48;

          $TotalReal48 += $Real48;
          $TotalAch48 += $Ach48;
          $TotalKPI48 += $KPI48;
          $TotalNC48 += $NC48;

          $RealGAUL = ($kpi_assurance->GAUL/$kpi_assurance->JUMLAH)*100;
          $AchGAUL = (((3-$RealGAUL)/3)+1)*100;
          if ($AchGAUL<0) {
            $AchGAUL = 0;
          }
          $KPIGAUL = ($AchGAUL*10/100);

          if ($KPIGAUL>11){
            $KPIGAUL = 11;
          }
          $TotalRealGAUL += $RealGAUL;
          $TotalAchGAUL += $AchGAUL;
          $TotalKPIGAUL += $KPIGAUL;
          ++$num;

          $jumlahKPI = $KPI48 + $KPI1DS + $KPIGAUL + 11;

        ?>
        <tr>
          <td>{{ $kpi_assurance->sektor_asr ? : 'OTHERS' }}</td>
          <td>{{ $kpi_assurance->JUMLAH }}</td>
          <td><a href="/kpi/assuranceList/REAL48/{{ $kpi_assurance->sektor_asr ? : 'UNKNOWN' }}/{{ $periode }}">{{ round($Real48,2)  }}%</a></td>
          <td>{{ round($Ach48,2)  }}%</td>
          <td>{{ round($KPI48,2)  }}%</td>
          <td><a href="/kpi/assuranceList/NC48/{{ $kpi_assurance->sektor_asr ? : 'UNKNOWN' }}/{{ $periode }}">{{ round($NC48,2)  }}%</a></td>
          <td><a href="/kpi/assuranceList/REAL1DS/{{ $kpi_assurance->sektor_asr ? : 'UNKNOWN' }}/{{ $periode }}">{{ round($Real1DS,2)  }}%</a></td>
          <td>{{ round($Ach1DS,2)  }}%</td>
          <td>{{ round($KPI1DS,2)  }}%</td>
          <td><a href="/kpi/assuranceList/NC1DS/{{ $kpi_assurance->sektor_asr ? : 'UNKNOWN' }}/{{ $periode }}">{{ round($NC1DS,2)  }}%</a></td>
          <td>{{ $kpi_assurance->GAUL }}</td>
          <td><a href="/kpi/assuranceListGAUL/{{ $kpi_assurance->sektor_asr ? : 'UNKNOWN' }}/{{ $periode }}">{{ round($RealGAUL,2)  }}%</a></td>
          <td>{{ $kpi_assurance->GAUL }} / {{ $kpi_assurance->JUMLAH }} = {{ round($AchGAUL,2)  }}%</td>
          <td>{{ round($KPIGAUL,2)  }}%</td>
          <td>11%</td>
          <td>{{ round($jumlahKPI,2)  }}%</td>
        </tr>
        @endforeach
        <?php
          $TotalAchGAUL = (((3-($TotalRealGAUL/$num))/3)+1)*100;
          if ($TotalAchGAUL<0){
            $TotalAchGAUL = 0;
          }
          $TotalKPIGAUL = ($TotalAchGAUL*10/100);
          $TotalKPI = $TotalKPI48 + $TotalKPI1DS + $TotalKPIGAUL;
        ?>
        <tr>
          <td colspan="1">Total</td>
          <td>{{ $Total }}</td>
          <td><a href="/kpi/assuranceList/REAL48/ALL/{{ $periode }}">{{ round($TotalReal48/$num,2) }}%</a></td>
          <td>{{ round($TotalAch48/$num,2) }}%</td>
          <td>{{ round($TotalKPI48/$num,2) }}%</td>
          <td><a href="/kpi/assuranceList/NC48/ALL/{{ $periode }}">{{ round($TotalNC48/$num,2) }}%</a></td>
          <td><a href="/kpi/assuranceList/REAL1DS/ALL/{{ $periode }}">{{ round($TotalReal1DS/$num,2) }}%</a></td>
          <td>{{ round($TotalAch1DS/$num,2) }}%</td>
          <td>{{ round($TotalKPI1DS/$num,2) }}%</td>
          <td><a href="/kpi/assuranceList/NC1DS/ALL/{{ $periode }}">{{ round($TotalNC1DS/$num,2) }}%</a></td>
          <td>{{ $jumlahGAUL }}</td>
          <td><a href="/kpi/assuranceListGAUL/ALL/{{ $periode }}">{{ round($TotalRealGAUL/$num,2) }}%</a></td>
          <td>{{ round($TotalAchGAUL/$num,2) }}%</td>
          <td>{{ round($TotalKPIGAUL/$num,2) }}%</td>
          <td>11%</td>
          <td>{{ round(($TotalKPI/$num),2)+11 }}%</td>
        </tr>
      </table>
        Last update : {{ $last_update[0]->UPDATE_TIME }}
    </div>
    <div class="col-sm-4">
    <br />
          <!-- <center><h3>Tiket by ACTUAL SOLUTION</h3></center> -->
          <table class="table">
            <tr>
              <th>No</th>
              <th>Item</th>
              <th>Count</th>
            </tr>
            @foreach ($get_by_actual_solution as $numx => $actual_solution)
            <tr>
              <td>{{ ++$numx }}</td>
              <td>{{ $actual_solution->ACTUAL_SOLUTION }}</td>
              <td>{{ $actual_solution->JUMLAH }}</td>
            </tr>
            @endforeach
          </table>
    </div>
    <div class="col-sm-8">
    <script type="text/javascript">
          jQuery(document).ready(function($)
          {
            $("#bar-11").dxChart({
              title: 'Tiket by ACTUAL SOLUTION',
              label : {
                visible : true
              },
              argumentAxis: {
                  valueMarginsEnabled: false,
                  discreteAxisDivisionMode: "crossLabels",
                  grid: {
                      visible: true
                  }
              },
              AutoLayout : false,
                tooltip: {
                    enabled: true,
                  customizeText: function() {
                    return this.argumentText + "<br/>" + this.valueText;
                  }
                },
                size: {
                  height: 900
                },
                legend: {
                  visible: true
                },

                rotated : true,
              dataSource: [
                @foreach ($get_by_actual_solution as $numx => $actual_solution)
                { reporting : "{{ $actual_solution->ACTUAL_SOLUTION }}", val : {{ $actual_solution->JUMLAH }}},
                @endforeach
              ],
              series: [{
                  label: {
                      visible: true,
                      customizeText: function (segment) {
                          return segment.percentText;
                      },
                      connector: {
                          visible: true
                      }
                  },
                  name : "Action",
                  type: "bar",
                  argumentField: "reporting"
                }]
            });
          });
          </script>
          <div id="bar-11" style="height: 290px; width: 100%;"></div>
    </div>
    </div>
    <div class="row">
    <div class="col-sm-4">
    <br />
          <!-- <center><h3>Tiket by JENIS GGN</h3></center> -->
          <table class="table">
            <tr>
              <th>No</th>
              <th>Item</th>
              <th>Count</th>
            </tr>
            @foreach ($get_by_jenis_ggn as $numx => $actual_solution)
            <tr>
              <td>{{ ++$numx }}</td>
              <td>{{ $actual_solution->ACTUAL_SOLUTION ? : 'UNDEFINED' }}</td>
              <td>{{ $actual_solution->JUMLAH }}</td>
            </tr>
            @endforeach
          </table>
    </div>
    <div class="col-sm-8">
    <script type="text/javascript">
          jQuery(document).ready(function($)
          {
            $("#bar-22").dxChart({
              title: 'Tiket by JENIS GGN',
              label : {
                visible : true
              },
              argumentAxis: {
                  valueMarginsEnabled: false,
                  discreteAxisDivisionMode: "crossLabels",
                  grid: {
                      visible: true
                  }
              },
              AutoLayout : false,
                tooltip: {
                    enabled: true,
                  customizeText: function() {
                    return this.argumentText + "<br/>" + this.valueText;
                  }
                },
                legend: {
                  visible: true
                },

                rotated : true,
              dataSource: [
                @foreach ($get_by_jenis_ggn as $numx => $actual_solution)
                { reporting : "{{ $actual_solution->ACTUAL_SOLUTION }}", val : {{ $actual_solution->JUMLAH }}},
                @endforeach
              ],
              series: [{
                  label: {
                      visible: true,
                      customizeText: function (segment) {
                          return segment.percentText;
                      },
                      connector: {
                          visible: true
                      }
                  },
                  name : "Action",
                  type: "bar",
                  argumentField: "reporting"
                }]
            });
          });
          </script>
          <div id="bar-22" style="height: 290px; width: 100%;"></div>
          <br />
          <br />

    </div>
  </div>
@endsection
