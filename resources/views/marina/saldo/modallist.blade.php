<table class="table table-bordered table-fixed">
    <thead>
      <tr>
        <th width="20" class="head">#</th>
        <th width="20" class="head">RFC</th>
        <th width="20" class="head">ID Item</th>
        <th width="20" class="head">Out Gudang</th>
        <th width="20" class="head">Pemakaian</th>
        <th width="20" class="head">In Gudang</th>
        <th width="20" class="head">Status</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $no => $list) 
        <tr>
          <td>{{ ++$no }}</td>
          <td>{{ $list->rfc }}</td>
          <td>{{ $list->id_item }}</td>
          <td>{{ str_replace('-','',$list->value) }}</td>
          <td>{{ str_replace('-','',$list->pemakaian)?:'-' }}</td>
          <td>{{ str_replace('-','',$list->kembalian)?:'-' }}</td>
          <td>{{ ($list->value+$list->pemakaian+$list->kembalian)?'Outstanding':'Clear' }}</td>
        </tr>
      @endforeach
    </tbody>
</table>