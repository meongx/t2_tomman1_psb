@extends('layout')

@section('content')
@include('partial.alerts')
<form id="formbuatC" name="formbuatC" method="post" class="col-sm-6">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputnama">Nama</label>
      <input type="text" class="form-control" id="inputnama" placeholder="Nama Kontak" name="nama" required value="{!! $edit->nama!!}">
    </div>
    <div class="form-group col-md-6">
      <label for="nomortelp">Nomor Telepon</label>
      <input type="text" class="form-control" id="inputnotelp" placeholder="Nomor Telepon" name="telp" required value="{!! $edit->telp!!}">
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress2">Alamat Sekarang</label>
    <input type="text" class="form-control" id="alamatS" placeholder="Tempat Tinggal Sekarang" name="alamatS" required value="{!! $edit->A_S!!}">
  </div>
  <button type="submit" class="btn btn-primary">Simpan</button>
  <button type="button" class="btn btn-danger" onclick="window.location='{{ URL::to('/group/sms')}}'">Batal</button>
</form>
<script type="text/javascript">
  $(document).ready(function() {
    $("#inputnotelp").keydown(function (e) {
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        return;
    }

    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  });

    @endsection