@extends('layout')

@section('content')
@include('partial.alerts')
 <h4>MANJA DISPATCH</h4>
  <form class="row" style="margin-bottom: 20px">
      <div class="col-md-12">
          <div class="input-group {{ $errors->has('q') ? 'has-error' : '' }}">
            <input type="text" class="form-control" placeholder="Cari No. SC / WO / MYIR" name="q"/>
              <span class="input-group-btn">
                <button class="btn btn-primary" type="submit">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
          </div>
          {!! $errors->first('q','<p class=help-block>:message</p>') !!}
      </div>
  </form>

  @if(count($listManjas) <> 0)
    <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>#</th>
  		    <th>Work Order</th>
          <th>Nama Pelanggan</th>
          <th>STO</th>
          <th>Regu</th>
          <th>Transaksi</th>
          <th>Manja Status</th>
          <th>Manja</th>
          <th>Manja By</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
          @if($ketSc=='sc')
              <tr>
                  <td>1</td>
                  <td>{{ $listManjas[0]->orderId }} / {{ $myir }}</td>
                  <td>{{ $listManjas[0]->orderName  ?:  $listManjas[0]->customer }} </td>
                  <td>{{ $listManjas[0]->sto ?: $listManjas[0]->stoMyir }}</td> 
                  <td>{{ $listManjas[0]->uraian}}</td>
                  <td>{{ $listManjas[0]->jenisPsb ?: $listManjas[0]->lokerJns }}</td>
                   
                  @if ($listManjas[0]->manja_status==0 || $listManjas[0]->manja_status==NULL || $listManjas[0]->manja_status=='' )
                      <td>Belum Manja</td>
                  @elseif ($listManjas[0]->manja_status==1)
                      <td>Manja OK</td>
                  @elseif ($listManjas[0]->manja_status==2)
                      <td>Manja NOK</td>
                  @endif 

                  <td>{{ $listManjas[0]->manja }}</td>
                  <td>{{ $listManjas[0]->manjaBy }}</td>

                  @if($listManjas[0]->manja_status==0 || $listManjas[0]->manja_status==NULL || $listManjas[0]->manja_status='')
                      <td><a href="/dispatch/manja/{{ $listManjas[0]->orderId }}" class="btn btn-primary btn-sm">Dispatch Manja</a></td>
                  @else
                      <td><a href="/dispatch/manja/{{ $listManjas[0]->orderId }}" class="btn btn-danger btn-sm">Re-Dispatch Manja</a></td>
                  @endif
              </tr>
          @else
              <tr>
                  <td>1</td>
                  <td>{{ $listManjas[0]->myir }}</td>
                  <td>{{ $listManjas[0]->customer }} </td>
                  <td>{{ $listManjas[0]->sto }}</td> 
                  <td>{{ $listManjas[0]->uraian}}</td>
                  <td>{{ $listManjas[0]->lokerJns }}</td>
                   
                  @if ($listManjas[0]->manja_status==0 || $listManjas[0]->manja_status==NULL || $listManjas[0]->manja_status=='' )
                      <td>Belum Manja</td>
                  @elseif ($listManjas[0]->manja_status==1)
                      <td>Manja OK</td>
                  @elseif ($listManjas[0]->manja_status==2)
                      <td>Manja NOK</td>
                  @endif 

                  <td>{{ $listManjas[0]->manja }}</td>
                  <td>{{ $listManjas[0]->manjaBy }}</td>

                  @if($listManjas[0]->manja_status==0 || $listManjas[0]->manja_status==NULL || $listManjas[0]->manja_status='')
                      <td><a href="/dispatch/manja/{{ $listManjas[0]->myir }}" class="btn btn-primary btn-sm">Dispatch Manja</a></td>
                  @else
                      <td><a href="/dispatch/manja/{{ $listManjas[0]->myir }}" class="btn btn-danger btn-sm">Re-Dispatch Manja</a></td>
                  @endif
              </tr>

          @endif
      </tbody>
    </table>
  @endif
  {{-- </div> --}}
@endsection
