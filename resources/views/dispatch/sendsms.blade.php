@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
   MONITORING WORK ORDER <a href="/ms2n/sync/VA" class="btn btn-info">
      <span class="glyphicon glyphicon-refresh"> SyncNow</span>
    </a>
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <li class="{{ (Request::path() == 'dispatch') ? 'active' : '' }}"><a href="/dispatch">SEARCH WORK ORDER</a></li>
    <li class="{{ (Request::path() == 'dispatchPI') ? 'active' : '' }}"><a href="/dispatchPI">WORK ORDER READY</a></li>
    <li class="{{ (Request::path() == 'woSTB') ? 'active' : '' }}"><a href="/woSTB">WORK ORDER STB</a></li>
    <li class="{{ (Request::path() == 'woCFC') ? 'active' : '' }}"><a href="/woCFC">WORK ORDER CFC</a></li>
    <li class="{{ (Request::path() == 'woPI') ? 'active' : '' }}"><a href="/woPI">PI to FAILSWA</a></li>
    <li class="{{ (Request::path() == 'falloutWFM') ? 'active' : '' }}"><a href="/falloutWFM">FALLOUT WFM</a></li>
    <li class="{{ (Request::path() == 'falloutActivation') ? 'active' : '' }}"><a href="/falloutActivation">FALLOUT ACTIVATION</a></li>
<!--     <li class="{{ (Request::segment(2) == 'dispatchedPsb') ? 'active' : '' }}"><a href="/dispatch/dispatchedPsb">DISPATCHED SHVA</a></li> -->
    <li class="{{ (Request::segment(2) == 'reportPsb2') ? 'active' : '' }}"><a href="/dispatch/reportPsb2/<?php echo date('Y-m-d') ?>">REPORT WO SHVA</a></li>
    <li class="{{ (Request::segment(2) == 'reportCFC') ? 'active' : '' }}"><a href="/dispatch/reportCFC">REPORT WO CFC</a></li>
  </ul>

  
  <!-- dialog !-->
  
  Sent to : {{ $get_data->orderNotel }}<br />
  Message : <BR /> 
	<?PHP
	$text = "Salam. Kami dr Petugas Telkom, ingin melakukan pemasangan instalasi fiber di lokasi Anda. kira2 kapan bapak / ibu bisa kami kunjungi ? trims"; 
	$jumlah_text = strlen($text);
	echo $text."<br />";
	echo $jumlah_text;
	?>
  
  <div class="modal fade" id="modal-1">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Verifikasi</h4>
				</div>
				
				<div class="modal-body">
					Anda akan mengirimkan SMS Notifikasi kepada Pelanggan
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
					<button type="button" class="btn btn-info">Lanjutkan</button>
				</div>
			</div>
		</di
  
  <script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
@endsection
