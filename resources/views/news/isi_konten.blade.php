@extends('layout')

@section('content')
<style>
th, td {
	padding: 1px;
}
/* .side {
    width: 350px;
    position: fixed;
    z-index: 1;
    top: 68px;
    right: 10px;
    bottom: 120px;
    background: #eee;
    overflow-x: hidden;
    padding: 8px 0;
    } */
</style>
<form>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:history.back()">See News</a></li>
			<li class="breadcrumb-item active" aria-current="page">{!! $show_news[0]->judul !!}</li>
		</ol>
	</nav>
	<div class="col-sm-9" style="word-break: break-all;">
		<img style="max-width: 100%;height: auto" src="{{ asset('/upload/Thumbnails/'.$show_news[0]->thumbnails)}}"/>
		<br>
		<p><h4>{!! $show_news[0]->judul !!}</h4></p>
		<p>{!! $show_news[0]->konten !!}</p>
	</div>
	<div class="col-sm-3" style="position: relative;">
		<p><h4> TRENDING Topic Today</h4></p>
		<?php
		error_reporting( E_ERROR | E_PARSE );
		$tanggalve = $show_news[0]->create_at;
		$tahun_1 = substr( $tanggalve, 0, 4 );
		$bulan_1 = substr( $tanggalve, 5, 2 );
		$tgl_1 = substr( $tanggalve, 8, 2 );
		$result_new = $tahun_1 . "-" . $bulan_1 . "-" . $tgl_1;
		$tanggalva = $show_news[0]->last_update;
		$tahun_2 = substr( $tanggalva, 0, 4 );
		$bulan_2 = substr( $tanggalva, 5, 2 );
		$tgl_2 = substr( $tanggalva, 8, 2 );
		$result_last = $tahun_2 . "-" . $bulan_2 . "-" . $tgl_2;
		$tanggalrawcreate = strtotime( $show_news[0]->create_at );
		$tanggalrawlast = strtotime( $show_news[0]->last_update );
		$date2 = strtotime(date('Y-m-d H:i:s'));
		$seconds_diff = $date2 - $tanggalrawcreate;
		$seconds_diff1 = $date2 - $tanggalrawlast;
		if(!isset($show_news[0]->last_update)){
			if ($seconds_diff >= 31536000) {
				$seen=intval($seconds_diff / 31536000) . " tahun lalu";
			} elseif ($seconds_diff >= 2419200) {
				$seen=intval($seconds_diff / 2419200) . " bulan lalu";
			} elseif ($seconds_diff >= 86400) {
				$seen=intval($seconds_diff / 86400) . " hari lalu";
			} elseif ($seconds_diff >= 3600) {
				$seen= intval($seconds_diff / 3600) . " jam lalu";
			} elseif ($seconds_diff >= 60) {
				$seen= intval($seconds_diff / 60) . " menit lalu";
			} else {
				$seen= "kurang dari semenit lalu";
			}
		}
		if(isset($show_news[0]->last_update)){
			if ($seconds_diff1 >= 31536000) {
				$saw="Diperbaharui ". intval($seconds_diff1 / 31536000) . " tahun lalu";
			} elseif ($seconds_diff1 >= 2419200) {
				$saw="Diperbaharui ". intval($seconds_diff1 / 2419200) . " bulan lalu";
			} elseif ($seconds_diff1 >= 86400) {
				$saw="Diperbaharui ". intval($seconds_diff1 / 86400) . " hari lalu";
			} elseif ($seconds_diff1 >= 3600) {
				$saw= "Diperbaharui ". intval($seconds_diff1 / 3600) . " jam lalu";
			} elseif ($seconds_diff >= 60) {
				$saw= "Diperbaharui ". intval($seconds_diff1 / 60) . " menit lalu";
			} else {
				$saw= "Diperbaharui kurang dari semenit lalu";
			}
		}
		?>
		@foreach($show as $toplist)
		<table style="float: left">
			<tr>
				<th width="150px">
					@if(isset($toplist->last_update))
					<a onclick="window.location='{{ URL::to("/view/{$toplist->id_berita}")}}'">{!! $toplist->judul !!}</a>
					@else
					<a onclick="window.location='{{ URL::to("/view/{$toplist->id_berita}")}}'">{!! $toplist->judul !!}</a>
					@endif
				</th>
			</tr>
			<tr>
				<td colspan="2" align="left">{!!"Telah Dilihat ". $toplist->view." Kali" !!}</td>
			</tr>
		</table>
		<div>
			<table>
				<tr>
					<td>
						@if(isset($toplist->last_update))
						<a onclick="window.location='{{ URL::to("/view/{$toplist->id_berita}")}}'"><img style="max-width: 100%;height: 100px;" src="{{ asset('/upload/Thumbnails/'.$toplist->thumbnails)}}" width="140" class="img-thumbnail" /></a>
						@else
						<a onclick="window.location='{{ URL::to("/view/{$toplist->id_berita}")}}'"><img style="max-width: 100%;height: 100px;" src="{{ asset('/upload/Thumbnails/'.$toplist->thumbnails)}}" width="140" class="img-thumbnail" /></a>
						@endif
					</td>
				</tr>
			</table>
		</div>
		<hr>
		@endforeach
	</div>
</form>
@endsection
