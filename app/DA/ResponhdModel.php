<?php

namespace App\DA;

use Illuminate\Database\Eloquent\Model;
use DB;

class ResponhdModel extends Model
{
    public static function getKeluhan(){
    	return DB::select('SELECT id as id, keluhan as text FROM psb_keluhan_hd');
    }
}
