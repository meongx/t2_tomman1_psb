@extends('layout')
@section('header')
  <!-- <link rel="stylesheet" href="/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" /> -->
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <style type="text/css">
    th{
        text-align: center;
    }
  </style>
@endsection
@section('content')
<div class="panel panel-default" id="info">
    <div class="panel-body">
    <form class="row" style="margin-bottom: 20px;">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search Order" name="q"/>
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </div>
    </form>
    <?php $authLevel = 1 ?>
    @if ($authLevel == '1')
    
    <div class="form-group row">
        <!-- <div class="col-sm-2">
            <a href="/order/input" class="btn btn-info btn-sm" style="margin: 0 -15px;">
                <span class="glyphicon glyphicon-plus"></span>
                <span>Input Order</span>
            </a>
        </div> -->
        <div class="col-sm-1">
            <button class="btn btn-default btn-sm btnFilter" type="button">
                <span class="glyphicon glyphicon-filter">Filter</span>
            </button>
            <button class="btn btn-default btn-sm btnHide" type="button">
                <span class="glyphicon glyphicon-filter">Hide</span>
            </button>
        </div>
      <div class="col-sm-2 filter">
        <input name="sd" type="text" id="sd" class="form-control input-sm" placeholder="Start Date" />
      </div>
      <input name="jenis" type="hidden" id="jenis" class="form-control" value="{{ Request::segment(3) }}" />
      <div class="col-sm-4 filter">
        <input name="sts" type="text" id="sts" class="form-control" placeholder="Status" />
      </div>
      <div class="col-sm-2 input-group filter">
        <span class="input-group-btn">
            <input name="ed" type="text" id="ed" class="form-control input-sm" placeholder="End Date" />
            <button class="btn btn-default btn-sm" type="button" id="btnFilter">
                <span class="glyphicon glyphicon-filter">Filter</span>
            </button>
        </span>
      </div>
    </div>
</div>
</div>
    @endif
    <br />
    @if($matrix)
    <input name="tglnav" id="tglnav" type="hidden" value="{{Request::segment(5)}}"/>
        <div class="panel panel-default" id="info">
            <div class="panel-heading">MATRIK {{ date('Y-m-d') }} <span class="clock">{{ date('H:i:s') }}</span></div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-fixed">
                    <tr>
                        <th>JENIS<br/>ORDER</th>
                        <th>TOMMAN<br/>PSB</th>
                        <th>NO<br/>DISPATCH</th>
                        <th>NO<br/>UPDATE</th>
                        <th>KENDALA<br/>PELANGGAN</th>
                        <th>KENDALA<br/>TEKNIS</th>
                        <th class="align-middle">CLOSE/BLN</th>
                        <!--<th class="align-middle">TOTAL</th>-->
                    </tr>
                    <?php
                        $total = 0;
                        $undisp = 0;
                        $no_update = 0;
                        $kendala_pelanggan = 0;
                        $kendala_teknis = 0;
                        $close = 0;
                        $stotal = 0;
                    ?>
                    @foreach($data as $no => $d)
                        <?php
                        
                            $total = $d->undisp+$d->no_update+$d->kendala_pelanggan+$d->kendala_teknis+$d->close;
                            $undisp += $d->undisp;
                            $no_update += $d->no_update;
                            $kendala_pelanggan += $d->kendala_pelanggan;
                            $kendala_teknis += $d->kendala_teknis;
                            $close += $d->close;
                            $stotal += $total;
                        ?>
                        <tr>
                            <td class="col-md-5"><a href="#" class="btnnewrow" data-jenisorder="{{ $d->id }}" data-datel="all">{{ $d->jenis_order }}</a><a href="#" class="btnrmrow">{{ $d->jenis_order }}</a>
                                <!-- @if($d->jenis_order == "REMO")
                                    <a href="/listRemo/all">Lookup</a>
                                @endif -->
                            </td>
                            @if($d->id == 3)
                                <td class="col-md-1"><a href="/marina/listtommanpsb/2"> {{ $psb->xcheck }}</a></td>
                            @elseif($d->id == 9)
                                <td class="col-md-1"><a href="/marina/listtommanpsb/24"> {{ $odpfullpsb->xcheck }}</a></td>
                            @elseif($d->id == 6)
                                <td class="col-md-1"><a href="/marina/listtommanpsb/11"> {{ $insertpsb->xcheck }}</a></td>
                            @else
                                <td class="col-md-1"></td>
                            @endif
                            @if($d->jenis_order=="REMO")
                                <td class="col-md-1"><a href="/listRemo/sisa">{{ $d->order_remo }}</a></td>
                            @else
                                <td class="col-md-1"><a href="/order/{{$d->id}}/undisp/all/all">{{ $d->undisp }}</a></td>
                            @endif
                            <td class="col-md-1"><a href="/marina/order/{{$d->id}}/null/all/all">{{ $d->no_update }}</a></td>
                            <td class="col-md-1"><a href="/marina/order/{{$d->id}}/kendala pelanggan/all/all">{{ $d->kendala_pelanggan }}</a></td>
                            <td class="col-md-1"><a href="/marina/order/{{$d->id}}/kendala teknis/all/all">{{ $d->kendala_teknis }}</a></td>
                            <td class="col-md-1"><a href="/marina/order/{{$d->id}}/close/{{Request::segment(5)}}/all">{{ $d->close }}</a></td>
                            <!--<td class="col-md-1"><a href="/order/{{$d->id}}/all">{{ $total }}</a></td>-->
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2">TOTAL STATUS</td>
                        <td><a href="/marina/order/all/undisp/all">{{ $undisp }}</a></td>
                        <td><a href="/marina/order/all/null/all">{{ $no_update }}</a></td>
                        <td><a href="/marina/order/all/kendala pelanggan/all">{{ $kendala_pelanggan }}</a></td>
                        <td><a href="/marina/order/all/kendala teknis/all">{{ $kendala_teknis }}</a></td>
                        <td><a href="/marina/order/all/close/{{Request::segment(5)}}">{{ $close }}</a></td>
                        <!--<td><a href="/order/all/all">{{ $stotal }}</a></td>-->
                    </tr>
                </table>
            </div>
        </div>
         <script type="text/javascript">
            $(function() {
                setInterval(function() {

                  var currentTime = new Date();
                  var hours = currentTime.getHours();
                  var minutes = currentTime.getMinutes();
                  var seconds = currentTime.getSeconds();

                  // Add leading zeros
                  hours = (hours < 10 ? "0" : "") + hours;
                  minutes = (minutes < 10 ? "0" : "") + minutes;
                  seconds = (seconds < 10 ? "0" : "") + seconds;

                  // Compose the string for display
                  var currentTimeString = hours + ":" + minutes + ":" + seconds;

                  $(".clock").html(currentTimeString);

                }, 1000);
            })
        </script>
    @else
        <div class="panel panel-default" id="info">
            <div class="panel-heading">LIST</div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped" id="table">
                    <tr>
                        <th>#</th>
                        <th>NO TIKET</th>
                        <th>IMPACT</th>
                        <th>ACTION</th>
                        <th>UMUR</th>
                        <th>SELESAI</th>
                        <th><input name="fregu" type="fregu" id="fregu" class="form-control" placeholder="REGU"/></th>
                        <th>STO</th>
                        <th><input name="fsto" type="text" id="fsto" class="form-control" placeholder="DATEL"/></th>
                        <th>ORDER DARI</th>
                    </tr>

                    @foreach($data as $no => $d)
                        <?php
                            $dteStart = new \DateTime($d->created_at);
                            if($d->tgl_selesai)
                                $dteEnd   = new \DateTime($d->tgl_selesai);
                            else
                                $dteEnd   = new \DateTime(date('Y-m-d H:i:s'));
                            $dteDiff  = $dteStart->diff($dteEnd); 
                            $durasi   = $dteDiff->format("%Dd %Hh %im");
                        ?>
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td><a href="/marina/tech/{{ $d->id }}">{{ $d->no_tiket }}</a><br/><span class="label label-primary">{{ $d->nama_dp or $d->nama_odp }}</span>
                                @if($d->status != 'close')
                                <br/>
                                <span>
                                    <button class="btn btn-xs btn-success button_refer" id-mt="{{ $d->id_mt }}">Refer</button>
                                    <button class="btn btn-xs btn-success button_check" id-disp="{{ $d->id_mt }}">{{ empty($d->dispatch_regu_id) ? 'Dispatch' : 'Re-Dispatch' }}</button>
                                    <button class="btn btn-xs btn-danger button_delete" id-mt="{{ $d->id_mt }}">Delete</button>
                                </span>
                                @endif
                                <br/><span class="label label-primary">{{ $d->nama_order }}</span>
                            </td>
                            <td><p class="anak_tiket" id-mtt="{{ $d->id_mt }}" style="color:#0ce3ac;text-decoration:underline;">{{ $d->jml_anak }} Tiket</p>
                                <div id-mtt="{{ $d->id_mt }}"></div></td>
                            <td>{{ $d->action }}</td>
                            <td>{{ $d->created_at }}/{{ $durasi }}</td>
                            <td>{{ $d->tgl_selesai }}</td>
                            <td>{{ $d->dispatch_regu_name }}<br/><span class="label label-success">{{ $d->nama1 }}&{{ $d->nama2 }}</span></td>
                            <td>{{ $d->sto }}</td>
                            <td>{{ $d->kandatel }}</td>
                            <td>{{ $d->order_from }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div id="mapModal" class="modal fade">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Kirim Tiket <span id="wo" class="label label-primary"></span> ke Teknisi
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div style="height:350px;">
                            <input name="id_maint" type="hidden" id="id_maint" class="form-control" rows="1" value="" />
                            <div class="form-group">
                                <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Order ID</label>
                                <input name="no_tiket" id="no_tiket" class="form-control input-sm" rows="1" value="" readonly/>
                            </div>
                            <div class="form-group">
                                <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Regu</label>
                                <input name="regu" id="regu" class="form-control" rows="1" value="" placeholder="Pilih Regu"/>
                            </div>
                            <div class="form-group">
                                <label for="sto" class="col-sm-4 col-md-3 control-label">STO</label>
                                <input name="sto" type="text" id="sto" class="form-control" value=""/>
                            </div>
                            <div class="form-group">
                                <label for="odp" class="col-sm-4 col-md-3 control-label">ODP</label>
                                <input name="odp" id="odp" class="form-control input-sm" rows="1" value="" readonly/>
                            </div>
                            <div class="form-group">
                                <label for="koordinat" class="col-sm-4 col-md-3 control-label">Koordinat</label>
                                <input name="koordinat" id="koordinat" class="form-control input-sm" rows="1" value="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
                        <button id="hideButton" id-dispatch="" class="btn btn-primary btn-sm">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="referModal" class="modal fade">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <form method="post" action="/marina/refer">
                        <div class="modal-header">
                            <button class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">
                                Refer Tiket <span class="label label-primary wo"></span>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div style="height:350px;">
                                <input name="id_maint" type="hidden" class="form-control id_maintenance" rows="1" value="" />
                                <div class="form-group">
                                    <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Order ID</label>
                                    <input name="no_tiket" id="tiket_no" class="form-control input-sm" rows="1" value="" readonly/>
                                </div>
                                <div class="form-group">
                                    <label for="refer" class="col-sm-4 col-md-3 control-label">Refer ke</label>
                                    <input name="refer" id="refer" class="form-control" rows="1" value="" placeholder="Pilih Tiket Induk"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
                            <button id-dispatch="" class="btn btn-primary btn-sm">OK</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                var nt = 0;
                var id_maintenance = 0;
                var sto = 0;
                var tim = 0;
                $("#fsto").change(function() {
                  var input, filter, table, tr, td, i;
                  input = document.getElementById("fsto");
                  filter = input.value.toUpperCase();
                  table = document.getElementById("table");
                  tr = table.getElementsByTagName("tr");

                  // Loop through all table rows, and hide those who don't match the search query
                  for (i = 0; i < tr.length; i++) {
                    if(filter=="ALL"){
                        tr[i].style.display = "";
                    }else{
                        td = tr[i].getElementsByTagName("td")[8];
                        if (td) {
                          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                          } else {
                            tr[i].style.display = "none";
                          }
                        } 
                    }
                  }
                });
                $("#fregu").change(function() {
                  var input, filter, table, tr, td, i;
                  input = document.getElementById("fregu");
                  filter = input.value.toUpperCase();
                  table = document.getElementById("table");
                  tr = table.getElementsByTagName("tr");

                  // Loop through all table rows, and hide those who don't match the search query
                  for (i = 0; i < tr.length; i++) {
                    if(filter=="ALL"){
                        tr[i].style.display = "";
                    }else{
                        td = tr[i].getElementsByTagName("td")[6];
                        if (td) {
                          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                          } else {
                            tr[i].style.display = "none";
                          }
                        } 
                    }
                  }
                });
                $("#hideButton").hide();
                $(".button_check").click(function(event) {
                    nt = this.getAttribute("id-disp");
                    $("#sto").val(0).trigger('change');
                    $("#hideButton").attr("id-dispatch", nt);
                    $.getJSON( "/marina/jsonOrder/"+nt, function(data) {
                        id_maintenance = data.id;
                        tim = data.dispatch_regu_id;
                        sto = data.sto;
                        $("#id_maint").val(data.id);
                        $("#wo").html(data.nama_order);
                        $("#no_tiket").val(data.no_tiket);
                        $("#regu").val(data.dispatch_regu_id).trigger('change');
                        $("#sto").val(data.sto).trigger('change');
                        if(data.koordinat)
                            $("#koordinat").val(data.koordinat);
                        else
                            $("#koordinat").val(data.kordinat_odp);
                        if(data.nama_dp)
                            $("#odp").val(data.nama_dp);
                        else
                            $("#odp").val(data.nama_odp);
                    })
                    .done(function(data) {
                        $("#hideButton").show();
                    });
                    
                    $('#mapModal').modal({show:true});
                });
                $(".button_refer").click(function(event) {
                    nt = this.getAttribute("id-mt");
                    $("#tiket_no").val(nt);
                    $.getJSON( "/marina/jsonOrder/"+nt, function(data) {
                        $(".id_maintenance").val(data.id);
                        $(".wo").html(data.no_tiket);
                    });
                    var mt = <?= json_encode($mt) ?>;
                    $('#refer').select2({
                        data : mt,
                        formatResult: function(mt) {
                            return    '<span class="label label-default">'+mt.text+'</span>'+mt.nama_order+'<br><span class="label label-primary">'+mt.status+'</span>'+'<br><span class="label label-primary">'+mt.nama_odp+'</span>';
                        }
                    });
                    $('#referModal').modal({show:true});
                });
                

                $("#hideButton").click(function(event) {
                    tim = $("#regu").val();
                    sto = $("#sto").val();
                    $.ajax({
                      type: "POST",
                      url: "/marina/dispatchOrder",
                      data: { id_mt: id_maintenance, regu: tim, sto: sto},
                      dataType: "html"
                    }).done(function(data) {
                        $("button[id-disp='"+nt+"']").parent().html(data);
                    });
                    $('#mapModal').modal('toggle');
                });
                var data = <?= json_encode($regu) ?>;
                var combo = $('#regu').select2({
                    data: data,
                    formatResult: function(data) {
                        return    '<span class="label label-default">'+data.text+'</span>'+
                                '<strong style="margin-left:5px">'+data.nama1+'&'+data.nama2+'</strong>';
                    }
                });
                var data = <?= json_encode($sto) ?>;
                var sto = $('#sto').select2({
                    data: data
                });
                var data = <?= json_encode($datel) ?>;
                data.push({"id":"all", "text":"ALL"});

                var fsto = $('#fsto').select2({
                    data: data
                });
                var data = <?= json_encode($regu) ?>;
                data.push({"id":"all", "text":"ALL"});

                var fregu = $('#fregu').select2({
                    data: data
                });
                $(".button_delete").click(function(event) {
                    mt = this.getAttribute("id-mt");
                    var r = confirm("Yakin lah pian handak menDELETE order ngini?"+mt);
                    if (r == true) {
                        $.ajax({
                          type: "POST",
                          url: "/deleteOrder",
                          data: { id_mt: mt},
                          dataType: "html"
                        }).done(function(data) {
                           $("button[id-disp='"+mt+"']").parent().html(data);
                        });
                    }
                });

            });
            
        </script>
    @endif
    
@endsection
@section('plugins') 
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
    <!--<script src="/bower_components/select2/select2.min.js"></script> -->
    <script type="text/javascript">
        var shtml="";
        $(function() {
            $(".anak_tiket").click(function() {
                nt = this.getAttribute("id-mtt");
                shtml="";
                $(this).slideUp();
                $.getJSON( "/marina/jsonAnak/"+nt, function(data){
                    $.each(data, function(i, item) {
                        shtml += "<span class='label label-primary'>"+item.no_tiket+"</span><br/>";
                    });
                    $("div[id-mtt='"+nt+"']").html(shtml);
                });
            });
            $('.btnrmrow').hide();
            $(".btnnewrow").click(function(e){
                var $this = $(this);
                var mt = this.getAttribute("data-jenisorder");
                var dd = this.getAttribute("data-datel");
                var tglnav = $('#tglnav').val();
                $.ajax({
                    type: "GET",
                    url: "/marina/ajaxOrder/"+mt+"/"+dd+"/"+tglnav,
                    dataType: "html"
                }).done(function(data) {
                    $('.btnrmrow').show();
                    $('.btnnewrow').hide();
                    $this.closest('tr').after(data);
                });
            });
            $(".btnrmrow").click(function(e){
                $('.ajax').remove();
                $('.btnrmrow').hide();
                $('.btnnewrow').show();
            });
            var data = [{"id":"all", "text":"ALL"},{"id":"close", "text":"close"},{"id":"kendala pelanggan", "text":"kendala pelanggan"},{"id":"kendala teknis", "text":"kendala teknis"}];
            var sts = $('#sts').select2({
                data: data
            });
            var day = {
              format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
            };
            $("#sd").datepicker(day).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
            $("#ed").datepicker(day).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
            $(".filter").hide();
            $(".btnHide").hide();
            $(".btnFilter").click(function() {
                $(".filter").show();
                $(".btnHide").show();
                $(".btnFilter").hide();
            });
            $(".btnHide").click(function() {
                $(".filter").hide();
                $(".btnHide").hide();
                $(".btnFilter").show();
            });
            $("#btnFilter").click(function() {
                var sd = $("#sd").val();
                var jenis = $("#jenis").val();
                var ed = 0;
                if($("#ed").val())
                    ed = $("#ed").val();
                var sts = 0;
                if($("#sts").val())
                    sts = $("#sts").val();
                if(sd){
                    $('.wait-indicator').show();
                    $.get("/marina/filter/"+sd+"/"+ed+"/"+sts+"/"+jenis, function( data ) {
                      $("#info").html(data);
                      $('.wait-indicator').hide();
                    });
                }else{
                    alert("Isi Start Date BosQue!")
                }
            });
        });
    </script>
@endsection