@if (count($dataUndispatch)==0 && count($dataDispatch)==0)
	<h5>Data Tidak Ada</h5>
@else
<table class="table table-bordered">
	<tr>	
		<th>No</th>
		<th>Tgl Order</th>
		<th>Myir / SC</th>
		<th>Tgl Dispatch</th>
		<th>SC Sinkron</th>
		<th>Pelanggaan</th>
		<th>Alamat</th>
		<th>STO</th>
		<th>ODP</th>
		<th>Layanan</th>
		<th>PSB</th>
		<th>Paket Indihome</th>
		<th>Ket Dispatch</th>
		<th>Sales ID</th>		
		<th>Status Laporan</th>
		<th>Ket</th>
	</tr>

	<!-- undispatc -->
	@foreach($dataUndispatch as $no=>$data)
		<tr>	
			<td>{{ ++$no }}</td>
			<td>{{ $data->orderDate }}</td>
			<td>{{ $data->myir }}</td>
			<td>-</td>
			<td>-</td>
			<td>{{ $data->customer }}</td>
			<td>{{ $data->alamatLengkap }}</td>
			<td>{{ $data->sto }}</td>
			<td>{{ $data->namaOdp }}</td>
			<td>{{ $data->layanan }}</td>
			<td>{{ $data->psb ?: '-'}}</td>
			<td>{{ $data->paket_harga ?: '-' }}</td>									
			<td>Belum Terdispatch</td>
			<td>{{ $data->sales_id }}</td>
			<td>-</td>

			@if($data->ket_input==0)
				<td>
					PLASA
					<a href="/dshr/plasa-sales/cetak-plasa/{{ $data->id_wo }}" target="_blank" class="btn btn-info btn-sm">
						<i class="glyphicon glyphicon-print" aria-hidden="true"></i>
					</a>
				</td>
			@else
				<td>SALES</td>
			@endif
		</tr>
	@endforeach

	@foreach($dataDispatch as $no=>$data)
		<tr>	
			<td>{{ ++$no + $jumlahUndispatch }}</td>
			<td>{{ $data->orderDate }}</td>
			<td>{{ $data->myirInput }}</td>
			<td>{{ $data->tglDispatch }}</td>
			<td>{{ $data->Ndem ?: '-'}}</td>
			<td>{{ $data->customer }}</td>
			<td>{{ $data->alamatLengkap }}</td>
			<td>{{ $data->sto_wo }}</td>
			<td>{{ $data->namaOdp }}</td>
			<td>{{ $data->layanan }}</td>
			<td>{{ $data->psb ?: '-'}}</td>
			<td>{{ $data->paket_harga ?: '-' }}</td>									

			<td>
				<label class="label label-danger btn-sm">{{ $data->uraian }} - {{ $data->title }}</label>
			</td>
			
			<td>{{ $data->sales_id }}</td>
			<td>{{ $data->laporan_status ?: 'NEED PROGRESS' }}</td>

			@if($data->ket_input==0)
				<td>
					PLASA
					<a href="/dshr/plasa-sales/cetak-plasa/{{ $data->id_wo }}" target="_blank" class="btn btn-info btn-sm">
						<i class="glyphicon glyphicon-print" aria-hidden="true"></i>
					</a>
				</td>
			@else
				<td>SALES</td>
			@endif
		</tr>
	@endforeach
	
</table>
@endif