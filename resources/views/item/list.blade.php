@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
  <a href="/item/input" class="btn btn-info">
      <span class="glyphicon glyphicon-plus"></span>
  </a>
  List Item</h3>

  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <li class="{{ (Request::path() == 'item') ? 'active' : '' }}"><a href="/item">Active</a></li>
    <li class="{{ (Request::path() == 'item/non-aktif') ? 'active' : '' }}"><a href="/item/non-aktif">Non-active</a></li>
  </ul>

  <div class="list-group">
    @foreach($list as $no => $data)
      <a href="/item/{{ $data->id_item }}" class="list-group-item">
        <span class="label label-info">{{ ++$no }}</span>
        <span>{{ $data->id_item }}</span>
        <span>{{ $data->nama_item }}</span>
        <span class="label label-info badge">{{ $data->unit_item }}</span>
      </a>
    @endforeach
  </div>
@endsection