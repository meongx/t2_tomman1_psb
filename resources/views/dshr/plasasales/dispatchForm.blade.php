@extends('layout')

@section('content')
  @include('partial.alerts')
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmAwGwcaeJKLu7f3Noyhw2ihC8s8aaoPs"></script> -->
  <div class="panel panel-primary">
      <div class="panel-heading">Dispatch {{ $ket=="plasa" ? "Plasa" : "Sales" }} {{ $ket=="plasa" ? "Order" : "Myir" }} - {{ $id }}</div>
      <div class="panel-body">
        <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">   
          <div class="form-group{{ $errors->has('myir') ? 'has-error' : '' }}">
            <label class="control-label" for="myir">MYIR</label>
            <input type="text" name="myir" id="myir" value="{{ $id }}" class="form-control" readonly>
          </div>

          
          <div class="form-group{{ $errors->has('sales') ? 'has-error' : '' }}">
            <label class="control-label" for="sales">{{ $ket=="plasa" ? "Created By" : "Sales"}}</label>
            <input type="text" name="sales" id="sales" value="{{ $getData->sales_id }}" class="form-control" readonly>
          </div>
          
          <div class="form-group{{ $errors->has('nmCustomer') ? 'has-error' : '' }}">
            <label class="control-label" for="myir">Nama Pelanggan</label>
            <input type="text" name="nmCustomer" id="nmCustomer" value="{{ $getData->customer }}" class="form-control" readonly>
          </div>

          <div class="form-group">
              <label class="control-label">Nama ODP</label>
              <input type="text" name="nama_odp" id="odp2" class="form-control" rows="1" value="{{ $getData->namaOdp }}" readonly />
          </div>
            
          <div class="form-group{{ $errors->has('sto') ? 'has-error' : '' }}">
            <label class="control-label" for="myir">STO</label>
            <input type="hidden" name="sto" id="sto" value="{{ $getData->sto }}" class="form-control" readonly>
          </div>

          <div class="form-group">
            <label class="control-label" for="jenis_layanan">Jenis Layanan</label>
            <input name="jenis_layanan" type="text" id="jenis_layanan" class="form-control" value="{{ $getData->layanan }}"  readonly/>
          </div>

          <div class="form-group">
            <label class="control-label" for="input-regu">Regu</label>
            <input name="id_regu" type="hidden" id="input-regu" class="form-control" value="{{ old('id_regu') }}" />
            {!! $errors->first('id_regu', '<span class="label label-danger">:message</span>') !!}
          </div>

          <div class="form-group">
            <label class="control-label" for="sektor">Sektor</label>
            <input name="sektor" type="text" id="sektor" class="form-control" readonly />
          </div>

          @if($getData->dispatch==1)
              <div class="form-group">
                <label class="control-label" for="sektor">Tanggal Re-Dispatch</label>
                <input name="tglRedispatch" type="text" id="tglRedispatch" class="form-control" value="{{ date('Y-m-d') }}"  readonly />
              </div>            
          @endif

          <div class="form-group">
            <label class="control-label" for="input-tgl">Tanggal Order</label>
            <input name="tgl" type="tgl" id="input-tgl" class="form-control" value="{{ $getData->orderDate }}" readonly />
          </div>

          <div class="form-group{{ $errors->has('kordinatPel') ? 'has-error' : '' }}">
            <label class="control-label" for="input-timeslot">Koordinat Pelanggan</label>
            <input type="text" name="kordinatPel" id="kordinatPel" value="{{ $getData->koorPelanggan }}" class="form-control" readonly>
          </div>

          <div class="form-group{{ $errors->has('alamatSales') ? 'has-error' :'' }} ">
            <label class="control-label" for="input-timeslot">Alamat Sales</label>
            <textarea name="alamatSales" id="alamatSales" class="form-control" readonly>{{ $getData->alamatLengkap }}</textarea>
          </div>

          <div class="form-group{{ $errors->has('picPelanggan') ? 'has-error' :'' }} ">
            <label class="control-label" for="input-timeslot">PIC Pelanggan</label>
            <textarea name="picPelanggan" id="picPelanggan" class="form-control" maxlength="14" readonly>{{ $getData->picPelanggan }}</textarea>
          </div>
          
          <input type="hidden" name="ket" value="{{ $getData->ket }}">
          <div style="margin:40px 0 20px">
            <button class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
  </div>
 
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
    $(function() {            
      var dataRegu = <?=json_encode($getRegu); ?>;
      $("#input-regu").select2({
          data: dataRegu,
          placeholder: 'Input Regu'
      })

      $('#input-regu').on('click',function(){
            var id = $('#input-regu').val(),
                url = '/dshr/plasa-sales/getRegu/'+id;

            $.ajax({
                url: url,
                dataType: 'json',
                success: function(data){
                    console.log(data.title);
                    $('#sektor').val(data.title);
                }
            })
      });

      var day = {
              format: 'yyyy-mm-dd',
            viewMode: 0,
            minViewMode: 0
          };
      
      $('#tglRedispatch').datepicker(day).on('changeDate', function(e){
          $(this).datepicker('hide');
      });
    })
  </script>
@endsection
