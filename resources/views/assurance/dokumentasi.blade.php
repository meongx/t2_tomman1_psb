<script src="/js/jquery.min.js"></script>

<div class="panel panel-default" id="dokumentasi">
  <div class="panel-heading">Dokumentasi <?php echo $get_penyebab[0]->kategori ?> </div>
  <div class="panel-body">
    <div class="row text-center input-photos" style="margin: 20px 0">
    <?php foreach ($check_foto as $input) : ?>

                  <div class="col-xs-6 col-sm-2">
                    <?php
                      $path = "/upload3/asurance/$id_dt/$input->name";
                      $th   = "$path-th.jpg";
                      $img  = "$path.jpg";
                      $flag = "";
                      $name = "flag_".$input->name;
                    ?>
                    @if (file_exists(public_path().$th))
                      <a href="<?php echo $img ?>">
                        <img src="<?php echo  $th ?>" alt="<?php echo $input->name ?>" width="100" />
                      </a>
                      <?php
                        $flag = 1;
                      ?>
                    @else
                      <img src="/image/placeholder.gif" width="100" alt="" />
                    @endif
                    <br />
                    <input type="text" class="hidden" name="flag_<?php echo $input->name ?>" value="<?php echo $flag ?>"/>
                    <input type="file" class="hidden" name="photo-<?php $input->name ?>" accept="image/jpeg" />

                    <button type="button" class="btn btn-sm btn-info">
                      <i class="glyphicon glyphicon-camera"></i>
                    </button>
                    <p><?php echo str_replace('_',' ',$input->name) ?></p>
                    {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
                  </div>
    <?php endforeach ?>
  </div>
  </div>
</div>
<script>
$(function(){
  $('input[type=file]').change(function() {
    console.log(this.name);
    var inputEl = this;
    if (inputEl.files && inputEl.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $(inputEl).parent().find('img').attr('src', e.target.result);
      }
      reader.readAsDataURL(inputEl.files[0]);
    }
  });

  $('.input-photos').on('click', 'button', function() {
    $(this).parent().find('input[type=file]').click();
  });
});
