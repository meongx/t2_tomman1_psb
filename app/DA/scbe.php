<?php

namespace App\DA;

use Illuminate\Database\Eloquent\Model;
use DB;

class scbe extends Model
{
     public static function simpanDispatch($req, $status){
    	if ($status=='0'){
            $myirDepan = substr($req->myir, 0, 2);
            $ketMyirGangguan = 0;
            if ($myirDepan=='62'){
                $ketMyirGangguan = 1;
            };

	    	return DB::table('psb_myir_wo')->insert([
	    		'myir'		 => $req->myir,
	    		'customer' 	 => $req->nmCustomer,
	    		'namaOdp'	 => $req->nama_odp,
	    		'ket'		 => '0',
	    		'lokerJns'	 => 'myir',
                'sto'        => $req->sto,
	    		'orderDate'  => DB::raw('NOW()'),
                'picPelanggan'      => $req->picPelanggan,
                'dispatch'          => 1,
                'sales_id'          => $req->sales,
                'ketMyirGangguan'   => $ketMyirGangguan
	    	]);
    	}
    	else {
    		return DB::table('psb_myir_wo')->where('myir',$req->myir)->update([
	    		'customer' 	 => $req->nmCustomer,
	    		'namaOdp'	 => $req->nama_odp,
                'sto'        => $req->sto,
                'picPelanggan'  => $req->picPelanggan,
                'sales_id'      => $req->sales
	    	]);
    	}
    }

    public static function cariMyir($cari){
        $sql = 'SELECT dt.*, dt.id as id_dt, r.uraian, my.*, my.id as idMyir, pls.laporan_status_id, pls.laporan_status, pl.nama_odp as odpByTeknisi
                from
                dispatch_teknisi dt
                    LEFT JOIN psb_myir_wo my ON (dt.Ndem=my.myir OR dt.Ndem=my.sc)
                    LEFT JOIN regu r ON dt.id_regu=r.id_regu
                    LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
                    LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
                where my.myir="'.$cari.'"';

        return DB::select($sql);
    }

    public static function cariMyirStatusSatu($myir){
        $sql = 'SELECT dt.*, r.uraian, pls.laporan_status_id, pls.laporan_status, dps.*, my.myir, my.customer, my.id as idMyir, my.namaOdp, my.sto, my.picPelanggan, pl.nama_odp as odpByTeknisi
                from
                dispatch_teknisi dt
                    LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem=dps.orderId
                    LEFT JOIN regu r ON dt.id_regu=r.id_regu
                    LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
                    LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
                    LEFT JOIN psb_myir_wo my ON dps.orderId=my.sc
                where dps.kcontact like "%'.$myir.'%" ';

        return DB::select($sql);
    }

    public static function getMyirByMyir($myir){
        return DB::table('psb_myir_wo')->select('psb_myir_wo.*', 'psb_myir_wo.id as idMyir')->where('myir',$myir)->first();
    }

    public static function getMyir($id){
         $sql = 'SELECT dt.*, r.uraian, my.myir, my.customer, my.id as idMyir, my.namaOdp, my.sto, my.picPelanggan, r.mainsector, my.sales_id from dispatch_teknisi dt LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir LEFT JOIN regu r ON dt.id_regu=r.id_regu where my.id="'.$id.'"';

        return DB::select($sql);
    }

    public static function getDataStarClickByMyir($myir){
        return DB::table('Data_Pelanggan_Starclick')->where('kcontact', 'LIKE', '%'.$myir.'%')->whereNotIN('orderStatus',['UNSC','REVOKE ORDER UNSC','REVOKE ORDER','CANCEL COMPLETED','Cancel Order'])->first();
    }

    public static function getDataDispatchByNdem($ndem){
        return DB::table('dispatch_teknisi')->where('Ndem',$ndem)->first();
    }

    public static function hapusNdemMyir($myir){
        return DB::table('dispatch_teknisi')->where('Ndem',$myir)->delete();
    }

    public static function hapusMyir($myir){
        return DB::table('psb_myir_wo')->where('myir',$myir)->delete();
    }

    public static function ubahKetScMyir($myir, $sc){
        return DB::table('psb_myir_wo')->where('myir',$myir)->update([
            'ket'   => '1',
            'sc'    => $sc
        ]);
    }

    public static function cekDataMyir($myir){
        return DB::table('psb_myir_wo')->where('myir',$myir)->first();
    }

    public static function getOdpFull($odp){
        $sql = ' SELECT dt.*, pls.laporan_status_id, pls.laporan_status, my.myir, my.customer, my.id as idMyir, my.namaOdp, my.sto, pl.catatan, pl.nama_odp
          from
            dispatch_teknisi dt
          LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
          LEFT JOIN psb_myir_wo my ON my.myir=dt.Ndem
          where
            (pl.nama_odp="'.$odp.'" or my.namaOdp="'.$odp.'") order by dt.updated_at desc limit 1';

        return DB::select($sql);
        // pls.laporan_status_id=24 and  (pl.nama_odp="'.$odp.'" or my.namaOdp="'.$odp.'") order by dt.updated_at desc limit 1';
    }

    public static function updateDispatch($myir){
        return DB::table('psb_myir_wo')->where('myir',$myir)->update(['dispatch' => 1]);
    }

}
