@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    PS REPORT 
    <a href="/ms2n/sync/PS" class="btn btn-info">
      <span class="glyphicon glyphicon-refresh"> Sync Now</span>
    </a>
  </h3>
  <div>
    <div class="input-group">
      <div class="input-group-btn">
        <button type="button" class="btn btn-default aksi" aria-label="Left Align">
          <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
        </button>
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
        <ul class="dropdown-menu">
          <li><a href="#" id="daily">Daily</a></li>
          <li><a href="#" id="monthly">Monthly</a></li>
         
        </ul>
      </div><!-- /btn-group -->
      <div class="ins">
        <input type="text" class="form-control" id="tanggal" value="{{ $tgl_ps }}">
      </div>
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->


  <br/>
  <div class="panel panel-primary">
    <div class="panel-heading">Potensi PS Witel Kalsel {{ $tgl_ps }}</div>
    <div class="panel-body">
      <div class="list-group fiber">
			 <div class="table-responsive">  
				       
			  <table class="table" border="0">
			    <thead>
			      <tr>
			        <th width="20">No</th>
			        <th width="100">Datel</th>
			        <th width="100">PS</th>
			        <th width="100">Sudah Aktif</th>
			        <th width="100">Potensi</th>
			        <th width="100">Total</th>
			      </tr>
			    </thead>
			    <tbody>
						{{-- */
							$total_ps = 0;
							$total_sudah_aktif = 0;
							$total_potensi = 0;
							$total_total = 0;
						/* --}} 
				    @foreach($get_ps_harian as $no => $ps_harian) 
				    {{-- */$total_area = $ps_harian->area + $ps_harian->PS + $ps_harian->Sudah_Aktif + $ps_harian->Potensi;/* --}}
			      <tr>
			        <td>{{ ++$no }}</td>
			        <td>{{ $ps_harian->area }}</td>
			        <td>{{ $ps_harian->PS }}</td>
			        <td><a href="/sudah_aktif/{{ $ps_harian->area }}">{{ $ps_harian->Sudah_Aktif }}</a></td>
			        <td><a href="/potensi/{{ $ps_harian->area }}">{{ $ps_harian->Potensi }}</a></td>
			        <td>{{ $total_area }}</td>
			      </tr>
			      {{-- */
							$total_ps = $total_ps + $ps_harian->PS;
							$total_sudah_aktif = $total_sudah_aktif + $ps_harian->Sudah_Aktif;
							$total_potensi = $total_potensi + $ps_harian->Potensi;
							$total_total = $total_total + $total_area;
						/* --}} 
			      @endforeach
			    </tbody>
							<tr>
			        <th width="100" colspan="2">Total</th>
			        <th width="100">{{ $total_ps }}</th>
			        <th width="100"><a href="/sudah_aktif/ALL">{{ $total_sudah_aktif }}</a></th>
			        <th width="100"><a href="/potensi/ALL">{{ $total_potensi }}</a></th>
			        <th width="100">{{ $total_total }}</th>
			      </tr>
			  </table>
			 </div>
      </div>
    </div>
  </div>
    <div class="panel panel-primary">
    <div class="panel-heading">Potensi PS DSHR Kalsel {{ $tgl_ps }}</div>
    <div class="panel-body">
      <div class="list-group fiber">
			 <div class="table-responsive">  
				       
			  <table class="table" border="0">
			    <thead>
			      <tr>
			        <th width="20">No</th>
			        <th width="100">Datel</th>
			        <th width="100">PS</th>
			        <th width="100">Sudah Aktif</th>
			        <th width="100">Potensi</th>
			        <th width="100">Total</th>
			      </tr>
			    </thead>
			    <tbody>
						{{-- */
							$total_ps = 0;
							$total_sudah_aktif = 0;
							$total_potensi = 0;
							$total_total = 0;
						/* --}} 
				    @foreach($get_ps_harian_dshr as $no => $ps_harian_dshr) 
				    {{-- */$total_area = $ps_harian_dshr->area + $ps_harian_dshr->PS + $ps_harian_dshr->Sudah_Aktif + $ps_harian_dshr->Potensi;/* --}}
			      <tr>
			        <td>{{ ++$no }}</td>
			        <td>{{ $ps_harian_dshr->area }}</td>
			        <td>{{ $ps_harian_dshr->PS }}</td>
			        <td><a href="/sudah_aktif/{{ $ps_harian_dshr->area }}">{{ $ps_harian_dshr->Sudah_Aktif }}</a></td>
			        <td><a href="/potensi/{{ $ps_harian_dshr->area }}">{{ $ps_harian_dshr->Potensi }}</a></td>
			        <td>{{ $total_area }}</td>
			      </tr>
			      {{-- */
							$total_ps = $total_ps + $ps_harian_dshr->PS;
							$total_sudah_aktif = $total_sudah_aktif + $ps_harian_dshr->Sudah_Aktif;
							$total_potensi = $total_potensi + $ps_harian_dshr->Potensi;
							$total_total = $total_total + $total_area;
						/* --}} 
			      @endforeach
			    </tbody>
							<tr>
			        <th width="100" colspan="2">Total</th>
			        <th width="100">{{ $total_ps }}</th>
			        <th width="100"><a href="/sudah_aktif/ALL">{{ $total_sudah_aktif }}</a></th>
			        <th width="100"><a href="/potensi/ALL">{{ $total_potensi }}</a></th>
			        <th width="100">{{ $total_total }}</th>
			      </tr>
			  </table>
			 </div>
      </div>
    </div>
  </div>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />
  <script>
    $(function(){
      var day = {
        format: "yyyy-mm-dd",
        viewMode: 0, 
        minViewMode: 0
      };
      var month = {
        format: "yyyy-mm",
        viewMode: 2, 
        minViewMode: 1
      };
      $("input").datepicker(day)
      	.on('changeDate',function(e){
	      	document.location = '/ms2n/PS/'+$(this).val();
	      });



      $("#daily").click(function(){
        $("input").remove();
        $(".ins").after("<input type=text class=form-control id=tanggal>");
        $("input").datepicker(day);
      });

      $("#monthly").click(function(){
        $("input").remove();
        $(".ins").after("<input type=text class=form-control id=tanggal>");
        $("input").datepicker(month);
      });
      $(".aksi").click(function(){
        var tgl = document.getElementById("tanggal").value;
        if(tgl == ""){
          alert("Can you please select the date? or i will smash u down!");
        }else{
          $(".loading").css({"visibility":"visible"});
          $.getJSON( "/ms2n/getJsonPs/" + tgl, function( data ) {
            var fiber = [];
            var dslam = [];
            var msan = [];
            $.each( data, function( key, val) {
              fiber.push( "<li class='list-group-item'>" + val.Nama + "</li>" );
              console.log(val);
            });
            $(".list-group").html(fiber);
            $(".loading").css({"visibility":"hidden"});
          });
        }
      });
        
    });
  </script>
@endsection
