@extends('layout')

@section('content')
  @include('partial.alerts')

  <form method="POST" action="{{ route('approve.massal') }}">
    <div class="row">
      <div class="col-md-12" id="bio_tl">
        <div class="panel panel-default">
            <div class="panel-heading">
            Profile
            </div>
            <div class="panel-body table-responsive">
              <table style="padding:10px;">
                <tr>
                  <td style="padding:15px">
                    <img src="/image/people.png" width="100" />
                  </td>
                  <td style="padding:10px">

                    {{ session('auth')->nama }}<br />
                    @foreach ($group_telegram as $gt)
                    <span class="label label-info">{{ $gt->title }}</span>
                    @endforeach
                    <br />
                    <small>
                      Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                    </small>
                  </td>
                  <td style="padding : 10px" valign=top>

                  </td>
                </tr>

              </table>
              @include ('partial.home')

                    <?php
                      $nama_tim = ''; 
                      $jumlah_tim = 0;
                      $jumlah_teknisi = 0;
                    ?>
                    <table class="table">
                      <tr>
                        <td width=10>No</td>
                        
                        @if(session('auth')->level==46 || session('auth')->level==2 )
                          <td><input type="submit" class="btn btn-primary btn-xs" value="Approve Cheklist"></td>
                        @endif
                        
                        <td>NIK</td>
                        <td>Laborcode</td>
                        <td>Name</td>
                        <td>Squad</td>
                        <td>Team</td>
                        <td>Kehadiran</td>
                        <td>Waktu Absen</td>
                        <td>Status Approval</td>
                        <td>Waktu Approval</td>
                        @if (session('auth')->id_user=='88159353' || session('auth')->id_user=='720428')
                          <td>TL</td>
                          <td>Sektor</td>
                        @endif
                      </tr>
                      <!-- <?php print_r($data_team); ?> -->
                      @if (count($active_team)>0)
                    
                    @foreach ($active_team as $num=>$at)
                      <tr>
                        <td>{{ ++$num }}.</td>
                        
                        <td>
                            @if ($at->status_kehadiran=="HADIR")
                              <?php
                                  $disable = 'disabled';
                                  if ($at->approval==0){
                                      $disable = '';
                                  }
                               ?>  
                              <center><input type="checkbox" name="approveMassal[]" value="{{ $at->nik }}" {{ $disable }}></center>
                            @endif
                        </td>

                        <td>{{ $at->NIK  }}</td>
                        <td>{{ $at->laborcode  }}</td>
                        <!-- <td><a class="label label-info" href="/employee/{{ $at->id_people }}">{{ $at->nama  }}</a></td> -->
                        <td><a class="label label-info">{{ $at->nama  }}</a></td>
                        <td><a class="label label-info" href="/updatesquad/{{ $at->NIK }}">{{ $at->squad ? : 'NOTSET' }}</a></td>
                        <td>

                          @if (!empty($data_team[$at->NIK]))
                            @foreach ($data_team[$at->NIK] as $team)

                                  <!-- <a class="label label-info" href="/team/{{ $team->id_regu }}">{{ $team->uraian }} // {{ $team->crewid ? : 'EMPTY' }}</a> -->

                                   <a class="label label-info">{{ $team->uraian }} // {{ $team->crewid ? : 'EMPTY' }}</a>
                            @endforeach
                          @endif
                        </td>
                        <td>
                          @if ($at->status_kehadiran=="HADIR")
                          <span class="label label-success">HADIR</span>
                          <br />
                            @for ($i=0;$i<count(@$data_alker[$at->NIK]);$i++)

                            <span class="label label-info">{{ @$data_alker[$at->NIK][$i] }}</span>
                            @endfor
                          @else
                          <span class="label label-danger">TIDAK HADIR</span>
                          @endif
                        </td>
                        <td>
                          <span class="label label-default">{{ $at->waktu_absen }}</span>
                        </td>
                        <td>
                          @if ($at->status_kehadiran=="HADIR")
                            @if ($at->approval==0)
                              <a href="/home/absen/approve/{{ $at->NIK }}/{{ date('Y-m-d') }}" class="label label-default">APPROVE</a>
                              <a href="/home/absen/decline/{{ $at->absen_id }}" class="label label-danger">DECLINE</a>
                            @elseif ($at->approval==3)
                              <span class="label label-warning">DECLINED</span>
                            @else
                              <span class="label label-success">APPROVED</span>
                            @endif
                          @endif
                        </td>
                        <td>
                          <span class="label label-default">{{ $at->date_approval }}</span>
                        </td>

                        @if (session('auth')->id_user=='88159353' || session('auth')->id_user=='720428')
                          <td><span class="label label-primary">{{ $at->TL }}</span></td>
                          <td><span class="label label-primary">{{ $at->title }}</span></td>
                        @endif
                      </tr>
                    @endforeach
                    @endif
                  </table>

            </div>
        </div>
      </div>
      </div>
  </form>
@endsection
