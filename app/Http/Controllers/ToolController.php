<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\Tool;
use DB;
use Excel;

class ToolController extends Controller
{
    protected $photoInputsPs = [ 
      'Lokasi','Berita_Acara', 'Telephone_Incoming', 'Telephone_Outgoing', 'Wifi_Analyzer', 'LiveTV'
    ];

    protected $photoInputs = [
      'Lokasi','Berita_Acara'
    ];

    public function logistik(Request $req, Tool $tool)
    {
    	$project = $tool->getProject();
    	$gudang  = $tool->getGudang();
    	$mitra   = $tool->getMitra();
    	$data 	 = new \stdClass();   

    	return view('tool.logistik',compact('project', 'gudang', 'mitra', 'data'));
    }

    public function logistikProses(Request $req, Tool $tool)
    {
      // dd($req->mitra);
      $mitras=''; $gudangs='';
      if ($req->mitra[0]<>null){
          $listMitra = explode(',', $req->mitra[0]);
          $mitras='';
          foreach($listMitra as $list){
              $mitras .= "'$list'".',';
          }
          $mitras = substr($mitras, 0, -1);
          $mitras .= '';
      }

      if ($req->nama_gudang[0]<>null){
          $listGudang = explode(',', $req->nama_gudang[0]);
          $gudangs='';
          foreach($listGudang as $list){
              $gudangs .= "'$list'".',';
          }
          $gudangs = substr($gudangs, 0, -1);
          $gudangs .= '';
      }

      $sql = '';
      if($req->proaktif_id)
          $sql.='project = "'.$req->proaktif_id.'" and ';

      if ($req->nama_gudang[0]<>null){
          $sql.='nama_gudang in ('.$gudangs.') and ';
      }

      if ($req->mitra[0]<>null){
          $sql.='mitra in ('.$mitras.') and ';
      }


      if($req->tgl)
          $sql.='tgl like "%'.$req->tgl.'%" and ';

      if ($req->noRfc)
          $sql.='no_rfc like "%'.$req->noRfc.'%" and ';

      // if($req->nama_gudang)
      //     $sql.='nama_gudang = "'.$req->nama_gudang.'" and ';

      // if($req->mitra)
      //     $sql.='mitra in ('.$mitra.') and ';

      $sql = substr($sql, 0, -5);

      $data = array();
      $dataGudang     = DB::select('select * from alista_material_keluar where '.$sql);
      foreach($dataGudang as $gudang){
          $register = 0; $terpakai = 0; $kembali = 0;
          $dataJumlah = DB::table('rfc_sal')->where('alista_id',$gudang->alista_id)->where('id_item',$gudang->id_barang)->first();
          if (count($dataJumlah)){
              $register = $dataJumlah->jumlah;
              $terpakai = $dataJumlah->jmlTerpakai;
              $Kembali  = $dataJumlah->jmlKembali;
          };
          $getData = array('register' => $register, 'terpakai'  => $terpakai, 'kembali' => $kembali);
          $data[]  = array_merge_recursive((array)$gudang, $getData);
      }
      // $data     = DB::select('SELECT * from rfc_sal a LEFT JOIN rfc_input b ON b.alista_id=a.alista_id WHERE '.$sql);
      $project  = DB::select('SELECT project as id, project as text FROM alista_material_keluar GROUP BY project ORDER BY project');
      $gudang   = DB::select('SELECT nama_gudang as id, nama_gudang as text FROM alista_material_keluar GROUP BY nama_gudang ORDER BY nama_gudang');
      $mitra    = DB::select('SELECT mitra as id, mitra as text FROM alista_material_keluar GROUP BY mitra ORDER BY mitra');

    	$session = $req->session();
  		$session->put('proaktif_id', $req->proaktif_id);
  		$session->put('nama_gudang', $gudangs);
  		$session->put('mitra', $mitras);
      $session->put('tgl', $req->tgl);
      $session->put('noRfc', $req->noRfc);
      // $session->put('nama_gudang', $req->nama_gudang);
      // $session->put('mitra', $req->mitra);
      // dd(session('nama_gudang'));
      // dd(count($data));
    	return view('tool.logistik',compact('project', 'gudang', 'mitra', 'data'));
    }

    public function exsportExcelAlista(Request $req, Tool $tool)
    {	
       if (session('mitra')<>''){
          $listMitra = explode(',', session('mitra'));
          $mitras='';
          foreach($listMitra as $list){
              $mitras .= "$list".',';
          }

          $mitras = substr($mitras, 0, -1);
          $mitras .= '';
      }

      if (session('nama_gudang')<>''){
          $listGudang = explode(',', session('nama_gudang'));
          $gudangs='';
          foreach($listGudang as $list){
              $gudangs .= "$list".',';
          }
          $gudangs = substr($gudangs, 0, -1);
          $gudangs .= '';
      }

      $sql = '';
      if(session('proaktif_id'))
          $sql.='project = "'.session('proaktif_id').'" and ';

      if(session('nama_gudang'))
          $sql.='nama_gudang in ('.$gudangs.') and ';

      if(session('mitra'))
          $sql.='mitra in ('.$mitras.') and ';

      if(session('tgl'))
          $sql.='tgl like "%'.session('tgl').'%" and ';

      if(session('noRfc'))
          $sql.='no_rfc like "%'.session('noRfc').'%" and ';

      $sql       = substr($sql, 0, -5);
      // $datas     = DB::select('select * from alista_material_keluar where '.$sql);  
      // $datas     = DB::select('SELECT * from rfc_sal a LEFT JOIN rfc_input b ON b.alista_id=a.alista_id WHERE '.$sql);

      $dataGudang     = DB::select('select * from alista_material_keluar where '.$sql);
      foreach($dataGudang as $gudang){
          $register = 0; $terpakai = 0; $kembali = 0;
          $dataJumlah = DB::table('rfc_sal')->where('alista_id',$gudang->alista_id)->where('id_item',$gudang->id_barang)->first();
          if (count($dataJumlah)){
              $register = $dataJumlah->jumlah;
              $terpakai = $dataJumlah->jmlTerpakai;
              $Kembali  = $dataJumlah->jmlKembali;
          };
          $getData = array('register' => $register, 'terpakai'  => $terpakai, 'kembali' => $kembali);
          $datas[]  = array_merge_recursive((array)$gudang, $getData);
      }

      
    	// $datas = $tool->getMaterialKeluar(session('proaktif_id'), session('nama_gudang'), session('mitra'), session('tgl'));

    	Excel::create("List Material - ".session('noRfc'), function($excel) use ($datas) {
            //  set property
            $excel->setTitle("Data Project");
              
            $excel->sheet('List Material', function($sheet) use ($datas){
                $sheet->mergeCells('A1:M1');
                $sheet->cells('A1', function ($cells) use ($datas){
                    $cells->setFontWeight('bold');
                    $cells->setValue('List Material');
                });

                $row = 3;
                $sheet->row($row, [
                    'Alista ID',
                    'Tanggal Pengeluaran',
                    'Nama Gudang',
                    'Project',
                    'Pengambil',
                    'Mitra',
                    'ID Barang',
                    'Nama Barang',
                    'Jumlah',
                    'Registered',
                    'Terpakai',
                    'Kembali',
                    'No. RFC'
                  ]);

                foreach($datas as $no=>$data){
                  $sheet->row(++$row,[
                      // ++$no,
                      $data['alista_id'],
                      $data['tgl'],
                      $data['nama_gudang'],
                      $data['project'],
                      $data['pengambil'],
                      $data['mitra'],
                      $data['id_barang'],
                      $data['nama_barang'],
                      $data['jumlah'],
                      $data['register'],
                      $data['terpakai'],
                      $data['kembali'],
                      $data['no_rfc']
                    ]);
                }
            });
        })->export('xlsx');
    }

    public function downloadRfc($idpermintaan)
    {
      //echo '<iframe id="iframe1" src="https://alista.telkomakses.co.id/index.php?r=gudang/printoutpengeluaran&id=902762" frameborder="0"></iframe>';
      //die();
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);

      $akun = DB::table('akun')->where('id',8)->first();
      $username = $akun->user;
      $password = $akun->pwd;
      curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$username."&LoginForm%5Bpassword%5D=".$password."&yt0=");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      
      $rough_content = curl_exec($ch);
      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);
      
      $header_content = substr($rough_content, 0, $header['header_size']);
      $body_content = trim(str_replace($header_content, '', $rough_content));
      $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      preg_match_all ($pattern, $header_content, $matches);
      $cookiesOut = $matches['cookie'][1];
      $header['errno'] = $err;
      $header['errmsg'] = $errmsg;
      $header['headers'] = $header_content;
      $header['cookies'] = $cookiesOut;
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/printoutpengeluaran&id='.$idpermintaan);
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      $result = curl_exec($ch);
      $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
      $header = substr($result, 0, $header_size);
      $body = substr($result, $header_size);
      $body = str_replace("images/logotabaru.jpg","https://alista.telkomakses.co.id/images/logotabaru.jpg",$body);
      $sty = '<style type="text/css" media="print">
      @media print
      {
         @page {
           margin-top: 0;
           margin-bottom: 0;
         }
         body  {
           padding-top: 72px;
           padding-bottom: 72px ;
         }
      } 
      </style>';
      echo $sty.$body;
    }

  public function getOrderTarikan($bulan, Tool $tool)
  {
      $data = $tool->getListOrderTarikan($bulan);
      $title = 'List Order Tarikan';
      return view('tool.orderTarikan', compact('data', 'title'));
  }

  public function psbview($tgl){
     $sql = '
        SELECT dps.*, dt.*, pl.* FROM 
          Data_Pelanggan_Starclick dps
          LEFT JOIN dispatch_teknisi dt ON dps.orderId=dt.Ndem
          LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
        WHERE 
          dps.orderDate LIKE "%'.$tgl.'%" AND
          dt.Ndem is not null AND
          pl.status_laporan NOT IN (28,29,30,6,7)
      ';

      $datas = DB::select($sql);
      $photoInputs   = $this->photoInputs;
      $photoInputsPs = $this->photoInputsPs;
      return view('tool.psbview',compact('datas', 'photoInputs', 'photoInputsPs'));
  }

  public function rekapOdp(Request $req){ 
     $sql = '
        SELECT nama_odp, count(*) as jml_pel from psb_laporan where nama_odp in (select DISTINCT nama_odp from psb_laporan where nama_odp is not null and nama_odp<>"") group by nama_odp  order by modified_by desc
     ';

     if ($req->has('q')){
         $cari = $req->input('q');
         $sql = '
            SELECT nama_odp, count(*) as jml_pel from psb_laporan where nama_odp like "%'.$cari.'%"  group by nama_odp
         ';

     }

     $data = DB::select($sql);
     return view('tool.rekapOdp',compact('data'));
  }

  public function rekapOdpPelanggan(Request $req){
     $sql ='
        SELECT
            a.*, d.nama_odp, d.kordinat_pelanggan, d.kordinat_odp, b.uraian, d.created_at as createdPsbLap, d.modified_at as modifiedPsbLap, e.orderName, e.orderNotel, e.ndemSpeedy, e.orderAddr, e.orderKontak, dm.NAMA, mw.Nama as nama_micc, mw.Alamat as alamat_micc, my.customer    
          FROM
            dispatch_teknisi a
          LEFT JOIN regu b ON a.id_regu = b.id_regu
          LEFT JOIN roc c ON a.Ndem = c.no_tiket
          LEFT JOIN roc_active cc ON c.no_tiket = cc.no_tiket
          LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
          LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
          LEFT JOIN dossier_master dm ON a.Ndem = dm.ND
          LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
          LEFT JOIN psb_myir_wo my ON a.Ndem=my.myir and my.ket="0"
          WHERE
            d.nama_odp like "%'.$req->sort.'%"
     ';
     $data = DB::select($sql);
     $odp  = $req->sort;
     return view('tool.listPelangganOdp',compact('data','odp'));
  }

  public function getOrderTarikanGangguan($bulan, Tool $tool)
  {
      $data  = $tool::getListOrderTarikanGangguan($bulan);
      $title = 'List Order Tarikan Gangguan';
      return view('tool.orderTarikanGangguan', compact('data', 'title'));
  }

  public function listHadirProv($date){
    date_default_timezone_set('Asia/Makassar');
  
    $sektor = '';
    $auth   = session('auth');
    if($auth->level==2){
        $sektor = DB::table('group_telegram')
                    ->where('sektor','LIKE','%PROV%')
                    ->get();
    }
    elseif ($auth->level==46){
        $sektor = DB::table('group_telegram')
                    ->where('TL_NIK',$auth->id_user)
                    ->where('sektor','LIKE','%PROV%')
                    ->get();
    };
    $listData  = tool::getAbsenTekProv($date);
    return view('tool.listHadir',compact('sektor', 'listData'));
  }

  public function listMigrasiUp($tgl){
      $sql = 'SELECT a.*, b.*,c.*
              from dossier_master a 
              left join dispatch_teknisi b on b.Ndem=a.ND
              left join psb_laporan c on b.id=c.id_tbl_mj
              where c.status_laporan="1" AND 
              c.modified_at like"%'.$tgl.'%"';
          
      $data = DB::select($sql);
      return view('tool.migrasiUp',compact('data'));
  }

  public function listMaterialStok(){
      $data = tool::getListStokMaterial();
      $title = 'List Stok Material';
      return view('tool.stokMaterial', compact('data', 'title'));
  }

  public function logistikKembali(Request $req, Tool $tool)
  {
    $project = $tool->getProject();
    $gudang  = $tool->getGudang();
    $mitra   = $tool->getMitra();
    $data    = '';

    return view('tool.logistikKembali',compact('project', 'gudang', 'mitra', 'data'));
  }

 public function logistikProsesKembali(Request $req, Tool $tool)
  {
    $mitras=''; $gudangs='';
    if ($req->mitra[0]<>null){
        $listMitra = explode(',', $req->mitra[0]);
        $mitras='';
        foreach($listMitra as $list){
            $mitras .= "'$list'".',';
        }
        $mitras = substr($mitras, 0, -1);
        $mitras .= '';
    }

    if ($req->nama_gudang[0]<>null){
        $listGudang = explode(',', $req->nama_gudang[0]);
        $gudangs='';
        foreach($listGudang as $list){
            $gudangs .= "'$list'".',';
        }
        $gudangs = substr($gudangs, 0, -1);
        $gudangs .= '';
    }

    $sql = '';
    if($req->proaktif_id)
        $sql.='Project = "'.$req->proaktif_id.'" and ';

    if ($req->nama_gudang[0]<>null){
        $sql.='Nama_Gudang in ('.$gudangs.') and ';
    }

    if($req->tgl){
        $sql.='Tgl_Pengembalian like "%'.$req->tgl.'%" and ';
    }

    if($req->noRfc){
        $sql.='NO_RFC = "'.$req->noRfc.'" and ';
    }

    // if($req->nama_gudang)
    //     $sql.='nama_gudang = "'.$req->nama_gudang.'" and ';

    // if($req->mitra)
    //     $sql.='mitra in ('.$mitra.') and ';

    $sql = substr($sql, 0, -5);

    $data     = DB::select('select * from alista_material_kembali where '.$sql);  
    $project  = DB::select('SELECT Project as id, project as text FROM alista_material_kembali GROUP BY Project ORDER BY Project');
    $gudang   = DB::select('SELECT Nama_Gudang as id, Nama_Gudang as text FROM alista_material_kembali GROUP BY Nama_Gudang ORDER BY Nama_Gudang');

    $session = $req->session();
    $session->put('proaktif_id', $req->proaktif_id);
    $session->put('nama_gudang', $gudangs);
    $session->put('tgl', $req->tgl);
    $session->put('noRfc', $req->noRfc);

    // $session->put('nama_gudang', $req->nama_gudang);
    // $session->put('mitra', $req->mitra);
    // dd(session('nama_gudang'));


    return view('tool.logistikKembali',compact('project', 'gudang',  'data'));
  }

  public function exsportExcelAlistaKembali(Request $req, Tool $tool){ 
    if (session('nama_gudang')<>''){
        $listGudang = explode(',', session('nama_gudang'));
        $gudangs='';
        foreach($listGudang as $list){
            $gudangs .= "$list".',';
        }
        $gudangs = substr($gudangs, 0, -1);
        $gudangs .= '';
    }

    $sql = '';
    if(session('proaktif_id'))
        $sql.='Project = "'.session('proaktif_id').'" and ';

    if(session('nama_gudang'))
        $sql.='Nama_Gudang in ('.$gudangs.') and ';


    if(session('tgl')){
        $sql.='Tgl_Pengembalian like "%'.session('tgl').'%" and ';
    }
    
    if(session('noRfc')){
        $sql.='NO_RFC = "'.session('noRfc').'" and ';
    }

    $sql       = substr($sql, 0, -5);
    $datas     = DB::select('select * from alista_material_kembali where '.$sql);  

    // $datas = $tool->getMaterialKeluar(session('proaktif_id'), session('nama_gudang'), session('mitra'), session('tgl'));

    Excel::create("List Material Kembali", function($excel) use ($datas) {
          //  set property
          $excel->setTitle("Data Project");
            
          $excel->sheet('List Material Kembali', function($sheet) use ($datas){
              $sheet->mergeCells('A1:J1');
              $sheet->cells('A1', function ($cells) use ($datas){
                  $cells->setFontWeight('bold');
                  $cells->setValue('List Material Kembali');
              });

              $row = 3;
              $sheet->row($row, [
                  'Id Pengembalian',
                  'Tgl Pengembalian',
                  'Nama Gudang',
                  'Project',
                  'Petugas Gudang',
                  'ID Barang',
                  'Nama Barang',
                  'Jumlah',
                  'Satuan'
                ]);

              foreach($datas as $no=>$data){
                $sheet->row(++$row,[
                    // ++$no,
                    $data->ID_Pengembalian,
                    $data->Tgl_Pengembalian,
                    $data->Nama_Gudang,
                    $data->Project,
                    $data->Petugas_Gudang,
                    $data->ID_Barang,
                    $data->Nama_Barang,
                    $data->jumlah,
                    $data->Satuan
                  ]);
              }
          });
      })->export('xlsx');
  }

  public function logistikPakai(Request $req, Tool $tool){
    $project = $tool->getProject();
    $gudang  = $tool->getGudang();
    $data    = '';

    return view('tool.logistikPakai',compact('project', 'gudang', 'data'));
  }

  public function logistikProsesPakai(Request $req, Tool $tool)
  {
    $gudangs='';
    if ($req->nama_gudang[0]<>null){
        $listGudang = explode(',', $req->nama_gudang[0]);
        $gudangs='';
        foreach($listGudang as $list){
            $gudangs .= "'$list'".',';
        }
        $gudangs = substr($gudangs, 0, -1);
        $gudangs .= '';
    }

    $sql = '';

    if ($req->nama_gudang[0]<>null){
        $sql.='b.gudang in ('.$gudangs.') and ';
    }
   
    if($req->noRFc<>''){
      $sql.='b.no_rfc =  "'.$req->noRFc.'" and ';
    }

    $sql      = substr($sql, 0, -5);
    $ssql     = 'SELECT * from rfc_sal a 
                 LEFT JOIN rfc_input b ON b.no_rfc=a.rfc 
                 WHERE 
                  '.$sql.' Order By b.gudang Asc';

    $data     = DB::select($ssql);
    
    $gudang   = $tool->getGudang();
    $session = $req->session();
    $session->put('rfc', $req->noRFc);
    $session->put('nama_gudang', $gudangs);
  
    return view('tool.logistikPakai',compact('project', 'gudang',  'data'));
  }

  public function exsportExcelAlistaPakai(Request $req, Tool $tool){ 
    $gudangs='';
    if (session('nama_gudang')<>null){
        $listGudang = explode(',', session('nama_gudang'));
        $gudangs='';
        foreach($listGudang as $list){
            $gudangs .= "'$list'".',';
        }
        $gudangs = substr($gudangs, 0, -1);
        $gudangs .= '';
    }

    $sql = '';

    if (session('nama_gudang')<>null){
        $sql.='b.gudang in ('.$gudangs.') and ';
    }
   
    if(session('rfc')<>''){
      $sql.='b.no_rfc =  "'.session('rfc').'" and ';
    }

    $sql      = substr($sql, 0, -5);
    $datas    = DB::select('SELECT * from rfc_sal a LEFT JOIN rfc_input b ON b.no_rfc=a.rfc WHERE '.$sql.' Order By b.gudang Asc');

    Excel::create("List Material Pakai", function($excel) use ($datas) {
          //  set property
          $excel->setTitle("Data Project");
            
          $excel->sheet('List Material Pakai', function($sheet) use ($datas){
              $sheet->mergeCells('A1:J1');
              $sheet->cells('A1', function ($cells) use ($datas){
                  $cells->setFontWeight('bold');
                  $cells->setValue('List Material Pakai');
              });

              $row = 3;
              $sheet->row($row, [
                  'Gudang',
                  'Nik Pemakai',
                  'No RFC',
                  'ID Barang',
                  'Nama Barang',
                  'Jumlah Material',
                  'Jumlah Terpakai',
                  'Saldo Material'
                ]);

              foreach($datas as $no=>$data){
                $sheet->row(++$row,[
                    // ++$no,
                    $data->gudang,
                    $data->nik,
                    $data->rfc,
                    $data->id_item,
                    $data->nama_item,
                    $data->jumlah,
                    $data->jmlTerpakai,
                    $data->jumlah - $data->jmlTerpakai
                  ]);
              }
          });
      })->export('xlsx');
  }

  public function rekapMaterialRfc($bulan){
      $cari = date('m',strtotime($bulan)).'/'.date('Y',strtotime($bulan));
      $data = tool::getRekapRfc($cari);    

      return view('tool.rekapRfc',compact('data')); 
  }

  public function detailRfc($rfc){
      $rfc = str_replace('-', '/', $rfc);
      $data = tool::getByRfc($rfc);

      return view('tool.detail',compact('data'));
  }

  public function rekapMaterialRfcDetail($bulan){
      $cari = date('m',strtotime($bulan)).'/'.date('Y',strtotime($bulan));      
      $data = tool::getRekapRfcDetail($cari);
      
      return view('tool.rekapRfcDetail',compact('data')); 
  }

  public function cekWo($tgl, $nik){
      $sqll = 'SELECT 
                  SUM(CASE WHEN b.status_laporan IN (1,37,38) THEN 1 ELSE 0 END) as jumlahUp,
                  SUM(CASE WHEN b.status_laporan not IN (1,37,38) THEN 1 ELSE 0 END) as jumlahKendala,
                  count(*) totalWo 
              FROM 
                dispatch_teknisi a
              LEFT JOIN data_nossa_1_log dn ON a.Ndem = dn.Incident
              LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
              LEFT JOIN psb_laporan_status e ON b.status_laporan=e.laporan_status_id
              LEFT JOIN regu c ON a.id_regu=c.id_regu
              LEFT JOIN 1_2_employee d ON (c.nik1=d.nik OR c.nik2=d.nik)
              WHERE
                  dn.Incident <> "" AND
                  c.ACTIVE=1 AND
                  DATE(a.tgl) LIKE "%'.$tgl.'%" AND
                  d.nik = "'.$nik.'"
              ';
      $jumlahWo = DB::select($sqll);

      $sql = 'SELECT 
                a.*,
                b.*,
                e.laporan_status,
                d.nik as nikTeknisi,
                d.nama,
                c.uraian
              FROM 
                dispatch_teknisi a
              LEFT JOIN data_nossa_1_log dn ON a.Ndem = dn.Incident
              LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
              LEFT JOIN psb_laporan_status e ON b.status_laporan=e.laporan_status_id
              LEFT JOIN regu c ON a.id_regu=c.id_regu
              LEFT JOIN 1_2_employee d ON (c.nik1=d.nik OR c.nik2=d.nik)
              WHERE
                  dn.Incident <> "" AND
                  c.ACTIVE=1 AND
                  DATE(a.tgl) LIKE "%'.$tgl.'%" AND
                  d.nik = "'.$nik.'"
              ';
      $data = DB::select($sql);

      return view('tool.cekwo',compact('data', 'jumlahWo'));
  }

  public function cekWoDashboard(Request $req){
      $where_sektor = 'AND gt.ketTer=1';
      $tanggalAwal   = date('Y-m-d',mktime('0','0','0',date('m'),1,date('Y')));
      $tanggalAkhir  = date('Y-m-d');

      if ($req->has('tanggalAwal')){
          $tanggalAwal   = $req->input('tanggalAwal');
          $tanggalAkhir  = $req->input('tanggalAkhir');
      };

      $sektorSelect = '';
      if ($req->has('sektor')){
          $where_sektor = ' AND gt.chat_id="'.$req->input('sektor').'"';
          $sektorSelect = $req->input('sektor');
      };

      $where_jenis = ' AND c.sttsWo IN (0,1)';
      if ($req->has('jenis')){
          if ($req->input('jenis')=='psb'){
              $where_jenis = ' AND c.sttsWo IN (1)';
          }
          elseif ($req->input('jenis')=='asr'){
              $where_jenis = ' AND c.sttsWo IN (0)';
          }
          elseif ($req->input('jenis')=='all'){
              $where_jenis = ' AND c.sttsWo IN (0,1)';
          }
      }
      $jenisPil = $req->input('jenis');
      
      $sqll = 'SELECT 
            c.id_regu,
            c.uraian,
            c.mainsector,
            gt.title,
            SUM(CASE WHEN (dps.jenisPsb LIKE "%AO%" OR dps.jenisPsb="AS|VOICE" OR a.dispatch_by=1 OR dm.ketMigrasi=1)  THEN 1 ELSE 0 END) as jumlahAo,
            SUM(CASE WHEN dps.jenisPsb LIKE "%MO%" THEN 1 ELSE 0 END) as jumlahMo,
            SUM(CASE WHEN a.Ndem LIKE "IN%" THEN 1 ELSE 0 END) as jumlahIn,
            count(*) as total
        FROM 
          dispatch_teknisi a
        LEFT JOIN Data_Pelanggan_Starclick dps ON a.Ndem = dps.orderId
        LEFT JOIN data_nossa_1_log dn ON a.Ndem = dn.Incident
        LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
        LEFT JOIN dossier_master dm ON a.Ndem = dm.ND
        LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
        LEFT JOIN psb_laporan_status e ON b.status_laporan=e.laporan_status_id
        LEFT JOIN regu c ON a.id_regu=c.id_regu
        LEFT JOIN group_telegram gt ON c.mainsector=gt.chat_id
        WHERE
          (a.dispatch_by IS NULL OR a.dispatch_by = 2 OR a.dispatch_by = 1) AND
          c.ACTIVE=1 AND
          (DATE(a.tgl) BETWEEN "'.$tanggalAwal.'" AND "'.$tanggalAkhir.'") AND
          e.laporan_status_id IN (1,37,38)
          '.$where_sektor.'
          '.$where_jenis.'
        GROUP BY 
          c.id_regu    
        ORDER BY 
          total DESC
        ';

      $getData = DB::select($sqll);
      $getSektor = array();
      foreach($getData as $data){
        if (!in_array($data->title, $getSektor)){
          array_push($getSektor, $data->title);
        }
      };

      $sektor = DB::select('SELECT chat_id as id, title as text FROM group_telegram WHERE ket_Sektor=1');
      return view('tool.CekWoDashboard',compact('sektor', 'getData', 'tanggalAwal', 'tanggalAkhir', 'sektorSelect','getSektor','jenisPil'));

  }

  public function cekWoDetail($jenis, $tanggalAwal, $tanggalAkhir, $regu){
      $sql = 'SELECT 
              a.*,
              b.*,
              e.laporan_status,
              c.uraian,
              a.id as id_dt
            FROM 
              dispatch_teknisi a
            LEFT JOIN data_nossa_1_log dps ON a.Ndem = dps.Incident
            LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
            LEFT JOIN psb_laporan_status e ON b.status_laporan=e.laporan_status_id
            LEFT JOIN regu c ON a.id_regu=c.id_regu
            WHERE
              dps.Incident <> "" AND
              c.ACTIVE=1 AND
              (DATE(a.tgl) BETWEEN "'.$tanggalAwal.'" AND "'.$tanggalAkhir.'") AND
              e.laporan_status_id IN (1,37,38) AND
              c.id_regu = "'.$regu.'"
              ORDER BY a.tgl
            ';
      $getData = DB::select($sql);

      if ($jenis=="AO" || $jenis=="MO"){
          if ($jenis=="AO"){
              $where_jenis = 'AND (dps.jenisPsb LIKE "%AO%" OR dps.jenisPsb="AS|VOICE" OR a.dispatch_by=1 OR dm.ketMigrasi=1) ';
          }
          else{
              $where_jenis = 'AND dps.jenisPsb LIKE "%MO%"';
          }    

          $sql = 'SELECT 
                  a.*,
                  b.*,
                  e.laporan_status,
                  c.uraian,
                  dps.jenisPsb,
                  a.id as id_dt,
                  dm.ketMigrasi
                FROM 
                  dispatch_teknisi a
                LEFT JOIN Data_Pelanggan_Starclick dps ON a.Ndem = dps.orderId
                LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
                LEFT JOIN dossier_master dm ON a.Ndem = dm.ND 
                LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
                LEFT JOIN psb_laporan_status e ON b.status_laporan=e.laporan_status_id
                LEFT JOIN regu c ON a.id_regu=c.id_regu
                WHERE
                  (a.dispatch_by IS NULL OR a.dispatch_by = 1) AND
                  c.ACTIVE=1 AND
                  (DATE(a.tgl) BETWEEN "'.$tanggalAwal.'" AND "'.$tanggalAkhir.'") AND
                  e.laporan_status_id IN (1,37,38) AND
                  c.id_regu = "'.$regu.'"
                  '.$where_jenis.'
                  ORDER BY a.tgl
                ';
          $getData = DB::select($sql);
      };

      return view('tool.CekWoPsb',compact('getData', 'jenis'));
  }

  public function inputExcel(){
      return view('tool.inputExcel');
  }

  public function inputExcelSave(Request $req){
      $data = json_decode($req->sns);
      foreach ($data as $dt){

          $tgl = $dt->orderDate;
          $unixTgl = ($tgl - 25569) * 86400;
          $asliTgl = date('Y-m-d H:i:s',$unixTgl);

          $exists = DB::table('Data_Pelanggan_UNSC')->where('orderId',$dt->orderId)->first();
          if(count($exists)==0){
            $simpan = [
              'orderId'       => $dt->orderId,
              'orderNo'       => $dt->orderNo,
              'orderName'     => $dt->orderName,
              'orderAddr'     => $dt->orderAddr,
              'orderNotel'    => $dt->orderNotel,
              'orderKontak'   => $dt->orderKontak,
              'orderDate'     => $asliTgl, 
              'orderDatePs'   => $dt->orderDatePs,
              'orderCity'     => $dt->orderCity,
              'orderStatus'   => $dt->orderStatus, 
              'orderStatusId' => $dt->orderStatusId,  
              'orderPlasa'    => $dt->orderPlasa,  
              'orderNcli'     => $dt->orderNcli,  
              'ndemSpeedy'    => $dt->ndemSpeedy,  
              'ndemPots'      => $dt->ndemPots, 
              'orderPaket'    => $dt->orderPaket, 
              'orderPaketID'  => $dt->orderPaketID, 
              'kcontact'      => $dt->kcontact,    
              'username'      => $dt->username,  
              'alproname'     => $dt->alproname,  
              'tnNumber'      => $dt->tnNumber,  
              'reserveTn'     => $dt->reserveTn, 
              'reservePort'   => $dt->reservePort,  
              'jenisPsb'      => $dt->jenisPsb,  
              'sto'           => $dt->sto,
              'lon'           => $dt->lon,
              'lat'           => $dt->lat  
            ];
            // dd($simpan);
            DB::table('Data_Pelanggan_UNSC')->insert($simpan);
            
          }

      }
      
  }

  public function listBaDigital(Request $req){
      $getData = '';
      $ket     = 2;
      if ($req->has('sc')){
          $getData = tool::getBaDigitalBySc($req->input('sc'));
          $ket = 0;
          if (count($getData)==0){
              $ket = 1;
          }
      };

      return view('tool.ListBaDigital', compact('getData', 'ket'));
  }

  public function rfcKembaliForm(){
      return view('tool.RfcKembaliForm');
  }

  public function rfcKembaliProses(Request $req){
      $rfc = $req->noRfc;
      $rfcExplode = explode(', ', $rfc);
      $rfcImplode = implode("','", $rfcExplode);
      $rfcImplode = "'".$rfcImplode."'";

      $sql = 'SELECT * FROM rfc_sal WHERE rfc IN ('.$rfcImplode.')';
      $data = DB::select($sql);
      
      foreach($data as $dt){

          if (($dt->jumlah - $dt->jmlTerpakai) > 0 ){
              $jmlKembali = $dt->jumlah - $dt->jmlTerpakai;
              $status     = 'kembali';
          }
          else{
              $jmlKembali = 0;
              $status     = null;
          };
          // echo $jmlKembali." <br> ";
          // die;
          DB::table('rfc_sal')->where('id',$dt->id)->update([
              'jmlKembali'    => $jmlKembali,
              'statusReturn'  => $status
          ]);

          echo 'sukses '."<br>";
      }
  }

  public function updateScunsc(){
     $data = DB::table('update_unsc')->get();
      foreach($data as $dt){
          $getData = DB::table('dispatch_teknisi')
                  ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                  ->where('dispatch_teknisi.Ndem',$dt->myir)
                  ->first();

          if(count($getData)==1){
              DB::table('psb_laporan')->where('id_tbl_mj',$getData->id_tbl_mj)->update([
                  'sc_unsc' => $dt->sc,
                  'kcontact_unsc' => $dt->kcontack
              ]);
          };
      };


      dd('sukses');
  }


}