<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use File;
use DateTime;
use Telegram;
use App\DA\GrabModel;
use App\DA\DashboardModel;
use TG;

class GrabController extends Controller
{

	public function check_sc($id){
		$check = DB::connection('pg')->SELECT("
			SELECT
			a.order_id as ORDER_ID,
			a.create_dtm as ORDER_DATE,
			a.order_code as EXTERN_ORDER_ID,
			a.xn4 as NCLI,
			a.customer_desc as CUSTOMER_NAME,
			a.xs3 as WITEL,
			a.create_user_id as AGENT,
			a.xs2 as SOURCE,
			a.xs12 as STO,
			a.xs10 as SPEEDY,
			a.xs11 as POTS,
			a.order_status_id as STATUS_CODE_SC,
			a.xs7 as USERNAME,
			a.xs8 as ORDER_ID_NCX,
			c.xs11 as KCONTACT,
			b.xs3 as ORDER_STATUS
			FROM
				sc_new_orders a
			LEFT JOIN sc_new_order_activities b ON a.order_id = b.order_id AND a.order_status_id = b.xn2
			LEFT JOIN sc_new_order_details c ON a.order_id = c.order_id
			WHERE
				a.xs3 = 'KALSEL' AND
				date(a.create_dtm) = '2020-02-04'
		");
		header("Content-Type: application/json");
		print_r(json_encode($check));
	}

	public function no_hp_onecall($witel,$tglAwal,$tglAkhir){
		$link = "http://36.92.189.82/onecall/api/sales-order?witel=".$witel."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
		$file = @file_get_contents($link);
		$get = @json_decode($file);
		foreach ($get as $result){
			echo $result->myir;

			$check = DB::SELECT('select * from psb_myir_wo where myir = "'.$result->myir.'"');
			if (count($check)>0){
				$query = DB::table('psb_myir_wo')->where('myir',$result->myir)->update([
					'picPelanggan' => $result->no_hp
				]);
				echo " DONE";
			} else {
				echo " SKIP";
			}
			echo "<br />";
		}
	}

	public function sync_starclick_dbmirror($witel){
		$date = date('Y-m-28');
		$check = DB::connection('pg')->SELECT("
			SELECT
			a.*,
			b.*
			FROM
				sc_new_orders a
			LEFT JOIN sc_new_order_details b ON a.order_id = b.order_id
			WHERE
				a.xs3 = '$witel' AND
				date(a.create_dtm) = '$date'
		");
		header("Content-Type: application/json");

		print_r($check);

	}

	public function grabQCBorneo(){
		$tgl = date('Y-m');
		$get_data = DashboardModel::rekonMitraList($tgl);
		foreach ($get_data as $data){
			$get_status_qc = file_get_contents("http://10.128.18.14/proactive-caring/api/quality_control.php?sc=".$data->orderId);
      $get_status_qc  = json_decode($get_status_qc);
      if ($get_status_qc<>""){
				print_r($get_status_qc->data);
				$insert = $get_status_qc->data;
				if ($insert<>"SC TIDAK DITEMUKAN/BELUM DIVALIDASI"){
				if (count($insert)){
				foreach ($insert as $qc){
					$cek_db = DB::table('qc_borneo')->where('sc',$qc->sc)->get();
					if (count($cek_db)==0){
					$insert = DB::table('qc_borneo')->insert([
						'sc' => $qc->sc,
						'status_qc' => $qc->status_qc,
						'unit' => $qc->unit,
						'foto_rumah' => $qc->foto_rumah,
						'ket_rumah' => $qc->ket_rumah,
						'foto_teknisi' => $qc->foto_rumah,
						'ket_teknisi' => $qc->ket_teknisi,
						'foto_odp' => $qc->foto_odp,
						'ket_odp' => $qc->ket_odp,
						'foto_redaman' => $qc->foto_redaman,
						'ket_redaman' => $qc->ket_redaman,
						'foto_ont' => $qc->foto_ont,
						'ket_ont' => $qc->ket_ont,
						'foto_berita' => $qc->foto_berita,
						'ket_berita' => $qc->ket_berita,
						'tagging_lokasi' => $qc->tagging_lokasi,
						'ket_tagging' => $qc->ket_tagging,
						'last_sync' => date('Y-m-d H:i:s')
					]);
					} else {
						$insert = DB::table('qc_borneo')->where('sc',$qc->sc)->update([
						'sc' => $qc->sc,
						'status_qc' => $qc->status_qc,
						'unit' => $qc->unit,
						'foto_rumah' => $qc->foto_rumah,
						'ket_rumah' => $qc->ket_rumah,
						'foto_teknisi' => $qc->foto_rumah,
						'ket_teknisi' => $qc->ket_teknisi,
						'foto_odp' => $qc->foto_odp,
						'ket_odp' => $qc->ket_odp,
						'foto_redaman' => $qc->foto_redaman,
						'ket_redaman' => $qc->ket_redaman,
						'foto_ont' => $qc->foto_ont,
						'ket_ont' => $qc->ket_ont,
						'foto_berita' => $qc->foto_berita,
						'ket_berita' => $qc->ket_berita,
						'tagging_lokasi' => $qc->tagging_lokasi,
						'ket_tagging' => $qc->ket_tagging,
						'last_sync' => date('Y-m-d H:i:s')
					]);
					}
				}
				}
				}
			}
		}
	}

  public function index()
  {
    return view('grab.index');
  }

  public function date_loop(){
    date_default_timezone_set('Asia/Makassar');
    echo date('Y-m-d')."<br />";
    for ($i=0;$i<=7;$i++){
      $date = date('d/m/Y',strtotime("-$i days"));
      echo $date."<br />";
    }
  }

  public function GrabNossaFix(){
    $this->GrabNossaFixQuery('data_nossa_1');
    $this->GrabNossaFixQuery('data_nossa_1_log');
  }

  public static function grabstarclickncx_insert($link){
    echo "SYNC TO STARCLICKNCX<br />";

    $ch = curl_init();
    // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1573355998402&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
    $get_cookie = DB::table('cookie_starclick')->get();
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    echo urldecode($link);
    $headers = array();
		$headers[] = 'Connection: keep-alive';
		$headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36';
		$headers[] = 'X-Requested-With: XMLHttpRequest';
		$headers[] = 'Accept: */*';
		$headers[] = 'Sec-Fetch-Site: same-origin';
		$headers[] = 'Sec-Fetch-Mode: cors';
		$headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
		$headers[] = 'Accept-Encoding: gzip, deflate, br';
		$headers[] = 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7';
    $headers[] = $get_cookie[0]->cookie;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    // print_r($result);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    } else {
      $result = json_decode($result);
      $allRecord = array();
      if (count($result)){
      $list = $result->data;

      if (count($list)){
      $list = $list->LIST;
      foreach ($list as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
            if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array(
              "orderId"=>@$data->ORDER_ID,
              "orderName"=>str_replace("'","",@$data->CUSTOMER_NAME),
              "orderAddr"=>str_replace("'","",@$data->INS_ADDRESS),
              "orderNotel"=>@$data->POTS,
              "orderDate"=>$orderDate,
              "orderDatePs"=>$orderDatePs,
              "orderCity"=>str_replace("'","",@$data->CUSTOMER_ADDR),
              "orderStatus"=>@$data->STATUS_RESUME,
              "orderStatusId"=>@$data->STATUS_CODE_SC,
              "orderNcli"=>@$data->NCLI,
              "ndemSpeedy"=>@$data->SPEEDY,
              "ndemPots"=>@$data->POTS,
              "orderPaketID"=>@$data->ODP_ID,
              "kcontact"=>str_replace("'"," ",@$data->KCONTACT),
              "username"=>@$data->USERNAME,
              "alproName"=>@$data->LOC_ID,
              "tnNumber"=>@$data->POTS,
              "reservePort"=>@$data->ODP_ID,
              "jenisPsb"=>@$data->JENISPSB,
              "sto"=>@$data->STO,
              "lat"=>@$data->GPS_LATITUDE,
              "lon"=>@$data->GPS_LONGITUDE,
              "internet"=>@$internet,
              "noTelp"=>@$noTelp,
              "orderPaket"=>@$data->PACKAGE_NAME,
              "witel"=>@$data->WITEL);
        echo $data->ORDER_ID."<br />";
      }
      }
      }
      echo "saving\n";
      $srcarr=array_chunk($allRecord,500);
      foreach($srcarr as $item) {
          //DB::table('proaktif')->insert($item);
          self::insertOrUpdate($item);
          if (count($srcarr)>0){
            DB::table('cookie_starclick')->update([
              'last_sync' => date('Y-m-d H:i:s')
            ]);
          }
      }

    }
    curl_close($ch);
  }

  public static function grabstarclickncx($witel){
    date_default_timezone_set('Asia/Makassar');
    for ($i=0;$i<=2;$i++){
      $datex = date('d/m/Y',strtotime("-$i days"));
      $ch = curl_init();
      $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1573355998402&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$witel.'","Field":"ORG","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"'.$datex.'","EndDate":"'.$datex.'","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page=1&start=0&limit=10';

      // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1573355998402&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
      $get_cookie = DB::table('cookie_starclick')->get();
      curl_setopt($ch, CURLOPT_URL, $link);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
      // echo urldecode($link);
      $headers = array();
			$headers[] = 'Connection: keep-alive';
			$headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36';
			$headers[] = 'X-Requested-With: XMLHttpRequest';
			$headers[] = 'Accept: */*';
			$headers[] = 'Sec-Fetch-Site: same-origin';
			$headers[] = 'Sec-Fetch-Mode: cors';
			$headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
			$headers[] = 'Accept-Encoding: gzip, deflate, br';
			$headers[] = 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7';
      $headers[] = $get_cookie[0]->cookie;
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      $result = curl_exec($ch);
      print_r($result);
      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
      } else {
        $result = json_decode($result);
        $allRecord = array();
        if (count($result)){
          $list = $result->data;
          print_r(@$list->CNT);
          echo "<br />";
          $jumlahpage = round(@(int)$list->CNT/10);
          echo $jumlahpage."<br />";
          // print_r($list);
          // echo "jumlah".count($list);
          if (isset($list->CNT) && isset($list->LIST)){
            $list = @$list->LIST;
            $start = 0;
            for ($x=1;$x<=$jumlahpage;$x++){
            $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1573355998402&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$witel.'","Field":"ORG","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"'.$datex.'","EndDate":"'.$datex.'","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page='.$x.'&start='.$start.'&limit=10';
            $start = $start+10;
            self::grabstarclickncx_insert($link);
            echo $link."<br />";
            }
            // echo "jumlah page : ".$jumlahpage."<br />";

          }
        }
    }
  }
}


  public function GrabNossaFixQuery($table){
    // $query = DB::table($table)->where('ODP',NULL)->where('Witel','KALSEL (BANJARMASIN)')->get();
    // foreach ($query as $result){
    //   $DATEK = $result->Datek;
    //   $split = explode(' ',$DATEK);
    //   if (count($split)>1){
    //     DB::table($table)->where('Incident',$result->Incident)->update([
    //       'ODP' => $split[2]
    //     ]);
    //     echo $split[2]."<br />";
    //   }
    // }
    DB::statement("update ".$table." set ODP = SUBSTRING_INDEX(SUBSTRING_INDEX(Datek, ' ', 3), ' ', -1) WHERE ODP is null");

  }

  public function starclick(){
    # default
    $start = date('d/m/Y');
    $end = date('d/m/Y');
    $witel = "Kalsel";
    $status = '';

    # get from link
    // $start = Input::get('start');
    // $end = Input::get('end');
    // $witel = Input::get('witel');
    $status = Input::get('status');

    # get from starclick link
    $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=".$witel."&Field=ORG&Fieldstatus=".$status."&Fieldwitel=&StartDate=".$start."&EndDate=".$end."&page=1&start=0&limit=10000";
    $result = json_decode(@file_get_contents($link));
    echo $link."<br />";

    # generate result
    var_dump((array)$result->data);
    $array = (array)$result->data;

    # insert ignore
    DB::connection('mysql4')->table('4_0_master_order_1_log_pi')->insertIgnore($array);

  }

  public function tommannossa($in){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $login = curl_exec($ch);

    $dom = @\DOMDocument::loadHTML(trim($login));
    $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");
    //$value = $input->item($i)->getAttribute("value");
    print_r($input);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=850039&password=telkomjuara");
    $result = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
    curl_setopt($ch, CURLOPT_POST, false);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
    $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");
    echo "<br />Finding : ".$in."<br />";
    // event to find
    $event = json_encode(array((object)array(
      "type"=>"setvalue",
      "targetId"=>"mx89",
      "value"=> $in,
      "requestType"=>"ASYNC",
      "csrftokenholder"=>$csrftokenholder
    ),(object)array(
      "type"=>"find",
      "targetId"=>"mx89",
      "value"=>"",
      "requestType"=>"SYNC",
      "csrftokenholder"=>$csrftokenholder)));

    $postdata = http_build_query(
      array(
          "uisessionid" => $uisesid,
          "csrftoken" => $csrftokenholder,
          "currentfocus" => "mx89",
          "scrollleftpos" => "0",
          "scrolltoppos" => "0",
          "requesttype" => "SYNC",
          "responsetype" => "text/xml",
          "events" => $event
      )
    );
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $ResolveClick = $dom->getElementsByTagName("li")[0]->getAttribute("id");
    $TOclick = $dom->getElementsByTagName("li")[1]->getAttribute("id");
    print_r("TOID : ".$TOclick);
    print_r("<br />");
    print_r("RESOLVEID : ".$ResolveClick."<br />");
    // event for TO
    echo "Takeover....<br />";
    $event = json_encode(array((object)array(
      "type"=>"click",
      "targetId"=>$TOclick,
      "value"=> "",
      "requestType"=>"ASYNC",
      "csrftokenholder"=>$csrftokenholder
    )));

    $postdata = http_build_query(
      array(
          "uisessionid" => $uisesid,
          "csrftoken" => $csrftokenholder,
          "currentfocus" => "mx89",
          "scrollleftpos" => "0",
          "scrolltoppos" => "0",
          "requesttype" => "SYNC",
          "responsetype" => "text/xml",
          "events" => $event
      )
    );
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $result = curl_exec($ch);
    // event for RESOLVE
    $event = json_encode(array((object)array(
      "type"=>"click",
      "targetId"=>$ResolveClick,
      "value"=> "",
      "requestType"=>"SYNC",
      "csrftokenholder"=>$csrftokenholder
    )));

    $postdata = http_build_query(
      array(
          "uisessionid" => $uisesid,
          "csrftoken" => $csrftokenholder,
          "currentfocus" => "mx89",
          "scrollleftpos" => "0",
          "scrolltoppos" => "0",
          "requesttype" => "SYNC",
          "responsetype" => "text/xml",
          "events" => $event
      )
    );
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $result = curl_exec($ch);
    echo "Resolving....<br />";
    //Resolve
    $result=str_replace('<?xml version="1.0" encoding="UTF-8" ?>', ' ', $result);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $textarea = $dom->getElementsByTagName("input")[3]->getAttribute("id");
    $radio = $dom->getElementsByTagName("input")[2]->getAttribute("id");
    $button = $dom->getElementsByTagName("button")[0]->getAttribute("id");
    // echo "radio:".$radio;
    // echo "#textarea:".$textarea;
    $event = json_encode(array((object)array(
      "type"=>"setvalue",
      "targetId"=>$textarea,
      "value"=> "Tomman",
      "requestType"=>"ASYNC",
      "csrftokenholder"=>$csrftokenholder
    ),(object)array(
      "type"=>"click",
      "targetId"=>$radio,
      "value"=> "",
      "requestType"=>"SYNC",
      "csrftokenholder"=>$csrftokenholder
    ),(object)array(
      "type"=>"click",
      "targetId"=>$button,
      "value"=> "",
      "requestType"=>"SYNC",
      "csrftokenholder"=>$csrftokenholder
    )));

    $postdata = http_build_query(
      array(
          "uisessionid" => $uisesid,
          "csrftoken" => $csrftokenholder,
          "currentfocus" => "mx89",
          "scrollleftpos" => "0",
          "scrolltoppos" => "0",
          "requesttype" => "SYNC",
          "responsetype" => "text/xml",
          "events" => $event
      )
    );
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $result = curl_exec($ch);
    echo "Done...<br />";
    // print_r($result);

  }

  public function nossaIN($in){
    $user_nossa = GrabModel::user_nossa()[0];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $login = curl_exec($ch);

    $dom = @\DOMDocument::loadHTML(trim($login));
    $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");
    //$value = $input->item($i)->getAttribute("value");
    print_r($input);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
    $result = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
    curl_setopt($ch, CURLOPT_POST, false);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
    $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");

    $event = json_encode(array((object)array(
      "type"=>"setvalue",
      "targetId"=>"mx".$user_nossa->incident,
      "value"=>"=".$in,
      "requestType"=>"ASYNC",
      "csrftokenholder"=>$csrftokenholder
    ),(object)array(
      "type"=>"filterrows",
      "targetId"=>"mx454",
      "value"=>"",
      "requestType"=>"SYNC",
      "csrftokenholder"=>$csrftokenholder)));

    $postdata = http_build_query(
      array(
          "uisessionid" => $uisesid,
          "csrftoken" => $csrftokenholder,
          "currentfocus" => "mx1077",
          "scrollleftpos" => "0",
          "scrolltoppos" => "0",
          "requesttype" => "SYNC",
          "responsetype" => "text/xml",
          "events" => $event
      )
    );

    //print_r($postdata);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $result = curl_exec($ch);
    print_r($result);
    //
    //dd($result);
    $posisi = strpos($result,'<component vis="true" id="mx310_holder" compid="mx310"><![CDATA[<a ctype="label"  id="mx310"  href="')+100;
    //echo "posisi : ".$posisi;
    $asd = substr_replace($result, "", 0, $posisi);
    $asd = substr_replace($asd, "", strpos($asd,'" onmouseover="return noStatus();" target="_blank"   noclick="1"   targetid="mx310"    class="text tht  " style="display:block;;;;;" title="Download"'), strlen($asd));
    echo "test ".($asd)." #";
    curl_setopt($ch, CURLOPT_URL, $asd);

    $result = curl_exec($ch);
    curl_close ($ch);
    $result=str_replace('&nbsp;', ' ', $result);
    // dd($result);

        $columns = array("Incident", "Customer_Name", "Contact_Name","Contact_Phone","Contact_Email","Summary", "Owner_Group", "Owner", "Last_Updated_Work_Log", "Last_Work_Log_Date", "Count_CustInfo", "Last_CustInfo", "Assigned_to","Booking_Date", "Assigned_by","Reported_Priority","Source","Subsidiary","External_Ticket_ID", "External_Ticket_Status","Segment","Channel","Customer_Segment","Closed_By","Customer_ID","Service_ID","Service_No","Service_Type","Top_Priority","SLG","Technology","Datek","RK_Name","Induk_Gamas","Reported_Date","LAPUL","GAUL","TTR_Customer","TTR_Nasional","TTR_Regional","TTR_Witel","TTR_Mitra","TTR_Agent","TTR_Pending","Status","Hasil_Ukur","OSM_Resolved_Code","Last_Update_Ticket","Status_Date","Resolved_By","Workzone","Witel","Regional","Incident_Symptom","Solution_Segment","Actual_Solution","ID");
    //$result = ;
    $dom = @\DOMDocument::loadHTML(trim($result));
    // print_r($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 0, $jcount = count($columns)-1; $j < $jcount; $j++)
      {
          $td = $cells->item($j);
          if ($j==13) {
            $new_datetime = "";
            $old_datetime = explode(' ', $td->nodeValue);
            if (count($old_datetime)>1){
              $old_date = explode('-', $old_datetime[0]);
							if (count($old_date)>0){
              $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
              $new_datetime = $new_date.' '.$old_datetime[1];
              $data[$columns[$j]] = $new_datetime;
							}
            }
          }

          if ($j==34) {
            $old_datetime = explode(' ', $td->nodeValue);
            if (count($old_datetime)>0){
              $old_date = explode('-', $old_datetime[0]);
              $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
              $new_datetime = $new_date.' '.$old_datetime[1];

              $getTime = explode(':', $old_datetime[1]);

              if ($getTime[0]>=17 || $getTime[0]<8) {
                $data['Hold'] = '1';
              } else {
                $data['Hold'] = '0';
              }

              if ($getTime[0]>=17) {
                $newDatetime = new DateTime($new_date);
                $newDatetime->modify('+1 day');
                $data['UmurDatetime'] = $newDatetime->format('Y-m-d 08:00:01');
                //echo $newDatetime->format('Y-m-d 08:00:01');
              } else if ($getTime[0]<8) {
                $data['UmurDatetime'] = date($new_date.' 08:00:02');

              } else {
                $data['UmurDatetime'] = $new_datetime;
              }

              $data[$columns[$j]] = $new_datetime;

            } else {
              $data[$columns[$j]] = str_replace('&nbsp;', '', $td->nodeValue);
            }
          } else {
          $data[$columns[$j]] =  str_replace('&nbsp;', '', $td->nodeValue);
          }
          if($j==0) {
            $data['ID'] =  substr($td->nodeValue,2);
              //echo $td->nodeValue."<br />";

          }
      }
      $result[] = $data;
    }

    //print_r($result);
    // DB::table('data_nossa_1_log')->where('Incident',$in)->delete();
    DB::table('data_nossa_1_log')->insert($result);
    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> UPDATE']
    ]);
  }

  public function nossaINx($in){
    $user_nossa = GrabModel::user_nossa()[0];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $login = curl_exec($ch);

    $dom = @\DOMDocument::loadHTML(trim($login));
    $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");
    //$value = $input->item($i)->getAttribute("value");
    print_r($input);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
    $result = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
    curl_setopt($ch, CURLOPT_POST, false);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
    $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");

    $event = json_encode(array((object)array(
      "type"=>"setvalue",
      "targetId"=>"mx".$user_nossa->incident,
      "value"=>"=".$in,
      "requestType"=>"ASYNC",
      "csrftokenholder"=>$csrftokenholder
    ),(object)array(
      "type"=>"filterrows",
      "targetId"=>"mx454",
      "value"=>"",
      "requestType"=>"SYNC",
      "csrftokenholder"=>$csrftokenholder)));

    $postdata = http_build_query(
      array(
          "uisessionid" => $uisesid,
          "csrftoken" => $csrftokenholder,
          "currentfocus" => "mx1077",
          "scrollleftpos" => "0",
          "scrolltoppos" => "0",
          "requesttype" => "SYNC",
          "responsetype" => "text/xml",
          "events" => $event
      )
    );

    //print_r($postdata);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $result = curl_exec($ch);
    //print_r($result);
    //
    //dd($result);
    $posisi = strpos($result,'<component vis="true" id="mx310_holder" compid="mx310"><![CDATA[<a ctype="label"  id="mx310"  href="')+100;
    //echo "posisi : ".$posisi;
    $asd = substr_replace($result, "", 0, $posisi);
    $asd = substr_replace($asd, "", strpos($asd,'" onmouseover="return noStatus();" target="_blank"   noclick="1"   targetid="mx310"    class="text tht  " style="display:block;;;;;" title="Download"'), strlen($asd));
    //echo "test ".($asd)." #";
    curl_setopt($ch, CURLOPT_URL, $asd);

    $result = curl_exec($ch);
    curl_close ($ch);
    $result=str_replace('&nbsp;', ' ', $result);
    //dd($result);

        $columns = array("Incident", "Customer_Name", "Contact_Name","Contact_Phone","Contact_Email","Summary", "Owner_Group", "Owner", "Last_Updated_Work_Log", "Last_Work_Log_Date", "Count_CustInfo", "Last_CustInfo", "Assigned_to","Booking_Date", "Assigned_by","Reported_Priority","Source","Subsidiary","External_Ticket_ID", "External_Ticket_Status","Segment","Channel","Customer_Segment","Closed_By","Customer_ID","Service_ID","Service_No","Service_Type","Top_Priority","SLG","Technology","Datek","RK_Name","Induk_Gamas","Reported_Date","LAPUL","GAUL","TTR_Customer","TTR_Nasional","TTR_Regional","TTR_Witel","TTR_Mitra","TTR_Agent","TTR_Pending","Status","Hasil_Ukur","OSM_Resolved_Code","Last_Update_Ticket","Status_Date","Resolved_By","Workzone","Witel","Regional","Incident_Symptom","Solution_Segment","Actual_Solution","ID");

    //$result = ;
    $dom = @\DOMDocument::loadHTML(trim($result));
    //print_r($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    $incident = '';
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
      $new_datatime = "";
      $cells = $rows->item($i)->getElementsByTagName('td');
      $old_datetime = explode(' ', $cells->item(13)->nodeValue);
      if (count($old_datetime)>0){
        $old_date = explode('-', $old_datetime[0]);
        $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
        $new_datetime = $new_date.' '.$old_datetime[1];
      }
      $data = array();
      $incident = $cells->item(0)->nodeValue;
      $data['Owner_Group'] = $cells->item(6)->nodeValue;
      $data['Owner'] = $cells->item(7)->nodeValue;
      $data['Last_Work_Log_Date'] = $cells->item(9)->nodeValue;
      $data['Assigned_To'] = $cells->item(12)->nodeValue;
      $data['Booking_Date'] = $new_datetime;
      $data['Assigned_By'] = $cells->item(14)->nodeValue;
      $data['TTR_Customer'] = $cells->item(34)->nodeValue;
      $data['TTR_Nasional'] = $cells->item(35)->nodeValue;
      $data['TTR_Regional'] = $cells->item(36)->nodeValue;
      $data['TTR_Witel'] = $cells->item(37)->nodeValue;
      $data['TTR_Mitra'] = $cells->item(38)->nodeValue;
      $data['TTR_Agent'] = $cells->item(39)->nodeValue;
      $data['TTR_Pending'] = $cells->item(40)->nodeValue;
      $data['Status'] = $cells->item(41)->nodeValue;
      $data['Last_Update_Ticket'] = $cells->item(44)->nodeValue;
      $data['Status_Date'] = $cells->item(45)->nodeValue;
    }

    //print_r($result);
    if ($incident<>''){
      DB::table('data_nossa_1_log')->where('Incident',$incident)->update($data);
    }
    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> UPDATE']
    ]);
  }

  public function grabNossa(){
    $this->nossa('KALSEL (BANJARMASIN)',1);
    // $this->nossa('TA HD WITEL KALBAR',0);
    // $this->nossa('TA HD WITEL KALTENG',0);
  }

  public function grabNossaMaintenance(){
    $this->nossa('ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)',1);
    // $this->nossa('ACCESS MAINTENANCE WITEL KALBAR (PONTIANAK)',0);
    // $this->nossa('ACCESS MAINTENANCE WITEL KALTENG (PALANGKARAYA)',0);
  }

  public function grabNossaWOC(){
    $this->nossa('WOC FULFILLMENT KALSEL (BANJARMASIN)',1);
    // $this->nossa('WOC FULFILLMENT KALBAR (PONTIANAK)',0);
    // $this->nossa('WOC FULFILLMENT KALTENG (PALANGKARAYA)',0);
  }

  public function testNossa1(){

  }

  public function testNossa(){
      date_default_timezone_set('Asia/Makassar');
    $user_nossa = GrabModel::user_nossa()[0];
    $ch = curl_init();
    // Reset all previously set options
    curl_reset($ch);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookiex.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $login = curl_exec($ch);
    // if ($user_nossa->umurlogin>1){
    // print_r($login);
    $dom = @\DOMDocument::loadHTML(trim($login));
    $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");
    echo "umur ($user_nossa->umurlogin)";

    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
    $result = curl_exec($ch);
    // print_r($result);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
    curl_setopt($ch, CURLOPT_POST, false);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
    $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");
    DB::table('user_nossa')->update([
      'uisesid' => $uisesid,
      'csrftokenholder' => $csrftokenholder,
      'updatetime' => date('Y-m-d H:i:s')
    ]);
  // } else {
  //   $uisesid = $user_nossa->uisesid;
  //   $csrftokenholder = $user_nossa->csrftokenholder;
  // }

          $event = json_encode(array((object)array(
            "type"=>"setvalue",
            "targetId"=>"mx".$user_nossa->owner,
            "value"=>"=TA HD WITEL KALSEL",
            "requestType"=>"ASYNC",
            "csrftokenholder"=>$csrftokenholder
          ),(object)array(
            "type"=>"setvalue",
            "targetId"=>"mx".$user_nossa->statusx,
            "value"=>"=QUEUED,=SLAHOLD,=BACKEND",
            "requestType"=>"ASYNC",
            "csrftokenholder"=>$csrftokenholder
          ),(object)array(
            "type"=>"filterrows",
            "targetId"=>"mx452",
            "value"=>"",
            "requestType"=>"SYNC",
            "csrftokenholder"=>$csrftokenholder)));

          $postdata = http_build_query(
            array(
                "uisessionid" => $uisesid,
                "csrftoken" => $csrftokenholder,
                "currentfocus" => "mx4733",
                "scrollleftpos" => "0",
                "scrolltoppos" => "0",
                "requesttype" => "SYNC",
                "responsetype" => "text/xml",
                "events" => $event
            )
          );
          print_r($event);
          echo "<br />";
          print_r($postdata);
          curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
          $result = curl_exec($ch);
          print_r($result);

  }


  public function nossa($loker,$kalsel){
      echo "test";
      $user_nossa = GrabModel::user_nossa()[0];
      $ch = curl_init();
      // Reset all previously set options
      curl_reset($ch);
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookiex.jar');  //could be empty, but cause problems on some hosts
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      $login = curl_exec($ch);
      // print_r($login);
      $dom = @\DOMDocument::loadHTML(trim($login));
      $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");

      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
      $result = curl_exec($ch);
      // print_r($result);
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
      curl_setopt($ch, CURLOPT_POST, false);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML(trim($result));
      $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
      $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");

      $event = json_encode(array((object)array(
        "type"=>"setvalue",
        "targetId"=>"mx".$user_nossa->owner,
        "value"=>"=".$loker,
        "requestType"=>"ASYNC",
        "csrftokenholder"=>$csrftokenholder
      ),(object)array(
        "type"=>"setvalue",
        "targetId"=>"mx".$user_nossa->statusx,
        "value"=>"=QUEUED,=SLAHOLD,=BACKEND,=FINALCHECK,=MEDIACARE,=SALAMSIM,=RESOLVED",
        "requestType"=>"ASYNC",
        "csrftokenholder"=>$csrftokenholder
      ),(object)array(
        "type"=>"filterrows",
        "targetId"=>"mx452",
        "value"=>"",
        "requestType"=>"SYNC",
        "csrftokenholder"=>$csrftokenholder)));

      $postdata = http_build_query(
        array(
            "uisessionid" => $uisesid,
            "csrftoken" => $csrftokenholder,
            "currentfocus" => "mx".$user_nossa->incident,
            "scrollleftpos" => "0",
            "scrolltoppos" => "0",
            "requesttype" => "SYNC",
            "responsetype" => "text/xml",
            "events" => $event
        )
      );

      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
      $result = curl_exec($ch);
      // dd($result);
      $posisi = strpos($result,'<component vis="true" id="mx310_holder" compid="mx310"><![CDATA[<a ctype="label"  id="mx310"  href="')+100;
      // print_r($result);
      $asd = substr_replace($result, "", 0, $posisi);
      $asd = substr_replace($asd, "", strpos($asd,'" onmouseover="return noStatus();" target="_blank"   noclick="1"   targetid="mx310"    class="text tht  " style="display:block;;;;;" title="Download"'), strlen($asd));

      curl_setopt($ch, CURLOPT_URL, $asd);

      $result = curl_exec($ch);
      curl_close ($ch);
      $result=str_replace('&nbsp;', ' ', $result);
      // dd($result);

      $columns = array("Incident", "Customer_Name", "Contact_Name","Contact_Phone","Contact_Email","Summary", "Owner_Group", "Owner", "Last_Updated_Work_Log", "Last_Work_Log_Date", "Count_CustInfo", "Last_CustInfo", "Assigned_to","Booking_Date", "Assigned_by","Reported_Priority","Source","Subsidiary","External_Ticket_ID", "External_Ticket_Status","Segment","Channel","Customer_Segment","Closed_By","Customer_ID","Service_ID","Service_No","Service_Type","Top_Priority","SLG","Technology","Datek","RK_Name","Induk_Gamas","Reported_Date","LAPUL","GAUL","TTR_Customer","TTR_Nasional","TTR_Regional","TTR_Witel","TTR_Mitra","TTR_Agent","TTR_Pending","Status","Hasil_Ukur","OSM_Resolved_Code","Last_Update_Ticket","Status_Date","Resolved_By","Workzone","Witel","Regional","Incident_Symptom","Solution_Segment","Actual_Solution","ID");

      $dom = @\DOMDocument::loadHTML(trim($result));
      $table = $dom->getElementsByTagName('table')->item(0);
      // dd($table);
      $rows = $table->getElementsByTagName('tr');
      $result = array();
      $roc = array();
      $rock = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
        $cells = $rows->item($i)->getElementsByTagName('td');

        // if ($cells->item(48)->nodeValue=="KALSEL (BANJARMASIN)"){
        //   $old_datetime1 = explode(' ', $cells->item(29)->nodeValue);
        //   if (count($old_datetime1)>0){
        //     $old_date1 = explode('-', $old_datetime1[0]);
        //     $new_date1 = $old_date1[2].'-'.$old_date1[1].'-'.$old_date1[0];
        //     $new_datetime1 = $new_date1.' '.$old_datetime1[1];
        //   } else {
        //     $new_datetime1 = "0";
        //   }
        //   $roc[$i]['no_tiket'] = $cells->item(0)->nodeValue;
        //   $roc[$i]['headline'] = $cells->item(4)->nodeValue;
        //   $roc[$i]['tglOpen'] = $new_datetime1;
        //   $roc[$i]['tgl_open'] = $new_datetime1;
        //   $roc[$i]['CMDF'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['sto_trim'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['sto'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['Channel'] = $cells->item(18)->nodeValue;
        //   $roc[$i]['laborcode'] = $cells->item(11)->nodeValue;
        //   $roc[$i]['RK'] = $cells->item(26)->nodeValue;
        //   $roc[$i]['no_tiket'] = $cells->item(0)->nodeValue;
        //
        //   $rock[$i]['trouble_no'] = $cells->item(0)->nodeValue;
        //   $rock[$i]['headline'] = $cells->item(4)->nodeValue;
        //   $rock[$i]['trouble_opentime'] = $new_datetime1;
        //   // $rock[$i]['tgl_open'] = $new_datetime1;
        //   $rock[$i]['cmdf'] = $cells->item(44)->nodeValue;
        //   $rock[$i]['sto'] = $cells->item(44)->nodeValue;
        //   $rock[$i]['channel'] = $cells->item(18)->nodeValue;
        //   $rock[$i]['keluhan_desc'] = $cells->item(47)->nodeValue;
        //   $rock[$i]['rk'] = $cells->item(26)->nodeValue;
        //   $rock[$i]['regional'] = $cells->item(46)->nodeValue;
        //   $rock[$i]['witel'] = $cells->item(45)->nodeValue;
        //
        //   $startDate = new DateTime($new_datetime1);
        //   $endDate = new DateTime();
        //   $since_start = $startDate->diff($endDate);
        //
        //   $rock[$i]['hari'] = $cells->item(31)->nodeValue;
        // }
        $data = array();

        for ($j = 0, $jcount = count($columns)-1; $j < $jcount; $j++)
        {
            $td = $cells->item($j);

						if ($j==13){
							$old_datetime = explode(' ',$td->nodeValue);
							if (count($old_datetime)>0){

							}
						}

            if ($j==34) {
              $old_datetime = explode(' ', $td->nodeValue);
              if (count($old_datetime)>0){
                $old_date = explode('-', $old_datetime[0]);
                $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
                $new_datetime = $new_date.' '.$old_datetime[1];

                $getTime = explode(':', $old_datetime[1]);

                if ($getTime[0]>=17 || $getTime[0]<8) {
                  $data['Hold'] = '1';
                } else {
                  $data['Hold'] = '0';
                }

                if ($getTime[0]>=17) {
                  $newDatetime = new DateTime($new_date);
                  $newDatetime->modify('+1 day');
                  $data['UmurDatetime'] = $newDatetime->format('Y-m-d 08:00:01');
                  echo $newDatetime->format('Y-m-d 08:00:01');
                } else if ($getTime[0]<8) {
                  $data['UmurDatetime'] = date($new_date.' 08:00:02');

                } else {
                  $data['UmurDatetime'] = $new_datetime;
                }

                $data[$columns[$j]] = $new_datetime;

              } else {
                $data[$columns[$j]] = str_replace('&nbsp;', '', $td->nodeValue);
              }
            } else {
            $data[$columns[$j]] =  str_replace('&nbsp;', '', $td->nodeValue);
            }
            if($j==0) {
              $data['ID'] =  substr($td->nodeValue,2);
                echo $td->nodeValue."<br />";

            }
            if ($j==13) {
              $new_datetime = "";

              $old_datetime = explode(' ', $td->nodeValue);
              if (count($old_datetime)>0){
                $old_date = explode('-', @$old_datetime[0]);
                $new_date = @$old_date[2].'-'.@$old_date[1].'-'.@$old_date[0];
                $new_datetime = $new_date.' '.@$old_datetime[1];
                $data[$columns[$j]] = $new_datetime;
                // $data[$columns[$j]] = $td->nodeValue;
              }

            }
        }
        $result[] = $data;
      }
      //print_r($result);
      DB::table('data_nossa_1')->where('Witel',$loker)->delete();
      if ($kalsel == 1){
        DB::table('roc_active')->truncate();
        DB::table('roc_active')->insert($roc);
        #DB::table('rock_excel')->truncate();
        #DB::table('rock_excel')->insert($rock);
      }
      print_r($roc);
      foreach (array_chunk($result,1000) as $t) {
        DB::table('data_nossa_1')->insertIgnore($t);
        DB::table('data_nossa_1_log')->insertIgnore($t);
      }
  }

public function nossaCust($loker,$kalsel){
      echo "test";
      $user_nossa = GrabModel::user_nossa()[0];
      $ch = curl_init();
      // Reset all previously set options
      curl_reset($ch);
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookiex.jar');  //could be empty, but cause problems on some hosts
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      $login = curl_exec($ch);
      // print_r($login);
      $dom = @\DOMDocument::loadHTML(trim($login));
      $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");

      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
      $result = curl_exec($ch);
      // print_r($result);
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
      curl_setopt($ch, CURLOPT_POST, false);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML(trim($result));
      $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
      $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");

      $event = json_encode(array((object)array(
        "type"=>"setvalue",
        "targetId"=>"mx".$user_nossa->owner,
        "value"=>"=".$loker,
        "requestType"=>"ASYNC",
        "csrftokenholder"=>$csrftokenholder
      ),(object)array(
        "type"=>"setvalue",
        "targetId"=>"mx".$user_nossa->statusx,
        "value"=>"=QUEUED,=SLAHOLD,=BACKEND,=FINALCHECK,=MEDIACARE,=SALAMSIM,=RESOLVED",
        "requestType"=>"ASYNC",
        "csrftokenholder"=>$csrftokenholder
      ),(object)array(
        "type"=>"filterrows",
        "targetId"=>"mx452",
        "value"=>"",
        "requestType"=>"SYNC",
        "csrftokenholder"=>$csrftokenholder)));

      $postdata = http_build_query(
        array(
            "uisessionid" => $uisesid,
            "csrftoken" => $csrftokenholder,
            "currentfocus" => "mx".$user_nossa->incident,
            "scrollleftpos" => "0",
            "scrolltoppos" => "0",
            "requesttype" => "SYNC",
            "responsetype" => "text/xml",
            "events" => $event
        )
      );

      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
      $result = curl_exec($ch);

      $posisi = strpos($result,'<component vis="true" id="mx310_holder" compid="mx310"><![CDATA[<a ctype="label"  id="mx310"  href="')+100;
      //print_r($result);
      $asd = substr_replace($result, "", 0, $posisi);
      $asd = substr_replace($asd, "", strpos($asd,'" onmouseover="return noStatus();" target="_blank"   noclick="1"   targetid="mx310"    class="text tht  " style="display:block;;;;;" title="Download"'), strlen($asd));

      curl_setopt($ch, CURLOPT_URL, $asd);

      $result = curl_exec($ch);
      curl_close ($ch);
      $result=str_replace('&nbsp;', ' ', $result);


      $columns = array("Incident", "Customer_Name", "Contact_Name","Contact_Phone","Contact_Email","Summary", "Owner_Group", "Owner", "Last_Updated_Work_Log", "Last_Work_Log_Date", "Count_CustInfo", "Last_CustInfo", "Assigned_to","Booking_Date", "Assigned_by","Reported_Priority","Source","Subsidiary","External_Ticket_ID", "External_Ticket_Status","Segment","Channel","Customer_Segment","Closed_By","Customer_ID","Service_ID","Service_No","Service_Type","Top_Priority","SLG","Technology","Datek","RK_Name","Induk_Gamas","Reported_Date","LAPUL","GAUL","TTR_Customer","TTR_Nasional","TTR_Regional","TTR_Witel","TTR_Mitra","TTR_Agent","TTR_Pending","Status","Hasil_Ukur","OSM_Resolved_Code","Last_Update_Ticket","Status_Date","Resolved_By","Workzone","Witel","Regional","Incident_Symptom","Solution_Segment","Actual_Solution","ID");

      $dom = @\DOMDocument::loadHTML(trim($result));
      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr');
      $result = array();
      $roc = array();
      $rock = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
        $cells = $rows->item($i)->getElementsByTagName('td');

        // if ($cells->item(48)->nodeValue=="KALSEL (BANJARMASIN)"){
        //   $old_datetime1 = explode(' ', $cells->item(29)->nodeValue);
        //   if (count($old_datetime1)>0){
        //     $old_date1 = explode('-', $old_datetime1[0]);
        //     $new_date1 = $old_date1[2].'-'.$old_date1[1].'-'.$old_date1[0];
        //     $new_datetime1 = $new_date1.' '.$old_datetime1[1];
        //   } else {
        //     $new_datetime1 = "0";
        //   }
        //   $roc[$i]['no_tiket'] = $cells->item(0)->nodeValue;
        //   $roc[$i]['headline'] = $cells->item(4)->nodeValue;
        //   $roc[$i]['tglOpen'] = $new_datetime1;
        //   $roc[$i]['tgl_open'] = $new_datetime1;
        //   $roc[$i]['CMDF'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['sto_trim'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['sto'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['Channel'] = $cells->item(18)->nodeValue;
        //   $roc[$i]['laborcode'] = $cells->item(11)->nodeValue;
        //   $roc[$i]['RK'] = $cells->item(26)->nodeValue;
        //   $roc[$i]['no_tiket'] = $cells->item(0)->nodeValue;
        //
        //   $rock[$i]['trouble_no'] = $cells->item(0)->nodeValue;
        //   $rock[$i]['headline'] = $cells->item(4)->nodeValue;
        //   $rock[$i]['trouble_opentime'] = $new_datetime1;
        //   // $rock[$i]['tgl_open'] = $new_datetime1;
        //   $rock[$i]['cmdf'] = $cells->item(44)->nodeValue;
        //   $rock[$i]['sto'] = $cells->item(44)->nodeValue;
        //   $rock[$i]['channel'] = $cells->item(18)->nodeValue;
        //   $rock[$i]['keluhan_desc'] = $cells->item(47)->nodeValue;
        //   $rock[$i]['rk'] = $cells->item(26)->nodeValue;
        //   $rock[$i]['regional'] = $cells->item(46)->nodeValue;
        //   $rock[$i]['witel'] = $cells->item(45)->nodeValue;
        //
        //   $startDate = new DateTime($new_datetime1);
        //   $endDate = new DateTime();
        //   $since_start = $startDate->diff($endDate);
        //
        //   $rock[$i]['hari'] = $cells->item(31)->nodeValue;
        // }
        $data = array();

        for ($j = 0, $jcount = count($columns)-1; $j < $jcount; $j++)
        {
            $td = $cells->item($j);

            if ($j==34) {
              $old_datetime = explode(' ', $td->nodeValue);
              if (count($old_datetime)>0){
                $old_date = explode('-', $old_datetime[0]);
                $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
                $new_datetime = $new_date.' '.$old_datetime[1];

                $getTime = explode(':', $old_datetime[1]);

                if ($getTime[0]>=17 || $getTime[0]<8) {
                  $data['Hold'] = '1';
                } else {
                  $data['Hold'] = '0';
                }

                if ($getTime[0]>=17) {
                  $newDatetime = new DateTime($new_date);
                  $newDatetime->modify('+1 day');
                  $data['UmurDatetime'] = $newDatetime->format('Y-m-d 08:00:01');
                  echo $newDatetime->format('Y-m-d 08:00:01');
                } else if ($getTime[0]<8) {
                  $data['UmurDatetime'] = date($new_date.' 08:00:02');

                } else {
                  $data['UmurDatetime'] = $new_datetime;
                }

                $data[$columns[$j]] = $new_datetime;

              } else {
                $data[$columns[$j]] = str_replace('&nbsp;', '', $td->nodeValue);
              }
            } else {
            $data[$columns[$j]] =  str_replace('&nbsp;', '', $td->nodeValue);
            }
            if($j==0) {
              $data['ID'] =  substr($td->nodeValue,2);
                echo $td->nodeValue."<br />";

            }
            if ($j==13) {
              $new_datetime = "";

              $old_datetime = explode(' ', $td->nodeValue);
              if (count($old_datetime)>0){
                $old_date = explode('-', @$old_datetime[0]);
                $new_date = @$old_date[2].'-'.@$old_date[1].'-'.@$old_date[0];
                $new_datetime = $new_date.' '.@$old_datetime[1];
                $data[$columns[$j]] = $new_datetime;
                // $data[$columns[$j]] = $td->nodeValue;
              }

            }
        }
        $result[] = $data;
      }
      //print_r($result);
      DB::table('data_nossa_1')->where('Owner_Group',$loker)->delete();
      if ($kalsel == 1){
        DB::table('roc_active')->truncate();
        DB::table('roc_active')->insert($roc);
        #DB::table('rock_excel')->truncate();
        #DB::table('rock_excel')->insert($rock);
      }
      print_r($roc);
      foreach (array_chunk($result,1000) as $t) {
        DB::table('data_nossa_1')->insertIgnore($t);
        DB::table('data_nossa_1_log')->insertIgnore($t);
      }
  }


  public function nonatero_gaul($periode){
    $filename = "upload3/nonatero/monthly_gaul_dtl_".$periode."_R6.txt";
    try
    {
      $content = File::get($filename);
      $grab = explode("\n", $content);
      $grab = str_replace('"', '', $grab);
      echo count($grab);
      foreach($grab as $key=>$line) {
        if ($key<>0){
          $array[$key] = explode('|', $line);
          if (array_key_exists(34,$array[$key]) && $array[$key][34]=="44") {
            echo "OK#";
            $data = $array[$key];
            $data_to_insert[] = [
              'THNBLN'	=>	$data[0],
              'THNBLN_OPEN'	=>	$data[1],
              'THNBLNTGL_OPEN'	=>	$data[2],
              'JAM_OPEN'	=>	$data[3],
              'THNBLN_CLOSE'	=>	$data[4],
              'THNBLNTGL_CLOSE'	=>	$data[5],
              'JAM_CLOSE'	=>	$data[6],
              'IS_GAMAS'	=>	$data[7],
              'UNIT'	=>	$data[8],
              'PLBLCL'	=>	$data[9],
              'FLAG_TEKNIS'	=>	$data[10],
              'JENIS_GGN'	=>	$data[11],
              'STAT_INDI'	=>	$data[12],
              'P'	=>	$data[13],
              'KAT_PLG'	=>	$data[14],
              'PRODUK'	=>	$data[15],
              'TROUBLE_OPENTIME'	=>	$data[16],
              'TROUBLE_CLOSETIME'	=>	$data[17],
              'TGL_TECH_CLOSE'	=>	$data[18],
              'DATE_CLOSE'	=>	$data[19],
              'STATUS'	=>	$data[20],
              'ALPRO'	=>	$data[21],
              'CHANNEL_ID'	=>	$data[22],
              'CHANNEL'	=>	$data[23],
              'SEGMENTASI'	=>	$data[24],
              'SUBSEGMENTASI'	=>	$data[25],
              'ACTUAL_SOLUTION'	=>	$data[26],
              'NUM_LAPUL'	=>	$data[27],
              'TROUBLE_NO'	=>	$data[28],
              'NCLI'	=>	$data[29],
              'NDOS'	=>	$data[30],
              'TROUBLE_NUMBER'	=>	$data[31],
              'ND_REFERENCE'	=>	$data[32],
              'REG'	=>	$data[33],
              'CWITEL'	=>	$data[34],
              'WITEL'	=>	$data[35],
              'CDATEL'	=>	$data[36],
              'DATEL'	=>	$data[37],
              'CMDF'	=>	$data[38],
              'MDF'	=>	$data[39],
              'RK'	=>	$data[40],
              'DP'	=>	$data[41],
              'DESC_CATEGORY_SYMPTOM'	=>	$data[42],
              'KONDISI_JARINGAN'	=>	$data[43],
              'SYMPTOM_LV0'	=>	$data[44],
              'SOLUTION0'	=>	$data[45],
              'LETAK_PERBAIKAN'	=>	$data[46],
              'SOLUTION1'	=>	$data[47],
              'LAST_TCLOSE_USER_ID' =>  $data[48],
              'LAST_TCLOSE_LOKER_ID'  =>  $data[49],
              'AMCREW'	=>	$data[50]
            ];
          }
          else {
            echo "NOK#";
          }
        }
      }
      $records = array_chunk($data_to_insert, 500);
      DB::table('nonatero_detil_gaul')->where('THNBLN',$periode)->delete();
      foreach ($records as $batch) {
              print_r($batch);
              DB::table('nonatero_detil_gaul')
              ->insert($batch);
      }
      echo "Done !!!";
    } catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
    {
        die("The file doesn't exist");
    }
  }

  public function nonatero($periode){
    $filename = "upload3/nonatero/daily_detail_".$periode."_R6.txt";
    try
    {
      $content = File::get($filename);
      $grab = explode("\n", $content);
      $grab = str_replace('"', '', $grab);
      foreach($grab as $key=>$line) {
        if ($key<>0){
               $array[$key] = explode('|', $line);
                if (array_key_exists(35,$array[$key]) && $array[$key][35]=="Telkom KALSEL") {
                    $data = $array[$key];
                    $data_to_insert[] = [
                      'THNBLN'	=>	$data[0],
                      'THNBLN_OPEN'	=>	$data[1],
                      'THNBLNTGL_OPEN'	=>	$data[2],
                      'JAM_OPEN'	=>	$data[3],
                      'THNBLN_CLOSE'	=>	$data[4],
                      'THNBLNTGL_CLOSE'	=>	$data[5],
                      'JAM_CLOSE'	=>	$data[6],
                      'IS_GAMAS'	=>	$data[7],
                      'TROUBLENO_PARENT'	=>	$data[8],
                      'UNIT'	=>	$data[9],
                      'PLBLCL'	=>	$data[10],
                      'FLAG_TEKNIS'	=>	$data[11],
                      'JENIS_GGN'	=>	$data[12],
                      'STAT_INDI'	=>	$data[13],
                      'P'	=>	$data[14],
                      'CPROD'	=>	$data[15],
                      'PRODUK'	=>	$data[16],
                      'TROUBLE_OPENTIME'	=>	$data[17],
                      'TROUBLE_CLOSETIME'	=>	$data[18],
                      'TGL_TECH_CLOSE'	=>	$data[19],
                      'DATE_CLOSE'	=>	$data[20],
                      'STATUS'	=>	$data[21],
                      'ALPRO'	=>	$data[22],
                      'CHANNEL_ID'	=>	$data[23],
                      'CHANNEL'	=>	$data[24],
                      'SEGMENTASI'	=>	$data[25],
                      'SUBSEGMENTASI'	=>	$data[26],
                      'ACTUAL_SOLUTION'	=>	$data[27],
                      'TROUBLE_NO'	=>	$data[28],
                      'NCLI'	=>	$data[29],
                      'NDOS'	=>	$data[30],
                      'TROUBLE_NUMBER'	=>	$data[31],
                      'ND_REFERENCE'	=>	$data[32],
                      'REG'	=>	$data[33],
                      'CWITEL'	=>	$data[34],
                      'WITEL'	=>	$data[35],
                      'CDATEL'	=>	$data[36],
                      'DATEL'	=>	$data[37],
                      'STO'	=>	$data[38],
                      'CMDF'	=>	$data[39],
                      'MDF'	=>	$data[40],
                      'RK'	=>	$data[41],
                      'DP'	=>	$data[42],
                      'LAST_TCLOSE_LOKER_ID'	=>	$data[43],
                      'RESULT_HOLDRESOLVED'	=>	$data[44],
                      'AUTO_ASSIGNED'	=>	$data[45],
                      'JML_LAPUL'	=>	$data[46],
                      'IS_REOPEN'	=>	$data[47],
                      'TTR'	=>	$data[48],
                      'TTR_CONS'	=>	$data[49],
                      'KAT_PLG'	=>	$data[50],
                      'ONT_MANUFACTURE'	=>	$data[51],
                      'IS_3HS'	=>	$data[52],
                      'IS_6HS'	=>	$data[53],
                      'IS_12HS'	=>	$data[54],
                      'IS_24HS'	=>	$data[55],
                      'IS_36HS' =>  $data[56],
                      'IS_CLOSE_3JAM'	=>	$data[57],
                      'SYMPTOM_LV0'	=>	$data[58],
                      'SOLUTION1'	=>	$data[59],
                      'SOLUTION0'	=>	$data[60],
                      'LOKASI_GGN'	=>	$data[61],
                      'KONDISI_JARINGAN'	=>	$data[62],
                      'DESC_CATEGORY_SYMPTOM'	=>	$data[63],
                      'TK_WORKZONE'	=>	$data[64],
                      'WORKZONE_NAME'	=>	$data[65],
                      'AMCREW'	=>	$data[66],
                      'HEADLINE_GAMAS_INDUK'	=>	$data[67],
                      'UPDATE_TIME'	=>	$data[68]
                    ];


                }

        }
      }
      $records = array_chunk($data_to_insert, 500);
      DB::table('nonatero_detil')->where('THNBLN',$periode)->delete();
      foreach ($records as $batch) {
              DB::table('nonatero_detil')
              ->insert($batch);
      }
      echo "Done !!!";
    } catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
    {
        die("The file doesn't exist");
    }
  }

	public static function check_sc_not_sync($date){
		// $date = date('Y-m');
		$check = DB::SELECT('
			SELECT
			*
			FROM psb_myir_wo a
			LEFT JOIN dispatch_teknisi b ON a.sc = b.Ndem
			LEFT JOIN Data_Pelanggan_Starclick c ON a.sc = c.orderId
			WHERE
			b.tgl LIKE "'.$date.'%" AND
			c.orderId IS NULL
		');
		// print_r($check);
		if (count($check)>0){
			foreach ($check as $result){
				GrabController::sync2($result->sc);
				echo $result->sc."<br />";
			}
		}
	}

	public static function sync2($id){
    $order_id = $id;
    $ch = curl_init();
    $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1573355998402&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$order_id.'","Field":"ORDER_ID","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"","EndDate":"","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page=1&start=0&limit=10';

    // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1573355998402&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
    $get_cookie = DB::table('cookie_starclick')->get();
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    // echo urldecode($link);
    $headers = array();
    $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0';
    $headers[] = 'Accept: */*';
    $headers[] = 'Accept-Language: en-US,en;q=0.5';
    $headers[] = 'X-Requested-With: XMLHttpRequest';
    $headers[] = 'Connection: keep-alive';
    $headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
    $headers[] = $get_cookie[0]->cookie;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);

    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    } else {
      $result = json_decode($result);
      $allRecord = array();
      if (count($result)){
      $list = $result->data;

      if (count($list)){
      $list = $list->LIST;
			if (count($list)){
      foreach ($list as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
            if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array(
              "orderId"=>@$data->ORDER_ID,
              "orderName"=>str_replace("'","",@$data->CUSTOMER_NAME),
              "orderAddr"=>str_replace("'","",@$data->INS_ADDRESS),
              "orderNotel"=>@$data->POTS,
              "orderDate"=>$orderDate,
              "orderDatePs"=>$orderDatePs,
              "orderCity"=>str_replace("'","",@$data->CUSTOMER_ADDR),
              "orderStatus"=>@$data->STATUS_RESUME,
              "orderStatusId"=>@$data->STATUS_CODE_SC,
              "orderNcli"=>@$data->NCLI,
              "ndemSpeedy"=>@$data->SPEEDY,
              "ndemPots"=>@$data->POTS,
              "orderPaketID"=>@$data->ODP_ID,
              "kcontact"=>str_replace("'"," ",@$data->KCONTACT),
              "username"=>@$data->USERNAME,
              "alproName"=>@$data->LOC_ID,
              "tnNumber"=>@$data->POTS,
              "reservePort"=>@$data->ODP_ID,
              "jenisPsb"=>@$data->JENISPSB,
              "sto"=>@$data->STO,
              "lat"=>@$data->GPS_LATITUDE,
              "lon"=>@$data->GPS_LONGITUDE,
              "internet"=>@$internet,
              "noTelp"=>@$noTelp,
              "orderPaket"=>@$data->PACKAGE_NAME,
              "witel"=>@$data->WITEL);
        echo $data->ORDER_ID."<br />";
      }
      }
			}
      }
      echo "saving\n";
      $srcarr=array_chunk($allRecord,500);
      foreach($srcarr as $item) {
          //DB::table('proaktif')->insert($item);
          self::insertOrUpdate($item);
          if (count($srcarr)>0){
            DB::table('cookie_starclick')->update([
              'last_sync' => date('Y-m-d H:i:s')
            ]);
          }
      }
      return redirect('/customGrab')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> merubah SC Lama!']
      ]);
    }
    curl_close($ch);
  }

  public function sync(Request $req){
    $order_id = $req->sc;
    $ch = curl_init();
    $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1573355998402&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$order_id.'","Field":"ORDER_ID","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"","EndDate":"","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page=1&start=0&limit=10';

    // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1573355998402&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
    $get_cookie = DB::table('cookie_starclick')->get();
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    // echo urldecode($link);
    $headers = array();
    $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0';
    $headers[] = 'Accept: */*';
    $headers[] = 'Accept-Language: en-US,en;q=0.5';
    $headers[] = 'X-Requested-With: XMLHttpRequest';
    $headers[] = 'Connection: keep-alive';
    $headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
    $headers[] = $get_cookie[0]->cookie;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);

    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    } else {
      $result = json_decode($result);
      $allRecord = array();
      if (count($result)){
      $list = $result->data;

      if (count($list)){
      $list = $list->LIST;
      foreach ($list as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
            if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array(
              "orderId"=>@$data->ORDER_ID,
              "orderName"=>str_replace("'","",@$data->CUSTOMER_NAME),
              "orderAddr"=>str_replace("'","",@$data->INS_ADDRESS),
              "orderNotel"=>@$data->POTS,
              "orderDate"=>$orderDate,
              "orderDatePs"=>$orderDatePs,
              "orderCity"=>str_replace("'","",@$data->CUSTOMER_ADDR),
              "orderStatus"=>@$data->STATUS_RESUME,
              "orderStatusId"=>@$data->STATUS_CODE_SC,
              "orderNcli"=>@$data->NCLI,
              "ndemSpeedy"=>@$data->SPEEDY,
              "ndemPots"=>@$data->POTS,
              "orderPaketID"=>@$data->ODP_ID,
              "kcontact"=>str_replace("'"," ",@$data->KCONTACT),
              "username"=>@$data->USERNAME,
              "alproName"=>@$data->LOC_ID,
              "tnNumber"=>@$data->POTS,
              "reservePort"=>@$data->ODP_ID,
              "jenisPsb"=>@$data->JENISPSB,
              "sto"=>@$data->STO,
              "lat"=>@$data->GPS_LATITUDE,
              "lon"=>@$data->GPS_LONGITUDE,
              "internet"=>@$internet,
              "noTelp"=>@$noTelp,
              "orderPaket"=>@$data->PACKAGE_NAME,
              "witel"=>@$data->WITEL);
        echo $data->ORDER_ID."<br />";
      }
      }
      }
      echo "saving\n";
      $srcarr=array_chunk($allRecord,500);
      foreach($srcarr as $item) {
          //DB::table('proaktif')->insert($item);
          self::insertOrUpdate($item);
          if (count($srcarr)>0){
            DB::table('cookie_starclick')->update([
              'last_sync' => date('Y-m-d H:i:s')
            ]);
          }
      }
      return redirect('/customGrab')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> merubah SC Lama!']
      ]);
    }
    curl_close($ch);
  }

  public function syncx(Request $req)
  {
    //processing
     $arrContextOptions=array(
        "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        )
    );

    $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=".$req->sc."&Field=ORDER_ID&Fieldstatus=&Fieldwitel=&StartDate=&EndDate=&page=1&start=0&limit=1";
      $result = json_decode(@file_get_contents($link, false, stream_context_create($arrContextOptions)));
      echo $link."<br />";
      print_r(count($result->data));

      $order = $result->data;
      foreach ($order as $data){
        if ($data->ORDER_ID == "####"){
          return redirect('/customGrab')->with('alerts', [
            ['type' => 'warning', 'text' => '<strong>gagal</strong> ORDER ID tidak boleh Angka!']
          ]);
        }

        $get_where = DB::table("Data_Pelanggan_Starclick")->where("orderId", $data->ORDER_ID)->first();

        $orderDate = "";
        $orderDatePs = "";
        if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
        if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
        // dd($get_where)
        // dd($data);

        $KCONTACT  = str_replace("\\", '', $data->KCONTACT);
        $KCONTACT  = str_replace("'", '', $KCONTACT);
        $orderName = str_replace("'",'',$data->CUSTOMER_NAME);
        // dd($KCONTACT);

        if (!empty($get_where->orderId)){
          echo $data->ORDER_ID." UPDATED\n";
          $SQL = "
          UPDATE
            Data_Pelanggan_Starclick
          SET
            orderId='".$data->ORDER_ID."',
            orderKontak='".$data->PHONE_NO."',
            orderDate='".$orderDate."',
            orderDatePs='".$orderDatePs."',
            orderStatus='".$data->STATUS_RESUME."',
            orderStatusId='".$data->STATUS_CODE_SC."',
            orderNcli='".$data->NCLI."',
            orderPaket='".$data->PACKAGE_NAME."',
            lat = '".$data->GPS_LATITUDE."',
            lon = '".$data->GPS_LONGITUDE."',
            orderAddr = '".$data->INS_ADDRESS."',
            orderCity = '".$data->CUSTOMER_ADDR."',
            orderNotel = '".$data->POTS."',
            ndemSpeedy = '".$data->SPEEDY."',
            ndemPots = '".$data->POTS."',
            orderPaketID = '".$data->ODP_ID."',
            kcontact = '".$KCONTACT."',
            username = '".$data->USERNAME."',
            alproname = '".$data->LOC_ID."',
            tnNumber = '".$data->TN_NUMBER."',
            reservePort = '".$data->ODP_ID."',
            jenisPsb = '".$data->JENISPSB."',
            sto = '".$data->XS2."',
            orderName = '".$orderName."',
            reserveTn = '".$data->RNUM."'
          WHERE
            orderId='".$data->ORDER_ID."'
          ";
          // echo $SQL."\n";
          $update = DB::statement($SQL);
          return redirect('/customGrab')->with('alerts', [
            ['type' => 'success', 'text' => '<strong>SUKSES</strong> merubah SC Lama!']
          ]);
        } else {
          echo $data->ORDER_ID." INSERTED\n";
          $SQL = "
          INSERT IGNORE INTO
            Data_Pelanggan_Starclick
          (orderId,orderName,orderAddr,orderNotel,orderKontak,orderDate,orderDatePs,orderCity,orderStatus,orderStatusId,orderNcli,ndemSpeedy,ndemPots,orderPaketID,kcontact,username,alproName,tnNumber,reserveTn,reservePort,jenisPsb,sto,lat,lon)
          VALUES
          ('".$data->ORDER_ID."','".str_replace("'","",$data->CUSTOMER_NAME)."',
          '".str_replace("'","",$data->INS_ADDRESS)."','".$data->POTS."','".$data->PHONE_NO."','".$orderDate."','".$orderDatePs."','".str_replace("'","",$data->CUSTOMER_ADDR)."','".$data->STATUS_RESUME."',
          '".$data->STATUS_CODE_SC."','".$data->NCLI."','".$data->SPEEDY."','".$data->POTS."','".$data->ODP_ID."','".$KCONTACT."','".$data->USERNAME."','".$data->LOC_ID."','".$data->POTS."',
          '".$data->RNUM."','".$data->ODP_ID."','".$data->JENISPSB."','".$data->XS2."','".$data->GPS_LATITUDE."','".$data->GPS_LONGITUDE."')
          ";
          $insert = DB::statement($SQL);
          echo $SQL."\n";
          return redirect('/customGrab')->with('alerts', [
            ['type' => 'success', 'text' => '<strong>SUKSES</strong> menambah SC baru!']
          ]);
        }
      }

    //to view
    //return view('grab.index');
    return redirect('/customGrab')->with('alerts', [
      ['type' => 'warning', 'text' => '<strong>Gagal</strong> SC tidak ditemukan dalam kurun waktu 30 hari kebelakang!']
    ]);
  }
  public function a2s_absensi(){
    $this->a2s_absen(1);
    $this->a2s_absen(2);
  }
  public function a2s_absen($id){
    date_default_timezone_set('Asia/Makassar');
    $param = "end_date=".date("d")."%2F".date("m")."%2F".date("Y")."&p_regional_ta=%25&p_witel=44&alpro=".$id."&done=Done";
    $ch = curl_init();
    $url = 'https://apps.telkomakses.co.id/assurance/rta_detil_hadir.php?p_witel=44&start_date=&end_date='.date("d")."%2F".date("m")."%2F".date("Y").'&alpro='.$id.'&lama=0';
    echo $url;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $c = curl_exec($ch);
    //echo $c;
    $columns = array(
            1 =>
            'reg',
            'witel',
            'laborcode',
            'teknisi',
            'nik',
            'supervisor',
            'jenis_teknisi',
            'no_hp',
            'workzone',
            'jadwal',
            'hadir',
            'tgl',
            'div'
        );
    $dom = @\DOMDocument::loadHTML(trim($c));
    $tgl = date('Y-m-d');
    $div = $id;
    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
          if($j == 12){
            $data[$columns[$j]] = $tgl;
          }else if($j == 13){
            $data[$columns[$j]] = $div;
          }else{
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
          }
        }

        $result[] = $data;
    }
    //return $result;
    DB::table('a2s_kehadiran')->where('tgl', $tgl)->where('div', $div)->delete();
    DB::table('a2s_kehadiran')->insert($result);
  }
  public static function odp(){
    ini_set('max_execution_time', 60000);
    //DB::table('1_0_master_odp')->truncate();
    GrabController::odp_regional(6);

  }
  public static function odp_regional($reg){
    ini_set('max_execution_time', 60000);
    $filename = "ftp://UserFTP1:telkom135@10.65.10.238/SupportReport/Harian/odpe2e/".date('Ym')."04/odp_compliance_".date('Ym')."04_Regional 6.xlsx";
    //$handle = fopen($filename, "r");
    //$data_to_insert = array();
    $content = file_get_contents($filename);

    var_dump($content);
    dd();

    while(!feof($handle)){
      $data = explode(";", fgets($handle));
      if (count($data)>1){
          $data_to_insert[] = [
              'NOSS_ID'         => $data[0],
              'ODP_INDEX'       => $data[1],
              'ODP_NAME'        => $data[2],
              'LATITUDE'        => $data[3],
              'LONGITUDE'       => $data[4],
              'CLUSNAME'        => $data[5],
              'CLUSTERSTATUS'   => $data[6],
              'AVAI'            => $data[7],
              'USED'            => $data[8],
              'RSV'             => $data[9],
              'RSK'             => $data[10],
              'IS_TOTAL'        => $data[11],
              'REGIONAL'        => $data[12],
              'WITEL'           => $data[13],
              'DATEL'           => $data[14],
              'STO'             => $data[15],
              'STO_DESC'        => $data[16],
              'ODP_INFO'        => $data[17],
              'UPDATE_DATE'     => $data[18]
            ];

       }
    }
    $records = array_chunk($data_to_insert, 1000);
    $regional = "Regional ".$reg;
    DB::table('1_0_master_odp')->where('REGIONAL', $regional)->delete();
    foreach ($records as $batch) {
          try {
            DB::table('1_0_master_odp')
            ->insert($batch);
          }
          catch (\Exception $e) {

          }
    }

  }
  public function scDetail($id){
    $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=".$id."&Field=ORDER_ID&Fieldstatus=&Fieldwitel=&StartDate=&EndDate=&page=1&start=0&limit=1";
    $result = json_decode(@file_get_contents($link));
    return $order = $result->data;
  }

  public static function odpRegional(){
    ini_set('max_execution_time', 60000);
    $filename = "ftp://UserFTP1:telkom135@10.65.10.238/SupportReport/Harian/ODPOkupansi/".date('Y')."/".date('Ym')."/REPORT_ODP_TREG6_".date('Ymd').".txt";
    $handle = fopen($filename, "r");
    $data_to_insert = array();
    while(!feof($handle)){
       $data = explode(";", fgets($handle));
      if (count($data)>1){
          $data_to_insert[] = [
              'NOSS_ID'          => $data[0],
              'ODP_INDEX'        => $data[1],
              'ODP_NAME'         => $data[2],
              'LATITUDE'         => $data[3],
              'LONGITUDE'        => $data[4],
              'CLUSNAME'         => $data[5],
              'CLUSTERSTATUS'    => $data[6],
              'AVAI'             => $data[7],
              'USED'             => $data[8],
              'RSV'              => $data[9],
              'RSK'              => $data[10],
              'IS_TOTAL'         => $data[11],
              'REGIONAL'         => $data[12],
              'WITEL'            => $data[13],
              'DATEL'            => $data[14],
              'STO'              => $data[15],
              'STO_DESC'         => $data[16],
              'ODP_INFO'         => $data[17],
              'UPDATE_DATE'      => $data[18]
            ];
       }
    }
    $records = array_chunk($data_to_insert, 1000);
    $regional = "Regional 6";
    // DB::table('1_0_master_odp')->where('REGIONAL', $regional)->delete();
    DB::table('1_0_master_odp')->truncate();
    foreach ($records as $batch) {
      try {
              DB::table('1_0_master_odp')
               ->insert($batch);
          }
             catch (\Exception $e) {

          }
    }
    // DB::table('1_0_master_odp')->where('REGIONAL', 'REGIONAL')->delete();
  }

  public function grab_kpro_val(){
    // perform header
    date_default_timezone_set('Asia/Makassar');
    ini_set('max_execution_time', 60000);
    $ch = curl_init();

    $headers[] = "Accept: */*";
    $headers[] = "Connection: Keep-Alive";
    $headers[] = "Content-type: application/x-www-form-urlencoded;charset=UTF-8";
    curl_setopt($ch, CURLOPT_HTTPHEADER,  $headers);
    curl_setopt($ch, CURLOPT_HEADER,  0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/login.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS,"usr=850056&pwd=KalselHibat1");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $hasil=curl_exec($ch);

    echo $hasil;

    $postdata = http_build_query(
        array(
            "sql" => "SELECT distinct od.ORDER_ID, JENISPSB, REGIONAL, WITEL, DATEL, STO, LOC_ID, NCLI, POTS, SPEEDY, TYPE_TRANS, TYPE_LAYANAN, CUSTOMER_NAME, CONTACT_HP, INS_ADDRESS, GPS_LONGITUDE, GPS_LATITUDE, KCONTACT, STATUS_RESUME, STATUS_MESSAGE, to_char(order_date,'yyyy-mm-dd hh24:mi:ss') as order_date, to_char(last_updated_date,'yyyy-mm-dd hh24:mi:ss') as last_updated_date, STATUS_VOICE, STATUS_INET, STATUS_ONU, STATUS_REDAMAN, OLT_RX, ONU_RX, SNR_UP, SNR_DOWN, UPLOAD, DOWNLOAD, LAST_PROGRAM, CLID, to_char(last_start,'dd-mm-yyyy hh24:mi:ss') last_start, to_char(last_view,'dd-mm-yyyy hh24:mi:ss') last_view , to_char(ukur_time,'dd-mm-yyyy hh24:mi:ss') ukur_time, TINDAK_LANJUT, ISI_COMMENT, USER_ID_TL, to_char(TL_DATE,'dd-mm-yyyy hh24:mi:ss') as tgl_comment, to_char(SCHEDULE_LABOR,'dd-mm-yyyy hh24:mi:ss') as SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task, tek.device_id
from KPRO_DETAIL_ORDER od LEFT OUTER JOIN kpro_hasil_ukur uk on od.order_id=uk.order_id
LEFT OUTER JOIN (select order_id, SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task,device_id from kpro_detail_wfm where wo_parent is not null) tek on od.order_id=tek.order_id where status_resume = 'Process OSS (Provision Issued)' and (tindak_lanjut like 'Management Janji' or tindak_lanjut is null)  and  regional='6' and  1=1  and  1=1  and  type_trans='NEW SALES' and  1=1  and  1=1  and  1=1  order by order_date",
            "tombol" => "Download to Excel"
        )
    );

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/downloadreport.php');

    $context=curl_exec($ch);
    curl_close ($ch);

    //performe convert to variable
    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "regional", "witel", "datel", "sto", "order_id", "type_transaksi", "jenis_layanan", "alpro", "ncli", "pots","speedy", "status_resume", "status_message", "order_date", "last_update_sts", "nama_cust", "no_hp", "alamat", "kcontact", "lng", "lat","wfm_id", "status_wfm", "desk_task", "status_task", "tgl_install", "amcrew", "teknisi1", "hp_teknisi1", "teknisi2", "hp_teknisi2", "tinjut","ket","user","tgl_tinjut");

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    $jam    = date('G');

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['sts'] = "val";
        $data['jam'] = $jam;
        $result[] = $data;
    }

    DB::table("kpro_pi")->insert($result);
  }

  public function grab_kpro_tot(){
    // perform header
    date_default_timezone_set('Asia/Makassar');
    ini_set('max_execution_time', 60000);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/login.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS,"usr=850056&pwd=KalselHibat1");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $hasil=curl_exec($ch);

    echo $hasil;

    $postdata = http_build_query(
        array(
            "sql" => "SELECT distinct od.ORDER_ID, JENISPSB, REGIONAL, WITEL, DATEL, STO, LOC_ID, NCLI, POTS, SPEEDY, TYPE_TRANS, TYPE_LAYANAN, CUSTOMER_NAME, CONTACT_HP, INS_ADDRESS, GPS_LONGITUDE, GPS_LATITUDE, KCONTACT, STATUS_RESUME, STATUS_MESSAGE, to_char(order_date,'yyyy-mm-dd hh24:mi:ss') as order_date, to_char(last_updated_date,'yyyy-mm-dd hh24:mi:ss') as last_updated_date, STATUS_VOICE, STATUS_INET, STATUS_ONU, STATUS_REDAMAN, OLT_RX, ONU_RX, SNR_UP, SNR_DOWN, UPLOAD, DOWNLOAD, LAST_PROGRAM, CLID, to_char(last_start,'dd-mm-yyyy hh24:mi:ss') last_start, to_char(last_view,'dd-mm-yyyy hh24:mi:ss') last_view , to_char(ukur_time,'dd-mm-yyyy hh24:mi:ss') ukur_time, TINDAK_LANJUT, ISI_COMMENT, USER_ID_TL, to_char(TL_DATE,'dd-mm-yyyy hh24:mi:ss') as tgl_comment, to_char(SCHEDULE_LABOR,'dd-mm-yyyy hh24:mi:ss') as SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task, tek.device_id
            from KPRO_DETAIL_ORDER od LEFT OUTER JOIN kpro_hasil_ukur uk on od.order_id=uk.order_id
            LEFT OUTER JOIN (select order_id, SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task,device_id from kpro_detail_wfm where wo_parent is not null) tek on od.order_id=tek.order_id where ((status_resume not like 'Completed (PS)' and status_resume not like 'UN%' and lower(status_resume) not like ('%cancel%') and lower(status_resume) not like ('%revoke%')) or (status_resume like 'Completed (PS)' and TO_CHAR(last_updated_date,'yyyy-mm-dd') = TO_CHAR(CURRENT_DATE,'yyyy-mm-dd')))  and  regional='6' and  witel='BANJARMASIN' and  1=1  and  1=1  and  1=1  and  1=1  and  1=1  order by order_date",
            "tombol" => "Download to Excel"
        )
    );

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/downloadreport.php');

    $context=curl_exec($ch);
    curl_close ($ch);

    //performe convert to variable
    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "regional", "witel", "datel", "sto", "order_id", "type_transaksi", "jenis_layanan", "alpro", "ncli", "pots","speedy", "status_resume", "status_message", "order_date", "last_update_sts", "nama_cust", "no_hp", "alamat", "kcontact", "lng", "lat","wfm_id", "status_wfm", "desk_task", "status_task", "tgl_install", "amcrew", "teknisi1", "hp_teknisi1", "teknisi2", "hp_teknisi2", "tinjut","ket","user","tgl_tinjut");

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    $jam    = date('G');

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['sts'] = "tot";
        $data['jam'] = $jam;
        $result[] = $data;
    }

    DB::table("kpro_pi")->insert($result);
  }

  public function grabWifi(){
      $context = file_get_contents('https://dashboard.telkom.co.id/idwifi/public/ajaxdetailresume?kolom=&l0=&l1=&l2=&l3=&sql=&fokus=UBIS&ap_order=ORDER&mon_rep=MONITORING&startdate=2018%2F03%2F01&enddate=2018%2F03%2F07&group_ubis%5B%5D=CONS&group_ubis%5B%5D=EBIS&group_ubis%5B%5D=TSEL&group_ubis%5B%5D=PARTNERSHIP+SALES&group_ubis%5B%5D=PARTNERSHIP+SALES+AND+INSTALASI&group_ubis%5B%5D=PARTNERSHIP+JOIN+SSID&group_ubis%5B%5D=PARTNERSHIP+SALES%2C+INSTALASI+AND+AP+OWNER&group_ubis%5B%5D=OTH&group_ubis%5B%5D=DWS&treg%5B%5D=6&paket=ALL&p_on_air=0&dl=true');

      $dom = @\DOMDocument::loadHTML(trim($context));
      $columns = array("nom", "nsorder_quo", "createdon_quo", "userstatus_quo", "nsorder_ao", "createdon_ao", "userstatus_ao", "sid", "sotp_no", "nipnas", "name_sold", "region_sotp", "segmen_sotp", "shtp", "name_shtp", "region_shtp", "accountnas", "productid", "sero_id_fs", "status_abbreviation_fs", "status_date_fs", "sero_id_prov", "status_abbreviation_prov", "status_date_prov", "witel", "skema_bisnis", "groupstatus_tenoss", "ubis", "groupstatus", "reg_id", "projectname_prov", "projectname_fs", "amount_price", "jml_ap_attr", "billcomdate", "group_ubis", "task_name", "task_name_date", "tgl_pcom_ao", "kcontact", "createdby_quo", "nsorder_nkes", "createdby_nkes", "createdon_nkes", "userstatus_nkes", "createdby_ao", "address_sotp", "postalc_sotp", "city_sotp", "country_sotp", "address_shtp", "postalc_shtp", "city_shtp", "country_shtp", "btp_no", "name_btp", "address_btp", "postalc_btp", "city_btp", "country_btp", "region_btp", "productname", "bpemployresp", "nikemployresp", "name1resp", "cityresp", "nobpcp", "lnamecp", "address", "postalcodecp", "citycp", "regioncp", "cwitel", "process_type_id_ticares", "process_type_id_tenoss", "process_type", "currency", "jml_ap_tenoss", "cntrc_sdate", "billdate", "prodactdate", "tgl_cancel_quo", "tgl_cancel_nkes", "tgl_cancel_ao", "groupstatus_quo", "task_workgroup", "jml_ap_nms", "tgl_backhaul_done", "tgl_instap_done", "durasi_taskname", "durasi_fs_closed", "durasi_ao_closed", "ap_status", "partner_name", "dll");

      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr');
      $result = array();

      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
      }
      dd($result);
      DB::table("data_starclick_wifi")->insert($result);
  }

  public function grapA2s(){
    ini_set('max_execution_time', 60000);

    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    // curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/login.php');
    // curl_setopt($ch, CURLOPT_POSTFIELDS,"usr=850056&pwd=KalselHibat1");
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    // curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    // $hasil=curl_exec($ch);

    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    // curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/index.php?treg=6&witel=all&psb=NEW%20SALES&play=23p&hari=all&tl=all&mode=sto');
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    // curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    // $context=curl_exec($ch);
    // curl_close($ch);

    // $dom = @\DOMDocument::loadHTML(trim($context));
    // $columns = array("witel","unsc","re","not_va","va","prov_awl","prov_mj","prov_as_ex","prov_as_hi","prov_as_hplus","prov_val","prov_nok","prov_tot","acomp_ok","acomp_nok","prv_com","rw_os","comp_hi","comp_est","comp_dev","comp_h_1","fall_wfm","fall_uim","fall_asp","fall_osm","fall_nok","fall_tot","total_order");

    // dd($dom);
    // // dd($dom);
    // $table = $dom->getElementsByTagName('table')->item(0);
    // $rows = $table->getElementsByTagName('tr');
    // $result = array();

    // for ($i = 1, $count = $rows->length; $i < $count; $i++)
    // {
    //     $cells = $rows->item($i)->getElementsByTagName('td');
    //     $data = array();
    //     for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
    //     {
    //         $td = $cells->item($j);
    //         $data[$columns[$j]] =  $td->nodeValue;
    //     }
    //     $result[] = $data;
    // };

    // dd($result);

  /////////////////////////////////////////////////////

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);


    // assurance
    // curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/assurance/rta_detil_hadir.php?p_witel=44&start_date=&end_date=06/04/2018&alpro=1&lama=3');

    // provisioning
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/assurance/rta_detil_hadir.php?p_witel=44&start_date=&end_date=06/04/2018&alpro=2&lama=3');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no","regional","witel","laborcode","teknisi","nik","supervisor","jenis_teknisi","no_hp","workzone","jadwal","hadir");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };
    // dd($result);
    if (empty($result)){
      $text = 'Semua Teknisi Sudah Absen';
    }
    else{
        $text ='';
        foreach($result as $data){
          $text .= 'laborcode : '.$data['laborcode']."<br>".'NIK : '.$data['nik']."<br>".'Teknisi : '.$data['teknisi']."<br><br>";
        };
    }

    echo $text;
  }

  public function a2sGgnOpen()
  {
    ini_set('max_execution_time', 60000);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/assurance/rta_detil_ggn_open_idh.php?p_witel=44&lama=121');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no","ticketid", "nd", "jns_alpro", "lama_ggn", "witel", "workzone", "tgl_beckend", "trouble_headline", "laborcode", "schedule_date", "nama_plg", "alamat", "jns_ggn", "emosi", "kcontack", "datek");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };

    $count = count($result);
    $sql = "replace into a2s_detail_ggn_open_bank (no,ticketid, nd, jns_alpro, lama_ggn, witel, workzone, tgl_beckend, trouble_headline, laborcode, schedule_date, nama_plg, alamat, jns_ggn, emosi, kcontack, datek) values ";

    // $sql = "replace into a2s_detail_ggn_open_bank values ";

    $sparator = "";
    for ($a=0; $a<$count; $a++){
        $sql .= $sparator."('".$result[$a]["no"]."','".$result[$a]["ticketid"]."','".$result[$a]["nd"]."','".$result[$a]["jns_alpro"]."','".$result[$a]["lama_ggn"]."','".$result[$a]["witel"]."','".$result[$a]["workzone"]."','".$result[$a]["tgl_beckend"]."','".$result[$a]["trouble_headline"]."','".$result[$a]["laborcode"]."','".$result[$a]["schedule_date"]."','".$result[$a]["nama_plg"]."','".$result[$a]["alamat"]."','".$result[$a]["jns_ggn"]."','".$result[$a]["emosi"]."','".$result[$a]["kcontack"]."','".$result[$a]["datek"]."'),";
    }

    $sql = substr($sql, 0, -1);

    DB::table('a2s_detail_ggn_open')->truncate();
    DB::table("a2s_detail_ggn_open")->insert($result);
    DB::statement($sql);
  }

  public static function grabAlista($dt){
    if($dt == 'today'){
      $dt = date('Y-m-d');
    }
    elseif($dt == 'thismonth'){
      $dt = date('Y-m');
    };

    $gudang = array(
        ["id_gudang" => "GD0245", "nama_gudang" => "WH SO INV BANJARMASIN 2 (A.YANI)"],
        ["id_gudang" => "GD0244", "nama_gudang" => "WH SO INV BANJARMASIN 1 (BJM CENTRUM)"],
        ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
        ["id_gudang" => "GD0246", "nama_gudang" => "WH SO INV BATULICIN"],
        ["id_gudang" => "GD0247", "nama_gudang" => "WH SO INV BARABAI"],
        ["id_gudang" => "GD0248", "nama_gudang" => "WH SO INV TANJUNG TABALONG"],
        ["id_gudang" => "GD0243", "nama_gudang" => "WH SO INV BANJARBARU"]
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=850056&LoginForm%5Bpassword%5D=KalselHibat188&yt0=");
    $result = curl_exec($ch);

    echo"logged in \n";
    foreach($gudang as $g){
        $result = array();
        $no = 1;
        do {
          echo"\nke halaman ".$no." gudang ".$g['nama_gudang']." \n";
          curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/historypengeluaranproject&Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D='.str_replace(" ", "+", $g["nama_gudang"]).'&Permintaanpengambilanbarang%5Btgl_permintaan%5D='.$dt.'&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D=&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page='.$no);
          $als = curl_exec($ch);
          // dd($als);
          if(!strpos($als, 'No results found.')){
            $columns = array(
              'alista_id',
              'project',
              'nama_gudang',
              'tgl',
              'requester',
              'pengambil',
              'no_rfc',
              'nik_pemakai',
              'mitra',
              'id_permintaan'
            );
            $cals = str_replace('<a class="view" title="Lihat Detail Permintaan" href="javascript:detailpermintaan(&quot;', '', $als);
            $als = str_replace('&quot;)"><img src="images/find.png" alt="Lihat Detail Permintaan" /></a>', '', $cals);
            // dd($als);
            $dom = @\DOMDocument::loadHTML(trim($als));

            echo"convert string to variable ".$g['nama_gudang']." \n".strpos($als, "Last");
            $table = $dom->getElementsByTagName('table')->item(0);
            $rows = $table->getElementsByTagName('tr');
            for ($i = 2, $count = $rows->length; $i < $count; $i++)
            {
              $cells = $rows->item($i)->getElementsByTagName('td');
              $data = array();
              for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
              {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
              }
              $result[] = $data;
            }
            $no++;
          }
        }while(strpos($als, '<li class="last">'));
        // dd($result);
        $material = array();
        foreach($result as $noooooo => $r){
          echo "\nambil data ".$noooooo;
          curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/showdetailpermintaanoperation&id='.$r['id_permintaan']);
          $mtr = curl_exec($ch);
          $columnsx= array(2=>"id_barang", "nama_barang", "jumlah");
          $dom = @\DOMDocument::loadHTML(trim($mtr));
          $table = $dom->getElementsByTagName('table')->item(1);
          $rows = $table->getElementsByTagName('tr');

          for ($i = 1, $count = $rows->length; $i < $count; $i++)
          {
            $cells = $rows->item($i)->getElementsByTagName('td');
            for ($j = 2, $jcount = 4; $j <= $jcount; $j++)
            {
              $td = $cells->item($j);
              if($j==4){
                $data[$columnsx[$j]] =  explode(' ', $td->nodeValue)[0];
              }else{
                $data[$columnsx[$j]] =  $td->nodeValue;
              }
            }
            $material[] = array_merge($data,$r);
            // dd($material);
          }
        }
        DB::table('alista_material_keluar')->where('nama_gudang', $g["nama_gudang"])->where('tgl', 'LIKE', '%'.$dt.'%')->delete();
        DB::table('alista_material_keluar')->insert($material);
    }

    // add
    $data = DB::table('alista_material_keluar')->where('tgl',$dt)->get();
    foreach ($data as $d){
        $idItemBantu = $d->id_barang.'_'.$d->no_rfc;
        DB::table('alista_material_keluar')
            ->where('alista_id',$d->alista_id)
            ->where('id_barang',$d->id_barang)
            ->update([
                'id_item_bantu' => $idItemBantu
            ]);
    }
  }

  public function grabAlista2()
  {
      $date = date('Y-m-d');
      ini_set('max_execution_time', 60000);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
      $data = DB::table('akun')->where('user', '850056')->first();
      curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$data->user."&LoginForm%5Bpassword%5D=".$data->pwd."&yt0=");
      //curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=18950795&LoginForm%5Bpassword%5D=@telkom1234&yt0=");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
      curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);

      $rough_content = curl_exec($ch);
      // var_dump($rough_content);
      // dd($rough_content);

      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);

      $header_content = substr($rough_content, 0, $header['header_size']);
      $body_content = trim(str_replace($header_content, '', $rough_content));
      // // $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      // // preg_match_all ($pattern, $header_content, $matches);
      // $cookiesOut = $matches['cookie'][1] ;
      $header['errno'] = $err;
      $header['errmsg'] = $errmsg;
      $header['headers'] = $header_content;
      // $header['cookies'] = $cookiesOut;
      $cookie = null;
      $gudang = array(["id_gudang" => "GD0245", "nama_gudang" => "WH SO BANJARMASIN 2"],
        ["id_gudang" => "GD0244", "nama_gudang" => "WH SO BANJARMASIN 1"],
        ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
        ["id_gudang" => "GD0246", "nama_gudang" => "WH SO BATULICIN"],
        ["id_gudang" => "GD0247", "nama_gudang" => "WH SO KANDANGAN"],
        ["id_gudang" => "GD0248", "nama_gudang" => "WH SO TANJUNG TABALONG"],
        ["id_gudang" => "GD0243", "nama_gudang" => "WH SO BANJARBARU"]);
      $sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';
      foreach($gudang as $asd => $g){
        curl_setopt($ch, CURLOPT_URL, 'http://alista.telkomakses.co.id/index.php?r=gudang/reportingtransaksi');
        curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=".$g['id_gudang']."&Gudang%5Bstartdate%5D=".$date."&Gudang%5Benddate%5D=".$date."&yt0=");
        // curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
        $result = curl_exec($ch);
        // var_dump($result);
        // dd($result);

        $dom = @\DOMDocument::loadHTML($result);
        $table = $dom->getElementsByTagName('table')->item(1);
        $rows = $table->getElementsByTagName('tr');
        $columns = array(
                0 =>'no',
                'id',
                'tgl',
                'nama_gudang',
                'project',
                'pengambil',
                'mitra',
                'id_barang',
                'nama_barang',
                'jumlah',
                'no_rfc'
            );
        $result = array();
        for ($i = 1, $count = $rows->length; $i < $count; $i++)
        {
            $cells = $rows->item($i)->getElementsByTagName('td');
            $data = array();
            for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
            {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
            }
            $result[] = $data;
        }
        $data = $result;
        $count = count($data);
        if($count){
          for ($i = 0; $i < $count; $i++) {
            $sparator = ", ";
            // if($i==0 && $asd == 0){
            if($i==0){
              $sparator = "";
            }

            $proaktif_id = explode(" ",$data[$i]["project"])[0];
            $sql .= $sparator.'("'.$data[$i]['id'].'","'.$data[$i]['tgl'].'","'.$data[$i]['nama_gudang'].'"
              ,"'.$data[$i]['project'].'"
              ,"'.$data[$i]['pengambil'].'"
              ,"'.$data[$i]['mitra'].'"
              ,"'.$data[$i]['id_barang'].'"
              ,"'.$data[$i]['nama_barang'].'"
              ,"'.$data[$i]['jumlah'].'"
              ,"'.$data[$i]['no_rfc'].'"
              ,"'.$proaktif_id.'")';
          }
        }
      }
      echo $sql;
      // die;

      DB::table("alista_material_keluar")
      ->where("tgl", $date)
      ->delete();

      DB::statement($sql);

      // DB::connection('t1')->table("alista_material_keluar")
      // ->where("tgl", $date)
      // ->delete();

      // DB::connection('t1')->statement($sql);
  }

  public static function grabAlistaDate($start)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    $akun = DB::table('akun')->where('user', '850056')->first();
    $username = $akun->user;
    $password = $akun->pwd;

    // curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=91155635&LoginForm%5Bpassword%5D=@thebonk31&yt0=");
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$username."&LoginForm%5Bpassword%5D=".$password."&yt0=");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    $rough_content = curl_exec($ch);
    var_dump($rough_content);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1];
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookie = null;
    $gudang = array(["id_gudang" => "GD0245", "nama_gudang" => "WH SO BANJARMASIN 2"],
      ["id_gudang" => "GD0244", "nama_gudang" => "WH SO BANJARMASIN 1"],
      ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
      ["id_gudang" => "GD0246", "nama_gudang" => "WH SO BATULICIN"],
      ["id_gudang" => "GD0247", "nama_gudang" => "WH SO KANDANGAN"],
      ["id_gudang" => "GD0248", "nama_gudang" => "WH SO TANJUNG TABALONG"],
      ["id_gudang" => "GD0243", "nama_gudang" => "WH SO BANJARBARU"],
      ["id_gudang" => "GD175", "nama_gudang" => "WH SO INV BARABAI"]);

    //<option value="GD175">WH SO INV BARABAI</option>
    $tgl = str_replace('/', '-', $start);
    DB::table("alista_material_keluar")
    ->where("tgl", $tgl)
    ->delete();
    foreach($gudang as $asd => $g){
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/reportingtransaksi');
      curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=".$g['id_gudang']."&Gudang%5Bstartdate%5D=".$start."&Gudang%5Benddate%5D=".$start."&yt0=");
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML($result);
      $table = $dom->getElementsByTagName('table')->item(1);
      $rows = $table->getElementsByTagName('tr');
      $columns = array(
              0 =>'no',
              'id',
              'tgl',
              'nama_gudang',
              'project',
              'pengambil',
              'mitra',
              'id_barang',
              'nama_barang',
              'jumlah',
              'no_rfc'
          );
      $result = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
      }
      $data = $result;
      $count = count($data);
      if($count){
        $sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,pengambil_nama,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';

        for ($i = 0; $i < $count; $i++) {
          $sparator = ", ";
          if($i==0){
            $sparator = "";
          }
          $proaktif_id = explode(" ",$data[$i]["project"])[0];
          $pengambil = explode('#',$data[$i]["pengambil"]);
          if (count($pengambil)<2){
              $pengambil_nik  = $pengambil[0];
              $pengambil_nama = '';
          }
          else{
              $pengambil_nik  = $pengambil[0];
              $pengambil_nama = trim($pengambil[1]);
          };
          $sql .= $sparator.'("'.$data[$i]['id'].'","'.$data[$i]['tgl'].'","'.$data[$i]['nama_gudang'].'"
            ,"'.$data[$i]['project'].'"
            ,"'.$pengambil_nik.'"
            ,"'.$pengambil_nama.'"
            ,"'.$data[$i]['mitra'].'"
            ,"'.$data[$i]['id_barang'].'"
            ,"'.$data[$i]['nama_barang'].'"
            ,"'.$data[$i]['jumlah'].'"
            ,"'.$data[$i]['no_rfc'].'"
            ,"'.$proaktif_id.'")';
        }
        echo $sql;
        DB::statement($sql);
      }
    }
  }

  public static function grabAlistaKeluar()
  {
    $start = date('Y/m/d');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    $akun = DB::table('akun')->where('user', '850056')->first();
    $username = $akun->user;
    $password = $akun->pwd;

    // curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=91155635&LoginForm%5Bpassword%5D=@thebonk31&yt0=");
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$username."&LoginForm%5Bpassword%5D=".$password."&yt0=");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    $rough_content = curl_exec($ch);
    var_dump($rough_content);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1];
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookie = null;
    $gudang = array(["id_gudang" => "GD0245", "nama_gudang" => "WH SO BANJARMASIN 2"],
      ["id_gudang" => "GD0244", "nama_gudang" => "WH SO BANJARMASIN 1"],
      ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
      ["id_gudang" => "GD0246", "nama_gudang" => "WH SO BATULICIN"],
      ["id_gudang" => "GD0247", "nama_gudang" => "WH SO KANDANGAN"],
      ["id_gudang" => "GD0248", "nama_gudang" => "WH SO TANJUNG TABALONG"],
      ["id_gudang" => "GD0243", "nama_gudang" => "WH SO BANJARBARU"],
      ["id_gudang" => "GD175", "nama_gudang" => "WH SO INV BARABAI"]);

    //<option value="GD175">WH SO INV BARABAI</option>
    $tgl = str_replace('/', '-', $start);
    DB::table("alista_material_keluar")
    ->where("tgl", $tgl)
    ->delete();
    foreach($gudang as $asd => $g){
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/reportingtransaksi');
      curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=".$g['id_gudang']."&Gudang%5Bstartdate%5D=".$start."&Gudang%5Benddate%5D=".$start."&yt0=");
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML($result);
      dd($result);
      $table = $dom->getElementsByTagName('table')->item(1);
      $rows = $table->getElementsByTagName('tr');
      $columns = array(
              0 =>'no',
              'id',
              'tgl',
              'nama_gudang',
              'project',
              'pengambil',
              'mitra',
              'id_barang',
              'nama_barang',
              'jumlah',
              'no_rfc'
          );
      $result = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
      }
      $data = $result;
      $count = count($data);
      if($count){
        $sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,pengambil_nama,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';

        for ($i = 0; $i < $count; $i++) {
          $sparator = ", ";
          if($i==0){
            $sparator = "";
          }
          $proaktif_id = explode(" ",$data[$i]["project"])[0];

          $pengambil = explode('#',$data[$i]["pengambil"]);
          if (count($pengambil)<2){
              $pengambil_nik  = $pengambil[0];
              $pengambil_nama = '';
          }
          else{
              $pengambil_nik  = $pengambil[0];
              $pengambil_nama = trim($pengambil[1]);
          };

          $sql .= $sparator.'("'.$data[$i]['id'].'","'.$data[$i]['tgl'].'","'.$data[$i]['nama_gudang'].'"
            ,"'.$data[$i]['project'].'"
            ,"'.$pengambil_nik.'"
            ,"'.$pengambil_nama.'"
            ,"'.$data[$i]['mitra'].'"
            ,"'.$data[$i]['id_barang'].'"
            ,"'.$data[$i]['nama_barang'].'"
            ,"'.$data[$i]['jumlah'].'"
            ,"'.$data[$i]['no_rfc'].'"
            ,"'.$proaktif_id.'")';
        }
        echo $sql;
        DB::statement($sql);
      }
    }
  }
  public static function grabAlistaKembalian()
  {

    $start = date('Y/m/d');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    $akun = DB::table('akun')->where('id',8)->first();
    $username = $akun->user;
    $password = $akun->pwd;
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$username."&LoginForm%5Bpassword%5D=".$password."&yt0=");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    $rough_content = curl_exec($ch);
    var_dump($rough_content);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1];
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookie = null;
    $gudang = array(["id_gudang" => "GD0245", "nama_gudang" => "WH SO BANJARMASIN 2"],
      ["id_gudang" => "GD0244", "nama_gudang" => "WH SO BANJARMASIN 1"],
      ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
      ["id_gudang" => "GD0246", "nama_gudang" => "WH SO BATULICIN"],
      ["id_gudang" => "GD0247", "nama_gudang" => "WH SO KANDANGAN"],
      ["id_gudang" => "GD0248", "nama_gudang" => "WH SO TANJUNG TABALONG"],
      ["id_gudang" => "GD0243", "nama_gudang" => "WH SO BANJARBARU"],
      ["id_gudang" => "GD175", "nama_gudang" => "WH SO INV BARABAI"]);

    //<option value="GD175">WH SO INV BARABAI</option>
    $tgl = str_replace('/', '-', $start);
    // DB::table("alista_material_keluar")
    // ->where("tgl", $tgl)
    // ->delete();
    DB::statement('delete from alista_material_kembali where Tgl_Pengembalian = "'.$tgl.'"');
    foreach($gudang as $asd => $g){
      echo "get data gudang ".$g['nama_gudang']."\n";
      //$end = date("Y-m-t", strtotime($a_date));
      //curl_setopt($ch, CURLOPT_URL, "https://alista.telkomakses.co.id/index.php?r=gudang/downloadreportingtransaksi&idgudang=".$g['id_gudang']."&tglawal=".$a_date."&tglakhir=".$end."&id=3");
      curl_setopt($ch, CURLOPT_URL, "https://alista.telkomakses.co.id/index.php?r=gudang/downloadreportingtransaksi&idgudang=".$g['id_gudang']."&tglawal=".$tgl."&tglakhir=".$tgl."&id=3");
      //curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=".$g['id_gudang']."&Gudang%5Bstartdate%5D=".$start."&Gudang%5Benddate%5D=".$start."&yt0=");
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML($result);
      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr');
      $columns = array(
              1 =>'ID_Pengembalian',
              'Tgl_Pengembalian',
              'Nama_Gudang',
              'Project',
              'Petugas_Gudang',
              'ID_Barang',
              'Nama_Barang',
              'jumlah',
              'Satuan',
              'NO_RFC'
          );
      $result = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
      }
      $data = $result;

      $count = count($data);

      if($count){
        //dd($data);
        echo "saving\n";
        $srcarr=array_chunk($data,500);
        foreach($srcarr as $no => $insert) {
          echo "saving ".++$no."\n";
          self::insertOrUpdateWithTable($insert, 'alista_material_kembali');
        }

      }
    }
  }

  public function detailGangguanClose(){
    date_default_timezone_set('Asia/Makassar');
    $sekarang = date('d/m/Y');
    $awal     = date('d/m/Y', mktime(0, 0, 0, date("m"),1, date("Y")));

    ini_set('max_execution_time', 60000);
    $link = 'https://apps.telkomakses.co.id/assurance/rta_detil_ggn_close_3on3_gaul.php?loker_group=1&jns_channel=selected&jenis_ggn=1&workzone=&lama=5&p_witel=44&start_date='.$awal.'&end_date='.$sekarang.'&alpro=%25';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "ticket_id", "nd", "tgl_open", "status_manja", "tgl_manja", "tgl_close", "lama_ggn_jam", "is_gaul", "jenis_gangguan", "regional_ta", "witel", "workzone", "crew", "nama_teknisi", "trouble_headline", "layanan_ggn", "channel", "alpro", "datek", "actual_solution");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };

    $count = count($result);
    $sql = "replace into detail_gangguan_close (ticket_id, nd, tgl_open, status_manja, tgl_manja, tgl_close, lama_ggn_jam, is_gaul, jenis_gangguan, regional_ta, witel, workzone, crew, nama_teknisi, trouble_headline, layanan_ggn, channel, alpro, datek, actual_solution) values ";

    // $sql = "replace into a2s_detail_ggn_open_bank values ";

    $sparator = "";
    for ($a=0; $a<$count; $a++){
        $namaTeknisi      = str_replace("'", '', $result[$a]['nama_teknisi']);
        $troubleHeadline  = str_replace("'", '', $result[$a]['trouble_headline']);

        $sql .= $sparator."('".$result[$a]["ticket_id"]."','".$result[$a]["nd"]."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_open']))."','".$result[$a]["status_manja"]."','".$result[$a]['tgl_manja']."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_close']))."','".$result[$a]["lama_ggn_jam"]."','".$result[$a]["is_gaul"]."','".$result[$a]["jenis_gangguan"]."','".$result[$a]["regional_ta"]."','".$result[$a]["witel"]."','".$result[$a]["workzone"]."','".$result[$a]["crew"]."','".$namaTeknisi."','".$troubleHeadline."','".$result[$a]["layanan_ggn"]."','".$result[$a]["channel"]."','".$result[$a]["alpro"]."','".$result[$a]["datek"]."','".$result[$a]["actual_solution"]."'),";
    }

    $sql = substr($sql, 0, -1);
    // dd($sql);

    DB::table("detail_gangguan_close")->truncate();
    // DB::table("detail_gangguan_close")->insert($result);
    DB::statement($sql);
  }

  public static function grabAlistaStokNasionalBanjarmasinArea(){
    // $url = "https://alista.telkomakses.co.id/index.php?Stokgudangta%5Bnama_gudang%5D=Banjarmasin+-+Area&Stokgudangta%5Bnama_witel%5D=&Stokgudangta%5Bregional%5D=&Stokgudangta%5Bid_barang%5D=&Stokgudangta%5Bnama_barang%5D=&Stokgudangta%5Bnama_satuan%5D=&ajax=yw1&r=gudang%2Finfostoknasional&stokgudang_page=";

    echo"logging in\n";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $data = DB::table('akun')->where('user', '850056')->first();
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$data->user."&LoginForm%5Bpassword%5D=".$data->pwd."&yt0=");
    $result = curl_exec($ch);
    echo"logged in\n";

    echo"downloading\n";
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/downloadinfostoknasional');
    curl_setopt($ch, CURLOPT_POST, false);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            1 =>'nama_gudang',
            'regional',
            'id_barang',
            'nama_barang',
            'kategori',
            'total_stok',
            'nama_satuan'
        );
    $result = array();
    for ($i = 2, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        if($data['nama_gudang']=='Banjarmasin - Area'){
          $result[] = $data;
        }
    }

    echo"downloaded\n";
    //dd($result);

    DB::table('alista_stok')->truncate();
    DB::table('alista_stok')->insert($result);
    $alista = DB::table('alista_stok')->orderBy('id_barang', 'asc')->get();
    $msg = "Stok Gudang WH Banjarmasin-Area\n================================\n";
    foreach($alista as $da){
      $msg .= "<b>".$da->id_barang."</b> VOL : ".$da->total_stok."\n";
    }
    Telegram::sendMessage([
      'chat_id' => '-125769259',
      'text' => $msg,
      'parse_mode' => 'html'
    ]);
    // $dataarray = array();
    // for($h=1;$h<=17;$h++){
    //   curl_setopt($ch, CURLOPT_URL, $url.''.$h);
    //   curl_setopt($ch, CURLOPT_POST, false);
    //   $result = curl_exec($ch);

    //   $dom = @\DOMDocument::loadHTML($result);
    //     $table = $dom->getElementsByTagName('table')->item(0);
    //     $rows = $table->getElementsByTagName('tr');
    //     $columns = array(
    //             0 =>'nama_gudang',
    //             'nama_witel',
    //             'regional',
    //             'id_barang',
    //             'nama_barang',
    //             'total_stok',
    //             'nama_satuan'
    //         );
    //     $result = array();
    //     for ($i = 2, $count = $rows->length; $i < $count; $i++)
    //     {
    //         $cells = $rows->item($i)->getElementsByTagName('td');
    //         $data = array();
    //         for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
    //         {
    //             $td = $cells->item($j);
    //             $data[$columns[$j]] =  $td->nodeValue;
    //         }
    //         $result[] = $data;
    //         $dataarray[] = $data;
    //     }
    //     var_dump($result);
    //   DB::table('alista_stok')->insert($result);
    // }
    //asort($dataarray);
    // $msg = "Stok Gudang WH Banjarmasin-Area\n================================\n";
    // foreach($dataarray as $da){
    //   $msg .= "<code>".$da['id_barang']."</code> VOL : ".$da['total_stok']."\n";
    // }
    // Telegram::sendMessage([
    //   'chat_id' => '-125769259',
    //   'text' => $msg,
    //   'parse_mode' => 'html'
    // ]);
  }

  public function detailGangguanClose3on3(){
    date_default_timezone_set('Asia/Makassar');
    $sekarang = date('d/m/Y');
    $awal     = date('d/m/Y', mktime(0, 0, 0, date("m"),1, date("Y")));

    ini_set('max_execution_time', 60000);
    $link = "https://apps.telkomakses.co.id/assurance/rta_detil_ggn_close_3on3.php?loker_group=1&jns_channel=&jenis_ggn=&workzone=&lama=4&p_witel=44&start_date=".$awal."&end_date=".$sekarang."&alpro=%";

    echo $link;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "ticket_id", "nd", "tgl_open", "status_manja", "tgl_manja", "tgl_close", "lama_ggn_jam", "ttr3_jam", "ttr12_jam", "jenis_gangguan", "regional_ta", "witel", "workzone", "crew", "nama_teknisi", "trouble_headline", "layanan_ggn", "channel", "alpro", "datek", "actual_solution");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };

    $count = count($result);
    $sql = "replace into detail_gangguan_close_3on3 (ticket_id, nd, tgl_open, status_manja, tgl_manja, tgl_close, lama_ggn_jam, ttr3_jam, ttr12_jam, jenis_gangguan, regional_ta, witel, workzone, crew, nama_teknisi, trouble_headline, layanan_ggn, channel, alpro, datek, actual_solution) values ";

    // $sql = "replace into a2s_detail_ggn_open_bank values ";

    $sparator = "";
    for ($a=0; $a<$count; $a++){
        $namaTeknisi      = str_replace("'", '', $result[$a]['nama_teknisi']);
        $troubleHeadline  = str_replace("'", '', $result[$a]['trouble_headline']);

        $sql .= $sparator."('".$result[$a]["ticket_id"]."','".$result[$a]["nd"]."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_open']))."','".$result[$a]["status_manja"]."','".$result[$a]['tgl_manja']."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_close']))."','".$result[$a]["lama_ggn_jam"]."','".$result[$a]["ttr3_jam"]."','".$result[$a]["ttr12_jam"]."','".$result[$a]["jenis_gangguan"]."','".$result[$a]["regional_ta"]."','".$result[$a]["witel"]."','".$result[$a]["workzone"]."','".$result[$a]["crew"]."','".$namaTeknisi."','".$troubleHeadline."','".$result[$a]["layanan_ggn"]."','".$result[$a]["channel"]."','".$result[$a]["alpro"]."','".$result[$a]["datek"]."','".$result[$a]["actual_solution"]."'),";
    }

    $sql = substr($sql, 0, -1);
    // dd($sql);

    DB::table("detail_gangguan_close_3on3")->truncate();
    // DB::table("detail_gangguan_close")->insert($result);
    DB::statement($sql);
  }

  public static function starclickMin(){
        $startSync=30;
        $allRecord =[];
        for($i=1;$i<=5;$i++){
            $end = $startSync-7;
            echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
            echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
            if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
                $enddate = "31/12/2017";
            } else {
                $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
            }
            //ALL STO except GMB ULI

            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );

            $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Kalsel&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
            $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
            echo $link."\n";

            // print_r(count($result->data));
            $order = $result->data;
            foreach ($order as $data){
                //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
                $orderDate = "";
                $orderDatePs = "";
                if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
                if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
                if ($data->SPEEDY<>''){
                    $int = explode('~', $data->SPEEDY);
                    if (count($int)>1){
                        $internet = $int[1];
                    }else{
                        $internet = $data->SPEEDY;
                    }
                };
                if ($data->POTS<>''){
                    $telp = explode('~', $data->POTS);
                    if (count($telp)>1){
                        $noTelp = $telp[1];
                    }else{
                        $noTelp = $data->POTS;
                    }
                };
                echo "store ke array\n";
                $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Kalsel");
            }
            $startSync=$end-1;
        }
        echo "saving\n";
        $srcarr=array_chunk($allRecord,500);
        foreach($srcarr as $item) {
            //DB::table('proaktif')->insert($item);
            self::insertOrUpdate($item);
        }
    }

    public static function idosier(){
      ini_set('max_execution_time', 60000);
      $filename = "ftp://UserFTP1:telkom135@10.65.10.238/SupportReport/Bulanan/IDossierDatek/".date('Y-')."03/Data_I_Dossier_Datek_".date('Y-')."03_44_KALSEL.zip";
      file_put_contents("Tmpfile.zip", file_get_contents($filename));
      $zip = new \ZipArchive;
      $res = $zip->open('Tmpfile.zip');
      if ($res === TRUE) {
        $zip->extractTo(public_path().'/');
        $zip->close();
        echo "selesai extract \n";
      } else {
        echo 'doh!';
      }
      echo "reformat data \n";
      $content = file_get_contents(public_path()."/Data_I_Dossier_Datek_".date('Y-')."03_44_KALSEL.txt");

      $data_to_insert = array();
      $columns = array();
      foreach(explode("\r\n", $content) as $no => $line){
        $data = explode("|", $line);
        if($no){
          if (count($data)==81){
            foreach($data as $n => $d){
              $perform[$columns[$n]] = $d;
            }
            $data_to_insert[] = $perform;
          }
        }else{
          $columns = $data;
        }
      }
      DB::table('I_DOSSIER')->truncate();
      foreach (array_chunk($data_to_insert,500) as $tno => $t) {
        echo "insert batch data ke ".$tno."\n";
        DB::table('I_DOSSIER')->insert($t);
      }
      // dd($data_to_insert);
    }

    public static function insertOrUpdate(array $rows){
        $table = 'Data_Pelanggan_Starclick';
        $first = reset($rows);
        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );
        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                    array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                ).')';
            } , $rows )
        );
        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );
        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
        return \DB::statement( $sql );
    }

    public static function insertOrUpdateUnsc(array $rows){
        $table = 'Data_Pelanggan_UNSC';
        $first = reset($rows);
        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );
        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                    array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                ).')';
            } , $rows )
        );
        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );
        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
        return \DB::statement( $sql );
    }

    public static function insertOrUpdateWithTable(array $rows, $table){
        $first = reset($rows);
        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );
        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                    array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                ).')';
            } , $rows )
        );
        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );
        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
        return \DB::statement( $sql );
    }

    public static function grabAlistaByRfc($rfc){
      ini_set('max_execution_time', 60000);
      // $noRfc   = str_replace('-', '/', $rfc);
      // $gudangs = str_replace(' ', '%20', $gudang);
      $noRfc   = $rfc;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
      $data = DB::table('akun')->where('user', '850056')->first();
      curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$data->user."&LoginForm%5Bpassword%5D=".$data->pwd."&yt0=");
      //curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=18950795&LoginForm%5Bpassword%5D=@telkom1234&yt0=");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
      curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);

      $rough_content = curl_exec($ch);
      // var_dump($rough_content);
      // dd($rough_content);

      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);

      $header_content = substr($rough_content, 0, $header['header_size']);
      $body_content = trim(str_replace($header_content, '', $rough_content));
      $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      preg_match_all ($pattern, $header_content, $matches);
      $cookiesOut = $matches['cookie'][0] ;
      $header['errno'] = $err;
      $header['errmsg'] = $errmsg;
      $header['headers'] = $header_content;
      // $header['cookies'] = $cookiesOut;
      $cookie = null;

      // alista id
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D=&Permintaanpengambilanbarang%5Btgl_permintaan%5D=&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D='.$noRfc.'&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page=1&r=gudang%2Fhistorypengeluaranproject');

      $result = curl_exec($ch);

      $dom = @\DOMDocument::loadHTML($result);
      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr')->item(2)->getElementsByTagName('td');
      $idAlista = $rows->item(0)->nodeValue;

      //

      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);

      $header_content = substr($rough_content, 0, $header['header_size']);
      $body_content = trim(str_replace($header_content, '', $rough_content));
      $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      preg_match_all ($pattern, $header_content, $matches);
      $cookiesOut = $matches['cookie'][0] ;
      $header['errno'] = $err;
      $header['errmsg'] = $errmsg;
      $header['headers'] = $header_content;
      $cookie = null;

      $sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,pengambil_nama,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';

      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/downloadpengeluaranmaterial&a=&b=&c=&d=&e=&f='.$noRfc.'&g=&h=');
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      $result = curl_exec($ch);
      // var_dump($result);
      // dd($result);

      $dom = @\DOMDocument::loadHTML($result);
      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr');
      $columns = array(
              0 =>'alista_id',
              'tgl',
              'ket',
              'project',
              'no_rfc',
              'id_gudang',
              'nama_gudang',
              'regional',
              'id_barang',
              'nama_barang',
              'jumlah',
              'harga',
              'harga_total',
              'requester',
              'pengambilMaterial',
              'nik_pemakai',
              'mitra'
          );
      $result = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $data['alista_id'] = $idAlista;
          $result[] = $data;
      }
      $data = $result;

      if (count($data)<>0){
          DB::table("alista_material_keluar")->where("no_rfc", $noRfc)->delete();
          foreach($data as $datax){
              $dataPengambil = explode('#', $datax['pengambilMaterial']);
              if (count($dataPengambil)>1){
                  $nikPengambil  = $dataPengambil[0];
                  $namaPengambil = $dataPengambil[1];
              }
              else{
                  $nikPengambil  = $dataPengambil[0];
                  $namaPengambil = '';
              };

              $idItemBanntu = $datax['id_barang'].'_'.$datax['no_rfc'];
              $dataNikPengambil = array("pengambil" => $nikPengambil, "pengambil_nama"  => $namaPengambil, "id_item_bantu" => $idItemBanntu);

              $datax = array_merge_recursive($datax, $dataNikPengambil);
              unset($datax['ket'], $datax['regional'], $datax['harga'], $datax['harga_total'], $datax['id_gudang'], $datax['pengambilMaterial']);

              DB::table("alista_material_keluar")->insert($datax);
          }
        echo "sukses";
      }
      else{
        echo 'gagal';
      }
  }

  public static function baDigital($tgl){
      date_default_timezone_set('Asia/Makassar');
      if ($tgl=='today'){
          $tgl = date('Y-m-d');
      };

      ini_set('max_execution_time', 60000);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/dashboard_amalia/index.php?r=report/showWitel&excel=1&witel=75&tgl_mulai='.$tgl.'&tgl_selesai='.$tgl.'&tipe_teknisi=all_teknisi');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
      curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);

      $result = curl_exec($ch);
      // var_dump($result);

      $dom = @\DOMDocument::loadHTML($result);
      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr');
      $columns = array(
              0 =>'sto',
              'regional',
              'fiberzone',
              'witel',
              'tgl',
              'mitra',
              'pelanggan',
              'no_wo',
              'noTelp_pelanggan1',
              'noTelp_pelanggan2',
              'no_inet',
              'layanan',
              'nikTeknisi'
          );
    $result = array();
     for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          echo "data ".$i." save \n";
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
      }
      $data = $result;

    DB::table('ba_online')->whereDate('tgl',$tgl)->delete();
    DB::table('ba_online')->insert($data);
    echo "sukses \n";
  }

  public function dnsCheklistTl(){

  }

   public function detailGangguanClose3on3Fisik(){
    date_default_timezone_set('Asia/Makassar');
    $sekarang = date('d/m/Y');
    $awal     = date('d/m/Y', mktime(0, 0, 0, date("m"),1, date("Y")));

    ini_set('max_execution_time', 60000);
    // $link = "https://apps.telkomakses.co.id/assurance/rta_detil_ggn_close_3on3.php?loker_group=1&jns_channel=&jenis_ggn=&workzone=&lama=4&p_witel=44&start_date=".$awal."&end_date=".$sekarang."&alpro=%";
    $link = "https://apps.telkomakses.co.id/assurance/rta_detil_ggn_close_3on3.php?loker_group=1&jns_channel=selected&jenis_ggn=1&workzone=&lama=4&p_witel=44&start_date=".$awal."&end_date=".$sekarang."&alpro=%25";
    echo $link;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "ticket_id", "nd", "tgl_open", "status_manja", "tgl_manja", "tgl_close", "lama_ggn_jam", "ttr3_jam", "ttr12_jam", "jenis_gangguan", "regional_ta", "witel", "workzone", "crew", "nama_teknisi", "trouble_headline", "layanan_ggn", "channel", "alpro", "datek", "actual_solution");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };

    $count = count($result);
    $sql = "replace into detail_gangguan_close_3on3_fisik (ticket_id, nd, tgl_open, status_manja, tgl_manja, tgl_close, lama_ggn_jam, ttr3_jam, ttr12_jam, jenis_gangguan, regional_ta, witel, workzone, crew, nama_teknisi, trouble_headline, layanan_ggn, channel, alpro, datek, actual_solution) values ";

    // $sql = "replace into a2s_detail_ggn_open_bank values ";

    $sparator = "";
    for ($a=0; $a<$count; $a++){
        $namaTeknisi      = str_replace("'", '', $result[$a]['nama_teknisi']);
        $troubleHeadline  = str_replace("'", '', $result[$a]['trouble_headline']);

        $sql .= $sparator."('".$result[$a]["ticket_id"]."','".$result[$a]["nd"]."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_open']))."','".$result[$a]["status_manja"]."','".$result[$a]['tgl_manja']."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_close']))."','".$result[$a]["lama_ggn_jam"]."','".$result[$a]["ttr3_jam"]."','".$result[$a]["ttr12_jam"]."','".$result[$a]["jenis_gangguan"]."','".$result[$a]["regional_ta"]."','".$result[$a]["witel"]."','".$result[$a]["workzone"]."','".$result[$a]["crew"]."','".$namaTeknisi."','".$troubleHeadline."','".$result[$a]["layanan_ggn"]."','".$result[$a]["channel"]."','".$result[$a]["alpro"]."','".$result[$a]["datek"]."','".$result[$a]["actual_solution"]."'),";
    }

    $sql = substr($sql, 0, -1);
    // dd($sql);

    DB::table("detail_gangguan_close_3on3_fisik")->truncate();
    // DB::table("detail_gangguan_close")->insert($result);
    DB::statement($sql);
    echo "done";
  }

  public static function grabAlistaVersi2($dt){
    if($dt == 'today'){
      $dt = date('Y-m-d');
    }
    elseif($dt == 'thismonth'){
      $dt = date('Y-m');
    };

    $gudang = array(
        ["id_gudang" => "GD0245", "nama_gudang" => "WH SO INV BANJARMASIN 2 (A.YANI)"],
        ["id_gudang" => "GD0244", "nama_gudang" => "WH SO INV BANJARMASIN 1 (BJM CENTRUM)"],
        ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
        ["id_gudang" => "GD0246", "nama_gudang" => "WH SO INV BATULICIN"],
        ["id_gudang" => "GD0247", "nama_gudang" => "WH SO INV BARABAI"],
        ["id_gudang" => "GD0248", "nama_gudang" => "WH SO INV TANJUNG TABALONG"],
        ["id_gudang" => "GD0243", "nama_gudang" => "WH SO INV BANJARBARU"]
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $data = DB::table('akun')->where('id', '8')->first();
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$data->user."&LoginForm%5Bpassword%5D=".$data->pwd."&yt0=");
    // curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=850056&LoginForm%5Bpassword%5D=KalselHibat188&yt0=");
    $result = curl_exec($ch);

    echo"logged in \n";
    $data = DB::table('alista_material_keluar')->where('tgl', 'LIKE', '%'.$dt.'%')->delete();

    foreach($gudang as $g){
        $result = array();
        $no = 1;
        do {
          echo"\nke halaman ".$no." gudang ".$g['nama_gudang']." \n";
          curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/historypengeluaranproject&Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D='.str_replace(" ", "+", $g["nama_gudang"]).'&Permintaanpengambilanbarang%5Btgl_permintaan%5D='.$dt.'&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D=&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page='.$no);
          $als = curl_exec($ch);
          // dd($als);
          if(!strpos($als, 'No results found.')){
            $columns = array(
              'alista_id',
              'project',
              'nama_gudang',
              'tgl',
              'requester',
              'pengambil',
              'no_rfc',
              'nik_pemakai',
              'mitra',
              'id_permintaan'
            );
            $cals = str_replace('<a class="view" title="Lihat Detail Permintaan" href="javascript:detailpermintaan(&quot;', '', $als);
            $als = str_replace('&quot;)"><img src="images/find.png" alt="Lihat Detail Permintaan" /></a>', '', $cals);
            // dd($als);
            $dom = @\DOMDocument::loadHTML(trim($als));

            echo"convert string to variable ".$g['nama_gudang']." \n".strpos($als, "Last");
            $table = $dom->getElementsByTagName('table')->item(0);
            $rows = $table->getElementsByTagName('tr');
            for ($i = 2, $count = $rows->length; $i < $count; $i++)
            {
              $cells = $rows->item($i)->getElementsByTagName('td');
              $data = array();
              for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
              {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
              }
              $result[] = $data;
            }
            $no++;
          }
        }while(strpos($als, '<li class="last">'));
        // dd($result);
        $material = array();
        foreach($result as $noooooo => $r){
          echo "\nambil data ".$noooooo." Alista ID ".$r['alista_id'];
          // curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/showdetailpermintaanoperation&id='.$r['id_permintaan']);
          // curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/viewdetailpengambilanbarang&id='.$r['alista_id']);
          curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/downloadpengeluaranmaterial&a='.$r['alista_id'].'&b=&c=&d=&e=&f=&g=&h=');

          $mtr = curl_exec($ch);
          $columnsx= array(2=>"ket", "project", "rfc", "id_gudang", "nama_gudang", "regional", "id_barang", 'nama_barang', "jumlah", "harga", "harga_total", "req", "pengambil", "nik_pemakai", "peruntukan");
          $dom = @\DOMDocument::loadHTML(trim($mtr));
          $table = $dom->getElementsByTagName('table')->item(0);
          $rows = $table->getElementsByTagName('tr');

          for ($i = 1, $count = $rows->length; $i < $count; $i++)
          {
            $cells = $rows->item($i)->getElementsByTagName('td');
            for ($j = 2, $jcount = 16; $j <= $jcount; $j++)
            {
              $td = $cells->item($j);
              $data[$columnsx[$j]] =  $td->nodeValue;
            }

            unset($data['ket'],$data['rfc'],$data['id_gudang'],$data['regional'],$data['req'],$data['peruntukan']);
            $material[] = array_merge($data,$r);
          }
        }
        // dd($material);
        // DB::table('alista_material_keluar')->where('nama_gudang', $g["nama_gudang"])->where('tgl', 'LIKE', '%'.$dt.'%')->delete();
        DB::table('alista_material_keluar')->insert($material);
    }

    // add
    $data = DB::table('alista_material_keluar')->where('tgl',$dt)->get();
    foreach ($data as $d){
        $idItemBantu = $d->id_barang.'_'.$d->no_rfc;
        DB::table('alista_material_keluar')
            ->where('alista_id',$d->alista_id)
            ->where('id_barang',$d->id_barang)
            ->update([
                'id_item_bantu' => $idItemBantu
            ]);
    }
  }

  public static function grabNossaByDb($tgl){
    if ($tgl == 'today'){
        $tgl = date('Y-m-d');
    };

    $user_nossa = GrabModel::user_nossa()[0];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $login = curl_exec($ch);

    $dom = @\DOMDocument::loadHTML(trim($login));
    $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");
    //$value = $input->item($i)->getAttribute("value");
    print_r($input);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
    $result = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
    curl_setopt($ch, CURLOPT_POST, false);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
    $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");

    // getData
    $status = '';
    $getData = DB::table('data_nossa_1_log')->where('Reported_Date','LIKE','%'.$tgl.'%')->get();
    foreach($getData as $data){
        if (substr($data->Incident, 0, 3)<>'INT'){
            $status .= $data->Incident.',';
        }
    };

    $status = substr($status, 0, -1);

    $event = json_encode(array((object)array(
      "type"=>"setvalue",
      "targetId"=>"mx".$user_nossa->incident,
      "value"=>$status,
      "requestType"=>"ASYNC",
      "csrftokenholder"=>$csrftokenholder
    ),(object)array(
      "type"=>"filterrows",
      "targetId"=>"mx460",
      "value"=>"",
      "requestType"=>"SYNC",
      "csrftokenholder"=>$csrftokenholder)));

    $postdata = http_build_query(
      array(
          "uisessionid" => $uisesid,
          "csrftoken" => $csrftokenholder,
          "currentfocus" => "mx1116",
          "scrollleftpos" => "0",
          "scrolltoppos" => "0",
          "requesttype" => "SYNC",
          "responsetype" => "text/xml",
          "events" => $event
      )
    );

    //print_r($ postdata);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $result = curl_exec($ch);
    // print_r($result);
    //
    // dd($result);
    $posisi = strpos($result,'<component vis="true" id="mx310_holder" compid="mx310"><![CDATA[<a ctype="label"  id="mx310"  href="')+100;
    //echo "posisi : ".$posisi;
    $asd = substr_replace($result, "", 0, $posisi);
    $asd = substr_replace($asd, "", strpos($asd,'" onmouseover="return noStatus();" target="_blank"   noclick="1"   targetid="mx310"    class="text tht  " style="display:block;;;;;" title="Download"'), strlen($asd));
    echo "test ".($asd)." #";
    curl_setopt($ch, CURLOPT_URL, $asd);

    $result = curl_exec($ch);
    curl_close ($ch);
    $result=str_replace('&nbsp;', ' ', $result);
    // dd($result);

        $columns = array("Incident", "Customer_Name", "Contact_Name","Contact_Phone","Contact_Email","Summary", "Owner_Group", "Owner", "Last_Updated_Work_Log", "Last_Work_Log_Date", "Count_CustInfo", "Last_CustInfo", "Assigned_to","Booking_Date", "Assigned_by","Reported_Priority","Source","Subsidiary","External_Ticket_ID", "External_Ticket_Status","Segment","Channel","Customer_Segment","Closed_By","Customer_ID","Service_ID","Service_No","Service_Type","Top_Priority","SLG","Technology","Datek","RK_Name","Induk_Gamas","Reported_Date","LAPUL","GAUL","TTR_Customer","TTR_Nasional","TTR_Regional","TTR_Witel","TTR_Mitra","TTR_Agent","TTR_Pending","Status","Hasil_Ukur","OSM_Resolved_Code","Last_Update_Ticket","Status_Date","Resolved_By","Workzone","Witel","Regional","Incident_Symptom","Solution_Segment","Actual_Solution","ID");
    //$result = ;
    $dom = @\DOMDocument::loadHTML(trim($result));
    // print_r($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 0, $jcount = count($columns)-1; $j < $jcount; $j++)
      {
          $td = $cells->item($j);
          if ($j==13){
              if($td->nodeValue<>""){
                  $jam = date("Y-m-d H:i:s",strtotime($td->nodeValue));
              }
              else{
                  $jam = "";
              };
          };
          if ($j==13) {
            $new_datetime = "";
            $old_datetime = explode(' ', $td->nodeValue);
            if (count($old_datetime)>0){
              $old_date = explode('-', $old_datetime[0]);
              $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
              $new_datetime = $new_date.' '.$old_datetime[1];
              $data[$columns[$j]] = $new_datetime;
            }
          }

          if ($j==34) {
            $old_datetime = explode(' ', $td->nodeValue);
            if (count($old_datetime)>0){
              $old_date = explode('-', $old_datetime[0]);
              $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
              $new_datetime = $new_date.' '.$old_datetime[1];

              $getTime = explode(':', $old_datetime[1]);

              if ($getTime[0]>=17 || $getTime[0]<8) {
                $data['Hold'] = '1';
              } else {
                $data['Hold'] = '0';
              }

              if ($getTime[0]>=17) {
                $newDatetime = new DateTime($new_date);
                $newDatetime->modify('+1 day');
                $data['UmurDatetime'] = $newDatetime->format('Y-m-d 08:00:01');
                //echo $newDatetime->format('Y-m-d 08:00:01');
              } else if ($getTime[0]<8) {
                $data['UmurDatetime'] = date($new_date.' 08:00:02');

              } else {
                $data['UmurDatetime'] = $new_datetime;
              }

              $data[$columns[$j]] = $new_datetime;

            } else {
              $data[$columns[$j]] = str_replace('&nbsp;', '', $td->nodeValue);
            }
          } else {
          $data[$columns[$j]] =  str_replace('&nbsp;', '', $td->nodeValue);
          }
          if($j==0) {
            $data['ID'] =  substr($td->nodeValue,2);
              //echo $td->nodeValue."<br />";

          };



      }
      $data['Booking_Date'] = $jam;
      $data['update_grab'] = date('Y-m-d H:i:s');
      $result[] = $data;

    };

    $inHapus = '';
    foreach($result as $r){
        if(count($r['Incident']) <> NULL){
            $inHapus .= $r['Incident'].',';
        }
    };

    $statushapus = explode(',', $inHapus);
    $final = implode("','", $statushapus);
    $final = "'".$final."'";

    DB::select("DELETE FROM data_nossa_1_log WHERE Incident IN (".$final.")");
    DB::table('data_nossa_1_log')->insert($result);
    DB::table('data_nossa_1')->insertIgnore($result);
    echo "sukses";
  }

  public static function starclickMinKalbar(){
    $startSync=30;
    $allRecord =[];
    for($i=1;$i<=5;$i++){
        $end = $startSync-7;
        echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
        echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
            $enddate = "31/12/2017";
        } else {
            $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        }
        //ALL STO except GMB ULI

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Kalbar&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
        $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
        echo $link."\n";

        // print_r(count($result->data));
        $order = $result->data;
        foreach ($order as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
            if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Kalbar");
        }
        $startSync=$end-1;
    }
    echo "saving\n";
    $srcarr=array_chunk($allRecord,500);
    foreach($srcarr as $item) {
        //DB::table('proaktif')->insert($item);
        self::insertOrUpdate($item);
    }
  }

  public static function starclickMinBalikpapan(){
    $startSync=30;
    $allRecord =[];
    for($i=1;$i<=5;$i++){
        $end = $startSync-7;
        echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
        echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
            $enddate = "31/12/2017";
        } else {
            $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        }
        //ALL STO except GMB ULI

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Balikpapan&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
        $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
        echo $link."\n";

        // print_r(count($result->data));
        $order = $result->data;
        foreach ($order as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
            if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Balikpapan");
        }
        $startSync=$end-1;
    }
    echo "saving\n";
    $srcarr=array_chunk($allRecord,500);
    foreach($srcarr as $item) {
        //DB::table('proaktif')->insert($item);
        self::insertOrUpdate($item);
    }
  }

  public static function starclickMinKaltara(){
    $startSync=30;
    $allRecord =[];
    for($i=1;$i<=5;$i++){
        $end = $startSync-7;
        echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
        echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
            $enddate = "31/12/2017";
        } else {
            $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        }
        //ALL STO except GMB ULI

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Kaltara&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
        $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
        echo $link."\n";

        // print_r(count($result->data));
        $order = $result->data;
        foreach ($order as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
            if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Kaltara");
        }
        $startSync=$end-1;
    }
    echo "saving\n";
    $srcarr=array_chunk($allRecord,500);
    foreach($srcarr as $item) {
        //DB::table('proaktif')->insert($item);
        self::insertOrUpdate($item);
    }
  }

  public static function starclickMinSamarinda(){
    $startSync=30;
    $allRecord =[];
    for($i=1;$i<=5;$i++){
        $end = $startSync-7;
        echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
        echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
            $enddate = "31/12/2017";
        } else {
            $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        }
        //ALL STO except GMB ULI

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Samarinda&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
        $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
        echo $link."\n";

        // print_r(count($result->data));
        $order = $result->data;
        foreach ($order as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
            if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Samarinda");
        }
        $startSync=$end-1;
    }
    echo "saving\n";
    $srcarr=array_chunk($allRecord,500);
    foreach($srcarr as $item) {
        //DB::table('proaktif')->insert($item);
        self::insertOrUpdate($item);
    }
  }

  public static function starclickMinKalteng(){
    $startSync=30;
    $allRecord =[];
    for($i=1;$i<=5;$i++){
        $end = $startSync-7;
        echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
        echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
            $enddate = "31/12/2017";
        } else {
            $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        }
        //ALL STO except GMB ULI

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Kalteng&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
        $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
        echo $link."\n";

        // print_r(count($result->data));
        $order = $result->data;
        foreach ($order as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
            if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Kalteng");
        }
        $startSync=$end-1;
    }
    echo "saving\n";
    $srcarr=array_chunk($allRecord,500);
    foreach($srcarr as $item) {
        //DB::table('proaktif')->insert($item);
        self::insertOrUpdate($item);
    }
  }




}
