@extends('layout')

@section('content')
@include('partial.alerts')  
  <div class="panel panel-primary">
    <div class="panel-heading">Split Saldo DC</div>
    <div class="panel-body">
        <form method="POST">
            <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="rfc">NO. RFC</label>
                      <input type="text" name="rfc" class="form-control">
                      {!! $errors->first('rfc','<div class="label label-danger">:message</div>') !!}
                  </div>              
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                      <label for="rfc">Gudang</label>
                      <select name="gudang" id="gudang" class="form-control">
                        @foreach($gudang as $g)
                          <option value="{{ $g }}">{{ $g }}</option>
                        @endforeach
                      </select>
                      {!! $errors->first('gudang','<div class="label label-danger">:message</div>') !!}
                  </div>
                </div>

                  <div class="col-md-12">
                      <input type="submit" value="Proses" class="btn btn-primary">
                  </div>

            </div>
        </form>
    </div>
  </div>
@endsection

@section('plugins')
  <script type="text/javascript">
      $(function() {
          $("#gudang").select2();
      });
  </script>
@endsection
