
@extends('layout')

@section('content')
  @include('partial.alerts')
      <a href="/migrasi/monitoring/progress/form" class="btn btn-primary btn-sm">
        Tambah 
      </a><br><br>
      
      @foreach($listCluster as $cluster)
          <div class="panel panel-default">
              <div class="panel-heading">{{ $cluster->nama_cluster}}</div>
              <div class="panel-body">
                  <div class="table-responsive">          
                      <table class="table">
                        <thead>
                          <tr>
                            <th>ID / Regu Mitra</th>
                            <th>Progress (%)</th>
                            <th>Action</th>
                          </tr>
                        </thead>
             
                        @foreach($listProgress as $no=>$progress)
                            @if($progress->clusterId==$cluster->id_cluster)
                              <tbody>
                                  <tr>
                                      <td><label class="label label-primary">{{ $progress->reguId }}</label>{{ $progress->uraian }}</td>
                                      <td>{{ $progress->progress }}</td>
                                      <td>
                                          <a href="/migrasi/progress/{{ $progress->id }}/edit" class="label label-primary">Ubah Persentase</a>
                                          <a href="" class="label label-success">Dispatch</a>
                                      </td>
                                  </tr>
                              </tbody>
                            @endif
                        @endforeach
                      </table>
                  </div>
              </div>
          </div>
      @endforeach
  </script>
@endsection