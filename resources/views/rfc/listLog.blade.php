@extends('tech_layout')

@section('content')
@include('partial.alerts')
 <h4>Log RFC Teknisi</h4>

  @if(count($datas) <> 0)
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>#</th>
      		    <th>Tanggal RFC</th>
              <th>RFC</th>
            </tr>
          </thead>
          <tbody>
              @foreach($datas as $no=>$data)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $data->createdAt }}</td>
                    <td>{{ $data->rfc }}</td>
                </tr>
              @endforeach
          </tbody>
        </table>
    </div>
  @endif
@endsection
