<div class="table-responsive">
        <table class="table table-striped table-bordered ">
          <thead>
            <tr>
            <th width="5%">#</th>
            <th width="30%">Action</th>
            <th>Status</th>
            <th>Nama ODP</th>
            <th>Keterangan</th>
            <th width="10%">Flagging</td>
            <th width="10%">SC</td>
            <th>SC Status</th>
            <th>Created at</th>
            <th width="10%">Tanggal Deal</th>
            <th width="10%">Transaksi</th>
            <th width="10%">Jenis Layanan</th>
            <th>Nama Sales</th>
            <th width="10%">Nama</th>
            <th width="10%">No Internet</th>
            <th width="10%">No Telp</th>
            <th width="10%">Kordinat Pelanggan</th>
            <th width="10%">Kordinat ODP</th>
            <th width="10%">Alamat</th>
            <th>PIC</th>
            <th width="10%">Paket</th>
            <th width="10%">Harga</th>
            <th width="10%">KContact</th>
            </tr>
          </thead>
          <tbody>
            @foreach($list as $no => $data)

                <tr>
                <td>{{ ++$no }}</td>
                <td>
					        <a href="/dshr-transaksi-telegram/{{ $data->id }}" class="label label-info">Send To Telegram</a>
                </td>
                <td>
                  <a  href="/dshr-transaksi/{{ $data->id }}" class="label label-info">{{ $data->status_hdesk ? : 'EDIT' }}</a>
                </td>
                <td>
                    {{ $data->nama_odp }}
                </td>
                <td>
                  {{ $data->keterangan_hdesk }}
                </td>
        				<td>
                  {{ $data->flagging }}
                </td>
                <td>{{ $data->kode_sc }}</td>
                <td>{{ $data->orderStatus }}</td>
                <td>{{ $data->created_at }}</td>
                <td>
                  {{ $data->tanggal_deal }}
                </td>
                <td>
                  {{ $data->tipe_transaksi }}
                </td>
                <td>
                  {{ $data->jenis_layanan }}
                </td>
                <td>
                  {{ $data->sales }}
                </td>
                <td>
                  <b>{{ strtoupper($data->nama_pelanggan) }}</b>
                </td>
                <td>
                  {{ $data->no_internet }}
                </td>
                <td>
                  {{ $data->no_telp }}</td>
				        <td>
                  {{ $data->koor_pel }}
                </td>
                <td>
                  {{ $data->koor_odp }}
                </td>
                <td>
                  {{ $data->alamat }}
                </td>
                <td>
                  {{ $data->pic }}
                </td>
                <td>
                  {{ $data->paket_deal }}
                </td>
                <td>
                  {{ $data->harga_deal }}
                </td>
                <td>
                  PTTA;{{ $data->nik_sales }};{{ $data->paperless }};{{ $data->pic }}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
