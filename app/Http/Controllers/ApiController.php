<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
Use App\DA\AbsensiModel;
Use App\DA\DashboardModel;
use App\DA\PsbModel;
class ApiController extends Controller
{

  protected $QC_foto = ['Meteran_Rumah','Lokasi','Lokasi_1','Lokasi_2', 'ODP','ODP_1','ODP_2','Redaman_Pelanggan', 'SN_ONT' ,'SN_STB', 'Live_TV', 'Speedtest', 'Berita_Acara', 'Telephone_Incoming', 'Foto_Pelanggan_dan_Teknisi','additional_1','additional_2','additional_3','additional_4','additional_5'];

  public static function batch_send_to_dalapa($date){
    $get_sc = PsbModel::batch_get_data_for_dalapa($date);
    foreach ($get_sc as $result){
      ApiController::send_to_dalapa($result->id_dt);
      echo "NDEM : ".$result->Ndem." ; SC.".$result->orderId." <br />";
    }
  }

  public static function send_to_dalapa($id){
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL,"https://dalapa.id/api/dalapa/port");
    // curl_setopt($ch, CURLOPT_POST, 1);
    // curl_setopt($ch, CURLOPT_POSTFIELDS);

    $get_sc = PsbModel::get_data_for_dalapa($id);

    if (count($get_sc)){
      $result = $get_sc[0];
      $curl = curl_init();
      $get_sc = json_encode($result);
      // print_r($get_sc);

      $kord = explode(',',$result->KORDINAT);
      if (count($kord)>1){
        $latitude = $kord[0];
        $longitude = $kord[1];
      } else {
        $latitude = '';
        $longitude = '';
      }
      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://dalapa.id/api/dalapa/port",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_HTTPHEADER => array(
          "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ3aXRlbCI6IkJKTSIsInZhbHVlIjp7IjEiOjIwMzcsIjIiOjIwMzgsIjMiOjIwMzksIjQiOjIwNDAsIjUiOjIwNDEsIjYiOjIwNDIsIjciOjIwNDN9fQ.GF0HcedknFaFQ5h-Dll93F6MLRmVpj2sBYrpgoytDC0"
        ),
        CURLOPT_POSTFIELDS => array(
          'odp_location' => $result->ODP_LOCATION,
          'port_number' => $result->PORT_NUMBER,
          'latitude' => $latitude,
          'longitude' => $longitude,
          'sn_ont' => $result->SN_ONT,
          'nama_pelanggan' => $result->NAMA_PELANGGAN,
          'voice_number' => $result->VOICE_NUMBER,
          'internet_number' => $result->INTERNET_NUMBER,
          'date_validation' => $result->DATE_VALIDATION,
          'qr_port_odp' => $result->QR_PORT_ODP,
          'qr_dropcore' => $result->QR_DROPCORE,
          'nama_jalan' => $result->NAMA_JALAN,
          'contact' => $result->CONTACT,
          'type' => $result->TYPE
      )));

      $response = curl_exec($curl);

      curl_close($curl);

      $response = json_decode($response);
      print_r($response);

      if ($response<>""){
        if (property_exists($response,'status')){
        $update = DB::table('psb_laporan')->where('id_tbl_mj',$id)->update([
          'dalapa_status' => $response->status,
          'dalapa_message' => $response->message
        ]);
        }
        if (property_exists($response,'success')){
        $update = DB::table('psb_laporan')->where('id_tbl_mj',$id)->update([
          'dalapa_status' => $response->success,
          'dalapa_message' => implode($response->message)
        ]);
        }

      }


    } else {
      echo "DATA BELUM DI-SYNC";
    }

  }


	public function get_sc_kalsel($id){
		$link = "https://psb.tomman.info/get_sc/$id";
    $file = file_get_contents($link);
    header("Content-Type: application/json");
		print_r($file);
  }
  
  public function get_kord_kalsel($id){
  	$link = "https://psb.tomman.info/get_sc/$id";
    $file = file_get_contents($link);
    header("Content-Type: application/json");
		print_r($file);  
  }

	function get_sc($id){
		$get_data = DB::SELECT('
		select
		a.id,
		c.witel,
		pl.kordinat_pelanggan
		from dispatch_teknisi a
		left join psb_myir_wo b ON a.Ndem = b.sc
		left join psb_laporan pl ON a.id = pl.id_tbl_mj
    left join maintenance_datel c ON b.sto = c.sto
    left join Data_Pelanggan_Starclick d ON a.Ndem = d.orderId
		where a.Ndem = "'.$id.'"');
		header("Content-Type: application/json");
   	echo json_encode($get_data);
	}

  function workorder(){
    $mitra = Input::get('mitra');
    $tglps = Input::get('tglps');
    $witel = Input::get('witel');
    $datel = Input::get('datel');
    $sto    = Input::get('sto');
    $query = DashboardModel::provisioningListMitra_API($mitra,$tglps,$witel,$datel,$sto);
    header("Content-Type: application/json");
    echo json_encode($query);
  }

  function foto($sc,$filename){
    $get_data = DB::SELECT('select a.id,c.witel from dispatch_teknisi a left join psb_myir_wo b left join maintenance_datel c ON b.sto = c.sto ON a.Ndem = b.sc  where a.Ndem = "'.$sc.'"');

    if (count($get_data)>0){
      $result = $get_data[0];

      $witel = $result->witel;

      if ($witel==""){
        $get_data_from_sc = DB::SELECT('select * from Data_Pelanggan_Starclick where orderId = "'.$sc.'"');
        if (count($get_data_from_sc)>0){
          $result_sc = $get_data_from_sc[0];
          $witel = $result_sc->witel;
        } else {
          $witel = "WITEL TIDAK DITEMUKAN";
        }
      }
      if ($witel=="KALSEL"){
        $base_path = "/srv/htdocs/tomman1_psb/";
      } else {
        $base_path = "/srv/t2_tomman1_psb/";
      }
      $table = DB::table('hdd')->get()->first();
      $upload = $table->public;
      $hdd = $table->active_hdd;
        $img = $base_path."public/$upload/evidence/$result->id/$filename";
        $img2 = $base_path."public/upload3/evidence/$result->id/$filename";
        if (file_exists($img)){
        header("Content-Type: image/jpeg");
        readfile($img) or die('IMAGE FILE NOT FOUND 1');
        } else {
          $filename = str_replace("-kpro", "", $filename);
          $table = DB::table('hdd')->get()->first();
          $upload = $table->public;
          $hdd = $table->active_hdd;
          $path = $base_path."public/$upload/evidence/$result->id/$filename";
          $path2 = $base_path."public/upload3/evidence/$result->id/$filename";
          // echo $path;
          if (@file_exists($path)){
            $imgx = new \Imagick($path);
            $imgx->scaleImage(768, 1024, true);

            // $watermark = new \Imagick($base_path.'public/upload/watermark/watermark-1.png');
            // $x = 0;
            // $y = 0;
            // $imgx->compositeImage($watermark,\Imagick::COMPOSITE_OVER, $x, $y);
            $imgx->writeImage($img);
            header("Content-Type: image/jpeg");
            readfile($img) or die('IMAGE FILE NOT FOUND 2');
          } elseif (@file_exists($path2)){
              $imgx = new \Imagick($path2);
              $imgx->scaleImage(768, 1024, true);

              // $watermark = new \Imagick($base_path.'public/upload/watermark/watermark-1.png');
              // $x = 0;
              // $y = 0;
              // $imgx->compositeImage($watermark,\Imagick::COMPOSITE_OVER, $x, $y);
              $imgx->writeImage($img2);
              header("Content-Type: image/jpeg");
              readfile($img2) or die('IMAGE FILE NOT FOUND 2');
            } else {
              echo "IMAGE FILE NOT FOUND 3";
            }
        }
     } else {
      echo "IMAGE FILE NOT FOUND 4";
    }
  }

  function foto_api_by_date(){
    $date_start = Input::get('date_start');
    $date_end = Input::get('date_end');
    $opsi = Input::get('opsi');
    $check = DB::connection('pg')->SELECT("
    SELECT
    a.order_id as ORDER_ID,
    a.create_dtm as ORDER_DATE,
    a.order_code as EXTERN_ORDER_ID,
    a.xn4 as NCLI,
    a.customer_desc as CUSTOMER_NAME,
    a.xs3 as WITEL,
    a.create_user_id as AGENT,
    a.xs2 as SOURCE,
    a.xs12 as STO,
    a.xs10 as SPEEDY,
    a.xs11 as POTS,
    a.order_status_id as STATUS_CODE_SC,
    a.xs7 as USERNAME,
    a.xs8 as ORDER_ID_NCX,
    c.xs11 as KCONTACT,
    b.xs3 as ORDER_STATUS
    FROM
      sc_new_orders a
    LEFT JOIN sc_new_order_activities b ON a.order_id = b.order_id AND a.order_status_id = b.xn2
    LEFT JOIN sc_new_order_details c ON a.order_id = c.order_id
    WHERE
      a.xs3 IN ('KALSEL','BALIKPAPAN') AND
      (date(a.create_dtm) BETWEEN '".$date_start."' AND '".$date_end."')
  "); 
  // print_r(json_encode($check));
  $result = array();
  foreach ($check as $num => $result_x){
  $get = DB::SELECT('select * from dispatch_teknisi a left join psb_laporan pl ON a.id = pl.id_tbl_mj where a.Ndem = "'.$result_x->order_id.'"');
  if (count($get)>0){
    $get_result = $get[0];
    $result[$num]['status'] = "TRUE";
    $result[$num]['SC'] = $result_x->order_id;
    $result[$num]['kordinat_pelanggan'] = $get_result->kordinat_pelanggan;
    if ($opsi<>"kordinat_only"){
    foreach ($this->QC_foto as $foto){
      $result[$num][$foto] = "https://psb.tomman.app/foto/$result_x->order_id/".$foto."-kpro.jpg";
    }
    }
  } 
  // else{
  //   $link = "https://psb.tomman.info/get_sc_kalsel/$result_x->order_id";
	// 		$file = @file_get_contents($link);
	// 		$sc_kalsel = json_decode($file);
  //   	if (@property_exists($sc_kalsel[0],'kordinat_pelanggan')){
  //       $result[$num]['status'] = "TRUE";
  //       $result[$num]['SC'] = $result_x->order_id;
  //   		$result[$num]['kordinat_pelanggan'] = @$sc_kalsel[0]->kordinat_pelanggan;
	// 	    foreach ($this->QC_foto as $foto){
	// 	      $result[$num][$foto] = "https://psb.tomman.info/foto/$result_x->order_id/".$foto."-kpro.jpg";
	// 	    }
  //   	} else {
  //   		$result[$num]['status'] = "FALSE";
  //   	}
  // } 
  }
  print_r(json_encode($result));
  }

  function foto_api(){
    $id = Input::get('id');
    $key = Input::get('key');
    if ($key=="a2ed39c417316adbd5cd1d0211a5d711") {
    $result = array();
    $get = DB::SELECT('select * from dispatch_teknisi a left join psb_laporan pl ON a.id = pl.id_tbl_mj where a.Ndem = "'.$id.'"');
    if (count($get)>0){
    $get_result = $get[0];
    $result['status'] = "TRUE";
    $result['kordinat_pelanggan'] = $get_result->kordinat_pelanggan;
    foreach ($this->QC_foto as $foto){
      $result[$foto] = "https://psb.tomman.app/foto/$id/".$foto."-kpro.jpg";
    }
    } else {
    	$link = "https://psb.tomman.info/get_sc_kalsel/$id";
			$file = @file_get_contents($link);
			$sc_kalsel = json_decode($file);
    	if (@property_exists($sc_kalsel[0],'kordinat_pelanggan')){
    		$result['status'] = "TRUE";
    		$result['kordinat_pelanggan'] = @$sc_kalsel[0]->kordinat_pelanggan;
		    foreach ($this->QC_foto as $foto){
		      $result[$foto] = "https://psb.tomman.info/foto/$id/".$foto."-kpro.jpg";
		    }
    	} else {
    		$result['status'] = "FALSE";
    	}
    }
    } else {
      $result['status'] = "FALSE";
    }
    print_r(str_replace("\\","",json_encode($result)));
  }

  function status_order(){
    $key = "EpwvzxlvDpDDtEtxptmmADulJl";
    $result = array();

    $id = Input::get('id');
    $key_get = Input::get('key');
    if ($key == $key_get){
    $get = DB::SELECT('select
    a.Ndem as id,
    c.laporan_status as status_order,
    b.modified_at as time
    from dispatch_teknisi a
    left join psb_laporan b On a.id = b.id_tbl_mj
    left join psb_laporan_status c ON b.status_laporan = c.laporan_status_id
    WHERE a.Ndem = "'.$id.'" ');
    if ($get){
      $status = "TRUE";

    } else {
      $status = "FALSE";
    }
    array_push($result,array('status' => $status));
    array_push($result,array('data' => $get));
  } else {
    $status = "FALSE";
    array_push($result,array('status' => $status));
  }

    $result = json_encode($result);
    print_r($result);
  }
}
