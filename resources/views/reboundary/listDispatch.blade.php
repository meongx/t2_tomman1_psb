@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }

  </style>
  <a href="/" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
            <th>No</th>
            <th>Tiket</th>
            <th>SCID</th>
            <th>Tim PSB Sebelumnya</th>
            <th>Speedy</th>
            <th>Pelanggan</th>
            <th>Alamat</th>
            <th>STO</th>
            <th>Regu</th>
            <th>Status Laporan</th>
            <th>Tarikan Lama</th>
            <th>Estimasi Tarikan</th>
        </tr>
         
        @foreach($data as $no=>$dt)
            <tr>
                  <td>{{ ++$no }}</td>
                  
                  @if($dt->dispatch==1)
                      <td><a href="/reboundary/{{ $dt->idDt }}">INX{{ $dt->no_tiket_reboundary }}</a></td>
                      <td>{{ $dt->scid }}</td>
                  @else
                      <td>~</td>
                      <td><a href="/reboundary/dispatch/{{ $dt->scid }}/2"> {{ $dt->scid }}<a></td>
                  @endif
                  
                  <td>{{ $dt->uraianTimLama }}</td>
                  <td>{{ $dt->ndemSpeedy ?: '~'}}</td>
                  <td>{{ $dt->orderName }}</td>
                  <td>{{ $dt->orderAddr }} - {{ $dt->orderCity }}</td>
                  <td>{{ $dt->sto }}</td>
                  <td>{{ $dt->uraian ?: ' ~ '}}</td>
                  <td>{{ $dt->laporan_status ?: '~' }}</td>
                  <td>{{ $dt->tarikan_lama }} M</td>
                  <td>{{ $dt->estimasi_tarikan }} M</td>
            </tr>
        @endforeach
        
      </table>
    </div>
    </div>
  </div>   
@endsection     
