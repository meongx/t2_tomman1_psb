  @extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    
    <h3>
      <a href="/maintaince/status/np" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Input Ticket
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-tiket">No Tikcet</label>
      <input name="tiket" type="text" id="input-tiket" class="form-control" value="{{ $data->no_tiket or '' }}" />
    </div>
    <div class="form-group">
      <label class="control-label">Headline</label>
      <textarea name="headline" class="form-control" rows="1">{{ $data->headline or '' }}</textarea>
    </div>
    <div class="form-group">
      <label class="control-label">Info</label>
      <textarea name="info" class="form-control" rows="1">{{ $data->info or '' }}</textarea>
    </div>
    <div class="form-group">
      <label class="control-label" for="input-program">Program</label>
      <input name="program" type="hidden" id="input-program" class="form-control" value="{{ $data->program_id or '' }}" />
    </div>
    @if(session('auth')->level == 53)
    <div class="form-group">
      <label class="control-label" for="input-tim">Tim</label>
      <input name="tim" type="hidden" id="input-tim" class="form-control" value="{{ $data->dispatch_regu_id or '' }}" />
    </div>
    @endif
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id }}" />

    @endif
  </form>
  @if (isset($data->id))
  <form id="delete-form" method="post" autocomplete="off">
    <input name="_method" type="hidden" value="DELETE">
    
      <div style="margin:40px 0 20px">
        <button class="btn btn-danger">Hapus</button>
      </div>
    
  </form>
  @endif
  <script>
  $(function() {
    $('.btn-danger').click(function() {
      var sure = confirm('Yakin hapus data ?');
      if (sure) {
        $('#delete-form').submit();
      }
    })
      var data = [{"id":1, "text":"QE Akses Copper"},{"id":2, "text":"QE Akses FTTH"},{"id":3, "text":"QE Relokasi Utilitas Copper"},{"id":4, "text":"QE Recovery"}];
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Program',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-program').select2(select2Options());
      
      var tim = <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: tim,
          placeholder: 'Input Regu',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.job+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-tim').select2(regu());
  })
  </script>
@endsection
