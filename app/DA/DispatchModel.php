<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class DispatchModel
{

  public static function get_sektor(){
      $query = DB::table('group_telegram')
                      ->groupBy('sektor')
                      ->where('sektor','<>','')
                      ->orderBy('urut','ASC')
                      ->get();
      return $query;
  }

  public static function getSektorASR(){
    $query = DB::SELECT('
      SELECT
        a.title,
        count(*) as jumlah
      FROM
        group_telegram a
      LEFT JOIN mdf b ON a.sektor = b.sektor_asr
      LEFT JOIN nonatero_excel c ON b.mdf = c.CMDF
      WHERE
        title LIKE "%ASR%"
      GROUP BY a.title
    ');
    return $query;
  }

  public static function lastupdate($idregu){
    return DB::SELECT('
        SELECT 
        * 
        FROM 
          psb_laporan_log a 
        LEFT JOIN psb_laporan_status b ON a.status_laporan = b.laporan_status_id
        WHERE 
          a.id_regu = "'.$idregu.'" AND 
          a.status_laporan <> 6 AND 
          DATE(a.created_at) = "'.date('Y-m-d').'"   
        ORDER BY a.created_at DESC
        LIMIT 1
      ');
  }

  public static function produktifitasteklist($date,$id_regu,$status){
    $where_status = '';
    if ($status=="NP"){
      $where_status = ' AND (b.id IS NULL OR c.grup="SISA")';
    } elseif ($status<>"ALL"){
      $where_status = ' AND c.grup="'.$status.'"';
    } 
    $query = DB::SELECT('
      SELECT 
      * 
      FROM 
        dispatch_teknisi a 
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        LEFT JOIN Data_Pelanggan_Starclick f ON a.Ndem = f.orderId
        WHERE   
          a.tgl LIKE "2019%" AND 
          (f.orderId <> "" OR a.dispatch_by = 5) AND 
          a.id_regu = "'.$id_regu.'" 
          '.$where_status.'
      ');
    return $query;
  }

  public static function produktifitastek3($date,$area,$mitra){
    $where_area = "";
    if ($area <> "ALL") {
      $where_area = ' AND e.ket_sektor = '.$area;  
    }
    $query = DB::SELECT('
        SELECT 
        d.uraian,
        count(*) as jumlah,
        SUM(CASE WHEN (c.grup = "SISA" OR b.id IS NULL) THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN c.grup = "KP" THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN c.grup = "KT" THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN c.grup = "HR" THEN 1 ELSE 0 END) as HR,
        SUM(CASE WHEN c.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN b.status_laporan = 1 THEN 1 ELSE 0 END) as UP,
        e.title,
        e.sektor,
        a.id_regu,
        a.Ndem
        FROM 
          dispatch_teknisi a 
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        LEFT JOIN Data_Pelanggan_Starclick f ON a.Ndem = f.orderId
        WHERE 
          
          ((b.status_laporan = 4 AND a.tgl LIKE "2019%") OR (
          a.tgl LIKE "'.$date.'%" AND 
          (f.orderId <> "" OR a.dispatch_by = 5)))
          '.$where_area.'
        GROUP BY 
          d.id_regu
        ORDER BY UP DESC
      ');
    return $query;
  }

  public static function produktifitastekA($date,$mode,$mitra){

    $where_mode = '';
    $where_mode_having = '';
    if ($mode=="INNER"){
      $where_mode = 'AND e.ket_sektor=1';
    }
    if ($mode=="OUTER"){
      $where_mode = 'AND e.ket_sektor=0';
    }
    if ($mode=="UP0"){
      $where_mode_having = 'HAVING UP = 0';
    }
    $query = DB::SELECT('
        SELECT 
        d.uraian,
        count(*) as jumlah,
        SUM(CASE WHEN (c.grup = "SISA" OR b.id IS NULL) THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN c.grup = "KP" THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN c.grup = "KT" THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN c.grup = "HR" THEN 1 ELSE 0 END) as HR,
        SUM(CASE WHEN c.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN b.status_laporan = 1 THEN 1 ELSE 0 END) as UP,
        e.title,
        e.sektor,
        a.id_regu,
        a.Ndem
        FROM 
          dispatch_teknisi a 
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        LEFT JOIN data_nossa_1_log f ON a.Ndem = f.Incident
        WHERE 
          ((f.Incident <> "" AND b.status_laporan = 4 AND a.tgl LIKE "2019%") OR (
          a.tgl LIKE "'.$date.'%" AND 
          (f.Incident <> "")))
          '.$where_mode.'
        GROUP BY 
          d.id_regu
          '.$where_mode_having.'
        ORDER BY UP DESC
      ');
    return $query;
  }

  public static function produktifitassektor($date,$mode){
    $where_mode = '';
    $where_mode_having = '';
    if ($mode=="INNER"){
      $where_mode = 'AND e.ket_sektor=1';
    }
    if ($mode=="OUTER"){
      $where_mode = 'AND e.ket_sektor=0';
    }
    if ($mode=="UP0"){
      $where_mode_having = 'HAVING UP = 0';
    }

    $query = DB::SELECT('
        SELECT 
        d.uraian,
        count(*) as jumlah,
        SUM(CASE WHEN (c.grup = "SISA" OR b.id IS NULL) THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN c.grup = "KP" THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN c.grup = "KT" THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN c.grup = "HR" THEN 1 ELSE 0 END) as HR,
        SUM(CASE WHEN c.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN b.status_laporan = 1 THEN 1 ELSE 0 END) as UP,
        e.title,
        e.sektor,
        a.id_regu,
        a.Ndem
        FROM 
          dispatch_teknisi a 
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        LEFT JOIN data_nossa_1_log f ON a.Ndem = f.Incident
        WHERE 
          ((f.Incident <> "" AND b.status_laporan = 4 AND a.tgl LIKE "2019%") OR (
          a.tgl LIKE "'.$date.'%" AND 
          (f.Incident <> ""))) AND 
          e.title <> "EMPTY GROUP"
          '.$where_mode.'
        GROUP BY 
          d.mainsector
          '.$where_mode_having.'
        ORDER BY UP DESC
      ');
    return $query;    
  }

  public static function produktifitassektorlist($date,$sektor,$mode){
    $where_sektor = '';
    $where_mode = '';
    if ($sektor<>"ALL"){
      $where_sektor = 'AND e.title = "'.$sektor.'"';
    }
    if ($mode=="NP"){
      $where_mode = 'AND (c.grup = "SISA" OR b.id IS NULL)';
    } else if ($mode<>"ALL"){
      $where_mode = 'AND c.grup = "'.$mode.'"';
    } 
    $query = DB::SELECT('
        SELECT 
          *,
          h.ONU_Link_Status as ukur_before,
          a.id as id_dt
        FROM 
          dispatch_teknisi a 
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        LEFT JOIN data_nossa_1_log f ON a.Ndem = f.Incident
        LEFT JOIN psb_laporan_action g ON b.action = g.laporan_action_id
        LEFT JOIN ibooster h ON a.Ndem = h.order_id
        LEFT JOIN psb_laporan_penyebab i ON b.penyebabId = i.idPenyebab
        WHERE 
          ((f.Incident <> "" AND b.status_laporan = 4 AND a.tgl LIKE "2019%") OR (
          a.tgl LIKE "'.$date.'%" AND 
          (f.Incident <> ""))) AND 
          e.title <> "EMPTY GROUP"
          '.$where_mode.'
          '.$where_sektor.'

        ORDER BY f.Reported_Date DESC
      ');
    return $query;
  }

  public static function produktifitastek2($date,$area){
    if ($area=="ALL") {
      $get_where_area = "";
    } else {
      $get_where_area = "AND b.mitra = '".$area."'";
    }
    $query = DB::SELECT('
      SELECT
       emp.nik,
       emp.nama,
       count(b.id_regu) as jumlah_regu
      FROM
        1_2_employee emp
      LEFT JOIN regu b ON (emp.nik = b.NIK1) OR (emp.nik = b.NIK2)
      LEFT JOIN dispatch_teknisi a ON b.id_regu = a.id_regu
      WHERE
        1
        '.$get_where_area.'
      GROUP BY emp.nik
    ');
    return $query;
  }
  public static function produktifitastek($date,$area){
    if ($area=="ALL") {
      $get_where_area = "";
    } else if ($area == "DELTA"){
      $get_where_area = 'AND rm.klasifikasi = "DELTA"';
    } else {
      $get_where_area = "AND b.mitra = '".$area."'";
    }
    $query = DB::SELECT('
    SELECT *,(prov_UP+gangguan_UP_Copper+gangguan_UP_GPON+asis_UP+gangguan_UP_DES+addon_UP)/(target_MH*jumlah_regu) as rank FROM(
      SELECT
        count(*) as jumlah,
        g.TL,
        g.sektor,
        SUM(CASE WHEN a.dispatch_by IN (2) THEN 1 ELSE 0 END) as jlhggn,
        SUM(CASE WHEN a.dispatch_by is null OR a.dispatch_by NOT IN(3,2,1) THEN 1 ELSE 0 END) as jlhpsb,
        SUM(CASE WHEN d.status_laporan IN (5,6,4) THEN 1 ELSE 0 END) as PROGRESS,
        SUM(CASE WHEN d.status_laporan NOT IN (5,6,1,4) THEN 1 ELSE 0 END) as KENDALA,
        SUM(CASE WHEN c.no_tiket<>"" THEN 1 ELSE 0 END) as gangguan,
        SUM(CASE WHEN e.orderId<>"" THEN 1 ELSE 0 END) as prov,
        SUM(CASE WHEN d.status_laporan = 1 THEN 1 ELSE 0 END) as UP,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1.2 ELSE 0 END) as gangguan_UP_Copper,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%" )) THEN 2.4 ELSE 0 END) as gangguan_UP_GPON,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 AND d.status_laporan = 1 ) THEN 4.8 ELSE 0 END) as gangguan_UP_DES,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%")) THEN 5.3 ELSE 0 END) as prov_UP,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%")) THEN 3.3 ELSE 0 END) as addon_UP,
        SUM(CASE WHEN (dm.ND<>"" AND d.status_laporan = 1) THEN 5.3 ELSE 0 END) as asis_UP,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_Copper,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%")) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_GPON,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 AND d.status_laporan = 1 ) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_DES,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%") ) THEN 1 ELSE 0 END) as jumlah_prov_UP,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%") ) THEN 1 ELSE 0 END) as jumlah_addon_UP,
        SUM(CASE WHEN (dm.ND<>"" AND d.status_laporan = 1) THEN 1 ELSE 0 END) as jumlah_asis_UP,
        SUM(CASE WHEN (e.orderId<>"" AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%")) THEN 5.3 ELSE 0 END) as prov_MH,
        SUM(CASE WHEN (e.orderId<>"" AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%")) THEN 3.3 ELSE 0 END) as addon_MH,
        SUM(CASE WHEN (dm.ND<>"") THEN 5.3 ELSE 0 END) as asis_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 ) THEN 4.8 ELSE 0 END) as gangguan_DES_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2  AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1.2 ELSE 0 END) as gangguan_Copper_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2  AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%")) THEN 2.4 ELSE 0 END) as gangguan_GPON_MH,
        b.id_regu,
        b.uraian as uraian,
        b.mainsector,
        b.kemampuan as target_MH,
        b.kemampuan,
        COUNT(DISTINCT(a.id_regu)) as jumlah_regu,
        a.updated_at,
        b.star,
        b.mitra
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram g ON b.mainsector = g.chat_id
      LEFT JOIN roc c ON a.Ndem = c.no_tiket
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN group_telegram f ON b.mainsector = f.chat_id
      LEFT JOIN dossier_master dm ON a.Ndem = dm.ND
      LEFT JOIN regu_mitra rm ON b.mitra = rm.mitra
      WHERE
        ((DATE(d.modified_at) Like "%'.$date.'%" OR DATE(d.created_at) Like "%'.$date.'%") and
        a.tgl LIKE "%'.$date.'%") and
        f.title <> "" AND
        f.title NOT LIKE "%CCAN%" AND
        f.sektor like "%prov%" and
        f.sektor <> "PROVSSC1"
        '.$get_where_area.'
      GROUP BY b.id_regu
    ) x ORDER BY rank DESC
    ');
    return $query;
         // DATE(a.tgl) like "%'.$date.'%" AND
        // ((DATE(d.modified_at) = "'.$date.'" OR DATE(d.created_at) = "'.$date.'") and
  }

  public static function group_telegram($area){
    if ($area=="ALL"){
      $sql = 'SELECT * from group_telegram order by urut asc';
    } 
    else if ($area=="INNER"){
      $get_where = " WHERE a.ket_sektor=1";
      $sql = 'SELECT a.* FROM group_telegram a LEFT JOIN regu b ON a.chat_id=b.mainsector '.$get_where. ' group by a.title order by a.urut asc';
    }
    else if ($area=="OUTER"){
      $get_where = " WHERE a.ket_sektor=0";
      $sql = 'SELECT a.* FROM group_telegram a LEFT JOIN regu b ON a.chat_id=b.mainsector '.$get_where. ' group by a.title order by a.urut asc';
    }
    else if ($area=="PROV"){
      $get_where = ' WHERE a.ket_sektor in (0,1) AND b.sttsWo=1'; 
      $sql = 'SELECT a.* FROM group_telegram a LEFT JOIN regu b ON a.chat_id=b.mainsector '.$get_where. ' group by a.title order by a.urut asc';
    }
    else if ($area=="ASR"){
      $get_where = ' WHERE a.ket_sektor in (0,1) AND b.sttsWo=0 ';
      $sql = 'SELECT a.* FROM group_telegram a LEFT JOIN regu b ON a.chat_id=b.mainsector '.$get_where. ' group by a.title order by a.urut asc'; 
    }
    else {
      $get_where = ' WHERE sektor LIKE "%'.$area.'%"';
      $sql = 'SELECT a.* FROM group_telegram a LEFT JOIN regu b ON a.chat_id=b.mainsector '.$get_where. ' group by a.title order by a.urut asc';
    }
    return DB::SELECT($sql);
    // return DB::table('group_telegram')->where('type',1)->get();
  }
  public static function listWO($date,$id_regu){
    $query = DB::SELECT('
      SELECT
        a.Ndem,
        b.status_laporan,
        c.jenisPsb as jenisPsb,
        e.laporan_status,
        b.action,
        f.action as actionText,
        b.modified_at,
        b.created_at,
        a.created_at as tglDispatch,
        c.sto,
        a.updated_at,
        a.id as id_dt,
        b.catatan,
        c.orderDate,
        a.dispatch_by,
        a.jenis_layanan,
        e.grup,
        g.qcondesk_order
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN psb_laporan_status e ON b.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_action f ON f.laporan_action_id = b.action
      LEFT JOIN qcondesk g ON a.id = g.qcondesk_order
      WHERE 
          ((DATE(b.modified_at) = "'.$date.'" OR DATE(b.created_at) = "'.$date.'") OR ((b.status_laporan IN (5,6,7,31,4) OR b.id_tbl_mj is NULL) AND a.tgl LIKE "'.date('Y-m').'%")) AND (a.dispatch_by IS NULL OR a.dispatch_by IN (1,2,3,4,5)) AND a.id_regu = "'.$id_regu.'"
    ');
    return $query;
  }

  public static function getHdMatrix($date){
    return DB::select('SELECT b.nama,a.Ndem,c.status_laporan,a.updated_by FROM dispatch_teknisi a
      LEFT JOIN 1_2_employee b ON a.updated_by = b.nik
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      WHERE a.tgl LIKE "%'.$date.'%" AND dispatch_by IS NULL ORDER BY a.updated_by asc');
  }
  public static function getHdFalloutMatrix($date){
    return DB::select('SELECT a.workerName,a.orderId,c.orderStatus,a.workerId,a.ket FROM dispatch_hd a
      LEFT JOIN Data_Pelanggan_Starclick c ON a.orderId = c.orderId
      WHERE a.updated_at LIKE "%'.$date.'%" ORDER BY a.workerId asc');
  }
  public static function getTeamMatrix1($date,$area){
    if ($area=="ALL") {
      $get_where_area = "";
    } else {
      $get_where_area = "AND f.sektor LIKE '%".$area."%'";
    }
    if (strlen($date)<8) {
      if (date('Y-m')==$date) {
        if (date('d')<=22){
          $konstanta = date('d');
        } else {
          $konstanta = 22;
        }
      } else {
        $konstanta = 22;
      }
    } else {
      $konstanta = 1;
    }
    $query = DB::SELECT('
    SELECT *,(prov_UP+gangguan_UP_Copper+gangguan_UP_GPON+asis_UP+gangguan_UP_DES+addon_UP)/(target_MH*jumlah_regu*'.$konstanta.') as rank FROM(
      SELECT
        count(*) as jumlah,
        g.TL,
        SUM(CASE WHEN a.dispatch_by IN (2) THEN 1 ELSE 0 END) as jlhggn,
        SUM(CASE WHEN a.dispatch_by is null OR a.dispatch_by NOT IN(3,2,1) THEN 1 ELSE 0 END) as jlhpsb,
        SUM(CASE WHEN d.status_laporan IN (5,6,4) THEN 1 ELSE 0 END) as PROGRESS,
        SUM(CASE WHEN d.status_laporan NOT IN (5,6,1,4) THEN 1 ELSE 0 END) as KENDALA,
        SUM(CASE WHEN c.no_tiket<>"" THEN 1 ELSE 0 END) as gangguan,
        SUM(CASE WHEN (e.orderId<>"" AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%")) THEN 1 ELSE 0 END) as prov,
        SUM(CASE WHEN d.status_laporan = 1 THEN 1 ELSE 0 END) as UP,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1.2 ELSE 0 END) as gangguan_UP_Copper,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%" )) THEN 2.4 ELSE 0 END) as gangguan_UP_GPON,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 AND d.status_laporan = 1 ) THEN 4.8 ELSE 0 END) as gangguan_UP_DES,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%")) THEN 5.3 ELSE 0 END) as prov_UP,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%")) THEN 3.3 ELSE 0 END) as addon_UP,
        SUM(CASE WHEN (dm.ND<>"" AND d.status_laporan = 1) THEN 5.3 ELSE 0 END) as asis_UP,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_Copper,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%")) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_GPON,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 AND d.status_laporan = 1 ) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_DES,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%") ) THEN 1 ELSE 0 END) as jumlah_prov_UP,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%") ) THEN 1 ELSE 0 END) as jumlah_addon_UP,
        SUM(CASE WHEN (dm.ND<>"" AND d.status_laporan = 1) THEN 1 ELSE 0 END) as jumlah_asis_UP,
        SUM(CASE WHEN (e.orderId<>"" AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%")) THEN 5.3 ELSE 0 END) as prov_MH,
        SUM(CASE WHEN (e.orderId<>"" AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%")) THEN 3.3 ELSE 0 END) as addon_MH,
        SUM(CASE WHEN (dm.ND<>"") THEN 5.3 ELSE 0 END) as asis_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 ) THEN 4.8 ELSE 0 END) as gangguan_DES_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2  AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1.2 ELSE 0 END) as gangguan_Copper_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2  AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%")) THEN 2.4 ELSE 0 END) as gangguan_GPON_MH,
        b.id_regu,
        b.nik1,
        b.nik2,
        f.title as uraian,
        b.mainsector,
        b.kemampuan as target_MH,
        b.kemampuan,
        COUNT(DISTINCT(a.id_regu)) as jumlah_regu,
        a.updated_at,
        (SELECT abs1.approval from absen abs1 where abs1.nik = b.nik1 AND date(abs1.date_created)="'.$date.'%" group by date(abs1.date_created)) as nik1_absen,
        (SELECT abs1.approval from absen abs1 where abs1.nik = b.nik2 AND date(abs1.date_created)="'.$date.'%" group by date(abs1.date_created)) as nik2_absen
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu

      LEFT JOIN group_telegram g ON b.mainsector = g.chat_id
      LEFT JOIN roc c ON a.Ndem = c.no_tiket
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN group_telegram f ON b.mainsector = f.chat_id
      LEFT JOIN dossier_master dm ON a.Ndem = dm.ND

      WHERE
        f.title NOT LIKE "%CCAN%" AND
        DATE(a.tgl) like "%'.$date.'%" AND
        b.mitra IN ("TA","UPATEK","CUI") AND
        f.title <> ""
        '.$get_where_area.'
      GROUP BY f.title
    ) x ORDER BY rank DESC
    ');
    return $query;
  }
  public static function getTeamMatrix($date,$area){
    if ($area=="ALL") {
      $get_where_area = '';
    } 
    else if ($area=="INNER"){
        $get_where_area = 'AND b.sttsWo=1 AND f.ket_sektor="1"';
    }
    else if ($area=="OUTER"){
       $get_where_area = 'AND b.sttsWo=1 AND f.ket_sektor="0"';
    }
    else if ($area=="PROV"){
      $get_where_area = ' AND b.sttsWo=1 AND f.ket_sektor in (0,1)';
    }
    
    else {
      $get_where_area = "AND f.sektor='".$area."' AND b.sttsWo=1";
    }
  
    $query = DB::SELECT('
     SELECT
        b.id_regu,
        b.nik1,
        b.nik2,
        b.uraian,
        b.mainsector,
        b.kemampuan as target_MH,
        b.kemampuan,
        a.updated_at,
        b.mitra,
        f.chatTl,
        f.title,
        f.sektor,
        f.ket_sektor,
        d.modified_at,
        d.created_at,
        my.*,
        d.status_laporan
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN group_telegram f ON b.mainsector = f.chat_id
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
      LEFT JOIN myirSCTgl my ON a.Ndem=my.myir
      WHERE
        ((DATE(d.modified_at) = "'.$date.'" OR DATE(d.created_at) = "'.$date.'") OR ((d.status_laporan IN (5,6,7,4) OR d.id_tbl_mj is NULL ) AND a.tgl LIKE "'.date('Y-m').'%" )) AND (a.dispatch_by IS NULL OR a.dispatch_by IN (1,2,3,4,5))
        '.$get_where_area.'
      GROUP BY a.id_regu
    ');
  
    return $query;
  }
  public static function getArea($area){
    if ($area=="ALL") {
      $get_where_area = "";
    } else {
      $get_where_area = "AND a.area = '".$area."'";
    }
    $get_area = DB::SELECT('
      SELECT
        a.area,
        b.DESKRIPSI
      FROM
        mdf a
      LEFT JOIN area b ON a.area = b.area
      WHERE
        b.DESKRIPSI is not NULL
        '.$get_where_area.'
      GROUP BY a.area
    ');
    return $get_area;
  }
  public static function listTimDispatch($date){
    $listTim = DB::SELECT('
      SELECT
        a.id_regu,
        b.uraian,
        d.area,
        count(*) as jumlah_wo,
        SUM(CASE WHEN a.manja_status = 1 THEN 1 ELSE 0 END) as jumlah_manja,
        SUM(CASE WHEN a.manja_status = 2 THEN 1 ELSE 0 END) as jumlah_manja_nok
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN mdf d ON c.sto = d.mdf
      WHERE
        a.tgl LIKE "'.$date.'%"
      GROUP BY b.id_regu
    ');
    return $listTim;
  }
  public static function timeslot(){
    $query = DB::table('dispatch_teknisi_timeslot')->get();
    return $query;
  }
  public static function dispatch_timeslot($date){
    $dispatch_timeslot = DB::SELECT('
      SELECT
        a.*,
        b.*,
        h.*,
        c.status_laporan,
        c.catatan,
        c.modified_at,
        c.created_at,
        d.uraian,
        d.id_regu,
        e.orderName,
        g.laporan_status,
        c.id_tbl_mj,
        f.area
      FROM
        dispatch_teknisi a
      LEFT JOIN dispatch_teknisi_timeslot b ON a.updated_at_timeslot = b.dispatch_teknisi_timeslot_id
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN regu d ON a.id_regu = d.id_regu
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN roc h ON a.Ndem = h.no_tiket
      LEFT JOIN mdf f ON e.sto = f.mdf
      LEFT JOIN psb_laporan_status g ON c.status_laporan = g.laporan_status_id
      WHERE
        a.tgl LIKE "'.$date.'%"
      ORDER BY d.id_regu,f.area
    ');
    return $dispatch_timeslot;
  }
  public static function WorkOrderAsr($id){
    $get = DB::SELECT('
      SELECT
        a.*,
        c.uraian,
        b.id as id_dt,
        b.dispatch_by,
        a.Incident as no_tiket,
        a.Service_ID as no_telp,
        a.Summary as no_speedy
      FROM
        data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
        Where
        a.Incident = "'.$id.'" OR 
        a.Service_No LIKE "%'.$id.'%";
    ');
    // $get = DB::SELECT('
    //   SELECT
    //     a.*,
    //     c.uraian,
    //     b.id as id_dt,
    //     b.dispatch_by,
    //     a.no_tiket,
    //     a.no_telp,
    //     a.no_speedy,
    //     aa.*
    //   FROM
    //     roc a
    //   LEFT JOIN data_nossa_1_log aa ON a.no_tiket = aa.Incident
    //   LEFT JOIN dispatch_teknisi b ON a.no_tiket = b.Ndem
    //   LEFT JOIN regu c ON b.id_regu = c.id_regu
    //     Where
    //     a.no_tiket = "'.$id.'"
    // ');
    return $get;
  }
  public static function CountWO($id){
    $Where  = DispatchModel::GetWhere($id,NULL);
    $result = DispatchModel::QueryCount($Where);
    return $result;
 }
 public static function QueryCount($Where){
   $SQL = '
     SELECT
       count(*) as jumlah
     FROM
       Data_Pelanggan_Starclick a
     LEFT JOIN ms2n b ON a.orderNcli = b.Ncli
     LEFT JOIN dispatch_teknisi c ON b.Ndem=c.Ndem OR a.orderId = c.Ndem
     LEFT JOIN psb_laporan d ON c.id=d.id_tbl_mj
     LEFT JOIN ms2n_sebab e ON b.Sebab = e.ms2n_sebab
     LEFT JOIN ms2n_status_hd f ON b.Ndem=f.Ndem
     LEFT JOIN regu g ON c.id_regu = g.id_regu
  	 LEFT JOIN odp_ftp h ON a.alproname = h.INDEX_NAME
  	 LEFT JOIN olt i ON h.OLT_IP = i.OLT_IP
     LEFT JOIN dshr j ON a.orderId = j.kode_sc
     '.$Where.'
   ';
   $query = DB::select($SQL);
   return $query;
 }

  public static function WorkOrder($id,$search){
    $Where  = DispatchModel::GetWhere($id,$search);
    $result = DispatchModel::QueryBuilder($Where);
    
    return $result;
  }

  public static function GetWhere($id,$search){
    $tomorrow = mktime(date("H"), date("i"), date("s"), date("m"), date("d")+1, date("Y"));
    $id = strtoupper($id);
    switch ($id) {
      case 'SURVEY_OK' :
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
            d.status_laporan = "8"
        ';
      break;
      case 'ALL':
        $Where = '
          WHERE
            a.sto NOT IN ("TML") AND
            a.orderStatus NOT IN ("Cancel Order","Process OSS (Cancel)") AND
           (a.orderDate BETWEEN "'.date("Y-m-d",strtotime("first day of previous month")).'" AND "'.date("Y-m-d",$tomorrow).'")
        ';
        break;
      case 'READY':
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
            b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji","",NULL) AND
            (b.Status <> "PS" OR b.Status is NULL) AND
            (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )

          ';
        break;

      case 'UNDISPATCH_MS2N' :
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)") AND
            b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
            c.id_regu is NULL
        ';
        break;
      case 'NAL' :
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
            b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
            b.Status <> "PS" AND
            (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
        ';
        break;

      case 'UNDISPATCH_NONMS2N' :
        $Where = '
          WHERE
            a.jenisPsb NOT IN ("", "MO|", "MO|BUNDLING", "MO|INTERNET", "MO|INTERNET + VOICE", "MO|IPTV") AND
            a.orderStatus = "Process OSS (Provision Issued)" AND
            b.Status is NULL
        ';
        break;
        case 'NONNAL' :
          $Where = '
            WHERE
              a.jenisPsb NOT IN ("", "MO|", "MO|BUNDLING", "MO|INTERNET", "MO|INTERNET + VOICE", "MO|IPTV") AND
              a.orderStatus = "Process OSS (Provision Issued)" AND
              b.Status is NULL AND
              (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
          ';
          break;
          case 'NONNAL_CLUSTER' :
            $Where = '
              WHERE
                a.jenisPsb NOT IN ("", "MO|", "MO|BUNDLING", "MO|INTERNET", "MO|INTERNET + VOICE", "MO|IPTV") AND
                a.orderStatus = "Process OSS (Provision Issued)" AND
                b.Status is NULL AND
                j.flagging <> "" AND
                (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
            ';
            break;
            case 'NONNAL_NONCLUSTER' :
              $Where = '
                WHERE
                  a.jenisPsb NOT IN ("", "MO|", "MO|BUNDLING", "MO|INTERNET", "MO|INTERNET + VOICE", "MO|IPTV") AND
                  a.orderStatus = "Process OSS (Provision Issued)" AND
                  b.Status is NULL AND
                  (j.flagging is NULL OR j.flagging = "") AND
                  (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
              ';
              break;


      case 'CFC':
        $Where = '
          WHERE
            a.kcontact LIKE ("%CFC%") AND
            a.orderStatus NOT IN ("Completed (PS)") AND
            a.sto NOT IN ("TML")
        ';
        break;
      case 'MBSP':
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
            b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
            b.Deskripsi LIKE "%MBSP%" AND
            a.jenisPsb <> "MIGRATE" AND
            b.Status <> "PS" AND
            (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
        ';
        break;
        case 'MBSP_CLUSTER':
          $Where = '
            WHERE
              a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
              b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
              b.Deskripsi LIKE "%MBSP%" AND
              b.Status <> "PS" AND
              a.jenisPsb <> "MIGRATE" AND
              j.flagging is not NULL AND
              (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
          ';
          break;
          case 'MBSP_NONCLUSTER':
            $Where = '
              WHERE
                a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
                b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
                b.Deskripsi LIKE "%MBSP%" AND
                b.Status <> "PS" AND
                j.flagging is NULL AND
                a.jenisPsb <> "MIGRATE" AND
                (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
            ';
          break;

      case 'REGULER':
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
            b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
            b.Status <> "PS" AND
            b.Deskripsi NOT LIKE "%MBSP%" AND
            (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
        ';
        break;
        case 'REGULER_CLUSTER':
          $Where = '
            WHERE
              a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
              b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
              b.Deskripsi NOT LIKE "%MBSP%" AND
              j.flagging is not NULL AND
              b.Status <> "PS" AND
              (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
          ';
          break;
          case 'REGULER_NONCLUSTER':
            $Where = '
              WHERE
                a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
                b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
                b.Deskripsi NOT LIKE "%MBSP%" AND
                b.Status <> "PS" AND
                j.flagging is NULL AND
                (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
            ';
            break;

      case 'FALLOUTWFM':
        $Where = '
          WHERE
            a.orderStatus IN ("Fallout (WFM)") AND
            a.sto NOT IN ("TML")
        ';
        break;
      case 'STB':
        $Where = '
          WHERE
            a.Kcontact LIKE "%STB%" AND
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM) ","Fallout (Activation)","Fallout (Data)")
        ';
        break;
      case 'PITOFAILSWA':
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)") AND
            b.Sebab NOT IN ("Belum Ada Janji","Sudah Ada Janji","Follow Up Permintaan","Update","PORT Habis") AND
            b.Status NOT IN ("PS") AND
            a.sto NOT IN ("TML")
        ';
        break;
      case 'FALLOUTACTIVATION':
        $Where = '
          WHERE
            (dps.orderStatus = "Fallout (Activation)" OR a.orderStatus = "Fallout (Activation)")
        ';
        break;
      case 'UPTOPS':
        $Where = '
          WHERE
            a.orderStatus NOT IN ("Completed (PS)") AND
            b.Status <> "PS" AND
            d.status_laporan = 1
        ';
        break;
      case 'MAINTENANCE' :
        $Where = '
          WHERE
            (b.Ket_Sebab LIKE "%ODP LOS%" OR b.Ket_Sebab LIKE "%redaman%" OR b.Sebab = "Follow Up Permintaan") AND
            b.Status NOT IN ("PS")
        ';
        break;
      case 'SEARCH' :
        $Where = '
          WHERE
            a.orderId = "'.$search.'"  OR
            a.ndemPots LIKE "%'.$search.'%" OR
            a.ndemSpeedy like "%'.$search.'%" OR
            a.orderKontak = "'.$search.'" OR
            a.orderNcli like "%'.$search.'%" OR
            a.kcontact like "%'.$search.'%"
        ';
        break;
      default:
        $Where = '
          WHERE
          a.orderId = 0
        ';
        break;
    }
    return $Where;
  }

   private static function QueryBuilder($id){
     $SQL = '
     	SELECT
   			c.id as id_dt,
   			c.updated_at,
        c.updated_by as hd,
        c.updated_at as hd_date,
       	a.*,
       	b.*,
   			h.LOCN_X,
   			h.LOCN_Y,
   			h.OLT_IP,
   			i.OLT,
     		c.id_regu AS id_r,
     		g.uraian,
     		c.updated_at,
     		d.status_laporan,
     		d.catatan,
     		f.created_by,
 				a.orderId as status_wo_by_hd,
 				c.id as jumlah_material,
        j.*,
        j.id as id_dshr,
        a.Kcontact as Kcontact,
        c.dispatch_by,
        c.kordinatPel,
        c.alamatSales,
        pls.laporan_status
     	FROM
     		Data_Pelanggan_Starclick a
     	LEFT JOIN ms2n b ON a.orderNcli = b.Ncli
     	LEFT JOIN dispatch_teknisi c ON b.Ndem=c.Ndem OR a.orderId = c.Ndem
     	LEFT JOIN psb_laporan d ON c.id=d.id_tbl_mj
     	LEFT JOIN ms2n_sebab e ON b.Sebab = e.ms2n_sebab
     	LEFT JOIN ms2n_status_hd f ON b.Ndem=f.Ndem
     	LEFT JOIN regu g ON c.id_regu = g.id_regu
 		  LEFT JOIN odp_ftp h ON a.alproname = h.INDEX_NAME
 		  LEFT JOIN olt i ON h.OLT_IP = i.OLT_IP
      LEFT JOIN dshr j ON a.orderId = j.kode_sc
      LEFT JOIN psb_laporan_status pls ON d.status_laporan=pls.laporan_status_id
       '.$id.'
 		  ORDER BY a.orderDate DESC ';

      $query = DB::select($SQL);
      return $query;
  }
   private static function Transaksi(){
     $SQL = '
     SELECT
      a.*
     FROM
        dshr a
 		 ORDER BY a.modified_at DESC ';
     $query = DB::select($SQL);
     return $query;
  }

  public static function cekWoTeknisi($tgl, $idRegu)
  {
    return DB::table('dispatch_teknisi')
            ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
            ->select('dispatch_teknisi.id','dispatch_teknisi.id_regu','dispatch_teknisi.created_at','psb_laporan.id_tbl_mj','psb_laporan.status_laporan')
            ->where('dispatch_teknisi.id_regu','=',$idRegu)
            ->where('psb_laporan.status_laporan','=','6')
            ->where(function ($query) use ($tgl){
                  $query->where('dispatch_teknisi.created_at','LIKE','%'.$tgl.'%')
                        ->orwhere('dispatch_teknisi.updated_at','LIKE','%'.$tgl.'%');
              })
            ->get();
  }

  public static function getData($ndem)
  {
      return DB::table('dispatch_teknisi')->where('Ndem',$ndem)->first();
  }

  public static function updateManja($req, $Ndem, $manjaBy)
  {
      return DB::table('dispatch_teknisi')->where('id',$Ndem)->update([
          'manja_status'  => $req->manja_status,
          'manja'         => $req->manja,
          'manjaBy'       => $manjaBy
      ]);
  }

  public static function simpanManja($req, $manjaBy, $id)
  {
      return DB::table('dispatch_teknisi')->insert([
          'Ndem'                => $id,
          'updated_by'          => DB::raw('NOW()'),
          'manja'               => $req->manja,
          'created_at'          => DB::raw('NOW()'),
          'manja_status'        => $req->manja_status,
          'manjaBy'             => $manjaBy
      ]);
  }

  public static function getJmlWoBySektor($tgl)
  {
      $query = DB::SELECT('
      SELECT
        a.Ndem,
        b.status_laporan,
        c.jenisPsb as jenisPsb,
        d.TROUBLE_NO as close_status,
        d.JAM as umur,
        e.laporan_status,
        b.action,
        f.action as actionText,
        b.modified_at,
        b.created_at,
        c.sto,
        r.id_regu,
        r.uraian,
        gt.chat_id,
        SUM(CASE WHEN c.sto = "BBR" THEN 1 ELSE 0 END) as BBR,
        SUM(CASE WHEN c.sto = "MTP" THEN 1 ELSE 0 END) as MTP,
        SUM(CASE WHEN c.sto = "LUL" THEN 1 ELSE 0 END) as LUL,
        SUM(CASE WHEN c.sto is null or c.sto = "" THEN 1 ELSE 0 END) as lain,
        count(r.id_regu) as jumlahWo
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN nonatero_excel_bank d ON a.Ndem = d.TROUBLE_NO
      LEFT JOIN psb_laporan_status e ON b.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_action f ON f.laporan_action_id = b.action
      LEFT JOIn regu r on a.id_regu=r.id_regu
      LEFT JOIN group_telegram gt on r.mainsector=gt.chat_id
      WHERE
        ((DATE(b.modified_at) = "'.$tgl.'" OR DATE(b.created_at) = "'.$tgl.'" OR DATE(a.updated_at)="'.$tgl.'") OR ((b.status_laporan IN (5,6,7,31)
        OR b.id_tbl_mj is NULL) AND a.tgl LIKE "'.date('Y').'%")) and gt.chat_id="-1001108596633" GROUP By r.id_regu
    ');
    return $query;
  }

  public static function getJmlWoBySektorIdRegu($tgl, $sto, $idRegu)
  {
      if ($sto == 'ALL'){
          $where = ' and r.id_regu = "'.$idRegu.'"';
      }
      elseif ($sto=='lain'){
          $where = ' and (c.sto is null or c.sto="") and r.id_regu = "'.$idRegu.'"';
      }
      else {
          $where = ' and c.sto="'.$sto.'" and r.id_regu="'.$idRegu.'"';
      };

      $query = DB::SELECT('
      SELECT
        a.Ndem,
        b.status_laporan,
        c.jenisPsb as jenisPsb,
        d.TROUBLE_NO as close_status,
        d.JAM as umur,
        e.laporan_status,
        b.action,
        f.action as actionText,
        b.modified_at,
        b.created_at,
        c.sto,
        r.id_regu,
        r.uraian,
        gt.chat_id
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN nonatero_excel_bank d ON a.Ndem = d.TROUBLE_NO
      LEFT JOIN psb_laporan_status e ON b.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_action f ON f.laporan_action_id = b.action
      LEFT JOIn regu r on a.id_regu=r.id_regu
      LEFT JOIN group_telegram gt on r.mainsector=gt.chat_id
      WHERE
        ((DATE(b.modified_at) = "'.$tgl.'" OR DATE(b.created_at) = "'.$tgl.'" OR DATE(a.updated_at)="'.$tgl.'") OR ((b.status_laporan IN (5,6,7,31)
        OR b.id_tbl_mj is NULL) AND a.tgl LIKE "'.date('Y').'%")) and gt.chat_id="-1001108596633"'.$where);

    return $query;
  }

  public static function QueryBuilderTambahan($orderKontak){
     $SQL = '
      SELECT
        c.id as id_dt,
        c.updated_at,
        c.updated_by as hd,
        c.updated_at as hd_date,
        a.*,
        b.*,
        h.LOCN_X,
        h.LOCN_Y,
        h.OLT_IP,
        i.OLT,
        c.id_regu AS id_r,
        g.uraian,
        c.updated_at,
        d.status_laporan,
        d.catatan,
        f.created_by,
        a.orderId as status_wo_by_hd,
        c.id as jumlah_material,
        j.*,
        j.id as id_dshr,
        a.Kcontact as Kcontact,
        mn.manja_status_id,
        mn.manja_status,
        c.dispatch_by,
        c.dispatch_by,
        c.kordinatPel,
        c.alamatSales
      FROM
        Data_Pelanggan_Starclick a
      LEFT JOIN ms2n b ON a.orderNcli = b.Ncli
      LEFT JOIN dispatch_teknisi c ON b.Ndem=c.Ndem OR a.orderId = c.Ndem
      LEFT JOIN psb_laporan d ON c.id=d.id_tbl_mj
      LEFT JOIN ms2n_sebab e ON b.Sebab = e.ms2n_sebab
      LEFT JOIN ms2n_status_hd f ON b.Ndem=f.Ndem
      LEFT JOIN regu g ON c.id_regu = g.id_regu
      LEFT JOIN odp_ftp h ON a.alproname = h.INDEX_NAME
      LEFT JOIN olt i ON h.OLT_IP = i.OLT_IP
      LEFT JOIN dshr j ON a.orderId = j.kode_sc
      LEFT JOIN manja_status mn on c.manja_status=mn.manja_status_id
      Where a.orderKontak = "'.$orderKontak.'"
      GROUP BY a.orderId DESC ORDER BY a.orderDate DESC LIMIT 5';
      $query = DB::select($SQL);
      return $query;
  }

  public function listManja($tglAwal, $tglAkhir)
  {
      return DB::table('listmanja')
        ->where('orderStatusId',16)
        ->whereBetween('orderDate',[$tglAwal,$tglAkhir])
        ->orderBy('orderDate','DESC')
        ->paginate(20);
        // ->get();
  }

  public function manjaSearchBySc($ndem)
  {
      return DB::table('listmanja')
        ->where('orderId',$ndem)
        ->get();
  }

  public static function cariManja($myir){
      return DB::table('psb_myir_wo')->where('myir',$myir)->first();
  }

  public static function manjaSearchByMyir($ndem)
  {
      return DB::table('psb_myir_wo')
        ->leftJoin('dispatch_teknisi','psb_myir_wo.myir','=','dispatch_teknisi.Ndem')
        ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
        ->select('psb_myir_wo.*','dispatch_teknisi.*','regu.uraian','regu.mainsector')
        ->where('psb_myir_wo.ket','0')
        ->where('psb_myir_wo.myir',$ndem)
        ->get();
  }

  public static function cariMyir($myir){
      return DB::table('psb_myir_wo')->where('myir',$myir)->first();
  }

  public function jumlahManjaByUser($bulan)
  {
      $sql = 'SELECT a.manjaBy, 1_2_employee.nama, sum(case WHEN a.manja_status=1 THEN 1 ELSE 0 END) as manjaOk, sum(case WHEN a.manja_status=2 THEN 1 else 0 END) as manjaNOK FROM dispatch_teknisi a LEFT JOIN 1_2_employee on 1_2_employee.nik=a.manjaBy where a.updated_at like "%'.$bulan.'%" and a.manjaBy is not null and a.manjaBy<>0 GROUP by a.manjaBy';

      return DB::select($sql);
  }

  public static function getGroupTelegramProv(){
      return DB::table('group_telegram')
              ->where('sektor', 'LIKE', 'prov%')
              ->where('TL','!=','')
              ->get();
  }

  public static function getUnscByUnsc($unsc){
      return DB::table('unsc_tinjut')->where('unsc',$unsc)->first();
  }

  public static function getTeamMatrixNot($date){
    $get_where_area = 'AND f.title LIKE "%TERR%"';
    $sql = '
     SELECT
        b.id_regu,
        b.nik1,
        b.nik2,
        b.uraian,
        b.mainsector,
        b.kemampuan as target_MH,
        b.kemampuan,
        a.updated_at,
        b.mitra,
        f.chatTl,
        f.title,
        f.sektor,
        f.ket_sektor,
        d.modified_at,
        d.created_at,
        my.*,
        d.status_laporan,
        f.chat_id,
        b.sttsWo
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN group_telegram f ON b.mainsector = f.chat_id
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
      LEFT JOIN myirSCTgl my ON a.Ndem=my.myir
      WHERE
        ((DATE(d.modified_at) = "'.$date.'" OR DATE(d.created_at) = "'.$date.'") OR ((d.status_laporan IN (5,6,7,4) OR d.id_tbl_mj is NULL ) AND a.tgl LIKE "'.date('Y-m').'%" )) AND (a.dispatch_by IS NULL OR a.dispatch_by IN (1,2,3,4,5))
        '.$get_where_area.'
      GROUP BY a.id_regu 
    ';

    $query = DB::SELECT($sql);
    return $query;
  }

  public static function listWOSek($date,$id_regu){
    $query = DB::SELECT('
      SELECT
        a.Ndem,
        b.status_laporan,
        c.jenisPsb as jenisPsb,
        d.TROUBLE_NO as close_status,
        d.hari as umur,
        e.laporan_status,
        b.action,
        f.action as actionText,
        b.modified_at,
        b.created_at,
        a.created_at as tglDispatch,
        c.sto,
        TIMESTAMPDIFF(MINUTE,b.modified_at,"'.date("Y-m-d H:i:s").'") as interval1,
        TIMESTAMPDIFF(MINUTE,b.created_at,"'.date("Y-m-d H:i:s").'") as interval2,
        TIMESTAMPDIFF(HOUR,d.TROUBLE_OPENTIME,"'.date('Y-m-d H:i:s').'") as umur_gangguan,
        TIMESTAMPDIFF(HOUR,c.orderDate,"'.date('Y-m-d H:i:s').'") as umur_prov,
        a.updated_at,
        a.id as id_dt,
        b.catatan,
        d.TROUBLE_OPENTIME,
        c.orderDate,
        d.channel,
        a.dispatch_by
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN rock_excel_bank d ON a.Ndem = d.trouble_no
      LEFT JOIN psb_laporan_status e ON b.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_action f ON f.laporan_action_id = b.action
      WHERE
        ((DATE(b.modified_at) = "'.$date.'" OR DATE(b.created_at) = "'.$date.'" OR DATE(a.updated_at)="'.$date.'") OR ((b.status_laporan IN (6,7) OR b.id_tbl_mj is NULL) AND a.tgl LIKE "'.date('Y').'%")) AND
        a.id_regu = "'.$id_regu.'"
    ');
    return $query;
  }

  public static function getReguNotWo($date){
      $query = DB::select('select t1.uraian, t1.title from (select r.id_regu, r.uraian, d.Ndem, g.title, (SELECT abs1.approval from absen abs1 where abs1.nik = r.nik1 AND date(abs1.date_created)="'.$date.'") as nik1_absen, (SELECT abs1.approval from absen abs1 where abs1.nik = r.nik2 AND date(abs1.date_created)="'.$date.'") as nik2_absen from regu r left join dispatch_teknisi d on d.id_regu=r.id_regu and date(d.updated_at)= "'.$date.'" left join group_telegram g on r.mainsector=g.chat_id where r.ACTIVE=1 and g.sektor like "%prov%") as t1 where t1.nik1_absen=1 and t1.nik2_absen=1 and t1.Ndem is NULL ORDER BY t1.title ASC');

      return $query;
  }

  public static function statusAbsen($nik, $date){  
      return DB::table('absen')->where('date_created','LIKE','%'.$date.'%')->where('approval',1)->where('nik',$nik)->first();
  }

  public static function getTeamMatrixReboundary($date){
    $get_where_area = "AND b.sttsWo=1";
    $query = DB::SELECT('
     SELECT
        b.id_regu,
        b.nik1,
        b.nik2,
        b.uraian,
        b.mainsector,
        b.kemampuan as target_MH,
        b.kemampuan,
        a.updated_at,
        b.mitra,
        f.chatTl,
        f.title,
        f.sektor,
        f.ket_sektor,
        d.modified_at,
        d.created_at,
        my.*,
        d.status_laporan
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN group_telegram f ON b.mainsector = f.chat_id
      LEFT JOIN reboundary_survey my ON e.orderId=my.scid
      WHERE
        ((DATE(d.modified_at) = "'.$date.'" OR DATE(d.created_at) = "'.$date.'") OR ((d.status_laporan IN (5,6,7,4) OR d.id_tbl_mj is NULL ) AND a.tgl LIKE "'.date('Y-m').'%" )) AND a.dispatch_by = 6
        '.$get_where_area.'
      GROUP BY a.id_regu
    ');
  
    return $query;
  }

  public static function listWOReboundary($date,$id_regu){
    $query = DB::SELECT('
      SELECT
        a.Ndem,
        b.status_laporan,
        c.jenisPsb as jenisPsb,
        e.laporan_status,
        b.action,
        f.action as actionText,
        b.modified_at,
        b.created_at,
        a.created_at as tglDispatch,
        c.sto,
        a.updated_at,
        a.id as id_dt,
        b.catatan,
        c.orderDate,
        a.dispatch_by,
        a.jenis_layanan,
        e.grup
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN psb_laporan_status e ON b.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_action f ON f.laporan_action_id = b.action
      WHERE 
          ((DATE(b.modified_at) = "'.$date.'" OR DATE(b.created_at) = "'.$date.'") OR ((b.status_laporan IN (5,6,7,31,4) OR b.id_tbl_mj is NULL) AND a.tgl LIKE "'.date('Y-m').'%")) AND a.dispatch_by = 6 AND a.id_regu = "'.$id_regu.'"
    ');
    return $query;
  }

}
