@extends($layout)

@section('content')
  @include('partial.alerts')
  <h3>MATRIX OPEN MANJA KALSEL</h3>
  <style>
  td,th {
    padding: 4px;
  }
  td {
    vertical-align: top;
  }
  .label-KP {
    background-color: #ff6b6b;
  }
  .label-KT {
    background-color: #0abde3;
  }
  .label-UP {
    background-color: #1dd1a1;
  }
  .label-OGP {
    background-color: #feca57;
  }
  .label- {
    background-color: #8395a7;
  }
  .label-SISA {
    background-color: #8395a7;
  }



  </style>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          #GAUL
        </div>
        <div class="panel-body">
          <table class="table">
            <tr>
              <th>KPI</th>
              <th>TROUBLE_NO</th>
              <th>TROUBLE_NUMBER</th>
              <th>HASIL UKUR AKHIR</th>
              <th>SEBAB 1</th>
              <th>SEBAB 2</th>
              <th>SEBAB 3</th>
              <th>SEBAB 4</th>
            </tr>
            @foreach ($get_gaul as $num => $result)
            <tr>
              <td>GAUL</td>
              <td>{{ $result->TROUBLE_NO }}</td>
              <td>{{ $result->TROUBLE_NUMBER }}</td>
              <td>
                 $data_ibooster[$result->TROUBLE_NO][0]['ONU_Rx']
              </td>
              @foreach ($data_gaul[$result->TROUBLE_NO] as $gaul)
              <td>
                {{ $gaul->JENIS_GGN }} - {{ $gaul->action }} - {{ $gaul->penyebab }}<br />
                <small>{{ $gaul->keterangan }} ({{ $gaul->title }} - {{ $gaul->uraian }} - {{$gaul->DATE_CLOSE}})</small> 
              </td>
              @endforeach
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>    
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          #TTR3JAM
        </div>
        <div class="panel-body">
          <table class="table">
            <tr>
              <th>NO</th>
              <th>KPI</th>
              <th>TROUBLE_NO</th>
              <th>TROUBLE_NUMBER</th>
              <th>HASIL UKUR</th>
              <th>ACTION</th>
              <th>SEBAB</th>
              <th>JENIS_GGN</th>
              <th>TIM</th>
              <th>SEKTOR</th>
              <th>KETERANGAN</th>
            </tr>
            @foreach ($get_ttr3jam as $num => $result)
            <tr>
              <td>TTR 3 JAM</td>
              <td>{{ $result->TROUBLE_NO }}</td>
              <td>{{ $result->TROUBLE_NUMBER }}</td>
              <td>$data_ibooster[$result->TROUBLE_NO][0]['ONU_Rx']</td>
              <td>{{ $result->action }}</td>
              <td>{{ $result->penyebab }}</td>
              <td>{{ $result->JENIS_GGN }}</td>
              <td>{{ $result->uraian }}</td>
              <td>{{ $result->title }}</td>
              <td>{{ $result->keterangan }}</td>
            </tr>
            @endforeach
            @foreach ($get_ttr12jam as $num => $result)
            <tr>
              <td>TTR 12 JAM</td>
              <td>{{ $result->TROUBLE_NO }}</td>
              <td>{{ $result->TROUBLE_NUMBER }}</td>
              <td>$data_ibooster[$result->TROUBLE_NO][0]['ONU_Rx']</td>
              <td>{{ $result->action }}</td>
              <td>{{ $result->penyebab }}</td>
              <td>{{ $result->JENIS_GGN }}</td>
              <td>{{ $result->uraian }}</td>
              <td>{{ $result->title }}</td>
              <td>{{ $result->keterangan }}</td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
       
    </div>

  </div>