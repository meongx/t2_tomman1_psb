<div class="panel panel-primary">
    <div class="panel-heading">Material</div>
    <div class="panel-body">
      <div class="list-group fiber">
			 <div class="table-responsive">

			  <table class="table table-striped table-bordered" border="0">
			    <thead>
			      <tr>
			        <th width="20">No</th>
			        <th width="20">Area</th>
			        <th width="20">MDF</th>
			        <th width="50">Kode SC</th>
			        <th width="50">Order Status</th>
			        <th width="50">Tanggal PS</th>
			        <th width="50">ND</th>
			        <th width="50">ND Speedy</th>
			        <th width="50">Status Indihome</th>
			        <th width="150">Nama</th>
			        <th width="250">Tanggal PS</th>
			        <th width="250">Tanggal Dispatch</th>
			        <th width="150">Nama Tim</th>
			        <th width="150">KContact</th>
			        <th width="350">Alamat</th>
			        <th width="350">Kota</th>
			        <th width="50">DC</th>

			      </tr>
			    </thead>
			    <tbody>
				@foreach($data as $no => $list)
				  <tr>
			        <td>{{ ++$no }}</td>
			        <td>{{ $list['area'] ? : 'UNKNOWN' }}<span></span></td>
			        <td>{{ $list['sto'] }}<span></span></td>
			        <td>{{ $list['orderId'] }}<span></span></td>
			        <td>{{ $list['orderStatus'] }}<span></span></td>
			        <td>{{ $list['orderDatePs'] }}<span></span></td>
			        <td>{{ $list['ndemPots'] }}<span></span></td>
			        <td>{{ $list['ndemSpeedy'] }}<span></span></td>
			        <td>{{ $list['jenisPsb'] }}<span></span></td>
			        <td>{{ $list['orderName'] }}<span></span></td>
			        <td>{{ $list['orderDatePs'] }}</td>
			        <td>{{ $list['tgl_dispatch'] }}<span></span></td>
			        <td>{{ $list['uraian'] }}<span></span></td>
			        <td>{{ $list['kcontact'] }}<span></span></td>
			        <td>{{ $list['orderAddr'] }}<span></span></td>
			        <td>{{ $list['orderCity'] }}<span></span></td>
			        <td>{{ $list['DC'] }}</td>
			      </tr>
			    @endforeach
			    </tbody>
			  </table>
			 </div>
      </div>
    </div>
  </div>
