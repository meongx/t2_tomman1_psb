@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .panel-info .panel-heading {
      background-color: #54a0ff;
    }
    .customer_info {
      border-color: #FFF;
    }
    .customer_info td {
      padding : 5px;
    }
    .customer_field {
      align: right !important;

    }
  </style>
  <h3>Customer Info</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Input
        </div>
        <div class="panel-body">
          <form method="post">
            <table>
              <tr>
                <td>Cari Customer</td>
                <td>
                  &nbsp
                  &nbsp
                  :
                  &nbsp
                  &nbsp
                </td>
                <td><input type="text" name="customer" value="{{ $customer }}" /></td>
                <td>
                  <input type="submit" name="submit" value="submit">
                </td>
              </tr>
            </table>
        </form>
        </div>
      </div>
    </div>
    @if ($customer <> "")
    <a href="/createTicket/{{ $customer }}" class="btn btn-warning">BUAT PENGADUAN via Tomman</a>

    <div class="col-sm-8">
    <div class="col-sm-12">
      <div class="panel panel-info">
        <div class="panel-heading">
          Customer Info
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-6">
              <table class="customer_info" width="100%">
                <tr>
                  <td width=100 class="customer_field">Nama</td>
                  <td class="customer_value">{{ $customer_info->orderName }}</td>
                </tr>
                <tr>
                  <td width=100 class="customer_field">NDEM - POTS</td>
                  <td class="customer_value">{{ $customer_info->ndemPots }}</td>
                </tr>
                <tr>
                  <td width=100 class="customer_field">NDEM - INET</td>
                  <td class="customer_value">{{ $customer_info->ndemSpeedy }}</td>
                </tr>
                <tr>
                  <td width=100 class="customer_field">Alamat</td>
                  <td wraptext class="customer_value">{{ $customer_info->orderAddr }}</td>
                </tr>

              </table>
            </div>
            <div class="col-sm-6">
              <table class="table" width="100%">

                <tr>
                  <td width=100 class="customer_field">Tgl PS</td>
                  <td class="customer_value">{{ $customer_info->orderDatePs }}</td>
                </tr>
                <tr>
                  <td width=100 class="customer_field">SC.ID</td>
                  <td class="customer_value"><a href="/{{ $customer_info->id_dt }}">{{ $customer_info->orderId }}</a></td>
                </tr>


              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    @if ($ibooster <> "")
    <div class="col-sm-12">
      <div class="panel panel-warning">
        <div class="panel-heading">
          Hasil Ukur
        </div>
        <div class="panel-body">
          @if ($gamas <> "")
          <div class="alert alert-danger">{{ $gamas." ".$Calling_Station_Id }}</div>
          @endif
          <?php
            $data =  @(object) $ibooster[0];
          ?>
          <div class="row">
            <div class="col-sm-4">
              ND : {{ $data->ND }}<br />
              IP_Embassy : {{ $data->IP_Embassy }}<br />
              Type : {{ $data->Type }}<br />
              Calling_Station_Id : {{ $data->Calling_Station_Id }}<br />
              IP_NE : {{ $data->IP_NE }}<br />
              ONU_Link_Status : {{ $data->ONU_Link_Status }}<br />
            </div>
            <div class="col-sm-4">
              OLT_Tx : {{ $data->OLT_Tx }}<br />
              OLT_Rx : {{ $data->OLT_Rx }}<br />
              ONU_Tx : {{ $data->ONU_Tx }}<br />
              ONU_Rx : {{ $data->ONU_Rx }}<br />
              Framed_IP_Address : {{ $data->Framed_IP_Address }}<br />
            </div>
            @if ($data->ONU_Link_Status=="LOS")
            <a href="#" class="btn btn-warning">SILAHKAN OPEN TIKET via MyCX</a>

            <a href="/createTicket/{{ $customer }}" class="btn btn-warning">BUAT PENGADUAN via Tomman</a>
            @else
            <a href="/createTicket/{{ $customer }}" class="btn btn-warning">BUAT PENGADUAN via Tomman</a>
            @endif
          </div>
        </div>
      </div>
    </div>
    @endif
  </div>
  <div class="col-sm-4">
    <div class="panel panel-default">
      <div class="panel-heading">History Order</div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <td>Tiket</td>
            <td>Tgl Open</td>
            <td>Status</td>
          </tr>
          @foreach ($get_history_order as $history_order)
          <tr>
            <td><a href="/tiket/{{ $history_order->id_dt }}">{{ $history_order->Incident }}</a></td>
            <td>{{ $history_order->Reported_Date }}</td>
            <td>{{ $history_order->laporan_status ? : "NEED PROGRESS" }}</td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
    @endif
  </div>
@endsection
