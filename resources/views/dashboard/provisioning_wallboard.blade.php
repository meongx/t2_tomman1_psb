@extends('public_layout')

@section('content')
  @include('partial.alerts') 
    <style>
      td, th {
        text-align: center;
      }
      .left {
        text-align: left !important;
      }
    </style>
    <h3>FZ KALIMANTAN 2</h3>
    <small class="label label-danger">Lastsync : {{ $lastsync[0]->LASTSYNC }}</small>
    <div class="row">
          <div class="col-sm-6">
            <div class="table-responsive">
            <h5>ALL ORDER BY JAM PER WITEL</h5>
            <table class="table styledTable" border="1" style='width:100%' >
              <tr>
                <th rowspan="2">WITEL</th>
                <th colspan="7" align="CENTER">HOURS</th>
                <th rowspan="2">TOT</th>
                <th colspan="2" align="CENTER">DATA</th>
              </tr>
              <tr>
                <th>0-2</th>
                <th>2-4</th>
                <th>4-6</th>
                <th>6-8</th>
                <th>8-10</th>
                <th>10-12</th>
                <th>>12</th>
                <th><= 8</th>
                <th>>8</th>
              </tr>
              <?php
                $x0dan2jam = 0;
                $x2dan4jam = 0;
                $x4dan6jam = 0;
                $x6dan8jam = 0;
                $x8dan10jam = 0;
                $x10dan12jam = 0;
                $x12jam = 0;
                $x8jam = 0;
                $xlebih8jam = 0;
                $jumlah = 0;
              ?>
              @foreach ($query as $datax)
              <?php
              $x0dan2jam += $datax->x0dan2jam;
              $x2dan4jam += $datax->x2dan4jam;
              $x4dan6jam += $datax->x4dan6jam;
              $x6dan8jam += $datax->x6dan8jam;
              $x8dan10jam += $datax->x8dan10jam;
              $x10dan12jam += $datax->x10dan12jam;
              $x8jam += $datax->x8jam;
              $xlebih8jam += $datax->xlebih8jam;
              $x12jam += $datax->x12jam;
              $jumlah += $datax->jumlah;
              ?>
              <tr>
                <td class="left">{{ $datax->WITEL }}</td>
                <td><a href="/provisioningbyjam/0_2jam/{{ $datax->WITEL }}">{{ $datax->x0dan2jam }}</a></td>
                <td><a href="/provisioningbyjam/2_4jam/{{ $datax->WITEL }}">{{ $datax->x2dan4jam }}</a></td>
                <td><a href="/provisioningbyjam/4_6jam/{{ $datax->WITEL }}">{{ $datax->x4dan6jam }}</a></td>
                <td><a href="/provisioningbyjam/6_8jam/{{ $datax->WITEL }}">{{ $datax->x6dan8jam }}</a></td>
                <td><a href="/provisioningbyjam/8_10jam/{{ $datax->WITEL }}">{{ $datax->x8dan10jam }}</a></td>
                <td><a href="/provisioningbyjam/10_12jam/{{ $datax->WITEL }}">{{ $datax->x10dan12jam }}</a></td>
                <td><a href="/provisioningbyjam/12jam/{{ $datax->WITEL }}">{{ $datax->x12jam }}</a></td>
                <td><a href="/provisioningbyjam/ALL/{{ $datax->WITEL }}">{{ $datax->jumlah }}</a></td>
                <td><a href="/provisioningbyjam/8jam/{{ $datax->WITEL }}">{{ $datax->x8jam }}</a></td>
                <td><a href="/provisioningbyjam/lebih8jam/{{ $datax->WITEL }}">{{ $datax->xlebih8jam }}</a></td>
              </tr>
              @endforeach
              <tr>
                <td>TOTAL</td>
                <td><a href="/provisioningbyjam/0_2jam/ALL">{{ $x0dan2jam }}</a></td>
                <td><a href="/provisioningbyjam/2_4jam/ALL">{{ $x2dan4jam }}</a></td>
                <td><a href="/provisioningbyjam/4_6jam/ALL">{{ $x4dan6jam }}</a></td>
                <td><a href="/provisioningbyjam/6_8jam/ALL">{{ $x6dan8jam }}</a></td>
                <td><a href="/provisioningbyjam/8_10jam/ALL">{{ $x8dan10jam }}</a></td>
                <td><a href="/provisioningbyjam/10_12jam/ALL">{{ $x10dan12jam }}</a></td>
                <td><a href="/provisioningbyjam/12jam/ALL">{{ $x12jam }}</a></td>
                <td><a href="/provisioningbyjam/ALL/ALL">{{ $jumlah }}</a></td>
                <td><a href="/provisioningbyjam/8jam/ALL">{{ $x8jam }}</a></td>
                <td><a href="/provisioningbyjam/lebih8jam/ALL">{{ $xlebih8jam }}</a></td>
              </tr>
            </table>
          </div>
      </div>
      <div class="col-sm-6">
        <div class="table-responsive">
        <h5>ALL ORDER BY HARI PER WITEL</h5>
        <table class="table styledTable" border="1" style='width:100%'>
          <tr>
            <th rowspan="2">WITEL</th>
            <th colspan="8" align="CENTER">DAYS</th>
            <th rowspan="2">TOT</th>
            <th colspan="2" align="CENTER">DATA</th>
          </tr>
          <tr>
            <th><=1</th>
            <th>1-2</th>
            <th>2-3</th>
            <th>3-4</th>
            <th>4-7</th>
            <th>7-15</th>
            <th>15-30</th>
            <th>>30</th>
            <th><=1d</th>
            <th>>1d</th>
          </tr>
          <?php
            $hari1 = 0;
            $hari2 = 0;
            $hari3 = 0;
            $hari4 = 0;
            $hari7 = 0;
            $hari15 = 0;
            $hari30 = 0;
            $lebih30hari = 0;
            $jumlahx = 0;
            $kurang1hari = 0;
            $lebih1hari = 0;
          ?>
          @foreach ($query_hari as $data_hari)
          <?php
            $hari1 += $data_hari->hari1;
            $hari2 += $data_hari->hari2;
            $hari3 += $data_hari->hari3;
            $hari4 += $data_hari->hari4;
            $hari7 += $data_hari->hari7;
            $hari15 += $data_hari->hari15;
            $hari30 += $data_hari->hari30;
            $lebih30hari += $data_hari->lebih30hari;
            $kurang1hari += $data_hari->kurang1hari;
            $lebih1hari += $data_hari->lebih1hari;
            $jumlahx += $data_hari->jumlah;
          ?>
          <tr>
            <td class="left">{{ $data_hari->WITEL }}</td>
            <td><a href="/provisioningbyhari/hari1/{{ $data_hari->WITEL }}">{{ $data_hari->hari1 }}</a></td>
            <td><a href="/provisioningbyhari/hari2/{{ $data_hari->WITEL }}">{{ $data_hari->hari2 }}</a></td>
            <td><a href="/provisioningbyhari/hari3/{{ $data_hari->WITEL }}">{{ $data_hari->hari3 }}</a></td>
            <td><a href="/provisioningbyhari/hari4/{{ $data_hari->WITEL }}">{{ $data_hari->hari4 }}</a></td>
            <td><a href="/provisioningbyhari/hari7/{{ $data_hari->WITEL }}">{{ $data_hari->hari7 }}</a></td>
            <td><a href="/provisioningbyhari/hari15/{{ $data_hari->WITEL }}">{{ $data_hari->hari15 }}</a></td>
            <td><a href="/provisioningbyhari/hari30/{{ $data_hari->WITEL }}">{{ $data_hari->hari30 }}</a></td>
            <td><a href="/provisioningbyhari/lebih30hari/{{ $data_hari->WITEL }}">{{ $data_hari->lebih30hari }}</a></td>
            <td><a href="/provisioningbyhari/ALL/{{ $data_hari->WITEL }}">{{ $data_hari->jumlah }}</a></td>
            <td><a href="/provisioningbyhari/kurang1hari/{{ $data_hari->WITEL }}">{{ $data_hari->kurang1hari }}</a></td>
            <td><a href="/provisioningbyhari/lebih1hari/{{ $data_hari->WITEL }}">{{ $data_hari->lebih1hari }}</a></td>
          </tr>
          @endforeach
          <tr>
            <td>TOTAL</td>
            <td><a href="/provisioningbyhari/hari1/ALL">{{ $hari1 }}</a></td>
            <td><a href="/provisioningbyhari/hari2/ALL">{{ $hari2 }}</a></td>
            <td><a href="/provisioningbyhari/hari3/ALL">{{ $hari3 }}</a></td>
            <td><a href="/provisioningbyhari/hari4/ALL">{{ $hari4 }}</a></td>
            <td><a href="/provisioningbyhari/hari7/ALL">{{ $hari7 }}</a></td>
            <td><a href="/provisioningbyhari/hari15/ALL">{{ $hari15 }}</a></td>
            <td><a href="/provisioningbyhari/hari30/ALL">{{ $hari30 }}</a></td>
            <td><a href="/provisioningbyhari/lebih30hari/ALL">{{ $lebih30hari }}</a></td>
            <td><a href="/provisioningbyhari/ALL/ALL">{{ $jumlahx }}</a></td>
            <td><a href="/provisioningbyhari/kurang1hari/ALL">{{ $kurang1hari }}</a></td>
            <td><a href="/provisioningbyhari/lebih1hari/ALL">{{ $lebih1hari }}</a></td>
          </tr>
        </table>
      </div>
      </div>
    </div>
    <div class="row">
    <div class="col-sm-6">
      <br />
      <div class="table-responsive">
      <h5>ALL ORDER 2ND STB BY HARI PER WITEL</h5>
      <table class="styledTable" border="1" style='width:100%'>
        <tr>
          <th rowspan="2">WITEL</th>
          <th colspan="8" align="CENTER">DAYS</th>
          <th rowspan="2">TOT</th>
          <th colspan="2" align="CENTER">DATA</th>
        </tr>
        <tr>
          <th><=1</th>
          <th>1-2</th>
          <th>2-3</th>
          <th>3-4</th>
          <th>4-7</td>
          <th>7-15</th>
          <th>15-30</th>
          <th>>30</th>
          <th><=1d</th>
          <th>>1d</th>
        </tr>
        <?php
          $hari1 = 0;
          $hari2 = 0;
          $hari3 = 0;
          $hari4 = 0;
          $hari7 = 0;
          $hari15 = 0;
          $hari30 = 0;
          $lebih30hari = 0;
          $jumlahx = 0;
          $kurang1hari = 0;
          $lebih1hari = 0;
        ?>
        @foreach ($query2_hari as $data2_hari)
        <?php
          $hari1 += $data2_hari->hari1;
          $hari2 += $data2_hari->hari2;
          $hari3 += $data2_hari->hari3;
          $hari4 += $data2_hari->hari4;
          $hari7 += $data2_hari->hari7;
          $hari15 += $data2_hari->hari15;
          $hari30 += $data2_hari->hari30;
          $lebih30hari += $data2_hari->lebih30hari;
          $kurang1hari += $data2_hari->kurang1hari;
          $lebih1hari += $data2_hari->lebih1hari;
          $jumlahx += $data2_hari->jumlah;
        ?>
        <tr>
          <td class="left">{{ $data2_hari->WITEL }}</td>
          <td><a href="/provisioning2byhari/hari1/{{ $data2_hari->WITEL }}">{{ $data2_hari->hari1 }}</a></td>
          <td><a href="/provisioning2byhari/hari2/{{ $data2_hari->WITEL }}">{{ $data2_hari->hari2 }}</a></td>
          <td><a href="/provisioning2byhari/hari3/{{ $data2_hari->WITEL }}">{{ $data2_hari->hari3 }}</a></td>
          <td><a href="/provisioning2byhari/hari4/{{ $data2_hari->WITEL }}">{{ $data2_hari->hari4 }}</a></td>
          <td><a href="/provisioning2byhari/hari7/{{ $data2_hari->WITEL }}">{{ $data2_hari->hari7 }}</a></td>
          <td><a href="/provisioning2byhari/hari15/{{ $data2_hari->WITEL }}">{{ $data2_hari->hari15 }}</a></td>
          <td><a href="/provisioning2byhari/hari30/{{ $data2_hari->WITEL }}">{{ $data2_hari->hari30 }}</a></td>
          <td><a href="/provisioning2byhari/lebih30hari/{{ $data2_hari->WITEL }}">{{ $data2_hari->lebih30hari }}</a></td>
          <td><a href="/provisioning2byhari/ALL/{{ $data2_hari->WITEL }}">{{ $data2_hari->jumlah }}</a></td>
          <td><a href="/provisioning2byhari/kurang1hari/{{ $data2_hari->WITEL }}">{{ $data2_hari->kurang1hari }}</a></td>
          <td><a href="/provisioning2byhari/lebih1hari/{{ $data2_hari->WITEL }}">{{ $data2_hari->lebih1hari }}</a></td>
        </tr>
        @endforeach
        <tr>
          <td>TOTAL</td>
          <td><a href="/provisioning2byhari/hari1/ALL">{{ $hari1 }}</a></td>
          <td><a href="/provisioning2byhari/hari2/ALL">{{ $hari2 }}</a></td>
          <td><a href="/provisioning2byhari/hari3/ALL">{{ $hari3 }}</a></td>
          <td><a href="/provisioning2byhari/hari4/ALL">{{ $hari4 }}</a></td>
          <td><a href="/provisioning2byhari/hari7/ALL">{{ $hari7 }}</a></td>
          <td><a href="/provisioning2byhari/hari15/ALL">{{ $hari15 }}</a></td>
          <td><a href="/provisioning2byhari/hari30/ALL">{{ $hari30 }}</a></td>
          <td><a href="/provisioning2byhari/lebih30hari/ALL">{{ $lebih30hari }}</a></td>
          <td><a href="/provisioning2byhari/ALL/ALL">{{ $jumlahx }}</a></td>
          <td><a href="/provisioning2byhari/kurang1hari/ALL">{{ $kurang1hari }}</a></td>
          <td><a href="/provisioning2byhari/lebih1hari/ALL">{{ $lebih1hari }}</a></td>
        </tr>
      </table>
    </div>
    </div>
  </div>
@endsection
