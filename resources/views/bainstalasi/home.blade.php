
@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    td {
      padding : 10px;
    }
  </style>
<h3>BERITA ACARA INSTALASI NO USAGE </h3>
Saldo Anda Rp. {{ number_format($yours*1000) }}
<br />
<br />
<table>
  <tr>
    <td>
<table border="1">
  <tr>
    <td align="center">No</td>
    <td>No Internet</td>
    <td>Petugas</td>
    <td>BA (BARU)</td>
    <td>Action</td>
  </tr>
  @foreach ($data as $no => $result)


  <tr>
    <td align="center">{{ ++$no }}</td>
    <td>{{ $result->no_internet }}</td>
    <td>
      {{ $result->last_owner }}
    </td>
    <td align="center">
      <?php
      if (file_exists('upload/bainstalasinew/'.$result->no_internet.'.pdf')) :
      ?>
      <a href="/upload/bainstalasinew/{{ $result->no_internet }}.pdf">
        <img src="/image/pdf.png" width=50 alt="">
      </a>
      <?php
      endif;
      ?>
    </td>
    <td>
      <a class="label label-info" href="/ba/upload/{{ $result->no_internet }}">Upload</a>

      @if (session('auth')->id_karyawan=="91153709")
      <a class="label label-info" href="/ba/upload/{{ $result->no_internet }}">Re-Upload</a>
      @elseif ($result->status=="uploaded" && $result->updated_by==session('auth')->id_karyawan )
      <a class="label label-info" href="/ba/upload/{{ $result->no_internet }}">Re-Upload</a>
      @elseif ($result->status=="revisi" && $result->updated_by==session('auth')->id_karyawan)
      <a class="label label-info" href="/ba/upload/{{ $result->no_internet }}">Re-Upload</a>
      @elseif ($result->updated_by==session('auth')->id_karyawan)
      <a class="label label-info" href="/ba/upload/{{ $result->no_internet }}">Upload</a>
      @endif
      @if ($result->status=="uploaded")
      <span class="label label-success">Uploaded by {{ $result->updated_by }}</span>
      @elseif ($result->status=="booked")
      <span class="label label-default">Booked by {{ $result->updated_by }}</span>
      @elseif ($result->status=="revisi")
      <span class="label label-danger">Revisi {{ $result->updated_by }}</span>
      @else
      <a class="label label-warning" href="/ba/book/{{ $result->no_internet }}">BOOK</a>
      @endif
      <br /><span class="label label-default"> {{ $result->folder_ftp }}</span>

    </td>
  </tr>

  @endforeach
</table>
</td>
  <td valign=top>


  </td>


</tr>
</table>
@endsection
