@extends('layout')

@section('content')
  <script src="/js/dropzone.js"></script>
  <link rel="stylesheet" href="/css/dropzone.min.css">
  @include('partial.alerts')
  <div class="row">
    <div class="col-md-12" id="bio_tl">
      <div class="panel panel-default">
          <div class="panel-heading">
          Profile
          </div>
          <div class="panel-body table-responsive">
            <table style="padding:10px;">
              <tr>
                <td style="padding:15px">
                  <img src="/image/people.png" width="100" />
                </td>
                <td style="padding:10px">

                  {{ session('auth')->nama }}<br />
                  @foreach ($group_telegram as $gt)
                  <span class="label label-info">{{ $gt->title }}</span>
                  @endforeach
                  <br />
                  <small>
                    Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                  </small>
                </td>
                <td style="padding : 10px" valign=top>

                </td>
              </tr>

            </table>
            @include ('partial.home')
            <br>
           	
           	<div class="panel panel-primary">
				<div class="panel-heading">Send DC Area | Sektor : {{ count($getChatId) ? $getChatId[0]->title : '-' }}</div>
				<div class="panel-body">
					<table class="table table-bordered">
						<tr>
							<th>No</th>
							<th>RFC</th>
							<th>ID Item</th>
							<th>Item</th>
							<th>Jumlah</th>
							<th>Ket</th>
						</tr>
						
						@foreach($getData as $no=>$data)
							<tr>
								<td>{{ ++$no }}</td>
								<td>{{ $data->rfc_alista }}</td>
								<td>{{ $data->id_item }}</td>
								<td>{{ $data->item }}</td>
								<td>{{ $data->jumlah }}</td>
								@if($data->ket==3)
									<td><a href="{{ route('konfirmasi.proses',['id' => $data->id]) }}" class="btn btn-primary">Konfirmasi Terima</a></td>
								@else
									<td><button class="btn btn-success">Sudah Diterima</button></td>
								@endif
							</tr>
						@endforeach
					</table>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">Stok Sektor {{ count($getChatId) ? $getChatId[0]->title : '-' }}</div>
						<div class="panel-body">
							<table class="table table-bordered">
								<tr>
									<th>Item</th>
									<th>Stok</th>
									<th>Pemakaian</th>
									<th>Saldo</th>
								</tr>
								@foreach($ukuran as $ukur)
									<tr>
										<td>{{ $ukur }}</td>
										@foreach($dataSaldo[$ukur] as $dt)
											<td>{{ $dt->stok ?: '0' }}</td>
											<td>{{ $dt->pakai ?: '0'}}</td>
											<td>{{ $dt->stok - $dt->pakai }}</td>
										@endforeach
									</tr>
								@endforeach
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">Send DC Ke Teknisi</div>
						<div class="panel-body">
							<form>
								<label for="regu">Regu</label>
								<input type="text" name="regu" id="regu" value="">
								<input type="submit" value="Kirim" class="btn btn-primary">
								<hr>

								<div class="row">
									<div class="col-md-12">
										<div class="col-md-2">
											<div class="form-group">
												<label>100 | Stok : {{ $dataSaldo[100][0]->stok ? : '0' }}</label>
												<?php
													$disable = '';
													if ($dataSaldo[100][0]->stok==NULL){
														$disable = 'readonly';
													};
												 ?>
												<input type="text" name="split100" class="form-control" {{ $disable }}>
											</div>	
										</div>

										<div class="col-md-2">
											<div class="form-group">
												<label>150 | Stok : {{ $dataSaldo[150][0]->stok ? : '0' }} </label>
												<?php
													$disable = '';
													if ($dataSaldo[150][0]->stok==NULL){
														$disable = 'readonly';
													};
												 ?>
												<input type="text" name="split150" class="form-control" {{ $disable }} >
											</div>	
										</div>

										<div class="col-md-2">
											<div class="form-group">
												<label>200 | Stok : {{ $dataSaldo[200][0]->stok ? : '0' }} </label>
												<?php
													$disable = '';
													if ($dataSaldo[200][0]->stok==NULL){
														$disable = 'readonly';
													};
												 ?>
												<input type="text" name="split200" class="form-control" {{ $disable }} >
											</div>	
										</div>

										<div class="col-md-2">
											<div class="form-group">
												<label>250 | Stok : {{ $dataSaldo[250][0]->stok ? : '0' }}</label>
												<?php
													$disable = '';
													if ($dataSaldo[250][0]->stok==NULL){
														$disable = 'readonly';
													};
												 ?>
												<input type="text" name="split250" class="form-control" {{ $disable }} >
											</div>	
										</div>

										<div class="col-md-2">
											<div class="form-group">
												<label>300 | Stok : {{ $dataSaldo[300][0]->stok ? : '0' }}</label>
												<?php
													$disable = '';
													if ($dataSaldo[300][0]->stok==NULL){
														$disable = 'readonly';
													};
												 ?>
												<input type="text" name="split300" class="form-control" {{ $disable }} >
											</div>	
										</div>

										<div class="col-md-2">
											<div class="form-group">
												<label>350 | Stok : {{ $dataSaldo[350][0]->stok ? : '0' }}</label>
												<?php
													$disable = '';
													if ($dataSaldo[350][0]->stok==NULL){
														$disable = 'readonly';
													};
												 ?>
												<input type="text" name="split350" class="form-control" {{ $disable }} >
											</div>	
										</div>
									</div>
								</div>

							</form>

						</div>
					</div>
				</div>	
			</div>

          </div>
        </div>
      </div>
    </div>
@endsection
@section('plugins')
	<script>
		$(function(){
			var data = <?=json_encode($getRegu) ?>;
			$('#regu').select2({data:data});

			$('#regu').on('change',function(){	
				
			})
		})
	</script>
@endsection