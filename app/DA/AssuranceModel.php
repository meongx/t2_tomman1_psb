<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class AssuranceModel{

  public static function ttr3notcomply($query){
    return DB::SELECT($query);
  }

  public static function monitoringkpi_ttr3jam($sektor,$tgl){
    return DB::SELECT('
        SELECT
        b.*,
        a.*,
        e.*,
        f.*,
        h.*,
        c.Status as statusopen
        FROM
          data_nossa_1_log a
          LEFT JOIN mdf b ON a.Workzone = b.mdf
          LEFT JOIN data_nossa_1 c ON a.Incident = c.Incident
          LEFT JOIN dispatch_teknisi d ON a.Incident = d.Ndem
          LEFT JOIN regu e ON d.id_regu = e.id_regu
          LEFT JOIN group_telegram f ON e.mainsector = f.chat_id
          LEFT JOIN psb_laporan g ON d.id = g.id_tbl_mj
          LEFT JOIN psb_laporan_status h ON g.status_laporan = h.laporan_status_id
        WHERE
          b.sektor_asr = "'.$sektor.'" AND
          date(a.Booking_Date) = "'.$tgl.'"
      ');
  }

  public static function materialKembali($bulan, $tahun, $pid, $mitra, $id_item, $sektor){
    if ($pid=="ALL"){
        $where_pid = 'AND a.project IN ("R06-8/2019","R06-2/2019","R06-13/2019")';
    } else {
        $where_pid = 'AND a.project LIKE "'.$pid.'%"';
    }

    if ($mitra=="ALL"){
        $where_mitra2 = '';
    } else {
        $where_mitra2 = 'AND a.mitra LIKE "%'.$mitra.'%"';
    }

    if ($sektor=='ALL'){
        $where_sektor = '';
    }
    else{
        $where_sektor = 'AND d.chat_id="'.$sektor.'"';
    }

    $sql = 'SELECT
              b.id_item,
              sum(b.jmlKembali) as kembali
            FROM
              rfc_input a
            LEFT JOIN rfc_sal b ON b.alista_id = a.alista_id
            LEFT JOIN regu c ON a.id_regu = c.id_regu
            LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
            WHERE
              rfc LIKE "%'.$bulan.'/'.$tahun.'" AND
              id_item ="'.$id_item.'"
              '.$where_pid.'
              '.$where_mitra2.'
              '.$where_sektor.'
           ';

    $data = DB::select($sql);
    return $data;
  }

  public static function materialPakai($bulan, $tahun, $pid, $mitra, $id_item, $sektor){
    if ($pid=="ALL"){
        $where_pid = 'AND a.project IN ("R06-8/2019","R06-2/2019","R06-13/2019")';
    } else {
        $where_pid = 'AND a.project LIKE "'.$pid.'%"';
    }

    if ($mitra=="ALL"){
        $where_mitra2 = '';
    } else {
        $where_mitra2 = 'AND a.mitra LIKE "%'.$mitra.'%"';
    }

    if ($sektor=='ALL'){
        $where_sektor = '';
    }
    else{
        $where_sektor = 'AND d.chat_id="'.$sektor.'"';
    }

    $sql = 'SELECT
              b.id_item,
              sum(b.jmlTerpakai) as terpakai
            FROM
              rfc_input a
            LEFT JOIN rfc_sal b ON b.alista_id = a.alista_id
            LEFT JOIN regu c ON a.id_regu = c.id_regu
            LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
            WHERE
              rfc LIKE "%'.$bulan.'/'.$tahun.'" AND
              id_item ="'.$id_item.'"
              '.$where_pid.'
              '.$where_mitra2.'
              '.$where_sektor.'
           ';

    $data = DB::select($sql);
    return $data;
  }


  public static function kendala_teknik_by_umur(){
    return DB::SELECT('
      SELECT
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , d.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , d.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<12 THEN 1 ELSE 0 END) as kurang12jam,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , d.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=12 AND TIMESTAMPDIFF( HOUR , d.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as x12jam,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , d.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=24 THEN 1 ELSE 0 END) as x24jam,
      c.title
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN data_nossa_1_log d ON a.Ndem = d.Incident
      LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
        WHERE
          f.grup = "KT" AND YEAR(a.tgl)="'.date('Y').'" AND
          c.title LIKE "TERR%"
      GROUP BY
        c.title
      ');
  }

  public static function ttr3notcomply_sektor(){
    $start = date('Y-m-01');
    $end = date('Y-m-d');
    return DB::SELECT('
      SELECT
      d.title,
      count(*) as jumlah
      FROM
        detail_gangguan_close_3on3 a
      LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      WHERE
        (a.tgl_close BETWEEN "'.$start.'" AND "'.$end.'") AND
        a.ttr3_jam = 0
      GROUP BY d.title
      ORDER BY jumlah DESC
    ');
  }

  public static function trendgaullist($sektor,$date){
    $when_sektor = '';
    $when_date = '';
    if ($sektor <> "ALL"){
      $when_sektor = 'AND d.title = "'.$sektor.'"';
    }
    if ($date <> "ALL"){
      $when_date = 'AND date(a.tgl_close)="'.$date.'"';
    }
    return DB::SELECT('
      SELECT
      *
      FROM
        detail_gangguan_close a
      LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_action f ON e.action = f.laporan_action_id
      LEFT JOIN psb_laporan_penyebab g ON e.penyebabId = g.idPenyebab
        WHERE 1
         '.$when_sektor.'
         '.$when_date.'
    ');
  }
  public static function ttr3notcomplylist($sektor,$date){
    $when_sektor = '';
    $when_date = '';
    if ($sektor <> "ALL"){
      $when_sektor = 'AND d.title = "'.$sektor.'"';
    }
    if ($date <> "ALL"){
      $when_date = 'AND date(a.tgl_close)="'.$date.'"';
    }
    return DB::SELECT('
      SELECT
      *,
      "#NA" as is_gaul
      FROM
        detail_gangguan_close_3on3 a
      LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_action f ON e.action = f.laporan_action_id
      LEFT JOIN psb_laporan_penyebab g ON e.penyebabId = g.idPenyebab
        WHERE
          a.ttr3_jam = 0
         '.$when_sektor.'
         '.$when_date.'
    ');
  }

  public static function trendgaul_date(){
    return DB::SELECT('
      SELECT
      DATE(tgl_close) as date_close,
      DAY(tgl_close) as tgl_close
      FROM
        detail_gangguan_close
      GROUP BY date(tgl_close)
    ');
  }

  public function trendgaul(){
    return DB::SELECT('
      SELECT
        *
      FROM
        detail_gangguan_close a
      LEFT JOIN dispatch_teknisi b on a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      GROUP BY
        d.title
    ');
  }
  public static function trendgaul_sektor($query){
    return DB::SELECT($query);
  }

  public static function get_sektor(){
    return DB::SELECT('SELECT * FROM group_telegram a WHERE title LIKE "TERR%" AND urut IS NOT NULL ORDER BY urut ASC');
  }

  public static function get_sektor_matrix_team($mainsector,$date){
    return DB::SELECT('
      SELECT
      b.uraian,
      a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      WHERE
      b.ACTIVE=1 AND a.Ndem LIKE "IN%" AND c.chat_id = "'.$mainsector.'" AND (a.tgl="'.$date.'" OR DATE(d.modified_at) = "'.$date.'" OR
      (e.grup IN ("OGP","KP","KT") AND YEAR(a.tgl)="'.date('Y').'" AND MONTH(a.tgl)="'.date('m').'"))
      GROUP BY b.id_regu');
  }

  public static function get_sektor_matrix_team_order($id_regu,$date){
    return DB::SELECT('
      SELECT
      *,
      a.Ndem,
      d.grup,
      d.laporan_status,
      a.updated_at,
      a.id as id_dt,
      c.is_3hs as is_3HS,
      c.is_12hs as is_12HS
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN data_nossa_1_log e ON a.Ndem = e.Incident
      WHERE
      b.ACTIVE=1 AND a.Ndem LIKE "IN%" AND b.id_regu = "'.$id_regu.'" AND (a.tgl="'.$date.'" OR DATE(c.modified_at) = "'.$date.'" OR (c.status_laporan = 1 AND DATE(c.modified_at) = "'.$date.'") OR
      ((d.grup IN ("OGP","KP","KT","SISA ") OR c.id IS NULL) AND YEAR(a.tgl)="'.date('Y').'"))
    ');
  }

  public static function nonatero_gaul($periode,$rayon){
    return DB::SELECT('
        SELECT
          a.*,
          c.*,
          c.catatan as keterangan,
          f.uraian,
          d.action,
          e.penyebab,
          g.title
        FROM
          nonatero_detil_gaul a
        LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        LEFT JOIN psb_laporan_penyebab e ON c.penyebabId = e.idPenyebab
        LEFT JOIN regu f ON b.id_regu = f.id_regu
        LEFT JOIN group_telegram g ON f.mainsector = g.chat_id
        LEfT JOIN mdf h ON a.CMDF = h.mdf
          WHERE
            g.title NOT LIKE "CCAN%" AND
            a.IS_GAMAS = 0 AND
            h.sektor_asr = "'.$rayon.'" AND
            a.THNBLNTGL_CLOSE LIKE "'.$periode.'%"
          GROUP BY a.TROUBLE_NO
      ');
  }
   public static function nonatero_gaul_detil($number){
    return DB::SELECT('
        SELECT
          a.*,
          c.*,
          c.catatan as keterangan,
          f.uraian,
          d.action,
          e.penyebab,
          g.title
        FROM
          nonatero_detil_gaul a
        LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        LEFT JOIN psb_laporan_penyebab e ON c.penyebabId = e.idPenyebab
        LEFT JOIN regu f ON b.id_regu = f.id_regu
        LEFT JOIN group_telegram g ON f.mainsector = g.chat_id
        LEfT JOIN mdf h ON a.CMDF = h.mdf
          WHERE
            a.TROUBLE_NUMBER = "'.$number.'"
        ORDER BY a.DATE_CLOSE DESC
        LIMIT 4
      ');
  }
   public static function nonatero_ttr3jam($periode,$rayon){
    return DB::SELECT('
        SELECT
          a.*,
          c.*,
          c.catatan as keterangan,
          f.uraian,
          d.action,
          e.penyebab,
          g.title
        FROM
          nonatero_detil a
        LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        LEFT JOIN psb_laporan_penyebab e ON c.penyebabId = e.idPenyebab
        LEFT JOIN regu f ON b.id_regu = f.id_regu
        LEFT JOIN group_telegram g ON f.mainsector = g.chat_id
        LEfT JOIN mdf h ON a.CMDF = h.mdf
          WHERE
            g.title NOT LIKE "CCAN%" AND
            a.IS_GAMAS = 0 AND
            a.IS_CLOSE_3JAM = 1 AND
            a.IS_3HS = 0 AND
            h.sektor_asr = "'.$rayon.'" AND
            a.THNBLNTGL_CLOSE LIKE "'.$periode.'%"
      ');
  }
  public static function nonatero_ttr12jam($periode,$rayon){
    return DB::SELECT('
        SELECT
          a.*,
          c.*,
          c.catatan as keterangan,
          f.uraian,
          d.action,
          e.penyebab,
          g.title
        FROM
          nonatero_detil a
        LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        LEFT JOIN psb_laporan_penyebab e ON c.penyebabId = e.idPenyebab
        LEFT JOIN regu f ON b.id_regu = f.id_regu
        LEFT JOIN group_telegram g ON f.mainsector = g.chat_id
        LEfT JOIN mdf h ON a.CMDF = h.mdf
          WHERE
            g.title NOT LIKE "CCAN%" AND
            a.IS_GAMAS = 0 AND
            a.IS_12HS = 0 AND
            h.sektor_asr = "'.$rayon.'" AND
            a.THNBLNTGL_CLOSE LIKE "'.$periode.'%"
      ');
  }

  public static function get_tiketmanja($title,$date,$jam){
    if ($title=="") $title="-";
    if ($jam=="expired"){
      $where_jam = 'AND TIMESTAMPDIFF( HOUR , a.Booking_Date,  "'.date('Y-m-d H:i:s').'" )>3';
    } else {
      $where_jam = 'AND
        HOUR(a.Booking_Date) = "'.$jam.'"';
    }
    return DB::SELECT('
      SELECT
        a.*,
        f.laporan_status,
        f.grup
      FROM
        data_nossa_1 a
      LEFT JOIN dispatch_teknisi b On a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
      WHERE
        d.title = "'.$title.'" AND
        date(a.Booking_Date) = "'.$date.'"
        '.$where_jam.'
    ');
  }
  public static function get_tiketmanja2($title,$date,$jam){
    if ($title=="") $title="-";
    return DB::SELECT('
      SELECT
        a.Incident,
        f.laporan_status,
        f.grup
      FROM
        data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b On a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
      WHERE
        d.title = "'.$title.'" AND
        date(a.Booking_Date) = "'.$date.'" AND
        HOUR(a.Booking_Date) = "'.$jam.'" AND
        e.status_laporan <> 1

    ');
  }

  public static function manja($date){
    return DB::SELECT('
      SELECT
        SUM(CASE WHEN HOUR(a.Booking_Date)=8 THEN 1 ELSE 0 END) as jam8,
        SUM(CASE WHEN HOUR(a.Booking_Date)=9 THEN 1 ELSE 0 END) as jam9,
        SUM(CASE WHEN HOUR(a.Booking_Date)=10 THEN 1 ELSE 0 END) as jam10,
        SUM(CASE WHEN HOUR(a.Booking_Date)=11 THEN 1 ELSE 0 END) as jam11,
        SUM(CASE WHEN HOUR(a.Booking_Date)=12 THEN 1 ELSE 0 END) as jam12,
        SUM(CASE WHEN HOUR(a.Booking_Date)=13 THEN 1 ELSE 0 END) as jam13,
        SUM(CASE WHEN HOUR(a.Booking_Date)=14 THEN 1 ELSE 0 END) as jam14,
        SUM(CASE WHEN HOUR(a.Booking_Date)=15 THEN 1 ELSE 0 END) as jam15,
        SUM(CASE WHEN HOUR(a.Booking_Date)=16 THEN 1 ELSE 0 END) as jam16,
        SUM(CASE WHEN HOUR(a.Booking_Date)=17 THEN 1 ELSE 0 END) as jam17,
        d.title
      FROM
        data_nossa_1 a
      LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      WHERE
        date(a.Booking_Date) = "'.$date.'" AND
        a.Assigned_by = "CUSTOMERASSIGNED"
        GROUP BY d.chat_id
    ');
  }
  public static function manja2($date){
    return DB::SELECT('
      SELECT
        SUM(CASE WHEN HOUR(a.Booking_Date)=8 THEN 1 ELSE 0 END) as jam8,
        SUM(CASE WHEN HOUR(a.Booking_Date)=9 THEN 1 ELSE 0 END) as jam9,
        SUM(CASE WHEN HOUR(a.Booking_Date)=10 THEN 1 ELSE 0 END) as jam10,
        SUM(CASE WHEN HOUR(a.Booking_Date)=11 THEN 1 ELSE 0 END) as jam11,
        SUM(CASE WHEN HOUR(a.Booking_Date)=12 THEN 1 ELSE 0 END) as jam12,
        SUM(CASE WHEN HOUR(a.Booking_Date)=13 THEN 1 ELSE 0 END) as jam13,
        SUM(CASE WHEN HOUR(a.Booking_Date)=14 THEN 1 ELSE 0 END) as jam14,
        SUM(CASE WHEN HOUR(a.Booking_Date)=15 THEN 1 ELSE 0 END) as jam15,
        SUM(CASE WHEN HOUR(a.Booking_Date)=16 THEN 1 ELSE 0 END) as jam16,
        SUM(CASE WHEN HOUR(a.Booking_Date)=17 THEN 1 ELSE 0 END) as jam17,
        d.title
      FROM
        data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      WHERE
        date(a.Booking_Date) = "'.$date.'" AND
        e.status_laporan <> 1 AND
        a.Assigned_by = "CUSTOMERASSIGNED"
        GROUP BY d.chat_id
    ');
  }

  public static function gaulsektor($periode){
    $data = DB::SELECT('
      SELECT
        d.title,
        a.TROUBLE_NO as ticket_id,
        SUM(CASE WHEN aa.TROUBLE_NO<>"" THEN 1 ELSE 0 END) as gaulv2,
        count(*) as JML
      FROM
        nonatero_detil a
      LEFT JOIN nonatero_detil_gaul aa ON (a.TROUBLE_NO = aa.TROUBLE_NO AND aa.THNBLN = "'.$periode.'")
      LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
        WHERE
        a.THNBLN = "'.$periode.'" AND
        a.IS_GAMAS = 0 AND
        d.title <> ""
      GROUP BY d.title
      ORDER BY JML DESC
    ');
    return $data;
  }

  public static function gaulsektorlist($sektor){
    $data = DB::SELECT('
      SELECT
        *
      FROM
        detail_gangguan_close a
      LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_action f ON e.action = f.laporan_action_id
      LEFT JOIN psb_laporan_penyebab g ON e.penyebabId = g.idPenyebab
        WHERE
        d.title = "'.$sektor.'"
      ORDER BY
        a.is_gaul DESC
    ');
    return $data;
  }

  public static function gaulgenerator($periode){
    $data = DB::SELECT('
    SELECT
      *,
      e.action as actionText
    FROM nonatero_detil_gaul a
      LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN psb_laporan_action e ON c.action = e.laporan_action_id
      LEFT JOIN psb_laporan_penyebab f ON c.penyebabId = f.idPenyebab
      LEFT JOIN regu g ON b.id_regu = g.id_regu
      LEFT JOIN group_telegram h ON g.mainsector = h.chat_id
    WHERE a.THNBLN = "'.$periode.'"
    ORDER BY a.TROUBLE_NUMBER
    ');
    return $data;
  }

  public static function dashboard_gaul_by_seq_list($action,$periode){
    $data = DB::SELECT('
    SELECT
      *,
      e.action as actionText
    FROM nonatero_detil_gaul a
      LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN psb_laporan_action e ON c.action = e.laporan_action_id
      LEFT JOIN psb_laporan_penyebab f ON c.penyebabId = f.idPenyebab
      LEFT JOIN regu g ON b.id_regu = g.id_regu
      LEFT JOIN group_telegram h ON g.mainsector = h.chat_id
    WHERE
      a.THNBLN = "'.$periode.'" AND
      a.SEQ = "'.$action.'"
    ORDER BY a.TROUBLE_NUMBER
    ');
    return $data;
  }

  public static function dashboard_gaul_by_seq($periode,$sort){
    $data = DB::SELECT('
      SELECT
        *,
        count(*) as jumlah
      FROM
        nonatero_detil_gaul a
      WHERE
        a.THNBLN LIKE "'.$periode.'%" AND
        a.SEQ NOT IN ("LOGIC»","FISIK»","NA»","NONFISIK»")
      GROUP BY
        a.SEQ
      ORDER BY
        jumlah '.$sort.'
    ');
    return $data;
  }

  public static function getDataIboosterByIn($inOrder){
      return DB::table('ibooster')->where('order_id',$inOrder)->first();
  }

  public static function detailWoMaterial($id_item){
      $sql = 'SELECT
                a.*,
                c.Ndem,
                c.dispatch_by,
                c.id as id_dt
              FROM
                psb_laporan_material a
              LEFT JOIN psb_laporan b ON a.psb_laporan_id=b.id
              LEFT JOIN dispatch_teknisi c ON b.id_tbl_mj=c.id
              WHERE
                a.id_item_bantu="'.$id_item.'"
              ';

      $data = DB::select($sql);
      return $data;
  }

  public static function getStatusOrderMaintenance($bulan){
      $sql = 'SELECT
                a.*,
                b.*,
                c.*,
                d.uraian,
                e.chat_id,
                e.title
              FROM
                data_nossa_1_log a
              LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
              LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
              LEFT JOIN regu d ON b.id_regu = d.id_regu
              LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
              WHERE
                c.status_laporan = 2 AND
                b.tgl LIKE "%'.$bulan.'%"
              ';
      $data = DB::select($sql);
      return $data;
  }

  public static function getStatusKirimTeknisi($bulan){
      $sql = 'SELECT
                a.*,
                b.*,
                c.*,
                d.uraian,
                e.chat_id,
                e.title
              FROM
                data_nossa_1_log a
              LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
              LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
              LEFT JOIN regu d ON b.id_regu = d.id_regu
              LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
              WHERE
                c.status_laporan = 67 AND
                b.tgl LIKE "%'.$bulan.'%"
              ';
      $data = DB::select($sql);
      return $data;
  }

  public static function getManjaForBot($date,$jam){
    $jamCari = '(';
    for($a=8; $a<=$jam; $a++){
        $jamCari .= '"'.$a.'"'.',';
    };

    $jamCari = substr($jamCari, 0, -1);
    $jamCari .=')';

    $where_jam = 'AND HOUR(a.Booking_Date) IN '.$jamCari;

    return DB::SELECT('
      SELECT
        a.Incident,
        d.title,
        c.uraian,
        HOUR(a.Booking_Date) as jamBooking,
        f.laporan_status,
        a.Service_ID
      FROM
        data_nossa_1 a
      LEFT JOIN dispatch_teknisi b On a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
      WHERE
        b.Ndem <>"" AND
        a.Assigned_by="CUSTOMERASSIGNED" AND
        (e.status_laporan NOT IN ("1","37","38") OR e.status_laporan IS NULL) AND
        date(a.Booking_Date) = "'.$date.'"
        '.$where_jam.'
    ');
  }
}
?>
