@extends('layout')

@section('content')
  @include('partial.alerts')
  <a href="/dashboard/provisioning/{{ $tgl }}/{{ $jenis }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List Migrasi </h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>No.</th>
          <th>STO</th>
          <th>AREA</th>
          <th>NAMA TIM</th>
          <th>NAMA MITRA</th>
          <th>TGL DISPATCH</th>
          <th>SC</th>
          <th>NCLI</th>
          <th>ND</th>
          <th>ND SPEEDY</th>
          <th>NAMA PELANGGAN</th>
          <th>KORDINAT</th>
          <th>JENISPSB</th>
          <th>STATUS SC</th>
          <th>STATUS TEK.</th>
          <th>CATATAN TEK.</th>
          <th>DC</th>
        </tr>
        @foreach ($data as $num => $result)
        <tr>
          <td>{{ ++$num }}</td>
          <td>{{ $result->sto }}</td>
          <td>{{ $result->area_migrasi }}</td>
          <td>{{ $result->uraian }}</td>
          <td>{{ $result->mitra }}</td>
          <td>{{ $result->tanggal_dispatch }}</td>
          <td>{{ $result->orderId }}</td>
          <td>{{ $result->orderNcli }}</td>
          <td>{{ $result->ndemPots }}</td>
          <td>{{ $result->ndemSpeedy }}</td>
          <td>{{ $result->orderName }}</td>
          <td>{{ $result->kordinat_pelanggan }}</td>
          <td>{{ $result->jenisPsb }}</td>
          <td>{{ $result->orderStatus }}</td>
          <td>{{ $result->laporan_status }}</td>
          <td>{{ $result->catatan }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    </div>
  </div>
@endsection
