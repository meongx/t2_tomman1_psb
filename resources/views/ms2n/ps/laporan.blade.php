@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    MS2N NOSSF
    <a href="/ms2n/sync/PS" class="btn btn-info">
      <span class="glyphicon glyphicon-refresh"> Sync Now</span>
    </a>
  </h3>
  <div>
    <div class="input-group">
      <div class="input-group-btn">
        <button type="button" class="btn btn-default aksi" aria-label="Left Align">
          <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
        </button>
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
        <ul class="dropdown-menu">
          <li><a href="#" id="daily">Daily</a></li>
          <li><a href="#" id="monthly">Monthly</a></li>
         
        </ul>
      </div><!-- /btn-group -->
      <div class="ins">
        <input type="text" class="form-control" id="tanggal" value="{{ $tgl_ps }}">
      </div>
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->


  <br/>
  <div class="panel panel-primary">
    <div class="panel-heading">REKAP STATUS MS2N NOSSF {{ $tgl_ps }}</div>
    <div class="panel-body">
      <div class="list-group fiber">
			 <div class="table-responsive">  
				       
			  <table class="table" border="0">
			    <thead>

			    </thead>
			    <tbody>
						{{-- */
							$total_bjm = 0;
							$total_bjb = 0;
							$total_bln = 0;
							$total_ttg = 0;
							$total_total = 0;
							$jumlah = 0;
							$PIC  = '';
						/* --}} 
				    @foreach($get_ps_harian as $no => $ps_harian) 
				    {{-- */
							$total_bjm = $total_bjm + $ps_harian->BJM;
							$total_bjb = $total_bjb + $ps_harian->BJB;
							$total_bln = $total_bln + $ps_harian->BLN;
							$total_ttg = $total_ttg + $ps_harian->TTG;
							$jumlah = $ps_harian->BJM + $ps_harian->BJB + $ps_harian->BLN + $ps_harian->TTG;
							$total_total = $total_total + $jumlah;
						/* --}} 
					
					@if ($PIC<>$ps_harian->ms2n_sebab_pic)
					<tr>
						<td colspan=6><br /><b>{{ $ps_harian->ms2n_sebab_pic }}</b></td>
					</tr>
					  <tr>
						<th width="100">Sebab</th>
						<th width="50">BJM</th>
						<th width="50">BJB</th>
						<th width="50">BLN</th>
						<th width="50">TTG</th>
						<th width="50">Jumlah</th>
					  </tr>					
					{{-- */
						$PIC = $ps_harian->ms2n_sebab_pic;
					/* --}}
					@endif
				    <tr>
			        <td>{{ $ps_harian->Sebab }}</td>
			        <td><a href="/getdata/{{ $ps_harian->Sebab }}/BJM">{{ $ps_harian->BJM }}</a></td>
			        <td><a href="/getdata/{{ $ps_harian->Sebab }}/BJB">{{ $ps_harian->BJB }}</a></td>
			        <td><a href="/getdata/{{ $ps_harian->Sebab }}/BLN">{{ $ps_harian->BLN }}</a></td>
			        <td><a href="/getdata/{{ $ps_harian->Sebab }}/TTG">{{ $ps_harian->TTG }}</a></td>
					<td><a href="/getdata/{{ $ps_harian->Sebab }}/ALL">{{ $jumlah }}</a></td>
					
					</tr>
					

			      @endforeach
			    </tbody>
			      <tr>
			        <th >Total</th>
			        <th ><a href="/getdata/ALL/BJM">{{ $total_bjm }}</th>
			        <th ><a href="/getdata/ALL/BJB">{{ $total_bjb }}</th>
			        <th ><a href="/getdata/ALL/BLN">{{ $total_bln }}</th>
			        <th ><a href="/getdata/ALL/TTG">{{ $total_ttg }}</th>
					<th ><a href="/getdata/ALL/ALL">{{ $total_total }}</th>
					
			        
			      </tr>
			  </table>
			 </div>
      </div>
    </div> 
  </div>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />
  <script>
    $(function(){
      var day = {
        format: "yyyy-mm-dd",
        viewMode: 0, 
        minViewMode: 0
      };
      var month = {
        format: "yyyy-mm",
        viewMode: 2, 
        minViewMode: 1
      };
      $("input").datepicker(day)
      	.on('changeDate',function(e){
	      	document.location = '/ms2n/PS/'+$(this).val();
	      });



      $("#daily").click(function(){
        $("input").remove();
        $(".ins").after("<input type=text class=form-control id=tanggal>");
        $("input").datepicker(day);
      });

      $("#monthly").click(function(){
        $("input").remove();
        $(".ins").after("<input type=text class=form-control id=tanggal>");
        $("input").datepicker(month);
      });
      $(".aksi").click(function(){
        var tgl = document.getElementById("tanggal").value;
        if(tgl == ""){
          alert("Can you please select the date? or i will smash u down!");
        }else{
          $(".loading").css({"visibility":"visible"});
          $.getJSON( "/ms2n/getJsonPs/" + tgl, function( data ) {
            var fiber = [];
            var dslam = [];
            var msan = [];
            $.each( data, function( key, val) {
              fiber.push( "<li class='list-group-item'>" + val.Nama + "</li>" );
              console.log(val);
            });
            $(".list-group").html(fiber);
            $(".loading").css({"visibility":"hidden"});
          });
        }
      });
        
    });
  </script>
@endsection
