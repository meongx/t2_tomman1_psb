<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\DA\EmployeeModel;

class EmployeeController extends Controller
{
	public function index(){
		$getAll = EmployeeModel::getAll();
		$auth   = session('auth');
		return view('employee.index',compact('getAll', 'auth'));
	}
	public function input($id){
		$getDistinctPSA = EmployeeModel::distinctPSA();
		$getDistinctCATJOB = EmployeeModel::distinctCATJOB();
		$getDistinctSubDirectorat = EmployeeModel::distinctSubDirectorat();
		$getDistinctPosition = EmployeeModel::distinctPosition();
		$getSQUAD = EmployeeModel::getSQUAD();
		$getMitraAmija = EmployeeModel::getMitraAmija();
		$data = EmployeeModel::getById($id);
		$getWitel = DB::SELECT('SELECT witel FROM maintenance_datel GROUP BY witel');
		$auth   = session('auth');
		return view('employee.input',compact('getWitel','getMitraAmija','getSQUAD','getDistinctPSA','getDistinctCATJOB','getDistinctSubDirectorat','getDistinctPosition', 'data', 'auth'));
	}
	public function store(Request $req){
		date_default_timezone_set("Asia/Makassar");
		$auth = session('auth');
		if(isset($req->id_people)){
		 	EmployeeModel::update($req,$auth->id_karyawan);
		 	return back()->with('alerts', [
				['type' => 'success', 'text' => '<strong>SUCCESS UPDATE!</strong>']
			]);
		}
		else{
			if ($req->input('nik')!=""){
			 	$check = DB::table('1_2_employee')
			 								->where('nik',$req->input('nik'))
			 								->get();
			 	echo count($check);
			 	if (count($check)>0){
				 	return back()->with('alerts', [
			        ['type' => 'danger', 'text' => '<strong>FAILED ! NIK SUDAH DIGUNAKAN</strong>']
			    ]);
			 	} else {
					EmployeeModel::store($req,$auth->id_karyawan);

					// user defaault
				 	$exists = DB::table('user')->where('id_user',$auth->id_karyawan)->get();
				 	if (count($exists)==0){
				 		DB::table('user')->insert([
				 			'id_user'		=> $req->nik,
				 			'password'		=> md5('telkomAkses'),
				 			'id_karyawan'	=> $req->nik,
				 			'level'			=> '10',
				 			'witel'			=> '1',
				 			'status'		=> '0',
				 		]);
				 	};

					return back()->with('alerts', [
				        ['type' => 'success', 'text' => '<strong>SUCCESS INSERT!</strong>']
				    ]);
			  	}
		  	} else {
			 	return back()->with('alerts', [
		        	['type' => 'danger', 'text' => '<strong>FAILED !</strong>']
		    	]);
		  	}
		}
	}
}
