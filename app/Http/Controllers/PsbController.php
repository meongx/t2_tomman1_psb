<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Telegram;
use DB;
use Validator;
use App\DA\PsbModel;
use App\DA\HomeModel;
use App\DA\QcondeskModel;
use App\DA\GrabModel;
use App\DA\Tele;
use TG;

class PsbController extends Controller
{
  // protected $photoInputs = [
  //   'Foto_Action','Lokasi', 'ODP', 'Redaman_ODP', 'Redaman_Pelanggan', 'Instalasi_Kabel', 'SN_ONT', 'SN_STB', 'LiveTV',
  //   'TVOD','RFC_Form', 'Speedtest', 'Berita_Acara', 'Telephone_Incoming', 'Telephone_Outgoing', 'Wifi_Analyzer', 'Foto_Pelanggan', 'Pullstrap', 'Tray_Cable', 'K3','MyIndihome_Pelanggan','Login_MyIndihome','KLEM_S', 'LABEL','QC_1','QC_2','QC_3','Excheck_Helpdesk'
  // ];

  protected $photoInputs = [
    'Foto_Action','Lokasi','Lokasi_1','Lokasi_2', 'ODP','ODP_1','ODP_2', 'Redaman_ODP', 'Redaman_Pelanggan', 'Instalasi_Kabel', 'SN_ONT', 'SN_STB', 'Live_TV',
    'TVOD','RFC_Form', 'Speedtest', 'Berita_Acara', 'Telephone_Incoming', 'Telephone_Outgoing', 'Wifi_Analyzer', 'Foto_Pelanggan_dan_Teknisi', 'Pullstrap', 'Tray_Cable', 'K3','KLEM_S', 'LABEL','Excheck_Helpdesk','Patchcore','Stopper','Breket','Roset','BA_Digital', 'Meteran_Rumah',
    'additional_1','additional_2','additional_3','additional_4','additional_5'
  ];

  protected $photoReboundary = [
    'Lokasi_Pelanggan', 'Redaman_Ont', 'Berita_Acara', 'ODP_Baru', 'ODP_Lama', 'Label', 'Sn_Ont_Baru', 'Sn_Ont_Lama', 'Port_Odp_Lama', 'Action_Penurunan_Dropcore', 'Ex_Dropcore','Speedtest', 'Test_Live_TV'
  ];

  protected $photokpro = [
    'BA_Digital',
    'Berita_Acara',
    'Speedtest',
    'SN_ONT',
    'ODP',
    'Lokasi',
    'Telephone_Incoming',
    'Foto_Pelanggan_dan_Teknisi',
    'LABEL',
    'KLEM_S',
    'K3',
    'Redaman_ODP',
    'SN_STB',
    'Live_TV'
  ];

  protected $assuranceInputs = [
    'Foto_Action','Berita_Acara','Rumah_Pelanggan','Test_Layanan_Internet', 'Test_Layanan_TV', 'Test_Layanan_Telepon', 'MODEM', 'SN_ONT_RUSAK'
  ];

  protected $photoCommon = ['Sebelum', 'Progress', 'Sesudah', 'Foto-GRID', 'Denah-Lokasi'];
  protected $photoRemo = ['ODP-dan-Redaman-GRID', 'Sebelum', 'Progress', 'Sesudah', 'Pengukuran', 'Pelanggan_GRID', 'BA-Remo'];
  protected $photoOdpLoss = ['ODP', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID'];

  private $process;
	private $pipes;
	private $core;

// cek history teknisi dimatikan
  public function check_history(){
    $input = '';
    $alert = '';
    $alertText = '';
    $alert_asr = '';
    $alertText_asr = '';

    return view('psb.check_history',compact('input','alert','alertText','alert_asr','alertText_asr'));
  }
// check history teknisi
  public function exec_check_history(Request $req){
    $input = $req->input('input');
    $check_tomman = DB::SELECT('
      SELECT *,b.id as id FROM
        Data_Pelanggan_Starclick a
      LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN psb_laporan d ON b.id = d.id_tbl_mj
      WHERE
        a.ndemSpeedy LIKE "'.$input.'%" OR
        a.internet = "'.$input.'"
      ORDER by a.orderDate DESC
    ');
    if (count($check_tomman)>0){
      $alert = "success";
      $alertText = "Data ditemukan";
    } else {
      $alert = "danger";
      $alertText = "Data tidak ditemukan";
    }
    $check_tomman_asr = DB::SELECT('
      SELECT *,b.id as id FROM
        data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN psb_laporan d ON b.id = d.id_tbl_mj
      WHERE
        a.Service_ID LIKE "%'.$input.'%"
    ');
    if (count($check_tomman_asr)>0){
      $alert_asr = "success";
      $alertText_asr = "Data ditemukan";
    } else {
      $alert_asr = "danger";
      $alertText_asr = "Data tidak ditemukan";
    }
    return view('psb.check_history',compact('input','alert','alertText','check_tomman','alert_asr','alertText_asr','check_tomman_asr'));
  }

  public function sendkprobySC(){
    $date = date('Y-m-d');
    return view('psb.sendkpro',compact('date'));
  }

  public function sendkprobySC_save(Request $req){
    $data = explode(",", $req->input('SC'));
    if (count($data)>0){
      for ($i=0;$i<count($data);$i++){
        echo $data[$i]."<br />";
        $sc = str_replace(array(" ","\n","\r","\r\n"), "", $data[$i]);
        $this->sendtobotkpro($sc);
        sleep(20);
      }
    } else {
      echo "NO DATA FOUND";
    }
  }

  public function sendkprodaily($sektor){
       ini_set('max_execution_time', '30000000000000');
    $query = DB::SELECT('
      select
        *
      from
        Data_Pelanggan_Starclick a
      LEFT JOIN mdf b ON a.sto = b.mdf
      LEFT JOIN dispatch_teknisi c ON a.orderId = c.Ndem
      WHERE b.sektor_asr = "'.$sektor.'" AND DATE(a.orderDatePs)="'.date('Y-m-d').'" AND c.Ndem <> ""');
    foreach ($query as $num => $result){
        echo ++$num.". ";
        echo $result->orderId." | ".$result->id."<br />";
        $this->sendtobotkpro($result->orderId);
        sleep(20);
      }
    }

    public function sendbyfotoname($name,$sektor,$date){
      ini_set('max_execution_time', '30000000000000');
      $query = DB::SELECT('
        select
          *
        from
          Data_Pelanggan_Starclick a
        LEFT JOIN mdf b ON a.sto = b.mdf
        LEFT JOIN dispatch_teknisi c ON a.orderId = c.Ndem
        WHERE b.sektor_asr = "'.$sektor.'" AND DATE(a.orderDatePs)="'.$date.'" AND c.Ndem <> ""');
      foreach ($query as $num => $result){
        echo ++$num.". ";
        echo $result->orderId." | ".$result->id."<br />";
        $this->sendtobotkpro2($result->orderId,$name);
        sleep(3);
      }
    }

  public function sendkprobydate($sektor,$date){
    ini_set('max_execution_time', '30000000000000');
    $query = DB::SELECT('
      select
        *
      from
        Data_Pelanggan_Starclick a
      LEFT JOIN mdf b ON a.sto = b.mdf
      LEFT JOIN dispatch_teknisi c ON a.orderId = c.Ndem
      WHERE b.sektor_asr = "'.$sektor.'" AND DATE(a.orderDatePs)="'.$date.'" AND c.Ndem <> ""');
    foreach ($query as $num => $result){
      echo ++$num.". ";
      echo $result->orderId." | ".$result->id."<br />";
      $this->sendtobotkpro($result->orderId);
      sleep(20);
    }
  }

  public function refixkordinat($id,$sc){
    $query = DB::SELECT('SELECT * FROM psb_laporan a WHERE a.id_tbl_mj = '.$id);
    if (count($query)>0) {
      $result = $query[0];
      $kordinat_lama = $result->kordinat_pelanggan;
      $cekpos = strpos($kordinat_lama,'ttp://maps.google.com/?q=');
      $cleantext = substr_replace($kordinat_lama,"",0,$cekpos+25);
      $cleantext = str_replace(" ","",$cleantext);
      $splitting = explode(",",$cleantext);
      echo "test..";
      if (count($splitting)>1){
        $kordinat_baru = $splitting[0].",".$splitting[1];
        DB::table('psb_laporan')->where('id_tbl_mj',$id)->update(['kordinat_pelanggan' => $cleantext]);
        TG::sendLocation('Kpro_Kordinat',$splitting[0],$splitting[1]);
        TG::sendMsg('Kpro_Kordinat',"@kprofoto_bot ".$sc);
      } else {
        $kordinat_baru = "INVALID";
      }
      $get_new_kordinat = DB::table('psb_laporan')->where('id_tbl_mj',$id)->get();
      $kordinat_baru = $get_new_kordinat[0]->kordinat_pelanggan;
      if ($get_new_kordinat[0]->sendtokpro==1){
        echo "unable to send";
      } else {
        echo "sending...";
        $splitting = explode(',',$kordinat_baru);
        if (count($splitting)>0){
          TG::sendLocation('@kprofoto_bot',$splitting[0],$splitting[1]);
          TG::sendMsg('@kprofoto_bot',$sc);
        }
      }
      echo $kordinat_baru." |<br />";
    }
  }

  public function batchrefixkordinat($periode){
    $query = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem WHERE a.orderDatePs LIKE "'.$periode.'%" AND b.id IS NOT NULL');
    foreach ($query as $result){
      echo "| $result->orderId | ";
      $this->refixkordinat($result->id,$result->orderId);
    }
  }

  public function sendtobotkpro2($id,$name){
    $get_sc = DB::SELECT('
      SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.id = "'.$id.'"
    ');

    if (count($get_sc)==0){
      $get_sc = DB::SELECT('
        SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.Ndem = "'.$id.'"
      ');

      if($get_sc){
          $id = $get_sc[0]->id;
      };
    };

    $result = $get_sc[0];
      if (count($get_sc)>0){
        $table = DB::table('hdd')->get()->first();
        $upload = $table->public;
        $hdd = $table->active_hdd;
        echo "oke ".date('Y-m-d H:i:s');
        $path = '/mnt/'.$hdd.'/'.$upload.'/evidence/'.$id.'/'.$name.'.jpg';
        $path_kpro = '/mnt/'.$hdd.'/'.$upload.'/evidence/'.$id.'/'.$name.'-kpro.jpg';

        if (file_exists($path)) {
          if (file_exists($path_kpro)){
            $img = new \Imagick($path);

            $img->scaleImage(768, 1024, true);

            $watermark = new \Imagick('/mnt/hdd3/upload/watermark/watermark-1.png');
            $x = 0;
            $y = 0;
            $img->compositeImage($watermark,\Imagick::COMPOSITE_OVER, $x, $y);
            $img->writeImage("/mnt/".$hdd."/".$upload."/evidence/".$id."/".$name."-kpro.jpg");
          }
          $foto = 'https://psb.tomman.info/'.$upload.'/evidence/'.$id.'/'.$name.'-kpro.jpg';

          echo $foto;
          TG::sendMsg('@mnorrifani',$id);
          TG::sendPhoto('@kprofoto_bot', $foto,true,$result->orderId);
          #echo exec('/usr/share/tg/bin/telegram-cli -WR -e "send_photo @kprofoto_bot /mnt/hdd2/upload/evidence/'.$id.'/Lokasi.jpg ['.$result->orderId.'] "');
        }
      }
  }

  public function sendtobotkpro($id){
    $get_sc = DB::SELECT('
      SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.id = "'.$id.'"
    ');

    if (count($get_sc)==0){
      $get_sc = DB::SELECT('
        SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.Ndem = "'.$id.'"
      ');

      if($get_sc){
          $id = $get_sc[0]->id;
      };
    };

    $result = $get_sc[0];
    foreach($this->photokpro as $name) {
      if (count($get_sc)>0){
        echo "oke ".date('Y-m-d H:i:s');
        $table = DB::table('hdd')->get()->first();
        $hdd = $table->active_hdd;

        $path = '/mnt/'.$hdd.'/upload3/evidence/'.$id.'/'.$name.'.jpg';

        if (file_exists($path)) {
          $img = new \Imagick($path);

          $img->scaleImage(768, 1024, true);

          $watermark = new \Imagick('/mnt/hdd3/upload/watermark/watermark-1.png');
          $x = 0;
          $y = 0;
          $img->compositeImage($watermark,\Imagick::COMPOSITE_OVER, $x, $y);
          $img->writeImage("/mnt/".$hdd."/upload3/evidence/".$id."/".$name."-kpro.jpg");
          $foto = 'https://psb.tomman.info/upload3/evidence/'.$id.'/'.$name.'-kpro.jpg';

          echo $foto;
          TG::sendMsg('@mnorrifani',$id);
          TG::sendPhoto('@kprofoto_bot', $foto,true,$result->orderId);
          #echo exec('/usr/share/tg/bin/telegram-cli -WR -e "send_photo @kprofoto_bot /mnt/hdd2/upload/evidence/'.$id.'/Lokasi.jpg ['.$result->orderId.'] "');
        }
      }
    }
  }

  public function batchtokpro($date){
    $get_sc = DB::SELECT('
      SELECT *,dt.id as id_dt FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.id IS NOT NULL AND a.orderDatePs LIKE "'.$date.'%"
    ');
    foreach ($get_sc as $result){
      $id = $result->id_dt;
      $img = new \Imagick('/mnt/hdd3/upload/evidence/'.$id.'/Lokasi.jpg');
      $img->scaleImage(300, 450, true);
      $img->writeImage("/mnt/hdd3/upload/evidence/".$id."/Lokasi-kpro.jpg");
      TG::sendPhoto('@kprofoto_bot', 'https://psb.tomman.info/upload3/evidence/'.$id.'/Lokasi-kpro.jpg',true,$result->orderId);

    }
  }

  public function get_odp($id){
    $plg = DB::table("dispatch_teknisi")
      ->leftJoin("Data_Pelanggan_Starclick", "dispatch_teknisi.Ndem", "=", "Data_Pelanggan_Starclick.orderId")
      ->where("id", $id)->first();
    //dd($plg);

    $lon = $plg->lon;
    if ($plg->lon=='NaN' || $plg->lon==''){
      $lon = 0;
    };

    $lat = $plg->lat;
    if($plg->lat=='NaN' || $plg->lat==''){
      $lat = 0;
    };

    $list = DB::select("SELECT *,
        (6371 * acos(cos(radians(".$lat."))
        * cos(radians(latitude)) * cos(radians(longitude)
        - radians(".$lon.")) + sin(radians(".$lat."))
        * sin(radians(latitude))))
        AS jarak
        FROM odp_compliance
        HAVING jarak <= 3
        ORDER BY jarak
        LIMIT 3");
    //dd($list);
    return json_encode($list);
  }

  public function get_odp_point($lat,$lng){

    $list = DB::select("SELECT *,
        (6371 * acos(cos(radians(".$lat."))
        * cos(radians(latitude)) * cos(radians(longitude)
        - radians(".$lng.")) + sin(radians(".$lat."))
        * sin(radians(latitude))))
        AS jarak
        FROM odp_compliance
        HAVING jarak <= 0.2
        ORDER BY jarak
        LIMIT 3");
    //dd($list);
    return json_encode($list);
  }

  public function WorkOrder($id, Request $req){
    $bulan = date('Y-m');
    if ($req->has('bulan')){
        $bulan = $req->input('bulan');
    };

    $get_laporan_status = PsbModel::psb_laporan_status();
    $group_telegram = DB::table('group_telegram')->get();
    $list = PsbModel::WorkOrder($id,$bulan);

    // if ($id<>"UP") {
    //   $list = PsbModel::WorkOrder($id,$bulan);
    // } else {
    //   $list = array();
    // }

    $updatenote = PsbModel::updatenote();
    $checkONGOING = PsbModel::checkONGOING();
      $checkQCOndesk = QcondeskModel::checkQCOndesk();
    $list_ffg = PsbModel::WorkOrder('NEED_PROGRESS_FFG', $bulan);

    $status_absen = HomeModel::status_absen(session('auth')->id_user);
    $statusProv = HomeModel::cekStatusTekProv($status_absen[0]->nik);
    $status_absenEsok = HomeModel::status_absenEsok($status_absen[0]->nik);

    return view('psb.list', compact('updatenote','checkQCOndesk','list','group_telegram','get_laporan_status','checkONGOING','list_ffg','statusProv','status_absenEsok'));
  }

  public function HDFallout(){
    $start = date('Y-m', strtotime(date('Y-m')." -1 month"));
    $end = date('Y-m');

    $listFallout = DB::select("SELECT a.*,b.workerName,b.workerId,b.updated_at as takeTime FROM  Data_Pelanggan_Starclick a LEFT JOIN dispatch_hd b ON a.orderId = b.orderId WHERE (orderDate like  '".$start."%' OR orderDate like '".$end."%') AND  orderStatus LIKE  'Fallout (Activation)'");

    $listFallout2 = DB::select("SELECT a.*,b.workerName,b.workerId,b.updated_at as takeTime FROM  Data_Pelanggan_Starclick a LEFT JOIN dispatch_hd b ON a.orderId = b.orderId WHERE (orderDate like  '".$start."%' OR orderDate like '".$end."%') AND b.ket=1");

    $jumlahList1 = count($listFallout);
    return view('hd.listFallout', compact('listFallout', 'listFallout2', 'jumlahList1'));
  }

  public function orderHDFallout(Request $req){
    $auth = session('auth');
    if($req->status == "store"){
      $exists = DB::table('dispatch_hd')->where('orderId', $req->orderId)->first();
      if($exists){
        return redirect('/hdfallout')->with('alerts', [
          ['type' => 'warning', 'text' => '<strong>Gagal</strong> mengambil order, sudah ditikung orang!']
        ]);
      }else{
        DB::table('dispatch_hd')->insert([
          "orderId"=> $req->orderId,
          "workerId"=> $auth->id_karyawan,
          "workerName"=> $auth->nama,
          "updated_at"=>DB::raw('NOW()')
        ]);
        return redirect('/hdfallout')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengambil order']
        ]);
      }
    }else{
      DB::table('dispatch_hd')->where('orderId', $req->orderId)->delete();
      return redirect('/hdfallout')->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>SUKSES</strong> mendrop order']
      ]);
    }
  }
  public function index()
  {
    $bulan = date('Y-m');
    date_default_timezone_set('Asia/Makassar');
    $get_laporan_status = PsbModel::psb_laporan_status();
    $group_telegram = DB::table('group_telegram')->where('sektor','<>','')->get();
    $auth = session('auth');
    $list_ffg = PsbModel::WorkOrder('NEED_PROGRESS_FFG', $bulan);
    $list = PsbModel::WorkOrder('NEED_PROGRESS', $bulan);

    $data1 = new \stdClass();
    $statusProv = '';
    $status_absenEsok = '';
    if(session('auth')->level == 49 || session('auth')->level==50 || session('auth')->level==51 ) {
  	  return view('dshr.transaksi.list');
  	} else {
      $status_absen = HomeModel::status_absen(session('auth')->id_user);
      if (count($status_absen)>0){
          if (session('auth')->level==10 && $status_absen[0]->approval==1){
            $updatenote = PsbModel::updatenote();
            $checkONGOING = PsbModel::checkONGOING();
            $checkQCOndesk = QcondeskModel::checkQCOndesk();
            $statusProv = HomeModel::cekStatusTekProv($status_absen[0]->nik);
            $status_absenEsok = HomeModel::status_absenEsok($status_absen[0]->nik);

  		        return view('psb.list', compact('updatenote','checkQCOndesk','list','list_ffg','group_telegram','get_laporan_status','checkONGOING','statusProv','status_absenEsok'));
          } else {
            return redirect('/home');
          }
      }
      elseif (session('auth')->level == 55){
        return redirect('/psbview/'.date('Y-m-d'));
      }
      elseif(session('auth')->level==61 || session('auth')->level==59){
        $tglAwal = date('Y-m-01');
        $tglAkhir = date('Y-m-d');
        return redirect('/dshr/plasa-sales/list-wo-by-sales/ALL/'.$tglAwal.'/'.$tglAkhir);
      }
      else {
        return redirect('/home');
      }
  	}
  }
  public function sendtotelegram3($id){
    Tele::sendReportTelegram($id);
  }
  public function sendtotelegram($id,$chatID){
	  // data WO
  	$auth = session('auth');
    $order = DB::select('
      SELECT
      	*,
      	ms2n.*,
      	d.id as id_dt,
      	d.updated_at as tgl_dispatch,
      	r.uraian as nama_tim,
      	m.Kcontact as mKcontact,
      	m.ND as mND,
      	m.ND_Speedy as mND_Speedy,
      	m.Nama as mNama,
      	m.Alamat as mAlamat,
      	p2.mdf_grup_id as grup_id_psb,
      	m2.mdf_grup_id as grup_id_migrasi,
      	p2.mdf_grup as grup_psb,
      	m2.mdf_grup as grup_migrasi,
      	a2.mdf_grup as grup_starclick,
      	a2.mdf_grup_id as grup_id_starclick,
      	a.orderName,
      	a.ndemPots,
      	a.ndemSpeedy,
      	a.sto,
      	a.jenisPsb,
      	a.Kcontact as KcontactSC,
        yy.laporan_status,
        roc.no_telp,
        roc.no_tiket,
        roc.no_speedy,
        roc.headline,
        roc.umur,
          ra.no_tiket as status_tiket,
          dd.manja_status,
          ddd.dispatch_teknisi_timeslot,
          dm.*
      FROM dispatch_teknisi d
      LEFT JOIN manja_status dd ON d.manja_status = dd.manja_status_id
      LEFT JOIN dispatch_teknisi_timeslot ddd ON d.updated_at_timeslot = ddd.dispatch_teknisi_timeslot_id
      LEFT JOIN regu r ON (d.id_regu=r.id_regu)
      LEFT JOIN ms2n USING (Ndem)
      LEFT JOIN mdf p1 ON ms2n.mdf=p1.mdf
      LEFT JOIN mdf_grup p2 ON p1.mdf_grup_id = p2.mdf_grup_id
      LEFT JOIN micc_wo m on d.Ndem = m.ND
      LEFT JOIN mdf m1 ON m.MDF = m1.mdf
      LEFT JOIN mdf_grup m2 ON m1.mdf_grup_id = m2.mdf_grup_id
      LEFT JOIN Data_Pelanggan_Starclick a ON d.Ndem = a.orderId
      LEFT JOIN psb_laporan zz ON d.id = zz.id_tbl_mj
      LEFT JOIN psb_laporan_status yy ON zz.status_laporan = yy.laporan_status_id
      LEFT JOIN roc on d.Ndem = roc.no_tiket
      LEFT JOIN roc_active ra ON roc.no_tiket = ra.no_tiket
      LEFT JOIN mdf a1 ON a.sto = a1.mdf
      LEFT JOIN mdf_grup a2 ON a1.mdf_grup_id = a2.mdf_grup_id
      LEFT JOIN dossier_master dm ON d.Ndem = dm.ND
      WHERE d.id = ?
    ',[
      $id
    ])[0];
 	  // Informasi teknisi
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ])[0];
    //data material
    $get_materials = DB::select('
    	SELECT a.*
    	FROM
    		psb_laporan_material a
    	LEFT JOIN psb_laporan b ON a.psb_laporan_id = b.id
    	WHERE id_tbl_mj = ?
    ',[$id]);

    $materials = "";
    if (count($get_materials)>0) {
	    foreach ($get_materials as $material) :
				if ($material->id_item<>'')
					$materials .= $material->id_item." : ".$material->qty."\n";
	    endforeach;
    }  else {
	    $materials = "";
    }

    $nama_tim = explode(' ',str_replace(array('-',':'),' ',$order->nama_tim));
    $new_nama_tim = '';
    for ($i=0;$i<count($nama_tim);$i++){
	    $new_nama_tim .= $nama_tim[$i];
    }
    $messaggio = "Pengirixm : ".$auth->id_user."-".$auth->nama."\n";
    //$messaggio .= strtoupper($order->grup_psb ? : $order->grup_migrasi ? : $order->grup_starclick ? : 'UNKNOWN')."\n\n".$order->nama_tim."\n\n";
// 		$chatID = "-143383971";
    if ($order->no_tiket<>NULL) {
      $noorder = $data->no_tiket;
    } else if ($order->ND<>NULL) {
      $noorder = "MIGRASI AS IS ".$order->ND;
    } else {
      $noorder = "SC".$order->orderId;

    }
    $messaggio = "Reporting Order ".$noorder."\n\n";
	  $messaggio .= "Dispatch WO Pada : ".$order->tgl_dispatch;
	  $messaggio .= "\n";
    $get_group = DB::table('group_telegram')->where('chat_id',$chatID)->first();
    $messaggio .= "<i>".$get_group->title."</i>\n\n";
	  $messaggio .= "STATUS ORDER : ".$order->laporan_status;
    $messaggio .= "\n";
    if ($order->no_tiket==NULL) {
    } else {
      $messaggio .= "\nMTTR : ".$order->mttr."h";
      $messaggio .= "\nACTION : ".$order->action;
      $messaggio .= "\n";
      }
    if ($order->no_tiket<>NULL) {
      $id = $order->id_dt;
      $s = $order->no_tiket;
      $messaggio .= "WO Assurance \n";
      $messaggio .= "<b>".$order->no_tiket."</b> | ";

      if ($order->status_tiket<>NULL){
        $messaggio .= "<b>Open </b>";
      } else {
        $messaggio .= "<b>Close </b>";
      }
      if ($order->umur<>''){
        $umur = $order->umur;
      } else {
        $umur = '0';
      }
      $messaggio .= "<b>| ".$umur."h</b>\n";
      $messaggio .= $order->no_telp."\n";
      $messaggio .= $order->no_speedy."\n";
      $headline = explode('+',$order->headline);
      foreach ($headline as $val){
        $messaggio .= trim($val)."\n";
      }
      $messaggio .= "\n<b>Status Link </b> \nLink : ";
      $messaggio .= "<b>".@$headline[7]."</b>\n";
      $messaggio .= "INET : ";
      $messaggio .= "<b>".@$headline[9]."</b>\n";
      $messaggio .= @$headline[10]."\n";
      $messaggio .= "\n";
    } else if ($order->ND<>NULL) {
      $s = "MIG-ASIS ".$order->ND;
      $messaggio .= "\n<b>WO MIGRASI AS IS</b>\n<b>".$order->NAMA."</b>\n";
      $messaggio .= $order->ND."\n";
      $messaggio .= $order->ND_REFERENCE."\n";
      $messaggio .= $order->LVOIE."\n";
      $messaggio .= $order->LCOM."\n";
      $messaggio .= "\n";
      $messaggio .= "<b>Appointment</b>\n";
      $messaggio .= "Timeslot : ".$order->dispatch_teknisi_timeslot."\n";
      $messaggio .= "Manja : ".$order->manja ? : '--'."\n";
      $messaggio .= "\n";

    }else {
      $s = "SC".$order->orderId;
      $messaggio .= "\n<b>WO Provisioning</b>\n<b>".$order->orderName."</b>\n";
      $messaggio .= "SC".$order->orderId."\n";
      $messaggio .= $order->ndemPots."\n";
      $messaggio .= $order->ndemSpeedy."\n";
      $messaggio .= $order->kcontact."\n";
      $messaggio .= $order->jenisPsb."\n";
      $messaggio .= "\n";
      $messaggio .= "<b>Appointment</b>\n";
      $messaggio .= "Timeslot : ".$order->dispatch_teknisi_timeslot."\n";
      $messaggio .= "Manja : ".$order->manja ? : '--'."\n";
      $messaggio .= "\n";
    }

    if ($exists->status_laporan=="8"){
      if ($exists->catatan<>''){
  		  $messaggio .= "CATATAN : \n";
  		  $messaggio .= $exists->catatan."\n\n";
  	  }

  	  $s .= " #".strtolower($new_nama_tim);
    } else {

	  if ($exists->catatan<>''){
		  $messaggio .= "CATATAN : \n";
		  $messaggio .= $exists->catatan."\n\n";
	  }

	  $s .= " #".strtolower($new_nama_tim);

			$messaggio .= "\n";
    if ($exists->kordinat_pelanggan<>'')
		{
    	$messaggio .= "Kordinat Pelanggan : \n";
			$messaggio .= $exists->kordinat_pelanggan."\n";
    }
    else
    	$messaggio .= "Tidak ada info kordinat pelanggan\n";

		  $messaggio .= "\n";

    if ($exists->nama_odp<>'')
    {
	  	$messaggio .= "Nama ODP : \n";
			$messaggio .= $exists->nama_odp."\n";
    }
    else
    	$messaggio .= "Tidak ada info nama ODP pelanggan\n";

      $messaggio .= "\n";
	  if ($exists->kordinat_odp<>'')
	  {
	    $messaggio .= "Kordinat ODP : \n";
			$messaggio .= $exists->kordinat_odp."\n";
    }
    else
    	$messaggio .= "Tidak ada info kordinat ODP pelanggan\n";



    $messaggio .= "\n";
    $messaggio .= "#report".strtolower($new_nama_tim)."\n";

    $messaggio .= "\n";


    $messaggio .= $materials;

  }
//
    if ($exists->status_laporan=="8"){
      $chatID = "-140569787";
    }
    Telegram::sendMessage([
      'chat_id' => $chatID,
      'parse_mode' => 'html',
      'text' => $messaggio
    ]);

    if ($order->no_tiket<>NULL){
      $photoInputs = $this->assuranceInputs;
      $folder = "asurance";
    } else {
      $photoInputs = $this->photoInputs;
      $folder = "evidence";
    }
		for($i=0;$i<count($photoInputs);$i++) {
    	$file = public_path()."/upload3/".$folder."/".$id."/".$photoInputs[$i].".jpg";
      // Telegram::sendMessage([
      //   'chat_id' => $chatID,
      //   'caption' => $photoInputs[$i]." ".$s,
      //   'text' => $file
      // ]);
    	if (file_exists($file)){
		    Telegram::sendPhoto([
		      'chat_id' => $chatID,
		      'caption' => $photoInputs[$i]." ".$s,
		      'photo' => $file
		    ]);

// 			echo $file;
		  }
    }

      return redirect('/')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengirim laporan']
      ]);

  }

  function status_wo($id){
	  switch ($id){
		  case 1 :
		  	$result = "SELESAI";
		  	break;
		  case 2 :
		  	$result = "KENDALA TEKNIS";
		  	break;
		  case 3 :
		  	$result = "KENDALA PELANGGAN";
		  	break;
		  case 4 :
		  	$result = "HR";
		  	break;
		  case 5 :
		  	$result = "OGP";
		  	break;
		  	break;
		  case 6 :
		  	$result = "BELUM SURVEY";
		  	break;
		  	break;
		  case 7 :
		  	$result = "RESCHEDLUE";
		  	break;
		  	break;
		  case 8 :
		  	$result = "SURVEY OK";
		  	break;
        case 9 :
  		  	$result = "FOLLOW UP SALES";
  		  	break;


		  default :
		  	$result = "BELUM SURVEY";
				break;
	  }
	  return $result;
  }

  public function stokTeknisi()
  {
    $auth = session('auth');

    $list = DB::select('
      SELECT m.*,i.nama_item,i.unit_item
      FROM stok_material_teknisi m
      LEFT JOIN item i
      ON m.id_item=i.id_item
      WHERE m.id_karyawan = ?
    ',[
      $auth->id_karyawan
    ]);
    $ntes = DB::select('
      SELECT *
      FROM nte
      WHERE position_key = ?
    ',[
      $auth->id_karyawan
    ]);
    return view('psb.stok', compact('list', 'ntes'));
  }

  public function input($id)
  {
    $get_status_qc = "";
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ]);


    $odpAlternatif = '';

    // cek data distarclick
    $ada = DB::table('Data_Pelanggan_Starclick')
                  ->leftJoin('dispatch_teknisi','Data_Pelanggan_Starclick.orderId','=','dispatch_teknisi.Ndem')
                  ->where('dispatch_teknisi.id',$id)
                  ->first();
    if($ada){
	    //echo substr($ada->jenisPsb, 0, 2);
      $cekDispatch = DB::table('dispatch_teknisi')
                        ->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.sc')
                        ->where('dispatch_teknisi.id',$id)->first();
      $myir = $cekDispatch->myir;
      if($cekDispatch->dispatch_by==NULL){
          $odpAlternatif = json_decode($this->get_odp($id));
      };
    } else {
      $cekDispatch = DB::table('dispatch_teknisi')
                        ->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.myir')
                        ->where('dispatch_teknisi.id',$id)->first();
      if ($cekDispatch){
        $myir = $cekDispatch->myir;
      } else {
        $myir = "";
      }
    }

    // if($odpAlternatif==''){
    //   if($exists<>NULL){
    //     if($exists[0]->kordinat_pelanggan<>''){
    //         $koor = explode(',', $exists[0]->kordinat_pelanggan);
    //         $odpAlternatif = json_decode($this->get_odp_point($koor[0],$koor[1]));
    //     }
    //   }
    // }

    // periode rfc
    // $periode = date('m/Y');

    // cek foto onecall
    if ($myir<>""){
      if (session('witel')=="KALSEL"){
          $onecall = @file_get_contents("http://36.92.189.82/onecall/api/sales-order/".$myir);
      }
      else {
          $onecall = @file_get_contents("http://10.128.16.66/onecall/api/sales-order/".$myir);
      };
    } else {
      $onecall = "";
    }
    $onecall = json_decode($onecall);

    $tglWo = DB::table('dispatch_teknisi')->where('id',$id)->first();
    $periode = date('m/Y',strtotime($tglWo->tgl));

    $bulanEnd = date('m',strtotime($tglWo->tgl));
    if ($bulanEnd==1){
        $endBulan = '12';
        $endTahun = date('Y',strtotime($tglWo->tgl));

        $peiodeEnd = $endBulan.'/'.$endTahun-1;
    }
    else{
        $endBulan = $bulanEnd-1;
        $endTahun = date('Y',strtotime($tglWo->tgl));

        $peiodeEnd = $endBulan.'/'.$endTahun;
    };

    // dd($tglWo);
    // if ($tglWo->dispatch_by=='5'){
    //     $dt = DB::table('psb_myir_wo')->where('myir',$tglWo->Ndem)->first();
    //     $periode = date('m/Y',strtotime($dt->orderDate));
    // }
    // else if ($tglWo->dispatch_by==NULL){
    //     $dt = DB::table('Data_Pelanggan_Starclick')->where('orderId',$tglWo->Ndem)->first();
    //     if (count($dt)<>0){
    //       if (substr($dt->orderDatePs, 0, 4)=="0000"){
    //           $periode = date('m/Y',strtotime($dt->orderDate));
    //       }
    //       else{
    //           $periode = date('m/Y',strtotime($dt->orderDatePs));
    //       }
    //     }
    //     else{
    //         $periode = date('m/Y',strtotime($tglWo->tgl));
    //     }
    // };
    if (count($ada)){

      $get_status_qc = file_get_contents("http://10.128.18.14/proactive-caring/api/quality_control.php?sc=".$ada->Ndem);
      $get_status_qc  = json_decode($get_status_qc);

      }
    if (count($exists)) {
      $data = $exists[0];
      // echo $data->id;
      // TODO: order by most used
      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item,
          COALESCE(m.qty, 0) AS qty,
          0 as rfc,
          0 as saldo
        FROM item i
        LEFT JOIN
          psb_laporan_material m
          ON i.id_item = m.id_item
          AND m.psb_laporan_id = ?
        WHERE
        	i.grup = 1
        ORDER BY id_item
      ', [
        $data->id
      ]);





      // tes ja
      // dd($data->id);
      // $periode = date('m/Y');
      // if ($auth->level==2 || $auth->level==15 || $auth->level==46){
      //   //   $materials = DB::select('
      //   //   SELECT
      //   //     i.id_item, i.nama_item,
      //   //     COALESCE(i.jmlTerpakai, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
      //   //   FROM rfc_sal i
      //   //   where
      //   //     i.psb_laporan_id = ?
      //   //   ORDER BY i.rfc
      //   // ', [
      //   //   $data->id
      //   // ]);
      //
      //   $materials = DB::select('
      //     SELECT
      //       i.id_item, i.nama_item,
      //       COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo, i.jmlTerpakai
      //     FROM rfc_sal i
      //     LEFT JOIN
      //       psb_laporan_material m
      //       ON i.id_item_bantu = m.id_item_bantu
      //     Where
      //       (i.rfc LIKE "%'.$periode.'" OR i.rfc LIKE "%'.$peiodeEnd.'" OR i.rfc LIKE "%12/2019%") AND
      //       m.psb_laporan_id = ?
      //     ORDER BY i.rfc asc, i.id_item asc
      //   ', [
      //     $data->id
      //   ]);
      //
      //   if (empty($materials)){
      //         $materials = DB::select('
      //           SELECT * FROM psb_laporan_material where type = 1 AND psb_laporan_id = ?
      //         ', [
      //           $data->id
      //         ]);
      //   }
      // }
      // else{
      //   $materials = DB::select('
      //     SELECT
      //       i.id_item, i.nama_item,
      //       COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo, i.jmlTerpakai
      //     FROM rfc_sal i
      //     LEFT JOIN
      //       psb_laporan_material m
      //       ON i.id_item_bantu = m.id_item_bantu
      //       AND m.psb_laporan_id = ?
      //     WHERE
      //      (i.rfc LIKE "%'.$periode.'" OR i.rfc LIKE "%'.$peiodeEnd.'" OR i.rfc LIKE "%12/2019%") AND
      //      i.nik="'.$auth->id_user.'"
      //     ORDER BY i.rfc asc, i.id_item asc
      //   ', [
      //     $data->id
      //   ]);
      // }
    }
    else {
      // echo "0";
      $data = new \StdClass;
      $data->id = null;
      $data->typeont = null;
      $data->snont = null;
      $data->typestb = null;
      $data->snstb = null;
      $data->jenis_odp = null;
      $data->redaman_in = null;
      // TODO: order by most used

      $materials = DB::select('
        SELECT a.id_item, a.nama_item, 0 AS qty,  0 as rfc,
          0 as saldo

        FROM item a left join psb_laporan_material b ON a.id_item=b.id_item WHERE a.grup = 1
        GROUP BY a.id_item
        ORDER BY a.id_item
      ');

      // $materials = DB::select('
      //   SELECT id_item, nama_item, 0 AS qty, (jumlah - jmlTerpakai) as saldo, rfc, jmlTerpakai
      //   FROM rfc_sal i where (i.rfc LIKE "%'.$periode.'" OR i.rfc LIKE "%'.$peiodeEnd.'") AND nik="'.$auth->id_user.'" AND jumlah<>0
      //   ORDER BY rfc
      // ');


      // $materials = DB::select('
      //   SELECT
      //     i.id_item, i.nama_item,
      //     COALESCE(m.qty, 0) AS qty
      //   FROM item i
      //   LEFT JOIN
      //     psb_laporan_material m
      //     ON i.id_item = m.id_item
      //     AND m.psb_laporan_id = ?
      //   WHERE 1
      //   ORDER BY id_item
      // ', [
      //   $data->id
      // ]);

    }
    $nte1 = DB::select('select i.id as id, i.sn as text
        FROM nte i
        LEFT JOIN
          psb_laporan_material m
          ON i.id = m.id_item
          AND m.psb_laporan_id = ?
        WHERE m.type=2
        ORDER BY id_item',[
        $data->id
      ]);
    $no = 1;
    $ntes = '';
    foreach($nte1 as $n){
      if($no == 1){
        $ntes .= $n->id;
        $no = 2;
      }else{
        $ntes .= ', '.$n->id;
      }
    }
    $nte = DB::select('select  sn as id, sn as text from nte
      where position_key = ?',[
        $auth->id_karyawan
      ]);
    $project = DB::select('
      SELECT
      	*,
      	ms2n.*,
      	d.id as id_dt,
      	m.ND as mND,
      	m.ND_Speedy as mND_Speedy,
      	m.mdf as mMDF,
      	m.Kcontact as mKcontact,
      	m.Nama as mNama,
      	m.Alamat as mAlamat,
				c.ndemPots,
				c.ndemSpeedy,
				c.Kcontact as KcontactSC,
				c.jenisPsb,
				c.sto,
        e.id as id_dshr,
        dm.*,
        ne.STO as neSTO,
        ne.RK as neRK,
        ne.ND_TELP as neND_TELP,
        ne.ND_INT as neND_INT,
        ne.PRODUK_GGN as nePRODUK_GGN,
        ne.HEADLINE as neHEADLINE,
        ne.LOKER_DISPATCH as neLOKER_DISPATCH,
        ne.KANDATEL as neKANDATEL,
        c.lon,c.lat,
        d.jenis_layanan,
        my.*,
        pl.*,
        d.Ndem as NdemId,
        my.sc,
        my.myir,
        my.no_internet,
        my.no_telp,
        my.customer
      FROM dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick c ON d.Ndem = c.orderId
      LEFT JOIN ms2n USING (Ndem)
      LEFT JOIN reboundary_survey rs ON d.Ndem = rs.no_tiket_reboundary
      LEFT JOIN nonatero_excel_bank ne ON d.Ndem = ne.TROUBLE_NO
      LEFT JOIN dossier_master dm ON d.Ndem = dm.ND
      LEFT JOIN micc_wo m on d.Ndem = m.ND
      LEFT JOIN dshr e ON c.orderKontak = e.pic
      LEFT JOIN step_tracker st ON st.step_tracker_id = d.step_id
      LEFT JOIN psb_laporan  pl ON d.id=pl.id_tbl_mj
      LEFT JOIN psb_myir_wo my ON (d.Ndem=my.myir OR d.Ndem=my.sc)
      WHERE
      d.id = ?
    ',[
      $id
    ])[0];

    // error step_id
    if (empty($project->step_next)){

        // cek psb_laporan_log
        $dispatch = DB::table('dispatch_teknisi')
                    ->where('id','=',$id)
                    ->first();

        $psbLog = DB::table('psb_laporan_log')
                    ->where('ndem','=',$dispatch->Ndem)
                    ->orderBy('psb_laporan_log_id','desc')
                    ->first();

        if (empty($psbLog->Ndem)){
            $stepId = '1.0';
        }
        else {
            $psbStatus = DB::table('psb_laporan_status')
                            ->where('laporan_status_id','=',$psbLog->status_laporan)
                            ->first();

            $stepId = $psbStatus->step_id;
        };

        DB::table('dispatch_teknisi')
            ->where('id',$id)
            ->update([
                'step_id'   => $stepId
            ]);

        // code untuk mengisi label
        $dataStep = DB::table('step_tracker')
                      ->where('step_tracker_id','=',$stepId)
                      ->first();

        $step_id   = $dataStep->step_tracker_id;
        $step_next = $dataStep->step_next;
        $step_name = $dataStep->step_name;
    }
    else {
        $step_id   = $project->step_id;
        $step_next = $project->step_next;
        $step_name = $project->step_name;
    }

    if ($step_next=='1.5'){
        $step_next = '1.4';
    }
    else{
      $step_next = $step_next;
    };
    // aslinya
    // $project->step_next
    $get_Ndem = DB::table('dispatch_teknisi')
                  ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                  ->select('psb_laporan.id','psb_laporan.id_tbl_mj','dispatch_teknisi.Ndem', 'dispatch_teknisi.dispatch_by')
                  ->where('psb_laporan.id_tbl_mj',$id)
                  ->first();

    if (count($get_Ndem)==0){
        $get_Ndem = DB::table('dispatch_teknisi')->where('id',$id)->first();
        $cari = 'a.Ndem = "'.$get_Ndem->Ndem.'" AND';
    }
    else{
        $ndemIn  = DB::table('psb_laporan_log')->where('psb_laporan_id',$get_Ndem->id)->groupBy('Ndem')->get();
        $ndemNew = '';
        if (count($ndemIn)<>0){
            if (count($ndemIn)==1){
                $ndemNew  = "'".$ndemIn[0]->Ndem."'";
                $ndemMyir = DB::table('psb_myir_wo')->where('sc',$ndemIn[0]->Ndem)->first();
                if (count($ndemMyir)<>0){
                    $ndemNew  .= ",'".$ndemMyir->myir."'";
                };
            }
            else{
              foreach($ndemIn as $ndem){
                  $ndemNew .= "'".$ndem->Ndem."'".',';
              }

              $ndemNew = substr($ndemNew, 0, -1);
            }

            $ndemNew = '('.$ndemNew.')';
            $cari = '(a.psb_laporan_id = "'.$get_Ndem->id.'" OR a.Ndem in '.$ndemNew.') AND';
        }
        else{
            $ndemMyir = DB::table('psb_myir_wo')->where('sc',$get_Ndem->Ndem)->first();
            // $cari = 'a.psb_laporan_id = "'.$get_Ndem->id.'" AND';
            if (count($ndemMyir)==0){
                $cari = 'a.Ndem = "'.$get_Ndem->Ndem.'" AND';
            }
            else{
                $cari = '(a.psb_laporan_id = "'.$get_Ndem->id.'" OR a.Ndem ="'.$ndemMyir->myir.'") AND';
            }

        }
        // dd($cari);
    };

    $data_log = DB::SELECT('
                SELECT
                *
                FROM psb_laporan_log a
                LEFT JOIN user b ON a.created_by = b.id_user
                LEFT JOIN 1_2_employee c ON b.id_karyawan = c.nik
                LEFT JOIN psb_laporan_status d ON a.status_laporan = d.laporan_status_id
                WHERE
                  '.$cari.'
                  a.created_by <> 0
                ORDER BY created_at DESC
              ');

    $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');

    if ($auth->level==2 OR $auth->id_user=="94132168"){
        $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IS NOT NULL AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
    }
    else{
      if ($auth->level==15){
        $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV","PROVVA","PROVA", "PROVB", "PROVC") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
      }

      if ($project->jenis_layanan=="ADDONSTB" || $project->jenis_layanan=="CHANGESTB" || $project->jenis_layanan=="2ndSTB"){
          if ($project->id_regu=="789" || $project->id_regu=="790" || $project->id_regu=="920" || $project->id_regu=="930" || $project->id_regu=="931" || $project->id_regu=="937" || $project->id_regu=="938" || $project->id_regu=="939"){
              // dd('ok');
               $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE manja="1" order by laporan_status asc');
          }
      }

      // daman
      if ($project->status_laporan=="24"){
          $nikData = PsbModel::getAkses($auth->id_user, 'daman');
          if (count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV", "PROVA") OR laporan_status_id=61 order by laporan_status asc');
          };

          $nikData = PsbModel::getAkses($auth->id_user, 'lanjutpt1');
          if (count($nikData)<>0){
             $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV") OR laporan_status_id=61 order by laporan_status asc');
          }
      }

      // aso
      if ($project->status_laporan=="11"){
          $nikData = PsbModel::getAkses($auth->id_user, 'aso');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV", "PROVB") order by laporan_status asc');
          }
      }

      // amo
      if ($project->status_laporan=="2"){
          $nikData = PsbModel::getAkses($auth->id_user, 'amo');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV", "PROVC") order by laporan_status asc');
          }
      }

      // kendala
      if ($project->status_laporan=="25" || $project->status_laporan=="2" || $project->status_laporan=="34" || $project->status_laporan=="42" || $project->status_laporan=="60"){
          $nikData = PsbModel::getAkses($auth->id_user, 'kendala');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE (jenis_order in ("ALL PROVV") OR laporan_status_id in (57,2)) order by laporan_status asc');
          }
      }

      // rna
      if ($project->status_laporan=="49"){
          $nikData = PsbModel::getAkses($auth->id_user, 'rna');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV") OR laporan_status_id=16 order by laporan_status asc');
          }
      }

      // odp full nok
      if ($project->status_laporan=="61"){
          $nikData = PsbModel::getAkses($auth->id_user, 'odpNok');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV") OR laporan_status_id=57 order by laporan_status asc');
          };

          $nikData = PsbModel::getAkses($auth->id_user, 'lanjutpt1');
          if (count($nikData)<>0){
             $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV") order by laporan_status asc');
          }

      }

      // unsc
      if ($project->status_laporan=="57"){
          $nikData = PsbModel::getAkses($auth->id_user, 'unsc');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV") OR laporan_status_id=56 OR laporan_status_id=16 OR laporan_status_id=49 order by laporan_status asc');
          }
      }


    }


    $photoInputs = $this->photoInputs;
    if ($get_Ndem->dispatch_by==6){
        $photoInputs = $this->photoReboundary;
    };

    $dispatch_teknisi_id = $id;
    // $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
    $getTambahTiang     = psbmodel::getTambahTiang();
    $getNdem = DB::table('dispatch_teknisi')->where('id',$id)->first();
    $getStepId = $getNdem->step_id;
    $Ndem      = $getNdem->Ndem;
    $redamanIboster = NULL;
    if ($exists){
        $redamanIboster = $exists[0]->redaman_iboster;
    };

    // cek kalo ada di table psb_myir_wo
    $ketInput = '';
    $idMyir   = '';
    $getData  = DB::table('psb_myir_wo')->where('sc',$project->NdemId)->orWhere('myir',$project->NdemId)->first();
    if (count($getData)){
        $ketInput = $getData->ket_input;
        $idMyir   = $getData->id;
    }

    $photoInfo = '';
    if ($ketInput==1){
        $photoInfo = ['ktp','odp','rumah'];
    }
    else if ($ketInput==0){
        $photoInfo = ['ktp','ttd','ktp_pelanggan','capture_odp'];
    }

    if (session('auth')->level==10) {
    $layout = 'tech_layout';
    } else {
    $layout = 'layout';
    }

    $table = DB::table('hdd')->get()->first();

    return view('psb.input', compact('table','onecall','get_status_qc','layout','data', 'project', 'materials', 'photoInputs', 'ntes', 'nte', 'nte1', 'dispatch_teknisi_id','get_laporan_status','step_id', 'step_name', 'getTambahTiang', 'data_log', 'odpAlternatif', 'getStepId', 'Ndem','redamanIboster', 'ketInput', 'photoInfo', 'idMyir'));
  }

  public function save(Request $request, $id)
  {
    date_default_timezone_set('Asia/Makassar');
    if ($id=='419513' || $id=='457657' || $id=='404667' || $id =='264721'){
        $this->handleFileUpload2($request, $id);
    }
    else{
        $upload = $this->handleFileUpload($request, $id);
        if (@$upload['type']=="danger")
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => $upload['message']]
        ]);
    };

    $disp = DB::table('dispatch_teknisi')
      ->select('dispatch_teknisi.*', 'Data_Pelanggan_Starclick.orderId', 'Data_Pelanggan_Starclick.jenisPsb','Data_Pelanggan_Starclick.internet','step_tracker.*','psb_laporan.redaman_iboster')
      ->leftJoin('Data_Pelanggan_Starclick', 'dispatch_teknisi.Ndem', '=', 'Data_Pelanggan_Starclick.orderId')
      ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
      ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
      ->leftJoin('step_tracker', 'psb_laporan_status.step_id', '=', 'step_tracker.step_tracker_id')
      ->where('dispatch_teknisi.id', $id)
      ->first();

    $jenisPsb = '';
    if(@$disp->jenisPsb){
      $jenisPsb = substr($disp->jenisPsb, 0, 2);
    }

    $jeniswo = $disp->dispatch_by;

    // $this->handleFileUpload($request, $id);
    $auth = session('auth');
    $input = $request->only([
      'status'
    ]);
    $rules = array(
      'status' => 'required'
    );
    $messages = [
      'dropcore_label_code.required'  => 'Silahkan isi kolom "Dropcore Label Code" Bang!',
      'odp_label_code.required'  => 'Silahkan isi kolom "ODP Label Code" Bang!',
      'status.required'  => 'Silahkan isi kolom "Status" Bang!',
      'sn_ont.required'  => 'Silahkan isi kolom "SN ONT" Bang!',
      'catatan.required' => 'Silahkan isi kolom "Catatan" Bang!',
      'kordinat_pelanggan.required' => 'Silahkan isi kolom "kordinat_pelanggan" Bang!',
      'nama_odp.required' => 'Silahkan isi kolom "nama_odp" Bang!',
      'kordinat_odp.required' => 'Silahkan isi kolom "kordinat_odp" Bang!',
      'flag_Berita_Acara.required' => 'Silahkan isi kolom upload "Berita Acara" Bang!',
      'flag_Redaman_Pelanggan.required' => 'Silahkan upload "Redaman Pelanggan" Bang!',
      'flag_Redaman_ODP.required' => 'Silahkan upload "Hasil Ukur Redaman ODP" Bang!',
      'flag_Lokasi.required'      => 'Silahkan upload "Foto Lokasi" Bang!',
      'flag_ODP.required'         => 'Silahkan upload "Foto ODP" Bang!',
      'tbTiang.required'          => 'Silahakan isi kolom "Tambah Tiang" Bang!',
      'flag_Foto_Action.required' => 'Silahkan upload "Foto Action" Bang !',
      'flag_Foto_Pelanggan_dan_Teknisi.required' => 'Silahkan upload "Foto Pelanggan dan Teknisi" Bang !',
      'flag_Excheck_Helpdesk.required'=> 'Silahkan upload "Excheck Helpdesk" Bang !',
      'catatanRevoke.required'        => 'Silahkan isi kolom "Catatan Revoke" Bang !',
      'flag_KLEM_S.required'          => 'Silahkan upload "Foto KLEM S" Bang !',
      'flag_Patchcore.required'       => 'Silahkan Upload "Foto Patchcore" Bang !',
      'flag_Stopper.required'         => 'Silahkan Upload "Foto Stopper" Bang !',
      'flag_Breket.required'          => 'Silahkan Upload "Foto Breket" Bang !',
      'flag_LABEL.required'           => 'Silahkan Upload "Foto LABEL" Bang !',
      'flag_Roset.required'           => 'Silahkan Upload "Foro Roset" Bang !',
      'noPelangganAktif.required'     => 'Silahkan Isi "No. Hp Pelanggan Aktif !',
      'noPelangganAktif.numeric'      => 'Inputan Hanya Boleh Angka',
      'flag_Pullstrap.required'       => 'Silahkan Upload "Foto Pullstrap" Bang !',
      'flag_Speedtest.required'       => 'Silahkan Upload "Foto Speedtest" Bang !',
      'flag_SN_ONT.required'          => 'Silahkan Upload "Foto SN ONT" Bang !',
      'flag_SN_STB.required'          => 'Silahkan Upload "Foro SN STB" Bang !',
      'flag_Telephone_Incoming.required' => 'Silahkan Upload "Foto Telephone Incoming" Bang !',
      'flag_K3.required'              => 'Silahkan Upload "Foto K3" Bang !',
      'flag_Live_TV.required'         => 'Silahkan Upload "Foto Live Tv" Bang !',
      'flag_BA_Digital.required'      => 'Silahkan Upload "Foto BA Digital" Bang !',
      'type_ont.required'             => 'Silahkan isi "Type ONT" Bang !',
      'type_stb.required'             => 'Silahkan isi "Type STB" Bang !',
      'sn_stb.required'               => 'Silahkan isi "SN STB" Bang !',
      'flag_Tray_Cable.required'      => 'Silhkan Upload "Foto Tray" Bang !',
      'redaman.required'              => 'Redaman Jangan Dikosongi !',
      'redamanOdp.required'           => 'Redaman ODP Jangan Dikosongi !',
      'jenisOdp.required'             => 'Jenis ODP Diisi',
      'redamanInOdp.required'         => 'Redaman IN ODP Diisi',
      'dropcore_label_code.required'  => 'Dorcore Label Code Diisi',
      'port_number.required'          => 'Port Number Diisi Bang'
    ];


        $validator = Validator::make($request->all(), $rules, $messages);
        $a = $request->input('status');

    if ($id<>'419513'){
        //yg harus bekoordinat-pt1/pt2-selesai-kendala sistem-odp loss-insert tiang-hr
        if($a == 1 || $a == 2 || $a == 4 || $a == 10 || $a == 11 || $a == 25 || $a == 24 || $a == 34 || $a == 42 || $a ==66) {
          $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
              return true;
          });
        };
        if($jeniswo == null && $jenisPsb != 'MO' && $a == 1){
          $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_Redaman_ODP', 'required', function ($input) {
              return true;
          });
        };

        if (($a == 2 || $a == 11 || $a == 25) && $jenisPsb!='MO'){
            $validator->sometimes('flag_Redaman_ODP', 'required', function ($input) {
              return true;
            });

            $validator->sometimes('flag_ODP', 'required', function ($input) {
                return true;
            });
        }

        if ($a == 2 || $a == 11 || $a == 25 || $a == 24 || $a ==66){
            $validator->sometimes('flag_Lokasi', 'required', function ($input) {
                return true;
            });
            };

        if ($a == 10 || $a == 34 || $a == 42){
            $validator->sometimes('flag_Lokasi', 'required', function ($input) {
                return true;
            });
        };

        if ($a == 34 || $a == 42){
            $validator->sometimes('flag_Foto_Action', 'required', function ($input) {
                return true;
            });
        };

        if ($a == 11){
            $validator->sometimes('tbTiang', 'required', function ($input) {
                return true;
            });
        };

        if ($a == 24 || $a == 25 || $a == 2 || $a == 34 || $a == 11 || $a == 42 || $a ==66){
            $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
                return true;
            });
        };


        if ($a == 24 || $a == 25 || $a == 2 || $a == 34 || $a ==66){
            $validator->sometimes('flag_Excheck_Helpdesk', 'required', function ($input) {
                return true;
            });
        };

        //memastikan itu wo PSB
        if($jeniswo == null){
          //filter non MODIFID
          if($jenisPsb != 'MO'){
            if($a==1 || $a == 2 || $a == 4 || $a == 25 || $a == 11 || $a == 24 || $a ==66){
              $validator->sometimes('nama_odp', 'required', function ($input) {
                return true;
              });
              $validator->sometimes('kordinat_odp', 'required', function ($input) {
                  return true;
              });
            }
         }
         }


        if(($a == 1 || $a == 2)&& $jenisPsb != "MO"){
          $validator->sometimes('flag_Redaman_Pelanggan', 'required', function ($input) {
            return true;
          });
        };

        if($a <> 6 && $a <> 28 && $a <> 29 && $a <> 5 && $a <> 52 && $a <> 57 && $a <> 58 && $a <> 59 && $a <> 60 ){
          $validator->sometimes('catatan', 'required', function ($input) {
            return true;
          });

          $validator->sometimes('noPelangganAktif', 'required', function ($input) {
            return true;
          });

          $validator->sometimes('noPelangganAktif', 'numeric', function ($input) {
            return true;
          });
        }


    if ($a == 1 && $jenisPsb=="AO") {
      $cek_qrcode = GrabModel::barcodePsb($request->input('dropcore_label_code'));
      if ($cek_qrcode=="FALSE"){
        echo $cek_qrcode;
         return redirect()->back()
                ->withInput($request->input())
                        ->withErrors($validator)
                        ->with('alerts', [
                          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan QRCODE tidak terdeteksi di DAVAMAS']
                        ]);
      } elseif ($cek_qrcode<>"TRUE" && ($cek_qrcode<>$disp->orderId)){
        return redirect()->back()
                ->withInput($request->input())
                        ->withErrors($validator)
                        ->with('alerts', [
                          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan QRCODE sudah digunakan untuk '.$cek_qrcode]
                        ]);
      }

    }

    if($jenisPsb=="MO"){
      if($a == 1){
/*
        $validator->sometimes('flag_Telephone_Incoming', 'required', function ($input) {
          return true;
        });
*/

/*
        $validator->sometimes('flag_LABEL', 'required', function ($input) {
          return true;
        });
*/

/*
        $validator->sometimes('flag_KLEM_S', 'required', function ($input) {
          return true;
        });

        $validator->sometimes('flag_K3', 'required', function ($input) {
          return true;
        });

        $validator->sometimes('flag_Redaman_Pelanggan', 'required', function ($input) {
          return true;
        });
*/

        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });

        $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
          return true;
        });

/*
        $validator->sometimes('flag_ODP', 'required', function ($input) {
          return true;
        });
*/

/*
        $validator->sometimes('flag_Redaman_ODP', 'required', function ($input) {
          return true;
        });
*/

        $validator->sometimes('flag_SN_ONT', 'required', function ($input) {
          return true;
        });

        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
          return true;
        });

        $validator->sometimes('flag_BA_Digital', 'required', function ($input) {
          return true;
        });

        $validator->sometimes('flag_Live_TV', 'required', function ($input) {
          return true;
        });

/*
        $validator->sometimes('nama_odp', 'required', function ($input) {
              return true;
        });
*/

/*
        $validator->sometimes('flag_Tray_Cable', 'required', function ($input) {
              return true;
        });
*/

        $validator->sometimes('sn_ont', 'required', function ($input) {
              return true;
        });

      }
    }

    if ($a == 43 || $a == 44){
      $validator->sometimes('catatanRevoke', 'required', function ($input) {
        return true;
      });
    };

    if ($jenisPsb=="AO"){
        if ($a==1 || $a==38 || $a==37){
            $validator->sometimes('flag_KLEM_S', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('flag_Patchcore', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('flag_Stopper', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('flag_Breket', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('flag_LABEL', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('flag_Roset', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('flag_Pullstrap', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('sn_ont', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('type_ont', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('nama_odp', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('flag_Tray_Cable', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('dropcore_label_code', 'required', function ($input) {
                return true;
            });
            $validator->sometimes('odp_label_code', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('port_number', 'required', function ($input) {
                return true;
            });
        };

    }
    if ($a==24 || $a ==66){
        $validator->sometimes('redaman', 'required', function ($input) {
          return true;
        });

        $validator->sometimes('redamanOdp', 'required', function ($input) {
          return true;
        });
    };

    // wo migrasi
/*
    $cekNdemMigrasi = substr($disp->Ndem, 0, 1);
    if ($cekNdemMigrasi=='5'){
        if  ($a <> 49 && $a <> 54 && $a <> 48 && $a <> 55 && $a <> 45 && $a <> 28 && $a <> 29 && $a<>5) {
            $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
                return true;
            });

            $validator->sometimes('kordinat_odp', 'required', function ($input) {
                return true;
            });
        };

        if ($a==2 || $a==1 || $a==24 || $a ==66){
            $validator->sometimes('flag_Redaman_ODP', 'required', function ($input) {
                return true;
            });
        }

        if ($a==1){
            $validator->sometimes('flag_Tray_Cable', 'required', function ($input) {
                return true;
            });
        }
    }
*/

    // status odp full cek fiel required
    if($a==24){
        $validator->sometimes('jenisOdp', 'required', function ($input) {
          return true;
        });

        $validator->sometimes('redamanInOdp', 'required', function ($input) {
          return true;
        });
    }

    if ($validator->fails()) {
            return redirect()->back()
                ->withInput($request->input())
                        ->withErrors($validator)
                        ->with('alerts', [
                          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan']
                        ]);
    };

    };
    if ($request->kordinat_pelanggan=="ERROR" || $request->kordinat_pelanggan=="Harap Tunggu..."){
        return back()->with('alerts',[['type' => 'danger', 'text' => 'Kordinat Pelanggan Salah !. Hidupkan GPS Anda..']]);
    };

    $ndemWoMigrasi='';
    $materials = json_decode($request->input('materials'));
    $cekNdemMigrasi = substr($disp->Ndem, 0, 1);
    // if ($cekNdemMigrasi=='5'){
    //     $ndemWoMigrasi = $disp->Ndem;
    //     if ($a==1){
    //         if (count($materials)==''){
    //             return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Inputkan Material !']]);
    //         }
    //     }
    // }

    if ($jenisPsb=="AO"){
        if ($a==1 || $a==37 || $a==38){
            if (count($materials)==''){
                return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Inputkan Material !']]);
            }
        }
    }


    // dd($request);
    // die;

    // kalo up wo migrasi simpan dshr
    // if ($disp->ketOrder<>''){
    //     if ($a=='1'){
    //         $dataDshr = DB::table('dshr')->where('NDID',$disp->Ndem)->first();
    //         if(count($dataDshr)==0){
    //             $dataWO = DB::table('dossier_master')
    //                         ->where('ND','LIKE','%'.$disp->Ndem.'%')
    //                         ->orwhere('ND_REFERENCE','LIKE'.'%'.$disp->Ndem.'%')
    //                         ->first();

    //             if ($dataWO){
    //                 $dataWoFromTeknisi = DB::table('dispatch_teknisi')
    //                                         ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
    //                                         ->select('dispatch_teknisi.*','psb_laporan.*')
    //                                         ->where('dispatch_teknisi.id',$id)
    //                                         ->first();

    //                 DB::table('dshr')->insert([
    //                     'status_hdesk'    => 'BELUM DIEDIT',
    //                     'status_nal'      => $dataWoFromTeknisi->jenis_layanan,
    //                     'paperless'       => 'BELUM',
    //                     'nama_pelanggan'  => $dataWO->NAMA,
    //                     'pic'             => $dataWoFromTeknisi->noPelangganAktif,
    //                     'tanggal_deal'    => DB::raw('NOW()'),
    //                     'alamat'          => $dataWO->LVOIE.', '.$dataWO->LQUARTIER.', '.$dataWO->LCOM,
    //                     'nama_odp'        => $dataWoFromTeknisi->nama_odp,
    //                     'no_internet'     => $dataWO->ND_REFERENCE,
    //                     'no_telp'         => $dataWO->ND,
    //                     'koor_pel'        => $dataWoFromTeknisi->kordinat_pelanggan,
    //                     'koor_odp'        => $dataWoFromTeknisi->kordinat_odp,
    //                     'created_at'      => DB::raw('NOW()'),
    //                     'NDID'            => $disp->Ndem
    //                 ]);

    //               Telegram::sendMessage([
    //                 'chat_id' => '192915232',
    //                 'text' => 'wo migrasi up sukses tkirim'
    //               ]);
    //             }
    //         }
    //     }
    // };


    // kalo belum ada sc jgn up
    if ($disp->dispatch_by==5){
      if ($a==1){
          $dataNotSc = PsbModel::cekDataMyirIfSc($id);
          if ($dataNotSc->id<>NULL){
            if ($dataNotSc->ket==0){
                return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan UP, Hubungi HDESK Untuk Sinkron SC']]);
            }
          }
      };
    };
    //

    // cek ba_online
    if ($disp->dispatch_by==NULL){
        if($a==1 || $a==37 || $a==38){
            $dataStart = DB::table('Data_Pelanggan_Starclick')->where('orderId',$disp->Ndem)->first();
            if (count($dataStart)<>NULL){
              $cek_badig = DB::table('fitur')->first();
              if ($cek_badig->badig==1){
                $hasil = $this->getApiBaDigital($disp->Ndem);
                if ($hasil->status=='F'){
                  return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan UP, Upload BA Online Terlebih Dahulu']]);
                };
              }
            }
        }

        if ($a==4){
            $dataStart = DB::table('Data_Pelanggan_Starclick')
                         ->where('orderId',$disp->Ndem)
                         ->whereIn('orderStatus',["Fallout (Data)","Process ISISKA (RWOS)","Completed (PS)"])
                         ->first();

            if (count($dataStart)<>NULL AND $id<>"264721" AND id<>"462319"){
                $hasil = $this->getApiBaDigital($disp->Ndem);
                if ($hasil->status=='F'){
                  return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan UP, Upload BA Online Terlebih Dahulu']]);
                };
            }
        }
    };

    // ukur ibooster sebelm up
    // if ($a==1){
    //     if ($disp->jenisPsb=="AO|IPTV"){
    //         $psb = 'MO';
    //     }
    //     else{
    //         $psb = substr($disp->jenisPsb, 0, 2);
    //     };

    //     // $psb   = substr($disp->jenisPsb, 0, 2);
    //     if ($psb=="AO"){
    //         if($disp->redaman_iboster==null){
    //             return back()->with('alerts',[['type' => 'danger', 'text' => 'Ukur Ibooster Dulu']]);
    //         }
    //         else{
    //             if ($disp->redaman_iboster > -13 || $disp->redaman_iboster < -25){
    //               return back()->with('alerts',[['type' => 'danger', 'text' => 'Tidak Bisa UP Redaman Ibooster Diijinkan  (> -13) - (< -25)']]);
    //             }
    //         }
    //     }
    // }


    if ($a==57){
        $simpan = array();
        $getData = DB::table('Data_Pelanggan_Starclick')->where('orderId',$disp->Ndem)->orwhere('kcontact','LIKE','%'.$disp->Ndem.'%')->first();
        $myir = '';
        if(count($getData)<>0){
            $simpan = [
                'MYIR_ID'     => 0,
                'orderId'     => $getData->orderId,
                'orderName'   => $getData->orderName,
                'orderAddr'   => $getData->orderAddr,
                'orderKontak' => $getData->orderKontak,
                'orderDate'   => $getData->orderDate,
                'alproname'   => $getData->alproname,
                'jenisPsb'    => $getData->jenisPsb,
                'sto'         => $getData->sto,
                'lon'         => $getData->lon,
                'lat'         => $getData->lat,
                'ndemSpeedy'  => $getData->ndemSpeedy,
                'ndemPots'    => $getData->ndemPots,
                'kcontact'    => $getData->kcontact,
                'internet'    => $getData->internet,
                'orderStatus' => $getData->orderStatus,
                'orderStatusId' => $getData->orderStatusId
            ];
        }
        else{
          $getData = DB::table('psb_myir_wo')->where('myir',$disp->Ndem)->first();
          $getKordinat = DB::table('dispatch_teknisi')
          					->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.myir')
          					->leftJoin('psb_laporan','dispatch_teknisi.id','psb_laporan.id_tbl_mj')
          					->select('dispatch_teknisi.Ndem', 'psb_laporan.kordinat_pelanggan')
          					->where('dispatch_teknisi.Ndem',$getData->myir)
          					->first();

          if ($getKordinat){
          		$kordinatt = explode(',',$getKordinat->kordinat_pelanggan);
          		if (count($kordinatt)>1){
          			$lot = $kordinatt[0];
          			$lat = $kordinatt[1];
          		}
          		else{
          			$lot = $kordinatt[0];
          			$lat = '';
          		}
          }

          if (count($getData)<>0){
              $simpan = [
                  'MYIR_ID'     => $getData->myir,
                  'orderId'     => 0,
                  'orderName'   => $getData->customer,
                  'orderAddr'   => $getData->alamatLengkap,
                  'orderKontak' => $getData->picPelanggan,
                  'orderDate'   => $getData->orderDate,
                  'alproname'   => $getData->namaOdp,
                  'jenisPsb'    => null,
                  'sto'         => $getData->sto,
                  'lon'         => $lot,
                  'lat'         => $lat,
                  'ndemSpeedy'  => null,
                  'ndemPots'    => null,
                  'kcontact'    => null,
                  'internet'    => null,
                  'orderStatus' => null,
                  'orderStatusId' => null
              ];

              $myir = $getData->myir;
          }
        };

        if (count($simpan)<>0){
            if ($myir<>''){
                DB::table('Data_Pelanggan_UNSC')->where('MYIR_ID',$myir)->delete();
            };

            DB::table('Data_Pelanggan_UNSC')->insert($simpan);
        }
    };

    $get_next_step = DB::table('psb_laporan_status')
                      ->select('step_tracker.*')
                      ->leftJoin('step_tracker','psb_laporan_status.step_id','=','step_tracker.step_tracker_id')
                      ->where('psb_laporan_status.laporan_status_id',$request->input('status'))
                      ->first();

    $next_step = DB::table('dispatch_teknisi')
                     ->where('id',$id)
                     ->update([
                       'step_id' => $get_next_step->step_tracker_id
                     ]);

    $ntes = explode(',', $request->input('nte'));
    $exists = DB::select('
      SELECT a.*,b.id_regu, b.Ndem, b.dispatch_by
      FROM psb_laporan a, dispatch_teknisi b
      WHERE a.id_tbl_mj=b.id AND a.id_tbl_mj = ?
    ',[
      $id
    ]);

    $sendReport = 0;
    $scid = NULL;

    if (count($exists)) {
      $data = $exists[0];

      if ($data->dispatch_by==NULL){
          $scid = $data->Ndem;
      }
      else{
        $scid = NULL;
      }

      if($data->status_laporan!=$request->input('status')){
        $sendReport = 1;
      }
      $kpro_status = '';
      if ($request->input('status')==2){
        $kpro_status = "Kendala Teknik";
      } else if ($request->input('status')==10){
        $kpro_status = "Proses PT2/PT3";
      } else if ($request->input('status')==3) {
        $kpro_status = "Kendala Pelanggan";
      }
      //if ($kpro_status<>'')
      //$this->kpro($data->Ndem,$kpro_status,'manja',$request->input('catatan'));
      DB::transaction(function() use($request, $data, $auth, $materials, $ntes, $scid) {
        $tgl_up = "";
        if($data->status_laporan!=$request->input('status') && $request->input('status')==1){
          $tgl_up = date("Y-m-d");
        }

        DB::table('psb_laporan_log')->insert([
           'created_at'      => DB::raw('NOW()'),
           'created_by'      => $auth->id_karyawan,
           'status_laporan'  => $request->input('status'),
           'id_regu'         => $data->id_regu,
           'catatan'         => $request->input('catatan'),
           'Ndem'            => $data->Ndem,
           'psb_laporan_id'  => $data->id
         ]);
        DB::table('psb_laporan')
          ->where('id', $data->id)
          ->update([
            'modified_at'         => DB::raw('NOW()'),
            'modified_by'         => $auth->id_karyawan,
            'catatan'             => $request->input('catatan'),
            'status_laporan'      => $request->input('status'),
            'kordinat_pelanggan'  => $request->input('kordinat_pelanggan'),
            'nama_odp'            => $request->input('nama_odp'),
            'kordinat_odp'        => $request->input('kordinat_odp'),
            'typeont'             => $request->input('type_ont'),
            'snont'               => $request->input('sn_ont'),
            'typestb'             => $request->input('type_stb'),
            'snstb'               => $request->input('sn_stb'),
            'dropcore_label_code' => $request->input('dropcore_label_code'),
            'odp_label_code'      => $request->input('odp_label_code'),
            'port_number'         => $request->input('port_number'),
            'redaman'             => $request->input('redaman'),
            'tgl_up'              => $tgl_up,
            'ttId'                => $request->tbTiang,
            'catatanRevoke'       => $request->input('catatanRevoke'),
            'noPelangganAktif'    => $request->input('noPelangganAktif'),
            'scid'                => $scid,
            'redaman_odp'         => $request->input('redamanOdp'),
            'jenis_odp'           => $request->input('jenisOdp'),
            'redaman_in'          => $request->input('redamanInOdp')
          ]);

        // if status odp loss, insert tiang, odp full
        // update field status_kendala dan tgl_status_kendala
        // hanya teknisi
        $dataNik = DB::table('psb_nik_scbe')->where('nik',$auth->id_karyawan)->first();
        if (count($dataNik)==0){
            $statusSc = '';
            $getStatusSc = DB::table('Data_Pelanggan_Starclick')->where('orderId',$data->Ndem)->first();
            if($getStatusSc){
                $statusSc = $getStatusSc->orderStatus;
            };

            $dataKendala = DB::table('psb_laporan_status')->where('laporan_status_id',$request->input('status'))->first();
            if ($dataKendala){
                if ($dataKendala->ketList=="KT"){
                    DB::table('psb_laporan')
                        ->where('id',$data->id)
                        ->update([
                            'status_kendala'      => $dataKendala->laporan_status,
                            'tgl_status_kendala'  => date('Y-m-d H:i:s'),
                            'status_sc'           => $statusSc
                        ]);
                };
            };
        };

        // if ($request->input('status')==11 || $request->input('status')==24 || $request->input('status')==2){
        //     if ($request->input('status')==11){
        //         $statusKendala = "INSERT TIANG";
        //     }
        //     else if ($request->input('status')==24){
        //         $statusKendala = "ODP FULL";
        //     }
        //     else{
        //         $statusKendala = "ORDER MAINTENANCE";
        //     };

        //     DB::table('psb_laporan')
        //       ->where('id',$data->id)
        //       ->update([
        //           'status_kendala'      => $statusKendala,
        //           'tgl_status_kendala'  => date('Y-m-d H:i:s')
        //       ]);
        // };


        $this->incrementStokTeknisi($data->id, $auth->id_karyawan);
        // DB::table('psb_laporan_material')
        //   ->where('psb_laporan_id', $data->id)
        //   ->delete();
        // foreach($ntes as $nte) {
        //   DB::table('psb_laporan_material')->insert([
        //     'id' => $data->id.'-'.$nte,
        //     'psb_laporan_id' => $data->id,
        //     'id_item' => $nte,
        //     'qty' => 1,
        //     'type' =>2
        //   ]);
        //   DB::table('nte')
        //     ->where('id', $nte)
        //     ->update([
        //       'position_type'  => 3,
        //       'position_key'  => $data->id_tbl_mj,
        //       'status' => 1
        //     ]);
        // }

        if ($materials){
            $this->insertMaterials($data->id, $materials);
        };

        $this->decrementStokTeknisi($auth->id_karyawan, $materials);

      });

      if ($a<>52 && $a<>57 && $a<>58 && $a<>59 && $a<>60){
        if($sendReport){
            if ($disp->dispatch_by==6){
                exec('cd ..;php artisan sendReportReboundary '.$id.' > /dev/null &');
            }
            else{
                exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
            }
        };
      };

      // send tele grup KP
      $lapStatus = $request->input('status');
      $getGrup   = DB::table('psb_laporan_status')->where('laporan_status_id',$lapStatus)->first();
      if ($getGrup->grup=="KP"){
            exec('cd ..;php artisan sendReportKp '.$id.' > /dev/null &');
      };

      // dd($data->dispatch_by);
      // if ($data->dispatch_by<>'5' && $cekNdemMigrasi<>'5'){
      //     $this->sendtobotkpro($id);
      // }

      // proses kirim sms
      // $getMyir = DB::table('Data_Pelanggan_Starclick')->where('orderId',$data->Ndem)->first();
      // if (!empty($getMyir)){
      //     $dataMyir  = $getMyir->kcontact;
      //     $pisah     = explode(';', $dataMyir);
      //     $myir      = $pisah[1];
      // };

      // $dataStatuslaporan = DB::table('psb_laporan_status')->where('laporan_status_id',$request->input('status'))->first();
      // if ($dataStatuslaporan->grup=="KT" || $dataStatuslaporan->grup=="KP"){
      //     $dataEksis = DB::table('psb_laporan_log')->where('Ndem',$data->Ndem)->where('status_laporan',$request->input('status'))->get();
      //     if (count($dataEksis)==0){
      //       $pesan = "Pelanggan Yth. \n";
      //       $pesan .= "Order PSB Anda dg nomor '".$myir."' Saat ini \n";
      //       $pesan .= "RESCHEDULE dengan alasan '".$dataStatuslaporan->laporan_status."' Jika sesuai abaikan sms ini, \n";
      //       $pesan .= "jika tidak silahkan sampaikan keterangan dg mereply sms ini.";

      //       // masukkan log
      //       DB::table('psb_laporan_log')->insert([
      //          'created_at'      => DB::raw('NOW()'),
      //          'created_by'      => $auth->id_karyawan,
      //          'status_laporan'  => $request->input('status'),
      //          'id_regu'         => $data->id_regu,
      //          'catatan'         => 'Pesan SMS! '.$pesan,
      //          'Ndem'            => $data->Ndem,
      //          'psb_laporan_id'  => $data->id
      //       ]);

      //       // kirim sms
      //       $psn = str_replace('\n', ' ', $pesan);
      //       $dataDikirim = '("'.$data->noPelangganAktif.'","'.$psn.'","true","3006")';
      //       $sql = 'INSERT INTO outbox (DestinationNumber,TextDecoded,MultiPart,CreatorID) values '.$dataDikirim;
      //       DB::connection('mysql2')->insert($sql);
      //     }
      // };


      if ($a == 1) {
        echo "kirim sms";
        $get_order = DB::SELECT('
          SELECT
          *,
          a.id as id_dt
          FROM
            dispatch_teknisi a
          LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
          LEFT JOIN regu c ON a.id_regu = c.id_regu
          LEFT JOIN Data_Pelanggan_Starclick d ON a.Ndem = d.orderId
          LEFT JOIN group_telegram e ON c.mainsector = e.chat_id
          WHERE
            a.id = "'.$id.'"
            ')[0];

            if ($get_order->orderId<>NULL && $get_order->sms_active_prov==1):
            $pesan = "Pelanggan Yth. \n";
            $pesan .= "Mhn bantuan penilaian kpd Teknisi Indihome Kami. Silahkan klik link berikut \n";
            $pesan .= "http://tomman.info/rv/".$this->encrypt($get_order->id_dt)." \n";
            $pesan .= "Jika layanan brmasalah hub 05116775775";

            // masukkan log
            DB::table('psb_laporan_log')->insert([
               'created_at'      => DB::raw('NOW()'),
               'created_by'      => $auth->id_karyawan,
               'status_laporan'  => $request->input('status'),
               'id_regu'         => $get_order->id_regu,
               'catatan'         => 'Pesan SMS Terkirim ',
               'Ndem'            => $get_order->Ndem,
               'psb_laporan_id'  => $data->id
            ]);

            // kirim sms
            $psn = str_replace('\n', ' ', $pesan);
            $dataDikirim = '("'.$get_order->orderKontak.'","'.$psn.'","true","3006")';
            $sql = 'INSERT INTO outbox (DestinationNumber,TextDecoded,MultiPart,CreatorID) values '.$dataDikirim;
            DB::connection('mysql2')->insert($sql);
            endif;
      };

      // send dava
      // if($a==1 && $jenisPsb=="AO"){
      //     // $getData = DB::table('dispatch_teknisi')
      //     //       ->leftJoin('Data_Pelanggan_Starclick','dispatch_teknisi.Ndem','=','Data_Pelanggan_Starclick.orderId')
      //     //       ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
      //     //       ->select('dispatch_teknisi.*', 'Data_Pelanggan_Starclick.*', 'psb_laporan.*')
      //     //       ->where('dispatch_teknisi.id',$id)
      //     //       ->get();

      //     $getData = DB::table('dispatch_teknisi')
      //       ->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.myir')
      //       ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
      //       ->select('dispatch_teknisi.*', 'psb_myir_wo.*', 'psb_laporan.*')
      //       ->where('dispatch_teknisi.id',$id)
      //       ->get();

      //     if (count($getData)){
      //         $getData = DB::table('dispatch_teknisi')
      //             ->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.sc')
      //             ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
      //             ->select('dispatch_teknisi.*', 'psb_myir_wo.*', 'psb_laporan.*')
      //             ->where('dispatch_teknisi.id',$id)
      //             ->get();
      //     };

      //     $data = array();
      //     if(count($getData)<>null){
      //         foreach($getData as $dt){
      //             $korOdp = explode(',', $dt->kordinat_odp);
      //             $data = [
      //                   'id'              => $id,
      //                   'odp_location'    => $dt->nama_odp,
      //                   'port_number'     => $dt->port_number,
      //                   'latitude'        => '',
      //                   'longitude'       => '',
      //                   'sn_ont'          => $dt->snont,
      //                   'nama_pelanggan'  => $dt->customer,
      //                   'voice_number'    => $dt->picPelanggan,
      //                   'internet_number' => $dt->no_internet,
      //                   'date_validation' => date('Y-m-d'),
      //                   'qr_port_odp'     => '',
      //                   'qr_dropcore'     => $dt->dropcore_label_code,
      //                   'nama_jalan'      => $dt->alamatLengkap,
      //                   'contact'         => $dt->noPelangganAktif,
      //             ];
      //         }
      //     };

      //     $pesan = $this->insertQrCodeApi($data);
      //     $listPesan = json_decode($pesan);
      //     if (is_array($listPesan->message)){
      //         $pesan = '';
      //         foreach($listPesan->message as $psn){
      //           $pesan .= $psn.', ';
      //         }
      //         $pesan = substr($pesan, 0, -2);
      //         return back()->with('alerts',[['type'=>'danger', 'text'=>'Gagal Simpan Data "<b>'.$pesan.'"</b>']]);
      //     };
      // };

      // exec('cd ..;php artisan kpro '.$disp->Ndem.' Laporan > /dev/null &');

      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }
    else {
      DB::transaction(function() use($request, $id, $auth, $materials, $ntes, $scid) {
        $tgl_up = "";
        if($request->input('status')==1){
          $tgl_up = date("Y-m-d");
        }

        $insertId = DB::table('psb_laporan')->insertGetId([
          'created_at'          => DB::raw('NOW()'),
          'modified_at'         => DB::raw('NOW()'),
          'created_by'          => $auth->id_karyawan,
          'id_tbl_mj'           => $id,
          'catatan'             => $request->input('catatan'),
          'status_laporan'      => $request->input('status'),
          'kordinat_pelanggan'  => $request->input('kordinat_pelanggan'),
          'nama_odp'            => $request->input('nama_odp'),
          'kordinat_odp'        => $request->input('kordinat_odp'),
          'typeont'             => $request->input('type_ont'),
          'snont'               => $request->input('sn_ont'),
          'typestb'             => $request->input('type_stb'),
          'snstb'               => $request->input('sn_stb'),
          'dropcore_label_code' => $request->input('dropcore_label_code'),
          'odp_label_code'      => $request->input('odp_label_code'),
          'port_number'         => $request->input('port_number'),
          'redaman'             => $request->input('redaman'),
          'tgl_up'              => $tgl_up,
          'ttId'                => $request->tbTiang,
          'catatanRevoke'       => $request->input('catatanRevoke'),
          'noPelangganAktif'    => $request->input('noPelangganAktif'),
          'scid'                => $scid,
          'redaman_odp'         => $request->input('redamanOdp'),
          'jenis_odp'           => $request->input('jenisOdp'),
          'redaman_in'          => $request->input('redamanInOdp')
        ]);
        foreach($ntes as $nte) {
          DB::table('psb_laporan_material')->insert([
            'id' => $insertId.'-'.$nte,
            'psb_laporan_id' => $insertId,
            'id_item' => $nte,
            'qty' => 1,
            'type' =>2
          ]);
          DB::table('nte')
            ->where('id', $nte)
            ->update([
              'position_type'  => 3,
              'position_key'  => $id,
              'status' => 1
            ]);
        }
        //$disp = @$disp[0];
        //print_r($disp);
        if ($materials){
            // $this->insertMaterials($data->id, $materials);
            $this->insertMaterials($insertId, $materials);
        };

        $this->decrementStokTeknisi($auth->id_karyawan, $materials);
        //insert into log
        /*
        DB::transaction(function() use($request, $disp, $auth, $insertId) {
           DB::table('psb_laporan_log')->insert([
             'created_at'      => DB::raw('NOW()'),
             'created_by'      => $auth->id_karyawan,
             'status_laporan'  => $request->input('status'),
             'id_regu'         => $disp->id_regu,
             'catatan'         => $request->input('catatan'),
             'Ndem'            => $disp->Ndem,
             'psb_laporan_id'  => $insertId
           ]);
         });
         */

      });

      $table = DB::table('hdd')->get()->first();

      // exec('cd ..;php artisan sendReport '.$id.' > /dev/null &', $out);
      if ($disp->dispatch_by==6){
          exec('cd ..;php artisan sendReportReboundary '.$id.' > /dev/null &');
      }
      else{
          exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
      }

      return redirect('/')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }

  }

  private function insertMaterials($psbId, $materials)
  {
    $nik = session('auth')->id_user;
    foreach($materials as $material) {
      $exis = DB::table('psb_laporan_material')
                ->where('id',$psbId.'-'.$material->id_item)
                ->first();

      if ($exis){
        DB::table('psb_laporan_material')
          ->where('id',$psbId.'-'.$material->id_item)
          ->update([
              'qty' => $material->qty
          ]);
      }
      else{
        DB::table('psb_laporan_material')->insert([
              'id' => $psbId.'-'.$material->id_item,
              'psb_laporan_id' => $psbId,
              'id_item' => trim($material->id_item),
              'id_item_bantu' => trim($material->id_item).'_'.$psbId,
              'qty' => $material->qty,
              'type' => 1
          ]);


          // simpan ke maintaince_saldo_rfc
          // $pakai = DB::table('psb_laporan')->where('id',$psbId)->first();
          // $dtSaldo     = DB::table('rfc_sal')->where('rfc',$material->rfc)->where('nik',$nik)->first();
          // $dataRegu    = DB::SELECT('SELECT a.*, b.*
          //                          FROM regu a
          //                          LEFT JOIN group_telegram b ON a.mainsector=b.chat_id
          //                          WHERE
          //                             a.ACTIVE=1 AND
          //                             (a.nik1="'.$nik.'" OR a.nik2="'.$nik.'")');
          //
          // $idRegu = NULL; $uraianRegu = NULL; $nik1   = NULL; $nik2 = NULL; $nikTl = NULL;
          // if(count($dataRegu)<>0){
          //     $idRegu     = $dataRegu[0]->id_regu;
          //     $uraianRegu = $dataRegu[0]->uraian;
          //     $nik1       = $dataRegu[0]->nik1;
          //     $nik2       = $dataRegu[0]->nik2;
          //     $nikTl      = $dataRegu[0]->TL_NIK;
          // }

            // DB::table('maintenance_saldo_rfc')->where('created_by',$nik)->where('bantu',$material->rfc.'#'.$material->id_item)->where('action',2)->where('transaksi',1)->delete();
            // DB::table('maintenance_saldo_rfc')->insert([
            //     'id_pengeluaran'    => $dtSaldo->alista_id,
            //     'rfc'               => $dtSaldo->rfc,
            //     'regu_id'           => $idRegu,
            //     'regu_name'         => $uraianRegu,
            //     'nik1'              => $nik1,
            //     'nik2'              => $nik2,
            //     'niktl'             => $nikTl,
            //     'created_at'        => DB::raw('NOW()'),
            //     'created_by'        => $nik,
            //     'id_pemakaian'      => $pakai->id_tbl_mj,
            //     'value'             => $material->qty * -1,
            //     'id_item'           => $material->id_item,
            //     'action'            => '2',
            //     'bantu'             => $material->rfc.'#'.$material->id_item,
            //     'transaksi'         => '1'
            // ]);
          ////
      }

      // send report
      // get id dari psbid
      $idPsb = PsbModel::getIdWoFrormPsbid($psbId);
      exec('cd ..;php artisan sendReportMaterial '.$idPsb->id.' > /dev/null &');

      // cek data material di psb_laporan_material type 1 klo ada update saldo
      $dataMaterial = PsbModel::cekDataMaterial($psbId);
      if (count($dataMaterial)<>0){
          // update saldo terpakai
          $dataA = DB::table('psb_laporan_material')
                    ->where('id_item_bantu',$material->id_item)
                    ->get();
          $terpakai = 0;
          foreach($dataA as $aaa){
              $terpakai += $aaa->qty;
          }

          // update terpakai
          // DB::table('rfc_sal')->where('nik',$nik)->where('id_item_bantu',$dataA[0]->id_item_bantu)->update(['jmlTerpakai' => $terpakai]);
      }

    }

  }

  private function decrementStokTeknisi($id_karyawan, $materials)
  {
    foreach($materials as $material) {
      $exists = DB::select('
        SELECT *
        FROM stok_material_teknisi
        WHERE id_karyawan = ? and id_item = ?
      ',[
        $id_karyawan, $material->id_item
      ]);

      if (count($exists)) {
        DB::table('stok_material_teknisi')
          ->where('id_item', $material->id_item)
          ->where('id_karyawan', $id_karyawan)
          ->decrement('stok', $material->qty);
      }else{
        $insertId=DB::table('stok_material_teknisi')->insertGetId([
          'id_item' => $material->id_item,
          'id_karyawan' => $material->qty
        ]);
        DB::table('stok_material_teknisi')
          ->where('id', $insertId)
          ->decrement('stok', $material->qty);
      }

    }
  }
  private function incrementStokTeknisi($id, $idkaryawan)
  {
    $materials = DB::select('
        SELECT *
        FROM psb_laporan_material
        WHERE psb_laporan_id = ? and type = 1
      ',[
        $id
      ]);
    foreach($materials as $material) {
      DB::table('stok_material_teknisi')
        ->where('id_item', $material->id_item)
        ->where('id_karyawan', $idkaryawan)
        ->increment('stok', $material->qty);
    }
    $ntes = DB::select('
        SELECT *
        FROM psb_laporan_material
        WHERE psb_laporan_id = ? and type = 2
      ',[
        $id
      ]);
    foreach($ntes as $nte) {
      DB::table('nte')
        ->where('id', $nte->id_item)
        ->update([
            'position_type'  => 2,
            'position_key'  => $idkaryawan,
            'status' => 1
          ]);
    }
  }

  private function handleFileUpload($request, $id)
  {
   $HasilFoto = array();
   $HasilFoto['type'] = 'success';
   $HasilFoto['message'] = 'Berhasil';
    foreach($this->photoInputs as $name) {
      $input = 'photo-'.$name;
      $table = DB::table('hdd')->get()->first();
      $hdd = $table->active_hdd;
      $upload = $table->public;
      if ($request->hasFile($input)) {
        $path = public_path().'/'.$upload.'/evidence/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true)){
             // $HasilFoto['type'] = 'danger';
             // $HasilFoto['message'] = 'Gagal membuat folder';
             //  return $HasilFoto;
          }
        }
        $file = $request->file($input);
        // $ext = $file->guessExtension();
        $ext = 'jpg';
        //$HasilFoto = array();

        $filemime = @exif_read_data($file);

        if ($name <> "Berita_Acara" && $name <> "Telephone_Incoming" && $name <> "Telephone_Outgoing" && $name <> "Speedtest" && $name <> "Wifi_Analyzer" && $name <> "Excheck_Helpdesk" && $name<> "Redaman_Pelanggan" && $name <> 'BA_Digital' && $name <> 'additional_1'
        && $name <> 'additional_2' && $name <> 'additional_3' && $name <> 'additional_4'  && $name <> 'additional_5') {
          if (@array_key_exists("Software",$filemime)){
            if (@$filemime['Software']<>"Timestamp Camera" && @$filemime['Software']<>"Timestamp Camera ENT")
            {
             $HasilFoto['type'] = 'danger';
             $HasilFoto['message'] = 'Foto '.$name.' tidak menggunakan timestamp Camera';
              return $HasilFoto;
            }
          } else {
           $HasilFoto['type'] = 'danger';
           $HasilFoto['message'] = 'Foto '.$name.' tidak menggunakan timestamp Camera';
            return $HasilFoto;
          }
        }

        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
          // return$HasilFoto;
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return $HasilFoto;
        }
      }
    }
    return $HasilFoto;
    // foreach($this->photoInputs as $name) {
    //   $flag  = 'flag_'.$name;
    //   $input = 'photo-'.$name;
    //   // if ($request->$flag==1){
    //       if ($request->hasFile($input)) {
    //         $path = public_path().'/upload/evidence/'.$id.'/';
    //         if (!file_exists($path)) {
    //           if (!mkdir($path, 0770, true))
    //             return 'gagal menyiapkan folder foto evidence';
    //         }
    //         $file = $request->file($input);
    //         // $ext = $file->guessExtension();
    //         $ext = 'jpg';
    //         //TODO: path, move, resize
    //         try {
    //           $moved = $file->move("$path", "$name.$ext");

    //           $img = new \Imagick($moved->getRealPath());

    //           $img->scaleImage(100, 150, true);
    //           $img->writeImage("$path/$name-th.$ext");



    //         }
    //         catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
    //           return 'gagal menyimpan foto evidence '.$name;
    //         }
    //       }
    //   // }
    // }
  }

  private function handleFileUploadReboundary($request, $id)
  {
   $HasilFoto = array();
   $HasilFoto['type'] = 'success';
   $HasilFoto['message'] = 'Berhasil';
    foreach($this->photoReboundary as $name) {
      $input = 'photo-'.$name;
      if ($request->hasFile($input)) {
        $path = public_path().'/upload3/evidence/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true)){
             // $HasilFoto['type'] = 'danger';
             // $HasilFoto['message'] = 'Gagal membuat folder';
             //  return $HasilFoto;
          }
        }
        $file = $request->file($input);
        // $ext = $file->guessExtension();
        $ext = 'jpg';
        //$HasilFoto = array();

        $filemime = @exif_read_data($file);

        if ($name <> "Speedtest" ) {
          if (@array_key_exists("Software",$filemime)){
            if (@$filemime['Software']<>"Timestamp Camera" && @$filemime['Software']<>"Timestamp Camera ENT")
            {
             $HasilFoto['type'] = 'danger';
             $HasilFoto['message'] = 'Foto '.$name.' tidak menggunakan timestamp Camera';
              return $HasilFoto;
            }
          } else {
           $HasilFoto['type'] = 'danger';
           $HasilFoto['message'] = 'Foto '.$name.' tidak menggunakan timestamp Camera';
            return $HasilFoto;
          }
        }

        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
          // return$HasilFoto;
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return $HasilFoto;
        }
      }
    }
    return $HasilFoto;
  }

  public function sendtotelegram2($id, $cid){

    // data WO
    $order = DB::select('
      SELECT
        *,
        ms2n.*,
        d.id as id_dt,
        d.updated_at as tgl_dispatch,
        r.uraian as nama_tim,
        m.Kcontact as mKcontact,
        m.ND as mND,
        m.ND_Speedy as mND_Speedy,
        m.Nama as mNama,
        m.Alamat as mAlamat,
        p2.mdf_grup_id as grup_id_psb,
        m2.mdf_grup_id as grup_id_migrasi,
        p2.mdf_grup as grup_psb,
        m2.mdf_grup as grup_migrasi
      FROM dispatch_teknisi d
      LEFT JOIN regu r ON (d.id_regu=r.id_regu)
      LEFT JOIN ms2n USING (Ndem)
      LEFT JOIN mdf p1 ON ms2n.mdf=p1.mdf
      LEFT JOIN mdf_grup p2 ON p1.mdf_grup_id = p2.mdf_grup_id
      LEFT JOIN micc_wo m on d.Ndem = m.ND
      LEFT JOIN mdf m1 ON m.MDF = m1.mdf
      LEFT JOIN mdf_grup m2 ON m1.mdf_grup_id = m2.mdf_grup_id
      WHERE d.id = ?
    ',[
      $id
    ])[0];
    // Informasi teknisi
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ])[0];
    //data material
    $get_materials = DB::select('
      SELECT a.*
      FROM
        psb_laporan_material a
      LEFT JOIN psb_laporan b ON a.psb_laporan_id = b.id
      WHERE id_tbl_mj = ?
    ',[$id]);

    $materials = "";
    if (count($get_materials)>0) {
      foreach ($get_materials as $material) :
        if ($material->id_item<>'')
          $materials .= $material->id_item." : ".$material->qty."\n";
      endforeach;
    }  else {
      $materials = "";
    }

    $nama_tim = explode(' ',str_replace(array('-',':'),' ',$order->nama_tim));
    $new_nama_tim = '';
    for ($i=0;$i<count($nama_tim);$i++){
      $new_nama_tim .= $nama_tim[$i];
    }

    $messaggio = "CCAN"."\n\n".$order->nama_tim."\n\n";
    $chatID = $cid;

    $messaggio .= "Dispatch WO Pada : ".$order->tgl_dispatch;
    $messaggio .= "\n\n";

    $messaggio .= "STATUS ORDER : ".$this->status_wo($exists->status_laporan);
    $messaggio .= "\n\n";

    $s = '';
    if ($exists->catatan<>''){
      $messaggio .= "CATATAN : \n";
      $messaggio .= $exists->catatan."\n\n";
    }
    if ($order->Status_Indihome<>'') {
      $s = $order->Nama;
      $messaggio .= $order->Ndem."\n";
      $messaggio .= $order->ND_Speedy."\n";
      $messaggio .= $order->Nama."\n";
      $messaggio .= $order->Kcontact."\n";
      $messaggio .= $order->Jalan." ".$order->No_Jalan."\n";
      $messaggio .= $order->Distrik." ".$order->Kota."\n";
    } else {
      $s = $order->mNama;
      $messaggio .= $order->mND."\n";
      $messaggio .= $order->mND_Speedy."\n";
      $messaggio .= $order->mNama."\n";
      $messaggio .= $order->mAlamat."\n";
    }

    $s .= " #".strtolower($new_nama_tim);

      $messaggio .= "\n";
    if ($exists->kordinat_pelanggan<>'')
    {
      $messaggio .= "Kordinat Pelanggan : \n";
      $messaggio .= $exists->kordinat_pelanggan."\n";
    }
    else
      $messaggio .= "Tidak ada info kordinat pelanggan\n";

      $messaggio .= "\n";

    if ($exists->nama_odp<>'')
    {
      $messaggio .= "Nama ODP : \n";
      $messaggio .= $exists->nama_odp."\n";
    }
    else
      $messaggio .= "Tidak ada info nama ODP pelanggan\n";

      $messaggio .= "\n";
    if ($exists->kordinat_odp<>'')
    {
      $messaggio .= "Kordinat ODP : \n";
      $messaggio .= $exists->kordinat_odp."\n";
    }
    else
      $messaggio .= "Tidak ada info kordinat ODP pelanggan\n";



    $messaggio .= "\n";
    $messaggio .= "#report".strtolower($new_nama_tim)."\n";

    $messaggio .= "\n";


    $messaggio .= $materials;


//     $chatID = "-131356768"; SHVA

    Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messaggio
    ]);


    $photoInputs = $this->photoInputs;
    for($i=0;$i<count($photoInputs);$i++) {
      $file = public_path()."/upload3/evidence/".$id."/".$photoInputs[$i].".jpg";
      if (file_exists($file)){
        Telegram::sendPhoto([
          'chat_id' => $chatID,
          'caption' => $photoInputs[$i]." ".$s,
          'photo' => $file
        ]);

//      echo $file;
      }
    }

      return redirect('/UP')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengirim laporan']
      ]);

  }
  public function redaman($id){
    $plg = explode("~", $id);
    $no_speedy = $plg[1];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://embassy.telkom.co.id/embassy/index.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $akun = DB::table('akun')->where('app', 'SSO Telkom')->first();
    $fields = array(
        'nik'=>$akun->user,
        'pass'=>$akun->pwd,
        'login'=> 'Login'
    );

    $fields_string = http_build_query($fields);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    $rough_content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0] ;
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    curl_setopt($ch, CURLOPT_URL, 'http://embassy.telkom.co.id/embassy/ukur_radonline.php');

    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    $fields = array(
        'no_speedy'=>$no_speedy,
        'enter'=>'UKUR',
        'domain'=> 'telkom.net'
    );

    $fields_string = http_build_query($fields);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    $result = trim(curl_exec($ch));
    curl_close($ch);
    $pos = strpos($result, "#CCDDEE>Rx Power")+50;
    $redaman = substr($result, $pos, 6);
    return $redaman;
  }

  public function maintenanceOrder($id){
    $get_laporan_status = PsbModel::psb_laporan_status();
    $group_telegram = DB::table('group_telegram')->get();
    $checkONGOING = PsbModel::checkONGOING();
    $list = PsbModel::orderMaintenance($id);

    return view('psb.list2', compact('list','group_telegram','get_laporan_status','checkONGOING'));
  }

  public function maintenanceProgress($id){
      $foto = $this->photoCommon;
      $exists = DB::select('
        SELECT m.*, pl.nama_odp, pl.kordinat_odp, pl.id_tbl_mj, mr.niktl
        FROM maintaince m
        left join psb_laporan pl on m.psb_laporan_id = pl.id
        LEFT JOIN maintenance_regu mr on m.dispatch_regu_id=mr.id
        WHERE m.id = ?
      ', [
        $id
      ]);
      $komen = DB::table('maintenance_note')->where('mt_id', $id)->orderBy('id', 'desc')->get();
      $action_cause = DB::table('maintenance_cause_action')->select('action_cause as text', 'action_cause as id', 'status')->get();
      if (count($exists)) {
        $data = $exists[0];
        if($data->jenis_order == 3)
          $foto = $this->photoOdpLoss;
        else if($data->jenis_order == 4)
          $foto = $this->photoRemo;
        if($data->action_cause){
          $action_cause = DB::table('maintenance_cause_action')
            ->select('action_cause as text', 'action_cause as id', 'status')
            ->where('status', $data->status)->get();
        }
      }
      else{
        $data = new \stdClass();
      }

      $materials = DB::select('
          SELECT i.*, i.uraian as nama_item,COALESCE(m.qty, 0) AS qty FROM maintenance_mtr_program mmp
          LEFT JOIN maintaince_mtr m ON mmp.id_item = m.id_item
            AND m.maintaince_id = ?
          LEFT JOIN khs_maintenance i ON mmp.id_item = i.id_item
          WHERE program_id = ?
          ', [
            $id, $data->jenis_order
          ]);
      // print_r($materials);

      $maintenanceLevel = DB::table('user')->where('id_user','=',session('auth')->id_user)->first();
      $level            = $maintenanceLevel->maintenance_level;

      return view('psb.progress', compact('data', 'materials', 'foto', 'action_cause', 'komen', 'level'));
  }

  private function handleFileUploadMaintenance($request, $id){
    foreach($this->photoInputs as $name) {
      $input = 'photo-'.$name;
      if ($request->hasFile($input)) {
        //dd($input);
        $path = public_path().'/upload/maintenance/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true))
            return 'gagal menyiapkan folder foto evidence';
        }
        $file = $request->file($input);
        $ext = 'jpg';
        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");
          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence '.$name;
        }
      }
    }
  }

  public function saveProgres(Request $request, $id){
    $rules = array(
      'status' => 'required'
    );
    $messages = [
      'status.required' => 'Silahkan pilih "Status" Bang!',
      'action_cause.required' => 'Silahkan pilih "Action/Cause" Bang!',
      'splitter.required' => 'Silahkan pilih "Jenis Splitter" Bang!',
      'flag_Sebelum.required' => 'Silahkan Isi Foto "Sebelum" Bang!',
      'flag_Progress.required' => 'Silahkan Isi Foto "Progress" Bang!',
      'flag_Sesudah.required' => 'Silahkan Isi Foto "Sesudah" Bang!',

      'flag_ODP.required' => 'Silahkan Isi Foto "ODP" Bang!',
      'flag_ODP-dan-Redaman-GRID.required' => 'Silahkan Isi Foto "ODP-dan-Redaman-GRID" Bang!',
      'flag_Pengukuran.required' => 'Silahkan Isi Foto "Pengukuran" Bang!',
      'flag_Pelanggan_GRID.required' => 'Silahkan Isi Foto "Pelanggan_GRID" Bang!',
      'flag_BA-Remo.required' => 'Silahkan Isi Foto "BA-Remo" Bang!',
      'flag_Foto-GRID.required' => 'Silahkan Isi Foto "Foto-GRID" Bang!',
      'flag_Denah-Lokasi.required' => 'Silahkan Isi Foto "Denah-Lokasi" Bang!'
    ];

    $validator = Validator::make($request->all(), $rules, $messages);

    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM maintaince
      WHERE id = ?
    ',[
      $id
    ]);
    $this->handleFileUploadMaintenance($request, $id);
    $materials = json_decode($request->input('materials'));
    if (count($exists)) {
      $data = $exists[0];
      if($request->status == 'close' && $data->verify == null && $request->action_cause != "CANCEL ORDER"){
        $validator->sometimes('action_cause', 'required', function ($input) {
            return true;
        });
        $validator->sometimes('flag_Sebelum', 'required', function ($input) {
            return true;
        });
        $validator->sometimes('flag_Progress', 'required', function ($input) {
            return true;
        });
        $validator->sometimes('flag_Sesudah', 'required', function ($input) {
            return true;
        });
        if($data->jenis_order == 4){
          $validator->sometimes('flag_ODP-dan-Redaman-GRID', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_Pengukuran', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_Pelanggan_GRID', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_BA-Remo', 'required', function ($input) {
              return true;
          });
        }else if($data->jenis_order == 3){
          $validator->sometimes('flag_ODP', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_Foto-GRID', 'required', function ($input) {
              return true;
          });
        }else{
          $validator->sometimes('flag_Foto-GRID', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_Denah-Lokasi', 'required', function ($input) {
              return true;
          });
        }
        if($data->jenis_order == 3 || $data->jenis_order == 4){
          $validator->sometimes('splitter', 'required', function ($input) {
              return true;
          });
        }
      }
      if ($validator->fails()) {
        return redirect()->back()
            ->withInput($request->all())
            ->withErrors($validator)
            ->with('alerts', [
              ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan (Dokumen atau Isian kurang lengkap)']
            ]);
      }
      DB::transaction(function() use($request, $data, $auth, $materials, $id) {
        DB::table('maintaince_mtr')
          ->where('maintaince_id', $data->id)
          ->delete();
        $mtrcount = 0;
        foreach($materials as $material) {
          DB::table('maintaince_mtr')->insert([
            'maintaince_id' => $data->id,
            'id_item'       => $material->id_item,
            'qty'           => $material->qty,
          ]);
          $mtrcount++;
        }
        $total = DB::select('
          SELECT
          (SELECT sum((qty*material_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as material,
          (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as jasa
          FROM maintaince m
          WHERE id = ?
        ', [$id])[0];
        $hasil_by_qc = $data->hasil_by_qc;
        $hasil_by_user = $data->hasil_by_user;
        if($request->input('status')=="close"){
          $hasil_by_user = $total->jasa + $total->material;
        }else if($request->input('status')=="qqc"){
          $hasil_by_qc = $total->jasa + $total->material;
        }
        $tgl_selesai = $data->tgl_selesai;
        $verify = $data->verify;
        //if($data->status!=$request->input('status') && $request->input('status')=='close'){
        if($data->status!=$request->input('status')){
          $tgl_selesai = date('Y-m-d H:i:s');
        }
        /*
          null = in TL
          1 = in SM
          2 = in TELKOM
          3 = siap rekon
        */
        if($request->verify != $data->verify){
          if($request->verify > $data->verify){
            //$verify = date('Y-m-d H:i:s');
            $status = "APPROVE";
          }
          else{
            //$verify = NULL;
            $status = "DECLINE";
          }
          if($request->catatan)
            $komen = $request->catatan;
          else
            $komen = $status;
          DB::table('maintenance_note')
          ->insert([
            'mt_id'     => $data->id,
            'catatan'   => $komen,
            'status'   => $status,
            'pelaku'    => $auth->id_karyawan,
            'ts'     => DB::raw('NOW()')
          ]);
        }
        DB::table('maintaince')
          ->where('id', $data->id)
          ->update([
            'status_name'     => $request->input('status'),
            'status'          => $request->input('status'),
            'tgl_selesai'     => $tgl_selesai,
            'verify'          => $request->verify,
            'mtrcount'        => $mtrcount,
            'action'          => $request->input('action'),
            'koordinat'       => $request->input('koordinat'),
            'modified_at'     => DB::raw('NOW()'),
            'modified_by'     => $auth->id_karyawan,
            'hasil_by_user'   => $hasil_by_user,
            'hasil_by_qc'     => $hasil_by_qc,
            'action_cause'    => $request->input('action_cause'),
            'splitter'        => $request->input('splitter'),
          ]);
      });
    }
    if($request->input('status') != $data->status){
      exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
    }
    return redirect('/tech')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
    ]);
  }

  public function maintenanceUpload($id){
    $path2 = public_path().'/upload/maintenance/'.$id.'th';
    $th = array();
    if (file_exists($path2)) {
      $photos = File::allFiles($path2);
      foreach ($photos as $photo){
        array_push($th,(string) $photo);
      }
    }

    return view('psb.photos', compact('id','th'));
  }

  public function uploadSave(Request $request, $id){
    // dd('tes');
    $files = $request->file('photo');
    $path = public_path().'/upload/maintenance/'.$id.'/';
    $th = public_path().'/upload/maintenance/'.$id.'th/';
    if (!file_exists($path)) {
      if (!mkdir($path, 0755, true))
        return 'gagal menyiapkan folder foto evidence';
    }
    if (!file_exists($th)) {
      if (!mkdir($th, 0755, true))
        return 'gagal menyiapkan folder foto evidence';
    }
    foreach ($files as $file) {
        $name = $file->getClientOriginalName();
        try {
          $moved = $file->move("$path", "$name");
          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$th/$name");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence '.$name;
        }
    }
    return redirect('/wo/maintenance')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Upload Photo']
    ]);
  }

  public function ubahStatusLaporan($ndem){
      $dispatchTeknisi = DB::table('dispatch_teknisi')->where('ndem','=',$ndem)->first();

      DB::table('dispatch_teknisi')
          ->where('ndem','=',$ndem)
          ->update([
              'step_id'   => '1.0'
          ]);

      DB::table('psb_laporan')
          ->where('id_tbl_mj','=',$dispatchTeknisi->id)
          ->update([
              'catatan'         => '',
              'status_laporan'  => '6'
          ]);
  }

  public function valTot($date, $jam){
      $dataVal = DB::SELECT("SELECT *, count(sto) as jumlah from kpro_pi WHERE sts='val' AND witel='BANJARMASIN' AND ts LIKE '%".$date."%' group by sto");

      $jmlData = DB::select("SELECT Count(*) as total from kpro_pi WHERE sts='val' AND witel='BANJARMASIN' AND ts LIKE '%".$date."%'");

      $dataTot = DB::select("SELECT *, SUM(case when status_resume like '%issued%' then 1 else 0 end) as issued, SUM(case when status_resume like '%fallout%' then 1 else 0 end) as fallout  from kpro_pi WHERE sts='tot' AND witel='BANJARMASIN' AND ts LIKE '%".$date.' '.$jam."%' group by sto");

      $jml = DB::select("SELECT *, SUM(case when status_resume like '%issued%' then 1 else 0 end) as issued, SUM(case when status_resume like '%fallout%' then 1 else 0 end) as fallout  from kpro_pi WHERE sts='tot' AND witel='BANJARMASIN' AND ts LIKE '%".$date.' '.$jam."%'");

      return view('laporan.kproval',compact('dataVal','jmlData', 'dataTot', 'jml', 'date', 'jam' ));
  }

  public function detVal($date, $jam, $sto){
      $dataVal = DB::SELECT("SELECT * from kpro_pi WHERE sts='val' AND witel='BANJARMASIN' AND sto='".$sto."' AND ts LIKE '%".$date."%'");

      return view('laporan.kprovalDet',compact('dataVal'));
  }

  public function detTot($date, $jns, $sto, $jam){
      $dataVal = DB::SELECT("SELECT * from kpro_pi WHERE sts='tot' AND witel='BANJARMASIN' AND status_resume like '%".$jns."%' AND sto='".$sto."' AND ts LIKE '%".$date.' '.$jam."%'");

      return view('laporan.kprototDet',compact('dataVal'));
  }

  public function livesearchSC(Request $req){
      $term = trim($req->q);
      if (empty($term)) {
          return \Response::json([]);
      }

      $fetchData = psbmodel::value_search($term);

      $formatted_tags = [];

      foreach ($fetchData as $row) {
          $formatted_tags[] = ["SC"=> $row->orderId];
      }

      return \Response::json($formatted_tags);
  }

   public function inputSebelumRfc($id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];

      // TODO: order by most used
      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item,
          COALESCE(m.qty, 0) AS qty
        FROM item i
        LEFT JOIN
          psb_laporan_material m
          ON i.id_item = m.id_item
          AND m.psb_laporan_id = ?
        WHERE
         i.grup = 1
        ORDER BY id_item
      ', [
        $data->id
      ]);

      // tes ja
      // dd($data->id);

      if ($auth->level==2){
        //   $materials = DB::select('
        //   SELECT
        //     i.id_item, i.nama_item,
        //     COALESCE(i.jmlTerpakai, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
        //   FROM rfc_sal i
        //   where
        //     i.psb_laporan_id = ?
        //   ORDER BY i.rfc
        // ', [
        //   $data->id
        // ]);

        // $materials = DB::select('
        //   SELECT
        //     i.id_item, i.nama_item,
        //     COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
        //   FROM rfc_sal i
        //   LEFT JOIN
        //     psb_laporan_material m
        //     ON i.id_item_bantu = m.id_item_bantu
        //   Where
        //     m.psb_laporan_id = ?
        //   ORDER BY i.id_item
        // ', [
        //   $data->id
        // ]);

      }
      else{
        $materials = DB::select('
          SELECT
            i.id_item, i.nama_item,
            COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
          FROM rfc_sal i
          LEFT JOIN
            psb_laporan_material m
            ON i.id_item_bantu = m.id_item_bantu
            AND m.psb_laporan_id = ?
          WHERE
           i.nik="'.$auth->id_user.'"
          ORDER BY id_item
        ', [
          $data->id
        ]);
      }
    }
    else {
      $data = new \StdClass;
      $data->id = null;
      $data->typeont = null;
      $data->snont = null;
      $data->typestb = null;
      $data->snstb = null;
      // TODO: order by most used

      // $materials = DB::select('
      //   SELECT a.id_item, a.nama_item, 0 AS qty
      //   FROM item a left join psb_laporan_material b ON a.id_item=b.id_item WHERE a.grup = 1
      //   GROUP BY a.id_item
      //   ORDER BY a.id_item
      // ');

      $materials = DB::select('
        SELECT id_item, nama_item, 0 AS qty, (jumlah - jmlTerpakai) as saldo, rfc
        FROM rfc_sal where nik="'.$auth->id_user.'" AND jumlah<>0
        ORDER BY rfc
      ');

/*
      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item,
          COALESCE(m.qty, 0) AS qty
        FROM item i
        LEFT JOIN
          psb_laporan_material m
          ON i.id_item = m.id_item
          AND m.psb_laporan_id = ?
        WHERE 1
        ORDER BY id_item
      ', [
        $data->id
      ]);
*/
    }
    $nte1 = DB::select('select i.id as id, i.sn as text
        FROM nte i
        LEFT JOIN
          psb_laporan_material m
          ON i.id = m.id_item
          AND m.psb_laporan_id = ?
        WHERE m.type=2
        ORDER BY id_item',[
        $data->id
      ]);
    $no = 1;
    $ntes = '';
    foreach($nte1 as $n){
      if($no == 1){
        $ntes .= $n->id;
        $no = 2;
      }else{
        $ntes .= ', '.$n->id;
      }
    }
    $nte = DB::select('select  id as id, sn as text from nte
      where position_key = ?',[
        $auth->id_karyawan
      ]);
    $project = DB::select('
      SELECT
        *,
        ms2n.*,
        d.id as id_dt,
        m.ND as mND,
        m.ND_Speedy as mND_Speedy,
        m.mdf as mMDF,
        m.Kcontact as mKcontact,
        m.Nama as mNama,
        m.Alamat as mAlamat,
        c.ndemPots,
        c.ndemSpeedy,
        c.Kcontact as KcontactSC,
        c.jenisPsb,
        c.sto,
        e.id as id_dshr,
        dm.*,
        ne.STO as neSTO,
        ne.RK as neRK,
        ne.ND_TELP as neND_TELP,
        ne.ND_INT as neND_INT,
        ne.PRODUK_GGN as nePRODUK_GGN,
        ne.HEADLINE as neHEADLINE,
        ne.LOKER_DISPATCH as neLOKER_DISPATCH,
        ne.KANDATEL as neKANDATEL,
        c.lon,c.lat
      FROM dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick c ON d.Ndem = c.orderId
      LEFT JOIN ms2n USING (Ndem)
      LEFT JOIN roc on d.Ndem = roc.no_tiket
      LEFT JOIN nonatero_excel_bank ne ON d.Ndem = ne.TROUBLE_NO
      LEFT JOIN dossier_master dm ON d.Ndem = dm.ND
      LEFT JOIN micc_wo m on d.Ndem = m.ND
      LEFT JOIN dshr e ON c.orderKontak = e.pic
      LEFT JOIN step_tracker st ON st.step_tracker_id = d.step_id
      LEFT JOIN psb_laporan  pl ON d.id=pl.id_tbl_mj
      WHERE
      d.id = ?
    ',[
      $id
    ])[0];

    // error step_id
    if (empty($project->step_next)){

        // cek psb_laporan_log
        $dispatch = DB::table('dispatch_teknisi')
                    ->where('id','=',$id)
                    ->first();

        $psbLog = DB::table('psb_laporan_log')
                    ->where('ndem','=',$dispatch->Ndem)
                    ->orderBy('psb_laporan_log_id','desc')
                    ->first();

        if (empty($psbLog->Ndem)){
            $stepId = '1.0';
        }
        else {
            $psbStatus = DB::table('psb_laporan_status')
                            ->where('laporan_status_id','=',$psbLog->status_laporan)
                            ->first();

            $stepId = $psbStatus->step_id;
        };

        DB::table('dispatch_teknisi')
            ->where('id',$id)
            ->update([
                'step_id'   => $stepId
            ]);

        // code untuk mengisi label
        $dataStep = DB::table('step_tracker')
                      ->where('step_tracker_id','=',$stepId)
                      ->first();

        $step_id   = $dataStep->step_tracker_id;
        $step_next = $dataStep->step_next;
        $step_name = $dataStep->step_name;
    }
    else {
        $step_id   = $project->step_id;
        $step_next = $project->step_next;
        $step_name = $project->step_name;
    }

    if ($step_next=='1.5'){
        $step_next = '1.4';
    }
    else{
      $step_next = $step_next;
    };
    // aslinya
    // $project->step_next

    $photoInputs = $this->photoInputs;
    $dispatch_teknisi_id = $id;
    $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
    $getTambahTiang     = psbmodel::getTambahTiang();

    return view('psb.input', compact('data', 'project', 'materials', 'photoInputs', 'ntes', 'nte', 'nte1', 'dispatch_teknisi_id','get_laporan_status','step_id', 'step_name', 'getTambahTiang'));
  }

  public function listWoTeknisi(){
      $bulan = Date('Y-m');
      $auth = session('auth');
      $list = PsbModel::WorkOrder('NEED_PROGRESS', $bulan);

      return view('psb.listWoTek', compact('list'));
  }

  private function handleFileUpload2($request, $id){
    foreach($this->photoInputs as $name) {
      $flag  = 'flag_'.$name;
      $input = 'photo-'.$name;
      // if ($request->$flag==1){
          if ($request->hasFile($input)) {
            $table = DB::table('hdd')->get()->first();
            $hdd = $table->active_hdd;
            $upload = $table->public;

            $path = public_path().'/'.$upload.'/evidence/'.$id.'/';
            if (!file_exists($path)) {
              if (!mkdir($path, 0770, true))
                return 'gagal menyiapkan folder foto evidence';
            }
            $file = $request->file($input);
            // $ext = $file->guessExtension();
            $ext = 'jpg';
            //TODO: path, move, resize
            try {
              $moved = $file->move("$path", "$name.$ext");

              $img = new \Imagick($moved->getRealPath());

              $img->scaleImage(100, 150, true);
              $img->writeImage("$path/$name-th.$ext");
            }
            catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
              return 'gagal menyimpan foto evidence '.$name;
            }
          }
      // }
    }
  }

  public function kirimSatuKpro($ndem){
      $this->sendtobotkpro($ndem);
      return back()->with('alerts',[['type' => 'success', 'text' => 'Kirim Ke Bot K-Pro Sukses']]);
  }

  public function kirimKoordinatMassalForm(){
      return view('psb.koordinatMasalForm');
  }

  public function kirimKoordinatMassalKirim(Request $req){
      if (empty($req->q)){
          return back()->with('alerts',[['type' => 'danger', 'text' => 'Jangan Kosong Mang !']]);
      }

      $datas = explode(' ', $req->q);
      foreach ($datas as $data){
          $id = $data;
          $get_data = DB::SELECT('
            SELECT
              a.*,
              b.mdf_grup_id
            FROM
              Data_Pelanggan_Starclick a
              LEFT JOIN mdf b ON a.sto = b.mdf
            WHERE a.orderId = ?
          ',[ $id ]);

          $get_data = $get_data[0];

          $chatID = "-374194073";
          // $chatID = "192915232";

          if ($get_data->lat<>NULL){
            Telegram::sendLocation([
              'chat_id' => $chatID,
              'latitude' => $get_data->lat,
              'longitude' => $get_data->lon
            ]);

            Telegram::sendMessage([
              'chat_id' => $chatID,
              'text' => $get_data->orderId
            ]);
          }
      }

      return back()->with('alerts',[['type' => 'success', 'text' => 'Berhasil Ngirim, Sabar Nang Lah Jangan Dikajut 1000 kalo Garing Server ']]);
  }

  public function kirimFotoKpro($date){
    $get_sc = DB::SELECT('
      SELECT *,dt.id as id_dt FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.id IS NOT NULL AND a.orderDatePs LIKE "'.$date.'%"
    ');

    foreach ($get_sc as $sc){
      $id = $sc->id;
      $wo = $sc->orderId;
      $photoInputs = $this->photokpro;
      for($i=0;$i<count($photoInputs);$i++) {
        $file = public_path()."/upload3/evidence/".$id."/".$photoInputs[$i].".jpg";
        if (file_exists($file)){
          Telegram::sendPhoto([
            'chat_id' => "-327333991",
            'caption' => $wo,
            'photo'   => $file
          ]);
        }
      };
    }
    echo "sukses ".$date;
  }

  public function kirimFotoKproScForm(){
      return view('psb.fotoKproMasalForm');
  }

  public function kirimFotoKproScKirim(Request $req){
      if (empty($req->q)){
          return back()->with('alerts',[['type' => 'danger', 'text' => 'Jangan Kosong Mang !']]);
      }

      $datas = explode(' ', $req->q);
      foreach ($datas as $data){
          $id = $data;
          $get_data = DB::SELECT('
            SELECT
              *,
              dt.id as id_dt
            FROM
              Data_Pelanggan_Starclick a
              LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem
            WHERE dt.id IS NOT NULL AND
              a.orderId = ?
          ',[ $id ]);

          $get_data = $get_data[0];

          $chatID = "-327333991";
          // $chatID = "192915232";

          $id = $get_data->id;
          $wo = $get_data->orderId;
          $photoInputs = $this->photokpro;
          for($i=0;$i<count($photoInputs);$i++) {
            $file = public_path()."/upload3/evidence/".$id."/".$photoInputs[$i].".jpg";
            if (file_exists($file)){
              Telegram::sendPhoto([
                'chat_id' => $chatID,
                'caption' => $wo,
                'photo'   => $file
              ]);
            }
          };
      }

      return back()->with('alerts',[['type' => 'success', 'text' => 'Berhasil Ngirim, Sabar Nang Lah Jangan Dikajut 1000 kalo Garing Server Foto Pang ini']]);
  }

  function encrypt($id){
      // echo "enkrip : ".$id."<br />";
      $en1 = str_replace('1','K',$id);
      $en2 = str_replace('2','a',$en1);
      $en3 = str_replace('3','N',$en2);
      $en4 = str_replace('4','t',$en3);
      $en5 = str_replace('5','u',$en4);
      $en6 = str_replace('6','r',$en5);
      $en7 = str_replace('7','J',$en6);
      $en8 = str_replace('8','f',$en7);
      $en9 = str_replace('9','X',$en8);
      $en0 = str_replace('0','Y',$en9);
      return $en0;
  }

  public function tes(){
      $data = PsbModel::workOrderProvNeedProgress();
      dd($data);
  }

   public function reboundaryList(Request $req, $tgl){
      $data = psbmodel::listReboundary($tgl);

      if ($req->has('q')){
          $cari = $req->input('q');
          $data = psbmodel::listReboundaryCari($cari);
      }

      return view('reboundary.list',compact('data'));
  }

  public function inputReboundary(){
      $dataSektor = DB::SELECT('
        SELECT
        a.chat_id as id,
        a.title as text
        FROM
          group_telegram a
      ');

      return view('reboundary.input',compact('dataSektor'));
  }

  public function simpanReboundary(Request $req){
      $this->validate($req,[
          'scid'            => 'required',
          'scid'            => 'numeric',
          'koorPel'         => 'required',
          'odpLama'         => 'required',
          'koorOdpLama'     => 'required',
          'odpBaru'         => 'required',
          'koorOdpBaru'     => 'required',
          'tarikanLama'     => 'required',
          'tarikanLama'     => 'numeric',
          'tarikanEstimasi' => 'required',
          'tarikanEstimasi' => 'numeric',
          'catatan'         => 'required',
          'sektor'          => 'required'

      ],[
          'scid.required'     => 'SCID Diisi',
          'scid.numeric'      => 'Angka Ja',
          'koorPel.required'  => 'Koordinat Pelanggan Jangan Kosong',
          'odpLama.required'  => 'ODP Lama Diisi',
          'koorOdpLama.required'  => 'Koordinat ODP Lama Diisi',
          'odpBaru.required'      => 'ODP Baru Diisi',
          'koorOdpBaru.required'  => 'koordinat ODP Baru DIisi',
          'tarikanLama.required'  => 'Tarikan Lama Diisi',
          'tarikanLama.numeric'   => 'Angka Ja',
          'tarikanEstimasi.required'  => 'Tarikan Estimasi Diisi',
          'tarikanEstimasi.numeric'   => 'Angka Ja',
          'catatan.required'          => 'Catatan Diisi',
          'sektor.required'           => 'Sektor Diisi'
      ]);

      // cek data reboundary kalo sudah ada
      $data = PsbModel::getDataReboundaryByScid($req->scid);
      if ($data){
          return back()->with('alerts',[['type' => 'danger', 'text' => 'Data Sudah Ada Men !']]);
      }
      else{
        $exists = PsbModel::cekSCInDataStarclick($req->scid);
        if ($exists){
            $nik = session('auth')->id_user;
            PsbModel::simpanReboundary($req,$nik,1);

            return redirect('/reboundary/list/'.date('Y-m-d'))->with('alerts',[['type' => 'success', 'text' => 'Data Berhasil Disimpan']]);
        }
        else{
            return back()->with('alerts',[['type' => 'danger', 'text' => 'SCID Tidak VALID !!!']]);
        }
      }

  }

  public function editReboundary($id){
      $data = PsbModel::getDataReboundaryById($id);
      $dataSektor = DB::SELECT('
        SELECT
        a.chat_id as id,
        a.title as text
        FROM
          group_telegram a
      ');

      return view('reboundary.edit',compact('data', 'dataSektor'));
  }

  public function editSimpanReboundary(Request $req, $id){
      $this->validate($req,[
          'scid'            => 'required',
          'scid'            => 'numeric',
          'koorPel'         => 'required',
          'odpLama'         => 'required',
          'koorOdpLama'     => 'required',
          'odpBaru'         => 'required',
          'koorOdpBaru'     => 'required',
          'tarikanLama'     => 'required',
          'tarikanLama'     => 'numeric',
          'tarikanEstimasi' => 'required',
          'tarikanEstimasi' => 'numeric',
          'catatan'         => 'required',
          'sektor'          => 'required'

      ],[
          'scid.required'     => 'SCID Diisi',
          'scid.numeric'      => 'Angka Ja',
          'koorPel.required'  => 'Koordinat Pelanggan Jangan Kosong',
          'odpLama.required'  => 'ODP Lama Diisi',
          'koorOdpLama.required'  => 'Koordinat ODP Lama Diisi',
          'odpBaru.required'      => 'ODP Baru Diisi',
          'koorOdpBaru.required'  => 'koordinat ODP Baru DIisi',
          'tarikanLama.required'  => 'Tarikan Lama Diisi',
          'tarikanLama.numeric'   => 'Angka Ja',
          'tarikanEstimasi.required'  => 'Tarikan Estimasi Diisi',
          'tarikanEstimasi.numeric'   => 'Angka Ja',
          'catatan.required'          => 'Catatan Diisi',
          'sektor.required'           => 'Sektor Diisi'
      ]);


      $nik = session('auth')->id_user;
      PsbModel::simpanReboundary($req,$nik,2,$id);

      return redirect('/reboundary/list/'.date('Y-m-d'))->with('alerts',[['type' => 'success', 'text' => 'Data Berhasil Disimpan']]);

  }

  public function dispatchReboundary($scid, $status){
      $getData = PsbModel::getDataReboundaryByScid($scid);
      $tiket   = '6'.date('m').$getData->scid;

      $regu = DB::select('
        SELECT a.id_regu as id,a.uraian as text, b.title
        FROM regu a
          LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
        WHERE
          ACTIVE = 1
      ');

      $dataSektor = DB::SELECT('
        SELECT
        a.chat_id as id,
        a.title as text
        FROM
          group_telegram a
      ');


      return view('reboundary.dispatch',compact('getData', 'tiket', 'regu', 'dataSektor', 'status'));
  }

  public function simpanDispatchReboundary(Request $req){
    $this->validate($req,[
        'tglOpen' => 'required',
        'jamOpen' => 'required',
        'id_regu' => 'required',
        'tgl'     => 'required'
    ],[
        'tglOpen.required'    => 'Tgl Open Disii',
        'jamOpen.required'    => 'Jam Open Diisi',
        'id_regu.required'    => 'Regu Diisi',
        'tgl.required'        => 'Tanggal Dispath Diisi'
    ]);


   // insert into log status
   $auth = session('auth');
   $status_laporan = 6;

   DB::transaction(function() use($req, $auth, $status_laporan) {
       DB::table('psb_laporan_log')->insert([
         'created_at'      => DB::raw('NOW()'),
         'created_by'      => $auth->id_karyawan,
         'status_laporan'  => $status_laporan,
         'id_regu'         => $req->input('id_regu'),
         'catatan'         => "DISPATCH at ".date('Y-m-d'),
         'Ndem'            => $req->noTiketAsli
       ]);
   });

    DB::transaction(function() use($req, $auth) {
       DB::table('dispatch_teknisi_log')->insert([
         'updated_at'          => DB::raw('NOW()'),
         'updated_by'          => $auth->id_karyawan,
         'tgl'                 => $req->input('tgl'),
         'id_regu'              => $req->input('id_regu'),
         'Ndem'                => $req->noTiketAsli
       ]);
    });

    $now = $req->tglOpen.' '.$req->jamOpen.':00';
    $openTanggal = date('Y-m-d H:i:s',strtotime($now));

    $auth = session('auth');
    $check = DB::table('roc')
              ->where('no_tiket',$req->input('noTiketAsli'))
              ->first();

    if (count($check)>0) {
      DB::table('roc')->where('no_tiket',$req->input('no_tiket'))->update([
        'no_tiket' => $req->input('noTiketAsli'),
        'loker_ta' => $req->input('loker'),
        'alpro'    => $req->input('alpro'),
        'tglOpen'  => $openTanggal,
        'loker_ta' => 6,
        'refSc'    => $req->refSc
      ]);
    } else {
      DB::table('roc')->insert([
        'no_tiket' => $req->input('noTiketAsli'),
        'loker_ta' => $req->input('loker'),
        'alpro'    => $req->input('alpro'),
        'tglOpen'  => $openTanggal,
        'loker_ta' => 6,
        'refSc'    => $req->refSc
      ]);
    }

    // redispatc dan dispatch
    $exists = DB::select('
        SELECT a.*,b.status_laporan, c.mainsector
          FROM dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN regu c ON a.id_regu = c.id_regu
        WHERE a.Ndem = ?
      ',[
        $req->noTiketAsli
      ]);

      $status_laporan = 6;

      // reset status if redispatch
      if (count($exists)) {
        $data = $exists[0];
        DB::transaction(function() use($req, $data, $auth, $status_laporan) {
          DB::table('psb_laporan')
            ->where('id_tbl_mj', $data->id)
            ->update([
              'status_laporan' => $status_laporan
            ]);
            DB::table('psb_laporan_log')->insert([
               'created_at'      => DB::raw('NOW()'),
               'created_by'      => $auth->id_karyawan,
               'status_laporan'  => $status_laporan,
               'id_regu'         => $req->input('id_regu'),
               'catatan'         => "REDISPATCH",
               'Ndem'            => $req->noTiketAsli,
               'psb_laporan_id'  => $data->id
             ]);
        });

        DB::transaction(function() use($req, $data, $auth) {
          DB::table('dispatch_teknisi')
            ->where('id', $data->id)
            ->update([
              'updated_at'            => DB::raw('NOW()'),
              'updated_by'            => $auth->id_karyawan,
              'tgl'                   => $req->input('tgl'),
              'id_regu'               => $req->input('id_regu'),
              'step_id'               => "1.0"
            ]);
        });
      }
      else {
        DB::transaction(function() use($req, $auth) {
          DB::table('dispatch_teknisi')->insert([
            'updated_at'          => DB::raw('NOW()'),
            'updated_by'          => $auth->id_karyawan,
            'tgl'                 => $req->input('tgl'),
            'id_regu'             => $req->input('id_regu'),
            'created_at'          => DB::raw('NOW()'),
            'Ndem'                => $req->noTiketAsli,
            'step_id'             => "1.0",
            'dispatch_by'         => 6
          ]);
        });

        DB::table('reboundary_survey')->where('scid',$req->refSc)->where('dispatch',0)->update(['dispatch' => 1]);
      }

      PsbModel::updateNoTiket($req->refSc, $req->noTiketAsli);

    return redirect('/reboundary/list/'.date('Y-m-d'))->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> men-dispatch WO Reboundary']
      ]);
  }

  public function inputLaporanReboundary($id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];
      if ($auth->level==2){

        $materials = DB::select('
          SELECT
            i.id_item, i.nama_item,
            COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
          FROM rfc_sal i
          LEFT JOIN
            psb_laporan_material m
            ON i.id_item_bantu = m.id_item_bantu
          Where
            m.psb_laporan_id = ?
          ORDER BY i.rfc asc, i.id_item asc
        ', [
          $data->id
        ]);

        if (empty($materials)){
              $materials = DB::select('
                SELECT * FROM psb_laporan_material where type = 1 AND psb_laporan_id = ?
              ', [
                $data->id
              ]);
        }
      }
      else{
        $materials = DB::select('
          SELECT
            i.id_item, i.nama_item,
            COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
          FROM rfc_sal i
          LEFT JOIN
            psb_laporan_material m
            ON i.id_item_bantu = m.id_item_bantu
            AND m.psb_laporan_id = ?
          WHERE
           i.nik="'.$auth->id_user.'"
          ORDER BY i.rfc asc, i.id_item asc
        ', [
          $data->id
        ]);
      }
    }
    else {
      $data = new \StdClass;
      $data->id = null;
      $data->typeont = null;
      $data->snont = null;
      $data->typestb = null;
      $data->snstb = null;


      $materials = DB::select('
        SELECT id_item, nama_item, 0 AS qty, (jumlah - jmlTerpakai) as saldo, rfc
        FROM rfc_sal where nik="'.$auth->id_user.'" AND jumlah<>0
        ORDER BY rfc
      ');
    }

    $nte1 = DB::select('select i.id as id, i.sn as text
        FROM nte i
        LEFT JOIN
          psb_laporan_material m
          ON i.id = m.id_item
          AND m.psb_laporan_id = ?
        WHERE m.type=2
        ORDER BY id_item',[
        $data->id
      ]);
    $no = 1;
    $ntes = '';
    foreach($nte1 as $n){
      if($no == 1){
        $ntes .= $n->id;
        $no = 2;
      }else{
        $ntes .= ', '.$n->id;
      }
    }
    $nte = DB::select('select  id as id, sn as text from nte
      where position_key = ?',[
        $auth->id_karyawan
      ]);
    $project = DB::select('
      SELECT
        *,
        ms2n.*,
        d.id as id_dt,
        m.ND as mND,
        m.ND_Speedy as mND_Speedy,
        m.mdf as mMDF,
        m.Kcontact as mKcontact,
        m.Nama as mNama,
        m.Alamat as mAlamat,
        c.ndemPots,
        c.ndemSpeedy,
        c.Kcontact as KcontactSC,
        c.jenisPsb,
        c.sto,
        e.id as id_dshr,
        dm.*,
        ne.STO as neSTO,
        ne.RK as neRK,
        ne.ND_TELP as neND_TELP,
        ne.ND_INT as neND_INT,
        ne.PRODUK_GGN as nePRODUK_GGN,
        ne.HEADLINE as neHEADLINE,
        ne.LOKER_DISPATCH as neLOKER_DISPATCH,
        ne.KANDATEL as neKANDATEL,
        c.lon,c.lat,
        d.jenis_layanan,
        my.*,
        pl.*
      FROM dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick c ON d.Ndem = c.orderId
      LEFT JOIN ms2n USING (Ndem)
      LEFT JOIN reboundary_survey rs ON d.Ndem = rs.no_tiket_reboundary
      LEFT JOIN nonatero_excel_bank ne ON d.Ndem = ne.TROUBLE_NO
      LEFT JOIN dossier_master dm ON d.Ndem = dm.ND
      LEFT JOIN micc_wo m on d.Ndem = m.ND
      LEFT JOIN dshr e ON c.orderKontak = e.pic
      LEFT JOIN step_tracker st ON st.step_tracker_id = d.step_id
      LEFT JOIN psb_laporan  pl ON d.id=pl.id_tbl_mj
      LEFT JOIN psb_myir_wo my ON d.Ndem=my.myir
      WHERE
      d.id = ?
    ',[
      $id
    ])[0];


    // error step_id
    if (empty($project->step_next)){

        // cek psb_laporan_log
        $dispatch = DB::table('dispatch_teknisi')
                    ->where('id','=',$id)
                    ->first();

        $psbLog = DB::table('psb_laporan_log')
                    ->where('ndem','=',$dispatch->Ndem)
                    ->orderBy('psb_laporan_log_id','desc')
                    ->first();

        if (empty($psbLog->Ndem)){
            $stepId = '1.0';
        }
        else {
            $psbStatus = DB::table('psb_laporan_status')
                            ->where('laporan_status_id','=',$psbLog->status_laporan)
                            ->first();

            $stepId = $psbStatus->step_id;
        };

        DB::table('dispatch_teknisi')
            ->where('id',$id)
            ->update([
                'step_id'   => $stepId
            ]);

        // code untuk mengisi label
        $dataStep = DB::table('step_tracker')
                      ->where('step_tracker_id','=',$stepId)
                      ->first();

        $step_id   = $dataStep->step_tracker_id;
        $step_next = $dataStep->step_next;
        $step_name = $dataStep->step_name;
    }
    else {
        $step_id   = $project->step_id;
        $step_next = $project->step_next;
        $step_name = $project->step_name;
    }

    if ($step_next=='1.5'){
        $step_next = '1.4';
    }
    else{
      $step_next = $step_next;
    };
    // aslinya
    // $project->step_next
    $get_Ndem = DB::table('dispatch_teknisi')
                  ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                  ->select('psb_laporan.id','psb_laporan.id_tbl_mj','dispatch_teknisi.Ndem', 'dispatch_teknisi.dispatch_by')
                  ->where('psb_laporan.id_tbl_mj',$id)
                  ->first();

    if (count($get_Ndem)==0){
        $get_Ndem = DB::table('dispatch_teknisi')->where('id',$id)->first();
        $cari = 'a.Ndem = "'.$get_Ndem->Ndem.'" AND';
    }
    else{
        $ndemIn  = DB::table('psb_laporan_log')->where('psb_laporan_id',$get_Ndem->id)->groupBy('Ndem')->get();
        $ndemNew = '';
        if (count($ndemIn)<>0){
            if (count($ndemIn)==1){
                $ndemNew  = "'".$ndemIn[0]->Ndem."'";
                $ndemMyir = DB::table('psb_myir_wo')->where('sc',$ndemIn[0]->Ndem)->first();
                if (count($ndemMyir)<>0){
                    $ndemNew  .= ",'".$ndemMyir->myir."'";
                };
            }
            else{
              foreach($ndemIn as $ndem){
                  $ndemNew .= "'".$ndem->Ndem."'".',';
              }

              $ndemNew = substr($ndemNew, 0, -1);
            }

            $ndemNew = '('.$ndemNew.')';
            $cari = '(a.psb_laporan_id = "'.$get_Ndem->id.'" OR a.Ndem in '.$ndemNew.') AND';
        }
        else{
            $ndemMyir = DB::table('psb_myir_wo')->where('sc',$get_Ndem->Ndem)->first();
            // $cari = 'a.psb_laporan_id = "'.$get_Ndem->id.'" AND';
            if (count($ndemMyir)==0){
                $cari = 'a.Ndem = "'.$get_Ndem->Ndem.'" AND';
            }
            else{
                $cari = '(a.psb_laporan_id = "'.$get_Ndem->id.'" OR a.Ndem ="'.$ndemMyir->myir.'") AND';
            }

        }
        // dd($cari);
    };

    $data_log = DB::SELECT('
                SELECT
                *
                FROM psb_laporan_log a
                LEFT JOIN user b ON a.created_by = b.id_user
                LEFT JOIN 1_2_employee c ON b.id_karyawan = c.nik
                LEFT JOIN psb_laporan_status d ON a.status_laporan = d.laporan_status_id
                WHERE
                  '.$cari.'
                  a.created_by <> 0
                ORDER BY created_at DESC
              ');

    $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');

    $photoInputs = $this->photoReboundary;

    $dispatch_teknisi_id = $id;
    // $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
    $getTambahTiang     = psbmodel::getTambahTiang();

    // data reboundary
    $dataReboundary = '';
    $dataDispatch = DB::table('dispatch_teknisi')->where('id',$id)->first();
    if ($dataDispatch->dispatch_by==6){
        $dataReboundary = DB::table('dispatch_teknisi')
                            ->leftJoin('reboundary_survey','dispatch_teknisi.Ndem','=','reboundary_survey.no_tiket_reboundary')
                            ->leftJoin('Data_Pelanggan_Starclick','reboundary_survey.scid','=','Data_Pelanggan_Starclick.orderId')
                            ->where('dispatch_teknisi.Ndem',$dataDispatch->Ndem)
                            ->first();
    };

    return view('psb.inputReboundary', compact('data', 'project', 'materials', 'photoInputs', 'ntes', 'nte', 'nte1', 'dispatch_teknisi_id','get_laporan_status','step_id', 'step_name', 'getTambahTiang', 'data_log', 'dataReboundary'));
  }

  public function simpanLaporanReboundary(Request $request, $id){
    $upload = $this->handleFileUploadReboundary($request, $id);
    if (@$upload['type']=="danger")
    return back()->with('alerts', [
      ['type' => 'danger', 'text' => $upload['message']]
    ]);

    $disp = DB::table('dispatch_teknisi')
      ->select('dispatch_teknisi.*', 'Data_Pelanggan_Starclick.jenisPsb','step_tracker.*')
      ->leftJoin('Data_Pelanggan_Starclick', 'dispatch_teknisi.Ndem', '=', 'Data_Pelanggan_Starclick.orderId')
      ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
      ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
      ->leftJoin('step_tracker', 'psb_laporan_status.step_id', '=', 'step_tracker.step_tracker_id')
      ->where('dispatch_teknisi.id', $id)
      ->first();

    // $this->handleFileUpload($request, $id);
    $auth = session('auth');
    $input = $request->only([
      'status'
    ]);
    $rules = array(
      'status' => 'required'
    );
    $messages = [
      'status.required'  => 'Silahkan isi kolom "Status" Bang!',
      'sn_ont.required'  => 'Silahkan isi kolom "SN ONT" Bang!',
      'catatan.required' => 'Silahkan isi kolom "Catatan" Bang!',
      'kordinat_pelanggan.required' => 'Silahkan isi kolom "kordinat_pelanggan" Bang!',
      'nama_odp.required' => 'Silahkan isi kolom "nama_odp" Bang!',
      'kordinat_odp.required' => 'Silahkan isi kolom "kordinat_odp" Bang!',
      'noPelangganAktif.required'     => 'Silahkan Isi "No. Hp Pelanggan Aktif !',
      'noPelangganAktif.numeric'      => 'Inputan Hanya Boleh Angka',
      'redaman.required'              => 'Redaman Jangan Dikosongi !',
      'flag_Lokasi_Pelanggan.required'          => 'Uploaad Foto "Lokasi Pelanggan" Bang !',
      'flag_Redaman_Ont.required'               => 'Uploaad Foto "Redaman ONT" Bang !',
      'flag_Berita_Acara.required'              => 'Uploaad Foto "Berita Acara" Bang !',
      'flag_ODP_Baru.required'                  => 'Uploaad Foto "ODP Baru" Bang !',
      'flag_ODP_Lama.required'                  => 'Uploaad Foto "ODP Lama" Bang !',
      'flag_Label.required'                     => 'Uploaad Foto "LABEL" Bang !',
      'flag_Sn_Ont_Baru.required'               => 'Uploaad Foto "SN ONT Baru" Bang !',
      'flag_Sn_Ont_Lama.required'               => 'Uploaad Foto "SN ONT Lama" Bang !',
      'flag_Port_Odp_Lama.required'             => 'Uploaad Foto "Port ODP Lama" Bang !',
      'flag_Action_Penurunan_Dropcore.required' => 'Uploaad Foto "Action Penurunan Dropcore" Bang !',
      'flag_Ex_Dropcore.required'               => 'Uploaad Foto "Ex Dropcore" Bang !',
      'flag_Speedtest.required'                 => 'Uploaad Foto "Speedtest" Bang !',
      'flag_Test_Live_TV.required'              => 'Uploaad Foto "Test Live Tv" Bang !'
  ];

    $validator = Validator::make($request->all(), $rules, $messages);
    $a = $request->input('status');

    if($a == 1 || $a == 2 || $a == 4 || $a == 10 || $a == 11 || $a == 25 || $a == 24 || $a == 34 || $a == 42) {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('kordinat_odp', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('nama_odp', 'required', function ($input) {
            return true;
        });
    };


    if($a <> 6 && $a <> 28 && $a <> 29 && $a <> 5 && $a <> 52 && $a <> 57 && $a <> 58 && $a <> 59 && $a <> 60 ){
      $validator->sometimes('catatan', 'required', function ($input) {
        return true;
      });

      $validator->sometimes('noPelangganAktif', 'required', function ($input) {
        return true;
      });

      $validator->sometimes('noPelangganAktif', 'numeric', function ($input) {
        return true;
      });
    };

    if ($a==24){
        $validator->sometimes('redaman', 'required', function ($input) {
          return true;
        });
    };

    if ($a==1 || $a==38 || $a==37){
        $validator->sometimes('flag_Lokasi_Pelanggan', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Redaman_Ont', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_ODP_Baru', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_ODP_Lama', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Label', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Sn_Ont_Baru', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Sn_Ont_Lama', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Port_Odp_Lama', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Action_Penurunan_Dropcore', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Ex_Dropcore', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Speedtest', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Test_Live_TV', 'required', function ($input) {
            return true;
        });
    };

    if ($validator->fails()) {
            return redirect()->back()
                ->withInput($request->input())
                        ->withErrors($validator)
                        ->with('alerts', [
                          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan']
                        ]);
    };

    if ($request->kordinat_pelanggan=="ERROR" || $request->kordinat_pelanggan=="Harap Tunggu..."){
        return back()->with('alerts',[['type' => 'danger', 'text' => 'Kordinat Pelanggan Salah !. Hidupkan GPS Anda..']]);
    };

    $materials = json_decode($request->input('materials'));
    // if ($a==1 || $a==37 || $a==38){
    //     if (count($materials)==''){
    //         return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Inputkan Material !']]);
    //     }
    // };

    $get_next_step = DB::table('psb_laporan_status')
                      ->select('step_tracker.*')
                      ->leftJoin('step_tracker','psb_laporan_status.step_id','=','step_tracker.step_tracker_id')
                      ->where('psb_laporan_status.laporan_status_id',$request->input('status'))
                      ->first();

    $next_step = DB::table('dispatch_teknisi')
                     ->where('id',$id)
                     ->update([
                       'step_id' => $get_next_step->step_tracker_id
                     ]);

    $ntes = explode(',', $request->input('nte'));
    $exists = DB::select('
      SELECT a.*,b.id_regu, b.Ndem, b.dispatch_by
      FROM psb_laporan a, dispatch_teknisi b
      WHERE a.id_tbl_mj=b.id AND a.id_tbl_mj = ?
    ',[
      $id
    ]);

    $sendReport = 0;
    $scid = NULL;

    if (count($exists)) {
      $data = $exists[0];

      if ($data->dispatch_by==NULL){
          $scid = $data->Ndem;
      }
      else{
        $scid = NULL;
      }

      if($data->status_laporan!=$request->input('status')){
        $sendReport = 1;
      }
      $kpro_status = '';
      if ($request->input('status')==2){
        $kpro_status = "Kendala Teknik";
      } else if ($request->input('status')==10){
        $kpro_status = "Proses PT2/PT3";
      } else if ($request->input('status')==3) {
        $kpro_status = "Kendala Pelanggan";
      }
      //if ($kpro_status<>'')
      //$this->kpro($data->Ndem,$kpro_status,'manja',$request->input('catatan'));
      DB::transaction(function() use($request, $data, $auth, $materials, $ntes, $scid) {
        $tgl_up = "";
        if($data->status_laporan!=$request->input('status') && $request->input('status')==1){
          $tgl_up = date("Y-m-d");
        }

        DB::table('psb_laporan_log')->insert([
           'created_at'      => DB::raw('NOW()'),
           'created_by'      => $auth->id_karyawan,
           'status_laporan'  => $request->input('status'),
           'id_regu'         => $data->id_regu,
           'catatan'         => $request->input('catatan'),
           'Ndem'            => $data->Ndem,
           'psb_laporan_id'  => $data->id
         ]);
        DB::table('psb_laporan')
          ->where('id', $data->id)
          ->update([
            'modified_at'         => DB::raw('NOW()'),
            'modified_by'         => $auth->id_karyawan,
            'catatan'             => $request->input('catatan'),
            'status_laporan'      => $request->input('status'),
            'kordinat_pelanggan'  => $request->input('kordinat_pelanggan'),
            'nama_odp'            => $request->input('nama_odp'),
            'kordinat_odp'        => $request->input('kordinat_odp'),
            'typeont'             => $request->input('type_ont'),
            'snont'               => $request->input('sn_ont'),
            'typestb'             => $request->input('type_stb'),
            'snstb'               => $request->input('sn_stb'),
            'dropcore_label_code' => $request->input('dropcore_label_code'),
            'port_number'         => $request->input('port_number'),
            'redaman'             => $request->input('redaman'),
            'tgl_up'              => $tgl_up,
            'ttId'                => $request->tbTiang,
            'catatanRevoke'       => $request->input('catatanRevoke'),
            'noPelangganAktif'    => $request->input('noPelangganAktif'),
            'scid'                => $scid
          ]);

        $this->incrementStokTeknisi($data->id, $auth->id_karyawan);

        if ($materials){
            $this->insertMaterials($data->id, $materials);
        };

        $this->decrementStokTeknisi($auth->id_karyawan, $materials);

      });

      if ($a<>52 && $a<>57 && $a<>58 && $a<>59 && $a<>60){
        if($sendReport){
            exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
        };
      };

      if ($a == 1) {
        echo "kirim sms";
        $get_order = DB::SELECT('
          SELECT
          *,
          a.id as id_dt
          FROM
            dispatch_teknisi a
          LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
          LEFT JOIN regu c ON a.id_regu = c.id_regu
          LEFT JOIN Data_Pelanggan_Starclick d ON a.Ndem = d.orderId
          LEFT JOIN group_telegram e ON c.mainsector = e.chat_id
          WHERE
            a.id = "'.$id.'"
            ')[0];

            if ($get_order->orderId<>NULL && $get_order->sms_active_prov==1):
            $pesan = "Pelanggan Yth. \n";
            $pesan .= "Mhn bantuan penilaian kpd Teknisi Indihome Kami. Silahkan klik link berikut \n";
            $pesan .= "http://tomman.info/rv/".$this->encrypt($get_order->id_dt)." \n";
            $pesan .= "Jika layanan brmasalah hub 05113360654";


            // masukkan log
            DB::table('psb_laporan_log')->insert([
               'created_at'      => DB::raw('NOW()'),
               'created_by'      => $auth->id_karyawan,
               'status_laporan'  => $request->input('status'),
               'id_regu'         => $get_order->id_regu,
               'catatan'         => 'Pesan SMS Terkirim ',
               'Ndem'            => $get_order->Ndem,
               'psb_laporan_id'  => $data->id
            ]);

            // kirim sms
            $psn = str_replace('\n', ' ', $pesan);
            $dataDikirim = '("'.$get_order->orderKontak.'","'.$psn.'","true","3006")';
            $sql = 'INSERT INTO outbox (DestinationNumber,TextDecoded,MultiPart,CreatorID) values '.$dataDikirim;
            DB::connection('mysql2')->insert($sql);
            endif;
      }

      // exec('cd ..;php artisan kpro '.$disp->Ndem.' Laporan > /dev/null &');
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }
    else {
      DB::transaction(function() use($request, $id, $auth, $materials, $ntes, $scid) {
        $tgl_up = "";
        if($request->input('status')==1){
          $tgl_up = date("Y-m-d");
        }

        $insertId = DB::table('psb_laporan')->insertGetId([
          'created_at'          => DB::raw('NOW()'),
          'modified_at'         => DB::raw('NOW()'),
          'created_by'          => $auth->id_karyawan,
          'id_tbl_mj'           => $id,
          'catatan'             => $request->input('catatan'),
          'status_laporan'      => $request->input('status'),
          'kordinat_pelanggan'  => $request->input('kordinat_pelanggan'),
          'nama_odp'            => $request->input('nama_odp'),
          'kordinat_odp'        => $request->input('kordinat_odp'),
          'typeont'             => $request->input('type_ont'),
          'snont'               => $request->input('sn_ont'),
          'typestb'             => $request->input('type_stb'),
          'snstb'               => $request->input('sn_stb'),
          'dropcore_label_code' => $request->input('dropcore_label_code'),
          'port_number'         => $request->input('port_number'),
          'redaman'             => $request->input('redaman'),
          'tgl_up'              => $tgl_up,
          'ttId'                => $request->tbTiang,
          'catatanRevoke'       => $request->input('catatanRevoke'),
          'noPelangganAktif'    => $request->input('noPelangganAktif'),
          'scid'                => $scid
        ]);
        foreach($ntes as $nte) {
          DB::table('psb_laporan_material')->insert([
            'id' => $insertId.'-'.$nte,
            'psb_laporan_id' => $insertId,
            'id_item' => $nte,
            'qty' => 1,
            'type' =>2
          ]);
          DB::table('nte')
            ->where('id', $nte)
            ->update([
              'position_type'  => 3,
              'position_key'  => $id,
              'status' => 1
            ]);
        }
        //$disp = @$disp[0];
        //print_r($disp);
        if ($materials){
            // $this->insertMaterials($data->id, $materials);
            $this->insertMaterials($insertId, $materials);
        };

        $this->decrementStokTeknisi($auth->id_karyawan, $materials);

        exec('cd ..;php artisan sendReport '.$id.' > /dev/null &', $out);
      });

      return redirect('/')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }

  }

  public function simpanPda(Request $req){
      $this->validate($req,[
          'scPda' => 'required',
          'scPda' => 'numeric'
      ],[
          'scPda.required'  => 'Jangan Kosong',
          'scPda.numeric'   => 'Hanya Boleh Angka'
      ]);

      // cek wo PDA
      $dataPda = DB::table('dispatch_teknisi')->where('Ndem',$req->scPda)->first();
      if ($dataPda) {
          if ($dataPda->jenis_layanan=='PDA'){
              // cek sudah ada yg pernah meinput atau belum
              $cek = DB::table('dispatch_hd')->where('orderId', $dataPda->Ndem)->first();
              if (count($cek)==0){
                  // simpan data
                  $auth = session('auth');
                  DB::table('dispatch_hd')->insert([
                      'orderId'     => $req->scPda,
                      'workerId'    => $auth->id_user,
                      'workerName'  => $auth->nama,
                      'updated_at'  => DB::RAW('NOW()'),
                      'ket'         => 1
                  ]);

                  return back()->with('alerts',[['type' => 'success', 'text'  => 'SC Berhasil Disimpan']]);
              }
              else{
                  return back()->with('alerts',[['type' => 'danger', 'text' => '<strong>Gagal</strong> SC Sudah Pernah Diinput']]);
              }
          }
          else{
            return back()->with('alerts',[['type' => 'danger', 'text' => '<strong>Gagal</strong> Inputan Bukan SC PDA']]);
          }
      }
      else{
          return back()->with('alerts',[['type' => 'danger', 'text' => '<strong>Gagal</strong> Sc Tidak Ditemukan']]);
      }
  }

  public function grabIboosterbySc($in,$menu){
    $query = DB::table('Data_Pelanggan_Starclick')->where('orderId',$in)->first();
    $url   = DB::table('dispatch_teknisi')->where('Ndem',$query->orderId)->first();

    if (count($query)>0){
      $result   = $query;
      $internet = $result->internet;

      if ($internet<>''){
          echo $internet;
          $result = $this->grabIbooster($internet,$in,$menu);
          //dump($result);

          $inet = '';
          if(count($result)<>0){
            $inet = $result[0]['ND'];
          };

          DB::table('ibooster')->where('ND',$inet)->delete();
          DB::table('ibooster')->insert($result);

          // simpan ke psb laporan redaman_iboster
          DB::table('psb_laporan')->where('id_tbl_mj',$url->id)->update([
            'redaman_iboster' => $result[0]['ONU_Rx'],
          ]);

          return redirect('/'.$url->id)->with('alerts', [
              ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengukur WO Redaman <strong>'.$result[0]['ONU_Rx'].'<strong>']
            ]);
      }
      else {
        return redirect('/assurance/dispatch/'.$in)->with('alerts', [
            ['type' => 'danger', 'text' => '<strong>Gagal</strong> mengukur Redaman WO']
          ]);
      }
    }
  }

  public function grabIbooster($spd,$orderId,$menu){
    $ch = curl_init();
    //curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login_a.php');
    // curl_setopt($ch, CURLOPT_URL, 'https://ibooster.telkom.co.id/login.php');
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login1.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'ibooster.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $login = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($login));
    // dd($login);
    $input = $dom->getElementsByTagName('input')->item(0)->getAttribute("value");
    //$value = $input->item($i)->getAttribute("value");
    // dd($input);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login_code_a.php');
    // curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login1.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $akun = DB::table('akun')->where('id', '7')->first();
    $username = $akun->user;
    $password = $akun->pwd;
    curl_setopt($ch, CURLOPT_POSTFIELDS, "sha256=".$input."&username=".$username."&password=".$password."&dropdown_login=sso");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $rough_content = curl_exec($ch);

    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    //print_r($matches['cookie']);
    $cookiesOut = "";
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookie = null;
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/ukur_massal_speedy.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "nospeedy=".$spd."&analis=ANALISA");
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $result = curl_exec($ch);
    // dd($results);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($result, 0, $header_size);
    $result = substr($result, $header_size);
    $columns = array(
      1=>
      "ND",
      "IP_Embassy",
      "Type",
      "Calling_Station_Id",
      "IP_NE",
      "ADSL_Link_Status",
      "Upstream_Line_Rate",
      "Upstream_SNR",
      "Upstream_Attenuation",
      "Upstream_Attainable_Rate",
      "Downstream_Line_Rate",
      "Downstream_SNR",
      "Downstream_Attenuation",
      "Downstream_Attainable_Rate",
      "ONU_Link_Status",
      "ONU_Serial_Number",
      "Fiber_Length",
      "OLT_Tx",
      "OLT_Rx",
      "ONU_Tx",
      "ONU_Rx",
      "Framed_IP_Address",
      "MAC_Address",
      "Last_Seen",
      "AcctStartTime",
      "AccStopTime",
      "AccSesionTime",
      "Up",
      "Down",
      "Status_Koneksi",
      "Nas_IP_Address"
    );
    $dom = @\DOMDocument::loadHTML(trim($result));
    // print_r($result);
    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 3, $count = $rows->length; $i < $count; $i++)
    {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 1, $jcount = count($columns); $j < $jcount; $j++)
      {
          //echo $j." ";
          $td = $cells->item($j);
          if (is_object($td)) {
            $node = $td->nodeValue;
          } else {
            $node = "empty";
          }
          $data[$columns[$j]] = $node;
      }
      $data['order_id'] = $orderId;
      $data['user_created'] = @session('auth')->id_user;
      $data['menu'] = $menu;
      $result[] = $data;
    }
    return ($result);
  }

  public function getWOApi(Request $req){
      if ($req->has('no_wo')){
          $noWo = $req->input('no_wo');
          $data = DB::table('Data_Pelanggan_Starclick')->where('orderId',$noWo)->first();
          if ($data){
              return response()->json([
                  'status'    => 'T',
                  'message'   => 'Data Has Found',
                  'data'      => $data
              ],200);
          }
          else{
              return response()->json([
                  'status'    => 'F',
                  'message'   => 'Data Not Found',
                  'data'      => $data
              ],404);
          }
      }
      else{
        return response()->json([
            'status'  => 'F',
            'message' => 'error no_wo'
        ],401);
      }
  }

  private function getApiBaDigital($wo){
    $postData = http_build_query(
        array(
          'no_wo' => $wo,
          'key'   => 'EpwvzxlvDpDlllllll@ulJl'
        )
    );

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => $postData
        )
    );

    $context = stream_context_create($opts);
    $result  = file_get_contents('https://api.telkomakses.co.id/API/alista/get_ba_digital_by_no_wo.php', false, $context);

    $hasil = json_decode($result);
    return $hasil;
  }

  private function insertQrCodeApi($data){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://dalapa.id/api/dalapa/port",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => false,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => array(
            'odp_location'    => $data['odp_location'],
            'port_number'     => $data['port_number'],
            'latitude'        => $data['latitude'],
            'longitude'       => $data['longitude'],
            'sn_ont'          => $data['sn_ont'],
            'nama_pelanggan'  => $data['nama_pelanggan'],
            'voice_number'    => $data['voice_number'],
            'internet_number' => $data['internet_number'],
            'date_validation' => $data['date_validation'],
            'qr_port_odp'     => $data['qr_port_odp'],
            'qr_dropcore'     => $data['qr_dropcore'],
            'nama_jalan'      => $data['nama_jalan'],
            'contact'         => $data['contact'],
            'type'            => '1',
            'evidence'        => new \CURLFILE('/srv/htdocs/tomman1_psb/public/upload3/evidence/'.$data['id'].'/LABEL.jpg')),
      CURLOPT_HTTPHEADER => array(
        "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ3aXRlbCI6IkJKTSIsInZhbHVlIjp7IjEiOjIwMzcsIjIiOjIwMzgsIjMiOjIwMzksIjQiOjIwNDAsIjUiOjIwNDEsIjYiOjIwNDIsIjciOjIwNDN9fQ.GF0HcedknFaFQ5h-Dll93F6MLRmVpj2sBYrpgoytDC0"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    if ($err) {
      $pesan = "cURL Error #:" . $err;
    } else {
      $pesan = $response;
    }

    return $pesan;
  }

}
