@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    td {
      padding : 5px;
      border:1px solid #ecf0f1;
    }
    .pdnol {
      padding : 0px;
    }
    .center {
      text-align: center;
    }
    th {
      text-align: center;
      vertical-align: middle;
      background-color: #0073b7;
      padding : 5px;
      color : #FFF;
      border:1px solid #ecf0f1;
    }
    td {
      font-size: 14px;
      padding : 3px !important;

    }
    .center a {
      color : #666666 !important;
    }
    .green {
      background-color: #2ecc71;
      font-weight: bold;
      color : #FFF;
    }
    .green a:link{
      color : #FFF;
    }
    .red {
      background-color: #e67e22;
      font-weight: bold;
      color : #FFF;
    }
    .redx {
      background-color: #e74c3c;
      font-weight: bold;
      color : #FFF;
    }
    .red a:link{
      color : #FFF;
    }
    .gray {
      background-color : #f4f4f4;
    }
  </style>
  <script src="/bower_components/devexpress-web-14.1/js/dx.chartjs.js"></script>
  <link rel="stylesheet" href="/bower_components/devexpress-web-14.1/css/dx.dark.css" />
  <center>
  <h3>Dashboard Panjang Tarikan TA KALSEL Periode {{ $periode }}</h3>
  <br />
  </center>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Table
        </div>
        <div class="panel-body">
          <table>
            <tr>
              <td>ORG</td>
              <td>ORDER</td>
              <td>DC</td>
            </tr>
            @foreach ($query as $result)
            <?php
              if ($result->orderId <> '') {
                $order = "PSB";
              } else {
                $order = "NONPSB";
              }
            ?>
            <tr>
              <td>{{ $result->id_dt }}</td>
              <td>{{ $order }}</td>
              <td>{{ $result->dc }}</td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
