@extends('layout')

@section('content')
  @include('partial.alerts')

  @foreach($sektor as $listSektor)
      <div class="panel panel-primary">
          <div class="panel-heading">{{ $listSektor->title }}</div>
          <div class="panel-body">
              @foreach($listData as $data)
                  @if($listSektor->chat_id==$data->mainsector)
                      <p>{{ $data->uraian }}</p>
                  @endif
              @endforeach
          </div>
      </div>
  @endforeach
     
@endsection
