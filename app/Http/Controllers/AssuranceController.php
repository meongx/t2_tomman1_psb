<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
use App\DA\DashboardModel;
use App\DA\DispatchModel;
use App\DA\PsbModel;
use App\DA\AssuranceModel;
use Illuminate\Support\Facades\Input;

class AssuranceController extends Controller
{
  protected $assuranceInputs = [
    'Label','Foto_Action','ODP', 'Hasil_Ukur_OPM' ,'Berita_Acara', 'Rumah_Pelanggan', 'Test_Layanan_Internet', 'Test_Layanan_TV', 'Test_Layanan_Telepon', 'MODEM', 'SN_ONT_RUSAK', 'Sambungan_Kabel_DC', 'Pullstrap_S_Clamp', 'Stoper', 'Clamp_Hook_Clamp_Pelanggan', 'Roset', 'Patchcord', 'Slack_Kabel_DC', 'Speedtest_before', 'Speedtest_after', 'Indikasi_before', 'Indikasi_after', 'Hasil_ukur_ODP', 'Kondisi_ODP', 'Foto_Letak_Gangguan'
  ];

  public function dashboard2(){
    $get_kendala_teknik = AssuranceModel::kendala_teknik_by_umur();
    return view('assurance.dashboard2',compact('get_kendala_teknik'));
  }

  public function gaulbysebab(){
    $periode = Input::get('date');
    $sektor = Input::get('sektor');
    $where_periode = 'AND a.tgl_close LIKE "'.$periode.'%"';
    $where_sektor = '';
    if ($sektor<>'') { $where_sektor = 'AND e.mainsector="'.$sektor.'"'; }
    $title = "PENYEBAB GAUL";
    $get_sektor = DB::SELECT('SELECT * FROM group_telegram WHERE title LIKE "TERR%" ORDER BY urut');
    $get_sebab = DB::SELECT('SELECT d.penyebab,count(*) as jumlah FROM detail_gangguan_close a LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj LEFT JOIN psb_laporan_penyebab d ON c.penyebabId = d.idPenyebab LEFT JOIN regu e ON b.id_regu = e.id_regu WHERE  1 '.$where_periode.' '.$where_sektor.' GROUP BY d.idPenyebab ORDER BY jumlah DESC');
    $get_sebab_chart = DB::SELECT('SELECT d.penyebab,count(*) as jumlah FROM detail_gangguan_close a LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj LEFT JOIN psb_laporan_penyebab d ON c.penyebabId = d.idPenyebab LEFT JOIN regu e ON b.id_regu = e.id_regu WHERE 1 '.$where_periode.' '.$where_sektor.' GROUP BY d.idPenyebab ORDER BY jumlah ASC');
    return view('assurance.gaulsebabaction',compact('title','periode','sektor','get_sektor','get_sebab','get_sebab_chart'));
  }

  public function A3on3TextMode(){
    $get_sektor = DB::SELECT('SELECT * FROM group_telegram WHERE title LIKE "TERR%"');
    foreach ($get_sektor as $sektor){
      echo $sektor->title."<br />";
      $get_ttr3notcomply = DB::SELECT('
        SELECT
        *,
        b.*
        FROM
          dispatch_teknisi a
        LEFT JOIN data_nossa_1_log b ON a.Ndem = b.Incident
        LEFT JOIN regu c ON a.id_regu = c.id_regu
        LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
        LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
        LEFT JOIN data_nossa_1 f ON b.Incident = f.Incident
        WHERE
          e.status_laporan = 1 AND
          c.mainsector = "'.$sektor->chat_id.'" AND
          date(b.Booking_Date) = "'.date('Y-m-d').'"

      ');
      echo "TTR 3 JAM : <br />";
      foreach ($get_ttr3notcomply as $ttr3notcomply){
        echo $ttr3notcomply->Incident."|".$ttr3notcomply->Booking_Date."<br />";
      }
      echo "<br />";
    }
  }

  public function ttr3notcomply(){
    $get_ttr3notcomply_sektor = AssuranceModel::ttr3notcomply_sektor();
    $get_ttr3notcomply_query = 'SELECT ';
    $month = date('Y-m');
    for($i=1;$i<=date('d');$i++){
      $get_ttr3notcomply_query .= 'SUM(CASE WHEN date(a.tgl_close)="'.$month.'-'.$i.'" THEN 1 ELSE 0 END) as tgl'.$i.',';
    }
    $get_ttr3notcomply_query .= '
      count(*) as jumlah,
      d.title
      FROM detail_gangguan_close_3on3 a
      LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      WHERE
        (a.tgl_close BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d').'") AND
        a.ttr3_jam = 0
      GROUP BY d.title
      ORDER BY jumlah DESC
    ';
    // echo $get_ttr3notcomply_query;
    $get_ttr3notcomply = AssuranceModel::ttr3notcomply($get_ttr3notcomply_query);
    $data = array();
    foreach ($get_ttr3notcomply as $num=>$ttr3notcomply_sektor){
      for ($i=1;$i<=date('d');$i++){
        $data[$ttr3notcomply_sektor->title]['tgl'.$i] = $ttr3notcomply_sektor->{'tgl'.$i};
      }
    }
    return view('assurance.ttrnotcomply',compact('get_ttr3notcomply','get_ttr3notcomply_sektor','start','end','data'));
  }

  public function ttr3notcomplylist($sektor,$date){
    $title = "TTR 3 JAM NOT COMPLY ".$sektor." ".$date;
    $get_gaulsektorlist = AssuranceModel::ttr3notcomplylist($sektor,$date);
    return view('assurance.gaulsektorlist',compact('title','get_gaulsektorlist','sektor','date'));

  }

  public function trendgaullist($sektor,$date){
    $title = "LIST GAUL ".$sektor." ".$date;
    $get_gaulsektorlist = AssuranceModel::trendgaullist($sektor,$date);
    return view('assurance.gaulsektorlist',compact('title','get_gaulsektorlist','sektor','date'));
  }

  public function trendgaul(){
    $get_trendgaul_date = AssuranceModel::trendgaul_date();
    $trendgaul_sektor_query = '
        SELECT ';
    foreach ($get_trendgaul_date as $num => $trendgaul_date){
      $trendgaul_sektor_query .= 'SUM(CASE WHEN date(a.tgl_close)="'.$trendgaul_date->date_close.'" THEN 1 ELSE 0 END) as tgl'.++$num.',';
    }
    $trendgaul_sektor_query .= ' d.title,count(*) as jumlah,c.mainsector
        FROM
          detail_gangguan_close a
        LEFT JOIN dispatch_teknisi b on a.ticket_id = b.Ndem
        LEFT JOIN regu c ON b.id_regu = c.id_regu
        LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
        GROUP BY
          d.title
        ORDER BY jumlah DESC
      ';
    $get_trendgaul_sektor = AssuranceModel::trendgaul_sektor($trendgaul_sektor_query);
    $trendgaul_sektor_query_total = '
        SELECT ';
    foreach ($get_trendgaul_date as $num => $trendgaul_date){
      $trendgaul_sektor_query_total .= 'SUM(CASE WHEN date(a.tgl_close)="'.$trendgaul_date->date_close.'" THEN 1 ELSE 0 END) as tgl'.++$num.',';
    }
    $trendgaul_sektor_query_total .= ' d.title,count(*) as jumlah,c.mainsector
        FROM
          detail_gangguan_close a
        LEFT JOIN dispatch_teknisi b on a.ticket_id = b.Ndem
        LEFT JOIN regu c ON b.id_regu = c.id_regu
        LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
        ORDER BY jumlah DESC
      ';
    $get_trendgaul_sektor_total = AssuranceModel::trendgaul_sektor($trendgaul_sektor_query_total);
    $data = array();
    $total = array();
    foreach ($get_trendgaul_sektor as $trendgaul_sektor) {
      foreach ($get_trendgaul_date as $num => $trendgaul_date){
        $data[$trendgaul_sektor->title]['tgl'.++$num] = $trendgaul_sektor->{'tgl'.$num};
      }
    }
    $trendgaul_sektor_total = $get_trendgaul_sektor_total[0];
    foreach ($get_trendgaul_date as $num => $trendgaul_date){
      $total['tgl'.++$num] = $trendgaul_sektor_total->{'tgl'.$num};
    }

    $layout = "layout";
    return view('assurance.trendgaul',compact('total','get_trendgaul_sektor','layout','get_trendgaul_date','data'));
  }

  public function matrix($date){
    $layout = "layout";
    $get_sektor = AssuranceModel::get_sektor();
    $matrix = array();
    foreach ($get_sektor as $sektor){
      $matrix[$sektor->chat_id] = AssuranceModel::get_sektor_matrix_team($sektor->chat_id,$date);
      foreach ($matrix[$sektor->chat_id] as $team){
        $matrix[$sektor->chat_id][$team->id_regu] = AssuranceModel::get_sektor_matrix_team_order($team->id_regu,$date);
      }
    }

    return view('assurance.matrix',compact('date','get_sektor','layout','matrix'));
  }

  public function len(){
    $message = "Pelanggan Yth. Mhn bantuan penilaian kpd Teknisi Indihome Kami. Silahkan klik link berikut http://tomman.info/rv/aXaYrN";
    echo $message."<br />";
    echo strlen($message);
  }

  public function manja($date){
    $get_manja = AssuranceModel::manja($date);
    $dataMatrix = array();
    foreach ($get_manja as $manja){
      $dataMatrix[$manja->title]['expired'] = AssuranceModel::get_tiketmanja($manja->title,$date,'expired');
      $dataMatrix[$manja->title]['jam8'] = AssuranceModel::get_tiketmanja($manja->title,$date,'8');
      $dataMatrix[$manja->title]['jam9'] = AssuranceModel::get_tiketmanja($manja->title,$date,'9');
      $dataMatrix[$manja->title]['jam10'] = AssuranceModel::get_tiketmanja($manja->title,$date,'10');
      $dataMatrix[$manja->title]['jam11'] = AssuranceModel::get_tiketmanja($manja->title,$date,'11');
      $dataMatrix[$manja->title]['jam12'] = AssuranceModel::get_tiketmanja($manja->title,$date,'12');
      $dataMatrix[$manja->title]['jam13'] = AssuranceModel::get_tiketmanja($manja->title,$date,'13');
      $dataMatrix[$manja->title]['jam14'] = AssuranceModel::get_tiketmanja($manja->title,$date,'14');
      $dataMatrix[$manja->title]['jam15'] = AssuranceModel::get_tiketmanja($manja->title,$date,'15');
      $dataMatrix[$manja->title]['jam16'] = AssuranceModel::get_tiketmanja($manja->title,$date,'16');
      $dataMatrix[$manja->title]['jam17'] = AssuranceModel::get_tiketmanja($manja->title,$date,'17');

    }
    $layout = 'layout';
    return view('assurance.manja',compact('get_manja','dataMatrix','layout'));
  }

  public function telco($periode,$rayon){
    $get_gaul = AssuranceModel::nonatero_gaul($periode,$rayon);
    $get_ttr3jam = AssuranceModel::nonatero_ttr3jam($periode,$rayon);
    $get_ttr12jam = AssuranceModel::nonatero_ttr12jam($periode,$rayon);
    $data_gaul = array();
    $data_ibooster = array();
    foreach ($get_gaul as $gaul){
      // $data_ibooster[$gaul->TROUBLE_NO] = $this->grabIbooster($gaul->TROUBLE_NUMBER,0,'tech');
      $data_gaul[$gaul->TROUBLE_NO] = AssuranceModel::nonatero_gaul_detil($gaul->TROUBLE_NUMBER);
    }
    // foreach ($get_ttr3jam as $ttr3jam){
    //  $data_ibooster[$ttr3jam->TROUBLE_NO] = $this->grabIbooster($ttr3jam->TROUBLE_NUMBER,0,'tech');
    // }
    // foreach ($get_ttr12jam as $ttr12jam){
    //  $data_ibooster[$ttr12jam->TROUBLE_NO] = $this->grabIbooster($ttr12jam->TROUBLE_NUMBER,0,'tech');
    // }

    $layout = 'public_layout';
    return view('assurance.telco',compact('get_gaul','layout','get_ttr3jam','get_ttr12jam','data_gaul','data_ibooster'));
  }

  public function public_manja($date){
    $get_manja = AssuranceModel::manja($date);
    $dataMatrix = array();
    foreach ($get_manja as $manja){
      $dataMatrix[$manja->title]['jam8'] = AssuranceModel::get_tiketmanja($manja->title,$date,'8');
      $dataMatrix[$manja->title]['jam9'] = AssuranceModel::get_tiketmanja($manja->title,$date,'9');
      $dataMatrix[$manja->title]['jam10'] = AssuranceModel::get_tiketmanja($manja->title,$date,'10');
      $dataMatrix[$manja->title]['jam11'] = AssuranceModel::get_tiketmanja($manja->title,$date,'11');
      $dataMatrix[$manja->title]['jam12'] = AssuranceModel::get_tiketmanja($manja->title,$date,'12');
      $dataMatrix[$manja->title]['jam13'] = AssuranceModel::get_tiketmanja($manja->title,$date,'13');
      $dataMatrix[$manja->title]['jam14'] = AssuranceModel::get_tiketmanja($manja->title,$date,'14');
      $dataMatrix[$manja->title]['jam15'] = AssuranceModel::get_tiketmanja($manja->title,$date,'15');
      $dataMatrix[$manja->title]['jam16'] = AssuranceModel::get_tiketmanja($manja->title,$date,'16');
      $dataMatrix[$manja->title]['jam17'] = AssuranceModel::get_tiketmanja($manja->title,$date,'17');

    }
    $layout = 'public_layout';
    return view('assurance.manja',compact('get_manja','dataMatrix','layout'));
  }

  public function monitoringkpi($sektor,$tgl){
    $layout = 'layout';
    $kpi_ttr3jam = AssuranceModel::monitoringkpi_ttr3jam($sektor,$tgl);
    return view('assurance.monitoringkpi',compact('kpi_ttr3jam','layout'));
  }

  public function public_manja2($date){
    $get_manja = AssuranceModel::manja($date);
    $dataMatrix = array();
    foreach ($get_manja as $manja){
      $dataMatrix[$manja->title]['jam8'] = AssuranceModel::get_tiketmanja2($manja->title,$date,'8');
      $dataMatrix[$manja->title]['jam9'] = AssuranceModel::get_tiketmanja2($manja->title,$date,'9');
      $dataMatrix[$manja->title]['jam10'] = AssuranceModel::get_tiketmanja2($manja->title,$date,'10');
      $dataMatrix[$manja->title]['jam11'] = AssuranceModel::get_tiketmanja2($manja->title,$date,'11');
      $dataMatrix[$manja->title]['jam12'] = AssuranceModel::get_tiketmanja2($manja->title,$date,'12');
      $dataMatrix[$manja->title]['jam13'] = AssuranceModel::get_tiketmanja2($manja->title,$date,'13');
      $dataMatrix[$manja->title]['jam14'] = AssuranceModel::get_tiketmanja2($manja->title,$date,'14');
      $dataMatrix[$manja->title]['jam15'] = AssuranceModel::get_tiketmanja2($manja->title,$date,'15');
      $dataMatrix[$manja->title]['jam16'] = AssuranceModel::get_tiketmanja2($manja->title,$date,'16');
      $dataMatrix[$manja->title]['jam17'] = AssuranceModel::get_tiketmanja2($manja->title,$date,'17');

    }
    $layout = 'public_layout';
    return view('assurance.manja',compact('get_manja','dataMatrix','layout'));
  }


  public function dokumentasi($id,$id_dt,$action){
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id_dt
    ]);
    if (count($exists)) {
      $data = $exists[0];
      DB::table('psb_laporan')
        ->where('id', $data->id)
        ->update([
          'modified_at'   => DB::raw('NOW()'),
          'modified_by'   => $auth->id_karyawan,
          'action'=>  $action,
          'penyebabId'  => $id
        ]);
    } else {
      $insertId = DB::table('psb_laporan')->insertGetId([
        'created_at'    => DB::raw('NOW()'),
        'created_by'    => $auth->id_karyawan,
        'id_tbl_mj'     => $id_dt,
        'action'        => $action,
        'penyebabId'  => $id
      ]);
    }
    $get_penyebab = DB::table('psb_laporan_penyebab')->where('idPenyebab',$id)->get();
    // $check_foto = DB::SELECT('
    //   SELECT
    //   a.name
    //   FROM cat_foto a
    //   WHERE
    //   a.'.$get_penyebab[0]->kategori.' = 1
    // ');
    // return view('assurance.dokumentasi',compact('id_dt','id','get_penyebab','check_foto'));
    return "true";
  }

  public function dashboard_gaul_by_seq($periode){
    $data = AssuranceModel::dashboard_gaul_by_seq($periode,"ASC");
    $data1 = AssuranceModel::dashboard_gaul_by_seq($periode,"DESC");
    return view('assurance.dashboard_gaul_by_seq',compact('data','data1','periode'));
  }

  public function dashboard_gaul_by_seq_list($action,$periode){
    $data = AssuranceModel::dashboard_gaul_by_seq_list($action,$periode);
    return view('assurance.dashboard_gaul_by_seq_list',compact('data','action','periode'));
  }

  public function gaulgenerator($periode){
    $data = AssuranceModel::gaulgenerator($periode);
      $color1 = "#FFF";
      $color2 = "#dfe6e9";
      $trouble_num = "";
      $numx = 0;
      $symp = "";
      foreach ($data as $num => $result)
      {
        if ($trouble_num<>$result->TROUBLE_NUMBER){
          $trouble_color = $color1;
          $trouble_num = $result->TROUBLE_NUMBER;
          $symp = $result->JENIS ? : "NA";
          $symp .="»";
          $numx++;
        } else {
          $symp .= $result->JENIS ? : "NA";
          $symp .="»";
          $trouble_color = $color2;
        }
        if ($result->SEQ==""){
          DB::table('nonatero_detil_gaul')->where('TROUBLE_NO',$result->TROUBLE_NO)
          ->update([
            'SEQ' => $symp
          ]);
        }
      }
    return view('assurance.gaulgenerator',compact('data','periode'));
  }

  public function reportibooster($periode){
    $data = DB::SELECT('SELECt * FROM ibooster a WHERE a.date_created LIKE "'.$periode.'%"');
    return view('assurance.reportibooster',compact('periode','data'));
  }

  public function ibooster(){
    $resultx = array();
    $updatenote = PsbModel::updatenote();
    $checkONGOING = PsbModel::checkONGOING();
    return view('ibooster.home',compact('updatenote','checkONGOING','resultx'));
  }

  public function ukurIbooster(Request $request){
    $ukur = $request->input('ukur');
    if ($ukur <> ""){
      $resultx = $this->grabIbooster($ukur,0,'tech');
      $checkONGOING = PsbModel::checkONGOING();
      return view('ibooster.home',compact('resultx','checkONGOING'));
    } else {
      return redirect()->back()->with('alerts', [
        ['type' => 'warning', 'text' => '<strong>Gagal mengukur</strong>, periksa kembali nomor Anda!']
      ]);
    }

  }

  public function batchGrabIbooster(){
    $query = DB::table('rock_excel')->get();
    foreach ($query as $result){
      $this->grabIboosterbyIN($result->trouble_no,'dispatch');
      echo $result->trouble_no." OK <br />";
    }
  }

  public function grabIboosterbyIN($in,$menu){
    $query = DB::table('data_nossa_1_log')->where('Incident',$in)->get();
    $url   = DB::table('dispatch_teknisi')->where('Ndem',$query[0]->Incident)->first();

    if (count($query)>0){
      $result = $query[0];

      $tiket = substr($result->Incident, 0, 3);
      if ($tiket=="INT"){
          $serviceId = $result->no_internet;
          $ser       = $serviceId;
      }
      else{
          $get_serviceId = $result->Service_ID;
          $serviceId = explode('_',$get_serviceId);
          if (count($serviceId)==1){
            $ser = $serviceId[0];
          }
          else if (count($serviceId)>1){
            $ser = $serviceId[1];
          };
      };

    if (count($serviceId)>0){
        // echo $ser;
        $result = $this->grabIbooster($ser,$in,$menu);
        //dump($result);
        DB::table('ibooster')->insertIgnore($result);

        if ($menu<>"assurance"){
            return redirect('/assurance/dispatch/'.$in)->with('alerts', [
                ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengukur WO GANGGUAN']
              ]);
        }
        else{
            // simpan ke psb laporan redaman_iboster
            DB::table('psb_laporan')->where('id_tbl_mj',$url->id)->update([
                'redaman_iboster' => $result[0]['ONU_Rx'],
            ]);

            return redirect('/tiket/'.$url->id)->with('alerts', [
              ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengukur WO Redaman <strong>'.$result[0]['ONU_Rx'].'<strong>']
            ]);
        }
    }
    else {
      return redirect('/assurance/dispatch/'.$in)->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>Gagal</strong> mengukur WO GANGGUAN']
        ]);
    }
  }
}

  public function grabIbooster($spd,$orderId,$menu){
    $ch = curl_init();
    //curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login_a.php');
    // curl_setopt($ch, CURLOPT_URL, 'https://ibooster.telkom.co.id/login.php');
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login1.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'ibooster.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $login = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($login));
    // dd($login);
    $input = $dom->getElementsByTagName('input')->item(0)->getAttribute("value");
    //$value = $input->item($i)->getAttribute("value");
    // dd($input);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login_code_a.php');
    // curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login1.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $akun = DB::table('akun')->where('id', '7')->first();
    $username = $akun->user;
    $password = $akun->pwd;
    curl_setopt($ch, CURLOPT_POSTFIELDS, "sha256=".$input."&username=".$username."&password=".$password."&dropdown_login=sso");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $rough_content = curl_exec($ch);

    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    //print_r($matches['cookie']);
    $cookiesOut = "";
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookie = null;
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/ukur_massal_speedy.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "nospeedy=".$spd."&analis=ANALISA");
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $result = curl_exec($ch);
    // dd($results);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($result, 0, $header_size);
    $result = substr($result, $header_size);
    $columns = array(
      1=>
      "ND",
      "IP_Embassy",
      "Type",
      "Calling_Station_Id",
      "IP_NE",
      "ADSL_Link_Status",
      "Upstream_Line_Rate",
      "Upstream_SNR",
      "Upstream_Attenuation",
      "Upstream_Attainable_Rate",
      "Downstream_Line_Rate",
      "Downstream_SNR",
      "Downstream_Attenuation",
      "Downstream_Attainable_Rate",
      "ONU_Link_Status",
      "ONU_Serial_Number",
      "Fiber_Length",
      "OLT_Tx",
      "OLT_Rx",
      "ONU_Tx",
      "ONU_Rx",
      "Framed_IP_Address",
      "MAC_Address",
      "Last_Seen",
      "AcctStartTime",
      "AccStopTime",
      "AccSesionTime",
      "Up",
      "Down",
      "Status_Koneksi",
      "Nas_IP_Address"
    );
    $dom = @\DOMDocument::loadHTML(trim($result));
    // print_r($result);
    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 3, $count = $rows->length; $i < $count; $i++)
    {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 1, $jcount = count($columns); $j < $jcount; $j++)
      {
          //echo $j." ";
          $td = $cells->item($j);
          if (is_object($td)) {
            $node = $td->nodeValue;
          } else {
            $node = "empty";
          }
          $data[$columns[$j]] = $node;
      }
      $data['order_id'] = $orderId;
      $data['user_created'] = @session('auth')->id_user;
      $data['menu'] = $menu;
      $result[] = $data;
    }
    return ($result);
  }

  public function index(){
    $tgl = date('Y-m-d');
    $SQL = '
        SELECT e.status_laporan, b.laporan_status AS status, a.dispatch_by
            FROM dispatch_teknisi a
            LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
            LEFT JOIN psb_laporan_status b ON e.status_laporan = b.laporan_status_id
            WHERE (e.status_laporan <> "") and
            date(a.updated_at) = "'.$tgl.'" order by e.status_laporan
        ';

    $list = DB::select($SQL);
    $dash = "";
    $lastStatus = '';
    foreach ($list as $no => $m){
      if($lastStatus == $m->status_laporan){
        $dash[count($dash)-1]['count'] += 1;
        if($m->dispatch_by == 2)
          $dash[count($dash)-1]['assurance'] += 1;
        else if($m->dispatch_by == 0)
          $dash[count($dash)-1]['psb'] += 1;
      }else{
        $dash[] = array("status" => $m->status_laporan, "nama" => $m->status,"psb" => 0,"assurance" => 0, "count" => 1);
        if($m->dispatch_by == 2)
          $dash[count($dash)-1]['assurance'] += 1;
        else if($m->dispatch_by == 0)
          $dash[count($dash)-1]['psb'] += 1;
      }
      $lastStatus = $m->status_laporan;
    }
    $data="";

    $ont_offline = DB::table('roc')->where('datek','ONTOFFLINE')->get();
    return view('assurance.index',compact('data','dash','ont_offline'));
  }
  public function search(){

    $ont_offline = DB::table('roc')->where('datek','ONTOFFLINE')->get();
    $data = "";
    $list = DB::select('
      SELECT
        a.*,c.uraian, d.title, b.id as id_dt, b.updated_at
      FROM
      roc a
      LEFT JOIN dispatch_teknisi b ON a.no_tiket = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      ORDER BY b.id_regu asc
    ');
    return view('assurance.index',compact('data','list','ont_offline'));
  }

  public function ont_offline(){
    $list = DB::select('
      SELECT
        a.*,c.uraian, d.title, b.id as id_dt, b.updated_at
      FROM
      roc_active a
      LEFT JOIN dispatch_teknisi b ON a.no_tiket = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      ORDER BY b.id_regu asc
    ');
    $ont_offline = DB::SELECT('
    SELECT
      a.*,c.uraian, d.title, b.id as id_dt, b.updated_at, e.STATUS_ONT
    FROM
    roc a
    LEFT JOIN dispatch_teknisi b ON a.no_tiket = b.Ndem
    LEFT JOIN regu c ON b.id_regu = c.id_regu
    LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
    LEFT JOIN ont_offline e ON a.no_telp = e.ND
    WHERE
      a.datek = "ONTOFFLINE"
    ORDER BY b.id_regu asc

    ');
    return view('assurance.list2',compact('list','ont_offline'));
  }

  public function searchpost(Request $request){

    $id = $request->input('search');
    if ($id<>"") {
      $query = DispatchModel::WorkOrderAsr($id);
      $data   = "Trying to search '".$id."'";
      return view('assurance.index',compact('data','query','id'));
    } else {
      echo "ERROR";
    }
  }

  public function dashboard()
  {

    $list = DB::select('
      SELECT * , loker_ta AS lt, (
        SELECT count( no_tiket )
        FROM roc_active
        WHERE loker_ta = lt
      ) AS ttl,
      CASE roc_active.loker_ta
      WHEN "1"
      THEN "PERSONAL"
      WHEN "2"
      THEN "CORPORATE"
      ELSE "UNKNOW"
      END AS uraian
      FROM roc_active
      WHERE 1
      ORDER BY sto
    ');
    //var_dump($list);
    $outer = array();
    $lastSto = '';
    $personal_co = 0;
    $personal_ftth = 0;
    $corp_co = 0;
    $corp_ftth = 0;
    foreach($list as $row) {
        if ($lastSto == $row->sto) {
          if($row->lt == 1 && ( $row->alpro == 'GPON' || $row->alpro == 'MSAN' ))
            $personal_ftth++;
          if($row->lt == 1 && $row->alpro == 'COOPPER' )
            $personal_co++;
          if($row->lt == 2 && ( $row->alpro == 'GPON' || $row->alpro == 'MSAN' ))
            $corp_ftth++;
          if($row->lt == 2 && $row->alpro == 'COOPPER' )
            $corp_co++;
          $outer[count($outer)-1] = array('sto' => $row->sto_trim, 'personal_co' => $personal_co, 'personal_ftth' => $personal_ftth, 'corp_co' => $corp_co, 'corp_ftth' => $corp_ftth);
          $lastSto = $row->sto_trim;
        }
        else {
          $personal_co = 0;
          $personal_ftth = 0;
          $corp_co = 0;
          $corp_ftth = 0;
          if($row->lt == 1 && ( $row->alpro == 'GPON' || $row->alpro == 'MSAN' ))
            $personal_ftth++;
          if($row->lt == 1 && $row->alpro == 'COOPPER' )
            $personal_co++;
          if($row->lt == 2 && ( $row->alpro == 'GPON' || $row->alpro == 'MSAN' ))
            $corp_ftth++;
          if($row->lt == 2 && $row->alpro == 'COOPPER' )
            $corp_co++;
          $outer[] = array('sto' => $row->sto_trim, 'personal_co' => $personal_co, 'personal_ftth' => $personal_ftth, 'corp_co' => $corp_co, 'corp_ftth' => $corp_ftth);
          $lastSto = $row->sto_trim;
        }
        //$countOuter++;
    }
    //var_dump($outer);
    return view('assurance.dashboard', compact('list', 'outer'));
  }
  public function list_tiket()
  {
    $ont_offline = DB::table('roc')->where('datek','ONTOFFLINE')->get();

    $list = DB::select('
      SELECT
        a.*,c.uraian, d.title,e.workerName,e.workerId,e.updated_at as takeTime
      FROM
      roc_active a
      LEFT JOIN dispatch_teknisi b ON a.no_tiket = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN dispatch_hd e ON a.no_tiket = e.orderId
      ORDER BY b.id_regu asc
    ');
    return view('assurance.list', compact('list','ont_offline'));
  }
  public function close_list($id)
  {
    $list = DB::select('
      SELECT
        b.Ndem, c.tgl_open, c.no_speedy, d.action
      FROM
      psb_laporan a
      LEFT JOIN dispatch_teknisi b ON a.id_tbl_mj = b.id
      LEFT JOIN roc c ON b.Ndem = c.no_tiket
      LEFT JOIN psb_laporan_action d ON a.action = d.laporan_action_id
      where b.Ndem like "%IN%" and a.modified_at like "%'.$id.'%"
    ');
    return view('assurance.list3', compact('list'));
  }
  public function to_logic(Request $req){
    $auth = session('auth');
    if($req->status == "store"){
      $exists = DB::table('dispatch_hd')->where('orderId', $req->orderId)->first();
      if($exists){
        return redirect()->back()->with('alerts', [
          ['type' => 'warning', 'text' => '<strong>Gagal</strong> mengambil order, sudah ditikung orang!']
        ]);
      }else{
        DB::table('dispatch_hd')->insert([
          "orderId"=> $req->orderId,
          "workerId"=> $auth->id_karyawan,
          "workerName"=> $auth->nama,
          "updated_at"=>DB::raw('NOW()')
        ]);
        return redirect()->back()->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengambil order']
        ]);
      }
    }else{
      DB::table('dispatch_hd')->where('orderId', $req->orderId)->delete();
      return redirect()->back()->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>SUKSES</strong> mendrop order']
      ]);
    }
  }
  public function grab_deployer(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/deployer/index.php/login/cek_login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $akun = DB::table('akun')->where('app', 'deployer')->first();
  $fields = array(
      'user_id'=>$akun->user,
      'password'=>$akun->pwd
  );

  $fields_string = http_build_query($fields);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_REFERER, 'http://apps.telkomakses.co.id/deployer/');

    $rough_content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1] ;
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/deployer/index.php/report/order_detail/9748');

    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $result = trim(curl_exec($ch));
    curl_close($ch);

    $result = substr_replace($result, '', 0, strpos($result, "<h5>Project files Lampiran</h5>"));
    $result = substr_replace($result, '', strpos($result, '<div class="text-left mtop20">'), strlen($result));

    $dom = @\DOMDocument::loadHTML(trim($result));

    $links = [];
    $arr = $dom->getElementsByTagName("a"); // DOMNodeList Object
    foreach($arr as $no => $item) { // DOMElement Object
      $href =  str_replace(" ", "+", $item->getAttribute("href"));
      $text = trim(preg_replace("/[\r\n]+/", " ", $item->nodeValue));
      //if($no>2){
        $ch = curl_init();
        //$string = urlencode("&file_name=IMG-KALSEL-758602 VALENSIA.JPG");
        curl_setopt($ch, CURLOPT_URL, $href);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);

        $file = file_get_contents();
        $handle = fopen($file, "r");
        $contents = '';
        while (!feof($handle)) {
            $contents = $contents . fread($handle, 8192);
        }
        file_put_contents(public_path()."/deployer/".$text, $contents);
        fclose($handle);
        file_put_contents(public_path()."/deployer/".$text, $data);
      //}


        //file_put_contents(public_path()."/deployer/".$text, fopen($href, 'r'));
    }

    return $links;


  }


  public function grab_dep(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://oss.telkomakses.co.id/deployer/index.php/login/cek_login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    //user_id=95130417&password=kholifah
    //user_id=95151947&password=didi%4012
    //93130693..tomman1!
    $akun = DB::table('akun')->where('app', 'deployer')->first();
  $fields = array(
      'user_id'=>$akun->user,
      'password'=>$akun->pwd
  );

  $fields_string = http_build_query($fields);
  echo $fields_string;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_REFERER, 'http://oss.telkomakses.co.id/deployer/');


    //http://apps.telkomakses.co.id/deployer/index.php/ta_area

    $rough_content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    var_dump($header_content);
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1] ;
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    //curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/deployer/index.php/report/list_total_witel?regional=REGIONAL%206&witel=KALSEL&witel=KALSEL');
    curl_setopt($ch, CURLOPT_URL, 'https://oss.telkomakses.co.id/deployer/index.php/report/list_total_witel?regional=REGIONAL%206&witel=BANJARMASIN&witel=BANJARMASIN');

    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    //http://apps.telkomakses.co.id/deployer/index.php/ta_area/order_vip
    $result = curl_exec($ch);
    //return $result;
    $sql = "insert ignore into deployer(id,project_name,current_id,proaktif_id,ex_id,region,witel,sto,kategory,order_type,status,customers,scid,odp_name
      ,total_odp,material_value,service_value,umur_status,umur_order) values";
    $sql .= $this->getQuery($result);
    /*
    curl_setopt($ch, CURLOPT_URL, 'http://oss.telkomakses.co.id/deployer/index.php/ta_area/order_vip');
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $sql .= ",".$this->getQuery($result);
    */
    //var_dump($sql);
    //return $sql;
    DB::transaction(function() use($sql) {
      DB::table('deployer')->truncate();
      DB::statement($sql);
      DB::table('deployer_sync')
            ->where('id', 1)
            ->update([
              'last'   => DB::raw('NOW()')
            ]);
      DB::connection('mysql3')->table('deployer')->truncate();
      DB::connection('mysql3')->statement($sql);
      DB::connection('mysql3')->table('deployer_sync')
            ->where('id', 1)
            ->update([
              'last'   => DB::raw('NOW()')
            ]);
    });
  }
  private function getQuery($result){
    $result = str_replace('<tr class="even pointer">','<tr>',$result);
    $result = str_replace('<td class=" last">','<td>',$result);
    $result = str_replace('<td class=" ">','<td>',$result);
    $result = str_replace('<table id="example" class="table table-striped responsive-utilities jambo_table">','<table>',$result);
    $result = str_replace('<thead>','',$result);
    $result = str_replace('<tbody>','',$result);
    $result = str_replace('</thead>','',$result);
    $result = str_replace('</tbody>','',$result);
    //return $result;
    $dom = @\DOMDocument::loadHTML(trim($result));

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            0 =>'ID',
            'Project_Name',
            'Current_ID',
            'Proactive_ID',
            'Ex_ID',
            'Region',
            'Witel',
            'STO',
            'Kategory',
            'Order_Type',
            'Status',
            'Customers',
            'SCID',
            'ODP_Name',
            'Total_ODP',
            'Material_Value',
            'Service_Value',
            'Umur_Status',
            'Umur_Order'
        );
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        //print_r($cells);
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }

        $result[] = $data;

    }
    $data = $result;
    $count = count($data);
    $sql ='';

    for ($i = 0; $i < $count; $i++) {
      $sparator = ", ";
      if($i==0){
        $sparator = "";
      }
      $sql .= $sparator."(".$data["$i"]["ID"].",'".$data["$i"]["Project_Name"]."','".$data["$i"]["Current_ID"]."','".$data["$i"]["Proactive_ID"]."'
        ,'".$data["$i"]["Ex_ID"]."'
        ,'".$data["$i"]["Region"]."'
        ,'".$data["$i"]["Witel"]."'
        ,'".$data["$i"]["STO"]."'
        ,'".$data["$i"]["Kategory"]."'
        ,'".$data["$i"]["Order_Type"]."'
        ,'".$data["$i"]["Status"]."'
        ,'".$data["$i"]["Customers"]."'
        ,'".$data["$i"]["SCID"]."'
        ,'".$data["$i"]["ODP_Name"]."'
        ,'".$data["$i"]["Total_ODP"]."'
        ,'".$data["$i"]["Material_Value"]."'
        ,'".$data["$i"]["Service_Value"]."'
        ,'".$data["$i"]["Umur_Status"]."'
        ,'".$data["$i"]["Umur_Order"]."')";
    }
    return $sql;
  }
  public function compare(){
    $list = DB::select('
      SELECT a.scid as depscid,a.*,b.* from deployer a left join project b on a.id=b.id_dep where b.id_dep is null and a.status in("SURVEY","Design LLD ","Construction ", "Completed ")
    ');
    $sql='';
    $no=1;
    $content = '';
    $t2='';
    foreach($list as $row) {
      $sparator = ", ";
      if($no==1){
        $sparator = "";
      }
      $content .= $no.". ".$row->project_name."(".$row->id.")\n";
      $sql .= $sparator."('".$row->proaktif_id."','".$row->project_name."','".$row->sto."','KALSEL',".$row->id.",'".date("Y/m/d")."','FTTH DEPLOYER','WITEL','START',1,'".date("Y/m/d")."','AUTO-GRAB','".$row->kategory."','".$row->order_type."')";
      $no++;
    }
    $sql1='';
    if($sql!=''){
      $sql1 = 'insert into project(id_project,nama_site,sto_p,witel,id_dep,toc_tgl,kategori,sponsor,status,modp,tgl_i,plup,prov_type,order_type) values';
      $sql1 .= $sql;
      DB::statement($sql1);
      $contents = "NEW ORDER DEPLOYER AT TOMMAN STARTING : \n".$content;
      exec("php /home/ta/htdocs/tomman_bot/sendTelegramApi.php 52369916 \"$contents\"");//kokoh
      exec("php /home/ta/htdocs/tomman_bot/sendTelegramApi.php 97995819 \"$contents\"");//didi
      exec("php /home/ta/htdocs/tomman_bot/sendTelegramApi.php 92737094 \"$contents\"");//udin
      //exec("php /home/ta/htdocs/tomman_bot/sendTelegramApi.php -96983703 \"$contents\"");//tommman
      exec("php /home/ta/htdocs/tomman_bot/sendTelegramApi.php -142967328 \"$contents\"");//vvip
    }
    $proaktif = DB::select('
      SELECT * FROM deployer left join project on deployer.id=project.id_dep WHERE deployer.proaktif_id != project.id_proaktif
    ');
    $no=1;
    $content_pro='';
    foreach($proaktif as $row) {
      $content_pro .= $no.". ".$row->project_name."(".$row->proaktif_id.")\n";
      DB::table('project')
      ->where('id_dep', $row->id)
      ->update([
        'id_proaktif' => $row->proaktif_id
        ]);
      $no++;
    }
    if($content_pro!=''){
      $contents= "NEW ID PROJECT :\n".$content_pro;
      //exec("php /home/ta/htdocs/tomman_bot/sendTelegramApi.php -96983703 \"$contents\"");//tommman
      exec("php /home/ta/htdocs/tomman_bot/sendTelegramApi.php -142967328 \"$contents\"");//vvip
    }
    DB::table('deployer_sync')
      ->where('id', 2)
      ->update([
        'last'   => DB::raw('NOW()')
        ]);
  }
  public function grab_roc_excel(){

    $ch = curl_init();
    $fields = array(
      "sql"=>"select REGIONAL, WITEL, KANDATEL, CMDF, RK, ND_TELP, ND_INT, GAUL_B0, LAPUL, TROUBLE_NO, TROUBLE_OPENTIME, HEADLINE, KELUHAN_DESC, STATUS, HARI, EMOSI_PLG, PRODUK_GGN, TIPE_TIKET, LOKER_DISPATCH, LOKER_ID_DISPATCH, CHANNEL, STO, URGENSI, PRIORITY, UNIT, IS_GAMAS, GNOM_RESSOURCE, INDI_TYPE,
status_voice, tgl_ukur_voice, last_program, last_view,
status_inet, upload, download, clid, snr_up, snr_down, olt_rx, onu_rx, status_redaman, ip_ne, node_id_ukur, status_onu, last_start, tgl_ukur_inet

from rock_detail_ggn left join rock_ukur_inet on nd_int=no_inet
left join rock_ukur_iptv on nd_int=no_iptv
left join rock_ukur_voice on nd_telp=no_voice

WHERE  regional='REGIONAL 6'  and  witel='KALSEL'  and  1=1 and  1=1  and  is_gamas='0'  order by trouble_no",
      'nama_file'=>"report_ggn_"
    );

    $fields_string = http_build_query($fields);
    curl_setopt($ch, CURLOPT_URL, 'http://rock.telkom.co.id/downloadreport.php');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    //ccan:http://10.62.166.52/nonadetail.php?reg=6&cust=corp&jam=&wtl=KALSEL
    //personal:http://10.62.166.52/nonadetail.php?reg=6&cust=psnl&waktu=&wtl=KALSEL
    //all:http://10.62.166.52/nonadetail.php?reg=6&cust=rkap&waktu=&wtl=KALSEL
    $result = curl_exec($ch);
    $str = str_getcsv($result);
    var_dump($str);
    //$lines = array_map("rtrim", explode("\n", $result));
    //$skuList = substr($result, 5);
    //dd($result);
  }
  public function grab_roc($url){
    //personal
    $ch = curl_init();
    //curl_setopt($ch, CURLOPT_URL, 'http://asr.rockal.id:7777/login.php');
    curl_setopt($ch, CURLOPT_URL, 'http://asr.rockal.id:7777/login.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "usr=roc_nas&pwd=roc#nas");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $rough_content = curl_exec($ch);
    if (curl_error($ch)) {
        echo curl_error($ch);
    }
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0] ;
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    //ccan:http://10.62.166.52/nonadetail.php?reg=6&cust=corp&jam=&wtl=KALSEL
    //personal:http://10.62.166.52/nonadetail.php?reg=6&cust=psnl&waktu=&wtl=KALSEL
    //all:http://10.62.166.52/nonadetail.php?reg=6&cust=rkap&waktu=&wtl=KALSEL
    $result = curl_exec($ch);
    if (curl_error($ch)) {
        echo curl_error($ch);
    }
    $columns = array(
            1 => 'No_Ticket',
            'Alpro',
            'Tgl_Open',
            'Jam',
            'Layanan',
            'Status_Link',
            'Cooper',
            'Ftth',
            'Usage'
        );
    $html = str_replace("<br>","+",$result);
    $html = str_replace("<span class='label label-danger'>","+",$html);
    $html = str_replace("<span class='label label-warning'>","+",$html);
    $html = str_replace("<span class='label label-success'>","+",$html);
    $dom = @\DOMDocument::loadHTML(trim($html));

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }

        $result[] = $data;
    }
    return $result;

  }
  public function insert_roc($results, $ta){
    //var_dump($results);
    $sql = "insert ignore into roc(no_tiket,no_telp,no_speedy,tgl_open,tgl_ukur,tgl_lv,headline,lapul,umur,loker,loker_ta,alpro,sto,paket,line_profile,sto_trim,assigned) values";
    $sql2 = "insert ignore into roc_active(no_tiket,no_telp,no_speedy,tgl_open,tgl_ukur,tgl_lv,headline,lapul,umur,loker,loker_ta,alpro,sto,paket,line_profile,sto_trim,assigned) values";
    $count=count($results);
    for ($i = 0; $i < $count; $i++) {
      $sparator = ", ";
      if($i==0){
        $sparator = "";
      }
      $data = $results["$i"]["No_Ticket"];
      $tiket = explode("+", $data);
      $no_tiket = trim($tiket[0]);


      $no_telp = trim($tiket[3]);
      $no_speedy = trim($tiket[4]);
      $tgl_open = $results["$i"]["Tgl_Open"];
      $data = $results["$i"]["Layanan"];
      $jam = $results["$i"]["Jam"];
      $jam = explode("+",$jam);
      $jam = $jam[0];
      $headline = $data;
      $assign = explode('#',$data);
      if (count($assign)>1) {$assigned = $assign[1]; } else { $assigned = 0; }
      $headline .= "+".$results["$i"]["Status_Link"]."+".$results["$i"]["Cooper"]."+".$results["$i"]["Ftth"]."+".$results["$i"]["Usage"];
      $cari = strpos($data, "Lapul : ");
      $cari += 8;
      $lapul = substr($data,$cari,1);
      $loker = "auto";
      $data = $results["$i"]["Alpro"];
      $data = explode("+", $data);
      if(isset($data[0]))
        $alpro = $data[0];
      if(isset($data[1]))
        $sto = str_replace("/", "", $data[1]);
      $sto_trim = substr($sto,0,3);
      if((strpos($sto,'SB') !== false) || (strpos($sto,'SK') !== false) || (strpos($sto,'SL') !== false) || (strpos($sto,'ST') !== false))
        $sto_trim = substr($sto,1,3);
      if((strpos($sto, 'SS') !== false) || (strpos($sto, 'MS') !== false) || (strpos($sto, 'GP') !== false))
        $sto_trim = substr($sto,2,3);
      if(isset($data[3]))
        $paket = $data[3];
      if(isset($data[4]))
        $line_profile = $data[4];
      $sql .= $sparator."('".$no_tiket."','".$no_telp."','".$no_speedy."','".$tgl_open."','','',".DB::connection()->getPdo()->quote(trim($headline)).",'".$lapul."','".$jam."','".$loker."',".$ta.",'".$alpro."','".$sto."','".$paket."','".$line_profile."','".$sto_trim."',".DB::connection()->getPdo()->quote(trim($assigned)).")";
      $sql2 .= $sparator."('".$no_tiket."','".$no_telp."','".$no_speedy."','".$tgl_open."','','',".DB::connection()->getPdo()->quote(trim($headline)).",'".$lapul."','".$jam."','".$loker."',".$ta.",'".$alpro."','".$sto."','".$paket."','".$line_profile."','".$sto_trim."',".DB::connection()->getPdo()->quote(trim($assigned)).")";

      $check = DB::table('roc')
                    ->where('no_tiket',$no_tiket)
                    ->get();
      if (count($check)>0){
        DB::table('roc')->where('no_tiket',$no_tiket)->update([
          'headline' => $headline,
          'no_telp' => $no_telp,
          'no_speedy' => $no_speedy,
          'umur' => $jam,
          'assigned' => $assigned
        ]);
      }

    }
    print_r($sql);
    if ($count>0){
	    DB::statement($sql);
	    DB::statement($sql2);
    } else {
	    echo "EMPTY RESULT<br />	";
    }

  }
  public function sync_roc_personal(){
    $result = $this->grab_roc('http://asr.rockal.id:7777/nonadetail.php?reg=6&cust=psnl&waktu=&wtl=KALSEL');
    $this->insert_roc($result, 1);

  }
  public function sync_roc_corp(){
    //$result = $this->grab_roc('http://10.62.166.52/nonadetail.php?reg=6&cust=corp&jam=&wtl=KALSEL');
    $result = $this->grab_roc('http://asr.rockal.id:7777/nonadetail.php?reg=6&cust=corp&jam=&wtl=KALSEL');
    $this->insert_roc($result, 2);
  }
  public function roc_sync(){
    DB::table('roc_active')->truncate();
    $this->sync_roc_corp();
    $this->sync_roc_personal();
  }
  public function dispatch($id){
    // ukur ibooster
    $getData = DB::table('data_nossa_1_log')->where('Incident',$id)->first();
    $check_fitur = DB::table('fitur')->first();
    if ($check_fitur->ibooster == 1){
    	$resultIbooster = $this->grabIboosterbyIN($getData->Incident,'dispatch');
		} else {
			#skip grabIbooster
		}
    $sektor = DB::SELECT('
      SELECT
      a.chat_id as id,
      a.title as text
      FROM
        group_telegram a
    ');
    $data = DB::select('
      SELECT m.*,d.*,r1.id_regu AS id_r,
        r1.uraian,
        r2.id_regu as id_regu2,
        m.Incident as no_tiket,
        m.Owner_Group as loker_ta,
        m.Technology as alpro,
        DATE(m.UmurDatetime) as tanggal,
        DATE(d.jadwal_manja) as tgl_jadwal_manja,
        TIME(d.jadwal_manja) as jam_jadwal_manja,
        r.*,
        ib.*,
        gt.*,
        ee.*,
        m.jenis_ont
      FROM data_nossa_1_log m left join dispatch_teknisi d on m.Incident = d.Ndem
      LEFT JOIN roc r ON m.Incident = r.no_tiket
      left join regu r1 on d.id_regu = r1.id_regu
      left join regu r2 on m.Assigned_to = r2.crewid
      left join ibooster ib ON m.Incident = ib.order_id
      left join group_telegram gt ON r1.mainsector = gt.chat_id
      LEFT JOIN Data_Pelanggan_Starclick ee ON ee.internet = m.Service_No  AND ee.orderStatusId = 7 AND ee.jenisPsb = "AO|BUNDLING"
      WHERE m.Incident = ?
    ',[
      $id
    ])[0];

    $cek_sektor_by_mapping = DB::SELECT('SELECT b.title FROM alpro_owner a LEFT JOIN group_telegram b ON a.nik = b.TL_NIK WHERE a.alproname = "'.$data->alproname.'" GROUP BY b.title');
    // $data = DB::select('
    //   SELECT m.*,d.*,r1.id_regu AS id_r,
    //     r1.uraian,
    //     r2.id_regu as id_regu2,
    //     m.no_tiket,
    //     m.no_tiket as Incident,
    //     m.loker_ta,
    //     m.alpro,
    //     m.tglOpen as Reported_Date,
    //     m.tglOpen as UmurDatetime,
    //     r1.uraian as Assigned_to,
    //     0 as Assigned_by,
    //     0 as Workzone,
    //     0 as Hold,
    //     0 as Datek,
    //     0 as Summary,
    //     DATE(m.tglOpen) as tanggal,
    //     DATE(d.jadwal_manja) as tgl_jadwal_manja,
    //     TIME(d.jadwal_manja) as jam_jadwal_manja,
    //     r.*,
    //     ib.*
    //   FROM roc m left join dispatch_teknisi d on m.no_tiket = d.Ndem
    //   LEFT JOIN roc r ON m.no_tiket = r.no_tiket
    //   left join regu r1 on d.id_regu = r1.id_regu
    //   left join regu r2 on d.id_regu = r2.crewid
    //   left join ibooster ib ON m.no_tiket = ib.order_id
    //   WHERE m.no_tiket = ?
    // ',[
    //   $id
    // ])[0];
    $regu = DB::select('
      SELECT a.id_regu as id,a.uraian as text, b.title
      FROM regu a
        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      WHERE
        ACTIVE = 1
    ');
    return view('assurance.dispatch', compact('data','regu','sektor','cek_sektor_by_mapping'));
  }
  public function dispatch_manual(){
    $regu = DB::select('
      SELECT a.id_regu as id,a.uraian as text, b.title
      FROM regu a
        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      WHERE
        ACTIVE = 1
    ');
    return view('dispatch.manual', compact('regu'));
  }

  public function dispatch_manual_save(Request $request){
    $this->validate($request,[
        'tglOpen'    => 'required',
        'jamOpen'    => 'required'
    ],[
        'tglOpen.required'  => 'Tgl Open Diisi',
        'jamOpen.required'  => 'Jam Open Diisi'
    ]);

    $a = $request->input('loker');
    if ($a==3 || $a == 4){
        $this->validate($request,[
            'refSc'   => 'required',
            'refSc'   => 'numeric'
        ],[
            'refSc.required'  => '"Referensi SC" Diisi Bang !',
            'refSc.numeric'   => 'Inputan Angka'
        ]);
    }

    // refRc Valid
    // dd($request->refSc);
    if ($a==3 || $a==4 ){
        $exis  = DB::table('Data_Pelanggan_Starclick')->where('orderId',$request->refSc)->first();
        if (!$exis){
            return back()->with('alerts',[['type' => 'danger', 'text' => 'Referensi SC Tidak Valid']]);
        };
    };

    // dd('ok');

    $now = $request->tglOpen.' '.$request->jamOpen.':00';
    $openTanggal = date('Y-m-d H:i:s',strtotime($now));

    $auth = session('auth');
    $check = DB::table('roc')
              ->where('no_tiket',$request->input('no_tiket'))
              ->get();
    if (count($check)>0) {
      DB::table('roc')->where('no_tiket',$request->input('no_tiket'))->update([
        'no_tiket' => $request->input('no_tiket'),
        'loker_ta' => $request->input('loker'),
        'alpro'    => $request->input('alpro'),
        'tglOpen'  => $openTanggal,
        'ketFull'  => $request->ket,
        'refSc'    => $request->refSc,
        'jenis_ont'=> $request->jenis_ont
      ]);
    } else {
      DB::table('roc')->insert([
        'no_tiket' => $request->input('no_tiket'),
        'loker_ta' => $request->input('loker'),
        'alpro'    => $request->input('alpro'),
        'tglOpen'  => $openTanggal,
        'ketFull'  => $request->ket,
        'refSc'    => $request->refSc,
        'jenis_ont'=> $request->jenis_ont
      ]);
    }

    $exists = DB::select('select * from dispatch_teknisi where Ndem = ? and (dispatch_by = 2 or dispatch_by = 4)',[
      $request->input('no_tiket')
    ]);

    if ($request->loker=="3"){
        $dispatchBy = "4";
    }
    else if ($request->loker=="4"){
        $dispatchBy = "6 ";
    }
    else{
      $dispatchBy = "2";
    };
    if(count($exists)){
      $data = $exists[0];
      DB::table('dispatch_teknisi')
          ->where('Ndem', $request->input('no_tiket'))
          ->update([
            'updated_at'   => DB::raw('NOW()'),
            'updated_by'   => $auth->id_karyawan,
            'tgl'          => $request->input('tgl'),
            'id_regu'      => $request->input('id_regu')
          ]);

    }else{
        DB::table('dispatch_teknisi')->insert([
          'created_at'    => DB::raw('NOW()'),
          'updated_at'    => DB::raw('NOW()'),
          'updated_by'    => $auth->id_karyawan,
          'tgl'           => $request->input('tgl'),
          'id_regu'       => $request->input('id_regu'),
          'Ndem'          => $request->input('no_tiket'),
          'dispatch_by'   => $dispatchBy
        ]);

    }
    return redirect('/assurance')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> men-dispatch WO GANGGUAN']
      ]);
  }

  public function inputAction(){
    $action = Input::get('action');
    $query = DB::SELECT('select * from psb_laporan_action a left join psb_laporan_penyebab b ON a.JENIS = b.kategori WHERE a.laporan_action_id = "'.$action.'"');
    $data = array();
    foreach ($query as $result){
      $data[] = array("id"=>$result->idPenyebab,"text"=>$result->penyebab);
    }
    return json_encode($data);
  }

  public function inputRegu(){
    $sektor = Input::get('sektor');
    if ($sektor==''){
        $regu = '';
    }
    else{
      $regu = DB::select('
        SELECT a.id_regu as id,a.uraian as text
        FROM regu a
        WHERE
          a.mainsector = "'.$sektor.'" AND
          a.ACTIVE=1
      ');
    }

    return json_encode($regu);
  }


  public function input($id, Request $req)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT a.*,
      dt.id as id_dt,
      c.*,
      d.*,
      b.jenis as kategori_penyebab
      FROM psb_laporan a
      LEFT JOIN dispatch_teknisi dt ON a.id_tbl_mj = dt.id
      LEFT JOIN psb_laporan_action b ON a.action = b.laporan_action_id
      LEFT JOIN psb_laporan_penyebab c ON a.penyebabId = c.idPenyebab
      LEFT JOIN psb_laporan_status d ON a.status_laporan = d.laporan_status_id
      WHERE id_tbl_mj = ?
    ',[
      $id
    ]);
    $get_Ndem = DB::table('dispatch_teknisi')->where('id',$id)->get()[0];
    $data_log = DB::SELECT('
      SELECT
      *
      FROM psb_laporan_log a
      LEFT JOIN user b ON a.created_by = b.id_user
      LEFT JOIN 1_2_employee c ON b.id_karyawan = c.nik
      LEFT JOIN psb_laporan_status d ON a.status_laporan = d.laporan_status_id
      WHERE
        a.Ndem = "'.$get_Ndem->Ndem.'" AND
        a.created_by <> 0
      ORDER BY created_at DESC
    ');

    if (count($exists)) {
      $data = $exists[0];
      if ($data->kategori_penyebab<>""){
        $penyebab = $data->kategori_penyebab;
      } else {
        $penyebab = "LOGIC";
      }
      $check_foto = DB::SELECT('
        SELECT
        a.name
        FROM cat_foto a
        WHERE
        a.'.$penyebab.' = 1
      ');
      // TODO: order by most used
      // $materials = DB::select('
      //   SELECT
      //     i.id_item, i.nama_item,
      //     COALESCE(m.qty, 0) AS qty
      //   FROM item i
      //   LEFT JOIN
      //     psb_laporan_material m
      //     ON i.id_item = m.id_item
      //     AND m.psb_laporan_id = ?
      //   WHERE
      //     i.grup = 1
      //   ORDER BY id_item
      // ', [
      //   $data->id
      // ]);

      // tes
        // periode rfc
        // $periode = date('m/Y');
        $tglWo = DB::table('dispatch_teknisi')->where('id',$id)->first();
        $periode = date('m/Y',strtotime($tglWo->tgl));
        // $dt = DB::table('data_nossa_1_log')->where('Incident',$tglWo->Ndem)->first();
        // if (count($dt)<>0){
        //     $periode = date('m/Y',strtotime($dt->Reported_Date));
        // }

        $bulanEnd = date('m',strtotime($tglWo->tgl));
        if ($bulanEnd==1){
            $endBulan = '12';
            $endTahun = date('Y',strtotime($tglWo->tgl));

            $peiodeEnd = $endBulan.'/'.$endTahun-1;
        }
        else{
            $endBulan = $bulanEnd-1;
            $endTahun = date('Y',strtotime($tglWo->tgl));

            $peiodeEnd = $endBulan.'/'.$endTahun;
        };

        if ($auth->level==2 || $auth->level==15 || $auth->level==46){
        $materials = DB::select('
          SELECT
            i.id_item, i.nama_item,
            COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo, i.jmlTerpakai
          FROM rfc_sal i
          LEFT JOIN
            psb_laporan_material m
            ON i.id_item_bantu = m.id_item_bantu
          Where
            (i.rfc LIKE "%'.$periode.'" OR i.rfc LIKE "%'.$peiodeEnd.'") AND
            m.psb_laporan_id = ?
          ORDER BY i.id_item
        ', [
          $data->id
        ]);
      }
      else{
        $materials = DB::select('
          SELECT
            i.id_item, i.nama_item,
            COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo, i.jmlTerpakai
          FROM rfc_sal i
          LEFT JOIN
            psb_laporan_material m
            ON i.id_item_bantu = m.id_item_bantu
            AND m.psb_laporan_id = ?
          WHERE
           (i.rfc LIKE "%'.$periode.'" OR i.rfc LIKE "%'.$peiodeEnd.'") AND
           i.nik="'.$auth->id_user.'"
          ORDER BY id_item
        ', [
          $data->id
        ]);
      }
    }
    else {
      $data = new \StdClass;
      $data->id = null;
      $data->idPenyebab = null;
      $data->penyebab = null;
      $data->typeont = null;
      $check_foto = array();
      // TODO: order by most used

      // $materials = DB::select('
      //   SELECT a.id_item, a.nama_item, 0 AS qty
      //   FROM item a,  psb_laporan_material b
      //   WHERE a.id_item=b.id_item
      //   GROUP BY a.id_item
      //   ORDER BY a.id_item
      // ');

      $materials = DB::select('
        SELECT id_item, nama_item, 0 AS qty, (jumlah - jmlTerpakai) as saldo, rfc, jmlTerpakai
        FROM rfc_sal where nik="'.$auth->id_user.'" AND jumlah<>0
        ORDER BY rfc
      ');
    }
    $nte1 = DB::select('select i.id as id, i.sn as text
        FROM nte i
        LEFT JOIN
          psb_laporan_material m
          ON i.id = m.id_item
          AND m.psb_laporan_id = ?
        WHERE m.type=2
        ORDER BY id_item',[
        $data->id
      ]);
    $no = 1;
    $ntes = '';
    foreach($nte1 as $n){
      if($no == 1){
        $ntes .= $n->id;
        $no = 2;
      }else{
        $ntes .= ', '.$n->id;
      }
    }
    $nte = DB::select('select  sn as id, sn as text from nte
      where position_key = ?',[
        $auth->id_karyawan
      ]);
    $project = DB::select('
      SELECT
        *,
        d.id as id_dt,
        roc.*,
        dn1log.Workzone as neSTO,
        dn1log.Datek as neRK,
        dn1log.Service_ID as neND_TELP,
        dn1log.Service_No as neND_INT,
        dn1log.Service_Type as nePRODUK_GGN,
        dn1log.Summary as neHEADLINE,
        dn1log.Owner_Group as neLOKER_DISPATCH,
        dn1log.Customer_Name as neKANDATEL,
        dn1log.Incident as no_tiket,
        dn1log.jenis_ont
      FROM dispatch_teknisi d
      LEFT JOIN roc on d.Ndem = roc.no_tiket
      LEFT JOIN nonatero_excel_bank ne on d.Ndem = ne.TROUBLE_NO
      LEFT JOIN data_nossa_1_log dn1log ON d.Ndem = dn1log.Incident
      WHERE
      d.id = ?
    ',[
      $id
    ])[0];
    $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","ASR")');

    if ($auth->level==15){
        $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","ASR","LOGIC")');
    };

    $get_laporan_action = DB::SELECT('SELECT laporan_action_id as id, action as text FROM psb_laporan_action WHERE jenis NOT IN ("DISABLE","EMPTY")');
    $get_laporan_penyebab = DB::select('SELECT idPenyebab as id, penyebab as text from psb_laporan_penyebab');
    $get_laporan_odp = DB::select('SELECT idKonOdp as id, kondisi_odp as text from psb_laporan_odp');
    $photoInputs = $this->assuranceInputs;

    $getNdem = DB::table('dispatch_teknisi')->where('id',$id)->first();
    $Ndem = $getNdem->Ndem;
    $redamanIboster = NULL;
    if ($exists){
        $redamanIboster = $exists[0]->redaman_iboster;
    };

    return view('assurance.input', compact('inputPenyebab','check_foto','data_log','data', 'project', 'materials', 'photoInputs', 'ntes', 'nte', 'nte1','get_laporan_status','get_laporan_action', 'get_laporan_penyebab', 'get_laporan_odp','Ndem','redamanIboster'));
  }
  public function dispatchSave(Request $request, $id){
    // cek diiboster disble dlu
    // $data = AssuranceModel::getDataIboosterByIn($id);
    // if (count($data)==null){
    //     return back()->with('alerts',[['type' => 'danger', 'text' => "Ukur <b>IBOSTER</b> Dulu Sebelum DISPATCH !!!"]]);
    // };

    $auth = session('auth');
    $exists = DB::select('select * from dispatch_teknisi where Ndem = ? and dispatch_by = 2',[
      $id
    ]);
    $status_laporan = 6;
    DB::transaction(function() use($request, $id, $auth, $status_laporan) {
       DB::table('psb_laporan_log')->insert([
         'created_at'      => DB::raw('NOW()'),
         'created_by'      => $auth->id_karyawan,
         'status_laporan'  => $status_laporan,
         'id_regu'         => $request->input('id_regu'),
         'jadwal_manja'    => $request->input('tgl')." ".$request->input('jamManja'),
         'catatan'         => "DISPATCH at ".date('Y-m-d'),
         'Ndem'            => $id
       ]);
     });
     DB::transaction(function() use($request, $id, $auth) {
       DB::table('dispatch_teknisi_log')->insert([
         'updated_at'          => DB::raw('NOW()'),
         'updated_by'          => $auth->id_karyawan,
         'jadwal_manja'        => $request->input('tgl')." ".$request->input('jamManja'),
         'tgl'                 => $request->input('tgl'),
         'id_regu'    	       => $request->input('id_regu'),
         'manja'               => $request->input('manja'),
         'manja_status'        => $request->input('manja_status'),
         'updated_at_timeslot' => $request->input('timeslot'),
         'Ndem'                => $id
       ]);
     });
    $cek_roc = DB::table('roc')->Where('no_tiket',$id)->get();
    if (count($cek_roc)>0){
     DB::table('roc')
          ->where('no_tiket',$id)
          ->update([
            'loker_ta' => $request->input('loker'),
            'alpro' => $request->input('alpro'),
            'jenis_ont' => $request->input('jenis_ont')
          ]);
    } else {
      DB::table('roc')
           ->insert([
             'no_tiket' => $id,
             'loker_ta' => $request->input('loker'),
             'alpro' => $request->input('alpro'),
             'jenis_ont' => $request->input('jenis_ont')
           ]);
    }
    if(count($exists)){
      $data = $exists[0];
      DB::table('psb_laporan')->where('id_tbl_mj',$data->id)
            ->update([
              'status_laporan' => 6,
              'action' => ''
            ]);
      DB::table('dispatch_teknisi')
          ->where('id', $data->id)
          ->update([
            'updated_at'   => DB::raw('NOW()'),
            'updated_by'   => $auth->id_karyawan,
            'tgl'          => $request->input('tgl'),
            'jadwal_manja' => $request->input('tgl')." ".$request->input('jamManja'),
            'id_regu'      => $request->input('id_regu')
          ]);

    }else{
        DB::table('dispatch_teknisi')->insert([
          'created_at'    => DB::raw('NOW()'),
          'updated_at'    => DB::raw('NOW()'),
          'updated_by'    => $auth->id_karyawan,
          'tgl'           => $request->input('tgl'),
          'jadwal_manja'  => $request->input('tgl')." ".$request->input('jamManja'),
          'id_regu'       => $request->input('id_regu'),
          'Ndem'          => $id,
          'dispatch_by'   => 2
        ]);
    }


    // save ont di data_nossa_1_log
    // $getData = DB::table('data_nossa_1_log')->where('Incident',$id)
    DB::table('data_nossa_1_log')->where('Incident',$id)->update([
        'jenis_ont'   => $request->input('jenis_ont')
    ]);

    return redirect('/assurance')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> men-dispatch WO GANGGUAN']
      ]);
  }
  public function matriks_dispatch()
  {
    $auth = session('auth');
    $list = DB::select('
      SELECT id_regu AS id_r,(
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian, dispatch_teknisi.id as id_dt,Ndem,id_regu,dispatch_teknisi.dispatch_by
      FROM dispatch_teknisi
      WHERE  dispatch_teknisi.updated_at like "'.date('Y-m-d').'%" and id_regu is not null order by id_regu
    ');

    $countWo = count($list);
    $countOuter = 0;
    $countInner = 0;
    $outer = array();
    $lastTeknisi = '';
    foreach($list as $row) {
        if ($lastTeknisi == $row->id_regu) {
          $outer[$row->id_regu]['WO'][] = array('NDEM' => $row->Ndem,'divisi' => $row->dispatch_by);
        }
        else {
          $outer[$row->id_regu] = array('head' => $row->uraian, 'WO' => array(array('NDEM'=>$row->Ndem,'divisi' => $row->dispatch_by)));
          $lastTeknisi = $row->id_regu;
        }
        $countOuter++;
    }
    //return ($outer);
    $regu = DB::select('SELECT uraian,id_regu
        FROM regu
        WHERE uraian like "[FTTH%" or uraian like "[CCAN%" or uraian like "[NODE-B%" order by wilayah, uraian
    ');
    return view('assurance.matrik', compact('outer', 'regu'));
  }

  function encrypt($id){
    // echo "enkrip : ".$id."<br />";
    $en1 = str_replace('1','K',$id);
    $en2 = str_replace('2','a',$en1);
    $en3 = str_replace('3','N',$en2);
    $en4 = str_replace('4','t',$en3);
    $en5 = str_replace('5','u',$en4);
    $en6 = str_replace('6','r',$en5);
    $en7 = str_replace('7','J',$en6);
    $en8 = str_replace('8','f',$en7);
    $en9 = str_replace('9','X',$en8);
    $en0 = str_replace('0','Y',$en9);
    return $en0;
  }

  public function gaulsektor($periode){
    $get_gaulsektor = AssuranceModel::gaulsektor($periode);
    return view('assurance.gaulsektor',compact('get_gaulsektor'));
  }

  public function gaulsektorlist($sektor){
    $title = 'LIST GAUL '.$sektor;
    $get_gaulsektorlist = AssuranceModel::gaulsektorlist($sektor);
    return view('assurance.gaulsektorlist',compact('title','get_gaulsektorlist'));
  }

  public function save(Request $request, $id)
  {
    $a = @$request->input('status');
    $b = @$request->input('action');
    if (@$request->input('penyebab')<>NULL){
      $c = $request->input('penyebab');
    } else {
      $c = "1";
    }
    $auth = session('auth');
    $disp = DB::table('dispatch_teknisi')
      ->select('dispatch_teknisi.*', 'Data_Pelanggan_Starclick.jenisPsb')
      ->leftJoin('Data_Pelanggan_Starclick', 'dispatch_teknisi.Ndem', '=', 'Data_Pelanggan_Starclick.orderId')
      ->where('id', $id)
      ->first();

    $materials = json_decode($request->input('materials'));
    $upload = $this->handleFileUpload($request, $id, $c);
    if (@$upload['type']=="danger")
    return back()->with('alerts', [
      ['type' => 'danger', 'text' => $upload['message']]
    ]);
    print_r($upload['waktu']);
    $ntes = explode(',', $request->input('nte'));
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ]);

    $input = $request->only([
      'status'
    ]);
    $rules = array(
      'status' => 'required'
    );
    $messages = [
      'status_nossa.required' => 'Hanya bisa dilaporkan UP jika MYI / Nossa sudah tech closed',
      'status.required' => 'Silahkan isi kolom "Status" Bang!',
      'penyebab.required' => 'Silahkan isi kolom "Penyebab" Bang!',
      'action.required' => 'Silahkan isi kolom "action" Bang!',
      'kordinat_pelanggan.required' => 'Silahkan isi kolom "kordinat_pelanggan" Bang!',
      'nama_odp.required' => 'Silahkan isi kolom "nama_odp" Bang!',
      'kordinat_odp.required' => 'Silahkan isi kolom "kordinat_odp" Bang!',
      // 'noTelp.required' => 'Silahkan isi kolom "No. Telpon" Bang!',
      'noTelp.numeric' => '"No. Telpon" Tidak Valid Bang!',
      'flag_LABEL.required' => 'Silahkan upload Foto "Label" Bang!',
      'flag_Foto_Action.required' => 'Silahkan upload Foto "Foto_Action" Bang!',
      'flag_ODP.required' => 'Silahkan upload Foto "ODP" Bang!',
      'flag_Berita_Acara.required' => 'Silahkan upload Foto "Berita_Acara" Bang!',
      'flag_Sambungan_Kabel_DC.required' => 'Silahkan upload Foto "Sambungan_Kabel_DC" Bang!',
      'flag_Pullstrap_S_Clamp.required' => 'Silahkan upload Foto "Pullstrap_S_Clamp" Bang!',
      'flag_Stoper.required' => 'Silahkan upload Foto "Stoper" Bang!',
      'flag_Clamp_Hook_Clamp_Pelanggan.required' => 'Silahkan upload Foto "Clamp_Hook_Clamp_Pelanggan" Bang!',
      'flag_Roset.required' => 'Silahkan upload Foto "Roset" Bang!',
      'flag_Patchcord.required' => 'Silahkan upload Foto "Patchcord" Bang!',
      'flag_Slack_Kabel_DC.required' => 'Silahkan upload Foto "Slack_Kabel_DC" Bang!',
      'catatan.required'  => 'Silahkan isi kolom "Catatan" Bang!',
      'flag_Speedtest_before.required'  => 'Silahkan upload Foto "Speedtest_before" Bang!',
      'flag_Speedtest_after.required'   => 'Silahkan upload Foto "Speedtest_after" Bang!',
      'flag_Indikasi_before.required'   => 'Silahkan upload Foto "Indikasi_before" Bang!',
      'flag_Indikasi_after.required'    => 'Silahkan upload Foto "Indikasi_after" Bang!',
      'flag_Hasil_ukur_ODP.required'    => 'Silahkan Upload Foto "Hasil_ukur_ODP" Bang!',
      'flag_Kondisi_ODP.required'       => 'Silahkan Upload Foto "Kondisi ODP" Bang!',
      'konOdp.required'                 => 'Silahkan Isi Kondisi ODP !'
    ];

    $validator = Validator::make($request->all(), $rules, $messages);
    $serviceKet = '';
    $getNossaLog = DB::table('data_nossa_1_log')->where('Incident',$disp->Ndem)->first();
    if(count($getNossaLog)){
      $serviceKet = $getNossaLog->Service_Type;
    };

    // $status_nossa = $request->input('status_nossa');
    // if ($a == 1 && $status_nossa=="BACKEND"){
    //   return redirect()->back()
    //       ->with('alerts', [
    //         ['type' => 'danger', 'text' => 'Hanya bisa dilaporkan UP jika MYI / Nossa sudah tech closed']
    //       ]);
    // }

    if ($serviceKet==''){
      if($a == 1){
        $validator->sometimes('action', 'required', function ($input) {
            return true;
        });
        $validator->sometimes('penyebab', 'required', function ($input) {
            return true;
        });
      }

      if ($a == 2 ){
        $validator->sometimes('flag_Kondisi_ODP', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('konOdp', 'required', function ($input) {
            return true;
        });
      }

      if ($b == 1 || $b == 8 || $b == 10 || $b == 45 ){
          $validator->sometimes('flag_Kondisi_ODP', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('konOdp', 'required', function ($input) {
            return true;
        });
      };

      if($a==3){
          $validator->sometimes('flag_Hasil_ukur_ODP', 'required', function ($input) {
            return true;
          });
      }

      if($a == 2) {
        $validator->sometimes('kordinat_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('nama_odp', 'required', function ($input) {
          return true;
        });
      }
      if($a == 11) {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
      }

      if ($b==19){
        $validator->sometimes('noTelp', 'required', function($input){
          return true;
        });
        $validator->sometimes('noTelp', 'numeric', function($input){
          return true;
        });
      }

      if ($a==1 && ($b==1 || $b==7)){
          $validator->sometimes('flag_Sambungan_Kabel_DC','required', function($input){
              return true;
          });
          $validator->sometimes('flag_LABEL','required', function($input){
              return true;
          });
          $validator->sometimes('flag_Foto_Action','required', function($input){
              return true;
          });
          $validator->sometimes('flag_ODP','required', function($input){
              return true;
          });
          $validator->sometimes('flag_Berita_Acara','required', function($input){
              return true;
          });
          $validator->sometimes('flag_Pullstrap_S_Clamp','required', function($input){
              return true;
          });

          $validator->sometimes('flag_Stoper','required', function($input){
              return true;
          });

          $validator->sometimes('flag_Clamp_Hook_Clamp_Pelanggan','required', function($input){
              return true;
          });

          $validator->sometimes('flag_Roset','required', function($input){
              return true;
          });

          $validator->sometimes('flag_Patchcord','required', function($input){
              return true;
          });

          $validator->sometimes('flag_Slack_Kabel_DC','required', function($input){
              return true;
          });
      };

      if ($c==3 || $c==4 || $c==17){
          $validator->sometimes('catatan', 'required', function($input){
            return true;
          });
      }

      if($c == 3){
          $validator->sometimes('flag_Speedtest_before','required', function($input){
              return true;
          });

          $validator->sometimes('flag_Speedtest_after','required', function($input){
              return true;
          });
      }

      if($c == 17){
          $validator->sometimes('flag_Indikasi_before','required', function($input){
              return true;
          });

          $validator->sometimes('flag_Indikasi_after','required', function($input){
              return true;
          });
      }

      if ($validator->fails()) {
        return redirect()->back()
            ->withInput($request->input())
            ->withErrors($validator)
            ->with('alerts', [
              ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan, Silahkan scroll ke bawah untuk melihat field apa saja yg harus di isi, scroll kebawah!']
            ]);
      }

      if ($b<>''){
          $getAction = DB::table('psb_laporan_action')->where('laporan_action_id',$b)->first();
          if ($getAction->statusmat==1){
              if (count($materials)==0)
              return back()->with('alerts',[['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan, Material Belum Dimasukkan!']]);
          };
      };

      if ($b==1 || $b==7){
          $ada = false;
          foreach ($materials as $material){
            if($material->id_item=='AC-OF-SM-1B' || $material->id_item=='Preconnectorized-1C-100-NonAcc' || $material->id_item=='Preconnectorized-1C-75-NonAcc' || $material->id_item=='Preconnectorized-1C-50-NonAcc' || $material->id_item=='Preconnectorized-1C-150-NonAcc' ){
                $ada = true;
            }
          }

          if ($ada==false){
              return back()->with('alerts',[['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan, Material "DROP CORE" Belum Dimasukkan!']]);
          }
      };

    }

    // ukur ibooster sebelm up
    // if ($a==1){
    //     $getData = DB::table('dispatch_teknisi')
    //                     ->select('data_nossa_1_log.*','psb_laporan.redaman_iboster')
    //                     ->leftJoin('data_nossa_1_log','dispatch_teknisi.Ndem','=','data_nossa_1_log.Incident')
    //                     ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
    //                     ->where('psb_laporan.id_tbl_mj',$id)
    //                     ->first();
    //     // if ($getData->no_internet <> null){
    //     if ($getData->Service_Type == 'INTERNET' || $getData->no_internet <> ''){
    //         if($getData->redaman_iboster==null){
    //             return back()->with('alerts',[['type' => 'danger', 'text' => 'Ukur Ibooster Dulu']]);
    //         }
    //         else{
    //             if ($getData->redaman_iboster > -13 || $getData->redaman_iboster < -25){
    //               return back()->with('alerts',[['type' => 'danger', 'text' => 'Tidak Bisa UP Redaman Ibooster Diijinkan  (> -13) - (< -25)']]);
    //             }
    //         }
    //     }
    // };

    if ($request->input('alpro')<>"") :
    DB::table('roc')
        ->where('no_tiket',$request->input('no_tiket'))
        ->update([
          'alpro' => $request->input('alpro')
        ]);
    endif;

    // kondisi odp status
    $statusOdp = '';
    if ($request->input('konOdp')<>''){
        $dataKonOdp = DB::table('psb_laporan_odp')->where('idKonOdp',$request->input('konOdp'))->first();
        $statusOdp = $dataKonOdp->kondisi_odp;
    };

    if (count($exists)) {
      $data = $exists[0];
      $sendReport = 0;
      if($data->status_laporan!=$request->input('status')){
        $sendReport = 1;
      }

      // update no.telp
      DB::transaction(function() use($request, $disp, $auth) {
          DB::table('roc')->where('no_tiket',$disp->Ndem)->update([
              'no_telp' => $request->noTelp
          ]);
      });

      DB::transaction(function() use($request, $disp, $auth) {
         DB::table('psb_laporan_log')->insert([
           'created_at'      => DB::raw('NOW()'),
           'created_by'      => $auth->id_karyawan,
           'status_laporan'  => $request->input('status'),
           'id_regu'         => $disp->id_regu,
           'catatan'         => $request->input('catatan'),
           'action'          => $request->input('action'),
           'Ndem'            => $disp->Ndem,
           'mttr'            => $this->datediff($disp->updated_at,date('Y-m-d h:i:s')),
           'psb_laporan_id'  => $disp->id,
           'penyebabId'      => $request->input('penyebab')
         ]);
       });
      DB::transaction(function() use($request, $data, $auth, $materials, $ntes, $statusOdp) {
        DB::table('psb_laporan')
          ->where('id', $data->id)
          ->update([
            'modified_at'       => DB::raw('NOW()'),
            'modified_by'       => $auth->id_karyawan,
            'catatan'           => $request->input('catatan'),
            'status_laporan'    => $request->input('status'),
            'action'            => $request->input('action'),
            'kordinat_pelanggan'=> $request->input('kordinat_pelanggan'),
            'nama_odp'          => $request->input('nama_odp'),
            'kordinat_odp'      => $request->input('kordinat_odp'),
            'penyebabId'        => $request->input('penyebab'),
            'kondisi_odp'       => $statusOdp,
            'typeont'           => $request->input('jenis_ont')
          ]);
        $this->incrementStokTeknisi($data->id, $auth->id_karyawan);
        DB::table('psb_laporan_material')
          ->where('psb_laporan_id', $data->id)
          ->delete();
        foreach($ntes as $nte) {
          DB::table('psb_laporan_material')->insert([
            'id' => $data->id.'-'.$nte,
            'psb_laporan_id' => $data->id,
            'id_item' => $nte,
            'qty' => 1,
            'type' =>2
          ]);
          DB::table('nte')
            ->where('id', $nte)
            ->update([
              'position_type'  => 3,
              'position_key'  => $data->id,
              'status' => 1
            ]);
        }
        $this->insertMaterials($data->id, $materials);
        $this->decrementStokTeknisi($auth->id_karyawan, $materials);
      });
      // $this->handleFileUpload($request, $id);
      if($sendReport){
          exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
      }
      if ($a == 1) {
        //update ttr 3 dan 12
         $dataDispatch = DB::table('data_nossa_1_log')
                        ->leftJoin('dispatch_teknisi','data_nossa_1_log.Incident','=','dispatch_teknisi.Ndem')
                        ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                        ->where('psb_laporan.id_tbl_mj',$data->id_tbl_mj)
                        ->first();

        if ($dataDispatch){
            $ttr3 = 0;
            if ($dataDispatch->Booking_Date<>'' && $dataDispatch->Booking_Date<>'-- '){
              $awal  = date_create($dataDispatch->Booking_Date);
              $akhir = date_create($dataDispatch->modified_at);
              $diff  = date_diff( $awal, $akhir );

              $ttr3 = $diff->h + ($diff->d * 24);
            }

            $ttr12 = 0;
            if($dataDispatch->UmurDatetime<>'' && $dataDispatch->UmurDatetime<>'-- '){
              $awal  = date_create($dataDispatch->UmurDatetime);
              $akhir = date_create($dataDispatch->modified_at);
              $diff  = date_diff( $awal, $akhir );

              $ttr12 = $diff->h + ($diff->d * 24);
            }

            DB::table('psb_laporan')->where('id_tbl_mj',$data->id_tbl_mj)->update([
                'is_3hs'    => $ttr3,
                'is_12hs'   => $ttr12
            ]);
        };

        echo "kirim sms";
        $get_order = DB::SELECT('
          SELECT
          *,
          a.id as id_dt
          FROM
            dispatch_teknisi a
          LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
          LEFT JOIN regu c ON a.id_regu = c.id_regu
          LEFT JOIN data_nossa_1_log d ON a.Ndem = d.Incident
          LEFT JOIN group_telegram e ON c.mainsector = e.chat_id
          WHERE
            a.id = "'.$data->id_tbl_mj.'"
            ')[0];
            if ($get_order->Incident<>NULL && $get_order->sms_active==1):
            $pesan = "Pelanggan Yth. \n";
            $pesan .= "Mhn bantuan penilaian kpd Teknisi Indihome Kami. Silahkan klik link berikut \n";
            $pesan .= "http://tomman.info/rv/".$this->encrypt($get_order->id_dt);

            // masukkan log
            DB::table('psb_laporan_log')->insert([
               'created_at'      => DB::raw('NOW()'),
               'created_by'      => $auth->id_karyawan,
               'status_laporan'  => $request->input('status'),
               'id_regu'         => $get_order->id_regu,
               'catatan'         => 'Pesan SMS Terkirim ',
               'Ndem'            => $get_order->Ndem,
               'psb_laporan_id'  => $data->id
            ]);

            // kirim sms
            $psn = str_replace('\n', ' ', $pesan);
            $dataDikirim = '("'.$get_order->Contact_Phone.'","'.$psn.'","true","3006")';
            $sql = 'INSERT INTO outbox (DestinationNumber,TextDecoded,MultiPart,CreatorID) values '.$dataDikirim;
            DB::connection('mysql2')->insert($sql);
            endif;
      }
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }
    else {
      // update no.telp
      DB::transaction(function() use($request, $disp, $auth) {
          DB::table('roc')->where('no_tiket',$disp->Ndem)->update([
              'no_telp' => $request->noTelp
          ]);
      });

      DB::transaction(function() use($request, $id, $auth, $materials, $disp, $ntes, $statusOdp) {
        $insertId = DB::table('psb_laporan')->insertGetId([
          'created_at'        => DB::raw('NOW()'),
          'created_by'        => $auth->id_karyawan,
          'id_tbl_mj'         => $id,
          'catatan'           => $request->input('catatan'),
          'status_laporan'    => $request->input('status'),
          'action'            => $request->input('action'),
          'kordinat_pelanggan'=> $request->input('kordinat_pelanggan'),
          'nama_odp'          => $request->input('nama_odp'),
          'kordinat_odp'      => $request->input('kordinat_odp'),
          'penyebabId'        => $request->input('penyebab'),
          'kondisi_odp'       => $statusOdp,
          'typeont'           => $request->input('jenis_ont')
        ]);

      foreach($ntes as $nte) {
        DB::table('psb_laporan_material')->insert([
          'id' => $insertId.'-'.$nte,
          'psb_laporan_id' => $insertId,
          'id_item' => $nte,
          'qty' => 1,
          'type' =>2
        ]);
        DB::table('nte')
          ->where('id', $nte)
          ->update([
            'position_type'  => 3,
            'position_key'  => $id,
            'status' => 1
          ]);
      }
      $this->insertMaterials($insertId, $materials);
      $this->decrementStokTeknisi($auth->id_karyawan, $materials);

      $dataDisp = $disp;
      DB::transaction(function() use($request, $dataDisp, $auth, $insertId) {
         DB::table('psb_laporan_log')->insert([
           'created_at'      => DB::raw('NOW()'),
           'created_by'      => $auth->id_karyawan,
           'mttr'            => $this->datediff($dataDisp->updated_at,date('Y-m-d h:i:s')),
           'status_laporan'  => $request->input('status'),
           'id_regu'         => $dataDisp->id_regu,
           'catatan'         => $request->input('catatan'),
           'action'          => $request->input('action'),
           'Ndem'            => $dataDisp->Ndem,
           'psb_laporan_id'  => $insertId,
           'penyebabId'      => $request->input('penyebab')
         ]);
       });
      });
      $upload = $this->handleFileUpload($request, $id, $c);
      if (@$upload['type']=="danger")
      return back()->with('alerts', [
        ['type' => 'danger', 'text' => $upload['message']]
      ]);

      exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');

      return redirect('/')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }
  }

  public function datediff($start,$end){
    $start = new DateTime(($start));
    $end = new DateTime(($end));
    $interval = $start->diff($end);
    return $interval->h + ($interval->days*24);
  }

  private function insertMaterials($psbId, $materials)
  {
    // foreach($materials as $material) {
    //   DB::table('psb_laporan_material')->insert([
    //     'id' => $psbId.'-'.$material->id_item,
    //     'psb_laporan_id' => $psbId,
    //     'id_item' => $material->id_item,
    //     'qty' => $material->qty,
    //     'type' => 1
    //   ]);
    // }

    $nik = session('auth')->id_user;
    foreach($materials as $material) {
      $exis = DB::table('psb_laporan_material')
                ->where('id',$psbId.'-'.$material->id_item.'_'.$material->rfc)
                ->first();

      if ($exis){
        DB::table('psb_laporan_material')
          ->where('id',$psbId.'-'.$material->id_item.'_'.$material->rfc)
          ->update([
              'qty' => $material->qty
          ]);
      }
      else{
        DB::table('psb_laporan_material')->insert([
              'id' => $psbId.'-'.$material->id_item.'_'.$material->rfc,
              'psb_laporan_id' => $psbId,
              'id_item' => trim($material->id_item),
              'id_item_bantu' => trim($material->id_item).'_'.$material->rfc,
              'qty' => $material->qty,
              'rfc' => $material->rfc,
              'type' => 1
          ]);

          // simpan ke maintaince_saldo_rfc
          $pakai = DB::table('psb_laporan')->where('id',$psbId)->first();
          $dtSaldo     = DB::table('rfc_sal')->where('rfc',$material->rfc)->where('nik',$nik)->first();
          $dataRegu    = DB::SELECT('SELECT a.*, b.*
                                   FROM regu a
                                   LEFT JOIN group_telegram b ON a.mainsector=b.chat_id
                                   WHERE
                                      a.ACTIVE=1 AND
                                      (a.nik1="'.$nik.'" OR a.nik2="'.$nik.'")');

          $idRegu = NULL; $uraianRegu = NULL; $nik1   = NULL; $nik2 = NULL; $nikTl = NULL;
          if(count($dataRegu)<>0){
              $idRegu     = $dataRegu[0]->id_regu;
              $uraianRegu = $dataRegu[0]->uraian;
              $nik1       = $dataRegu[0]->nik1;
              $nik2       = $dataRegu[0]->nik2;
              $nikTl      = $dataRegu[0]->TL_NIK;
          }

            DB::table('maintenance_saldo_rfc')->where('created_by',$nik)->where('bantu',$material->rfc.'#'.$material->id_item)->where('action',2)->where('transaksi',1)->delete();
            DB::table('maintenance_saldo_rfc')->insert([
                'id_pengeluaran'    => $dtSaldo->alista_id,
                'rfc'               => $dtSaldo->rfc,
                'regu_id'           => $idRegu,
                'regu_name'         => $uraianRegu,
                'nik1'              => $nik1,
                'nik2'              => $nik2,
                'niktl'             => $nikTl,
                'created_at'        => DB::raw('NOW()'),
                'created_by'        => $nik,
                'id_pemakaian'      => $pakai->id_tbl_mj,
                'value'             => $material->qty * -1,
                'id_item'           => $material->id_item,
                'action'            => '2',
                'bantu'             => $material->rfc.'#'.$material->id_item,
                'transaksi'         => '2'
            ]);
           ////
      }

        // update saldo terpakai
        $dataA = DB::table('psb_laporan_material')
                  ->where('id_item_bantu',$material->id_item.'_'.$material->rfc)
                  ->get();
        $terpakai = 0;
        foreach($dataA as $aaa){
            $terpakai += $aaa->qty;
        }

        // update terpakai
        DB::table('rfc_sal')->where('nik',$nik)->where('id_item_bantu',$dataA[0]->id_item_bantu)->update(['jmlTerpakai' => $terpakai]);
    }
  }

  private function decrementStokTeknisi($id_karyawan, $materials)
  {
    foreach($materials as $material) {
      $exists = DB::select('
        SELECT *
        FROM stok_material_teknisi
        WHERE id_karyawan = ? and id_item = ?
      ',[
        $id_karyawan, $material->id_item
      ]);

      if (count($exists)) {
        DB::table('stok_material_teknisi')
          ->where('id_item', $material->id_item)
          ->where('id_karyawan', $id_karyawan)
          ->decrement('stok', $material->qty);
      }else{
        $insertId=DB::table('stok_material_teknisi')->insertGetId([
          'id_item' => $material->id_item,
          'id_karyawan' => $material->qty
        ]);
        DB::table('stok_material_teknisi')
          ->where('id', $insertId)
          ->decrement('stok', $material->qty);
      }

    }
  }
  private function incrementStokTeknisi($id, $idkaryawan)
  {
    $materials = DB::select('
        SELECT *
        FROM psb_laporan_material
        WHERE psb_laporan_id = ? and type = 1
      ',[
        $id
      ]);
    foreach($materials as $material) {
      DB::table('stok_material_teknisi')
        ->where('id_item', $material->id_item)
        ->where('id_karyawan', $idkaryawan)
        ->increment('stok', $material->qty);
    }
    $ntes = DB::select('
        SELECT *
        FROM psb_laporan_material
        WHERE psb_laporan_id = ? and type = 2
      ',[
        $id
      ]);
    foreach($ntes as $nte) {
      DB::table('nte')
        ->where('id', $nte->id_item)
        ->update([
            'position_type'  => 2,
            'position_key'  => $idkaryawan,
            'status' => 1
          ]);
    }
  }
  private function handleFileUploadx($request, $id, $penyebab)
  {
    $return = array();
    $return['type'] = 'success';
    $return['message'] = 'Berhasil';
    $return['waktu'] = array();
    print_r($request->input);
    $get_penyebab = DB::table('psb_laporan_penyebab')->where('idPenyebab',$penyebab)->get();
    $check_foto = DB::SELECT('
      SELECT
      a.name
      FROM cat_foto a
      WHERE
      a.'.$get_penyebab[0]->kategori.' = 1
    ');
    foreach($check_foto as $photo) {
      $name = $photo->name;
      $input = 'photo-'.$name;
      echo $name." ";
      if ($request->hasFile($input)) {

        echo "true : ".$input."<br />";
        $table = DB::table('hdd')->get()->first();
        $upload = $table->public;
        $path = public_path().'/'.$upload.'/asurance/'.$id.'/';
        echo $path;
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true)){
          }
        }
        $file = $request->file($input);
        $ext = 'jpg';
        $filemime = @exif_read_data($file);
        try {
          $moved = $file->move("$path", "$name.$ext");
          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
          echo "true";
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return $return;
          echo "false";
        }
      } else {
        echo "false";
      }
      echo "<br />";
    }
    // return $return;
  }
  private function handleFileUpload($request, $id, $penyebab)
  {
    $return = array();
    $return['type'] = 'success';
    $return['message'] = 'Berhasil';
    $return['waktu'] = array();
    if ($penyebab==NULL){
      $penyebab =1;
    }
    print_r($request->input);
    $get_penyebab = DB::table('psb_laporan_penyebab')->where('idPenyebab',$penyebab)->get();
    $check_foto = DB::SELECT('
      SELECT
      a.name
      FROM cat_foto a
      WHERE
      a.'.$get_penyebab[0]->kategori.' = 1
    ');
    foreach($check_foto as $photo) {
      $name = $photo->name;
      $input = 'photo-'.$name;
      echo $name." ";
      if ($request->hasFile($input)) {
        echo "true : ".$input."<br />";
        //dd($input);
        $table = DB::table('hdd')->get()->first();
        $upload = $table->public;
        $path = public_path().'/'.$upload.'/asurance/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true)){
          //$return['type'] = 'danger';
          //$return['message'] = 'Gagal membuat folder';
          //return $return;
          }
        }
        $file = $request->file($input);
        // $ext = $file->guessExtension();
        $ext = 'jpg';
        $filemime = @exif_read_data($file);
        if ($name <> "Berita_Acara" && $name<> "Test_Layanan_Internet" && $name<>"Test_Layanan_Telepon" && $name<>"Speedtest_before" && $name<>"Speedtest_after" && $name<>"Indikasi_before" && $name<>"Indikasi_after") {
          if (@array_key_exists("Software",$filemime)){
            if (@$filemime['Software']<>"Timestamp Camera" && @$filemime['Software']<>"Timestamp Camera ENT")
            {
              $return['type'] = 'danger';
              $return['message'] = 'Foto '.$name.' tidak menggunakan timestamp';
              // return $return;
              echo "failed";
            }
            $return['waktu'][] = @$filemime['DateTimeOriginal'];
          } else {
            $return['type'] = 'danger';
            $return['message'] = 'Foto '.$name.' tidak menggunakan timestamp';
            // return $return;
            echo "success";
          }
        }

        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");
          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");

        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return $return;
        }
      } else {
        echo "false";
      }
      echo "<br />";
    }
    // return $return;
  }
  public function cacti(){
    //personal
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://10.140.18.81/index.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "action=login&login_username=user&login_password=user");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $rough_content = curl_exec($ch);
    if (curl_error($ch)) {
        echo curl_error($ch);
    }
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0] ;
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    //curl_setopt($ch, CURLOPT_URL, "http://10.140.18.81/host.php?host_status=-1&host_template_id=-1&host_rows=600&filter=");
    curl_setopt($ch, CURLOPT_URL, "http://10.140.18.81/plugins/monitor/monitor.php");
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    //ccan:http://10.62.166.52/nonadetail.php?reg=6&cust=corp&jam=&wtl=KALSEL
    //personal:http://10.62.166.52/nonadetail.php?reg=6&cust=psnl&waktu=&wtl=KALSEL
    //all:http://10.62.166.52/nonadetail.php?reg=6&cust=rkap&waktu=&wtl=KALSEL

    $result = curl_exec($ch);


    $result = substr_replace($result, '', 0, strpos($result, '<center><h2>Down Host Messages</h2><table cellspacing=0 cellpadding=1 bgcolor=black><tr><td><table bgcolor=white width="100%">'));

    $result = substr_replace($result, '', strpos($result, '<br><br><br>'), strlen($result));
    //$countstring = substr_count($result, "<table cellpadding=0 cellspacing=0>");
    //for($i=0; $i<$countstring; $i++){
    //  $result = substr_replace($result, "a".$i, strpos($result, "<td>IP Address:</td>"), strpos($result, "<table cellpadding=0 cellspacing=0>"));
    //}


    $result = str_replace("<center><h2>Down Host Messages</h2><table cellspacing=0 cellpadding=1 bgcolor=black><tr><td>","",$result);
    $result = str_replace("</table></td></tr></table></center>","</table>",$result);
    $result = str_replace("<b>","",$result);
    $result = str_replace("</b>","",$result);
    $result = str_replace('<table bgcolor=white width="100%"','<table',$result);

    $columns = array(
            0 => 'note',
            'device'
        );
    $dom = @\DOMDocument::loadHTML(trim($result));

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    $diff_array = array();
    for ($i = 0, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        //print_r($cells);
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $diff_array[] = $td->nodeValue;
        $result[] = $data;
    }
    $data = DB::table('cacti_data')->get();
    $up = '';
    foreach($data as $r){
      if(!in_array($r->device, $diff_array))
        $up .= $r->device." : UP\n";
    }
    $tele = '';
    $inserts = array();
    foreach($result as $r){
      $device = $r['device'];
      $tele .= $device." : DOWN\n";
      $inserts[] = ["device" => $r['device'], "status" => "Down", "note" => $r['note']];
    }
    DB::table('cacti_data')->truncate();
    DB::table('cacti_data')->insert($inserts);
    $tele = $up.$tele;
    self::aset_output("cacty", $tele);
    if(count($data)!=count($result)){
      //Telegram::sendMessage([
      //    'chat_id' => '@cactynotify',
      //    'text' => $tele
      //  ]);
    }

  }
  function aset_output($name,$html) {
    ob_start();
    echo "<body>
    <h1>Data Aset</h1>
    <br>
    <table><tr><th>NO</th><th>NAMA ASET</th><th>MERK</th><th>SN</th><th>PJ</th><th>Program</th>";
    $output = ob_get_contents();
    ob_end_clean();

    $basedir = public_path();
    $fname = "/home/ta/htdocs/tomman_bot/$name.html";
    file_put_contents($fname, $output);
    chmod($fname, 0777);
    passthru("phantomjs $basedir/out/rasterize.js file:///$fname /home/ta/htdocs/tomman_bot/$name.png \"980px\"");
  }
  public function grab_cacti(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://10.140.18.81/index.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "action=login&login_username=user&login_password=user");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $rough_content = curl_exec($ch);
    if (curl_error($ch)) {
        echo curl_error($ch);
    }
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0] ;
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    curl_setopt($ch, CURLOPT_URL, "http://10.140.18.81/plugins/monitor/monitor.php");
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $result = curl_exec($ch);

    $result = str_replace("#993333","",$result);
    $result = str_replace("#FFFFFF","",$result);
    $result = str_replace("#669966","",$result);

    $result = substr_replace($result, '', 0, strpos($result, "<table border=0 cellspacing=5 bgcolor='' width='100%' id=BJB>"));
    $result = substr_replace($result, '', strpos($result, "<table border=0 style='float:left; margin:10px;' cellpadding=1 cellspacing=0 bgcolor=''><tr><td>&nbsp;<font color=''><b>Area Kalsel"), strpos($result, "</table></span>Localhost</a></center>
</td></table></td></tr></table>"));
    $result = substr_replace($result, '', strpos($result, "Palangkaraya (Temp)")-134, strlen($result));

    $result = str_replace("<table cellpadding=0 cellspacing=0>","",$result);
    $result = str_replace("</table>","",$result);
    $result = str_replace("<span>","",$result);
    $result = str_replace("</span>","",$result);
    $result = str_replace("<a>","",$result);
    $result = str_replace("</a>","",$result);
    $result = str_replace("<b>","",$result);
    $result = str_replace("</b>","",$result);
    $result = str_replace("<script>","",$result);
    $result = str_replace("</script>","",$result);
    $result = str_replace("<center>","",$result);
    $result = str_replace("</center>","",$result);
    $result = str_replace("images/red.gif","",$result);
    $result = str_replace("images/green.gif","",$result);
    $result = str_replace("' src='' border='0'><br>","",$result);
    $result = str_replace("<tr><td valign=top><a class=info href='/graph_view.php?action=preview&host_id=","",$result);
    $result = str_replace("'><img id='status_","",$result);
    $result = str_replace("<table border=0 cellspacing=5 bgcolor='' width='100%' id=","",$result);
    $result = str_replace("</td><td valign=top><a class=info href='/graph_view.php?action=preview&host_id=","",$result);
    $result = str_replace("</td></td></tr>
<table border=0 style='float:left; margin:10px;' cellpadding=1 cellspacing=0 bgcolor=''><tr><td>&nbsp;<font color=''>","",$result);

    $result = str_replace("</font></td></tr><tr><td bgcolor=''>","",$result);
    $result = str_replace("<tr>","",$result);
    $result = str_replace("</tr>","",$result);
    $result = str_replace("<tr valign=top>
        <td>Status:</td>","",$result);
$result = str_replace("<tr valign=top>
        <td>IP Address:</td>","",$result);
$result = str_replace("<tr valign=top>
        <td>Ping:</td>","",$result);
$result = str_replace("<tr valign=top>
        <td>Last Fail:</td>","",$result);
$result = str_replace("<tr valign=top>
        <td>Availability:&nbsp;&nbsp;</td>","",$result);
$result = str_replace("<tr valign=top>
        <td>Notes:</td>","",$result);
$result = str_replace("<td colspan=2>","</tr><tr><td>",$result);
$result = str_replace("</tr><tr><td>BJB.FRA_1 BRIMOB</td>","<table><tr><td>BJB.FRA_1 BRIMOB</td>",$result);
$result = str_replace("<table border=0 style='float:left; margin:10px;' cellpadding=1 cellspacing=0 bgcolor=''><td>&nbsp;<font color=''>","",$result);
$result = str_replace("<br><font color='red'>","<td>",$result);
$result = str_replace("s</font>","s</td>",$result);
$result = str_replace("<td>Up</td>","<td>Up</td><td></td>",$result);


  $columns = array(
            0 => 'device',
            'status',
            'ip',
            'ping',
            'last_fail',
            'availability',
            'note',
            'down_time'
        );
  $dom = @\DOMDocument::loadHTML(trim($result."</table>"));

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    $diff_array = array();
    for ($i = 0, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        //print_r($cells);
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $td = $cells->item(1);
        if($td->nodeValue == 'Down'){
          $td = $cells->item(0);
          $diff_array[] = $td->nodeValue;
          $result[] = $data;
        }


    }

    $data = DB::table('cacti_data')->orderBy('down_time', 'desc')->get();
    $up = '';
    $parse = array();
    foreach($data as $r){
      if(!in_array($r->device, $diff_array)){
        $up .= $r->device." : UP\n";
        $parse[] = array("perangkat" => $r->device, "status" => "UP", "downtime" => "-");
      }
    }
    $tele = '';
    $inserts = array();
    foreach($result as $r){
      $device = $r['device'];
      $tele .= "(`".$r['down_time']."`)".$device.":DOWN\n";
      $inserts[] = ["device" => $r['device'],
        "status" => $r['status'],
        "ip" => $r['ip'],
        "ping" => $r['ping'],
        "last_fail" => $r['last_fail'],
        "availability" => $r['availability'],
        "down_time" => $r['down_time'],
        "note" => $r['note']
      ];
      $parse[] = array("perangkat" => $r['device'], "status" => "DOWN", "downtime" => $r['down_time']);
    }
    DB::table('cacti_data')->truncate();
    DB::table('cacti_data')->insert($inserts);
    return view('assurance.cacty', compact('parse'));
    //$tele = $up.$tele;
    //self::aset_output("cacty", $tele);
    //if(count($data)!=count($result)){
    //  Telegram::sendMessage([
    //      'chat_id' => '@cactynotify',
    //      'text' => $tele
    //    ]);
   // }
  }
  public static function sc_grab(){

    $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=KALSEL&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=&EndDate=&page=1&start=0&limit=10000";
    $result = json_decode(@file_get_contents($link));
    $db = DB::table("Data_Pelanggan_Starclick")->select("orderId")->get();
    $updates = "";
    $inserts = "insert into pcc(orderId, orderKontak) values";
    $no = 0;
    $db = json_decode(json_encode($db),TRUE);

    foreach($result->data as $data){
        if(array_search($data->ORDER_ID, array_column($db, 'orderId'))){
          echo "update";
        }else{
          echo "insert";
        }
    }

    echo $updates;
    echo $inserts;
    //DB::statement($updates);
    //DB::statement($inserts);

  }

  public static function iss_grab(){
    ini_set('max_execution_time', 6000);
    ini_set('memory_limit', '9G');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/issystem/index.php/login/check_database');
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=18950123&password=@kholifah13");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    $rough_content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1] ;
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/issystem/index.php/report/export_report/6/all/-/-');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0');
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $result = curl_exec($ch);
    $trim = '<table border="1px"';
    $html = substr_replace($result, '', 0, strpos($result, $trim));
    $html = str_replace('<thead>','',$html);
    $html = str_replace('<tbody>','',$html);
    $html = str_replace('</thead>','',$html);
    $html = str_replace('</tbody>','',$html);
    echo "HTML LOADED";
    echo "MAKING QUERY";
    self::getQueryISS($html);
    echo "finish";
  }

  public static function getQueryISS($result){

        //return $result;
        $dom = @\DOMDocument::loadHTML(trim($result));

        $table = $dom->getElementsByTagName('table')->item(0);
        $rows = $table->getElementsByTagName('tr');
        $columns = array(
                'no',
                'no_order',
                'create_date',
                'source',
                'order_by',
                'ref',
                'nama_project',
                'designator',
                'purose',
                'regional',
                'witel',
                'sto',
                'loker',
                'nik',
                'nama',
                'koordinator',
                'spv',
                'result',
                'satuan',
                'status',
                'closed_date',
                'id_smallworld',
                'status_change_state',
                'point',
                'pencapaian'
            );
        $result = array();
        for ($i = 1, $count = 100; $i < $count; $i++)
        {
            $cells = $rows->item($i)->getElementsByTagName('td');
            //print_r($cells);
            $data = array();
            for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
            {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
            }

            $result[] = $data;

        }
        $data = $result;
        dd($data);
        $count = count($data);
        $sql ='insert ignore into iss_project(id_order,create_date,source,order_by,ref,nama_project,purpose,regional,witel,sto,loker,closed_date,id_smallworld,status_change_state) values ';
        $sql2 = 'insert ignore into iss_status(id_order,status) values ';
        $sql3 = 'insert ignore into iss_designator(id_order,designator,nik,nama,koordinator,spv,result,satuan,point,pencapaian) values ';
        $r=0;
        for ($i = 0; $i < $count; $i++) {
            if($data["$i"]["witel"]=='KALSEL'){
              $sparator = ", ";
              if($r==0){
                $sparator = "";
                $r=1;
              }
              $sql .= $sparator."('".$data["$i"]["no_order"]."','".$data["$i"]["create_date"]."','".$data["$i"]["source"]."','".$data["$i"]["order_by"]."'
                ,'".$data["$i"]["ref"]."'
                ,'".addslashes($data["$i"]["nama_project"])."'
                ,'".$data["$i"]["purpose"]."'
                ,'".$data["$i"]["regional"]."'
                ,'".$data["$i"]["witel"]."'
                ,'".$data["$i"]["sto"]."'
                ,'".$data["$i"]["loker"]."'
                ,'".$data["$i"]["closed_date"]."'
                ,'".$data["$i"]["id_smallworld"]."'
                ,'".$data["$i"]["status_change_state"]."')";
                $sql2 .= $sparator."('".$data["$i"]["no_order"]."','".$data["$i"]["status"]."')";
                $sql3 .= $sparator."('".$data["$i"]["no_order"]."'
                    ,'".$data["$i"]["designator"]."'
                    ,'".$data["$i"]["nik"]."'
                    ,'".addslashes($data["$i"]["nama"])."'
                    ,'".$data["$i"]["koordinator"]."'
                    ,'".$data["$i"]["spv"]."'
                    ,'".$data["$i"]["result"]."'
                    ,'".$data["$i"]["satuan"]."'
                    ,'".$data["$i"]["point"]."'
                    ,'".$data["$i"]["pencapaian"]."')";
            }
        }
        DB::statement("truncate table iss_designator");
        DB::statement("truncate table iss_status");
        DB::statement($sql);
        DB::statement($sql2);
        DB::statement($sql3);
        echo $sql2;
  }

  public function istAllTiket($tgl){
      $tahun = date('Y',strtotime($tgl));
      $bulan = date('m',strtotime($tgl));

      $data = DB::table('dt2')
                ->leftJoin('psb_laporan','dt2.id','=','psb_laporan.id_tbl_mj')
                ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
                ->leftJoin('psb_laporan_penyebab','psb_laporan.penyebabId','=','psb_laporan_penyebab.idPenyebab')
                ->leftJoin('psb_laporan_action','psb_laporan.action','=','psb_laporan_action.laporan_action_id')
                ->leftJoin('regu','dt2.id_regu','=','regu.id_regu')
                ->select('dt2.*','psb_laporan.*','psb_laporan_status.laporan_status','psb_laporan_penyebab.penyebab','psb_laporan_action.action','regu.uraian')
                ->whereYear('psb_laporan.modified_at',$tahun)
                ->whereMonth('psb_laporan.modified_at',$bulan)
                ->get();

      return view('assurance.listAll',compact('data'));
  }

  public static function senDWoReporOrderMaintenance($bulan){
      if ($bulan == "today") $bulan = date('Y-m');
      $getData  = AssuranceModel::getStatusOrderMaintenance($bulan);
      $jumlah  = count($getData);
      $dataTitle = [];
      foreach($getData as $data){
          if (!in_array($data->title, $dataTitle)){
              array_push($dataTitle, $data->title);
          }
      }

      $pesan = '';
      foreach ($dataTitle as $header){
          $pesan .= $header."\n";
          foreach($getData as $data){
              if ($data->title == $header){
                  $pesan .= $data->Incident.' '.$data->nama_odp.' '.$data->updated_at."\n";
              }
          }
          $pesan .= "\n";
      }

      $pesan .= 'Total : '.$jumlah;

      // $penerima = ['102050936', '193004228', '107374530', '118072571', '117760342', '119568681', '359876079', '416151391', '181275843', '181275843', '54739476', '103313153', '538352266', '731022948', '313478957', '367471783'];
      // $penerima = ['192915232'];
      $penerima = ['-372051566'];

      foreach($penerima as $terima){
          Telegram::sendMessage([
            'chat_id' => $terima,
            'text' => $pesan
          ]);

          echo $terima . 'success to send'."\n";

      };
      // echo $pesan;
}

public static function senDWoReportKirimTeknisi($bulan){
    if ($bulan == "today") $bulan = date('Y-m');
    $getData  = AssuranceModel::getStatusKirimTeknisi($bulan);
    $jumlah  = count($getData);

    $dataTitle = [];
    foreach($getData as $data){
        if (!in_array($data->title, $dataTitle)){
            array_push($dataTitle, $data->title);
        }
    }

    $pesan = '';
    foreach ($dataTitle as $header){
        $pesan .= $header."\n";
        foreach($getData as $data){
            if ($data->title == $header){
                $pesan .= $data->Workzone.' '.$data->Incident.' KIRIM TEKNISI'."\n";
            }
        }
        $pesan .= "\n";
    }

    $pesan .= 'Total : '.$jumlah;

    // $penerima = ['102050936', '193004228', '107374530', '118072571', '117760342', '119568681', '359876079', '416151391', '181275843', '181275843', '54739476', '103313153', '538352266', '731022948', '313478957', '367471783'];
    $penerima = ['-1001131555233'];

    if(count($getData)<>0){
        foreach($penerima as $terima){
            Telegram::sendMessage([
              'chat_id' => $terima,
              'text' => $pesan
            ]);

            echo $terima . 'success to sssend'."\n";
        };

    }
    // echo $pesan;
}

public static function showManjaBot(){
    date_default_timezone_set("Asia/Makassar");
    $date = date('Y-m-d');
    $jam  = date('H');
    $jamMenit = date('H:i');
    $getData = AssuranceModel::getManjaForBot($date,$jam);

    $head = array();
    foreach($getData as $data){
        if(!in_array($data->title,$head)){
            array_push($head, $data->title);
        };
    };

    $pesan = "LIST MANJA JAM ".$jamMenit." Tanggal ".$date."\n";
    $pesan .= "=========================================\n\n";
    foreach($head as $h){
        $pesan .= $h."\n";
        foreach($getData as $data){
            if ($data->title==$h){
                $status = 'NEED PROGRESS';
                if ($data->laporan_status<>''){
                    $status = $data->laporan_status;
                };

                $pesan .= $data->uraian.' - '.$data->Incident.' - '.$status.' - '.$data->Service_ID.' - '.$data->jamBooking."\n";
            }
        };
        $pesan .= "\n";
    }

    $pesan .= 'Total '.count($getData);

    $penerima = ["-1001255117195", "-1001337388906", "-1001414765451", "-1001193267691", "-1001109389716", "-1001309048172", "-370267630", "-1001255871137"];
    foreach ($penerima as $nerima){
        Telegram::sendMessage([
            'chat_id' => $nerima,
            'text'    => $pesan
        ]);

        echo $nerima.' has been send'."\n";
    }

    echo "sukses\n";
}



}
