<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Telegram;
use App\DA\DispatchModel;
class Tele
{

  public function send_to_telegram_dshr($id){
	  $auth = session('auth');
		$list = DB::select('
		  SELECT a.uraian,d.*, b.nama as sales,b.sub_directorat
		  FROM dshr d
		  left join 1_2_employee b on d.nik_sales = b.nik
		  left join regu a on b.id_regu = a.id_regu
		  WHERE id = ?
		  order by d.id desc
		',[ $id ])[0];

		$s = $list->nama_pelanggan;
		$messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n";
		$messageio .= 'Tanggal Transaksi: '.$list->tanggal_deal."\n\n";
		$messageio .= 'Nama Sales : '.$list->sales."\n";
		$messageio .= 'NIK Sales : '.$list->nik_sales."\n";
		$messageio .= 'Tipe Transaksi : '.$list->tipe_transaksi."\n";
		$messageio .= '👤 : '.$list->nama_pelanggan."\n";
		$messageio .= '✉️ : '.$list->email."\n";
		$messageio .= '🏠 : '.$list->alamat."\n";
		$messageio .= '📱 : '.$list->pic."\n";
		$messageio .= '☎️ : '.$list->no_telp."\n\n";
		$messageio .= 'MYIR : '.$list->no_ktp."\n";
		$messageio .= 'Kordinat Pelanggan : '.$list->koor_pel."\n";
		$messageio .= 'Nama ODP : '.$list->nama_odp."\n";
		$messageio .= 'Kordinat ODP : '.$list->koor_odp."\n\n";
    if ($list->sub_directorat=="Avengers"){
	    $chatID = "-1001227198531";
	  } else {
      $chatID = "-171167699";
    }
	Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messageio
    ]);

    $photoInputs = $this->photoInputs;
		for($i=0;$i<count($photoInputs);$i++) {
    	$file = public_path()."/upload3/dshr/".$id."/".$photoInputs[$i].".jpg";
    	if (file_exists($file)){
		    Telegram::sendPhoto([
		      'chat_id' => $chatID,
		      'caption' => $photoInputs[$i]." ".$s,
		      'photo' => $file
		    ]);

		  }
    }
    return redirect('/dshr-transaksi')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengirim laporan']
    ]);
  }

  public static function sendOrderMaintenance($id){
    $order = DB::table('psb_laporan')
    ->select('1_2_employee.nama', 'psb_laporan.checked_at', 'psb_laporan_status.laporan_status', 'dispatch_teknisi.Ndem')
    ->leftJoin('dispatch_teknisi', 'psb_laporan.id_tbl_mj', '=', 'dispatch_teknisi.id')
    ->leftJoin('1_2_employee', 'psb_laporan.checked_by', '=', '1_2_employee.nik')
    ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
    ->where('id_tbl_mj', $id)->first();
    Telegram::sendMessage([
      'chat_id' => "-285944800",
      'parse_mode' => 'html',
      'text' => "New Order at TommanMaintenance\n==============================\nOrder_ID : ".$order->Ndem."\nJenis Order : ".$order->laporan_status."\nChecked By : ".$order->nama."\nChecked At : ".$order->checked_at
    ]);
  }

  public static function sendReportTelegram($id){
    date_default_timezone_set("Asia/Makassar");

    // $auth = session('auth');
    $order = DB::select('
      SELECT
        *,
        ms2n.*,
        d.id as id_dt,
        d.created_at as tgl_dispatch,
        d.updated_at as tgl_dispatch2,
        d.Ndem as Ndemm,
        r.uraian as nama_tim,
        m.Kcontact as mKcontact,
        m.ND as mND,
        m.ND_Speedy as mND_Speedy,
        m.Nama as mNama,
        m.Alamat as mAlamat,
        p2.mdf_grup_id as grup_id_psb,
        m2.mdf_grup_id as grup_id_migrasi,
        p2.mdf_grup as grup_psb,
        m2.mdf_grup as grup_migrasi,
        a2.mdf_grup as grup_starclick,
        a2.mdf_grup_id as grup_id_starclick,
        a.orderName,
        a.ndemPots,
        a.ndemSpeedy,
        a.sto,
        a.jenisPsb,
        a.Kcontact as KcontactSC,
        a.orderNcli,
        yy.laporan_status,
        reb.nd_telp as no_telp,
        dn1log.Service_ID as no_speedy,
        dn1log.Summary as headline,
        TIMESTAMPDIFF( HOUR , dn1log.UmurDatetime, "'.date('Y-m-d H:i:s').'") as umur,
        ra.no_tiket as status_tiket,
        dd.manja_status,
        ddd.dispatch_teknisi_timeslot,
        pla.action,
        dm.*,plog.mttr,
        zz.modified_at as tgl_update,
        zz.created_at as tgl_laporan,
        tt.ketTiang,
        zz.noPelangganAktif,
        d.kordinatPel,
        d.alamatSales,
        dn1log.*,
        dn1log.Incident as no_tiket,
        pls.penyebab,
        my.myir,
        my.customer,
        my.namaOdp as odpBySales,
        zz.nama_odp as odpByTeknisi,
        zz.redaman_odp,
        my.sc, 
        my.no_internet,
        my.no_telp,
        zz.dropcore_label_code
      FROM dispatch_teknisi d
      LEFT JOIN manja_status dd ON d.manja_status = dd.manja_status_id
      LEFT JOIN dispatch_teknisi_timeslot ddd ON d.updated_at_timeslot = ddd.dispatch_teknisi_timeslot_id
      LEFT JOIN regu r ON (d.id_regu=r.id_regu)
      LEFT JOIN ms2n USING (Ndem)
      LEFT JOIN mdf p1 ON ms2n.mdf=p1.mdf
      LEFT JOIN mdf_grup p2 ON p1.mdf_grup_id = p2.mdf_grup_id
      LEFT JOIN micc_wo m on d.Ndem = m.ND
      LEFT JOIN mdf m1 ON m.MDF = m1.mdf
      LEFT JOIN mdf_grup m2 ON m1.mdf_grup_id = m2.mdf_grup_id
      LEFT JOIN Data_Pelanggan_Starclick a ON d.Ndem = a.orderId
      LEFT JOIN psb_laporan zz ON d.id = zz.id_tbl_mj
      LEFT JOIN psb_laporan_status yy ON zz.status_laporan = yy.laporan_status_id
      LEFT JOIN psb_laporan_action pla ON zz.action = pla.laporan_action_id
      LEFT JOIN psb_laporan_penyebab pls ON zz.penyebabId = pls.idPenyebab
      LEFT JOIN psb_laporan_log plog ON d.id = plog.psb_laporan_log_id
      LEFT JOIN roc on d.Ndem = roc.no_tiket
      LEFT JOIN rock_excel_bank reb ON reb.trouble_no = d.Ndem
      LEFT JOIN roc_active ra ON roc.no_tiket = ra.no_tiket
      LEFT JOIN data_nossa_1_log dn1log ON d.Ndem = dn1log.Incident
      LEFT JOIN mdf a1 ON a.sto = a1.mdf
      LEFT JOIN mdf_grup a2 ON a1.mdf_grup_id = a2.mdf_grup_id
      LEFT JOIN dossier_master dm ON d.Ndem = dm.ND
      LEFT JOIN tambahTiang tt ON zz.ttId=tt.id
      LEFT JOIN psb_myir_wo my on (d.Ndem=my.myir or d.Ndem = my.sc)
      LEFT JOIN psb_sales ps ON ps.kode_sales = my.sales_id
      WHERE d.id = ?
    ',[
      $id
    ])[0];
    $chatID = $order->mainsector;
    // $chatID = '192915232';

    // Informasi teknisi
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ])[0];
    //data material
    $get_materials = DB::select('
      SELECT a.*
      FROM
        psb_laporan_material a
      LEFT JOIN psb_laporan b ON a.psb_laporan_id = b.id
      WHERE id_tbl_mj = ?
    ',[$id]);
    $materials = "";
    if (count($get_materials)>0) {
      foreach ($get_materials as $material) :
        if ($material->id_item<>'')
          $materials .= $material->id_item." : ".$material->qty."[".$material->rfc."]"."\n";
      endforeach;
    }else {
      $materials = "";
    }
    $nama_tim = explode(' ',str_replace(array('-',':'),' ',$order->nama_tim));
    $new_nama_tim = '';
    for ($i=0;$i<count($nama_tim);$i++){
      $new_nama_tim .= $nama_tim[$i];
    }
    //$messaggio = "Pengirim : ".$order->update. "-".$auth->nama."\n";
      if ($order->no_tiket<>NULL) {
        $noorder = $order->no_tiket;
      } else if ($order->ND<>NULL) {
        $noorder = "MIGRASI AS IS ".$order->ND;
      } else {
         if ($order->dispatch_by=='5'){
            $noorder = "MYIR-".$order->myir;
        }
        elseif($order->dispatch_by==''){
            $noorder = "SC.".$order->sc;
        }
        else{
            $noorder = "SC".$order->orderId;
        }

      }
    $messaggio = "Reporting Order ".$noorder."\n\n";
    if ($order->no_tiket<>NULL) {
      $messaggio .= "<b>Followed UP</b> \n";
      $messaggio .= "Opentime : ".$order->Reported_Date."\n";
      $messaggio .= "Is_Hold : ".$order->Hold."\n";
      }
    $messaggio .= "Assignedtime : ".$order->tgl_dispatch2 ? : $order->tgl_dispatch;
    $messaggio .= "\n";
    //$get_group = DB::table('group_telegram')->where('chat_id',$chatID)->first();
    //$messaggio .= "<i>".$get_group->title."</i>\n\n";
    $messaggio .= "Status : ".$order->laporan_status."\n";
    $messaggio .= "Updatetime : ";
    if ($order->tgl_update == NULL) {
      $messaggio .= $order->tgl_laporan."\n";
    } else {
      $messaggio .= $order->tgl_update."\n";
    }

    if ($order->no_tiket==NULL) {
    } else {
      if ($order->umur<0){
        $umur = 0;
      } else {
        $umur = $order->umur;
      }
      $messaggio .= "TTR : ".$umur."h";
      $messaggio .= "\nAction : ".$order->action;
      $messaggio .= "\nSebab : ".$order->penyebab;
    }
    $messaggio .= "\n\n";
    if ($order->no_tiket<>NULL) {
      $id = $order->id_dt;
      $s = $order->no_tiket;
      $messaggio .= "<b>Detail Ticket</b>\n";
      $messaggio .= "<b>".$order->no_tiket."</b> \n";
      $messaggio .= "Customer_Name : ".$order->Customer_Name."\n";
      $messaggio .= "Assigned_To : ".$order->Assigned_to."\n";
      $messaggio .= "Assigned_By : ".$order->Assigned_by."\n";
      $messaggio .= "Service ID : ".$order->no_speedy."\n";
      $headline = explode('+',$order->headline);
      foreach ($headline as $val){
        $messaggio .= trim($val)."\n";
      }
      $messaggio .= "\n";
    } else if ($order->ND<>NULL) {
      $s = "MIG-ASIS ".$order->ND;
      $messaggio .= "\n<b>WO MIGRASI AS IS</b>\n<b>".$order->NAMA."</b>\n";
      $messaggio .= $order->ND."\n";
      $messaggio .= $order->ND_REFERENCE."\n";
      $messaggio .= $order->LVOIE."\n";
      $messaggio .= $order->LCOM."\n";
      $messaggio .= "\n";
      $messaggio .= "<b>Appointment</b>\n";
      $messaggio .= "Timeslot : ".$order->dispatch_teknisi_timeslot."\n";
      $messaggio .= "Manja : ".$order->manja ? : '--'."\n";
      $messaggio .= "\n";

    }else {
      if ($order->dispatch_by=='5'){
          $s = "MYIR-".$order->myir;
          $messaggio .= "\n<b>WO Provisioning</b>\n<b>".$order->customer."</b>\n";
          $messaggio .= "MYIR-".$order->myir."\n";
      }
      elseif($order->dispatch_by==''){
          $s = "SC".$order->sc;
          $messaggio .= "\n<b>WO Provisioning</b>\n<b>".$order->customer."</b>\n";
          $messaggio .= "SC.".$order->sc."\n";
      }
      else{
          $s = "SC".$order->orderId;
          $messaggio .= "\n<b>WO Provisioning</b>\n<b>".$order->orderName."</b>\n";
          $messaggio .= "SC".$order->orderId."\n";
      }

      $messaggio .= $order->ndemPots."\n";
      $messaggio .= $order->ndemSpeedy."\n";
      $messaggio .= $order->kcontact;
      $messaggio .= "<b>NCLI : </b>".$order->orderNcli."\n";

      if($order->dispatch_by=='5'){
          $messaggio .= "<b>MYIR : </b>".'MYIR-'.$order->myir."\n";
          $messaggio .= "<b>Sales : </b>".$order->kode_sales.' - '.$order->nama_sales."\n";
      }
      elseif($order->dispatch_by==''){
          $messaggio .= "<b>MYIR : </b>".'MYIR-'.$order->myir."\n";
          $messaggio .= "<b>No. Internet : </b>".$order->no_internet."\n";
          $messaggio .= "<b>No. Telp : </b>".$order->no_telp."\n";  
          $messaggio .= "<b>Sales : </b>".$order->kode_sales.' - '.$order->nama_sales."\n"; 

      }
      else{
          $messaggio .= "<b>MYIR : </b>".$order->KcontactSC."\n";
          $messaggio .= "<b>Sales : </b>".$order->kode_sales.' - '.$order->nama_sales."\n";
      };

      $messaggio .= "<b>No Pelanggan Aktif : </b>".$order->noPelangganAktif."\n";
      $messaggio .= "<b>Dropcore Label Code : </b>".$order->dropcore_label_code."\n";
      
      $messaggio .= $order->jenisPsb."\n";
      $messaggio .= "\n";
      $messaggio .= "<b>Appointment</b>\n";
      $messaggio .= "Timeslot : ".$order->dispatch_teknisi_timeslot."\n";

      $messaggio .= "Manja : ".$order->manja."\n\n";
      $messaggio .= "<b>Koordinat Pelangggan : </b>".$order->kordinatPel."\n";
      $messaggio .= "<b>Alamat Sales : </b>".$order->alamatSales."\n";
      $messaggio .= "ODP BY Sales : ".$order->odpBySales."\n";
      $messaggio .= "ODP BY Teknisi : ".$order->odpByTeknisi."\n";
      $messaggio .= "\n";
    }

    if ($exists->status_laporan=="8"){
      if ($exists->catatan<>''){
        $messaggio .= "<b>Note : </b>\n";
        $messaggio .= $exists->catatan."\n\n";
      }
      $s .= " #".strtolower($new_nama_tim);
    } else {
      if ($exists->catatan<>''){
        $messaggio .= "<b>Note :</b> \n";
        $messaggio .= $exists->catatan."\n";
      }

      if ($exists->status_laporan=="11"){
        if ($order->ketTiang<>''){
            $messaggio .= "Tambah Tiang : ".$order->ketTiang."\n\n";
        }
      }

      if ($exists->catatanRevoke<>''){
          $messaggio .= "<b>Catatan Revoke : </b> \n";
          $messaggio .= $exists->catatanRevoke."\n";
      }

      $s .= " #".strtolower($new_nama_tim);
      $messaggio .= "\n";
      if ($exists->kordinat_pelanggan<>'')
      {
        $messaggio .= "Kordinat Pelanggan : \n";
        $messaggio .= $exists->kordinat_pelanggan."\n";
      }
      else {
        #$messaggio .= "Tidak ada info kordinat pelanggan\n";
        #$messaggio .= "\n";
      }
      if ($exists->nama_odp<>'')
      {
        $messaggio .= "Nama ODP : \n";
        $messaggio .= $exists->nama_odp."\n";
        $messaggio .= " Redaman ODP : \n";
        $messaggio .= $exists->redaman_odp. "\n";
      }
      else {
        #$messaggio .= "Tidak ada info nama ODP pelanggan\n";
        #$messaggio .= "\n";
      }
      if ($exists->kordinat_odp<>'')
      {
        $messaggio .= "Kordinat ODP : \n";
        $messaggio .= $exists->kordinat_odp."\n";
      }
      else {
        #$messaggio .= "Tidak ada info kordinat ODP pelanggan\n";
        #$messaggio .= "\n";
      }
      $messaggio .= "#report".strtolower($new_nama_tim)."\n";
      $messaggio .= "\n";
      $messaggio .= $materials;
  
    if ($order->no_tiket<>NULL){
      $photoInputs = [
        'Label','Foto_Action','ODP', 'Hasil_Ukur_OPM' ,'Berita_Acara', 'Rumah_Pelanggan', 'Test_Layanan_Internet', 'Test_Layanan_TV', 'Test_Layanan_Telepon', 'MODEM', 'SN_ONT_RUSAK', 'Sambungan_Kabel_DC', 'Pullstrap_S_Clamp', 'Stoper', 'Clamp_Hook_Clamp_Pelanggan', 'Roset', 'Patchcord', 'Slack_Kabel_DC'
      ];
      $folder = "asurance";
    } else {
      $photoInputs = [
        'Foto_Action','Lokasi', 'ODP', 'Redaman_Pelanggan', 'Instalasi_Kabel', 'SN_ONT', 'SN_STB', 'Live_TV',
        'TVOD', 'Speedtest', 'Berita_Acara', 'Telephone_Incoming', 'Telephone_Outgoing', 'Wifi_Analyzer', 'Pullstrap', 'Tray_Cable', 'K3','MyIndihome_Pelanggan','Login_MyIndihome','KLEM_S', 'LABEL','QC_1','QC_2','QC_3','Foto_Pelanggan_dan_Teknisi','Excheck_Helpdesk'
      ];
      $folder = "evidence";
    }

    if($exists->status_laporan=="2" || $exists->status_laporan=="11"){
      Telegram::sendMessage([
        // 'chat_id' => "-1001248567976",
        'chat_id' => "-285944800",
        'parse_mode' => 'html',
        'text' => $messaggio
      ]);
    }

    $chat   = '';
    $chat2  = '';
    if ($exists->status_laporan=="11" || $exists->status_laporan=="2" || $exists->status_laporan=="24" || $exists->status_laporan=="34" || $exists->status_laporan=="3" || $exists->status_laporan=="16" || $exists->status_laporan=="12" || $exists->status_laporan=="25" || $exists->status_laporan=="26" || $exists->status_laporan=="27"){
      if ($order->area_migrasi=="INNER"){
          Telegram::sendMessage([
            'chat_id' => '64811812',
            'parse_mode' => 'html',
            'text' => $messaggio
          ]);

          for($i=0;$i<count($photoInputs);$i++) {
            $file = public_path()."/upload3/".$folder."/".$id."/".$photoInputs[$i].".jpg";
            if (file_exists($file)){
              Telegram::sendPhoto([
                'chat_id' => '64811812',
                'caption' => $photoInputs[$i]." ".$s,
                'photo' => $file
              ]);
            }
          }

          Telegram::sendMessage([
            'chat_id' => '113449636',
            'parse_mode' => 'html',
            'text' => $messaggio
          ]);

          for($i=0;$i<count($photoInputs);$i++) {
            $file = public_path()."/upload3/".$folder."/".$id."/".$photoInputs[$i].".jpg";
            if (file_exists($file)){
              Telegram::sendPhoto([
                'chat_id' => '113449636',
                'caption' => $photoInputs[$i]." ".$s,
                'photo' => $file
              ]);
            }
          }
      }
      else if($order->area_migrasi=="TJL" || $order->area_migrasi=="KDG" ){
          $chat = '412967435';
      }
      else if($order->area_migrasi=="BBR"){
          $chat  = '452128209';
          $chat2 = '314081494';
      }
      else if($order->area_migrasi=="BLC"){
          $chat = '109025591';
      };

      if ($chat<>''){
        Telegram::sendMessage([
          'chat_id' => $chat,
          'parse_mode' => 'html',
          'text' => $messaggio
        ]);
      }

      if ($chat2<>''){
        Telegram::sendMessage([
          'chat_id' => $chat2,
          'parse_mode' => 'html',
          'text' => $messaggio
        ]);
      }
    };

    Telegram::sendMessage([
      'chat_id' => $chatID,
      'parse_mode' => 'html',
      'text' => $messaggio
    ]);

    for($i=0;$i<count($photoInputs);$i++) {
      $file = public_path()."/upload3/".$folder."/".$id."/".$photoInputs[$i].".jpg";

      if (file_exists($file)){
        Telegram::sendPhoto([
          'chat_id' => $chatID,
          'caption' => $photoInputs[$i]." ".$s,
          'photo' => $file
        ]);
      }
    }

    for($i=0;$i<count($photoInputs);$i++) {
      $file = public_path()."/upload3/".$folder."/".$id."/".$photoInputs[$i].".jpg";

      if (file_exists($file)){
        # sendPhoto to KPRO
        if ($photoInputs[$i]=="Lokasi"){
            // $result = $get_data[0];
            echo exec('/usr/share/tg/bin/telegram-cli -WR -e "send_photo @kprofoto_bot /mnt/hdd3/upload/evidence/'.$id.'/Lokasi.jpg ['.$order->orderId.'] "');
        }
      }
    }
  }
}

public static function sendReportUndispacth()
  {
      date_default_timezone_set("Asia/Makassar");
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
      $enddate = date('Y-m-d');

      $datas = DB::SELECT('
      SELECT
        *
      FROM
        Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
        LEFT JOIN mdf m ON dps.sto = m.mdf
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
      WHERE
          (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
          AND dps.orderStatusId = 16
          AND dt.id is NULL
      ');

      // return $datas;
      // dd($datas);

       $pesan = " List Undispatch ".date('d-m-Y')."|".date('H:i:s')."\n" ;
       $pesan .= "================================= \n";

       $total = count($datas);
       foreach($datas as $data){
          $pesan .= 'SC : '.$data->orderId."\n";
          $pesan .= 'STO : '.$data->mdf."\n";
          $pesan .= 'Transaksi : '.$data->jenisPsb."\n";
          $pesan .= 'Waktu : '.$data->orderDate."\n\n";
      };

      $pesan .= 'Total '.$total;
    
      if (count($datas)<>0){
          Telegram::sendMessage([
            'chat_id' => "-265593533",
            'parse_mode' => 'html',
            'text' => $pesan
          ]);

          Telegram::sendMessage([
            'chat_id' => "-187134069",
            'parse_mode' => 'html',
            'text' => '" SC backend Follow up Setiap 5 menit sekali & Cek 2ndstb "'
          ]);

          Telegram::sendMessage([
            'chat_id' => "192915232",
            'parse_mode' => 'html',
            'text' => 'semua pesan terkirim 1 Jam'
          ]);
      }

      echo 'done';
  }

  public static function sendReportUndispacthLagi()
  {
      date_default_timezone_set("Asia/Makassar");
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
      $enddate = date('Y-m-d');

      $dataActiv = DB::SELECT('
      SELECT
        *
      FROM
        Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
        LEFT JOIN mdf m ON dps.sto = m.mdf
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
      WHERE
          (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
          AND dps.orderStatusId = 52 order by dt.updated_at desc
      ');

      $jumlah = count($dataActiv);
      $pesan2 = "List activation completed ".date('d-m-Y')."|".date('H:i:s')."\n";
      $pesan2 .= "================================ \n";
      foreach($dataActiv as $activ){
          $pesan2 .= 'SC : '.$activ->orderId."\n";
          $pesan2 .= 'Transaksi : '.$activ->jenisPsb."\n";
          $pesan2 .= 'Time Act comp : '.$activ->updated_at."\n\n";
      }
      $pesan2 .= 'Total Act : '.$jumlah;

      if (count($dataActiv)<>0){
          // return $dataActiv;
          Telegram::sendMessage([
            'chat_id' => "140199221",
            'parse_mode' => 'html',
            'text' => $pesan2
          ]);

          Telegram::sendMessage([
            'chat_id' => "-1001127807166",
            'parse_mode' => 'html',
            'text' => $pesan2
          ]);

          Telegram::sendMessage([
            'chat_id' => "192915232",
            'parse_mode' => 'html',
            'text' => 'semua pesan terkirim 10 menit'
          ]);
      }

      echo 'done';
  }

  public static function sendSms()
  {
      $telp = ["082353089050","08977777136"];
      $jml = count($telp);
      $noTelp = '';
      for ($a=0; $a<$jml; $a++){
          $noTelp .= '("'.$telp[$a].'","Semangat Pagi, Tomman !","true","3006"),';
      }

      $noTelp = substr($noTelp, 0, -1);
      $sql = 'INSERT INTO outbox (DestinationNumber,TextDecoded,MultiPart,CreatorID) values '.$noTelp;
      // dd($sql);

      DB::connection('mysql2')->insert($sql);
      return back()->with('alerts',[['type' => 'success', 'text' => 'Pesan Berhasil Dikirim']]);
  }

  public static function sendTelegramSSC1($id, $nik)
  {
      $jenis = '';
      $sql = 'SELECT u.id_user, u.password, u.id_karyawan, k.nama, u.level, u.psb_remember_token, u.maintenance_level
      FROM user u
      LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
      WHERE id_user = "'.$nik.'"';

      $dataNik = DB::select($sql);

      $get_data = DB::SELECT('
        SELECT
          a.*,
          b.mdf_grup_id
        FROM
          Data_Pelanggan_Starclick a
          LEFT JOIN mdf b ON a.sto = b.mdf
        WHERE a.orderId = ?
      ',[ $id ]);

      if (count($get_data)==0) {
        $get_data = DB::SELECT('
          SELECT
           a.*,
           b.mdf_grup_id,
           a.ND as orderId,
           a.NAMA as orderName,
           a.LQUARTIER as kcontact,
           0 as lat,
           0 as lon,
           0 as kode_sales, 
           0 as nama_sales
          FROM
            dossier_master a
          LEFT JOIN mdf b ON a.STO = b.mdf
          WHERE
            a.ND = ?
        ', [ $id ]);
      }

      if (count($get_data)==0){
            $get_data = DB::SELECT('
                SELECT
                  a.*,
                  b.mdf_grup_id,
                  dt.*,
                  pss.kode_sales,
                  pss.nama_sales
                FROM
                  psb_myir_wo a
                  LEFT JOIN mdf b ON a.sto = b.mdf
                  LEFT JOIN dispatch_teknisi dt ON a.myir=dt.Ndem
                  LEFT JOIN psb_sales ps ON ps.id = a.sales_id
                  LEFT JOIN psb_sales pss ON pss.id = a.sales_id
                WHERE a.myir = ?
              ',[ $id ]);

            $jenis = 'myir';
      };
      
      // getDataDispatc
      $getDataDispatc = DB::select('SELECT * FROM dispatch_teknisi WHERE Ndem = ?',[$id]);

      // get regu
      $get_regu = DB::SELECT('
        SELECT a.uraian,a.mainsector FROM regu a WHERE a.id_regu = ?
      ',[ $getDataDispatc[0]->id_regu ]);
      $get_regu = $get_regu[0];

      // get manja
      $get_manja_status = [];
      if (!empty($getDataDispatc)){
          $get_manja_status = DB::SELECT('
            SELECT a.manja_status FROM manja_status a WHERE a.manja_status_id = ?
          ',[ $getDataDispatc[0]->manja_status ]);
          if (!empty($get_manja_status)){
             $get_manja_status = $get_manja_status[0];
          };
      };

      if ($jenis=='myir'){
           // get data
          $get_data = $get_data[0];

          // send to Telegram SSC1
          // $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";
          $messageio = "Pengirim : ".$dataNik[0]->id_user."-".$dataNik[0]->nama."\n\n";
          $messageio .= "Manajemen Janji\n";
          $messageio .= "Regu : ".$get_regu->uraian."\n";
          $messageio .= "Tanggal : ".$getDataDispatc[0]->updated_at."\n";
          $messageio .= "Jenis Layanan : ".$get_data->jenis_layanan."\n";
          $messageio .= "Related to MYIR-".$get_data->myir."\n".$get_data->customer."\n";
          $messageio .= "Koordinat Sales : ".$get_data->kordinatPel."\n";
          $messageio .= "Alamat Sales : ".$get_data->alamatSales."\n";
          $messageio .= "No Internet : ".$get_data->no_intenet."\n";
          $messageio .= "No. Telp : ".$get_data->no_telp."\n";
          $messageio .= "ODP : ".$get_data->namaOdp."\n";
          $messageio .= "Sales : ".$get_data->kode_sales.' - '.$get_data->nama_sales."\n";


          $chatID = "-199400365";
          // $chatID = "192915232";
          Telegram::sendMessage([
            'chat_id' => $chatID,
            'text' => $messageio
          ]);

          if ($get_regu->mainsector<>NULL){
            $chatID = $get_regu->mainsector;
          } else {
            $chatID = "-".$get_data->mdf_grup_id;
          }

          Telegram::sendMessage([
            'chat_id' => $chatID,
            'text' => $messageio
          ]);

          Telegram::sendMessage([
            'chat_id' => $chatID,
            'text' => "Lokasi SC".$get_data->myir." ".$get_data->customer." 👇🏻"
          ]);
      }
      else{
          // get data
          $get_data = $get_data[0];
          $manja = $getDataDispatc[0]->manja;
          // send to Telegram SSC1
          // $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";
          $messageio = "Pengirim : ".$dataNik[0]->id_user."-".$dataNik[0]->nama."\n\n";
          $messageio .= "Manajemen Janji\n";
          $messageio .= "Regu : ".$get_regu->uraian."\n";
          $messageio .= "Tanggal : ".$getDataDispatc[0]->updated_at."\n";
          
          if(!empty($get_manja_status)){
             $messageio .= "Status Manja : ".$get_manja_status->manja_status."\n";
          }
          else{
             $messageio .= "- \n";
          }

          $messageio .= "Keterangan : \n".$manja."\n\n";
          $messageio .= "Related to SC".$get_data->orderId."\n".$get_data->orderName."\n".$get_data->kcontact."\n";
          $messageio .= "No Internet : ".$get_data->ndemSpeedy."\n";

          $chatID = "-199400365";
          // $chatID = "192915232";
          Telegram::sendMessage([
            'chat_id' => $chatID,
            'text' => $messageio
          ]);

          if ($get_data->lat<>NULL){
            Telegram::sendMessage([
              'chat_id' => $chatID,
              'text' => "Lokasi SC".$get_data->orderId." ".$get_data->orderName." 👇🏻"
            ]);
            Telegram::sendLocation([
              'chat_id' => $chatID,
              'latitude' => $get_data->lat,
              'longitude' => $get_data->lon
            ]);
          }

          if ($get_regu->mainsector<>NULL){
            $chatID = $get_regu->mainsector;
          } else {
            $chatID = "-".$get_data->mdf_grup_id;
          }

          Telegram::sendMessage([
            'chat_id' => $chatID,
            'text' => $messageio
          ]);

          Telegram::sendMessage([
            'chat_id' => $chatID,
            'text' => "Lokasi SC".$get_data->orderId." ".$get_data->orderName." 👇🏻"
          ]);

          Telegram::sendLocation([
            'chat_id' => $chatID,
            'latitude' => $get_data->lat,
            'longitude' => $get_data->lon
          ]);
      }
  }

  public static function matrikSector(){
      date_default_timezone_set('Asia/Makassar');
      $date = date('Y-m-d');
      $area = 'ALL';
      $get_area = DispatchModel::getGroupTelegramProv();
      $getTeamMatrix = DispatchModel::getTeamMatrix($date,$area);
      $data = array();
      $lastRegu = '';

      foreach($getTeamMatrix as $team) :
        $listWO = DispatchModel::listWO($date,$team->id_regu);
        $data[$team->chatTl][] = array(
          "close_status" => $team->close_status,
          "PROGRESS" => $team->PROGRESS,
          "mitra" => $team->mitra,
          "KENDALA" => $team->KENDALA,
          "uraian"=>$team->uraian,
          "jumlah"=>$team->jumlah,
          "gangguan"=>$team->gangguan,
          "prov"=>$team->prov,
          "UP"=>$team->UP,
          "gangguan_UP_Copper"=>$team->gangguan_UP_Copper,
          "gangguan_UP_GPON"=>$team->gangguan_UP_GPON,
          "gangguan_UP_DES"=>$team->gangguan_UP_DES,
          "addon_UP"=>$team->addon_UP,
          "ccan_UP"=>$team->ccan_UP,
          "prov_UP"=>$team->prov_UP,
          "asis_UP"=>$team->asis_UP,
          "asis_MH"=>$team->asis_MH,
          "ccan_MH"=>$team->ccan_MH,
          "addon_MH"=>$team->addon_MH,
          "gangguan_Copper_MH"=>$team->gangguan_Copper_MH,
          "gangguan_GPON_MH"=>$team->gangguan_GPON_MH,
          "gangguan_DES_MH"=>$team->gangguan_DES_MH,
          "prov_MH"=>$team->prov_MH,
          "target_MH"=>$team->kemampuan,
          "kemampuan"=>$team->kemampuan,
          "nik1_absen"=>$team->nik1_absen,
          "nik2_absen"=>$team->nik2_absen,
          "listWO" =>$listWO
        );
      endforeach;
      // dd($data[171053504]);
      foreach($get_area as $areas){
          $chatId    = $areas->chatTl;
          $messaggio = '';
          $messaggio .= "<b>".$areas->title."</b>\n";
          $messaggio .= 'Report Tanggal : '.date('d-m-Y H:i:s')."\n\n";
          // $messaggio .= 'TL : '.$areas->TL."\n\n";

          $list = $data[$areas->chatTl];
          $jmlData = count($data[$areas->chatTl]);
          // dd($list);
          for ($iii=0; $iii<=$jmlData-1; $iii++){
              $messaggio .= "<b>".$list[$iii]['uraian']."</b>\n";

              foreach($list[$iii]['listWO'] as $wo){
                  if ($wo->status_laporan==6 || $wo->status_laporan==NULL){
                      $laporanStatus = 'NO UPDATE';
                  }
                  else{
                      $laporanStatus = $wo->laporan_status;
                  }

                  if ($wo->tglDispatch<>''){
                    date_default_timezone_set('Asia/Makassar');
                    $awal  = date_create($wo->tglDispatch);
                    $akhir = date_create(); // waktu sekarang
                    $diff  = date_diff( $awal, $akhir );

                    if($diff->d <> 0){
                          $waktu = $diff->d.'d '.$diff->h.'h '.$diff->i.'min ';
                    }
                    else {
                          if ($diff->h==0){
                              $waktu = $diff->i.'min ';
                          }
                          else{
                              $waktu = $diff->h.'h '.$diff->i.'min';
                          }
                    }
                  }
                  else{
                    $waktu = '';
                  }

                  if ($wo->dispatch_by==NULL){
                      $messaggio .= $waktu.' | SC'.$wo->Ndem.' | '.$laporanStatus."\n";
                  }
              }

              $messaggio .= "\n";
          }

          Telegram::sendMessage([
                'chat_id' => $chatId,
                'parse_mode' => 'html',
                'text' => $messaggio
          ]);

          if ($chatId=='76020428'){
            Telegram::sendMessage([
                'chat_id' => '90829569',
                'parse_mode' => 'html',
                'text' => $messaggio
            ]);
          }

          Telegram::sendMessage([
                'chat_id' => '192915232',
                'parse_mode' => 'html',
                'text' => $messaggio
          ]);
      }
  }

  public static function sendReportUndispacthAo()
  {
      date_default_timezone_set("Asia/Makassar");
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
      $enddate = date('Y-m-d');

      $datas = DB::SELECT('
      SELECT
        *
      FROM
        Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
        LEFT JOIN mdf m ON dps.sto = m.mdf
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
      WHERE
          (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
          AND dps.orderStatusId = 16 and dps.jenisPsb like "AO|%"
          AND dt.id is NULL
          order by m.area asc
      ');

      // return $datas;
      // dd($datas);

       $area = ["TTG","BJM","BBR","BLN"];
       $jml  = count($area);
       $pesan = '';
       for ($a=0; $a<=$jml-1; $a++){
           $pesan = " List Undispatch ".date('d-m-Y')."|".date('H:i:s')."\n" ;
           $pesan .= "================================= \n";
           if ($area[$a]=='TTG'){
              $datel = 'TJL';
           }
           else if ($area[$a]=='BLN'){
              $datel = 'BLC';
           }
           else {
              $datel = $area[$a];
           };

           $pesan .= 'DATEL : '.$datel."\n\n";

           $angka = 0;
           foreach($datas as $data){
              if ($data->area==$area[$a]){
                $pesan .= 'SC : '.$data->orderId."\n";
                $pesan .= 'STO : '.$data->mdf."\n";
                $pesan .= 'Transaksi : '.$data->jenisPsb."\n";
                $pesan .= 'Waktu : '.$data->orderDate."\n\n";

                $angka += 1;
              }
          };

          $pesan .= 'Total SC : '.$angka;
          if($angka<>0){
            Telegram::sendMessage([
              'chat_id' => "-1001309386322",
              'parse_mode' => 'html',
              'text' => $pesan
            ]);
          };
       }
  }

   public static function sendTelegramSSC1SyncSc($id, $nik)
  {
      $jenis = '';
      $sql = 'SELECT u.id_user, u.password, u.id_karyawan, k.nama, u.level, u.psb_remember_token, u.maintenance_level
      FROM user u
      LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
      WHERE id_user = "'.$nik.'"';

      $dataNik = DB::select($sql);

      $get_data = DB::SELECT('
        SELECT
          a.*,
          b.mdf_grup_id
        FROM
          Data_Pelanggan_Starclick a
          LEFT JOIN mdf b ON a.sto = b.mdf
        WHERE a.orderId = ?
      ',[ $id ]);

      // getDataDispatc
      $getDataDispatc = DB::select('SELECT * FROM dispatch_teknisi WHERE Ndem = ?',[$id]);

      // get regu
      $get_regu = DB::SELECT('
        SELECT a.uraian,a.mainsector FROM regu a WHERE a.id_regu = ?
      ',[ $getDataDispatc[0]->id_regu ]);
      $get_regu = $get_regu[0];

      // get manja
      $get_manja_status = [];
      if (!empty($getDataDispatc)){
          $get_manja_status = DB::SELECT('
            SELECT a.manja_status FROM manja_status a WHERE a.manja_status_id = ?
          ',[ $getDataDispatc[0]->manja_status ]);
          if (!empty($get_manja_status)){
             $get_manja_status = $get_manja_status[0];
          };
      };

      // get data
      $get_data = $get_data[0];
      $manja = $getDataDispatc[0]->manja;

      // send to Telegram SSC1
      // $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";
      $messageio = "Syncron SC By  : ".$dataNik[0]->id_user."-".$dataNik[0]->nama."\n\n";
      $messageio .= "SC.".$get_data->orderId."\n";
      $messageio .= "Manajemen Janji\n";
      $messageio .= "Regu : ".$get_regu->uraian."\n";
      $messageio .= "Tanggal : ".$getDataDispatc[0]->updated_at."\n";
       
      if(!empty($get_manja_status)){
             $messageio .= "Status Manja : ".$get_manja_status->manja_status."\n";
      }
      else{
         $messageio .= "Status Manja : - \n";
      }

      $messageio .= "Keterangan : \n".$manja."\n\n";
      $messageio .= "Related to SC".$get_data->orderId."\n".$get_data->orderName."\n".$get_data->kcontact."\n";
      $messageio .= "No Internet : ".$get_data->ndemSpeedy."\n";

      $chatID = "-199400365";
      // $chatID = "192915232";
      Telegram::sendMessage([
        'chat_id' => $chatID,
        'text' => $messageio
      ]);

      if ($get_data->lat<>NULL){
        Telegram::sendMessage([
          'chat_id' => $chatID,
          'text' => "Lokasi SC".$get_data->orderId." ".$get_data->orderName." 👇🏻"
        ]);
        Telegram::sendLocation([
          'chat_id' => $chatID,
          'latitude' => $get_data->lat,
          'longitude' => $get_data->lon
        ]);
      }

      if ($get_regu->mainsector<>NULL){
        $chatID = $get_regu->mainsector;
      } else {
        $chatID = "-".$get_data->mdf_grup_id;
      }

      Telegram::sendMessage([
        'chat_id' => $chatID,
        'text' => $messageio
      ]);

      Telegram::sendMessage([
        'chat_id' => $chatID,
        'text' => "Lokasi SC".$get_data->orderId." ".$get_data->orderName." 👇🏻"
      ]);

      Telegram::sendLocation([
        'chat_id' => $chatID,
        'latitude' => $get_data->lat,
        'longitude' => $get_data->lon
      ]);
  }

  public static function reportAbsenProv(){
    $dataSektor = DB::table('group_telegram')
              ->where('sektor','LIKE','%PROV%')
              ->get();

    foreach($dataSektor as $sektor){
        // $chatID = '192915232';
        if ($sektor->chat_id<>'-199400365'){
            $chatID = $sektor->chat_id;
          
            $pesan  = "Absen Malam Sudah Dibuka, Pastikan Lakukan Absen Sebelum Jam 12 Malam \n\n";
            $pesan .= "Ketikkan \n";
            $pesan .= "/absenProv {nik} \n\n"; 
            $pesan .= "Untuk Melakukan Absen Via Bot Tomman"; 

            Telegram::sendMessage([
              'chat_id' => $chatID,
              'text' => $pesan
            ]); 
        }
    }
  }

  public static function sendReportTelegramMaterial($id){
    date_default_timezone_set("Asia/Makassar");

    //data material
    $get_materials = DB::select('
      SELECT a.*, c.*, b.modified_at as tglLaporan, d.id_regu, d.uraian
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
      LEFT JOIN psb_laporan_material c ON c.psb_laporan_id=b.id
      LEFT JOIN regu d on a.id_regu=d.id_regu
      WHERE a.id = ?
    ',[$id]);

    $materials = "";
    $materials = 'WO. '.$get_materials[0]->Ndem."\n\n";
    $materials.= "Tgl Laporan ".date('d-m-Y H:i:s')."\n";
    $materials.= "TIM : ".$get_materials[0]->uraian."\n";
    $materials.= "Material Terpakai : \n";
    $materials.= "=========================== \n";
    
    if (count($get_materials)>0) {
      foreach ($get_materials as $material) :
        if ($material->id_item<>'')
          $materials .= $material->id_item." : ".$material->qty." [ ".$material->rfc." ] "."\n";
      endforeach;
    }
    
    Telegram::sendMessage([ 
    'chat_id' => '-326127951',
    'text'    => $materials
    ]);  
  }

  public static function sendReportTelegramKp($id){
      date_default_timezone_set("Asia/Makassar");

      // $auth = session('auth');
      $order = DB::select('
        SELECT
          *,
          ms2n.*,
          d.Ndem as NdemOrder,
          d.id as id_dt,
          d.created_at as tgl_dispatch,
          d.updated_at as tgl_dispatch2,
          r.uraian as nama_tim,
          r.mainsector,
          m.Kcontact as mKcontact,
          m.ND as mND,
          m.ND_Speedy as mND_Speedy,
          m.Nama as mNama,
          m.Alamat as mAlamat,
          p2.mdf_grup_id as grup_id_psb,
          m2.mdf_grup_id as grup_id_migrasi,
          p2.mdf_grup as grup_psb,
          m2.mdf_grup as grup_migrasi,
          a2.mdf_grup as grup_starclick,
          a2.mdf_grup_id as grup_id_starclick,
          a.orderName,
          a.ndemPots,
          a.ndemSpeedy,
          a.sto,
          a.jenisPsb,
          a.Kcontact as KcontactSC,
          a.orderNcli,
          yy.laporan_status,
          reb.nd_telp as no_telp,
          dn1log.Service_ID as no_speedy,
          dn1log.Summary as headline,
          TIMESTAMPDIFF( HOUR , dn1log.UmurDatetime, "'.date('Y-m-d H:i:s').'") as umur,
          ra.no_tiket as status_tiket,
          dd.manja_status,
          ddd.dispatch_teknisi_timeslot,
          pla.action,
          dm.*,plog.mttr,
          zz.modified_at as tgl_update,
          zz.created_at as tgl_laporan,
          tt.ketTiang,
          zz.noPelangganAktif,
          zz.catatan,
          d.kordinatPel,
          d.alamatSales,
          pls.penyebab,
          my.myir,
          my.customer,
          my.picPelanggan
        FROM dispatch_teknisi d
        LEFT JOIN manja_status dd ON d.manja_status = dd.manja_status_id
        LEFT JOIN dispatch_teknisi_timeslot ddd ON d.updated_at_timeslot = ddd.dispatch_teknisi_timeslot_id
        LEFT JOIN regu r ON (d.id_regu=r.id_regu)
        LEFT JOIN ms2n USING (Ndem)
        LEFT JOIN mdf p1 ON ms2n.mdf=p1.mdf
        LEFT JOIN mdf_grup p2 ON p1.mdf_grup_id = p2.mdf_grup_id
        LEFT JOIN micc_wo m on d.Ndem = m.ND
        LEFT JOIN mdf m1 ON m.MDF = m1.mdf
        LEFT JOIN mdf_grup m2 ON m1.mdf_grup_id = m2.mdf_grup_id
        LEFT JOIN Data_Pelanggan_Starclick a ON d.Ndem = a.orderId
        LEFT JOIN psb_laporan zz ON d.id = zz.id_tbl_mj
        LEFT JOIN psb_laporan_status yy ON zz.status_laporan = yy.laporan_status_id
        LEFT JOIN psb_laporan_action pla ON zz.action = pla.laporan_action_id
        LEFT JOIN psb_laporan_penyebab pls ON zz.penyebabId = pls.idPenyebab
        LEFT JOIN psb_laporan_log plog ON d.id = plog.psb_laporan_log_id
        LEFT JOIN roc on d.Ndem = roc.no_tiket
        LEFT JOIN rock_excel_bank reb ON reb.trouble_no = d.Ndem
        LEFT JOIN roc_active ra ON roc.no_tiket = ra.no_tiket
        LEFT JOIN data_nossa_1_log dn1log ON d.Ndem = dn1log.Incident
        LEFT JOIN mdf a1 ON a.sto = a1.mdf
        LEFT JOIN mdf_grup a2 ON a1.mdf_grup_id = a2.mdf_grup_id
        LEFT JOIN dossier_master dm ON d.Ndem = dm.ND
        LEFT JOIN tambahTiang tt ON zz.ttId=tt.id
        LEFT JOIN psb_myir_wo my on d.Ndem=my.myir
        LEFT JOIN psb_sales ps ON ps.id = my.sales_id
        WHERE d.id = ?
      ',[
        $id
      ])[0];
      // $chatID = $order->mainsector;
      // $chatID = '192915232';

      $getSektor = DB::table('group_telegram')->where('chat_id',$order->mainsector)->first();
         
      if ($order->dispatch_by==null){
          $orderan   = $order->NdemOrder;
          $pelanggan = $order->orderName ? : $order->NAMA;
          $hp        = $order->KcontactSC ? : $order->noPelangganAktif;
          $sales     = ''; 
      }
      elseif($order->dispatch_by==5){
          $orderan    = 'MYIR-'.$order->NdemOrder;
          $pelanggan  = $order->customer;
          $hp         = $order->picPelanggan;
          $sales      = $order->kode_sales.' - '.$order->nama_sales;
      }
    
      $pesan  = "Report Kendala Pelanggan \n";
      $pesan .= "==============================\n";
      $pesan .= "<b>Sektor : </b>".$getSektor->title."\n";
      $pesan .= "<b>TIM : </b>".$order->uraian."\n";
      $pesan .= "<b>Report Date : </b>".$order->tgl_update."\n";
      $pesan .= "<b>Order : </b>".$orderan."\n";
      $pesan .= "<b>Status : </b>".$order->laporan_status."\n";
      $pesan .= "<b>Keterangan : </b>".$order->catatan."\n";
      $pesan .= "<b>Pelanggan : </b>".$pelanggan."\n";
      $pesan .= "<b>HP : </b>".$hp."\n";
      $pesan .= "<b>Alamat Sales : </b>".$order->alamatSales."\n";
      $pesan .= "<b>Sales : </b>".$sales."\n";


      if($getSektor->chat_solusi<>''){
          $chatID = $getSektor->chat_solusi;
          Telegram::sendMessage([
              'chat_id' => $chatID,
              'parse_mode' => 'html',
              'text' => $pesan
          ]);

          Telegram::sendMessage([
              'chat_id' => '192915232',
              'parse_mode' => 'html',
              'text' => $pesan
          ]);
      };    
  }

  public static function sendReportTelegramReboundary($id){
    date_default_timezone_set("Asia/Makassar");

    // $auth = session('auth');
    $order = DB::select('
      SELECT
        d.*,
        d.id as id_dt,
        d.created_at as tgl_dispatch,
        d.updated_at as tgl_dispatch2,
        r.uraian as nama_tim,
        r.mainsector,
        a.orderName,
        a.ndemPots,
        a.ndemSpeedy,
        a.sto,
        a.jenisPsb,
        a.Kcontact,
        a.orderNcli,
        yy.laporan_status,
        zz.modified_at as tgl_update,
        zz.created_at as tgl_laporan,
        zz.noPelangganAktif,
        zz.catatanRevoke,
        zz.catatan,
        d.kordinatPel,
        d.alamatSales,
        zz.redaman_odp,
        rs.tarikan_lama,
        rs.estimasi_tarikan,
        rs.odp_lama,
        rs.odp_baru,
        rs.koor_pelanggan,
        rs.koor_odp_lama,
        rs.scid,
        rs.koor_odp_baru,
        tt.ketTiang
      FROM dispatch_teknisi d
      LEFT JOIN regu r ON d.id_regu=r.id_regu
      LEFT JOIN reboundary_survey rs ON d.Ndem=rs.no_tiket_reboundary
      LEFT JOIN Data_Pelanggan_Starclick a ON rs.scid = a.orderId
      LEFT JOIN psb_laporan zz ON d.id = zz.id_tbl_mj
      LEFT JOIN psb_laporan_status yy ON zz.status_laporan = yy.laporan_status_id
      LEFT JOIN psb_laporan_log plog ON d.id = plog.psb_laporan_log_id
      LEFT JOIN tambahTiang tt ON zz.ttId=tt.id
      WHERE d.id = ?
    ',[
      $id
    ])[0];
  
    $chatID = $order->mainsector;
    // $chatID = '192915232';

    // Informasi teknisi
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ])[0];

    //data material
    $get_materials = DB::select('
      SELECT a.*
      FROM
        psb_laporan_material a
      LEFT JOIN psb_laporan b ON a.psb_laporan_id = b.id
      WHERE id_tbl_mj = ?
    ',[$id]);

    $materials = "";
    if (count($get_materials)>0) {
      foreach ($get_materials as $material) :
        if ($material->id_item<>'')
          $materials .= $material->id_item." : ".$material->qty."[".$material->rfc."]"."\n";
      endforeach;
    }else {
      $materials = "";
    }
    
    $nama_tim = explode(' ',str_replace(array('-',':'),' ',$order->nama_tim));
    $new_nama_tim = '';
    for ($i=0;$i<count($nama_tim);$i++){
      $new_nama_tim .= $nama_tim[$i];
    }
    
    //$messaggio = "Pengirim : ".$order->update. "-".$auth->nama."\n";
    $noorder = "REBOUNDARY INX".$order->Ndem;
   
    $messaggio = "Reporting Order ".$noorder."\n\n";
    $messaggio .= "Assignedtime : ".$order->tgl_dispatch2 ? : $order->tgl_dispatch;
    $messaggio .= "\n";
    //$get_group = DB::table('group_telegram')->where('chat_id',$chatID)->first();
    //$messaggio .= "<i>".$get_group->title."</i>\n\n";
    $messaggio .= "Status : ".$order->laporan_status."\n";
    $messaggio .= "Updatetime : ";
    if ($order->tgl_update == NULL) {
      $messaggio .= $order->tgl_laporan."\n";
    } else {
      $messaggio .= $order->tgl_update."\n";
    }

    $s = "INX".$order->Ndem;
    $messaggio .= "\n<b>WO Reboudnary</b>\n<b>".$order->orderName."</b>\n";
    $messaggio .= "INX".$order->Ndem."\n";
    $messaggio .= $order->ndemPots."\n";
    $messaggio .= $order->ndemSpeedy."\n";
    $messaggio .= $order->Kcontact."\n";
    $messaggio .= "No. Pelanggan AKtif : ".$order->noPelangganAktif."\n";
    $messaggio .= "<b>OrderId : </b>".$order->scid."\n";
    $messaggio .= "<b>NCLI : </b>".$order->orderNcli."\n";
    $messaggio .= "<b>Koordinat Pelangggan : </b>".$order->koor_pelanggan."\n";
    $messaggio .= "\n";    
    $messaggio .= "<b>Tarikan Awal : </b>".$order->tarikan_lama." m \n";
    $messaggio .= "<b>Estimasi Tarikan : </b>".$order->estimasi_tarikan." m \n\n";


    if ($exists->status_laporan=="8"){
      if ($exists->catatan<>''){
        $messaggio .= "<b>Note : </b>\n";
        $messaggio .= $exists->catatan."\n\n";
      }
      $s .= " #".strtolower($new_nama_tim);
    } else {
      if ($exists->catatan<>''){
        $messaggio .= "<b>Note :</b> \n";
        $messaggio .= $exists->catatan."\n";
      }

      if ($exists->status_laporan=="11"){
        if ($order->ketTiang<>''){
            $messaggio .= "Tambah Tiang : ".$order->ketTiang."\n\n";
        }
      }

      if ($exists->catatanRevoke<>''){
          $messaggio .= "<b>Catatan Revoke : </b> \n";
          $messaggio .= $exists->catatanRevoke."\n";
      }

      $s .= " #".strtolower($new_nama_tim);
      $messaggio .= "\n";
      if ($exists->kordinat_pelanggan<>'')
      {
        $messaggio .= "Kordinat Pelanggan : \n";
        $messaggio .= $exists->kordinat_pelanggan."\n";
      }
      else {
        #$messaggio .= "Tidak ada info kordinat pelanggan\n";
        #$messaggio .= "\n";
      }
      if ($exists->nama_odp<>'')
      {
        $messaggio .= "Nama ODP : \n";
        $messaggio .= $exists->nama_odp."\n";
        $messaggio .= " Redaman ODP : \n";
        $messaggio .= $exists->redaman_odp. "\n";
      }
      else {
        #$messaggio .= "Tidak ada info nama ODP pelanggan\n";
        #$messaggio .= "\n";
      }
      if ($exists->kordinat_odp<>'')
      {
        $messaggio .= "Kordinat ODP : \n";
        $messaggio .= $exists->kordinat_odp."\n";
      }
      else {
        #$messaggio .= "Tidak ada info kordinat ODP pelanggan\n";
        #$messaggio .= "\n";
      }
      
      $messaggio .= "#report".strtolower($new_nama_tim)."\n";
      $messaggio .= "\n";
      $messaggio .= $materials;
  
      $photoInputs = [
        'Lokasi_Pelanggan', 'Redaman_Ont', 'Berita_Acara', 'ODP_Baru', 'ODP_Lama', 'Label', 'Sn_Ont_Baru', 'Sn_Ont_Lama', 'Port_Odp_Lama', 'Action_Penurunan_Dropcore', 'Ex_Dropcore','Speedtest', 'Test_Live_TV'
      ];

      $folder = "evidence";
    
      if($exists->status_laporan=="2" || $exists->status_laporan=="11"){
        Telegram::sendMessage([
          // 'chat_id' => "-1001248567976",
          'chat_id' => "-285944800",
          'parse_mode' => 'html',
          'text' => $messaggio
        ]);
      }
    }  

    $chat   = '';
    $chat2  = '';
    if ($exists->status_laporan=="11" || $exists->status_laporan=="2" || $exists->status_laporan=="24" || $exists->status_laporan=="34" || $exists->status_laporan=="3" || $exists->status_laporan=="16" || $exists->status_laporan=="12" || $exists->status_laporan=="25" || $exists->status_laporan=="26" || $exists->status_laporan=="27"){
      if ($order->area_migrasi=="INNER"){
          Telegram::sendMessage([
            'chat_id' => '64811812',
            'parse_mode' => 'html',
            'text' => $messaggio
          ]);

          for($i=0;$i<count($photoInputs);$i++) {
            $file = public_path()."/upload3/".$folder."/".$id."/".$photoInputs[$i].".jpg";
            if (file_exists($file)){
              Telegram::sendPhoto([
                'chat_id' => '64811812',
                'caption' => $photoInputs[$i]." ".$s,
                'photo' => $file
              ]);
            }
          }

          Telegram::sendMessage([
            'chat_id' => '113449636',
            'parse_mode' => 'html',
            'text' => $messaggio
          ]);

          for($i=0;$i<count($photoInputs);$i++) {
            $file = public_path()."/upload3/".$folder."/".$id."/".$photoInputs[$i].".jpg";
            if (file_exists($file)){
              Telegram::sendPhoto([
                'chat_id' => '113449636',
                'caption' => $photoInputs[$i]." ".$s,
                'photo' => $file
              ]);
            }
          }
      }
      else if($order->area_migrasi=="TJL" || $order->area_migrasi=="KDG" ){
          $chat = '412967435';
      }
      else if($order->area_migrasi=="BBR"){
          $chat  = '452128209';
          $chat2 = '314081494';
      }
      else if($order->area_migrasi=="BLC"){
          $chat = '109025591';
      };

      if ($chat<>''){
        Telegram::sendMessage([
          'chat_id' => $chat,
          'parse_mode' => 'html',
          'text' => $messaggio
        ]);
      }

      if ($chat2<>''){
        Telegram::sendMessage([
          'chat_id' => $chat2,
          'parse_mode' => 'html',
          'text' => $messaggio
        ]);
      }
    };

    Telegram::sendMessage([
      'chat_id' => $chatID,
      'parse_mode' => 'html',
      'text' => $messaggio
    ]);

    for($i=0;$i<count($photoInputs);$i++) {
      $file = public_path()."/upload3/".$folder."/".$id."/".$photoInputs[$i].".jpg";

      if (file_exists($file)){
        Telegram::sendPhoto([
          'chat_id' => $chatID,
          'caption' => $photoInputs[$i]." ".$s,
          'photo' => $file
        ]);
      }
    }
  }

  public static function plasaNotSc($tgl){
      if ($tgl=='today'){
          $tgl = date('Y-m-d');
      };

      $sql =' 
            SELECT 
              a.Ndem, c.uraian, e.laporan_status, b.sc
            FROM 
              dispatch_teknisi a
            LEFT JOIN psb_myir_wo b ON a.Ndem = b.myir
            LEFT JOIN regu c ON a.id_regu = c.id_regu
            LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
            LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
            WHERE
              b.dispatch=1 AND 
              b.ket=0 AND
              b.ket_input=0 AND 
              b.orderDate LIKE "%'.$tgl.'%"
            ';

      $getData = DB::select($sql);
      if (count($getData)<>0){
          $pesan = 'Plasa Not SC Tanggal '.$tgl."\n";
          $pesan .= '==============================='."\n";

          foreach($getData as $dt){
              if ($dt->laporan_status==''){
                  $status = 'Need Progress';
              }
              else{
                  $status = $dt->laporan_status;
              };

              $pesan .= $dt->Ndem.' | '.$dt->uraian.' | '.$status.' | - ';
          };

          Telegram::sendMessage([
            'chat_id' => '-1001309386322',
            'parse_mode' => 'html',
            'text' => $pesan
          ]); 
      }
  }
}
