@extends('layout')
@section('content')

<ul class="nav nav-tabs">
  <li class="active"><a href="/dshr/plasa-sales/plasa/form">Plasa</a></li>
  @if($level==2 || $level==45)
  	<li><a href="/dshr/plasa-sales/sales/form">Sales</a></li>
  @endif
</ul>
<br>
<form method="POST" enctype="multipart/form-data">
	<div class="panel panel-primary">
		<div class="panel-heading">Form Input Plasa</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>No KTP</label>
						<input type="text" name="orderNo" class="form-control" value="{{ old('orderNo') }}" maxlength="16">
						{!! $errors->first('orderNo','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Tgl Order</label>
						<input type="text" name="tglOrder" id="tglOrder" class="form-control" value="{{ $tgl }}" readonly>
						{!! $errors->first('tglOrder','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Nama Pemohon</label>
						<input type="text" name="nama_pemohon" class="form-control" value="{{ old('nama_pemohon') }}">
						{!! $errors->first('nama_pemohon','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Nama Tagihan</label>
						<input type="text" name="nama_tagihan" class="form-control" value="{{ old('nama_tagihan') }}">
						{!! $errors->first('nama_tagihan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="email" class="form-control" value="{{ old('email') }}">
						{!! $errors->first('email','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Pic Pelanggan</label>
						<input type="text" name="picPelanggan" class="form-control" value="{{ old('picPelanggan') }}">
						{!! $errors->first('picPelanggan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Tgl Tebit KTP</label>
						<input type="text" name="terbitKtp" class="form-control" id="terbitKtp" value="{{ old('terbitKtp') }}">
						{!! $errors->first('terbitKtp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Masa Berlaku KTP</label>
						<input type="text" name="masaKtp" class="form-control" id="masaKtp" value="{{ old('masaKtp') }}">
						{!! $errors->first('masaKtp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Ibu Kandung</label>
						<input type="text" name="ibuKandung" class="form-control" value="{{ old('ibuKandung') }}">
						{!! $errors->first('ibuKandung','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Tgl Lahir</label>
						<input type="text" name="tglLahir" id="tglLahir" class="form-control" value="{{ old('tglLahir') }}">
						{!! $errors->first('tglLahir','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>NPWP</label>
						<input type="text" name="npwp" class="form-control" value="{{ old('npwp') }}">
						{!! $errors->first('npwp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Alamat</label>
						<input type="text" name="alamat" class="form-control" value="{{ old('alamat') }}">
						{!! $errors->first('alamat','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>STO</label>
						<input type="text" name="sto" class="form-control" id="sto" value="{{ old('sto') }}">
						{!! $errors->first('sto','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Kelurahan</label>
						<input type="text" name="kelurahan" class="form-control" value="{{ old('kelurahan') }}">
						{!! $errors->first('kelurahan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Koordinat Pelanggan</label>
						<input type="text" name="koordinatPelanggan" class="form-control" value="{{ old('koordinatPelanggan') }}">
						{!! $errors->first('koordinatPelanggan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>ODP</label>
						<input type="text" name="namaOdp" class="form-control" id="odp" value="{{ old('namaOdp') }}">
						{!! $errors->first('namaOdp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Koordinat ODP</label>
						<input type="text" name="koordinatOdp" class="form-control" value="{{ old('koordinatOdp') }}">
						{!! $errors->first('koordinatOdp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Jenis Layanan</label>
						<input type="text" name="layanan" class="form-control" id="layanan" value="{{ old('layanan') }}">
						{!! $errors->first('layanan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Paket Harga</label>
						<input type="text" name="paketHarga" class="form-control" id="paketHarga" value="{{ old('paketHarga') }}">
						{!! $errors->first('paketHarga','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>No. Internet</label>
						<input type="text" name="no_internet" class="form-control" id="no_internet" value="{{ old('no_internet') }}">
						{!! $errors->first('no_internet','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Kcontack</label>
						<input type="text" name="kcontack" class="form-control" id="kcontack" value="{{ old('kcontack') }}">
						{!! $errors->first('kcontack','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>
		
				<div class="col-md-6">	
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">	
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">Dokumentasi Foto</div>
		<div class="panel-body">
			<div class="row">	
					<div class="col-xs-2 col-md-1 text-center input-photos">
						<img src="/image/placeholder.gif" alt="belum ada foto"><br>KTP<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_ktp" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_ktp','<span class="label label-danger">:message</span>') !!}
					</div>

					<div class="col-xs-2 col-md-1 text-center input-photos">
		                <img src="/image/placeholder.gif" alt="belum ada foto"><br>TTD Pelanggan<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_ttd" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_ttd','<span class="label label-danger">:message</span>') !!}

					</div>

					<div class="col-xs-2 col-md-1 text-center input-photos">
		                <img src="/image/placeholder.gif" alt="belum ada foto"><br>KTP + Pelanggan<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_ktp_pelanggan" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_ktp_pelanggan','<span class="label label-danger">:message</span>') !!}

					</div>

					<div class="col-xs-2 col-md-1 text-center input-photos">
		                <img src="/image/placeholder.gif" alt="belum ada foto"><br>Capture ODP<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_capture_odp" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_capture_odp','<span class="label label-danger">:message</span>') !!}

					</div>

				</div>
			</div>
		</div>	
	</div>
</form>
@endsection

@section('plugins')
	<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  	<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  	<script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
	<script>
		$(function(){
			var paket = <?=json_encode($paketHarga); ?>;
			$('#paketHarga').select2({
				data : paket,
				placeholder: 'Pilih Paket Harga'
			})

			var sto = <?=json_encode($getSto); ?>;
			$('#sto').select2({
				data: sto,
				placeholder: 'Pilih Sto'
			});

			$('input[type=file]').change(function() {
		        console.log(this.name);
		        var inputEl = this;
		        if (inputEl.files && inputEl.files[0]) {
		         	$(inputEl).parent().find('input[type=text]').val(1);
		          	var reader = new FileReader();
		          	reader.onload = function(e) {
		            	$(inputEl).parent().find('img').attr('src', e.target.result);
		          	}
		        	reader.readAsDataURL(inputEl.files[0]);
			    }
		    });

		    $('.input-photos').on('click', 'button', function() {
		    	$(this).parent().find('input[type=file]').click();
		    });


		    var day = {
		      	  	format: 'yyyy-mm-dd',
		        	viewMode: 0,
		       	 	minViewMode: 0
		      	};
		    
		    $('#tglLahir').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });

		   	$('#terbitKtp').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });

		   	$('#masaKtp').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });

      		$("#odp").inputmask("AAA-AAA-A{2,3}/999");

      		var datajenislayanan = [
		        {"id":"CHANGESTB2NDSTB", "text":"<span class='label label-success'>CHANGESTB2ND</span> CHANGE STB + 2ND STB"},
		        {"id":"CHANGESTBWIFIEXTENDER", "text":"<span class='label label-success'>CHANGESTBEXTENDER</span> CHANGE STB + WIFI EXTENDER"},
		        {"id":"ADDSTBPLC", "text":"<span class='label label-success'>ADDSTBPLC</span> ADD STB + PLC"},
		        {"id":"ADDSTBWIFIEXTENDER", "text":"<span class='label label-success'>ADDSTBWIFIEXTENDER</span> ADD STB + WIFI EXTENDER"},
		        {"id":"2NDSTBPLC", "text":"<span class='label label-success'>2ND</span> 2ND STB + PLC"},
		        {"id":"2NDSTBWIFIEXTENDER", "text":"<span class='label label-success'>2ND</span> 2ND STB + WIFI EXTENDER"},
		        {"id":"3RDSTBPLC", "text":"<span class='label label-success'>3RD</span> 3RD STB + PLC"},
		        {"id":"3RDSTBWIFIEXTENDER", "text":"<span class='label label-success'>3RD</span> 3RD STB + WIFI EXTENDER"},
		        {"id":"1PVOICE", "text":"<span class='label label-success'>1P</span> VOICE"},
		        {"id":"1PINET", "text":"<span class='label label-success'>1P</span> INET"},
		        {"id":"1PTV", "text":"<span class='label label-success'>1P</span> TV"},
		        {"id":"2PINETVOICE", "text":"<span class='label label-success'>2P</span> INET VOICE"},
		        {"id":"2PINETUSEETV", "text":"<span class='label label-success'>2P</span> INET USEETV"},
		        {"id":"3P", "text":"<span class='label label-success'>3P</span> VOICE INET USEETV"},
		        {"id":"ADDONSTB", "text":"<span class='label label-success'>ADD ON</span> STB"},
		        {"id":"CHANGESTB", "text":"<span class='label label-success'>CHANGE</span> STB"},
		        {"id":"2ndSTB", "text":"<span class='label label-success'>2nd</span> STB"},
		        {"id":"3ndSTB", "text":"<span class='label label-success'>3nd</span> STB"},
		        {"id":"PDA", "text":"<span class='label label-success'>PDA</span> PDA"},
		        {"id":"AddserviceInet", "text":"<span class='label label-success'>AddserviceInet</span> Addservice Inet"},
		        {"id":"AddserviceVoice", "text":"<span class='label label-success'>AddserviceVoice</span> Addservice Voice"},
		        {"id":"WifiExtender", "text":"<span class='label label-success'>WifiExtender</span> WIFI EXTENDER"},
		        {"id":"plc", "text":"<span class='label label-success'>PLC</span> PLC"},
		        {"id":"SMARTINDIHOME", "text":"<span class='label label-success'>SmartIndihome</span> SMART INDIHOME"},
		        {"id":"2NDSTBPLCWIFI", "text":"2ND STB + PLC + WIFI EXTENDER"},
		        {"id":"HOMECAMERA", "text":"HOME CAMERA"},
		        {"id":"AddServiceModify", "text":"AddService + Modify Paket"},
		      ];

		    var jenislayanan = function() {
			        return {
			          data: datajenislayanan,
			          placeholder: 'Input Jenis Layanan',
			          formatSelection: function(data) { return data.text },
			          formatResult: function(data) {
			            return  data.text;
			          }
			        }
			    }

		    $('#layanan').select2(jenislayanan());

		})
	</script>
@endsection
