      <div class="panel panel-primary">
        <div class="panel-heading">4. LAPORAN SEMUA TRANSAKSI BULAN INI PER TEAM</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>TEAM</th>
                <th>VA</th>
                <th>TC</th>
                <th>KENDALA VA</th>
                <th>KENDALA TC</th>
                <th>UNSC DEP</th>
                <th>UNSC NR2C</th>
                <th>Tanpa Status</th>
                <th>PS</th>
                <th>TOTAL</th>
                </tr>
              </thead>
              <tbody>
                {{-- */
                  $total = 0;
                  $va = 0;
                  $tc = 0;
                  $kva = 0;
                  $ktc = 0;
                  $dep = 0;
                  $nr2c = 0;
                  $ps = 0;
                  $ts = 0;
                /* --}}
                @foreach($empat as $no => $te)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $te->k }}</td>
                  <td>{{ $te->VA3 }}</td>
                  <td>{{ $te->TC3 }}</td>
                  <td>{{ $te->kendala_va3 }}</td>
                  <td>{{ $te->kendala_tc3 }}</td>
                  <td>{{ $te->unsc_dep3 }}</td>
                  <td>{{ $te->unsc_nr2c3 }}</td>
                  <td>{{ $te->ts }}</td>
                  <td>{{ $te->ps3 }}</td>
                  <td class="strong">{{ $te->total }}</td>
                  </tr>
                  {{-- */
                    $total = $total+$te->total;
                    $va = $va+$te->VA3;
                    $tc = $tc+$te->TC3;
                    $kva = $kva+$te->kendala_va3;
                    $ktc = $ktc+$te->kendala_tc3;
                    $dep = $dep+$te->unsc_dep3;
                    $nr2c = $nr2c+$te->unsc_nr2c3;
                    $ts = $ts+$te->ts;
                    $ps = $ps+$te->ps3;
                  /* --}}
                @endforeach
                <tr class="strong"><td colspan="2">TOTAL</td><td>{{ $va }}</td><td>{{ $tc }}</td><td>{{ $kva }}</td><td>{{ $ktc }}</td><td>{{ $dep }}</td><td>{{ $nr2c }}</td><td>{{ $ts }}</td><td>{{ $ps }}</td><td>{{ $total }}</td></tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>