@extends('public_layout')

@section('content')
  @include('partial.alerts')
<table class="table">
  <tr>
    <th>No</th>
    <th>SEKTOR</th>
    <th>TROUBLE_NO</th>
    <th>TROUBLE_NUMBER</th>
    <th>T_STATUS</th>
    <th>T_ACTION</th>
    <th>T_SEBAB</th>
    <th>T_JENIS</th>
    <th>N_JENIS</th>
    <th>T_NUM</th>
    <th>T_SYMP</th>
  </tr>
  <?php
    $color1 = "#FFF";
    $color2 = "#dfe6e9";
    $trouble_num = "";
    $numx = 0;
    $symp = "";
  ?>
  @foreach ($data as $num => $result)
  <?php
    if ($trouble_num<>$result->TROUBLE_NUMBER){
      $trouble_color = $color1;
      $trouble_num = $result->TROUBLE_NUMBER;
      $symp = $result->JENIS ? : "NA";
      $symp .="»";
      $numx++;
    } else {
      $symp .= $result->JENIS ? : "NA";
      $symp .="»";
      $trouble_color = $color2;
    }
  ?>
  <tr>
    <td>{{ ++$num }}</td>
    <td>{{ $result->title ? : 'NOT ASSIGN' }}</td>
    <td>{{ $result->TROUBLE_NO }}</td>
    <td>{{ $result->TROUBLE_NUMBER }}</td>
    <td>{{ $result->laporan_status ? : 'NOT ASSIGN' }}</td>
    <td>{{ $result->actionText ? : 'NOT ASSIGN' }}</td>
    <td>{{ $result->penyebab ? : 'NOT ASSIGN' }}</td>
    <td>{{ $result->JENIS ? : 'NOT ASSIGN' }}</td>
    <td>{{ $result->JENIS_GGN ? : 'NOT ASSIGN' }}</td>
    <td>{{ $numx }}</td>
    <td>{{ $symp }}</td>
  </tr>
  @endforeach

</table>
@endsection
