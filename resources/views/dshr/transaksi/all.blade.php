<div class="table-responsive">
        <table class="table table-striped table-bordered ">
          <thead>
            <tr>
              <th rowspan="2" width="5%">#</th>
              <th rowspan="2" width="10%">Action</th>
              <th rowspan="2" width="50%">Nama</th>
              <th colspan="2">Kordinat</th>
              <th rowspan="2">Nama Sales</th>
              <th colspan="2">Time</th>
              <th colspan="2">Transaksi</th>
              <th colspan="2">Migrasi</th>
            </tr>
            <tr>
              <th width="10%">Pelanggan</th>
              <th width="10%">ODP</th>
              <th>Created</th>
              <th width="10%">Deal</th>
              <th width="10%">Transaksi</th>
              <th width="10%">Layanan</th>
              <th width="10%">INET</th>
              <th width="10%">ND</th>
            </tr>
          </thead>
          <tbody>
            @foreach($list as $no => $data)
                <tr>
                <td>{{ ++$no }}</td>
                <td>
					        <a href="/dshr-transaksi-telegram/{{ $data->id }}" class="label label-info">Send To Telegram</a>
                <br />
                  <a  href="/dshr-transaksi/{{ $data->id }}" class="label label-info">{{ $data->status_hdesk ? : 'EDIT' }}</a>
                </td>
                <td>
                  <b>{{ strtoupper($data->nama_pelanggan) }}</b> <br />{{ $data->flagging ? : 'REGULER' }}
                  <br />
                  SC.{{ $data->orderId }} // {{ $data->orderStatus ? : 'NOT SYNC YET' }}<br />

                  {{ $data->alamat }}  <br />
                  PIC : {{ $data->pic }} <br />
                  KCONTACT : {{ $data->kcontact }} <br />
                </td>
                <td>
                  {{ $data->koor_pel }}
                </td>
                <td>
                  {{ $data->koor_odp }}
                </td>
                <td>
                  {{ $data->sales }}
                </td>
                <td>{{ $data->created_at }}</td>
                <td>
                  {{ $data->tanggal_deal }}
                </td>
                <td>
                  {{ $data->tipe_transaksi }}
                </td>
                <td>
                  {{ $data->jenis_layanan }}
                </td>


                <td>
                  {{ $data->no_internet }}
                </td>
                <td>
                  {{ $data->no_telp }}</td>

              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
