@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      text-align: center;
      vertical-align: middle;
    }
    .red {
      background-color: #FF0000;
      color : #FFF;
    }
    .label-greenx{
      background-color: #1abc9c;
    }
    .myindihome {
      border : 1px solid #000;
    }
    .label-darkgrey {
      background-color: #6b6b6b;

    }
    .label-bluepas {
      background-color : #fd79a8;
    }
    .label-purple {
      background-color : #800080;
    }
    .label-black {
      background-color: #000000;
    }

    .label-coklat {
      background-color: #A0522D;
    }
    .label-golden{
       background-color: #008B8B;
    }

    a {
      cursor: pointer;
    }
    .geser_kiri_dikit {
      position: relative;
      left : -12px;
    }
  </style>

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>

  <script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>


  @if (count($get_area)>0)
  <div class="panel panel-primary" id="matrix">
    <div class="panel-heading">MATRIX </div>
    <div class="panel-body">
      <div class="list-group">
      <div class="table-responsive">
        
        <table class="table table-striped table-bordered">
          <tr>
            <th width="200">Nama Tim</th>
            <th width="50">NO UPDATE</th>
            <th width="50">BERANGKAT</th>
            <th width="50">TIBA</th>
            <th width="50">OGP</th>
            <th width="50">PROSES SETTING</th>                        
          </tr>

          @foreach($data as $dt)

              <tr>
                  <td>{{ $dt['uraian'] }}</td>

                  <?php
                      $jmlWO = count($dt['listWO']);
                  ?>

                  <td>
                    @for ($a=0; $a<=$jmlWO-1; $a++)
                        @if ($dt['listWO'][$a]->status_laporan==6 || $dt['listWO'][$a]->status_laporan==NULL )
                          <?php
                              date_default_timezone_set('Asia/Makassar');
                              if ($dt['listWO'][$a]->modified_at=='' || $dt['listWO'][$a]->modified_at==NULL){
                                $jamWo = $dt['listWO'][$a]->updated_at;
                              }
                              else{
                                $jamWo = $dt['listWO'][$a]->modified_at;
                              };

                              if ($jamWo<>''){
                                  $awal  = date_create($jamWo);
                                  $akhir = date_create(); // waktu sekarang
                                  $diff  = date_diff( $awal, $akhir );
                                                      
                                  if($diff->h <> 0){
                                      $jam = $diff->h;
                                      $menit = $diff->i;
                                  }
                                  else {
                                      $jam = 0;
                                      $menit = $diff->i;
                                  }
                              }
                           ?>

                            @if ($jam==0 && $menit > 30 )
                              <label class="label label-default">{{ $dt['listWO'][$a]->Ndem }}</label>
                            @endif
                        @endif
                    @endfor
                  </td>

                  <td>
                    @for ($a=0; $a<=$jmlWO-1; $a++)
                        @if ($dt['listWO'][$a]->status_laporan==28)
                          <?php
                              date_default_timezone_set('Asia/Makassar');
                              if ($dt['listWO'][$a]->modified_at=='' || $dt['listWO'][$a]->modified_at==NULL){
                                $jamWo = $dt['listWO'][$a]->updated_at;
                              }
                              else{
                                $jamWo = $dt['listWO'][$a]->modified_at;
                              };

                              if ($jamWo<>''){
                                  $awal  = date_create($jamWo);
                                  $akhir = date_create(); // waktu sekarang
                                  $diff  = date_diff( $awal, $akhir );
                                                      
                                  if($diff->h <> 0){
                                      $jam = $diff->h;
                                      $menit = $diff->i;
                                  }
                                  else {
                                      $jam = 0;
                                      $menit = $diff->i;
                                  }
                              }
                           ?>

                            @if ($jam==0 && $menit > 30 )
                              <label class="label label-bluepas">{{ $dt['listWO'][$a]->Ndem }}</label>
                            @endif
                        @endif
                    @endfor
                  </td>

                  <td>
                    @for ($a=0; $a<=$jmlWO-1; $a++)
                        @if ($dt['listWO'][$a]->status_laporan==29)
                           <?php
                              date_default_timezone_set('Asia/Makassar');
                              if ($dt['listWO'][$a]->modified_at=='' || $dt['listWO'][$a]->modified_at==NULL){
                                $jamWo = $dt['listWO'][$a]->updated_at;
                              }
                              else{
                                $jamWo = $dt['listWO'][$a]->modified_at;
                              };

                              if ($jamWo<>''){
                                  $awal  = date_create($jamWo);
                                  $akhir = date_create(); // waktu sekarang
                                  $diff  = date_diff( $awal, $akhir );
                                                      
                                  if($diff->h <> 0){
                                      $jam = $diff->h;
                                      $menit = $diff->i;
                                  }
                                  else {
                                      $jam = 0;
                                      $menit = $diff->i;
                                  }
                              }
                           ?>

                            @if ($jam==0 && $menit > 30 )
                              <label class="label label-bluepas">{{ $dt['listWO'][$a]->Ndem }}</label>
                            @endif
                        @endif
                    @endfor
                  </td>

                  <td>
                    @for ($a=0; $a<=$jmlWO-1; $a++)
                        @if ($dt['listWO'][$a]->status_laporan==5)
                           <?php
                              date_default_timezone_set('Asia/Makassar');
                              if ($dt['listWO'][$a]->modified_at=='' || $dt['listWO'][$a]->modified_at==NULL){
                                $jamWo = $dt['listWO'][$a]->updated_at;
                              }
                              else{
                                $jamWo = $dt['listWO'][$a]->modified_at;
                              };

                              if ($jamWo<>''){
                                  $awal  = date_create($jamWo);
                                  $akhir = date_create(); // waktu sekarang
                                  $diff  = date_diff( $awal, $akhir );
                                                      
                                  if($diff->h <> 0){
                                      $jam = $diff->h;
                                      $menit = $diff->i;
                                  }
                                  else {
                                      $jam = 0;
                                      $menit = $diff->i;
                                  }
                              }
                           ?>

                            @if ($jam==0 && $menit > 30 )
                              <label class="label label-warning">{{ $dt['listWO'][$a]->Ndem }}</label>
                            @endif
                        @endif
                    @endfor
                  </td>

                  <td>
                    @for ($a=0; $a<=$jmlWO-1; $a++)
                        @if ($dt['listWO'][$a]->status_laporan==31)
                           <?php
                              date_default_timezone_set('Asia/Makassar');
                              if ($dt['listWO'][$a]->modified_at=='' || $dt['listWO'][$a]->modified_at==NULL){
                                $jamWo = $dt['listWO'][$a]->updated_at;
                              }
                              else{
                                $jamWo = $dt['listWO'][$a]->modified_at;
                              };

                              if ($jamWo<>''){
                                  $awal  = date_create($jamWo);
                                  $akhir = date_create(); // waktu sekarang
                                  $diff  = date_diff( $awal, $akhir );
                                                      
                                  if($diff->h <> 0){
                                      $jam = $diff->h;
                                      $menit = $diff->i;
                                  }
                                  else {
                                      $jam = 0;
                                      $menit = $diff->i;
                                  }
                              }
                           ?>

                            @if ($jam==0 && $menit > 30 )
                              <label class="label label-purple">{{ $dt['listWO'][$a]->Ndem }}</label>
                            @endif
                        @endif
                    @endfor
                  </td>

              </tr>
          @endforeach
         
         
        
        </table>

      </div>
    </div>
  </div>

</div>
@else
<div id="matrix"></div>
@endif

    <div class="panel-body">
    <script>
        $(document).ready(function(){
          $("[data-toggle='popover']").popover({html:true});
          var day = {
            format : 'yyyy-mm-dd',
            viewMode: 0,
            minViewMode: 0
          };

          $('#input-tgl').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
          });

        })


        var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
        var c=0;
        var minutes= 0;
        var t;
        var timer_is_on=0;

        function timedCount(element)
        {
        document.getElementById(element).innerHTML = minutes+' min '+c+' sec';
        c=c+1;
        if (c%60==0){
          minutes+=1;
          c=0;
        }
        t=setTimeout("timedCount()",1000);
        }

        function showElement(element){
          document.getElementById(element).innerHTML = "Detected";
        }


    </script>

            </div>
          </div>
  </div>
@endsection
