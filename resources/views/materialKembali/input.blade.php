@extends('layout')
@section('content')
  @include('partial.alerts')
  
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id }}" />
    @endif
    <h3>
      <a href="/material-kembali" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Input Pengembalian Material
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-gudang_id">Gudang</label>
      <input name="gudang" type="hidden" id="input-gudang_id" class="form-control" value="<?= $data->id_gudang ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-teknisi">Teknisi</label>
      <input name="karyawan" type="hidden" id="input-teknisi" class="form-control" value="<?= $data->id_karyawan ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-status">Status Material</label>
      <input name="status" type="hidden" id="input-status" class="form-control" value="<?= $data->status_material ?>" />
    </div>
    <input type="hidden" name="materials" value="[]" />
    <h3 style="margin-top:40px">
      Material
      <button data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info" type="button">
        <span class="glyphicon glyphicon-list"></span>
        Edit
      </button>
    </h3>

    <ul id="material-list" class="list-group">
      <li class="list-group-item" v-repeat="$data | hasQty ">
        <span class="badge" v-text="qty"></span>
        <strong v-text="id_item"></strong>
        <p v-text="nama_item"></p>
      </li>
    </ul>

    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>
  @if (isset($data->id))
  <form id="delete-form" method="post" autocomplete="off">
    <input name="_method" type="hidden" value="DELETE">
      <div style="margin:40px 0 20px">
        <button class="btn btn-danger">Hapus</button>
      </div>
  </form>
  @endif
  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Material Bon</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <div class="form-group">
            <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
          </div>
          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">
              <strong v-text="id_item"></strong>
              <p v-text="nama_item"></p>
              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <!-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit" disabled></button> -->
                <input v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <script>
    $(function() {
      var materials = <?= json_encode($materials) ?>;

      Vue.filter('hasQty', function(value) {
        return value.filter(function(a) { return a.qty > 0 });
      });

      Vue.filter('doubleDigit', function(value) {
        var v = Number(value);
        if (v < 1) return '00';
        else if (String(v).length < 2) return '0' + v;
      });

      var listVm = new Vue({
        el: '#material-list',
        data: materials
      });

      var modalVm = new Vue({
        el: '#material-modal',
        data: materials,
        methods: {
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });

      $('#submit-form').submit(function() {
        var result = [];
        materials.forEach(function(item) {
          if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty});
        });
        $('input[name=materials]').val(JSON.stringify(result));
      });

      $('.modal-body').css({ maxHeight: window.innerHeight - 170 });
      
      $('.btn-danger').click(function() {
        var sure = confirm('Yakin hapus data ?');
        if (sure) {
          $('#delete-form').submit();
        }
      })
      var data = <?= json_encode($gudangs) ?>;
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Gudang',
          allowClear: true,
          minimumInputLength: 1,
          escapeMarkup: function(m) { return m },
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-gudang_id').select2(select2Options());
      var data1 = <?= json_encode($karyawans) ?>;
      var select2Karyawan = function() {
        return {
          data: data1,
          placeholder: 'Input Karyawan',
          allowClear: true,
          minimumInputLength: 1,
          escapeMarkup: function(m) { return m },
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-teknisi').select2(select2Karyawan());
      var data1 = [{"id":"0","text":"Useable"}, {"id":"1","text":"Un-useable"}, {"id":"2","text":"Missing In-Action"}];
      var select2Status = function() {
        return {
          data: data1,
          placeholder: 'Input Status Material'
        }
      }
      $('#input-status').select2(select2Status());
     
    })
  </script>
@endsection
