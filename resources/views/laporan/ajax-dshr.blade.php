<div class="panel panel-primary">
    <div class="panel-heading">Laporan</div>
    <div class="panel-body">
      <div class="list-group fiber">
			 <div class="table-responsive">  
			  <table class="table" border="0">
			    <thead>
			      <tr>
			        <th width="20">No</th>
			        <th width="100">Nik</th>
			        <th width="100">Nama</th>
			        <th width="100">PS</th>
			        
			        
			      </tr>
			    </thead>
			    <tbody>
				@foreach($data as $no => $list) 
				  <tr>
			        <td>{{ ++$no }}</td>
			        <td>{{ $list->nik_sales }}</td>
					<td>{{ $list->nama }}</td>
					<td>{{ $list->ps }}</td>
					
			      </tr>
			    @endforeach
			    </tbody>
			  </table>
			 </div>
      </div>
    </div>
  </div>
