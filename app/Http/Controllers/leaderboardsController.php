<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use Telegram;
use App\DA\leaderboardsModel;

class leaderboardsController extends Controller
{
  public function index(){
    date_default_timezone_set('Asia/Makassar');
    $this->leaderboards('ALPHA.PROV',date('Y-m-d'));
  }

  public function leaderboards($squad,$date,$dateend,$mitra){
  	date_default_timezone_set("Asia/Makassar");

  	$data = leaderboardsModel::getData($squad);
  	// dd($data);
  	if (count($data)==0){
  		$listSquad = leaderboardsModel::get_squads($squad,$date,$dateend,$mitra);
    	foreach($listSquad as $squads){
    		leaderboardsModel::simpanData($squads, $squad);
    	};

  		$tampilSquad = leaderboardsModel::getAllData($squad);
  		return view('leaderboards/list',compact('tampilSquad','date','squad','dateend','mitra'));
  	}
  	else{
	  	$awal  		= date_create($data->tgl_catat);
	  	$akhir  	= date_create();
	  	$selisih 	= date_diff($awal, $akhir);
  		
  		$tampilSquad = leaderboardsModel::getAllData($squad);
	  	if ($selisih->i > 30 ){
	  		leaderboardsModel::hapusData($squad);
	    	$listSquad = leaderboardsModel::get_squads($squad,$date,$dateend,$mitra);
	    	foreach($listSquad as $squads){
	    		leaderboardsModel::simpanData($squads, $squad);
	    	};

		  	$tampilSquad = leaderboardsModel::getAllData($squad);
	  	};
  		return view('leaderboards/list',compact('tampilSquad','date','squad','dateend','mitra'));
  	}  	
  }
}
