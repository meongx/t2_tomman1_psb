@extends('layout')

@section('content')
  @include('partial.alerts')
  	@if (count($data)<>0)
	  <div class="panel panel-primary" id="info">
	      <div class="panel-heading">WO</div>
	      <div class="panel-body table-responsive">
	          <table class="table table-bordered table-fixed">
	              <tr>
	              	<td width="10%">Jumlah UP</td>
	              	<td width="90%">{{ $jumlahWo[0]->jumlahUp }}</td>
	              </tr>

	              <tr>
	              	<td width="10%">Jumlah Kendala</td>
	              	<td width="90%">{{ $jumlahWo[0]->jumlahKendala }}</td>
	              </tr>

	              <tr>
	              	<td width="10%">Total WO</td>
	              	<td width="90%">{{ $jumlahWo[0]->totalWo }}</td>
	              </tr>
	          </table>
	      </div>
	  </div>

	  <div class="panel panel-primary" id="info">
	      <div class="panel-heading">WO</div>
	      <div class="panel-body table-responsive">
	          <table class="table table-bordered table-fixed">
	              <tr>
	                  <th>#</th>
	                  <th>NIK</th>
	                  <th>Teknisi</th>
	                  <th>Regu</th>
	                  <th>Ndem</th>
	                  <th>Tgl</th>
	                  <th>Status</th>
	              </tr>

	              @foreach($data as $no=>$dt)
	              		<tr>
	              			<td>{{ ++$no }}</td>
	              			<td>{{ $dt->nikTeknisi }}</td>
	              			<td>{{ $dt->nama }}</td>
	              			<td>{{ $dt->uraian }}</td>
	              			<td>{{ $dt->Ndem }}</td>
	              			<td>{{ $dt->tgl }}</td>
	              			<td>{{ $dt->laporan_status }}</td>
	              		</tr>		
	              @endforeach
	             
	            
	          </table>
	      </div>
	  </div>
	@else
		<h1>{{ 'Kedada datanya' }}</h1>
	@endif
@endsection
