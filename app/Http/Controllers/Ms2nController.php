<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use cURL;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class Ms2nController extends Controller
{
  public function index()
  {
    $list = DB::select('
      SELECT distinct(Status) as s from ms2n
    ');
    return view('ms2n.list', compact('list'));
  }

  private function get_value(){
	  $list = DB::select('
	  	SELECT
	  		ms2n_value as Value
	  	FROM
	  		ms2n_sebab
	  	GROUP BY
	  ');

  }

  public function potensi($area){
	  if ($area == 'ALL') {
		  $where_area = '';
	  } else {
		  $where_area = ' AND bb.area="'.$area.'"';
	  }
	  $get_potensi = DB::select('
	  	SELECT
	  		aa.ND,
	  		aa.ND_Speedy,
	  		aa.Nama
	  	FROM
	  		ms2n aa
	  	LEFT JOIN mdf bb ON aa.mdf=bb.mdf
	  	WHERE
	  		aa.Status_Indihome IN ("MIGRASI_2P_3P_UNBUNDLED","MIGRASI_2P_3P_BUNDLED") AND
	  		aa.Status="VA" and
	  		aa.Sebab IN ("Update","Belum Ada Janji","Sudah Ada Janji")
				'.$where_area.'
	  ');
	  return view('ms2n.potensi',compact('get_potensi','area'));
  }

   public function getdata($sebab, $area){
	  $sebab = urldecode($sebab);
	  if ($area == 'ALL') {
		  $where_area = '';
	  } else {
		  $where_area = ' AND bb.area="'.$area.'"';
	  }

	  $get_potensi = DB::select('
	  	SELECT
	  		dd.id as id_dt,
				ee.uraian,
	  		cc.orderId,
	  		aa.ND,
	  		cc.sto,
	  		aa.ND_Speedy,
	  		aa.Nama,
	  		aa.Status_Indihome,
	  		aa.Ncli,
	  		cc.jenisPsb,
			aa.Ndem,
	  		aa.Ket_Sebab,
	  		cc.orderStatus,
			cc.orderDate,
			dd.updated_at,
			aa.Kcontact,
      cc.alproname,
      aa.Chanel
	  	FROM
	  		Data_Pelanggan_Starclick cc
	  	LEFT JOIN ms2n aa ON aa.Ncli =cc.orderNcli
	  	LEFT JOIN mdf bb ON aa.mdf=bb.mdf
	  	LEFT JOIN dispatch_teknisi dd ON cc.orderId = dd.Ndem or aa.Ndem = dd.Ndem
	  	LEFT JOIN regu ee ON dd.id_regu = ee.id_regu

	  	WHERE
	  		 cc.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)") AND
	  		 aa.Sebab="'.$sebab.'" AND
         aa.Status <> "PS"
				'.$where_area.'
	  ');
	  return view('ms2n.potensi',compact('get_potensi','area','sebab'));
  }

  public function getdata2($sebab, $area){
	  $sebab = urldecode($sebab);
	  $area = urldecode($area);
	  if ($area == 'MI') {
		  $where_area = 'AND aa.Kcontact LIKE "MI%"';
	  } else if ($area == 'OTHERS') {
		 $where_area = 'AND aa.Kcontact NOT LIKE "MI%" AND NOT IN  ("Digital Sales Home Reach","Avengers","Telkom_Group_Store")';
	  } else {
		  $where_area = ' AND aa.Chanel="'.$area.'"';
	  }

	  $get_potensi = DB::select('
	  	SELECT
	  		dd.id as id_dt,
			ee.uraian,
	  		cc.orderId,
	  		aa.ND,
	  		cc.sto,
	  		aa.ND_Speedy,
	  		aa.Nama,
	  		aa.Status_Indihome,
	  		aa.Ncli,
	  		cc.jenisPsb,
	  		aa.Ket_Sebab,
	  		cc.orderStatus,
			cc.orderDate,
			dd.updated_at,
			aa.Kcontact
	  	FROM
	  		Data_Pelanggan_Starclick cc
	  	LEFT JOIN ms2n aa ON aa.Ncli =cc.orderNcli
	  	LEFT JOIN mdf bb ON aa.mdf=bb.mdf
	  	LEFT JOIN dispatch_teknisi dd ON cc.orderId = dd.Ndem or aa.Ndem = dd.Ndem
	  	LEFT JOIN regu ee ON dd.id_regu = ee.id_regu

	  	WHERE
	  		 cc.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)") AND
	  		 aa.Sebab="'.$sebab.'" AND
         aa.Status <> "PS"
				'.$where_area.'
	  ');
	  return view('ms2n.potensi',compact('get_potensi','area','sebab'));
  }


   public function sudah_aktif($area){
	  if ($area == 'ALL') {
		  $where_area = '';
	  } else {
		  $where_area = ' AND bb.area="'.$area.'"';
	  }
	  $get_potensi = DB::select('
	  	SELECT
	  		aa.ND,
	  		aa.ND_Speedy,
	  		aa.Nama
	  	FROM
	  		ms2n aa
	  	LEFT JOIN mdf bb ON aa.mdf=bb.mdf
	  	WHERE
	  		aa.Sebab = "Sudah Aktif" AND
	  		aa.Status = "VA"
				'.$where_area.'
	  ');
	  return view('ms2n.potensi',compact('get_potensi','area'));
  }

  public function kategori()
  {

    $list = DB::select('
      SELECT
      	a.Sebab as s,
				b.ms2n_sebab_value as Value,
      	count( * ) AS jumlah
			FROM
				ms2n a
			RIGHT JOIN ms2n_sebab b ON a.Sebab = b.ms2n_sebab
      WHERE
        a.Status <> "PS"
			GROUP BY a.Sebab
			ORDER BY b.ms2n_sebab_value ASC
    ');

    $lastSync = $this->getLastSync('VA');
    return view('ms2n.va.kategori', compact('list', 'lastSync'));
  }

  public function filterPS($tgl_ps)
  {
	  $get_ps_harian = DB::select('
SELECT b.Sebab, c.ms2n_sebab_pic, (

SELECT COUNT( * )
FROM Data_Pelanggan_Starclick aa
LEFT JOIN ms2n cc ON aa.orderNcli = cc.Ncli
LEFT JOIN mdf bb ON aa.sto = bb.mdf
WHERE bb.area =  "BJM"
AND cc.Sebab = b.Sebab
AND cc.Status <> "PS"
AND aa.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
) AS BJM, (

SELECT COUNT( * )
FROM Data_Pelanggan_Starclick aa
LEFT JOIN ms2n cc ON aa.orderNcli = cc.Ncli
LEFT JOIN mdf bb ON aa.sto = bb.mdf
WHERE bb.area =  "BJB"
AND cc.Sebab = b.Sebab
AND cc.Status <> "PS"
AND aa.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
) AS BJB, (

SELECT COUNT( * )
FROM Data_Pelanggan_Starclick aa
LEFT JOIN ms2n cc ON aa.orderNcli = cc.Ncli
LEFT JOIN mdf bb ON aa.sto = bb.mdf
WHERE bb.area =  "BLN"
AND cc.Sebab = b.Sebab
AND cc.Status <> "PS"
AND aa.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
) AS BLN, (

SELECT COUNT( * )
FROM Data_Pelanggan_Starclick aa
LEFT JOIN ms2n cc ON aa.orderNcli = cc.Ncli
LEFT JOIN mdf bb ON aa.sto = bb.mdf
WHERE bb.area =  "TTG"
AND cc.Sebab = b.Sebab
AND cc.Status <> "PS"
AND aa.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
) AS TTG
FROM Data_Pelanggan_Starclick a
LEFT JOIN ms2n b ON a.orderNcli = b.Ncli
LEFT JOIN ms2n_sebab c ON b.Sebab = c.ms2n_Sebab
WHERE a.orderStatus
IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
AND b.Status <> "PS"
GROUP BY b.Sebab
ORDER BY c.ms2n_sebab_pic
	  ');
    return view('ms2n.ps.laporan',compact('get_ps_harian','tgl_ps','get_ps_harian_dshr'));
  }


  public function ms2nchanel()
  {
	  $get_ps_harian = DB::select('

SELECT b.Sebab, (

SELECT COUNT( * )
FROM Data_Pelanggan_Starclick aa
LEFT JOIN ms2n cc ON aa.orderNcli = cc.Ncli
LEFT JOIN mdf bb ON aa.sto = bb.mdf
WHERE cc.Chanel NOT IN  ("Digital Sales Home Reach","Avengers","Telkom_Group_Store") AND cc.Kcontact NOT LIKE  "MI%"
AND cc.Sebab = b.Sebab
AND aa.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
) AS OTHERS,
(SELECT COUNT( * )
FROM Data_Pelanggan_Starclick aa
LEFT JOIN ms2n cc ON aa.orderNcli = cc.Ncli
LEFT JOIN mdf bb ON aa.sto = bb.mdf
WHERE cc.Chanel =  "Digital Sales Home Reach"
AND cc.Sebab = b.Sebab
AND aa.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
) AS DSHR, (

SELECT COUNT( * )
FROM Data_Pelanggan_Starclick aa
LEFT JOIN ms2n cc ON aa.orderNcli = cc.Ncli
LEFT JOIN mdf bb ON aa.sto = bb.mdf
WHERE cc.Chanel =  "Avengers"
AND cc.Sebab = b.Sebab
AND aa.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
) AS AVG, (

SELECT COUNT( * )
FROM Data_Pelanggan_Starclick aa
LEFT JOIN ms2n cc ON aa.orderNcli = cc.Ncli
LEFT JOIN mdf bb ON aa.sto = bb.mdf
WHERE cc.Chanel =  "Telkom_Group_Store"
AND cc.Sebab = b.Sebab
AND aa.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
) AS PLASA, (

SELECT COUNT( * )
FROM Data_Pelanggan_Starclick aa
LEFT JOIN ms2n cc ON aa.orderNcli = cc.Ncli
LEFT JOIN mdf bb ON aa.sto = bb.mdf
WHERE cc.Kcontact LIKE  "MI%"
AND cc.Sebab = b.Sebab
AND aa.orderStatus IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
) AS MI
FROM Data_Pelanggan_Starclick a
LEFT JOIN ms2n b ON a.orderNcli = b.Ncli
WHERE a.orderStatus
IN ("Process OSS (Provision Issued)",  "Fallout (WFM)", "Fallout (Activation)","Process OSS (Activation Completed)")
AND b.Status NOT IN ("PS")
GROUP BY b.Sebab
ORDER BY b.Sebab
	  ');
    return view('ms2n.ps.laporan2',compact('get_ps_harian','tgl_ps','get_ps_harian_dshr'));
  }


  public function listKategori($id)
  {
    $list = DB::select('
      SELECT * from ms2n where Sebab = ?
      ',[
        urlencode($id)
      ]);
    return view('ms2n.va.list', compact('list', 'id'));
  }
  public function getJsonPs($id){
    $ms2ns = DB::select("
      SELECT *,t1.id as id_dt, t1.updated_at as tgl_dispatch  from psb_laporan_material m1
      right join psb_laporan m2 on m1.psb_laporan_id = m2.id
      right join dispatch_teknisi t1 on m2.id_tbl_mj = t1.id
      left join ms2n t2 on t1.Ndem = t2.Ndem
      RIGHT join mdf a on t2.mdf = a.mdf
      RIGHT join regu b on t1.id_regu = b.id_regu
      WHERE
      m1.id_item IN ('preconnectorized 75 M','preconnectorized-100','preconnectorized 50 M','preconnectorized-100','preconnectorized 100 M','preconnectorized-50','preconnectorized-75','DC-ROLL','AB-OF-SM-2D') AND
      t1.updated_at like '$id%' order by Nama, id_item

      ");
    $data = array();
    $lastNama = '';
    $jumlah_dc = 0;
    $no=0;
    $head = array();
    foreach ($ms2ns as $no => $ms2n){
      if(!empty($ms2n->id_item)){
        $head[] = $ms2n->id_item;
      }
    }
    $head = array_unique($head);
    foreach ($ms2ns as $no => $ms2n){
      if($lastNama == $ms2n->Nama){
        //if(!empty($ms2n->id_item)){
          $data[count($data)-1]['Material'][$ms2n->id_item] = $ms2n->qty;
           $check_qty = $ms2n->qty*$this->check_qty($ms2n->id_item);
	          $jumlah_dc = $jumlah_dc + $check_qty;
	          $data[count($data)-1]['DC'] = $jumlah_dc;
            $data[count($data)-1]['Material'][$ms2n->id_item] = $check_qty;
        //}
        $no++;
      }else{
        $data[] = array("kota"=>$ms2n->Kota, "distrik"=>$ms2n->Distrik,"no_jalan" => $ms2n->No_Jalan,"kcontact"=>$ms2n->Kcontact, "jalan"=>$ms2n->Jalan, "mdf" => $ms2n->mdf, "Tgl_PS" => $ms2n->Tgl_PS,"id_dt" => $ms2n->id_dt,"status_indihome" => $ms2n->Status_Indihome, "tgl_dispatch" => $ms2n->tgl_dispatch, "uraian" => $ms2n->uraian, "area" => $ms2n->area, "ND" => $ms2n->ND, "ND_Speedy" => $ms2n->ND_Speedy ,"nama" => $ms2n->Nama, "Material" => array());
        $jumlah_dc = 0;
        foreach($head as $h){
          if($h == $ms2n->id_item){
	          $check_qty = $ms2n->qty*$this->check_qty($ms2n->id_item);
	          $jumlah_dc = $jumlah_dc + $check_qty;
	          $data[count($data)-1]['DC'] = $jumlah_dc;
            $data[count($data)-1]['Material'][$h] = $check_qty;
          }else{
            $data[count($data)-1]['Material'][$h] = 0;
          }
        }
        $no=0;
      }
      $lastNama = $ms2n->Nama;
    }
    //return $data;
    return view('laporan.ajax-material',compact('data', 'head'));
  }
    public function getJsonGallery($id){
    $ms2ns = DB::select("
      SELECT *,t1.id as id_dt, t1.updated_at as tgl_dispatch  from psb_laporan_material m1
      right join psb_laporan m2 on m1.psb_laporan_id = m2.id
      right join dispatch_teknisi t1 on m2.id_tbl_mj = t1.id
      left join ms2n t2 on t1.Ndem = t2.Ndem
      RIGHT join mdf a on t2.mdf = a.mdf
      RIGHT join regu b on t1.id_regu = b.id_regu
      WHERE
      m1.id_item IN ('preconnectorized 75 M','preconnectorized-100','preconnectorized 50 M','preconnectorized-100','preconnectorized 100 M','preconnectorized-50','preconnectorized-75','DC-ROLL','AB-OF-SM-2D') AND
      t1.updated_at like '$id%' order by Nama, id_item

      ");
    $data = array();
    $lastNama = '';
    $jumlah_dc = 0;
    $no=0;
    $head = array();
    foreach ($ms2ns as $no => $ms2n){
      if(!empty($ms2n->id_item)){
        $head[] = $ms2n->id_item;
      }
    }
    $head = array_unique($head);
    foreach ($ms2ns as $no => $ms2n){
      if($lastNama == $ms2n->Nama){
        //if(!empty($ms2n->id_item)){
          $data[count($data)-1]['Material'][$ms2n->id_item] = $ms2n->qty;
           $check_qty = $ms2n->qty*$this->check_qty($ms2n->id_item);
	          $jumlah_dc = $jumlah_dc + $check_qty;
	          $data[count($data)-1]['DC'] = $jumlah_dc;
            $data[count($data)-1]['Material'][$ms2n->id_item] = $check_qty;
        //}
        $no++;
      }else{
        $data[] = array("id_dt" => $ms2n->id_dt,"status_indihome" => $ms2n->Status_Indihome, "tgl_dispatch" => $ms2n->tgl_dispatch, "uraian" => $ms2n->uraian, "area" => $ms2n->area, "ND" => $ms2n->ND, "ND_Speedy" => $ms2n->ND_Speedy ,"nama" => $ms2n->Nama, "Material" => array());
        $jumlah_dc = 0;
        foreach($head as $h){
          if($h == $ms2n->id_item){
	          $check_qty = $ms2n->qty*$this->check_qty($ms2n->id_item);
	          $jumlah_dc = $jumlah_dc + $check_qty;
	          $data[count($data)-1]['DC'] = $jumlah_dc;
            $data[count($data)-1]['Material'][$h] = $check_qty;
          }else{
            $data[count($data)-1]['Material'][$h] = 0;
          }
        }
        $no=0;
      }
      $lastNama = $ms2n->Nama;
    }
    //return $data;
    return view('laporan.ajax-gallery',compact('data', 'head'));
  }

  public function getJsonMigrasi($id){
    $ms2ns = DB::select("
      SELECT
        *,
        t2.orderName as Nama,
        t2.ndemPots as ND,
        t2.ndemSpeedy as ND_Speedy,
        t2.jenisPsb as Status_Indihome,
        t1.updated_at as tgl_dispatch
      FROM
        psb_laporan_material m1
      right join psb_laporan m2 on m1.psb_laporan_id = m2.id
      right join dispatch_teknisi t1 on m2.id_tbl_mj = t1.id
      left join Data_Pelanggan_Starclick t2 on t1.Ndem = t2.orderId
      right join mdf a on t2.sto = a.mdf
      right join regu b on t1.id_regu = b.id_regu
      WHERE
      m1.id_item IN ('preconnectorized 75 M','preconnectorized-100','preconnectorized 50 M','preconnectorized-100','preconnectorized 100 M','preconnectorized-50','preconnectorized-75','DC-ROLL','AB-OF-SM-2D') AND
      t1.updated_at like '$id%' order by t2.orderName, id_item

      ");
    $data = array();
    $lastNama = '';
    $no=0;
    $head = array();
    foreach ($ms2ns as $no => $ms2n){
      if(!empty($ms2n->id_item)){
        $head[] = $ms2n->id_item;
      }
    }
    $head = array_unique($head);
    foreach ($ms2ns as $no => $ms2n){
      if($lastNama == $ms2n->orderId){
        //if(!empty($ms2n->id_item)){
          $data[count($data)-1]['Material'][$ms2n->id_item] = $ms2n->qty;
           $check_qty = $ms2n->qty*$this->check_qty($ms2n->id_item);
	          $jumlah_dc = $jumlah_dc + $check_qty;
	          $data[count($data)-1]['DC'] = $jumlah_dc;
            $data[count($data)-1]['Material'][$ms2n->id_item] = $check_qty;
        //}
        $no++;
      }else{
        $data[] = array( "orderDatePs"=>$ms2n->orderDatePs,"orderId"=>$ms2n->orderId,"orderStatus"=>$ms2n->orderStatus,"orderAddr"=>$ms2n->orderAddr,"orderCity"=>$ms2n->orderCity,"kcontact"=>$ms2n->kcontact,"sto" => $ms2n->sto,"orderDatePs" => $ms2n->orderDatePs, "jenisPsb" => $ms2n->jenisPsb,
        "tgl_dispatch" => $ms2n->tgl_dispatch, "uraian" => $ms2n->uraian, "area" => $ms2n->area, "ndemPots" => $ms2n->ndemPots, "ndemSpeedy" => $ms2n->ndemSpeedy ,"orderName" => $ms2n->orderName, "Material" => array());
        $jumlah_dc = 0;
        foreach($head as $h){
          if($h == $ms2n->id_item){
	          $check_qty = $ms2n->qty*$this->check_qty($ms2n->id_item);
	          $jumlah_dc = $jumlah_dc + $check_qty;
	          $data[count($data)-1]['DC'] = $jumlah_dc;
            $data[count($data)-1]['Material'][$h] = $check_qty;
          }else{
            $data[count($data)-1]['Material'][$h] = 0;
          }
        }
        $no=0;
      }
      $lastNama = $ms2n->orderId;
    }
    //return $data;
    return view('laporan.ajax-material',compact('data', 'head'));
  }


  public function check_qty($item){
	  switch ($item){
		  case "preconnectorized 75 M" :
		  	$value = 75;
		  break;
		  case "preconnectorized-100" :
		  	$value = 100;
		  break;
		  case "preconnectorized 50 M" :
		  	$value = 50;
		  break;
		  case "preconnectorized-100" :
		  	$value = 100;
		  break;
		  case "preconnectorized 100 M" :
		  	$value = 100;
		  break;
		  case "preconnectorized-50" :
		  	$value = 50;
		  break;
		  case "preconnectorized-75" :
		  	$value = 100;
		  break;
			default :
		  	$value = 1;
		  break;
	  }
	  return $value;
  }



  public function publicAllSync(){
    // $this->publicSync("VA");
    // $this->publicSync("PI");
    // $this->publicSync("FO");
    $this->publicSync("PS");
    $this->publicSync("2PPS");
  }

  public function publicAllMbspSync(){
    $truncate = 'TRUNCATE prospek_mbsp';
    DB::transaction(function() use ($truncate){
      DB::statement($truncate);
    });
    $this->publicMbspSync("approve");
    $this->publicMbspSync("decline");
    $this->publicMbspSync("agree");
  }

  public function publicMbspSync($id){

    $request=$this->mydashboard_mbsp_request("http://10.60.165.45/ms2/report_mbsp_detil.php?start_date=01/11/2016&end_date=09/11/2016&p_kawasans=DIVRE_6&p_witels=ALL&p_filter1=&p_filter2=ALL&p_colom=KALSEL&p_head=".$id);

    $count=count($request);
    if ($count>0) {
      $query ="insert ignore into prospek_mbsp (
      Prospek_ID,
      ND,
      NO_INTERNET,
      Prospek,
      Status_Call,
      Status_Webcare,
      Tanggal_Call,
      Status_QA,
      Carring_ID,
      Nama,
      Alamat_Instalasi,
      Status_Inbox,
      SC_ID,
      Jenis_Psb,
      Tanggal_Create,
      Status_Order,
      Nama_Paket,
      Last_Update,
      Kawasan,
      Witel) values ";

      for ($i = 0; $i < $count; $i++) {
        $sparator = ", ";
        if($i==0){
          $sparator = "";
        }
        $query .= $sparator."(
        '".$request["$i"]["Prospek_ID"]."',
        '".$request["$i"]["ND"]."',
        '".$request["$i"]["NO_INTERNET"]."',
        '".$request["$i"]["Prospek"]."',
        '".$request["$i"]["Status_Call"]."',
        '".$request["$i"]["Status_Webcare"]."',
        '".$request["$i"]["Tanggal_Call"]."',
        '".$request["$i"]["Status_QA"]."',
        '".$request["$i"]["Carring_ID"]."',
        ".DB::connection()->getPdo()->quote($request["$i"]["Nama"]).",
        ".DB::connection()->getPdo()->quote($request["$i"]["Alamat_Instalasi"]).",
        '".$request["$i"]["Status_Inbox"]."',
        '".$request["$i"]["SC_ID"]."',
        '".$request["$i"]["Jenis_Psb"]."',
        '".$request["$i"]["Tanggal_Create"]."',
        '".$request["$i"]["Status_Order"]."',
        ".DB::connection()->getPdo()->quote($request["$i"]["Nama_Paket"]).",
        '".$request["$i"]["Last_Update"]."',
        '".$request["$i"]["Kawasan"]."',
        '".$request["$i"]["Witel"]."'
        )";
      }

      echo $query;

      DB::transaction(function() use($query, $id) {
        DB::statement($query);
      });
    }
  }

  public function refix_ndemSpeedy(){
    $check_ndemSpeedy = DB::SELECT('
      SELECT
        a.Ndem,
        a.Kcontact
      FROM
        ms2n a
      WHERE
        a.Tgl_PS LIKE "'.date('Y-m').'%"
    ');
    foreach ($check_ndemSpeedy as $data){
      $check = DB::SELECT('
        SELECT
          a.orderId
        FROM
          Data_Pelanggan_Starclick a
        WHERE
          a.kcontact = "'.$data->Kcontact.'"
      ');
      if (count($check)>0){
        $update = DB::table('ms2n')
                      ->where('Ndem',$data->Ndem)
                      ->update(['orderId'=>$check[0]->orderId]);
                      echo "done<br />";
      }
    }
  }

  public function migrasiSync($id){
    date_default_timezone_set('Asia/Makassar');
    $getLastSync = DB::table('ms2n_migrasi_sync')
                        ->where('status', $id)
                        ->orderBy('updated_at', 'desc')->first();
    $link = $this->mydashboard_migrasi_request("http://10.60.165.45/ms2/report_migrasi_ftth_detil.php?start_date=".date('01/m/Y')."&end_date=".date('d/m/Y')."&p_kawasans=DIVRE_6&p_witel=ALL&p_migrasi=ALL&p_alpro=ALL&p_tito=ALL&p_colom=KALSEL&p_etat=".$id."&p_sql=1");

    $row = count($link);
    echo "get ".$row." row from ms2n.<br />";
    if ($row>0) {
      $query = '
        INSERT IGNORE INTO ms2n_migrasi(
          orderId,
          transaksi_sc,
          tipe_migrasi,
          status,
          `order`,
          nama,
          alamat_instalasi,
          no_telp,
          no_hp,
          email,
          sto,
          alprolama,
          alprobaru,
          tito,
          keterangan,
          tgl_order,
          tgl_pi,
          last_update,
          umur,
          ncli,
          pots,
          speedy,
          datel,
          witel,
          reg
          )
          values
      ';
      for ($i = 0; $i < $row; $i++) {
        $sparator = ", ";
        if($i==0){
          $sparator = "";
        }
        $query .= $sparator.'(
          "'.$link[$i]['orderId'].'",
          "'.$link[$i]['transaksi_sc'].'",
          "'.$link[$i]['tipe_migrasi'].'",
          "'.$link[$i]['status'].'",
          "'.$link[$i]['order'].'",
          '.DB::connection()->getPdo()->quote($link[$i]['nama']).',
          '.DB::connection()->getPdo()->quote($link[$i]['alamat_instalasi']).',
          "'.$link[$i]['no_telp'].'",
          "'.$link[$i]['no_hp'].'",
          "'.$link[$i]['email'].'",
          "'.$link[$i]['sto'].'",
          "'.$link[$i]['alprolama'].'",
          "'.$link[$i]['alprobaru'].'",
          "'.$link[$i]['tito'].'",
          '.DB::connection()->getPdo()->quote($link[$i]['keterangan']).',
          "'.date('Y-m-d', strtotime($link[$i]['tgl_order'])).'",
          "'.date('Y-m-d', strtotime($link[$i]['tgl_pi'])).'",
          "'.date('Y-m-d', strtotime($link[$i]['last_update'])).'",
          "'.$link[$i]['umur'].'",
          "'.$link[$i]['ncli'].'",
          "'.$link[$i]['pots'].'",
          "'.$link[$i]['speedy'].'",
          "'.$link[$i]['datel'].'",
          "'.$link[$i]['witel'].'",
          "'.$link[$i]['reg'].'"
          )';
      }
      DB::table('ms2n_sync')->insert(
        ['status' => $id,
        'updated_by' => '92140917',
        'updated_at' => DB::raw('NOW()')]
      );
      DB::statement($query);
    }
  }

  public function publicSync($id){
  date_default_timezone_set('Asia/Makassar');
    // if($id=='VA'){
    //   $jenis = "3P";
    //   $va=$this->mydashboard_request("http://10.60.165.45/ms2/detil_progres_useetv2_.php?sub_chanel=%&chanel=%&p_kawasan=DIVRE_6&witel=KALSEL&indihome=&kode=1&c_witel=44&p_cseg=%&p_etat=4&start_date=01/05/2016&end_date=".date('d/m/Y')."&indihome=&migrasi=&starclick=&plblcl=&inden=&status_order=VA");
    // }else if($id=='PI'){
    //   $jenis = "3P";
    //   $va=$this->mydashboard_request("http://10.60.165.45/ms2/detil_progres_useetv2_.php?sub_chanel=%&chanel=%&p_kawasan=DIVRE_6&witel=KALSEL&indihome=&kode=1&c_witel=44&p_cseg=%&p_etat=4&start_date=01/05/2016&end_date=".date('d/m/Y')."&indihome=&migrasi=&starclick=&plblcl=&inden=&status_order=PI");
    // }else if($id=='FO'){
    //   $jenis = "3P";
    //   $va=$this->mydashboard_request("http://10.60.165.45/ms2/detil_progres_useetv2_.php?sub_chanel=%&chanel=%&p_kawasan=DIVRE_6&witel=KALSEL&indihome=&kode=1&c_witel=44&p_cseg=%&p_etat=4&start_date=01/05/2016&end_date=".date('d/m/Y')."&indihome=&migrasi=&starclick=&plblcl=&inden=&status_order=FO");
    // }else
    $getLastSync = DB::table('ms2n_sync')
                        ->where('status','PS')
                        ->orderBy('updated_at', 'desc')->first();

    $startDate = date('d/m/Y', strtotime($getLastSync->updated_at));
    if($id=='2PPS'){
      $jenis = "2P";

      $va=$this->mydashboard2P_request("http://10.60.165.45/ms2/report_internet2_detail.php?sub_chanel=%&chanel=%&p_kawasan=DIVRE_6&indihome=&kode=1&c_witel=44&p_cseg=%&p_etat=5&start_date=".$startDate."&end_date=".date('d/m/Y')."&indihome=&migrasi=&starclick=&chanel=&plblcl=&inden=&status_order=PS");
    }else {
      //ps
      $jenis = "3P";
      echo $jenis;
      echo date('Y-m-d')."<br />";
      // $va=$this->mydashboard_request("http://10.60.165.45/ms2/detil_progres_useetv2_.php?sub_chanel=%&chanel=%&p_kawasan=DIVRE_6&witel=KALSEL&indihome=&kode=1&c_witel=44&p_cseg=%&p_etat=5&start_date=".$startDate."&end_date=".date('d/m/Y')."&indihome=&migrasi=&starclick=&plblcl=&inden=&status_order=PS");
      $va=$this->mydashboard_request("http://10.60.165.45/ms2/detil_progres_useetv2_.php?sub_chanel=%&chanel=%&p_kawasan=DIVRE_6&witel=KALSEL&indihome=&kode=1&c_witel=44&p_cseg=%&p_etat=5&start_date=".$startDate."&end_date=".date('d/m/Y')."&indihome=&migrasi=&starclick=&plblcl=&inden=&status_order=PS");
    }

    $tes="insert ignore into ms2n(
      Wilayah,
      Kandatel,
      Ndem,
      Ncli,
      Status,
      ND,
      ND_Speedy,
      Citem,
      Kecepatan,
      MDF,
      Deskripsi,
      Chanel,
      Status_Indihome,
      Tgl_Reg,
      Tgl_PS,Umur,
      Nama,
      Kcontact,
      Jalan,
      No_Jalan,
      Distrik,
      Kota,
      Status_Indent_Awal,
      Status_Indent_Akhir,
      Sebab,
      Ket_Sebab,
      Solusi,
      Ket_Solusi,
      Due_Date_Solusi,
      Status_WO,
      nik_sales,
      kode_sc,
      sc,
      JENIS
    ) values ";
    $query = "";
    $count=count($va);
    // print_r($count);
    if ($count>0) {



      for ($i = 0; $i < $count; $i++) {
        $sparator = ", ";
        if($i==0){
          $sparator = "";
        }
        $nik = trim(str_replace(":",";", $va["$i"]["Kcontact"]), " ");
        $nik = explode(";", $nik);
        $salesNik = '';
        $kode_sc = '';
  	    $sc = '';
        foreach ($nik as $n){
          if(strlen($n) == 8 && is_numeric($n)){
            $salesNik = $n;
          }
          if(substr($n,0,2) == 'SC'){

            $kode_sc = $n;
          }
        }

        if ($id = "2PPS"){
          $citem = "1";
        } else {
          $citem = $va["$i"]["Citem"];
        }


        $row = $sparator."(
        '".$va["$i"]["Wilayah"]."',
        '".$va["$i"]["Kandatel"]."',
        '".$va["$i"]["Ndem"]."',
        '".$va["$i"]["Ncli"]."',
        '".$va["$i"]["Status"]."',
        '".$va["$i"]["ND"]."',
        '".$va["$i"]["ND_Speedy"]."',
        '".$citem."',
        '".$va["$i"]["Kecepatan"]."',
        '".$va["$i"]["MDF"]."',
        ".DB::connection()->getPdo()->quote($va["$i"]["DESKRIPSI"]).",
        ".DB::connection()->getPdo()->quote($va["$i"]["Chanel"]).",
        ".DB::connection()->getPdo()->quote($va["$i"]["Status_Indihome"]).",
        '".date('Y-m-d', strtotime($va["$i"]["Tgl_Reg"]))."',
        '".date('Y-m-d', strtotime($va["$i"]["Tgl_PS"]))."', '".$va["$i"]["Umur"]."',
        ".DB::connection()->getPdo()->quote($va["$i"]["Nama"]).",
        ".DB::connection()->getPdo()->quote($va["$i"]["Kcontact"]).",
        ".DB::connection()->getPdo()->quote($va["$i"]["Jalan"]).",
        ".DB::connection()->getPdo()->quote($va["$i"]["No_Jalan"]).",
        ".DB::connection()->getPdo()->quote($va["$i"]["Distrik"]).",
        ".DB::connection()->getPdo()->quote($va["$i"]["Kota"]).",
        '".$va["$i"]["Status_Indent_Awal"]."',
        '".$va["$i"]["Status_Indent_Akhir"]."',
        ".DB::connection()->getPdo()->quote($va["$i"]["Sebab"]).",
        ".DB::connection()->getPdo()->quote($va["$i"]["Ket_Sebab"]).",
        ".DB::connection()->getPdo()->quote($va["$i"]["Solusi"]).",
        ".DB::connection()->getPdo()->quote($va["$i"]["Ket_Solusi"]).",
        '".$va["$i"]["Due_Date_Solusi"]."',
        '1',
        '".$salesNik."',
        '".$kode_sc."',
        '".$sc."',
        '".$jenis."')";
        $tes .= $row;
      }
          echo $tes;
    } else {
      echo "NOTHING TO SYNC";
    }

    // echo $count;

    if ($count>0) {
      DB::transaction(function() use($tes, $id) {
        if($id=='VA'){
          DB::table('ms2n_control')
            ->where('id', '1')
            ->delete();
          DB::table('ms2n_control')->insert(
            ['id' => 1, 'c_status' => 'VA']
          );
        }
  	  	        if($id=='PI'){
          DB::table('ms2n_control')
            ->where('id', '5')
            ->delete();
          DB::table('ms2n_control')->insert(
            ['id' => 5, 'c_status' => 'PI']
          );
        }
  	        if($id=='FO'){
          DB::table('ms2n_control')
            ->where('id', '4')
            ->delete();
          DB::table('ms2n_control')->insert(
            ['id' => 4, 'c_status' => 'FO']
          );
        }
        DB::table('ms2n_sync')->insert(
          ['status' => $id,
          'updated_by' => '92140917',
          'updated_at' => DB::raw('NOW()')]
        );
        DB::statement($tes);
      });
    }
  }

  private function getLastSync($status){
    $last = DB::table('ms2n_sync')
                ->where('status', $status)
                ->orderBy('updated_at', 'desc')->first();
    return $last->updated_at;
  }

  private function mydashboard_migrasi_request($url, $columns = array(), $table_index = 1)
  {
    if (!count($columns)) {
        $columns = array(
            1 => 'orderId',
            'transaksi_sc',
            'tipe_migrasi',
            'status',
            'order',
            'nama',
            'alamat_instalasi',
            'no_telp',
            'no_hp',
            'email',
            'sto',
            'alprolama',
            'alprobaru',
            'tito',
            'keterangan',
            'tgl_order',
            'tgl_pi',
            'last_update',
            'umur',
            'ncli',
            'pots',
            'speedy',
            'datel',
            'witel',
            'reg'
        );
    }

    $html = file_get_contents($url);

    $dom = @\DOMDocument::loadHTML($html);
    //var_dump($dom);
    $table = $dom->getElementsByTagName('table')->item($table_index);
    $rows = $table->getElementsByTagName('tr');


    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');

        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] = $td->textContent;
        }

        $result[] = $data;
    }

    // print_r($result);

    return $result;
  }


  private function mydashboard_request($url, $columns = array(), $table_index = 2)
  {
    if (!count($columns)) {
        $columns = array(
            1 => 'Wilayah',
            'Kandatel',
            'Ndem',
            'Ncli',
            'Ndos',
            'Status',
            'Id_Alpro',
            'ND',
            'ND_Speedy',
            'Citem',
            'Kecepatan',
            'MDF',
            'DESKRIPSI',
            'Chanel',
            'Status_Indihome',
            'Tgl_Reg',
            'Tgl_PS',
            'Umur',
            'Nama',
            'Kcontact',
            'Jalan',
            'No_Jalan',
            'Distrik',
            'Kota',
            'Status_Indent_Awal',
            'Status_Indent_Akhir',
            'Sebab',
            'Ket_Sebab',
            'Solusi',
            'Ket_Solusi',
            'Due_Date_Solusi',
      			'ODP',
      			'Status_Ukur'
        );
    }

    $html = file_get_contents($url);

    $dom = @\DOMDocument::loadHTML($html);
    //var_dump($dom);
    $table = $dom->getElementsByTagName('table')->item($table_index);
    $rows = $table->getElementsByTagName('tr');


    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');

        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] = $td->textContent;
        }

        $result[] = $data;
    }

    // print_r($result);

    return $result;
  }


  private function mydashboard2P_request($url, $columns = array(), $table_index = 2)
  {
    if (!count($columns)) {
        $columns = array(
            1 => 'Wilayah',
            'Kandatel',
            'Ndem',
            'Ncli',
            'Ndos',
            'Status',
            'Id_Alpro',
            'SALES',
            'NETIZEN',
            'CCAT',
            'PLBLCL',
            'ND',
            'ND_Speedy',
            'Kecepatan',
            'MDF',
            'Status_Indihome',
            'Chanel',
            'Tgl_Reg',
            'Tgl_PS',
            'Umur',
            'Nama',
            'Kcontact',
            'Jalan',
            'No_Jalan',
            'Distrik',
            'Kota',
            'Status_Indent_Awal',
            'Status_Indent_Akhir',
            'Sebab',
            'Ket_Sebab',
            'Solusi',
            'Ket_Solusi',
            'Due_Date_Solusi',
      'ODP',
      'Status_Ukur',
      'DESKRIPSI'
        );
    }

    $html = file_get_contents($url);

    $dom = @\DOMDocument::loadHTML($html);
    //var_dump($dom);
    $table = $dom->getElementsByTagName('table')->item($table_index);
    $rows = $table->getElementsByTagName('tr');


    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');

        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] = $td->textContent;
        }

        $result[] = $data;
    }

    // print_r($result);

    return $result;
  }

  private function mydashboard_mbsp_request($url, $columns = array(), $table_index = 0)
  {
    if (!count($columns)) {
        $columns = array(
            1 => 'Prospek_ID',
            'ND',
            'NO_INTERNET',
            'Prospek',
            'Status_Call',
            'Status_Webcare',
            'Tanggal_Call',
            'Status_QA',
            'Carring_ID',
            'Nama',
            'Alamat_Instalasi',
            'Status_Inbox',
            'SC_ID',
            'Jenis_Psb',
            'Tanggal_Create',
            'Status_Order',
            'Nama_Paket',
            'Last_Update',
            'Kawasan',
            'Witel'
        );
    }

    $html = file_get_contents($url);

    $dom = @\DOMDocument::loadHTML($html);
    //var_dump($dom);
    $table = $dom->getElementsByTagName('table')->item($table_index);
    $rows = $table->getElementsByTagName('tr');


    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');

        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] = $td->textContent;
        }

        $result[] = $data;
    }

    // print_r($result);

    return $result;
  }
}
