@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="panel panel-default">
      <div class="panel-heading">
          <p>Input SC PDA</p>
      </div>

      <div class="panel-body">
          <form method="POST" action="/hdfallout/simpan-pda">
              <div class="form-group">
                  <input type="text" class="form-control" name="scPda" id="scPda">
                  {!! $errors->first('scPda','<div class="label label-danger">:message</div>') !!}
              </div>

              <input type="submit" class="btn btn-primary" value="Simpan">
          </form>
      </div>
    
  </div>

  <div class="table-responsive">
    <table class="table table-striped table-bordered dataTable">
      <thead>
        <tr>
          <th>#</th>
          <th width="75" colspan=3>Work Order HD Fallout</th>
        </tr>
      </thead>
      <tbody>
        @foreach($listFallout as $no => $data)
          <tr>
            <td rowspan=3>{{ ++$no }}.</td>
            @if(session('auth')->level <> 19)
              <td colspan=3><span>
            @if(!empty($data->workerName))
              <span class="label label-warning">{{ $data->workerName }} {{ $data->takeTime }}</span>
              @if($data->workerId == session('auth')->id_karyawan)
                <a data-toggle="modal" data-target="#modal-fallout" data-sc="{{ $data->orderId }}" data-st="drop" class="label label-info" data-toggle="tooltip" title="{{ $data->workerName }}">Drop Order</a>
              @endif
            @else
              <a data-toggle="modal" data-target="#modal-fallout" data-sc="{{ $data->orderId }}" data-st="store" class="label label-info">Take Order</a>
            @endif
            </span></td>
    @endif
    </tr>
    <tr>
        <td colspan=1>
          {{ $data->orderId }}
          {{ $data->orderStatus }}
          <br />
          {{ $data->orderDate ? : 'Tidak Ada SC' }}<br />
          {{ $data->ndemPots }}~{{  $data->ndemSpeedy }}<br />
          {{ $data->orderName }}<br />
          {{ $data->sto }} /
          {{ $data->jenisPsb }} <br />
          {{ $data->orderPaket }} <br />
          {{ $data->alproname ? : 'Tidak Ada Informasi ODP' }} <br />
          {{ $data->kcontact }}<br />
          {{ $data->orderAddr ? : '' }} </td>
       </tr>
      <tr>
        
    </td>

      </tr>
      @endforeach

      <?php
          $nomor = $jumlahList1;
       ?>

      @foreach($listFallout2 as $no=>$dt)
          <tr>
              <td rowspan="3">{{ ++$no + $nomor }}.</td>
              <td colspan="3">
                  <span class="label label-warning">{{ $dt->workerName }} {{ $dt->takeTime }}</span>
                  <span class="label label-primary">PDA</span>  
              </td>
          </tr>

          <tr>
            <td colspan=1>
              {{ $dt->orderId }}
              {{ $dt->orderStatus }}
              <br />
              {{ $dt->orderDate ? : 'Tidak Ada SC' }}<br />
              {{ $dt->ndemPots }}~{{  $dt->ndemSpeedy }}<br />
              {{ $dt->orderName }}<br />
              {{ $dt->sto }} /
              {{ $dt->jenisPsb }} <br />
              {{ $dt->orderPaket }} <br />
              {{ $dt->alproname ? : 'Tidak Ada Informasi ODP' }} <br />
              {{ $dt->kcontact }}<br />
              {{ $dt->orderAddr ? : '' }} </td>
           </tr>

           <tr></tr>
      @endforeach

    </tbody>
  </table>
  </div>

  <div class="modal fade" id="modal-fallout">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Verifikasi</h4>
        </div>

        <div class="modal-body">
          <input type="hidden" name="orderId" id="orderId" value=""/>
          <input type="hidden" name="status" id="status" value=""/>
          <b id="ket"></b> <label class="label label-warning" id="label-sc"></label> ?
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-info">Lanjutkan</button>
        </div>
        <form>
      </div>
    </div>
  </div>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    
    $("#modal-fallout").on('show.bs.modal', function(e) {
      if($(e.relatedTarget).data('st') == "drop"){
        $('#ket').html("Anda akan mendrop order Fallout ini");
      }else{
        $('#ket').html("Anda akan mengerjakan order Fallout ini");
      }
      $('#label-sc').html($(e.relatedTarget).data('sc'));
      $('#orderId').val($(e.relatedTarget).data('sc'));
      $('#status').val($(e.relatedTarget).data('st'));
    });
  });
</script>
@endsection
