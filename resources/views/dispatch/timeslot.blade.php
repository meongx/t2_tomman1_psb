@extends('layout')

@section('content')
  @include('partial.alerts')
  @include('workorderlist')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
  </style>
  <div style="padding-bottom: 10px;">
<!--     <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong> -->
  </div>
  @foreach ($get_area as $result_area)
  @if (array_key_exists($result_area->area, $data))
  <div class="panel panel-primary" id="timeslot">
    <div class="panel-heading">TIMESLOT WORKORDER {{ $result_area->DESKRIPSI }}</div>
    <div class="panel-body">
      <div class="list-group">
	    <div class="table-responsive">
				<table class="table table-striped table-bordered">
          <tr>
            <th rowspan=2>No</th>
            <th rowspan=2>Nama Tim</th>
            <th rowspan=2>Dispatched<br />Ticket</th>
            <th rowspan=2>MANJA<br />OK</th>
            <th rowspan=2>MANJA<br />NOK</th>
            <th colspan="10" align=center>Timeslot</th>
          </tr>
          <tr>
            @foreach ($timeslot as $result)


            <th>{{ $result->dispatch_teknisi_timeslot }}</th>
            @endforeach
          </tr>
          <?php
            // print_r($data);
            $number = 1;
          ?>
            @foreach ($listTim  as $result_tim)
            <?php $area = $result_area->area; ?>
            @if ($result_tim->area == $area)
            <tr>
              <td>{{ $number }}.</td>
              <td>{{ $result_tim->uraian }}</td>
              <td>{{ $result_tim->jumlah_wo }}</td>
              <td>{{ $result_tim->jumlah_manja }}</td>
              <td>{{ $result_tim->jumlah_manja_nok }}</td>
              <!-- <td>0</td> -->
              <?php $number1 = 1; ?>
              @foreach($timeslot as $result)
              @if (array_key_exists($result_tim->id_regu, $data[$area]))
              @if (array_key_exists($number1, $data[$area][$result_tim->id_regu]))
              <td>
              <?php
                $jumlah = count($data[$area][$result_tim->id_regu][$number1]);

                if ($jumlah>0) {
                for($i=0;$i<$jumlah; $i++){
                  $row = $data[$area][$result_tim->id_regu][$number1][$i];
                  if ($row['status_laporan']==1) {
                    $label = "success";
                  } else if ($row['status_laporan']==5) {
                    $label = "warning";
                  } else if ($row['status_laporan']==10 || $row['status_laporan']==2 || $row['status_laporan']==3 || $row['status_laporan']==7 || $row['status_laporan']==4) {
                    $label = "danger";
                  } else {
                    $label = "info";
                  }
                  echo '<a data-html="true" class="label label-'.$label.'" data-toggle="popover" title="'.$row['orderName'].'" data-content="'.str_replace('"','',$row['manja']).'">SC'.$row['SC'].'</a><br />';
                  if ($row['timeCount']<>"")
                  echo "<small><b>".$row['timeCount']."</b></small><br />";

                }
              }
              ?>

              </td>
              @else
              <td><span class="label label-default">NO WORKORDER</span></td>
              @endif
              @else
              <td><span class="label label-default">NO WORKORDER</span></td>

              @endif
              <?php
                $number1++;
              ?>
              @endforeach
            </tr>
            <?php
            $number++;
            ?>
            @endif
          @endforeach

        </table>

        <span class="label label-info">BELUM SURVEY</span>
        <span class="label label-warning">OGP</span>
        <span class="label label-danger">KEND. TEKNIS / PROGRESS PT2/PT3 / KEND. PELANGGAN / HR</span>
        <span class="label label-success">UP</span>
      </div>
    </div>
  </div>
</div>
@endif
@endforeach
<div id="timexcount">0 ms</div>
		<div class="panel-body">
		<script>
        $(document).ready(function(){
          $("[data-toggle='popover']").popover({html:true});
        })
				var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
        var c=0;
        var minutes= 0;
        var t;
        var timer_is_on=0;

        function timedCount(element)
        {
        document.getElementById(element).innerHTML = minutes+' min '+c+' sec';
        c=c+1;
        if (c%60==0){
          minutes+=1;
          c=0;
        }
        t=setTimeout("timedCount()",1000);
        }

        function showElement(element){
          document.getElementById(element).innerHTML = "Detected";
        }


    </script>

							<div id="bar-10" style="height: 450px; width: 100%;"></div>
						</div>
					</div>
  </div>
@endsection
