@extends('layout')

@section('content')
<form>
	@foreach($berita as $no => $new)
	{{-- <div class="row"> --}}
		<div class="col-sm-4">
			<div class="panel panel-default">
				<div class="panel-heading"><img style="max-width: 100%;height: 200px" src="{{ asset('/upload/Thumbnails/'.$new->thumbnails)}}" width="320"/></div>
				<div class="panel-body">
					<h4 class="card-text">{!! $new->judul !!}</h4>
					<?php
					$tanggalve = $new->create_at;
					$tahun_1 = substr( $tanggalve, 0, 4 );
					$bulan_1 = substr( $tanggalve, 5, 2 );
					$tgl_1 = substr( $tanggalve, 8, 2 );
					$result_new = $tahun_1 . "-" . $bulan_1 . "-" . $tgl_1;
					$tanggalva = $new->last_update;
					$tahun_2 = substr( $tanggalva, 0, 4 );
					$bulan_2 = substr( $tanggalva, 5, 2 );
					$tgl_2 = substr( $tanggalva, 8, 2 );
					$result_last = $tahun_2 . "-" . $bulan_2 . "-" . $tgl_2;
					$tanggalrawcreate = strtotime( $new->create_at );
					$tanggalrawlast = strtotime( $new->last_update );
					$date2 = strtotime(date('Y-m-d H:i:s'));
					$seconds_diff = $date2 - $tanggalrawcreate;
					$seconds_diff1 = $date2 - $tanggalrawlast;
					if ($seconds_diff >= 31536000) {
						$seen=intval($seconds_diff / 31536000) . " tahun lalu";
					} elseif ($seconds_diff >= 2419200) {
						$seen=intval($seconds_diff / 2419200) . " bulan lalu";
					} elseif ($seconds_diff >= 86400) {
						$seen=intval($seconds_diff / 86400) . " hari lalu";
					} elseif ($seconds_diff >= 3600) {
						$seen= intval($seconds_diff / 3600) . " jam lalu";
					} elseif ($seconds_diff >= 60) {
						$seen= intval($seconds_diff / 60) . " menit lalu";
					} else {
						$seen= "kurang dari semenit lalu";
					}
					if ($seconds_diff1 >= 31536000) {
						$saw="Diperbaharui ". intval($seconds_diff1 / 31536000) . " tahun lalu";
					} elseif ($seconds_diff1 >= 2419200) {
						$saw="Diperbaharui ". intval($seconds_diff1 / 2419200) . " bulan lalu";
					} elseif ($seconds_diff1 >= 86400) {
						$saw="Diperbaharui ". intval($seconds_diff1 / 86400) . " hari lalu";
					} elseif ($seconds_diff1 >= 3600) {
						$saw= "Diperbaharui ". intval($seconds_diff1 / 3600) . " jam lalu";
					} elseif ($seconds_diff >= 60) {
						$saw= "Diperbaharui ". intval($seconds_diff1 / 60) . " menit lalu";
					} else {
						$saw= "Diperbaharui kurang dari semenit lalu";
					}
					?>
					<p>{!!"Telah dilihat ".$new->view." kali"!!}</p>
					@if(isset($new->last_update))
					<p>{!!"Di Perbaharui Pada Tanggal ".$result_last, "/ ". $saw !!}</p>
					@else
					<p>{!!"Di Publish Pada Tanggal ".$result_new, "/ ". $seen !!}</p>
					@endif
					<a class="btn btn-primary btn-sm" role="button" onclick="window.location='{{ URL::to("/view/{$new->id_berita}")}}'">read more...</a>
					@if(session('auth')->level == 2)
					<a class="btn btn-success btn-sm" role="button" onclick="window.location='{{ URL::to("/update/{$new->id_berita}")}}'">Ubah</a>
					<button type="button" class="btn btn-danger btn-sm" onclick="window.location='{{ URL::to("/delete/{$new->id_berita}")}}'">Hapus</button>
					@endif
				</div>
			</div> 
		</div>
		@endforeach
	</form>
	<div class="text-left col-md-12">
		{!! $berita->render();!!}
	</div>
	@endsection