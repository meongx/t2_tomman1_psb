@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <style>
  	.panel-body {

  		padding : 0px !important;
  	}
  	.panel-body table td,th {
  		font-size: 14px !important;
  	}
  	.panel {
  		border-radius: 3px !important;
  	}
    td {
      padding : 5px !important;
    }
    .label-black {
      background-color: #34495e;
      color : #FFF;
    }
  </style>
  <h3>{{ $title }}</h3>
  <span class="label label-tomman">Lastsync x: {{ $lastsync[0]->UpdatedDatetime }}</span>
  <br />

  <div class="row">
  	<div class="col-sm-124">

  			<div class="panel panel-tomman">
  				<div class="panel-heading">Saldo by JAM (REGULER)</div>
  				<div class="panel-body">
  					<div class="table-responsive">
			  		<table class="table styledTable">
			  			<tr>
			  				<th rowspan="2">ORG</th>
			  				<th width="50" rowspan="2">HOLD</th>
			  				<th colspan="7">JAM</th>
			  				<th rowspan="2">TOTAL</th>
			  				<th colspan="2">DATA</th>
			  			</tr>
			  			<tr>
			  				<th width="50">0-2</th>
			  				<th width="50">2-4</th>
			  				<th width="50">4-6</th>
			  				<th width="50">6-8</th>
			  				<th width="50">8-10</th>
			  				<th width="50">10-12</th>
			  				<th width="50">>12</th>
			  				<th width="50"><12</th>
			  				<th width="50">>12</th>
			  			</tr>
			  			@foreach ($query as $result)
			  			<tr>
			  				<td class="areatext"><a href="/assuranceNossa/{{ $result->ORG }}">{{ $result->ORG }}</a></td>
                @foreach ($periode as $per)
			  				<td align="center">
                  <!-- <a href="/assuranceNossaList/ALL/{{ $per }}/{{ $result->ORG }}">{{ $per }}<br />{{ $result->$per }}</a> -->
                  @if ($result->$per>0)
                  <br />
                  <table class="table">
                    <tr>
                      <td><b>Team</b></td>
                    </tr>
                    @foreach ($data[$result->ORG][$per] as $num => $result_pertiket)
                    <tr>
                      <td nowrap style="text-align:left !important">
                        <small> {{ $result_pertiket->Incident }} ({{ $result_pertiket->Umur }}jam) // {{ $result_pertiket->Team }} // <b>{{ $result_pertiket->Status  ? : 'NO UPDATE' }}</b></small>
                      </td>
                    </tr>
                    @endforeach
                  </table>
                  @endif
                </td>
                @endforeach
			  				<td align="center"><a href="/assuranceNossaList/ALL/ALL/{{ $result->ORG }}">{{ $result->Jumlah }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/xkurang12jam/{{ $result->ORG }}">{{ $result->xkurang12jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x12jam/{{ $result->ORG }}">{{ $result->x12jam }}</a></td>
			  			</tr>
			  			@endforeach
			  		</table>
			  	</div>
			  </div>
	  	</div>
  	</div>
  	<div class="col-sm-8">

  			<div class="panel panel-tomman">
  				<div class="panel-heading">Saldo by JAM (REGULER MYI)</div>
  				<div class="panel-body">
  					<div class="table-responsive">
			  		<table class="table styledTable">
			  			<tr>
			  				<th rowspan="2">ORG</th>
			  				<th rowspan="2">HOLD</th>
			  				<th colspan="7">JAM</th>
			  				<th rowspan="2">TOTAL</th>
			  				<th colspan="2">DATA</th>
			  			</tr>
			  			<tr>
			  				<th width="50">0-2</th>
			  				<th width="50">2-4</th>
			  				<th width="50">4-6</th>
			  				<th width="50">6-8</th>
			  				<th width="50">8-10</th>
			  				<th width="50">10-12</th>
			  				<th width="50">>12</th>
			  				<th width="50"><12</th>
			  				<th width="50">>12</th>
			  			</tr>
			  			@foreach ($query_myi as $result)
			  			<tr>
			  				<td class="areatext">{{ $result->ORG }}</td>
			  				<td align="center"><a href="/assuranceNossaList/6/HOLD/{{ $result->ORG }}">{{ $result->HOLD }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x0dan2jam/{{ $result->ORG }}">{{ $result->x0dan2jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x2dan4jam/{{ $result->ORG }}">{{ $result->x2dan4jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x4dan6jam/{{ $result->ORG }}">{{ $result->x4dan6jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x6dan8jam/{{ $result->ORG }}">{{ $result->x6dan8jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x8dan10jam/{{ $result->ORG }}">{{ $result->x8dan10jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x10dan12jam/{{ $result->ORG }}">{{ $result->x10dan12jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x12jam/{{ $result->ORG }}">{{ $result->x12jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/ALL/{{ $result->ORG }}">{{ $result->Jumlah }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/xkurang12jam/{{ $result->ORG }}">{{ $result->xkurang12jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x12jam/{{ $result->ORG }}">{{ $result->x12jam }}</a></td>
			  			</tr>
			  			@endforeach
			  		</table>
			  	</div>
			  </div>
	  	</div>
  	</div>
  </div>
@endsection
