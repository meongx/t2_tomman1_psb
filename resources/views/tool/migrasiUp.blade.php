@extends('layout')

@section('content')
  @include('partial.alerts')
      <div class="panel panel-primary" id="info">
          <div class="panel-body table-responsive">
              <table class="table table-bordered table-fixed">
                  <tr>
                      <th>#</th>
                      <th>WO</th>
                      <th>ODP</th>
                      <th>Koordinat ODP</th>
                      <th>updated_at</th>
                  </tr>
        
                  @foreach($data as $no => $d)
                          <td>{{ ++$no }}</td>
                          <td><a href="/{{ $d->id_tbl_mj }}">{{ $d->Ndem }}</a></td>
                          <td>{{ $d->nama_odp }}</td>
                          <td>{{ $d->kordinat_odp }}</td>
                          <td>{{ date('Y-m-d',strtotime($d->updated_at)) }}</td>
                      </tr>
                  @endforeach
              </table>
          </div>
      </div>
@endsection
