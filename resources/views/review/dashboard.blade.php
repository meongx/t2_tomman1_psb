@extends('layout')

@section('content')
  @include('partial.alerts')
  <style type="text/css">
    .green {
      background-color : #00cec9;
      padding : 20px;
      color : #FFF;
    }
    .blue {
      background-color : #00b894;
      padding : 20px;
      color : #FFF;
    }
    .red {
      background-color : #ff7675;
      padding : 20px;
      color : #FFF;
    }
    .jumbo {
      font-size: 20px;
      font-weight: bold;
    }
  </style>
  <div class="row">
  	<center><h3>Review Pelanggan</h3></center>
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
          <div class="col-sm-4 green">
            <span class="jumbo">INBOX REVIEW : {{ $get_all_review[0]->jumlah }}</span>
          </div>
          <div class="col-sm-4 blue">
           <span class="jumbo">AVG RATING : {{ round($get_all_review[0]->rating,1) }}</span> 
          </div>
          <div class="col-sm-4 red">
           <span class="jumbo">UNDER RATE : <a href="/dashboardReviewListTekUnderrate">{{ $get_all_review[0]->underrate }}</a></span> 
          </div>

        </div>
      </div>
    </div>
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading"></div>
  			<div class="panel-body">
  				<div class="row">
  				<div class="col-sm-6">
            Ranking Sektor
  					<table class="table">
  						<tr>
  							<th>Rank</th>
  							<th>Sektor</th>
  							<th>Avg</th>
  							<th>Jml</th>
  						</tr>
              <?php
                $total = 0;
                $rating = 0;
              ?>
  						@foreach ($get_review as $num => $review)
              <?php
                $rating += $review->review;
                $total += $review->jumlah;
              ?>
  						<tr>
  							<td>{{ ++$num }}</td>
  							<td>{{ $review->title }}</td>
  							<td>{{ round($review->review,1) }}</td>
  							<td><a href="/dashboardReviewList/{{ $tgl }}/{{ $review->chat_id }}">{{ $review->jumlah }}</a></td>
  						</tr>
  						@endforeach
              <tr>
                <th colspan="2">Total</th>
                <th>{{ @round($rating/$num,1) }}</th>
                <th>{{ $total }}</th>
              </tr>
  					</table>
  				</div>
  				<div class="col-sm-6">
            Top Teknisi
  					<table class="table">
              <tr>
                <th>Rank</th>
                <th>Nama</th>
                <th>Avg</th>
                <th>Jml</th>
              </tr>
              @foreach ($get_top_tek as $num => $top_tek)
              <tr>
                <td>{{ ++$num }}</td>
                <td>{{ $top_tek->uraian }}</td>
                <td>{{ round($top_tek->review,1) }}</td>
                <td><a href="/dashboardReviewListTek/{{ $tgl }}/{{ $top_tek->id_regu }}">{{ $top_tek->jumlah }}</a></td>
              </tr>
              @endforeach
            </table>
  				</div>
  				</div>
  					
  			</div>
  		</div>
  	</div>
  </div>
@endsection