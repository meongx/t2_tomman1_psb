<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\SalesModel;

class SalesController extends Controller
{
    public function list(Request $req){
    	$getData = SalesModel::getAllSales();
    	if($req->has('cari')){
    		$getData = SalesModel::getSalesByCari($req->input('cari'));
    	};

    	return view('sales.ListSales',compact('getData'));
    }

    public function simpanSales(Request $req){
    	$this->validate($req,[
    		'kode'	=> 'required',
    		'nama'	=> 'required'
    	],[
    		'kode.required'	=> 'Kode Sales Jangan kosong',
    		'nama.required'	=> 'Nama Sales Jangan Kosong'
    	]);

    	$getSales = SalesModel::getSalesByKodeSales($req->kode);
    	if(count($getSales)<>0){
    		return response()->json([
    			'exist'	=> 'ada'
    		],404);
    	};

    	SalesModel::simpanData($req);
    }
}
