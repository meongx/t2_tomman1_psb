@extends($layout)

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      padding: 5px;
      text-align: center;
      vertical-align: middle;
    }
    td {
      padding :5px;
      border-color:black;
    }
    .UP4 {
      background-color : #2ecc71;
    }
    .okay {
      background-color : #636e72;  
    }
  </style>
  <div style="padding-bottom: 10px;">

    <h3>Produktifitas </h3>

    <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong>
  </div>

  <div class="panel panel-primary" id="produktifitas">
    <?php
      if ($area == 1) { $area = "INNER"; } elseif ($area == 0) { $area = "OUTER"; }
    ?>
    <div class="panel-heading">PRODUKTIFITAS {{ $area }} - {{ $date }}</div>
    <div class="panel-body">
      @if (count($get_hari_lembur)>0 && $konstanta == 1)

      <div class="alert alert-info">
         <strong>Info!</strong> Hari ini adalah <strong>{{ $get_hari_lembur->keterangan }}</strong>, Lembur per MH Rp. {{ number_format($get_hari_lembur->perMH)  }} ,-
      </div>
      @endif
      <div class="list-group">
	    <div class="table-responsive">
        <table class="table table-striped table-bordered" >
          <tr>
            <th>Rank</th>
            <th>Tim</th>
            <th>Sektor</th>
            <th>Last Update</th>
            <th>ORDER</th>
            <th>NP</th>
            <th>OGP</th>
            <th>KP</th>
            <th>KT</th>
            <th>HR</th>
            <th>UP</th>
          </tr>
          <?php
            $total = 0; 
            $NP = 0; 
            $OGP = 0; 
            $KP = 0; 
            $KT = 0; 
            $HR = 0; 
            $UP = 0; 
          ?>
          @foreach ($getTeamMatrix as $num => $team)
          <?php
            $total += $team->jumlah;
            $NP += $team->NP;
            $KP += $team->KP;
            $KT += $team->KT;
            $HR += $team->HR;
            $UP += $team->UP;
            $colorUP = "okay";
            if ($team->UP>=4) { $colorUP = "UP4"; } 
          ?>
          <tr>
            <td align="center">{{ ++$num }}</td>
            <td>{{ $team->uraian }}</td>
            <td>{{ $team->sektor }}</td>
            <td>
              <?php
              if ($dataLastUpdate[$team->id_regu]<>"Starting Progress at 09.00 AM") {
                ?>

                  @foreach ($dataLastUpdate[$team->id_regu] as $num => $teamx)
                  <span id="daysX{{ $teamx->Ndem }}"></span> days
                <span id="hoursX{{ $teamx->Ndem }}"></span> hours
        <span id="minsX{{ $teamx->Ndem }}"></span> mins
        <span id="secsX{{ $teamx->Ndem }}"></span> secs</b>
        <br />
                  <script>

                  function upTime{{ $teamx->Ndem }}(daysX,hoursX,minsX,secsX,countTo) {

                    now = new Date();
                    countTo = new Date(countTo);
                    difference = (now-countTo);

                    days=Math.floor(difference/(60*60*1000*24)*1);
                    hours=Math.floor((difference%(60*60*1000*24))/(60*60*1000)*1);
                    mins=Math.floor(((difference%(60*60*1000*24))%(60*60*1000))/(60*1000)*1);
                    secs=Math.floor((((difference%(60*60*1000*24))%(60*60*1000))%(60*1000))/1000*1);
 
                    document.getElementById(daysX).innerHTML = days;
                    document.getElementById(hoursX).innerHTML = hours;
                    document.getElementById(minsX).innerHTML = mins;
                    document.getElementById(secsX).innerHTML = secs;

                    clearTimeout(upTime{{ $teamx->Ndem }}.to);
                    upTime{{ $teamx->Ndem }}.to=setTimeout(function(){ upTime{{ $teamx->Ndem }}(daysX,hoursX,minsX,secsX,countTo); },1000);
                  }
                  <?php
                    if ($teamx->created_at<>NULL){ $tanggalnya = $teamx->created_at; } else { $tanggalnya = date('Y-m-d').' 08:00:00'; }
                    $dateTimeExplode = explode(' ',$tanggalnya);
                    $dateExplode = explode('-',$dateTimeExplode[0]);
                  ?>

                  upTime{{ $teamx->Ndem }}('daysX{{ $teamx->Ndem }}','hoursX{{ $teamx->Ndem }}','minsX{{ $teamx->Ndem }}','secsX{{ $teamx->Ndem }}','{{ $dateExplode[1] }},{{ $dateExplode[2] }},{{ $dateExplode[0] }},{{ $dateTimeExplode[1] }}');


                </script>
                
                  @endforeach
                    
                <?php
                foreach($dataLastUpdate[$team->id_regu] as $lastupdate){
                  echo $lastupdate->laporan_status." - ".$lastupdate->Ndem;
                }
              } else {
                echo "<span class='label label-danger'>Please Start Progress at 08.00 AM</span>";
              }
              ?>

            </td>
            <td align="center">{{ $team->jumlah }}</td>
            <td align="center"><a href="/produktifitasteklist/{{ $date }}/{{ $team->id_regu }}/NP">{{ $team->NP }}</a></td> 
            <td align="center"><a href="/produktifitasteklist/{{ $date }}/{{ $team->id_regu }}/OGP">{{ $team->OGP }}</a></td>
            <td align="center"><a href="/produktifitasteklist/{{ $date }}/{{ $team->id_regu }}/KP">{{ $team->KP }}</a></td>
            <td align="center"><a href="/produktifitasteklist/{{ $date }}/{{ $team->id_regu }}/KT">{{ $team->KT }}</a></td>
            <td align="center"><a href="/produktifitasteklist/{{ $date }}/{{ $team->id_regu }}/HR">{{ $team->HR }}</a></td>
            <td align="center"><a href="/produktifitasteklist/{{ $date }}/{{ $team->id_regu }}/UP"><span class="badge {{ $colorUP }}">{{ $team->UP }}</span></a></td>
          </tr>
          @endforeach
          <tr> 
            <th colspan="4">Total</th>
            <th>{{ $total }}</th>
            <th>{{ $NP }}</th>
            <th>{{ $OGP }}</th>
            <th>{{ $KP }}</th>
            <th>{{ $KT }}</th>
            <th>{{ $HR }}</th>
            <th>{{ $UP }}</th>
          </tr>
        </table>


      </div>
    </div>
  </div>
</div>

<div id="timexcount">0 ms</div>
		<div class="panel-body">


							<div id="bar-10" style="height: 450px; width: 100%;"></div>
						</div>
					</div>
  </div>
@endsection
