@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="row">
  	<center><h3>Dashboard Review Pelanggan</h3></center>
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading"></div>
  			<div class="panel-body table-responsive">
  				<table class="table">
  					<tr>
  						<th>No.</th>
  						<th>Regu</th>
  						<th>Sektor</th>
  						<th>Order</th>
  						<th>Nama Pelanggan</th>
  						<th>Kontak</th>
  						<th>Tgl</th>
  						<th>Rating</th>
  						<th>Catatan</th>
  						<th>Action</th>
  					</tr>
  					@foreach ($get_review as $num => $review)
  					<tr>
  						<td>{{ ++$num }}</td>
  						<td>{{ $review->uraian }}</td>
  						<td>{{ $review->title }}</td>
  						<td>{{ $review->Ndem }}</td>
  						<td>{{ $review->Customer_Name }}</td>
  						<td>{{ $review->Contact_Phone }}</td>
  						<td>{{ $review->date_created ? : 'DEFAULT' }}</td>
  						<td>{{ $review->Review ? : '3' }}</td>
  						<td>{{ $review->Catatan }}</td>
  						<td><a href="/review/followup/" class="label label-success">Follow UP</a></td>
  					</tr>
  					@endforeach
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection