@extends('tech_layout')

@section('content')
  <style>
  .btn strong.glyphicon {         
    opacity: 0;       
  }
  .btn.active strong.glyphicon {        
    opacity: 1;       
  }
  .input-photos img {
      width: 80px;
      height: 130px;
      margin-bottom: 5px;
    }
  </style>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
  <script src="/bower_components/vue/dist/vue.min.js"></script>
  <script src="/bower_components/bootstrap-list-filter/bootstrap-list-filter.min.js"></script>
  <!-- <link rel="stylesheet" href="/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" /> -->
  @include('marina.partial.alert')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    
    <h3>
      <a href="/marina/tech" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Proggress {{ $data->no_tiket }}
      </a>
    </h3>
    <div class="row col-sm-6">
    <div class="panel panel-default">
      <div class="panel-body">
    <!--<div class="form-group">
      <label class="control-label" for="input-team">Team</label> -->
      <input name="team" type="hidden" id="input-team" class="form-control" value="{{ $data->team or '' }}" />

    <!--</div>
    <div class="form-group">
      <label class="control-label" for="input-tgl">Tanggal Selesai</label>-->
      <input name="tgl_selesai" type="hidden" id="input-tgl" class="form-control" value="{{ $data->tgl_selesai or '' }}" />

    
    <div class="btn-group" role="group" aria-label="Basic example">
      <a href="/marina/validasiawal/{{Request::segment(3)}}" class="btn btn-sm btn-success">
      <span class="glyphicon glyphicon-arrow-right"> Update Port Awal</span></a>
      <a href="/marina/reboundary/{{Request::segment(3)}}" class="btn btn-sm btn-info">
      <span class="glyphicon glyphicon-arrow-right"> Reboundary</span></a>
      <a href="/marina/validasi/{{Request::segment(3)}}" class="btn btn-sm btn-success">
      <span class="glyphicon glyphicon-arrow-right"> Update Port Akhir</span></a>
    </div>
    <!--</div>-->
    <!--
    @if(@$data->jenis_order == 4)
    <div class="form-group">
      <label class="control-label" for="input-koor">NO_Speedy Ibooster ( "Close" bisa dipilih apabila Redaman Lebih dari -24)</label>
      <input name="speedy" type="text" id="input-speedy" class="form-control input-sm" value="{{ @$data->no_tiket }}" />
      <button class="btn btn-sm btn-info" type="button" id="ukur">
        <span class="glyphicon glyphicon-list"></span>
        Ukur <b id="red"></b>
      </button>
      <input type="hidden" name="ibooster" id="ibooster" class="form-control"/>
    </div>
    @endif
  --> 
    @if($data->jenis_order == 10)
   <a href="/marina/validasi/{{Request::segment(3)}}" class="btn btn-sm btn-success">
        <span class="glyphicon glyphicon-arrow-right"> Update Port</span></a>
    @endif
    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }} ">
      <!--
        {{ (@$data->jenis_order == 4 && @$data->status !='close')? 'input_remo':'' }}"
      -->
      <label for="input-status" class="control-label">Status</label>
      <input name="status" type="text" id="input-status" class="form-control input-sm" value="{{ old('status') ?: @$data->status }}"/>
      @foreach($errors->get('status') as $msg)
          <span class="help-block">{{ $msg }}</span>
      @endforeach
    </div>
    @if(@$data->jenis_order == 4)
      <div class="form-group col-sm-6 col-xs-6 {{ $errors->has('redaman_awal') ? 'has-error' : '' }}">
        <label class="control-label" for="redaman_awal">Redaman Sebelum</label>
        <input name="redaman_awal" type="text" id="redaman_awal" class="form-control input-sm" value="{{ old('redaman_awal') ?: @$data->redaman_awal }}" />
        @foreach($errors->get('redaman_awal') as $msg)
            <span class="help-block">{{ $msg }}</span>
        @endforeach
      </div>
      <div class="form-group col-sm-6 col-xs-6 {{ $errors->has('redaman_akhir') ? 'has-error' : '' }}">
        <label class="control-label" for="redaman_akhir">Redaman Sesudah</label>
        <input name="redaman_akhir" type="text" id="redaman_akhir" class="form-control input-sm" value="{{ old('redaman_akhir') ?: @$data->redaman_akhir }}" />
        @foreach($errors->get('redaman_akhir') as $msg)
            <span class="help-block">{{ $msg }}</span>
        @endforeach
      </div>
    @endif
    <div class="form-group {{ $errors->has('splitter') ? 'has-error' : '' }}">
      <label for="splitter" class="control-label">Jenis Splitter</label>
      <input name="splitter" type="text" id="splitter" class="form-control input-sm" value="{{ old('splitter') ?: @$data->splitter }}"/>
      @foreach($errors->get('splitter') as $msg)
          <span class="help-block">{{ $msg }}</span>
      @endforeach
    </div>
    <div class="form-group {{ $errors->has('port_idle') ? 'has-error' : '' }}">
      <label for="port_idle" class="control-label">Jumlah Port ODP Kosong</label>
      <input name="port_idle" type="text" id="port_idle" class="form-control input-sm" value="{{ old('port_idle') ?: @$data->port_idle }}"/>
      @foreach($errors->get('port_idle') as $msg)
          <span class="help-block">{{ $msg }}</span>
      @endforeach
    </div>
    <div class="form-group {{ $errors->has('port_used') ? 'has-error' : '' }}">
      <label for="port_used" class="control-label">Jumlah Port ODP Isi</label>
      <input name="port_used" type="text" id="port_used" class="form-control input-sm" value="{{ old('port_used') ?: @$data->port_used }}"/>
      @foreach($errors->get('port_used') as $msg)
          <span class="help-block">{{ $msg }}</span>
      @endforeach
    </div>

    <div class="form-group {{ $errors->has('action_cause') ? 'has-error' : '' }}">
      <label for="action_cause" class="control-label">Action</label>
      <input name="action_cause" type="text" id="action_cause" class="form-control input-sm" value="{{ old('action_cause') ?: @$data->action_cause }}"/>
      @foreach($errors->get('action_cause') as $msg)
          <span class="help-block">{{ $msg }}</span>
      @endforeach
    </div>
    @if(@$data->jenis_order == 3 || $data->jenis_order == 9)
    <div class="form-group {{ $errors->has('penyebab_id') ? 'has-error' : '' }}">
      <label for="penyebab_id" class="control-label">Penyebab</label>
      <input name="penyebab_id" type="text" id="penyebab_id" class="form-control input-sm" value="{{ old('penyebab_id') ?: @$data->penyebab_id }}"/>
      @foreach($errors->get('penyebab_id') as $msg)
          <span class="help-block">{{ $msg }}</span>
      @endforeach
    </div>
    @endif
    <div class="form-group">
      <label class="control-label" for="input-namaodp">Edit Nama Odp</label>
      <input name="nama_odp" type="text" id="input-namaodp" class="form-control input-sm" value="{{ old('nama_odp') ?: $data->odp_nama }}" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-koor">Koordinat</label>
      <input name="koordinat" type="text" id="input-koor" class="form-control input-sm" value="{{ old('koordinat') ?: @$data->koordinat }}" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-koor">Headline</label>
      <textarea name="headline" id="headline" class="form-control">{{ old('headline') ?: @$data->headline }}</textarea>
    </div>
    <div class="form-group">
      <label class="control-label" for="input-koor">Action</label>
      <textarea name="action" id="act" class="form-control">{{ old('action') ?: @$data->action }}</textarea>
    </div>
    
    <span style="font-size:10px;">
      <button data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info" type="button">
          <span class="glyphicon glyphicon-list"></span>
          Pekerjaan
        </button>
        <button data-toggle="modal" data-target="#material-alista" class="btn btn-sm btn-info" type="button">
          <span class="glyphicon glyphicon-list"></span>
          Material Alista
        </button>
      <br/>
      <br/>
      <div class="form-group">
        <ul id="material-list" class="list-group">
          <li class="list-group-item" v-repeat="$data | hasQty ">
            <span class="badge" v-text="qty"></span>
            <strong v-text="id_item"></strong>
            <p v-text="nama_item"></p>
          </li>
        </ul>
      </div>
    </span>

    <?php
      $ver = $data->verify;
      $decline = $ver-1;
      $approve = $ver+1;
      if($decline<1)
        $decline = null;
      // dd(session('auth'));
      $l = 1;
      $verifshow = 'hide';
      if(($l == 2 and $ver == null) or ($l == 3 and $ver == 1) or ($l == 4 and $ver == 2) or $l == 1)
        $verifshow = 'show';
    ?>
    <!--<div class="{{ (session('auth')->id_karyawan == '87142031' ? 'show' : (session('auth')->id_karyawan == '92140917' ? 'show' : ($data->niktl == session('auth')->id_karyawan ? 'show': 'hidden'))) }}" data-toggle="buttons">-->
    <div class="{{ $verifshow }}" data-toggle="buttons" style="margin-bottom: 5px;">
        <label class="btn btn-danger col-sm-6">
          <input type="radio" name="verify" autocomplete="off" value="{{ $decline }}">
          <strong class="glyphicon glyphicon-ok"></strong>
          Decline {{ $decline }}
        </label>

        <label class="btn btn-success col-sm-6">
          <input type="radio" name="verify" autocomplete="off" value="{{ $approve }}">
          <strong class="glyphicon glyphicon-ok"></strong>
          Approve {{ $approve }}
        </label>
    </div>
        <input type="text" class="form-control col-sm-6 {{ $verifshow }}" style="margin-top: 5px;margin-bottom: 5px;" placeholder="komen" name="catatan"/>
  <div class="table-responsive col-sm-12">
    <table class="table table-bordered table-striped">
      <tr>
        <th>NIK</th>
        <th>TANGGAL</th>
        <th>STATUS</th>
        <th>KOMENTAR</th>
      </tr>
      <tr>
        <td>{{ $data->created_by }}</td>
        <td>{{ $data->created_at }}</td>
        <td>{{ $data->order_from ? 'TOMMANPSB':'MANUAL' }}</td>
        <td>{{ $data->psbcreated }}</td>
      </tr>

      @foreach($dispatch_log as $dl)
        <tr>
          <td>{{ $dl->dispatch_by }}</td>
          <td>{{ $dl->ts_dispatch }}</td>
          <td>{{ $dl->action }}</td>
          <td>{{ $dl->regu_name }}</td>
        </tr>
      @endforeach
      @foreach($komen as $k)
        <tr>
          <td>{{ $k->pelaku }}</td>
          <td>{{ $k->ts }}</td>
          <td>{{ $k->status }}</td>
          <td>{{ $k->catatan }}</td>
        </tr>
      @endforeach
    </table>
    </div>
    </div>
    <input type="hidden" name="materials" value="[]" />
    <input type="hidden" name="saldo" value="[]" />
  </div>
</div>
   <div class="row col-sm-6 text-center">
  <div class="panel panel-default">
    <div class="panel-body">
      <label class="control-label"><b>Dokumentasi</b></label><br />
      <?php
        $number = 1;
        $col = 3;
        //echo "http://t1psb.tomman.info/upload/evidence/".$data->id_tbl_mj."/ODP.jpg";
        //if($data->psb_laporan_id){
        //if (file_exists("http://t1psb.tomman.info/upload/evidence/".$data->id_tbl_mj."/ODP.jpg")){
          //echo "http://t1psb.tomman.info/upload/evidence/".$data->id_tbl_mj."/ODP.jpg";
          //if($data->order_from){
            $folder="";
            if($data->order_from=="PSB")
              $folder = "evidence";
            else if($data->order_from=="ASSURANCE")
              $folder = "asurance";
            $odp = 'tommanpsb/'.$folder.'/'.$data->id_tbl_mj.'/ODP.jpg';
            if (file_exists($odp)){
              echo'<div class="col-xs-6 col-sm-6"><a href="/tommanpsb/'.$folder.'/'.$data->id_tbl_mj.'/ODP.jpg">
              <img src="/'.$odp.'"" style="width:100px;height:150px;" />
              </a><br/>FOTO ODP('.$data->order_from.')</div>';
            }
            $file = 'tommanpsb/'.$folder.'/'.$data->id_tbl_mj.'/ODP-th.jpg';
            if (file_exists($file)){
              echo'<div class="col-xs-6 col-sm-6"><a href="/tommanpsb/'.$folder.'/'.$file.'/Hasil_Ukur_OPM.jpg">
              <img src="/'.$file.'" style="width:100px;height:150px;" />
              </a><br/>FOTO Hasil_Ukur_OPM('.$data->order_from.')</div>';
            }
            $col = 4;
          //}
        //}

      ?>

    
       @foreach($foto as $input)
        <div class="col-xs-{{ $data->jenis_order==1?'4':'6' }} col-sm-{{ $data->jenis_order==1?'4':'6' }} text-center input-photos">
          <?php
            $path = "/upload3/maintenance/".Request::segment(3)."/$input";
            $path2 = "/upload2/maintenance/".Request::segment(3)."/$input";
            $th   = "$path-th.jpg";
            $th2   = "$path2-th.jpg";
            $img  = "$path.jpg";
            $img2  = "$path2.jpg";
            $flag = "";
            $name = "flag_".$input;
          ?>
          @if (file_exists(public_path().$th))
            <a href="{{ $img }}">
              <img src="{{ $th }}?x={{ filemtime(public_path().$th) }}" alt="{{ $input }}" />
            </a>
            <?php
              $flag = 1;
            ?>
          @elseif(file_exists(public_path().$th2))
            <a href="{{ $img2 }}">
              <img src="{{ $th2 }}?x={{ filemtime(public_path().$th2) }}" alt="{{ $input }}" />
            </a>
            <?php
              $flag = 1;
            ?>
          @else
            <img src="/image/placeholder.gif" alt="" />
          @endif
          <br />
          <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
          <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
          <button type="button" class="btn btn-sm btn-info">
            <i class="glyphicon glyphicon-camera"></i>
          </button>
          <p style="font-size: 9px;">{{ str_replace('_',' ',$input) }}</p>
          {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
        </div>
      <?php
        $number++;
      ?>
      @endforeach
    </div>
    <div style="margin:40px 0 20px" class="text-center">
      <button class="btn btn-primary">Simpan</button>
    </div>
    <br />
  </form>
  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #eee">
          <strong style="color:black;">Laporan Material</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 450px">
          <input id="searchinput" class="form-control input-sm" type="search" placeholder="Search..." />
          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">
              <strong v-text="id_item"></strong>
              <div class="input-group" style="width:150px;float:right;">
                <span class="input-group-btn">
                  <button class="btn btn-default btn-sm" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <!-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit" disabled></button> -->
                <input v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center input-sm" />
                <span class="input-group-btn">
                  <button class="btn btn-default btn-sm" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
              <p v-text="nama_item" style="font-size:10px;"></p>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          
        </div>
      </div>
    </div>
  </div>
  <div id="material-alista" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #eee">
          <strong style="color:black;">Laporan Material Alista</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 450px">
          <input id="searchinput" class="form-control input-sm" type="search" placeholder="Search..." />
          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">
              <strong v-text="id_item"></strong>
              <div class="input-group" style="width:150px;float:right;">
                <span class="input-group-btn">
                  <button class="btn btn-default btn-sm" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <!-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit" disabled></button> -->
                <input v-model="pemakaian" style="border-top: 1px solid #eeeeee" class="form-control text-center input-sm"/>
                <span class="input-group-btn">
                  <button class="btn btn-default btn-sm" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
              <p v-text="rfc" style="font-size:10px;"></p>
              <p style="font-size:10px;">In-Tech=<b v-text="sisa"></b>;Pemakaian=<b v-text="pemakaian"></b>;</p>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          
        </div>
      </div>
    </div>
  </div>
    </div>
  </div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="/bower_components/select2/select2.min.js"></script>
  <script>
  $(function() {
    $('.input_remo').hide();
    $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);
            
          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });
    var materials = <?= json_encode($materials) ?>;

      Vue.filter('hasQty', function(value) {
        return value.filter(function(a) { return a.qty > 0 });
      });

      Vue.filter('doubleDigit', function(value) {
        var v = Number(value);
        if (v < 1) return '00';
        else if (String(v).length < 2) return '0' + v;
      });

      var listVm = new Vue({
        el: '#material-list',
        data: materials
      });

      var modalVm = new Vue({
        el: '#material-modal',
        data: materials,
        methods: {
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });
      var saldo = <?= json_encode($saldo) ?>;
      var modalVmSaldo = new Vue({
        el: '#material-alista',
        data: saldo,
        methods: {
          onPlus: function(item) {
            if (item.sisa==null || item.sisa==0){
              item.sisa=0;
            }else{
              item.pemakaian++;
              item.sisa--;
            }
          },
          onMinus: function(item) {
            if (!item.pemakaian){
              item.pemakaian = 0;
            }else{
              item.pemakaian--;
              item.sisa++;
            }
          }
        }
      });
    $('#submit-form').submit(function(event) {
        var result = [];
        materials.forEach(function(item) {
          if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty});
        });
        var mtr = [];
        var isSaldoAdd = 0;
        saldo.forEach(function(item) {
          if (item.pemakaian > 0) {
            mtr.push(item);
            isSaldoAdd = 1;
          }
        });
        /*
        if($('#input-status').val() == "close" && !isSaldoAdd){
          event.preventDefault();
          alert("silahkan isi material Alista!!");
          return  false;
        }*/ 
        $('input[name=materials]').val(JSON.stringify(result));
        $('input[name=saldo]').val(JSON.stringify(mtr));
        $('.wait-indicator').show();
      });
    var level = <?= json_encode(session('auth')->level) ?>;
    var sts = $('#input-status').select2({
        data: [{"id":"close", "text":"Close"},{"id":"kendala pelanggan", "text":"Kendala Pelanggan"},{"id":"kendala teknis", "text":"Kendala Teknis"},{"id":"ogp", "text":"OGP"},{"id":"cancel order", "text":"Cancel Order"}],
        placeholder: 'Input status'
      });

    var day = {
      format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
    };
    $("#input-tgl").datepicker(day).on('changeDate', function (e) {
        $(this).datepicker('hide');
      });
    $("#searchinput").keyup(function() {
      setTimeout(function(){
        var input, filter, ul, li, a, i;
        input = document.getElementById("searchinput");
        filter = input.value.toUpperCase();
        ul = document.getElementById("searchlist");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("p")[0];
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";

            }
        }
      }, 2000);
    },);
    var action_cause = <?= json_encode($action_cause) ?>;
    var ac = $('#action_cause').select2({
        data: action_cause,
        minimumResultsForSearch: Infinity
    });
    var penyebab = <?= json_encode($penyebab) ?>;
    var pb = $('#penyebab_id').select2({
        data: penyebab,
        minimumResultsForSearch: Infinity
    });
    var splitter = [{"id":"1:8", "text":"1:8"},{"id":"1:16", "text":"1:16"},{"id":"1:8+1:2", "text":"1:8+1:2"},{"id":"1:8+1:4", "text":"1:8+1:4"},{"id":"1:8+1:8", "text":"1:8+1:8"},{"id":"1:4+1:4+1:8", "text":"1:4+1:4+1:8"},
{"id":"1:8+1:8+1:4", "text":"1:8+1:8+1:4"},
{"id":"1:8+1:8+1:4", "text":"1:2+1:2+1:8"},
{"id":"1:8+1:8+1:2", "text":"1:8+1:8+1:2"}];
    var spl = $('#splitter').select2({
        data: splitter,
        minimumResultsForSearch: Infinity
    });
    $("#input-status").change(function(){
      $.getJSON("/getActionCause/"+$("#input-status").val(), function(data){
          ac.select2({data:data,minimumResultsForSearch: Infinity});
      });
    });
    $("#ukur").click(function(){
      $('.wait-indicator').show();
      $.getJSON("/ibooster/"+$("#input-speedy").val(), function(data){
        $('#red').html(data[0].ONU_Rx);
        $('.input_remo').show();
        $('#ibooster').val(data[0]);
        if(data[0].ONU_Rx >2 || data[0].ONU_Rx <-24){
          console.log("hapus close");
          sts.select2({data: [{"id":"kendala pelanggan", "text":"Kendala Pelanggan"},{"id":"kendala teknis", "text":"Kendala Teknis"}]});
        }
        $('.wait-indicator').hide();
      });
    });
  });
  </script>

@endsection
