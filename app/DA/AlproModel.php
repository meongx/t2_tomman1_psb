<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class AlproModel{

  public static function dashboard(){
    return DB::SELECT('SELECT a.nik, b.nama, count(*) as jumlah,b.*, a.nik as nik_al from alpro_owner a left join 1_2_employee b ON a.nik = b.nik GROUP BY a.nik ORDER BY jumlah');
  }

  public static function all_alpro($witel){
    return DB::table('1_0_master_odp')->where('WITEL',$witel)->get();
  }

  public static function all_alpro_owner($p){
    return DB::table('alpro_owner')->where('status',$p)->get();
  }


  public static function get_alpro($chatId){
    return DB::SELECT('SELECT * FROM alpro_owner a LEFT JOIN 1_0_master_odp b ON a.alproname = b.ODP_NAME WHERE a.chatId = "'.$chatId.'"');
  }
  public static function get_alproname($req){
    $param = $req->input('param');
    return DB::SELECT('SELECT a.ODP_NAME as id, a.ODP_NAME as text,b.* from 1_0_master_odp a LEFT JOIN alpro_owner b ON a.ODP_NAME = b.alproname WHERE a.WITEL = "KALSEL" AND a.ODP_NAME LIKE "%'.$param.'%"');
  }
  public static function book_alproname($book,$nik,$chatId){
    if (count($book)>0){
      for($i=0;$i<count($book);$i++){
        $check = DB::table('alpro_owner')->where('alproname',$book[$i])->where('status','<>','BOOKED')->get();
        if (count($check)>0){
          $query = DB::table('alpro_owner')->where('alproname',$book[$i])->update([
            'status' => 'BOOKED',
            'date_created' => date('Y-m-d H:i:s'),
            'nik' => $nik,
            'chatId'  => $chatId
          ]);
        } else {
          $query = DB::table('alpro_owner')->insert([
            'alproname' => $book[$i],
            'nik' => $nik,
            'status' => 'BOOKED',
            'date_created' => date('Y-m-d H:i:s'),
            'chatId'  => $chatId
          ]);
        }
      }
      return true;
    } else {
      return false;
    }
  }

  public static function getChartId($nik){
      $sql = 'SELECT * from group_telegram WHERE title LIKE "%TERRITORY%" AND (TL_NIK="'.$nik.'" OR Nik_Atl="'.$nik.'")';
      return DB::select($sql);
  }
}
