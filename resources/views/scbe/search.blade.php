@extends('layout')

@section('content')
@include('partial.alerts')
 <h4>Cari MYIR</h4>
  <form class="row" style="margin-bottom: 20px">
      <div class="col-md-12 {{ $errors->has('scbeq') ? 'has-error' : '' }}">
          <div class="input-group">
            <input type="text" class="form-control" name="scbeq"/>
              <span class="input-group-btn">
                <button class="btn btn-primary" type="submit">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
          </div>
          {!! $errors->first('scbeq','<p class=help-block>:message</p>') !!}
      </div>
  </form>

  <a href="/scbe/input" class="btn btn-primary btn-sm">Dispatch Manual</a>

  @if($list<>NULL)
      <br /> <br />
      <table class="table table-striped table-bordered dataTable">
          <tr>
              <td width="1%">#</td>
              <td>
                  @if($myirAda==0)
                      @if ($list['ket']=='0')  
                          <a href="/scbe/edit/{{ $list['idMyir'] }}" class="label label-info">Re-Dispatch</a>
                          <a href="/dshr/plasa-sales/input-sc/{{ $list['myir'] }}" class="label label-success">ADD SC</a>
                      @else
                          <span class="label label-info">Sudah Terdispatch Ke SC {{ $list['Ndem'] }}</span>
                      @endif

                      @if($sc==1 && $ket==0)
                          <a href="/scbe/sinkronsc/{{ $list['myir'] }}" class="label label-success">Syncron SC</a>
                      @endif

                      <span class="label label-warning">{{ $list['uraian'] }}</span>
                      <span class="label label-default">{{ $list['tgl'] }}</span>
                    
                      @if ($list['laporan_status_id']=='1')
                          <span class="label label-success">{{ $list['laporan_status'] }}</span>
                      @else
                          <span class="label label-primary">{{ $list['laporan_status'] ?: 'NO UPDATE' }}</span>
                      @endif

                      @if ($ket=='0')
                          <a href="/scbe/sinkronsc/{{ $list['myir'] }}/hapus" class="label label-danger">Hapus</a>
                      @endif
                  @else
                      @if($list['ketInput']==0)
                        <a href="/dshr/plasa-sales/dispatch/plasa/{{ $list['myir'] }}" class="label label-info">Dispatch</a>
                      @else
                        <a href="/dshr/plasa-sales/dispatch/sales/{{ $list['myir'] }}" class="label label-info">Dispatch</a>
                      @endif
                      
                      <!-- <a href="/scbe/edit/{{ $list['idMyir'] }}" class="label label-danger">Hapus</a> -->
                        
                  @endif
              </td>
          </tr>

          <tr>
              <td>1</td>
              <td>
                  @if($myirAda==0)
                    <b>MYIR : </b> <a href="/{{ $list['id'] }}">{{ $list['myir'] }}</a> <br>
                  @else
                    <b>MYIR : </b>{{ $list['myir'] }}<br>
                  @endif
                  <b>Pelanggan : </b> {{ $list['customer'] }} <br>
                  <b>Jenis Layanan : </b> {{   $list['jenis_layanan'] }} <br>
                  <b>Koordinat Pelanggan : </b> {{ $list['kordinatPel'] }} <br>
                  <b>Alamat Sales : </b> {{ $list['alamatSales'] }} <br>
                  <b>ODP By Sales : </b> {{ $list['namaOdp'] }} <br>
                  <b>ODP By Teknisi : </b> {{ $list['odpByTeknisi'] }}
                  
                  @if ($ket=='1')
                      <br><b>MYIR : </b>MYIR-{{ $list['myir'] }}
                  @endif

                  <br><b>PIC Pelangan : </b>{{ $list['picPelanggan'] }}
                  <br><b>No.Internet : </b>{{ $list['no_internet'] }}
                  <br><b>No.Telp : </b>{{ $list['no_telp'] }}

              </td>
          </tr>
      </table>
  @endif



@endsection