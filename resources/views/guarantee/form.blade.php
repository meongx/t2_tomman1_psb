@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <div class="col-md-12">
        <h3>
          <a href="/" class="btn btn-sm btn-default">
            <span class="glyphicon glyphicon-arrow-left"></span>
          </a>
          Re-Dispatch FG {{ $data->Ndem }}
        </h3>
    </div>

    <div class="form-group col-md-12">
      <label class="control-label" for="input-regu">Loker</label>
      <input type="text" name="loker" value="FULFILLMENT GUARANTEE" class="form-control" readonly>
      {{-- <input name="loker" type="hidden" id="input-loker" class="form-control" /> --}}
    </div>
    
    <div class="form-group col-md-12">
      <label class="control-label" for="input-regu">Alpro</label>
      <input  type="text" name="alpro" class="form-control" value="{{ $data->alpro }}" readonly>
      {{-- <input name="alpro" type="hidden" id="input-alpro" class="form-control" /> --}}
    </div>

    <div class="form-group col-md-12">
      <label class="control-label" for="input-regu">Regu</label>
      <input name="id_regu" type="hidden" id="input-regu" class="form-control" />
    </div>
    
    <div class="form-group col-md-12">
      <label class="control-label" for="input-tgl">Tanggal</label>
      <input name="tgl" type="tgl" id="input-tgl" class="form-control" />
    </div>

    <div class="form-group col-md-2 {{ $errors->has('tglOpen') ? 'has-error' : '' }}">
        <label class="control-label" for="tglOpen">Tanggal Open</label>
        <input name="tglOpen" type="tgl" id="tglOpen" class="form-control" />  
        {!! $errors->first('tglOpen','<p class=help-block>:message</p>') !!}
    </div>

    <div class="form-group col-md-2 {{ $errors->has('jamOpen') ? 'has-error' : '' }}">
         <div class="input-group bootstrap-timepicker timepicker">
            <label class="label-control">Jam Open</label>
            <input id="jamOpen" name="jamOpen" type="text" class="form-control input-small" readonly>
            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
            {!! $errors->first('jamOpen','<p class=help-block>:message</p>') !!}
        </div>
    </div>

    <div style="margin:40px 0 20px" class="col-md-12">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>


  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script>
    $(function() {
      var dataloker = [
        {"id":"1", "text":"CONSUMER"},
        {"id":"2", "text":"CORPORATE"}
        ];

    var loker = function() {
      return {
        data: dataloker,
        placeholder: 'Input Loker',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }

    $('#input-loker').select2(loker());


    var dataloker = [
      {"id":"1", "text":"CONSUMER"},
      {"id":"2", "text":"CORPORATE"}
      ];

    var loker = function() {
      return {
        data: dataloker,
        placeholder: 'Input Loker',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#input-loker').select2(loker());

    var dataalpro = [
      {"id":"COPPER", "text":"COPPER"},
      {"id":"GPON", "text":"GPON"}
      ];

    var alpro = function() {
      return {
        data: dataalpro,
        placeholder: 'Input Alpro',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#input-alpro').select2(alpro());

      $('.btn-danger').click(function() {
        var sure = confirm('Yakin hapus data ?');
        if (sure) {
          $('#delete-form').submit();
        }
      })
      var state= <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Input Regu',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.title+'</span> '+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-regu').select2(regu());
      
      var day = {
        format : 'yyyy-mm-dd',
        viewMode: 0,
        minViewMode: 0
      };
 
      $('#input-tgl').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });

      $('#tglOpen').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });

      $('#jamOpen').timepicker({
          showMeridian : false,
          disableMousewheel : false,
          minuteStep : 1
      });

    });

  </script>
@endsection
