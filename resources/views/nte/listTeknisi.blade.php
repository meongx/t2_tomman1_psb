@extends('layout')

@section('content')
  @include('partial.alerts')
  <a href="/nte" class="btn btn-sm btn-default">
    	<span class="glyphicon glyphicon-arrow-left"></span>
  	</a>
  	<br />
  	<br />
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading"></div>
  			<div class="panel-body">
  				<table class="table">
  					<tr>
  						<th>NO</th>
  						<th>TANGGAL KELUAR</th>
              <th>Teknisi</th>
              <th>Program</th>
  						<th>JENIS NTE</th>
  						<th>SN</th>
              <th>WO</th>
              <th>No. Inet</th>
              <th>Pelanggan</th>
              <th>Alamat Pelanggan</th>
  						<th>PETUGAS</th>
  						<th>ID</th>
  					</tr>
  					@foreach ($query as $num => $result)
  					<tr>
  						<td>{{ ++$num }}</td>
  						<td>{{ $result->tanggal_keluar }}</td>
  						<td>{{ $result->position_key }}</td>
              <td>{{ $result->jenis_layanan ? : 0 }}</td>
  						<td>{{ $result->jenis_nte }}</td>
  						<td>{{ $result->sn }}</td>
              <td>{{ $result->Ndem }}</td>
              <td>{{ $result->internet ?: $result->Service_ID }}</td>
              <td>{{ $result->orderName ?: $result->Customer_Name }}</td>
              <td>{{ $result->orderAddr }}</td>
  						<td>{{ $result->petugas }}</td>
  						@if ($result->id_tbl_mj <> 0)
  						<td><a href="/{{ $result->id_tbl_mj }}">{{ $result->id_tbl_mj }}</a></td>
  						@else
  						<td>{{ $result->id_tbl_mj }}</td>
  						@endif
  					</tr>
  					@endforeach
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection 