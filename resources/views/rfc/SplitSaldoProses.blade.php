@extends('layout')

@section('content')
@include('partial.alerts')  
  <div class="panel panel-primary">
    <div class="panel-heading">Split Saldo DC</div>
    <div class="panel-body">
        <form method="POST">
            <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="rfc">NO. RFC</label>
                      <input type="text" name="rfc" class="form-control" value="{{ $inputan['rfc'] }}">
                      {!! $errors->first('rfc','<div class="label label-danger">:message</div>') !!}
                  </div>              
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="rfc">Gudang</label>
                      <select name="gudang" id="gudang" class="form-control">
                        @foreach($gudang as $g)
                          @if ($g==$inputan['gudang']) 
                            <option value="{{ $g }}" selected>{{ $g }}</option>
                          @else
                            <option value="{{ $g }}">{{ $g }}</option>
                          @endif
                        @endforeach
                      </select>
                      {!! $errors->first('gudang','<div class="label label-danger">:message</div>') !!}
                  </div>
                </div>

                  <div class="col-md-12">
                      <input type="submit" value="Proses" class="btn btn-primary">
                  </div>
            </div>
        </form>
    </div>
  </div>

  <div class="panel panel-primary">
      <div class="panel-heading">Split DC RFC : {{ $inputan['rfc'] }} | Jumlah DC : {{ $getData->jumlah }}</div>
      <div class="panel-body">
          <form method="POST" action="{{ route('split.proses') }}">
              <input type="hidden" name="jumlah" value="{{ $getData->jumlah }}">
              <input type="hidden" name="rfc" value="{{ $getData->no_rfc }}">
              <input type="hidden" name="gudang" value="{{ $getData->nama_gudang }}">
              <input type="hidden" name="alista_id" value="{{ $getData->alista_id }}">

              <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                        <div class="col-md-12">
                            <label for="split1">Split 1</label>
                            <input type="text" name="split1" class="form-control split" value="{{ $dataSplit[0] ?: '0' }}">
                        </div>

                         <div class="col-md-12">
                            <label for="split2">Split 2</label>
                            <input type="text" name="split2" class="form-control split" value="{{ $dataSplit[1] ?: '0' }}">
                        </div>

                         <div class="col-md-12">
                            <label for="split3">Split 3</label>
                            <input type="text" name="split3" class="form-control split" value="{{ $dataSplit[2] ?: '0' }}">
                        </div>

                         <div class="col-md-12">
                            <label for="split4">Split 4</label>
                            <input type="text" name="split4" class="form-control split" value="{{ $dataSplit[3] ?: '0' }}">
                        </div>

                         <div class="col-md-12">
                            <label for="split5">Split 5</label>
                            <input type="text" name="split5" class="form-control split" value="{{ $dataSplit[4] ?: '0' }}">
                        </div>
                      </div>
                  </div>

                  <div class="col-md-6">
                      <div class="form-group">
                        <div class="col-md-12">
                            <label for="split6">Split 6</label>
                            <input type="text" name="split6" class="form-control split" value="{{ $dataSplit[5] ?: '0' }}">
                        </div>

                         <div class="col-md-12">
                            <label for="split7">Split 7</label>
                            <input type="text" name="split7" class="form-control split" value="{{ $dataSplit[6] ?: '0' }}">
                        </div>

                         <div class="col-md-12">
                            <label for="split8">Split 8</label>
                            <input type="text" name="split8" class="form-control split" value="{{ $dataSplit[7] ?: '0' }}">
                        </div>

                         <div class="col-md-12">
                            <label for="split9">Split 9</label>
                            <input type="text" name="split9" class="form-control split" value="{{ $dataSplit[8] ?: '0' }}">
                        </div>

                         <div class="col-md-12">
                            <label for="split10">Split 10</label>
                            <input type="text" name="split10" class="form-control split" value="{{ $dataSplit[9] ?: '0' }}">
                        </div>
                      </div>
                  </div>
              </div>
              <br>
              <div class="col-md-12">
                  <div class="form-group">
                      <input type="submit" class="btn btn-primary" value="Simpan">
                  </div>
              </div>
          </form>
      </div>
  </div>
@endsection

@section('plugins')
  <script type="text/javascript">
      $(function() {
          $("#gudang").select2();

          let data = [
                  {"id":"0", "text":"0"},
                  {"id":"100", "text":"100"},
                  {"id":"150", "text":"150"},
                  {"id":"200", "text":"200"},
                  {"id":"250", "text":"250"},
                  {"id":"300", "text":"300"},
                  {"id":"350", "text":"350"},
              ];

          $(".split").select2({data:data});

          var vm = new Vue({
              
          })
      });
  </script>
@endsection
