<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
use App\DA\DashboardModel;
use App\DA\QcondeskModel;
use App\DA\HomeModel;
class QcondeskController extends Controller
{
  protected $assuranceInputs = [
    'Label','Foto_Action','ODP', 'Hasil_Ukur_OPM' ,'Berita_Acara', 'Rumah_Pelanggan', 'Test_Layanan_Internet', 'Test_Layanan_TV', 'Test_Layanan_Telepon', 'MODEM', 'SN_ONT_RUSAK', 'Sambungan_Kabel_DC', 'Pullstrap_S_Clamp', 'Stoper', 'Clamp_Hook_Clamp_Pelanggan', 'Roset', 'Patchcord', 'Slack_Kabel_DC','Foto_Letak_Gangguan'
  ];
  protected $provisioningInputs = [
    'Foto_Action','Lokasi', 'ODP', 'Redaman_ODP', 'Redaman_Pelanggan', 'Instalasi_Kabel', 'SN_ONT', 'SN_STB', 'Live_TV',
    'TVOD','RFC_Form', 'Speedtest', 'Berita_Acara', 'Telephone_Incoming', 'Telephone_Outgoing', 'Wifi_Analyzer', 'Foto_Pelanggan_dan_Teknisi', 'Pullstrap', 'Tray_Cable', 'K3','KLEM_S', 'LABEL','Excheck_Helpdesk','Patchcore','Stopper','Breket','Roset','BA_Digital'
  ];


  public function datediff($start,$end){
    $start = new DateTime(($start));
    $end = new DateTime(($end));
    $interval = $start->diff($end);
    return $interval->h + ($interval->days*24);
  }

  public function trigger(){
    $query = DB::SELECT('
      SELECT
        *,
        c.id as idpsb
      FROM
        qcondesk a
      LEFT JOIN dispatch_teknisi b ON a.qcondesk_order = b.id
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
    ');
    foreach ($query as $result) {
      DB::table('psb_laporan')->where('id_tbl_mj',$result->qcondesk_order)
      ->update([
        'modified_at'   => $result->qcondesk_datetime,
        'modified_by'   => $result->qcondesk_by,
        'catatan'       => $result->qcondesk_alasan." [QCONDESK]",
        'status_laporan'=> 6,
        'action'        => 35,
        'penyebabId'    => 41
      ]);
      DB::transaction(function() use($result) {
         DB::table('psb_laporan_log')->insert([
           'created_at'      => $result->qcondesk_datetime,
           'created_by'      => $result->qcondesk_by,
           'status_laporan'  => 6,
           'id_regu'         => $result->id_regu,
           'catatan'         => $result->qcondesk_alasan." [QCONDESK]",
           'action'          => 35,
           'Ndem'            => $result->Ndem,
           'psb_laporan_id'  => $result->idpsb,
           'penyebabId'      => 41
         ]);
       });
    }
  }

  public function reject($id){
    $order = QcondeskModel::order($id)[0];
    if (substr($order->Ndem,0,2)=="IN"){
      $photo = $this->assuranceInputs;
      $folder = "asurance";
    } else {
      $photo = $this->provisioningInputs;
      $folder = "evidence";
    }
    return view('qcondesk.reject',compact('id','photo','order','folder'));
  }

  public function rejectSave(Request $req){
    $auth = session('auth');
    $save = QcondeskModel::rejectSave($req,$auth);
    if ($save){
      return back()->with('alerts', [
        ['type' => 'success', 'text' => 'Order berhasil di-reject']
      ]);
    } else {
      return back()->with('alerts', [
        ['type' => 'danger', 'text' => 'Terjadi kesalahan dalam penyimpanan']
      ]);

    }
  }

  public function home($periode, $status){
    $nik = session('auth')->id_user;
    $group_telegram = HomeModel::group_telegram($nik);

    if ($status==1){
      $get_list_TL = QcondeskModel::list_TL($periode,$nik);
      
      $folder = "asurance";
      $photoInputs = $this->assuranceInputs;
      $order = "ASR";

      $data = array();
      foreach ($get_list_TL as $list_TL){
        foreach ($photoInputs as $input){
          $pathx = public_path()."/upload3/".$folder."/".$list_TL->id_dt."/$input.jpg";
          //echo $pathx;
          if (file_exists($pathx)){
            //echo $pathx."<br />";
            if (@exif_read_data($pathx))
            $data[$list_TL->id_dt][$input] = @exif_read_data($pathx);
          } else {
            $data[$list_TL->id_dt][$input] = array("DateTimeOriginal"=>'');
          }
        }
      }
    }
    else{
      $get_list_TL = QcondeskModel::list_TL_Prov($periode,$nik);
     
      $folder = "evidence";
      $photoInputs = $this->provisioningInputs;
      $order = "PROV";

      $data = array();
      foreach ($get_list_TL as $list_TL){
        foreach ($photoInputs as $input){
          $pathx = public_path()."/upload3/".$folder."/".$list_TL->id_dt."/$input.jpg";
          //echo $pathx;
          if (file_exists($pathx)){
            //echo $pathx."<br />";
            if (@exif_read_data($pathx))
            $data[$list_TL->id_dt][$input] = @exif_read_data($pathx);
          } else {
            $data[$list_TL->id_dt][$input] = array("DateTimeOriginal"=>'');
          }
        }
      }
    }   

    return view('qcondesk.home',compact('periode','nik','group_telegram','get_list_TL','data', 'photoInputs', 'order', 'folder'));
  }
}
?>
