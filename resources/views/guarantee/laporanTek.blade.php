@extends('layout')

@section('content')
  @include('partial.alerts')
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    @if (isset($data->id_pl))
      <input type="hidden" name="id" value="{{ $data->id_pl }}" />
    @endif
    <h3>
      <a href="/" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      @if(session('auth')->level <> 19)
        <button class="btn btn-primary">Simpan</button>
      @endif
      UPDATE SC
    </h3>

    <input type="hidden" name="no_tiket" value="{{ $project->Ndem }}" />
<div class="row">
<div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        Workorder Information
      </div>
      <div class="panel-body">

    <div class="form-group">
      <label class="control-label" for="input-status">Status</label>
      <input name="status" type="hidden" id="input-status" class="form-control" value="{{ $data->status_laporan or '' }}" />
    </div>

    <div class="form-group {{ $errors->has('penyebab') ? 'has-error' : '' }}">
      <label class="control-label" for="input-penyebab">Penyebab</label>
      <input name="penyebab" type="hidden" id="input-penyebab" class="form-control" value="{{ $data->penyebabId or '' }}" />
      {!! $errors->first('penyebab', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
      <label class="control-label" for="input-action">Action</label>
      <input name="action" type="hidden" id="input-action" class="form-control" value="{{ $data->action or '' }}" />
      {!! $errors->first('action', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
        <label class="label-control">No. Telpon</label>
        <input type="text" name="noTelp" class="form-control" value="{{ $project->noTelp }}">
        {!! $errors->first('noTelp', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
      <label class="control-label"><b>Catatan Pemasangan</b></label>
      <textarea name="catatan" class="form-control" rows="5">{{ $data->catatan or '' }}</textarea>
    </div>

		  <div class="form-group">
        <label for="" class="control-label"><button id="btn-gps" type="button" class="btn btn-sm btn-info">
          <i class="glyphicon glyphicon-map-marker"></i>
        </button> <b>Koordinat Pelanggan</b></label>
        <div>
          <input name="kordinat_pelanggan" id="kordinat_pelanggan" class="form-control" rows="1" value="{{ $data->kordinat_pelanggan or '' }}" />
        </div>
      </div>
      <div class="col-xs-12">{!! $errors->first('kordinat_pelanggan', '<span class="label label-danger">:message</span>') !!}</div>
		  <div class="form-group">
        <label class="control-label"><b>Nama ODP</b></label>
        <input name="nama_odp" class="form-control" id="nama_odp" rows="1" value="{{ $data->nama_odp or '' }}">
        {!! $errors->first('nama_odp', '<span class="label label-danger">:message</span>') !!}
      </div>
      <div class="form-group">
		    <label for="" class="control-label">
          <button id="btn-gps-odp" type="button" class="btn btn-sm btn-info">
            <i class="glyphicon glyphicon-map-marker"></i>
          </button>
          <b>Koordinat ODP</b>
        </label>
        <input name="kordinat_odp" id="kordinat_odp" class="form-control" rows="1" value="{{ $data->kordinat_odp or '' }}" />
      </div>
      <div class="col-xs-12">{!! $errors->first('kordinat_odp', '<span class="label label-danger">:message</span>') !!}</div>
    </div>
</div>
</div>
  <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">Material & NTE</div>
        <div class="panel-body">
          <input type="hidden" name="materials" value="[]" />
          @if(session('auth')->level <> 19)
            <button data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info" type="button">
              <span class="glyphicon glyphicon-list"></span>
              Edit
            </button>
          @endif
          <div class="form-group">
            <label class="control-label" for="input-ont">TYPE ONT</label>
              <input type="text" name="type_ont" id="input-type-ont" class="form-control" value="{{ $project->typeont ? : '' }}"/>
          </div>
          <div class="form-group">
            <label class="control-label" for="sn_ont">SN ONT</label>
            <input type="text" name="sn_ont" id="sn_ont" class="form-control" value="{{ $project->snont }}"/>
            {!! $errors->first('sn_ont', '<span class="label label-danger">:message</span>') !!}
          </div>
          <div class="form-group">
            <label class="control-label" for="input-ont">TYPE STB</label>
            <input type="text" name="type_stb" id="input-type-stb" class="form-control" value="{{ $project->typestb ? : '' }}"/>
          </div>
          <div class="form-group">
            <label class="control-label" for="input-ont">SN STB</label>
            <input type="text" name="sn_stb" id="input-sn-stb" class="form-control" value="{{ $project->snstb }}"/>
          </div>
          <ul id="material-list" class="list-group">
            <li class="list-group-item" v-repeat="$data | hasQty ">
              <span class="badge" v-text="qty"></span>
              <strong v-text="id_item"></strong>
              <p v-text="nama_item"></p>
              <p v-text="rfc"></p>
            </li>
          </ul>
        </div>
      </div>
  </div>

  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">Dokumentasi</div>
      <div class="panel-body">
        <div class="row text-center input-photos" style="margin: 20px 0">
           @foreach($photoInputs as $input)
            <div class="col-xs-6 col-sm-2">
              <?php
                $path = "/upload3/asurance/{$project->id_dt}/$input";
                $th   = "$path-th.jpg";
                $img  = "$path.jpg";
                $flag = "";
              ?>
              @if (file_exists(public_path().$th))
                <a href="{{ $img }}">
                  <img src="{{ $th }}" alt="{{ $input }}" />
                </a>
                <?php
                  $flag = 1;
                ?>
              @else
                <img src="/image/placeholder.gif" alt="" />
              @endif
              <br />
              <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
              <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />

              <button type="button" class="btn btn-sm btn-info">
                <i class="glyphicon glyphicon-camera"></i>
              </button>
              <p>{{ str_replace('_',' ',$input) }}</p>
            </div>
          @endforeach
        </div>
      </div>
    </div>
	@if(session('auth')->level <> 19)
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary form-control">Simpan</button>
    </div>
	@endif
  <br />
  </form>
</div>
  <br />
  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Material PSB</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <div class="form-group">
            <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
          </div>

          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">

              <strong v-text="id_item"></strong>
              <p v-text="nama_item"></p>
              <p v-text="rfc"></p>

              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <!-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit" disabled></button> -->
                <input v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('plugins')
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
    $(function() {
      $("#nama_odp").inputmask("AAA-AAA-A{2,3}/999");
      var materials = <?= json_encode($materials) ?>;

      Vue.filter('hasQty', function(value) {
        return value.filter(function(a) { return a.qty > 0 });
      });

      Vue.filter('doubleDigit', function(value) {
        var v = Number(value);
        if (v < 1) return '00';
        else if (String(v).length < 2) return '0' + v;
      });

      var listVm = new Vue({
        el: '#material-list',
        data: materials
      });

      var modalVm = new Vue({
        el: '#material-modal',
        data: materials,
        methods: {
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;

            if (item.qty > item.saldo) item.qty = item.qty-1;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });

      var $btnGps = $('#btn-gps');
      var $btnGpsODP = $('#btn-gps-odp');

      var $kordinat_pelanggan = $('#kordinat_pelanggan');
      $btnGps.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_pelanggan.val('ERROR');
          return;
        }

        $kordinat_pelanggan.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
	        $kordinat_pelanggan.val(result.coords.latitude+','+result.coords.longitude);
/*
					$labelGps.val('(' + Math.round(result.coords.accuracy) + 'm) ' + result.coords.latitude + ' ' + result.coords.longitude);

          $('input[name=gps_accuracy]').val(result.coords.accuracy);
          $('input[name=gps_latitude]').val(result.coords.latitude);
          $('input[name=gps_longitude]').val(result.coords.longitude);
*/
        }, function(error) {
          $kordinat_pelanggan.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert('Tidak mendapat izin menggunakan GPS');
              break;

            case error.POSITION_UNAVAILABLE:
              alert('Gagal menghubungi satelit GPS');
              break;

            case error.TIMEOUT:
              $kordinat_pelanggan.val('ERROR: TIMEOUT');
              break;
          }
        });
      });
      var $kordinat_odp = $('#kordinat_odp');
      $btnGpsODP.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_odp.val('ERROR');
          return;
        }

        $kordinat_odp.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
	        $kordinat_odp.val(result.coords.latitude+','+result.coords.longitude);
/*
					$labelGps.val('(' + Math.round(result.coords.accuracy) + 'm) ' + result.coords.latitude + ' ' + result.coords.longitude);

          $('input[name=gps_accuracy]').val(result.coords.accuracy);
          $('input[name=gps_latitude]').val(result.coords.latitude);
          $('input[name=gps_longitude]').val(result.coords.longitude);
*/
        }, function(error) {
          $kordinat_odp.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert('Tidak mendapat izin menggunakan GPS');
              break;

            case error.POSITION_UNAVAILABLE:
              alert('Gagal menghubungi satelit GPS');
              break;

            case error.TIMEOUT:
              $kordinat_odp.val('ERROR: TIMEOUT');
              break;
          }
        });
      });

      $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);
          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });

      $('#submit-form').submit(function() {
        var result = [];
        materials.forEach(function(item) {
          if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty, rfc: item.rfc});
        });
        $('input[name=materials]').val(JSON.stringify(result));
      });

      $('.modal-body').css({ maxHeight: window.innerHeight - 170 });

      var data = <?= json_encode($get_laporan_status) ?>;
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Status',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-status').select2(select2Options());

      var dataAction = <?= json_encode($get_laporan_action) ?>;
      var selectDataAction = function() {
        return {
          data: dataAction,
          placeholder: 'Input Action',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-action').select2(selectDataAction());

      var dataAction = <?= json_encode($get_laporan_penyebab) ?>;
      var selectDataAction = function() {
        return {
          data: dataAction,
          placeholder: 'Input Penyebab',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-penyebab').select2(selectDataAction());

      var data =  <?= json_encode($nte) ?>;
      var nte1 =  <?= json_encode($nte1) ?>;
      $.extend(data, nte1);
      var combo = $('#input-nte').select2({
        data: data,
        maximumSelectionSize: 2,
        multiple:true
      });
      
      $('#searchlist').btsListFilter('#searchinput', {itemChild: 'strong'});

      var datatypeont = [
        {"id":"ZTEF660", "text":"ZTE F660"},
        {"id":"ZTEF609", "text":"ZTE F609"},
        {"id":"ZTEF821", "text":"ZTE F821"},
        {"id":"ALUG240WA", "text":"ALU G240WA (HITAM)"},
        {"id":"ALUI240WA", "text":"ALU I240WA (PUTIH)"},
        {"id":"HG82455A", "text":"HUAWEI HG82455A"},
        {"id":"H87Z5675M21", "text":"HUAWEI H87Z5675M21"},
      ];

      var typeont = function() {
        return {
          data: datatypeont,
          placeholder: 'Input Type ONT',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }

      $('#input-type-ont').select2(typeont());

      var datatypestb = [
        {"id":"ZTEB700V5", "text":"<span class='label label-success'>ZTE STB HD</span>"},
        {"id":"ZTEB760H", "text":"<span class='label label-success'>ZTE STB HYBRID</span>"},
        {"id":"ZTEB660H", "text":"<span class='label label-success'>ZTE STB 4K</span>"},
      ];

      var typestb = function() {
        return {
          data: datatypestb,
          placeholder: 'Input Type STB',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }

      $('#input-type-stb').select2(typestb());

      var data =  <?= json_encode($nte) ?>;
      var nte1 =  <?= json_encode($nte1) ?>;
      $.extend(data, nte1);
      var combo = $('#input-nte').select2({
        data: data,
        maximumSelectionSize: 2,
        multiple:true
      });

    })
  </script>
@endsection