<?php

namespace App\DA;

use Illuminate\Database\Eloquent\Model;
use DB;

class SalesModel extends Model
{
    public static function getAllSales(){
    	return DB::table('psb_sales')->where('ket',1)->get();
    }

    public static function getSalesByCari($cari){
    	return DB::table('psb_sales')->where('kode_sales','LIKE','%'.$cari.'%')->orwhere('nama_sales', 'LIKE', '%'.$cari.'%')->get();
    }

    public static function simpanData($req){
   		return DB::table('psb_sales')->insert([
    		'kode_sales'	=> $req->kode,
    		'nama_sales'	=> $req->nama
    	]);
    }

    public static function getSalesByKodeSales($kodeSales){
    	return DB::table('psb_sales')->where('kode_sales',$kodeSales)->first();
    }
}
