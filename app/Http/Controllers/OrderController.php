<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use DB;
use Telegram;
use App\DA\User;
use App\DA\marina\Saldo;
use Validator;

class OrderController extends Controller
{
    protected static $sendPhoto = ['ODP', 'Hasil_Ukur_OPM'];
    protected $photoInputs = [
      'ODP', 'Sebelum','Progress', 'Sesudah', 'Foto-GRID', 'BA-Remo', 'Denah-Lokasi', 'Pengukuran', 'Pelanggan_GRID', 'ODP-dan-Redaman-GRID', 'ABD'
    ];
    protected $photoBenjar = [
      'ODC-Sebelum', 'ODC-Progress','ODC-Sesudah',
      'ODP-Sebelum', 'ODP-Progress','ODP-Sesudah',
      'Redaman-Sebelum', 'Redaman-Progress','Redaman-Sesudah',
      'Saluran-Penanggal-Sebelum', 'Saluran-Penanggal-Progress','Saluran-Penanggal-Sesudah',
      'Tiang-Sebelum', 'Tiang-Progress','Tiang-Sesudah'
    ];

    protected $photoValidasi = [
      'ODP-Sebelum', 'ODP-Progress','ODP-Sesudah',
      'Redaman-Sebelum', 'Redaman-Progress','Redaman-Sesudah',
      'Saluran-Penanggal-Sebelum', 'Saluran-Penanggal-Progress','Saluran-Penanggal-Sesudah',
      'Tiang-Sebelum', 'Tiang-Progress','Tiang-Sesudah'
    ];
    protected $photoValidasiSt = [
      'ODP-Sebelum', 'ODP-Progress','ODP-Sesudah',
      'Redaman-Sebelum', 'Redaman-Progress','Redaman-Sesudah',
      'Saluran-Penanggal-Sebelum', 'Saluran-Penanggal-Progress','Saluran-Penanggal-Sesudah',
      'Tiang-Sebelum', 'Tiang-Progress','Tiang-Sesudah'
    ];
    protected $photoOdpSehat = [
      'GRID_TIANG', 'GRID_PROGRES_TIANG','GRID_AKSESORIS', 'GRID_PROGRES_AKSESORIS', 'GRID_ODP', 'GRID_PROGRES_ODP', 'GRID_REBOUNDARY', 'GRID_PROGRES_REBOUNDARY', 'GRID_DENAH'
    ];
    protected static $photoOdpSehatst = [
      'GRID_TIANG', 'GRID_PROGRES_TIANG','GRID_AKSESORIS', 'GRID_PROGRES_AKSESORIS', 'GRID_ODP', 'GRID_PROGRES_ODP', 'GRID_REBOUNDARY', 'GRID_PROGRES_REBOUNDARY', 'GRID_DENAH'
    ];
    protected $photoRemo = ['ODP-dan-Redaman-GRID', 'Sebelum', 'Progress', 'Sesudah', 'Pengukuran', 'Pelanggan_GRID', 'BA-Remo'];
    protected $photoCommon = ['Sebelum', 'Progress', 'Sesudah', 'Foto-GRID', 'Denah-Lokasi', 'ABD'];
    protected $photoOdpLoss = ['ODP', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID'];
    protected static $photoInputst = [
      'ODP', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID', 'BA-Remo', 'Denah-Lokasi', 'Pengukuran', 'Pelanggan_GRID', 'ODP-dan-Redaman-GRID'
    ];
    protected $photoBenjarSt = [
      'ODC-Sebelum', 'ODC-Progress','ODC-Sesudah',
      'ODP-Sebelum', 'ODP-Progress','ODP-Sesudah',
      'Redaman-Sebelum', 'Redaman-Progress','Redaman-Sesudah',
      'Saluran-Penanggal-Sebelum', 'Saluran-Penanggal-Progress','Saluran-Penanggal-Sesudah',
      'Tiang-Sebelum', 'Tiang-Progress','Tiang-Sesudah'
    ];

    //transaksional HD
    public function input($id)
    {
      $data = null;
      $regu = DB::select('SELECT a.id_regu as id, a.uraian as text FROM regu a LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
        where a.ACTIVE=1 AND b.Witel_New = "'.session('witel').'"');
      $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
      $jenis_order = DB::table('maintenance_jenis_order')->get();
      return view('marina.order.input', compact('data', 'regu', 'jenis_order', 'sto'));
    }
    public function saveOrder($id, Request $req)
    {
      $auth = session('auth');
      //validator
      $rules = array(
        'headline' => 'required',
        'koordinat' => 'required',
        'sto' => 'required',
      );
      $messages = [
        'order_id.required' => 'Silahkan isi kolom "No Tiket" Bang!',
        'headline.required' => 'Silahkan isi kolom "Headline" Bang!',
        'koordinat.required' => 'Silahkan isi kolom "Koordinat" Bang!',
        'sto.required' => 'Silahkan Pilih "sto" Bang!',
        'no_inet.required' => 'Silahkan isi "no_inet" Bang!'
      ];
      if($req->jenis_order == 4){
        $rules['no_inet'] = 'required';
      }
      if($req->jenis_order != 1 && $req->jenis_order != 10){
        $rules['order_id'] = 'required';
      }
      $validator = Validator::make($req->all(), $rules, $messages);
      if ($validator->fails()) {
        return redirect()->back()
          ->withInput($req->all())
          ->withErrors($validator)
          ->with('alerts', [
            ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan order']
          ]);
      }
      if($req->jenis_order == 4){
        //validasi duplikasi no inet yg blm close
        $cekinet = DB::table('maintaince')->where('jenis_order', 4)->where('no_inet', $req->no_inet)->whereNull('status')->first();
        if($cekinet){
          return redirect()->back()
            ->withInput($req->all())
            ->with('alerts', [
              ['type' => 'danger', 'text' => '<strong>GAGAL</strong>, Duplikasi No Inet']
            ]);
        }
      }
      //end validator
      $order = DB::table('maintaince')->where('id', $id)->first();
      $jenis = DB::table('maintenance_jenis_order')->where('id', $req->jenis_order)->first();
      $sto = DB::table('maintenance_datel')->where('sto', $req->sto)->first();
      // if($auth->maintenance_level == 1)
        $regu = DB::table('regu')->where('id_regu', $req->regu)->first();
      // else
      //   $regu = DB::table('maintenance_regu')->where('nik1', $auth->id_karyawan)->orWhere('nik2', $auth->id_karyawan)->first();
      DB::transaction(function() use(&$id, $req, $order, $jenis, $regu, $auth, $sto) {
        if($order){
          $id_mt = $id;
          DB::table('maintaince')->where('id', $id)->update([
              "no_tiket"          =>$req->order_id,
              "witel"             =>session('witel'),
              "jenis_order"       =>$req->jenis_order,
              "nama_order"        =>$jenis->nama_order,
              "headline"          =>$req->headline,
              "pic"               =>$req->pic,
              "koordinat"         =>$req->koordinat,
              "dispatch_regu_id"  =>@$regu->id_regu,
              "dispatch_regu_name"=>@$regu->uraian,
              "username1"         =>@$regu->nik1,
              "username2"         =>@$regu->nik2,
              "modified_at"       =>DB::raw('now()'),
              "modified_by"       =>$auth->id_karyawan,
              "sto"               =>$sto->sto,
              "kandatel"          =>$sto->datel,
              "nama_odp"          =>$req->nama_odp,
              "no_inet"          =>$req->no_inet
          ]);

        }else{
            $no_tiket=$req->order_id;
            if($req->jenis_order==1 || $req->jenis_order==10){
              $count = DB::select('SELECT count(*) as banyak FROM maintaince WHERE created_at like "%'.date('Y-m-d').'%"');
              $banyak= $count[0]->banyak+1;
              $no_tiket=date('dmY').$banyak;
            }
            $id_mt = DB::table('maintaince')->insertGetId([
                "no_tiket"          =>$no_tiket,
                "witel"             =>session('witel'),
                "jenis_order"       =>$req->jenis_order,
                "nama_order"        =>$jenis->nama_order,
                "headline"          =>$req->headline,
                "pic"               =>$req->pic,
                "koordinat"         =>$req->koordinat,
                "dispatch_regu_id"  =>@$regu->id_regu,
                "dispatch_regu_name"=>@$regu->uraian,
                "username1"         =>@$regu->nik1,
                "username2"         =>@$regu->nik2,
                "created_at"        =>DB::raw('now()'),
                "created_by"        =>$auth->id_karyawan,
                "sto"               =>$sto->sto,
                "kandatel"          =>$sto->datel,
                "nama_odp"          =>$req->nama_odp,
                "no_inet"          =>$req->no_inet
            ]);
        }
        if($req->input('regu') != @$order->dispatch_regu_id){
          DB::table('maintenance_dispatch_log')->insert([
                "maintenance_id"  =>$id_mt,
                "regu_id"         =>@$regu->id,
                "regu_name"       =>@$regu->nama_regu,
                "ts_dispatch"     =>DB::raw('now()'),
                "dispatch_by"     =>$auth->id_karyawan,
                "action"     =>"dispatch"
          ]);
          // exec('cd ..;php artisan sendDispatch '.$id_mt.' > /dev/null &');
        }
      });
      return redirect('/marina/order/none/none/'.date('Y-m').'/all');
    }
    // public function refer(Request $req){
    //   DB::table('maintaince')->where('id', $req->id_maint)->update([
    //       "refer" => $req->refer
    //     ]);
    //   return redirect()->back();
    // }
    public function dispatchOrder(Request $req)
    {
      $sto = DB::table('maintenance_datel')->where('sto', $req->sto)->first();
      $auth = session('auth');
      $regu = DB::table('regu')->where('id_regu', $req->regu)->first();

      DB::table('maintaince')->where('id', $req->id_mt)->update([
                "dispatch_regu_id"  =>$req->regu,
                "dispatch_regu_name"=>$regu->uraian,
                "username1"         =>$regu->nik1,
                "username2"         =>$regu->nik2,
                "modified_at"       =>DB::raw('now()'),
                "modified_by"       =>$auth->id_karyawan,
                "sto"               =>$sto->sto,
                "kandatel"          =>$sto->datel,
                "witel"             =>$sto->witel
      ]);
      DB::table('maintenance_dispatch_log')->insert([
                "maintenance_id"  =>$req->id_mt,
                "regu_id"         =>$req->regu,
                "regu_name"       =>$regu->uraian,
                "ts_dispatch"     =>DB::raw('now()'),
                "dispatch_by"     =>$auth->id_karyawan,
                "action"     =>"dispatch"
      ]);

      $main = DB::table('maintaince')->where('id','=',$req->id_mt)->first();
      exec('cd ..;php artisan sendDispatch '.$req->id_mt.' > /dev/null &');
      return "<span class='label label-success'>success</span>";
    }
    // public function deleteOrder(Request $req){
    //   $mt = DB::table('maintaince')->where('id', $req->id_mt)->first();
    //   if($mt->status != "close"){
    //     if($mt->psb_laporan_id){
    //       DB::table('psb_laporan')->where('id', $mt->psb_laporan_id)->update([
    //         "checked_at" => "",
    //         "checked_by" => ""
    //         ]);
    //     }
    //     DB::table('maintaince')->where('id', $req->id_mt)->delete();
    //     DB::table('maintenance_dispatch_log')->insert([
    //             "maintenance_id"  =>$req->id_mt,
    //             "ts_dispatch"     =>DB::raw('now()'),
    //             "dispatch_by"     =>session('auth')->id_karyawan,
    //             "action"     =>  "delete ".$mt->no_tiket
    //       ]);
    //     return "success mendelete!";
    //   }else{
    //     return "gagal mendelete, order sudah close!";
    //   }
    // }
    public function tpsbcutoff($id,$jns){
      DB::table('psb_laporan')->where('id_tbl_mj', $id)->update(["isCutOff"=>1]);
      return redirect('/marina/listtommanpsb/'.$jns);
    }
    public function tpsbfixdispatch($id,$jns){
      DB::table('psb_laporan')->where('id_tbl_mj', $id)->update(["checked_by"=>null, "checked_at"=>null]);
      return redirect('/marina/listtommanpsb/'.$jns);
    }


    public function filter($sd, $ed, $sts, $jns)
    {
      $where = '';
      $tgl = '';
      if($sts)
        $where .= 'mt.status = "'.$sts.'" and ';
      if($sd){
        $tgl = 'mt.created_at like "%'.$sd.'%"';
      }
      if($ed){
        $tgl = '(mt.created_at between "'.$sd.' 00:00:01" and "'.$ed.' 23:59:59")';
      }
      $data = DB::select('select mt.*, mt.id as id_mt, pl.nama_odp,mt.nama_odp as nama_dp, pl.id_tbl_mj,
            (select nama from karyawan where id_karyawan = mt.username1) as nama1, 
            (select nama from karyawan where id_karyawan = mt.username2) as nama2,
            (select nama from karyawan where id_karyawan = mt.username3) as nama3, 
            (select nama from karyawan where id_karyawan = mt.username4) as nama4 
            from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id 
            left join psb_laporan pl on mt.psb_laporan_id = pl.id where '.$where.$tgl.' and mt.jenis_order='.$jns.' order by tgl_selesai desc');
      return view('marina.order.filter', compact('data'));
    }
    
    // public function listRemo($id)
    // {
    //   if($id == "sisa"){
    //     $data = DB::select('select *,m.id as id_mt from remo_servo rs left join maintaince m on rs.NO_SPEEDY=m.no_tiket where m.status is null');
    //   }else if($id == "all"){
    //     $data = DB::select('select *,m.id as id_mt from remo_servo rs left join maintaince m on rs.NO_SPEEDY=m.no_tiket where 1');
    //   }else{
    //     $data = DB::select('select *,m.id as id_mt from maintaince m left join remo_servo rs on m.no_tiket=rs.NO_SPEEDY where m.jenis_order=4 and m.status = "'.$id.'" and m.tgl_selesai like "%'.date('Y-m').'%"');
    //   }
    //   $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
    //   $regu = DB::table('maintenance_regu')->select('*', 'nama_regu as text')->get();
    //   return view('order.listRemo', compact('data', 'regu', 'sto'));
    // }
    // public function listRemoClose()
    // {
    //   $data = DB::table('maintaince')->where('status', 'close')->where('verifHd', 0)->where('jenis_order', 4)->get();
    //   return view('order.listverifremo', compact('data'));
    // }
    // public function verifRemo(Request $req)
    // {
    //   $auth = session('auth');
    //   DB::table('maintaince')->where('id', $req->id_maint)->update([
    //             "verifHd" => $req->redaman
    //   ]);
    //   DB::table('maintenance_verifhd_history')->insert([
    //             "maintenance_id" => $req->id_maint,
    //             "verif" => $req->redaman,
    //             "updated_nik" => $auth->id_karyawan,
    //             "updated_name" => $auth->nama
    //   ]);
    //   return redirect()->back();
    // }
    public function getJsonMaint($id)
    {
       return json_encode(DB::table('maintaince')
          ->select('maintaince.*', 'psb_laporan.kordinat_odp', 'psb_laporan.nama_odp', 'maintaince.nama_odp as nama_dp')
          ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
          ->where('maintaince.id', $id)->first());
    }
    // public function getJsonRemo($id)
    // {
    //   $mt = DB::table('maintaince')
    //       ->select('maintaince.*', 'psb_laporan.kordinat_odp', 'psb_laporan.nama_odp', 'maintaince.nama_odp as nama_dp')
    //       ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
    //       ->where('maintaince.no_tiket', $id)->first();
    //   return json_encode($mt);
    // }
    // public function dispatchOrderRemo(Request $req)
    // {
    //   $sto = DB::table('maintenance_datel')->where('sto', $req->sto)->first();
    //   $auth = session('auth');
    //   $regu = DB::table('maintenance_regu')->where('id', $req->regu)->first();
    //   $maint = DB::table('maintaince')->where('no_tiket', $req->no_tiket)->first();
    //   if(count($maint)){
    //     DB::table('maintaince')->where('no_tiket', $req->no_tiket)->update([
    //               "dispatch_regu_id"  =>$req->regu,
    //               "dispatch_regu_name"=>$regu->nama_regu,
    //               "username1"         =>$regu->nik1,
    //               "username2"         =>$regu->nik2,
    //               "username3"         =>$regu->nik3,
    //               "username4"         =>$regu->nik4,
    //               "modified_at"       =>DB::raw('now()'),
    //               "modified_by"       =>$auth->id_karyawan,
    //               "sto"               =>$sto->sto,
    //               "kandatel"          =>$sto->datel
    //     ]);
    //     $idmt = $req->id_mt;
    //   }else{
    //     $idremo = DB::table('maintaince')->insertGetId([
    //               "no_tiket"          =>$req->no_tiket,
    //               "pic"               =>$req->pic,
    //               "alamat"            =>$req->alamat,
    //               "koordinat"         =>$req->koordinatodp,
    //               "jenis_order"       =>4,
    //               "nama_order"        =>"REMO",
    //               "created_at"        =>DB::raw('now()'),
    //               "created_by"        =>$auth->id_karyawan,
    //               "dispatch_regu_id"  =>$req->regu,
    //               "dispatch_regu_name"=>$regu->nama_regu,
    //               "username1"         =>$regu->nik1,
    //               "username2"         =>$regu->nik2,
    //               "username3"         =>$regu->nik3,
    //               "username4"         =>$regu->nik4,
    //               "modified_at"       =>DB::raw('now()'),
    //               "modified_by"       =>$auth->id_karyawan,
    //               "sto"               =>$sto->sto,
    //               "kandatel"          =>$sto->datel
    //               ]);
    //     $idmt = $idremo;
    //   }
    //   DB::table('maintenance_dispatch_log')->insert([
    //             "maintenance_id"  =>$idmt,
    //             "regu_id"         =>$req->regu,
    //             "regu_name"       =>$regu->nama_regu,
    //             "ts_dispatch"     =>DB::raw('now()'),
    //             "dispatch_by"     =>$auth->id_karyawan,
    //             "action"     =>"dispatch"
    //   ]);
    //   exec('cd ..;php artisan sendDispatch '.$idmt.' > /dev/null &');
    //   return "<span class='label label-success'>success</span>";
    // }
  //   public function getJsonPsb($id){
  //       return json_encode(DB::table('psb_laporan')
  //         ->where('id', $id)->first());
  //   }
    
    public function ajax_matrik($id, $datel, $tgl){
      if($datel == 'all'){
        $data = DB::select("select * from (
          select md.*, 
            (select count(mt.id) from maintaince mt where mt.jenis_order = ".$id." and mt.kandatel = md.datel and mt.status = 'close' and refer is null and mt.tgl_selesai like '%".$tgl."%') as close,
            (select count(mt.id) from maintaince mt where mt.jenis_order = ".$id." and mt.kandatel = md.datel and mt.status = 'kendala pelanggan' and refer is null) as kendala_pelanggan,
            (select count(mt.id) from maintaince mt where mt.jenis_order = ".$id." and mt.kandatel = md.datel and mt.status = 'kendala teknis' and refer is null) as kendala_teknis,
            (select count(mt.id) from maintaince mt where mt.jenis_order = ".$id." and mt.kandatel = md.datel and mt.status is null and mt.dispatch_regu_id is not null and mt.dispatch_regu_id !=0 and refer is null) as no_update,
            (select count(mt.id) from maintaince mt where mt.jenis_order = ".$id." and mt.kandatel = md.datel and (mt.dispatch_regu_id is null or mt.dispatch_regu_id=0) and refer is null) as undisp
          from maintenance_so md where 1 order by datel asc) subq
          where kendala_pelanggan>0 or kendala_teknis>0 or no_update>0 or undisp>0
        ");
      }else{
        $data = DB::select("select * from (
          select md.*, 
            (select count(mt.id) from maintaince mt where mt.jenis_order = ".$id." and mt.sto = md.sto and mt.status = 'close' and refer is null and mt.tgl_selesai like '%".$tgl."%') as close,
            (select count(mt.id) from maintaince mt where mt.jenis_order = ".$id." and mt.sto = md.sto and mt.status = 'kendala pelanggan' and refer is null) as kendala_pelanggan,
            (select count(mt.id) from maintaince mt where mt.jenis_order = ".$id." and mt.sto = md.sto and mt.status = 'kendala teknis' and refer is null) as kendala_teknis,
            (select count(mt.id) from maintaince mt where mt.jenis_order = ".$id." and mt.sto = md.sto and mt.status is null and mt.dispatch_regu_id is not null and mt.dispatch_regu_id !=0 and refer is null) as no_update,
            (select count(mt.id) from maintaince mt where mt.jenis_order = ".$id." and mt.sto = md.sto and (mt.dispatch_regu_id is null or mt.dispatch_regu_id=0) and refer is null) as undisp
          from maintenance_datel md where 1 order by datel asc) subq
          where kendala_pelanggan>0 or kendala_teknis>0 or no_update>0 or undisp>0
        ");
      }
      $psb = DB::select("select m.id, m.status_psb, m.nama_order as jenis_order,
        (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and pl.status_laporan = m.status_psb and pl.isCutOff is null) as xcheck
        from maintenance_jenis_order m where m.id = 3")[0];
      $odpfullpsb = DB::select("select m.id, m.status_psb, m.nama_order as jenis_order,
        (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and pl.status_laporan = m.status_psb and pl.isCutOff is null) as xcheck
        from maintenance_jenis_order m where m.id = 9")[0];
      $insertpsb = DB::select("select m.id, m.status_psb, m.nama_order as jenis_order,
        (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and pl.status_laporan = m.status_psb and pl.isCutOff is null) as xcheck
        from maintenance_jenis_order m where m.id = 6")[0];
      return view('marina.order.ajaxMatrik', compact('data', 'id', 'psb', 'odpfullpsb', 'insertpsb'));
    }
    public function orderList($jns, $sts, $tgl, $kandatel, Request $req)
    {
      $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
      $datel = DB::table('maintenance_datel')->select('datel as id', 'datel as text')->groupBy('datel')->get();
      //untuk search search 
      if ($req->has('q')) {
        $matrix =0;
        $regu = DB::table('regu')
          ->select('id_regu as id', 'uraian as text', DB::raw('(select nama from karyawan where id_karyawan = regu.nik1) as nama1, (select nama from karyawan where id_karyawan = regu.nik2) as nama2'))->get();
        $data = DB::select("select mt.*, mt.id as id_mt, pl.nama_odp,mt.nama_odp as nama_dp,
          (select nama from karyawan where id_karyawan = mt.username1) as nama1, 
          (select nama from karyawan where id_karyawan = mt.username2) as nama2,
          (select count(id) from maintaince where refer = mt.id) as jml_anak
          from maintaince mt 
          left join psb_laporan pl on mt.psb_laporan_id = pl.id
          where (refer is null and mt.no_tiket like '%".$req->q."') or mt.no_tiket like '%".$req->q."' order by tgl_selesai desc");
        if(count($data)>0 && $data[0]->refer != null){
          $refer=$data[0]->refer;
          $data = DB::select("select mt.*, mt.id as id_mt, pl.nama_odp,mt.nama_odp as nama_dp,
          (select nama from karyawan where id_karyawan = mt.username1) as nama1, 
          (select nama from karyawan where id_karyawan = mt.username2) as nama2,
          (select count(id) from maintaince where refer = mt.id) as jml_anak
          from maintaince mt 
          left join psb_laporan pl on mt.psb_laporan_id = pl.id
          where mt.id='".$refer."'");
        }
      }else{
        $regu = DB::table('regu')
          ->select('id_regu as id', 'uraian as text', DB::raw('(select nama from karyawan where id_karyawan = regu.nik1) as nama1, (select nama from karyawan where id_karyawan = regu.nik2) as nama2'))->get();
        if($jns == "none" && $sts == "none"){
            //untuk matrix
            $matrix = 1;
            $data = DB::select("select m.id, m.nama_order as jenis_order, 
            (select count(mt.id) from maintaince mt where mt.jenis_order = m.id and mt.status = 'close' and refer is null and mt.tgl_selesai like '%".$tgl."%') as close,
            (select count(mt.id) from maintaince mt where mt.jenis_order = m.id and mt.status = 'kendala pelanggan' and refer is null) as kendala_pelanggan,
            (select count(mt.id) from maintaince mt where mt.jenis_order = m.id and mt.status = 'kendala teknis' and refer is null) as kendala_teknis,
            (select count(mt.id) from maintaince mt where mt.jenis_order = m.id and mt.status is null and mt.dispatch_regu_id is not null and mt.dispatch_regu_id !=0 and refer is null) as no_update,
            0 as order_remo,
            (select count(mt.id) from maintaince mt where mt.jenis_order = m.id and (mt.dispatch_regu_id is null or mt.dispatch_regu_id=0) and refer is null ) as undisp
            from maintenance_jenis_order m where 1");
        }else{
            //untuk list
            $matrix = 0;
            $sql = "";
            $jenis_order = "";
            $sql = " mt.status = '".$sts."'";
            if($jns != 'all')
              $jenis_order = " and (mt.jenis_order = '".$jns."')";
            if($sts == 'null')
              $sql = " mt.status is ".$sts." and mt.dispatch_regu_id is not null and mt.dispatch_regu_id !=0";
            else if($sts == 'undisp')
              $sql = " (mt.dispatch_regu_id is null or mt.dispatch_regu_id=0)";
            else if($sts == 'all')
              $sql = " 1 ";
            if ($sts == 'close')
              $sql .= " and mt.tgl_selesai like '%".$tgl."%'";
            $qdatel = "";
            if($kandatel != "all")
              $qdatel = " and mt.kandatel='".$kandatel."' ";
            $qtgl = "";
            if($tgl!="all")
              $qtgl = " and mt.tgl_selesai like '%".$tgl."%'";
            $data = DB::select("select mt.*, mt.id as id_mt, pl.nama_odp,mt.nama_odp as nama_dp,
              (select nama from karyawan where id_karyawan = mt.username1) as nama1, 
              (select nama from karyawan where id_karyawan = mt.username2) as nama2,
              (select count(id) from maintaince where refer = mt.id) as jml_anak
              from maintaince mt 
              left join psb_laporan pl on mt.psb_laporan_id = pl.id
              where refer is null and ".$sql." ".$jenis_order." ".$qtgl." ".$qdatel." order by tgl_selesai desc");
            
        }
      }
      $psb = DB::select("select m.id, m.status_psb, m.nama_order as jenis_order,
        (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and pl.status_laporan = m.status_psb and pl.isCutOff is null) as xcheck
        from maintenance_jenis_order m where m.id = 3")[0];
      $odpfullpsb = DB::select("select m.id, m.status_psb, m.nama_order as jenis_order,
        (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and pl.status_laporan = 24 and pl.isCutOff is null) as xcheck
        from maintenance_jenis_order m where m.id = 9")[0];
      $insertpsb = DB::select("select m.id, m.status_psb, m.nama_order as jenis_order,
        (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and pl.status_laporan = m.status_psb and pl.isCutOff is null) as xcheck
        from maintenance_jenis_order m where m.id = 6")[0];
      $mt = DB::table('maintaince')->select('id','no_tiket as text', 'nama_odp', 'nama_order', 'status')
      ->whereIn('jenis_order', [2,3,4])
      ->whereNull('refer')
      ->get();
      return view('marina.order.list', compact('data', 'matrix', 'regu', 'sto', 'datel', 'mt', 'psb', 'odpfullpsb', 'insertpsb'));
    }
  //   public function listodpfulltommanpsb(){
  //     $data = DB::select("select pls.laporan_status,pl.nama_odp, pl.kordinat_odp, pl.kordinat_pelanggan, d.id as id_dispatch, d.Ndem as no_tiket,
  //                pl.kordinat_odp as nama_order, pl.catatan as headline, pl.created_at, pl.created_by as dispatch_regu_name, redaman from 
  //                dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket 
  //                left join psb_laporan pl on d.id=pl.id_tbl_mj
  //                left join psb_laporan_status pls on pl.status_laporan=pls.laporan_status_id 
  //                where pl.isCutOff is null and mc.id is null and (pl.status_laporan = '24')");
  //     //dd($data);
  //     return view('order.tommanpsblist', compact('data'));
  //   }
    
    public function listtommanpsb($jns){
      //loss=2,full=24,insert=11
      $data = DB::select("select pls.laporan_status,pl.nama_odp, pl.kordinat_odp, pl.kordinat_pelanggan, d.id as id_dispatch, d.Ndem as no_tiket,
                 pl.kordinat_odp as nama_order, pl.catatan as headline, pl.created_at, pl.created_by as dispatch_regu_name, redaman, pl.isCutOff, pl.checked_at, pl.checked_by from 
                 dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket 
                 left join psb_laporan pl on d.id=pl.id_tbl_mj
                 left join psb_laporan_status pls on pl.status_laporan=pls.laporan_status_id 
                 where pl.isCutOff is null and mc.id is null and (pl.status_laporan = '".$jns."')");
      return view('marina.order.tommanpsblist', compact('data'));
    }
    public function registertommanpsb($jns,$id){
      $data = DB::table('psb_laporan')
        ->select('psb_laporan.nama_odp', 'psb_laporan.catatan','psb_laporan.id','psb_laporan.checked_by','psb_laporan.kordinat_odp','psb_laporan.catatan', 'maintenance_jenis_order.nama_order', 'maintenance_jenis_order.id as jenis_order', 'dispatch_teknisi.Ndem')
        ->leftJoin('dispatch_teknisi', 'psb_laporan.id_tbl_mj', '=', 'dispatch_teknisi.id')
        ->leftJoin('maintenance_jenis_order', 'maintenance_jenis_order.status_psb', '=', 'psb_laporan.status_laporan')
        ->where('id_tbl_mj', $id)->first();
        $jenis_order = DB::table('maintenance_jenis_order')->get();
        $regu = DB::table('regu')->select('*', 'uraian as text', 'id_regu as id')->get();
        $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();

      return view('marina.order.tommanpsbregister', compact('regu', 'sto', 'jenis_order', 'data'));
    }
    public function saveregistertommanpsb($jns,$id, Request $req){
      $rules = array(
        'headline' => 'required',
        'koordinat' => 'required',
        'sto' => 'required'
      );
      $messages = [
        'headline.required' => 'Silahkan isi kolom "Headline" Bang!',
        'koordinat.required' => 'Silahkan isi kolom "Koordinat" Bang!',
        'sto.required' => 'Silahkan Pilih "sto" Bang!'
      ];
      $validator = Validator::make($req->all(), $rules, $messages);
      if ($validator->fails()) {
        return redirect()->back()
            ->withInput($req->all())
            ->withErrors($validator)
            ->with('alerts', [
              ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan order']
            ]);
      }
      $auth = session('auth');
      $pl = DB::table('psb_laporan')
        ->select('psb_laporan.id','psb_laporan.checked_by','psb_laporan.kordinat_odp','psb_laporan.catatan', 'maintenance_jenis_order.nama_order', 'maintenance_jenis_order.id as jenis_order', 'dispatch_teknisi.Ndem')
        ->leftJoin('dispatch_teknisi', 'psb_laporan.id_tbl_mj', '=', 'dispatch_teknisi.id')
        ->leftJoin('maintenance_jenis_order', 'maintenance_jenis_order.status_psb', '=', 'psb_laporan.status_laporan')
        ->where('id_tbl_mj', $id)->first();

        if(substr($pl->Ndem,0,2) == "IN"){
          $order_from = "ASSURANCE";
        }else{
          $order_from = "PSB";
        }
        $datasto = DB::table('maintenance_datel')->where('sto', $req->sto)->first();
        $regu = DB::table('regu')->where('id_regu', $req->regu)->first();
        if(DB::table('maintaince')->where('no_tiket', $pl->Ndem)->first()){
          return redirect('/listtommanpsb/{{ $jns }}')->with('alerts', [
                ['type' => 'danger', 'text' => '<strong>Nomor Tiket Sudah ada di Databas Marina</strong>']
              ]);
        }else{
          $id_mt = DB::table('maintaince')->insertGetId([
                    "no_tiket"          =>$pl->Ndem,
                    "witel"             =>session('witel'),
                    "jenis_order"       =>$pl->jenis_order,
                    "nama_order"        =>$pl->nama_order,
                    "headline"          =>$req->headline,
                    "pic"               =>$auth->id_karyawan."/".$auth->nama,
                    "koordinat"         =>$req->koordinat,
                    "created_at"        =>DB::raw('now()'),
                    "created_by"        =>$auth->id_karyawan,
                    "psb_laporan_id"    =>$pl->id,
                    "order_from"        =>$order_from,
                    "kandatel"          =>$datasto->datel,
                    "sto"               =>$req->sto,
                    "nama_odp"          =>$req->nama_odp,
                    "dispatch_regu_id"  =>@$regu->id_regu,
                    "dispatch_regu_name"=>@$regu->uraian,
                    "username1"         =>@$regu->nik1,
                    "username2"         =>@$regu->nik2
          ]);
          DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
            'checked_by' => $auth->id_karyawan,
            'checked_at' => DB::raw('now()')
          ]);
          if($req->input('regu')){
            DB::table('maintenance_dispatch_log')->insert([
                  "maintenance_id"  =>$id_mt,
                  "regu_id"         =>@$regu->id,
                  "regu_name"       =>@$regu->nama_regu,
                  "ts_dispatch"     =>DB::raw('now()'),
                  "dispatch_by"     =>$auth->id_karyawan,
                  "action"     =>"dispatch"
            ]);
            exec('cd ..;php artisan sendDispatch '.$id_mt.' > /dev/null &');
          }
          return redirect('/marina/listtommanpsb/'.$jns);
        }
        //exec('cd ..;php artisan sendMt '.$id.' > /dev/null &');
    }
    



  //   //tech list order
    public function tech()
    {
      /*
      $auth = session('auth');
      $status = " and (m.status is NULL OR m.status != 'close')";
      $condition = "(m.username1 = '".$auth->id_karyawan."' OR m.username2 = '".$auth->id_karyawan."')";
      $list = DB::select('
        SELECT m.*, pl.nama_odp, pl.kordinat_odp, m.nama_odp as nama_odp_m
        FROM maintaince m left join psb_laporan pl on m.psb_laporan_id = pl.id
        WHERE '.$condition.' '.$status.' and refer is null order by m.id desc
      ');
      return view('tech.list', compact('list'));
        */
        $auth = session('auth');
        // $cektl = DB::table('maintenance_regu')->where('niktl', $auth->id_karyawan)->first();
        // if(count($cektl)){
        //   $data = DB::select('select mr.*, 
        //     k.id_karyawan as idt1,
        //     k.mt_absen as absent1,
        //     k.nama as namat1,
        //     k.mt_verif_absen as verift1,
        //     kk.id_karyawan as idt2,
        //     kk.mt_absen as absent2,
        //     kk.nama as namat2,
        //     kk.mt_verif_absen as verift2,
        //     kkk.id_karyawan as idt3,
        //     kkk.mt_absen as absent3,
        //     kkk.nama as namat3,
        //     kkk.mt_verif_absen as verift3,
        //     kkkk.id_karyawan as idt4,
        //     kkkk.mt_absen as absent4,
        //     kkkk.nama as namat4,
        //     kkkk.mt_verif_absen as verift4 from maintenance_regu mr 
        //     left join karyawan k on mr.nik1 = k.id_karyawan
        //     left join karyawan kk on mr.nik2 = kk.id_karyawan
        //     left join karyawan kkk on mr.nik3 = kkk.id_karyawan
        //     left join karyawan kkkk on mr.nik4 = kkkk.id_karyawan where mr.status_regu=1 and mr.niktl="'.$auth->id_karyawan.'" 
        //   ');
        //   return view('tech.absen', compact('msg', 'data'));

        // }else{
        //   $cekabsen = DB::table('karyawan')->where('id_karyawan', $auth->id_karyawan)->first();
        //   if($cekabsen){
        //     //cek absen teknisi maintenance
        //     $cektech = DB::table('maintenance_regu')->where('nik1', $auth->id_karyawan)->orWhere('nik2', $auth->id_karyawan)->first();
            
        //     if(!count($cektech) || substr($cekabsen->mt_verif_absen, 0, 10)==date('Y-m-d')){
              $status = " and (m.status is NULL OR m.status != 'close') and refer is null";
              // $status = " and m.status is NULL and refer is null";
              $condition = "(m.username1 = '".$auth->id_karyawan."' OR m.username2 = '".$auth->id_karyawan."' OR m.username3 = '".$auth->id_karyawan."' OR m.username4 = '".$auth->id_karyawan."')";
              $list = DB::select('
                SELECT m.*, pl.nama_odp, pl.kordinat_odp, m.nama_odp as nama_odp_m, dt.manja, dps.orderName, dps.orderAddr, dps.orderNotel, dps.ndemSpeedy, dps.ndemPots
                FROM maintaince m left join psb_laporan pl on m.psb_laporan_id = pl.id
                LEFT JOIN dispatch_teknisi dt on pl.id_tbl_mj = dt.id
                LEFT JOIN Data_Pelanggan_Starclick dps on dt.ndem = dps.orderId
                LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
                WHERE '.$condition.' '.$status.' order by m.id desc
              ');
              return view('marina.tech.list', compact('list'));
        //     }else{
        //       $msg = 0;
        //       if(substr($cekabsen->mt_absen, 0, 10) == date('Y-m-d'))
        //         $msg = "SILAHKAN MENUNGGU VERIFIKASI TL ANDA NIK/NAMA";
        //       return view('tech.absen', compact('msg'));
        //     }

        //   }else{
        //     //teknisi prov
        //     $status = " and (m.status is NULL OR m.status != 'close') and refer is null";
        //     // $status = " and m.status is NULL and refer is null";
        //     $condition = "(mr.nik1 = '".$auth->id_karyawan."' OR mr.nik2 = '".$auth->id_karyawan."' OR mr.nik3 = '".$auth->id_karyawan."' OR mr.nik4 = '".$auth->id_karyawan."')";
        //     $list = DB::select('
        //       SELECT m.*, pl.nama_odp, pl.kordinat_odp, m.nama_odp as nama_odp_m, dt.manja, dps.orderName, dps.orderAddr, dps.orderNotel, dps.ndemSpeedy, dps.ndemPots
        //       FROM maintaince m left join psb_laporan pl on m.psb_laporan_id = pl.id
        //       LEFT JOIN dispatch_teknisi dt on pl.id_tbl_mj = dt.id
        //       LEFT JOIN Data_Pelanggan_Starclick dps on dt.ndem = dps.orderId
        //       LEFT JOIN maintenance_regu mr on m.dispatch_regu_id = mr.id
        //       WHERE '.$condition.' '.$status.' order by m.id desc
        //     ');
        //     return view('tech.list', compact('list'));
        //   }
        // }

    }
    public function updated()
    {
        $auth = session('auth');
        $status = " and m.status = 'close' and m.tgl_selesai like '%".date('Y-m')."%'";
        $condition = "(m.username1 = '".$auth->id_karyawan."' OR m.username2 = '".$auth->id_karyawan."' OR m.username3 = '".$auth->id_karyawan."' OR m.username4 = '".$auth->id_karyawan."')";
        $list = DB::select('
          SELECT m.*, pl.nama_odp, pl.kordinat_odp, m.nama_odp as nama_odp_m, dt.manja, dps.orderName, dps.orderAddr, dps.orderNotel, dps.ndemSpeedy, dps.ndemPots
          FROM maintaince m left join psb_laporan pl on m.psb_laporan_id = pl.id
          LEFT JOIN dispatch_teknisi dt on pl.id_tbl_mj = dt.id
          LEFT JOIN Data_Pelanggan_Starclick dps on dt.ndem = dps.orderId
          LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
          WHERE '.$condition.' '.$status.' order by m.id desc
        ');
        return view('marina.tech.list', compact('list'));
    }

  //   //to form progress
    public function update($id)
    {

      $foto = $this->photoCommon;
      $exists = DB::select('
        SELECT m.*, pl.nama_odp,m.nama_odp as odp_nama, pl.kordinat_odp, pl.id_tbl_mj, mr.TL as niktl, k.nama as psbcreated
        FROM maintaince m 
        left join psb_laporan pl on m.psb_laporan_id = pl.id
        LEFT JOIN regu mr on m.dispatch_regu_id=mr.id_regu
        LEFT JOIN karyawan k on m.created_by = k.id_karyawan
        WHERE m.id = ?
      ', [
        $id
      ]);
      $komen = DB::table('maintenance_note')->where('mt_id', $id)->orderBy('id', 'asc')->get();
      $dispatch_log = DB::table('maintenance_dispatch_log')->where('maintenance_id', $id)->orderBy('id', 'asc')->get();
      $action_cause = DB::table('maintenance_cause_action')->select('action_cause as text', 'action_cause as id', 'status')->get();
      if (count($exists)) {
        $data = $exists[0];
        if($data->jenis_order == 8){
          if(!$data->step_id){
            $data = DB::table('maintaince')->where('id', $id)->first();
            $list = DB::table('maintenance_validasi_port')->where('maintenance_id', $id)->get();
            return view('tech.formValidasiPort', compact('data', 'list'));
          }else if($data->step_id== 1){
            $data = DB::table('maintaince')->where('id', $id)->first();
            $list = DB::table('maintenance_reboundary')->where('maintenance_id', $id)->get();
            return view('tech.reboundary', compact('data', 'list'));
          }else if($data->step_id== 2){
            $data = DB::table('maintaince')->where('id', $id)->first();
            $list = DB::table('maintenance_validasi_port')->where('maintenance_id', $id)->get();
            return view('tech.formValidasiPort', compact('data', 'list'));
          }
        }
        if($data->jenis_order == 3 || $data->jenis_order == 9){
          $foto = $this->photoOdpLoss;
        }else if($data->jenis_order == 4){
          $foto = $this->photoRemo;
        }else if($data->jenis_order == 8){
          $foto = $this->photoOdpSehat;
        }else if($data->jenis_order == 1){
          $foto = $this->photoBenjar;
        }else if($data->jenis_order == 10){
          $foto = $this->photoValidasi;
        }
        if($data->action_cause){
          $action_cause = DB::table('maintenance_cause_action')
            ->select('action_cause as text', 'action_cause as id', 'status')
            ->where('status', $data->status)->get();
        }
      }
      else{
        $data = new \stdClass();
      }
      // dd($data);
      /*
      $materials = DB::select('
          SELECT
            i.*, i.uraian as nama_item,
            COALESCE(m.qty, 0) AS qty
          FROM khs_maintenance i
          LEFT JOIN
            maintaince_mtr m
            ON i.id_item = m.id_item
            AND m.maintaince_id = ? 
          ORDER BY m.id_item
        ', [
          $id
        ]);
        */
      $saldos = Saldo::getSubmitedMaterial(session('auth')->id_karyawan, $id);
      $saldo = Saldo::getSisaMaterial(session('auth')->id_karyawan, $id);
      foreach($saldo as $s){

        foreach($saldos as $ss){
          // echo $s->rfc."-".$s->id_item." vs ".$ss->rfc."-".$ss->id_item;
          if($s->bantu != $ss->bantu){
            $saldo[] = $ss;
          }
        }
      }
      // dd($saldo);
      $materials = DB::select('
        SELECT i.*, i.uraian as nama_item,COALESCE(m.qty, 0) AS qty FROM maintenance_mtr_program mmp 
        LEFT JOIN maintaince_mtr m ON mmp.id_item = m.id_item
          AND m.maintaince_id = ? and mmp.witel = ?
        LEFT JOIN khs_maintenance i ON mmp.id_item = i.id_item 
        WHERE program_id = ? and i.witel = ?
      ', [
        $id, session('witel'), $data->jenis_order, session('witel')
      ]);
      // dd($saldo);
      //print_r($materials);
      $penyebab = DB::table('maintenance_penyebab')->select('*', 'penyebab as text')->get();
      return view('marina.tech.progress', compact('data', 'materials', 'foto', 'action_cause', 'komen','dispatch_log', 'penyebab', 'saldo'));
    }
    private function handleFileUpload($request, $id, $foto)
    {
      foreach($foto as $name) {
        $input = 'photo-'.$name;
        if ($request->hasFile($input)) {
          //dd($input);
          $path = public_path().'/upload3/maintenance/'.$id.'/';
          if (!file_exists($path)) {
            if (!mkdir($path, 0770, true))
              return 'gagal menyiapkan folder foto evidence';
          }
          $file = $request->file($input);
          $ext = 'jpg';
          //TODO: path, move, resize
          try {
            $moved = $file->move("$path", "$name.$ext");
            $img = new \Imagick($moved->getRealPath());
            $img->scaleImage(100, 150, true);
            $img->writeImage("$path/$name-th.$ext");
          }
          catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan foto evidence '.$name;
          }
        }
      }
    }
    public function saveProgres(Request $request, $id){
        $rules = array(
          'status' => 'required'
        );
        $messages = [
          'status.required' => 'Silahkan pilih "Status" Bang!',
          'action_cause.required' => 'Silahkan pilih "Action/Cause" Bang!',
          'action.required' => 'Silahkan isi "Action" Bang!',
          'splitter.required' => 'Silahkan pilih "Jenis Splitter" Bang!',
          'flag_Sebelum.required' => 'Silahkan Isi Foto "Sebelum" Bang!',
          'flag_Progress.required' => 'Silahkan Isi Foto "Progress" Bang!',
          'flag_Sesudah.required' => 'Silahkan Isi Foto "Sesudah" Bang!',

          'flag_ODP.required' => 'Silahkan Isi Foto "ODP" Bang!',
          'flag_ODP-dan-Redaman-GRID.required' => 'Silahkan Isi Foto "ODP-dan-Redaman-GRID" Bang!',
          'flag_Pengukuran.required' => 'Silahkan Isi Foto "Pengukuran" Bang!',
          'flag_Pelanggan_GRID.required' => 'Silahkan Isi Foto "Pelanggan_GRID" Bang!',
          'flag_BA-Remo.required' => 'Silahkan Isi Foto "BA-Remo" Bang!',
          'flag_Foto-GRID.required' => 'Silahkan Isi Foto "Foto-GRID" Bang!',
          'flag_Denah-Lokasi.required' => 'Silahkan Isi Foto "Denah-Lokasi" Bang!',
          'redaman_awal.required' => 'Silahkan Isi kolom "Redaman Sebelum" Bang!',
          'redaman_akhir.required' => 'Silahkan Isi kolom "Redaman Sesudah" Bang!',
          'penyebab_id.required' => 'Silahkan Pilih "Penyebab" Bang!'
        ];

        $auth = session('auth');
        $exists = DB::select('
          SELECT *
          FROM maintaince
          WHERE id = ?
        ',[
          $id
        ]);

        $saldo = json_decode($request->input('saldo'));
        $materials = json_decode($request->input('materials'));
        if (count($exists)) {
          $data = $exists[0];
          $foto = $this->photoCommon;
          if($data->jenis_order == 3 || $data->jenis_order == 9){
            $foto = $this->photoOdpLoss;
          }else if($data->jenis_order == 4){
            $foto = $this->photoRemo;
          }else if($data->jenis_order == 8){
            $foto = $this->photoOdpSehat;
          }else if($data->jenis_order == 1){
            $foto = $this->photoBenjar;
          }else if($data->jenis_order == 10){
            $foto = $this->photoValidasi;
          }
          $this->handleFileUpload($request, $id, $foto);
          if($request->status == 'close' && $data->verify == null && $request->action_cause != "CANCEL ORDER"){
            if($data->jenis_order != 8 && $data->jenis_order != 1 && $data->jenis_order != 10){
              $rules = array_merge($rules, array('action_cause' => 'required','action' => 'required','flag_Sebelum' => 'required','flag_Progress' => 'required','flag_Sesudah' => 'required',));
            }
            if($data->jenis_order == 4){
              $rules = array_merge($rules, array('flag_ODP-dan-Redaman-GRID' => 'required','flag_Pengukuran' => 'required','flag_Pelanggan_GRID' => 'required','flag_BA-Remo' => 'required','redaman_awal' => 'required','redaman_akhir' => 'required'));
            }else if($data->jenis_order == 3  || $data->jenis_order == 9){
              $rules = array_merge($rules,array('penyebab_id'=>'required','flag_ODP'=>'required','flag_Foto-GRID'=>'required'));
            }else if($data->jenis_order != 8 && $data->jenis_order != 1 && $data->jenis_order != 10){
              $rules = array_merge($rules,array('flag_Foto-GRID'=>'required','flag_Denah-Lokasi'=>'required'));
            }
            if($data->jenis_order == 3 || $data->jenis_order == 4 || $data->jenis_order == 9 || $data->jenis_order == 8 || $data->jenis_order == 10){
              $rulse['splitter'] = 'required';
            }
            if($data->jenis_order == 3 || $data->jenis_order == 9 || $data->jenis_order == 8 || $data->jenis_order == 10){
              $rules = array_merge($rules,array('port_idle'=>'required','port_used'=>'required'));
            }if($data->jenis_order == 8){
              $rules = array_merge($rules,array('flag_GRID_TIANG'=>'required','flag_GRID_PROGRES_TIANG'=>'required','flag_GRID_AKSESORIS'=>'required','flag_GRID_PROGRES_AKSESORIS'=>'required','flag_GRID_ODP'=>'required','flag_GRID_PROGRES_ODP'=>'required','flag_GRID_REBOUNDARY'=>'required','flag_GRID_PROGRES_REBOUNDARY'=>'required','flag_GRID_DENAH'=>'required'));
            }
            if($data->jenis_order == 10){
              $rules = array_merge($rules,array('flag_ODP-Sebelum'=>'required',
                'flag_ODP-Progress'=>'required',
                'flag_ODP-Sesudah'=>'required',
                'flag_Redaman-Sebelum'=>'required',
                'flag_Redaman-Progress'=>'required',
                'flag_Redaman-Sesudah'=>'required',
                'flag_Saluran-Penanggal-Sebelum'=>'required',
                'flag_Saluran-Penanggal-Progress'=>'required',
                'flag_Saluran-Penanggal-Sesudah'=>'required',
                'flag_Tiang-Sebelum'=>'required',
                'flag_Tiang-Progress'=>'required',
                'flag_Tiang-Sesudah'=>'required'));
            }
          }
          // dd($rules);
          $validator = Validator::make($request->all(), $rules, $messages);
          if ($validator->fails()) {
              // dd($validator);
            return redirect()->back()
                ->withInput($request->all())
                ->withErrors($validator)
                ->with('alerts', [
                  ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan (Dokumen atau Isian kurang lengkap)']
                ]);
          }
          DB::transaction(function() use($request, $data, $auth, $materials, $id, $saldo) {
            if($saldo){
              foreach($saldo as $s){
                $s->id_pemakaian = $id;
                $s->action = 2;
                $s->value = $s->pemakaian*-1;
                $s->created_at = date('Y-m-d H:i:s');
                $s->created_by = $auth->id_karyawan;
                unset($s->id);
                unset($s->tpemakaian);
                unset($s->pemakaian);
                unset($s->kembalian);
                unset($s->sisa);
              }
              Saldo::delete($id,2);
              //to array
              Saldo::insert(json_decode(json_encode($saldo), true));
              // dd($saldo);
            }
            DB::table('maintaince_mtr')
              ->where('maintaince_id', $data->id)
              ->delete();
            $mtrcount = 0;
            
            foreach($materials as $material) {
              DB::table('maintaince_mtr')->insert([
                'maintaince_id' => $data->id,
                'id_item'       => $material->id_item,
                'qty'           => $material->qty,
              ]);
              $mtrcount++;
            }
            $total = DB::select('
              SELECT 
              (SELECT sum((qty*material_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as material,
              (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as jasa
              FROM maintaince m
              WHERE id = ?
            ', [$id])[0];
            $hasil_by_qc = $data->hasil_by_qc;
            $hasil_by_user = $data->hasil_by_user;
            if($request->input('status')=="close"){
              $hasil_by_user = $total->jasa + $total->material;
            }else if($request->input('status')=="qqc"){
              $hasil_by_qc = $total->jasa + $total->material;
            }
            $tgl_selesai = $data->tgl_selesai;
            $verify = $data->verify;
            //if($data->status!=$request->input('status') && $request->input('status')=='close'){
            if($data->status!=$request->input('status')){
              $tgl_selesai = date('Y-m-d H:i:s');
            }
            /*
              null = in TL
              1 = in SM
              2 = in TELKOM
              3 = siap rekon
            */
            if($request->verify != $data->verify){
              if($request->verify > $data->verify){
                //$verify = date('Y-m-d H:i:s');
                $status = "APPROVE";
              }
              else{
                //$verify = NULL;
                $status = "DECLINE";
              }
              if($request->catatan)
                $komen = $request->catatan;
              else
                $komen = $status;
              DB::table('maintenance_note')
              ->insert([
                'mt_id'     => $data->id,
                'catatan'   => $komen,
                'status'   => $status,
                'pelaku'    => $auth->id_karyawan,
                'ts'     => DB::raw('NOW()')
              ]);
            }
            $penyebab_id = null;
            $penyebab_name = null;
            if($data->jenis_order == 3){
              $penyebab = DB::table('maintenance_penyebab')->where('id', $request->penyebab_id)->first();
              $penyebab_id = $request->penyebab_id;
              $penyebab_name = @$penyebab->penyebab;
            }
            DB::table('maintaince')
              ->where('id', $data->id)
              ->update([
                'status_name'    => $request->input('status'),
                'status'    => $request->input('status'),
                'tgl_selesai'    => $tgl_selesai,
                'nama_odp'    => $request->nama_odp,
                'verify'    => $request->verify,
                'mtrcount'    => $mtrcount,
                'action'    => $request->input('action'),
                'koordinat'    => $request->input('koordinat'),
                'modified_at'     => DB::raw('NOW()'),
                'modified_by'     => $auth->id_karyawan,
                'hasil_by_user'  => $hasil_by_user,
                'hasil_by_qc'    => $hasil_by_qc,
                'action_cause'    => $request->input('action_cause'),
                'splitter'    => $request->input('splitter'),
                'port_idle'    => $request->input('port_idle'),
                'port_used'    => $request->input('port_used'),
                'redaman_awal'    => $request->input('redaman_awal'),
                'redaman_akhir'    => $request->input('redaman_akhir'),
                'penyebab_id'    => $penyebab_id,
                'penyebab_name'    => $penyebab_name,
              ]);
          });
        }
        if($request->input('status') != $data->status){
          // exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
        }
        return redirect('/marina/tech')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
        ]);
      }
  //   public function upload($id){
  //     $path2 = public_path().'/upload/maintenance/'.$id.'th';
  //     $th = array();
  //     if (file_exists($path2)) {
  //       $photos = File::allFiles($path2);
  //       foreach ($photos as $photo){
  //         array_push($th,(string) $photo);
  //       }
  //     }
  //     return view('tech.photos', compact('id','th'));
  //   }
  //   public function uploadSave(Request $request, $id){
  //     $files = $request->file('photo');
  //     $path = public_path().'/upload/maintenance/'.$id.'/';
  //     $th = public_path().'/upload/maintenance/'.$id.'th/';
  //     if (!file_exists($path)) {
  //       if (!mkdir($path, 0755, true))
  //         return 'gagal menyiapkan folder foto evidence';
  //     }
  //     if (!file_exists($th)) {
  //       if (!mkdir($th, 0755, true))
  //         return 'gagal menyiapkan folder foto evidence';
  //     }
  //     foreach ($files as $file) {
  //         $name = $file->getClientOriginalName();
  //         try {
  //           $moved = $file->move("$path", "$name");
  //           $img = new \Imagick($moved->getRealPath());
  //           $img->scaleImage(100, 150, true);
  //           $img->writeImage("$th/$name");
  //         }
  //         catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
  //           return 'gagal menyimpan foto evidence '.$name;
  //         }
  //     }
  //     return redirect('/tech')->with('alerts', [
  //       ['type' => 'success', 'text' => '<strong>SUKSES</strong> Upload Photo']
  //     ]);
  //   }
  //   private static function getGrupTech($id){
  //     $nama = DB::select('select *, (select nama from karyawan where id_karyawan = mr.nik1) as nama1, (select nama from karyawan where id_karyawan = mr.nik2) as nama2 from maintenance_regu mr where id = '.$id);
  //     if(count($nama)){
  //       return $nama[0]->nama1."&".$nama[0]->nama2;
  //     }
  //   }
  //   public static function sendtotelegram($id){
  //     $auth = session('auth');
  //     $chat_id = "-285944800";
  //     $data = DB::table('maintaince')
  //       ->select('maintaince.*', 'maintenance_regu.chat_id', 'psb_laporan.kordinat_odp', 'psb_laporan.nama_odp')
  //       ->leftJoin('maintenance_regu', 'maintaince.dispatch_regu_id', '=', 'maintenance_regu.id')
  //       ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
  //       ->where('maintaince.id', $id)->first();
    
  //     $nama = self::getGrupTech($data->dispatch_regu_id);
  //     $nm = User::karyawanExists($data->modified_by);
  //     $msg = "<b>Laporan ".$data->nama_order."</b>\n";
  //     $msg .= "=============\n";
  //     $msg .= "<b>Nomor Order</b> : <i>".$data->no_tiket."</i>\n";
  //     $msg .= "<b>Status</b> : <i>".$data->status."</i>\n";
  //     $msg .= "<b>Jenis Splitter</b> : <i>".$data->splitter."</i>\n";
  //     if($data->jenis_order == 3){
  //       $msg .= "<b>Penyebab</b> : <i>".$data->penyebab_name."</i>\n";
  //     }
  //     $msg .= "<b>Action</b> : <i>".$data->action_cause."</i>\n";
  //     $msg .= "<b>Action Detil</b> : <i>".$data->action."</i>\n";
  //     if($data->jenis_order == 3){
  //       $msg .= "<b>Detail ODP</b> : <i>".$data->nama_odp."( ".$data->kordinat_odp." )</i>\n";
  //     }
  //     if($data->jenis_order == 4){
  //       $msg .= "<b>Redaman Sebelum</b> : <i>".$data->redaman_awal."</i>\n";
  //       $msg .= "<b>Redaman Sesudah</b> : <i>".$data->redaman_akhir."</i>\n";
  //     }
  //     $msg .= "<b>Headline</b> : <i>".$data->headline."</i>\n";
  //     $msg .= "<b>Koordinat</b> : <i>".$data->koordinat."</i>\n";
  //     $msg .= "<b>Dilaporkan Oleh</b> : <i>".@$data->modified_by."/".@$nm->nama."</i>\n";
  //     $msg .= "<b>Dikerjakan Oleh</b> : <i>".$data->dispatch_regu_name."( ".$nama." )</i>\n\n";
  //     $msg .= "<b>Material dan Jasa</b>\n";
  //     $msg .= "=============\n";
  //     $mtr = DB::table('maintaince_mtr')
  //       ->select('maintaince_mtr.id_item', 'maintaince_mtr.qty', 'khs_maintenance.uraian', 'khs_maintenance.satuan')
  //       ->leftJoin('khs_maintenance', 'maintaince_mtr.id_item', '=', 'khs_maintenance.id_item')
  //       ->where('maintaince_id', $id)->get();
  //       //dd($mtr);
  //     foreach($mtr as $m){
  //       $msg .= " -<code>".$m->id_item."</code> ( <b>".$m->qty." ".$m->satuan."</b> )\n   * <i>".$m->uraian."</i>\n";
  //     }

  //     $no_tikett = substr($data->no_tiket, 0, 2);
  //     if ($no_tikett<>'IN'){
  //       if ($data->status=='close' && ($data->jenis_order=='3' or $data->jenis_order=='6' or $data->jenis_order=='9')){
  //           Telegram::sendMessage([
  //             'chat_id' => '-267883773',
  //             'parse_mode' => 'html',
  //             'text' => $msg
  //           ]);

  //           $nameFoto = 'Foto-GRID';
  //           $path = public_path().'/upload/maintenance/'.$id.'/'.$nameFoto.'.jpg';
  //           if (file_exists($path)){
  //             try {
  //               Telegram::sendPhoto([
  //                 'chat_id' => '-267883773',
  //                 'caption' => $nameFoto,
  //                 'photo' => $path
  //               ]);
  //             }catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
  //               Telegram::sendMessage([
  //                 'chat_id' => '-267883773',
  //                 'parse_mode' => 'html',
  //                 'text' => $e
  //               ]);
  //             }
  //           }   
  //       }
  //     }

  //     Telegram::sendMessage([
  //       'chat_id' => $data->chat_id,
  //       'parse_mode' => 'html',
  //       'text' => $msg
  //     ]);
  //     $foto = self::$photoInputst;
  //     if($data->jenis_order == 8){
  //       $foto = self::$photoOdpSehatst;
  //     }
  //     if($data->jenis_order == 1){
  //       $foto = self::$photoBenjarSt;
  //     }
  //     if($data->jenis_order == 10){
  //       $foto = self::$photoValidasiSt;
  //     }
  //     foreach($foto as $name) {
  //       $path = public_path().'/upload/maintenance/'.$id.'/'.$name.'.jpg';
  //       if (file_exists($path)){
  //         try {
  //           Telegram::sendPhoto([
  //             'chat_id' => $data->chat_id,
  //             'caption' => $name,
  //             'photo' => $path
  //           ]);
  //         }catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
  //           Telegram::sendMessage([
  //             'chat_id' => $data->chat_id,
  //             'parse_mode' => 'html',
  //             'text' => $e
  //           ]);
  //         }
  //       }
  //     }
  //   }
  //   public static function senddispatchtotelegram($id){
  //     //$auth = session('auth');
  //     //$chat_id = "-285944800";
  //     $data = DB::table('maintaince')
  //       ->select('maintaince.*', 'maintenance_regu.chat_id', 'psb_laporan.kordinat_odp', 'psb_laporan.nama_odp', 'psb_laporan.id_tbl_mj')
  //       ->leftJoin('maintenance_regu', 'maintaince.dispatch_regu_id', '=', 'maintenance_regu.id')
  //       ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
  //       ->where('maintaince.id', $id)->first();
  //     $nama = self::getGrupTech($data->dispatch_regu_id);
  //     $msg = "<b>Mohon Bantuan WO ".$data->nama_order."</b>\n";
  //     $msg .= "===========\n";
  //     $msg .= " <b>Ditugaskan Kepada</b> : <i>".$data->dispatch_regu_name."( ".$nama." )</i>\n";
  //     $msg .= " <b>Nomor Order</b> : <i>".$data->no_tiket."</i>\n";
  //     if($data->jenis_order == 3){
  //       $msg .= " <b>Detail ODP</b> : <i>".$data->nama_odp."( ".$data->kordinat_odp." )</i>\n";
  //     }
  //     $msg .= " <b>Headline</b> : <i>".$data->headline."</i>\n";
  //     $msg .= " <b>Koordinat</b> : <i>".$data->koordinat."</i>\n";
  //     Telegram::sendMessage([
  //       'chat_id' => $data->chat_id,
  //       'parse_mode' => 'html',
  //       'text' => $msg
  //     ]);
  //     if($data->koordinat){
  //       $koor = explode(",", $data->koordinat);
  //       if(count($koor)){
  //         Telegram::sendLocation([
  //           'chat_id' => $data->chat_id,
  //           'latitude' => $koor[0],
  //           'longitude' => $koor[1]
  //         ]);
  //       }
  //     }
  //     if($data->id_tbl_mj){
  //       if($data->order_from == 'ASSURANCE'){
  //         $folder = 'asurance';
  //       }else if($data->order_from == 'PSB'){
  //         $folder = "evidence";
  //       }
  //       foreach(self::$sendPhoto as $photo){
  //         $file = "/srv/htdocs/tomman1_psb/public/upload/".$folder."/".$data->id_tbl_mj."/".$photo.".jpg";
  //         if (file_exists($file)){
  //           Telegram::sendPhoto([
  //             'chat_id' => $data->chat_id,
  //             'caption' => "FOTO ".$photo,
  //             'photo' => $file
  //           ]);
  //         }else{
  //           Telegram::sendMessage([
  //             'chat_id' => $data->chat_id,
  //             'parse_mode' => 'html',
  //             'text' => "tidak ada foto ".$photo."\n".$file
  //           ]);
  //         }  
  //       }
  //     }
      
  //   }
  // public static function pleaseUpdate(){
  //   $regu = DB::table('maintenance_grup')->get();
  //     //$data = DB::select("select * from maintaince mt left join maintenance_regu mr on mt.dispatch_regu_id = mr.id where status is null");
  //     //  dd($data);
  //   foreach($regu as $r){
  //     $msg = "<b>Please Report Progress! \n".$r->grup."</b> \n";
  //     $msg .= date('Y-m-d h:i:s')."\n";
  //     $data = DB::table('maintaince')
  //       ->leftJoin('maintenance_regu', 'maintaince.dispatch_regu_id', '=', 'maintenance_regu.id')
  //       ->where('chat_id', $r->chat_id)
  //       ->whereNull('status')
  //       ->whereNull('refer')
  //       ->where('jenis_order', '!=', '1')
  //       ->orderBy('dispatch_regu_id', 'asc')
  //       ->get();
  //     $regu = '';
  //     foreach($data as $d){
  //       $nama = self::getGrupTech($d->dispatch_regu_id);
  //       if ($regu<>$d->dispatch_regu_id){
  //         $msg .= "\n";
  //         $msg .= "<b>".$d->dispatch_regu_name." ( ".$nama." )</b>\n";
  //       }
  //       if (!$d->status){ $laporan = "NO UPDATE"; } else { $laporan = $d->status_name; }
  //       $startDate = new \DateTime($d->created_at);
  //       $endDate = new \DateTime();
  //       $since_start = $startDate->diff($endDate);
  //       if ($since_start->h>0) $h = $since_start->h."h "; else $h = '';
  //       if ($since_start->i>0) $i = $since_start->i."min "; else $i = '';
  //       $result = $h. $since_start->i."min";
  //       $msg .= $result." | ";
  //       if (substr($d->no_tiket,0,2)<>"IN" || substr($d->no_tiket,0,2)<>"SC"){
  //         $msg .= $d->no_tiket." | ".$laporan."\n";
  //       } else {
  //         $msg .= "SC".$d->no_tiket." | ".$laporan."\n";
  //       }
  //       $regu = $d->dispatch_regu_id;
  //     }

  //     Telegram::sendMessage([
  //       'chat_id' => $r->chat_id,
  //       'parse_mode'=> 'HTML',
  //       'text' => $msg
  //     ]);
  //   }
  // }
  public function action_cause($id){
    return json_encode(DB::table('maintenance_cause_action')
                  ->select('action_cause as text', 'action_cause as id', 'status')
                  ->where('status', $id)->get());
  }
  // public function chart_status($id){
  //   /*
  //   $data = DB::select('select *,
  //     (select count(id) from maintaince where status = mca.status and tgl_selesai like "'.$id.'%") as status_val
  //     from maintenance_cause_action mca group by mca.status');
  //   $array = json_decode(json_encode($data), true);
  //   $status = array("sts"=>array_column($array, 'status'), "nilai" => array_column($array, 'status_val'));
  //   $data = DB::select('SELECT COUNT( * ) AS  rows ,  action_cause 
  //     FROM  maintaince where tgl_selesai like "'.$id.'%" and action_cause is not null
  //     GROUP BY  action_cause
  //     ORDER BY  action_cause');
  //   $array = json_decode(json_encode($data), true);
  //   $action = array("sts"=>array_column($array, 'action_cause'), "nilai" => array_column($array, 'rows'));
  //   $dataChart = array("sts"=> $status, "act" => $action);
  //   return $dataChart;
  //   */
  //   $data = DB::select('select *,
  //     (select count(id) from maintaince where jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "close") as close,
  //     (select count(id) from maintaince where jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "kendala pelanggan") as kendala_pelanggan,
  //     (select count(id) from maintaince where jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "kendala teknis") as kendala_teknis
  //     from maintenance_jenis_order mjo');
  //   return json_encode($data);
  // }
  // public function dashboard_verify($tgl){
  //   $data = DB::select('select mt.tl,k.nama, 
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 1 and verify is null and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as tl_benjar,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 1 and verify = 1 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as sm_benjar,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 1 and verify = 2 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as telkom_benjar,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 2 and verify is null and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as tl_gamas,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 2 and verify = 1 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as sm_gamas,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 2 and verify = 2 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as telkom_gamas,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 3 and verify is null and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as tl_odp_loss,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 3 and verify = 1 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as sm_odp_loss,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 3 and verify = 2 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as telkom_odp_loss,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 4 and verify is null and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as tl_remo,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 4 and verify = 1 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as sm_remo,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 4 and verify = 2 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as telkom_remo,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 5 and verify is null and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as tl_utilitas,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 5 and verify = 1 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as sm_utilitas,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 5 and verify = 2 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as telkom_utilitas,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 6 and verify is null and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as tl_insert_tiang,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 6 and verify = 1 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as sm_insert_tiang,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = 6 and verify = 2 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as telkom_insert_tiang,
  //     (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl ) as total
  //     from maintenance_tl mt left join karyawan k on mt.tl = k.id_karyawan where 1');
    
  //   return view('order.verify', compact('data'));
  // }
  // public function ajax_verify($tl, $ds, $dj, $dt){
  //   $q = "and verify = ".$ds;
  //   if($ds == 'null')
  //     $q = "and verify is ".$ds;
  //   $q2 = "";
  //   if($dj != "all")
  //     $q2 = "and m.jenis_order = ".$dj;
  //   $data = DB::select('select m.* from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN")) and tgl_selesai like "%'.$dt.'%" and mr.niktl = "'.$tl.'" '.$q.' '.$q2);
  //   //dd('select m.* from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where m.status = "close" and m.mtrcount>0 and tgl_selesai like "%'.$dt.'%" and mr.niktl = "'.$tl.'" '.$q.' '.$q2);
  //   return view('order.ajaxVerify', compact('data'));
  // }
  public function ajax_anak_tiket($id){
    $data = DB::select('select * from maintaince where refer='.$id);
    return json_encode($data);
  }
  // public function list_verify($tgl){
  //   $auth = session('auth');
  //   //dd($auth);
  //   if($auth->maintenance_level == 2 || $auth->maintenance_level == 3 || $auth->maintenance_level == 4 || $auth->maintenance_level == 1){
  //     $query = " ";
  //     if($auth->jenis_verif == 1){
  //       $query = ' and mt.verify is null';
  //     }else if($auth->jenis_verif == 2){
  //       $query = ' and mt.verify=1';
  //     }else if($auth->jenis_verif == 3){
  //       $query = ' and mt.verify=2';
  //     }
  //     $qtgl = "";
  //     if($tgl != "all")
  //       $qtgl = ' and mt.tgl_selesai like "'.$tgl.'%" ';
  //     $data = DB::select('select mt.*, mt.id as id_mt, pl.nama_odp,mt.nama_odp as nama_dp,
  //                   (select nama from karyawan where id_karyawan = mt.username1) as nama1, 
  //                   (select nama from karyawan where id_karyawan = mt.username2) as nama2,
  //                   (select nama from karyawan where id_karyawan = mt.username3) as nama3, 
  //                   (select nama from karyawan where id_karyawan = mt.username4) as nama4,
  //                   (select count(id) from maintaince where refer = mt.id) as jml_anak
  //                   from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id 
  //                   left join maintenance_regu mr on mt.dispatch_regu_id = mr.id 
  //                   left join psb_laporan pl on mt.psb_laporan_id = pl.id
  //                   where refer is null and mt.status = "close" '.$qtgl.'
  //           '.$query.' and (mt.mtrcount>0 or mt.action NOT IN ("CANCEL ORDER","REDAMAN AMAN")) order by tgl_selesai desc');
  //   }else{
  //     $data = new \stdClass();
  //   }
  //   return view('order.listverify', compact('data'));
  // }
  // public static function pleaseVerif(){
  //   $tgl = date('Y-m');
  //   $msg = "<b>Please Verifikasi WO!</b> \n";
  //   $msg .= date('Y-m-d h:i:s')."\n";
  //   // $data = DB::select('select mt.tl,k.nama, 
  //   //   (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and verify is null and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as tl
  //   //   from maintenance_tl mt left join karyawan k on mt.tl = k.id_karyawan where 1 order by tl desc');
  //   $data = DB::select('SELECT SUM(CASE WHEN verify is null and action NOT IN ("CANCEL ORDER","REDAMAN AMAN")
  //        THEN 1 ELSE 0
  //   END) AS TL,SUM(CASE WHEN verify = 1
  //        THEN 1 ELSE 0
  //   END) AS SM,SUM(CASE WHEN verify = 2
  //        THEN 1 ELSE 0
  //   END) AS TELKOM FROM maintaince');
    
  //   foreach($data as $no => $d){
  //       $msg .= "<b>1. TL ( ".$d->TL." WO )</b>\n";
  //       $msg .= "<b>2. SM ( ".$d->SM." WO )</b>\n";
  //       $msg .= "<b>3. TELKOM ( ".$d->TELKOM." WO )</b>\n";
  //   }

  //   Telegram::sendMessage([
  //     'chat_id' => "-200657991",
  //     'parse_mode'=> 'HTML',
  //     'text' => $msg
  //   ]);
  // }
  // public function logistik(){
  //   $data = new \stdClass();
  //   $project = DB::select('SELECT project as id, project as text FROM alista_material_keluar GROUP BY project ORDER BY project');
  //   $gudang = DB::select('SELECT nama_gudang as id, nama_gudang as text FROM alista_material_keluar GROUP BY nama_gudang ORDER BY nama_gudang');
  //   $mitra = DB::select('SELECT mitra as id, mitra as text FROM alista_material_keluar GROUP BY mitra ORDER BY mitra');
  //   return view('logistik.logistik', compact('project', 'gudang', 'mitra', 'data'));
  // }
  // public function listLogistik(Request $req){
  //   $sql = '';
  //     if($req->proaktif_id)
  //         $sql.='project = "'.$req->proaktif_id.'"';

  //     if($req->nama_gudang)
  //         $sql.='and nama_gudang = "'.$req->nama_gudang.'"';

  //     if($req->mitra)
  //         $sql.=' and mitra = "'.$req->mitra.'"';

  //     if($req->tgl)
  //         $sql.=' and tgl like "%'.$req->tgl.'%"';
  //   $data = DB::select('select * from alista_material_keluar where '.$sql);
  //   $project = DB::select('SELECT project as id, project as text FROM alista_material_keluar GROUP BY project ORDER BY project');
  //   $gudang = DB::select('SELECT nama_gudang as id, nama_gudang as text FROM alista_material_keluar GROUP BY nama_gudang ORDER BY nama_gudang');
  //   $mitra = DB::select('SELECT mitra as id, mitra as text FROM alista_material_keluar GROUP BY mitra ORDER BY mitra');
  //   return view('logistik.logistik', compact('data', 'project', 'gudang', 'mitra'));
  // }
  

  // //transaksional teknisi
  public function formValidasi($id){
    $data = DB::table('maintaince')->where('id', $id)->first();
    $list = DB::table('maintenance_validasi_port')->where('maintenance_id', $id)->get();
    return view('marina.tech.formValidasiPort', compact('data', 'list'));
  }
  public function formValidasiAwal($id){
    $data = DB::table('maintaince')->where('id', $id)->first();
    $list = DB::table('maintenance_validasi_port_awal')->where('maintenance_id', $id)->get();
    return view('marina.tech.formValidasiPortAwal', compact('data', 'list'));
  }
  public function getJsonPort($id){
    $json = DB::table('maintenance_validasi_port')->where('id', $id)->first();
    return json_encode($json);
  }
  public function getJsonPortAwal($id){
    $json = DB::table('maintenance_validasi_port_awal')->where('id', $id)->first();
    return json_encode($json);
  }
  public function getJsonReboundary($id){
    $json = DB::table('maintenance_reboundary')->where('id', $id)->first();
    return json_encode($json);
  }

  public function saveJumlahPortAwal($id, Request $req){
    $count = DB::table('maintenance_validasi_port_awal')->where('maintenance_id', $id)->count();
    if($req->kapasitas != $count){

      $insert=array();
      for($i=1;$i<=$req->kapasitas;$i++){
        $insert[] = ["maintenance_id"=>$id, "port"=>$i];
      }
      DB::table('maintenance_validasi_port_awal')->where('maintenance_id',$id)->delete();
      DB::table('maintenance_validasi_port_awal')->insert($insert);
      // DB::table('maintaince')->where('id', $id)->update(['kapasitas_odp' => $req->kapasitas]);
    }
    return back();
  }
  public function saveJumlahPort($id, Request $req){
    $count = DB::table('maintenance_validasi_port')->where('maintenance_id', $id)->count();
    if($req->kapasitas != $count){

      $insert=array();
      for($i=1;$i<=$req->kapasitas;$i++){
        $insert[] = ["maintenance_id"=>$id, "port"=>$i];
      }
      DB::table('maintenance_validasi_port')->where('maintenance_id',$id)->delete();
      DB::table('maintenance_validasi_port')->insert($insert);
      DB::table('maintaince')->where('id', $id)->update(['kapasitas_odp' => $req->kapasitas]);
    }
    return back();
  }
  public function saveValidasi($id, Request $req){
    DB::table('maintenance_validasi_port')->where('id', $req->id)->update(["jenis_layanan"=>$req->jenis_layanan,"no_layanan"=>$req->no_layanan,"qrcode_spl"=>$req->qrcode_spl,"qrcode_dc"=>$req->qrcode_dc,"status"=>$req->status]);
    return back();
  }
  public function saveValidasiAwal($id, Request $req){
    DB::table('maintenance_validasi_port_awal')->where('id', $req->id)->update(["jenis_layanan"=>$req->jenis_layanan,"no_layanan"=>$req->no_layanan,"qrcode_spl"=>$req->qrcode_spl,"qrcode_dc"=>$req->qrcode_dc,"status"=>$req->status]);
    return back();
  }
  public function saveReboundary($id, Request $req){
    if($req->vid){
      DB::table('maintenance_reboundary')->where('id', $req->vid)->update(["no_layanan"=>$req->no_layanan,"odp_lama"=>$req->odp_lama,"qrcode_dc"=>$req->qrcode_dc,"odp_baru"=>$req->odp_baru,"tarikan_lama"=>$req->tarikan_lama,"tarikan_baru"=>$req->tarikan_baru]);
    }else{
      DB::table('maintenance_reboundary')->insert(["maintenance_id"=>$id,"no_layanan"=>$req->no_layanan,"odp_lama"=>$req->odp_lama,"qrcode_dc"=>$req->qrcode_dc,"odp_baru"=>$req->odp_baru,"tarikan_lama"=>$req->tarikan_lama,"tarikan_baru"=>$req->tarikan_baru]);
    }
    return back();
  }
  public function nextstep($id, Request $req){
    DB::table('maintaince')->where('id', $req->id)->update(["step_id"=>$req->nextstep]);
    if($req->nextstep == 1){
      DB::statement('INSERT INTO maintenance_validasi_port_awal(maintenance_id, port, jenis_layanan, no_layanan, qrcode_spl, qrcode_dc, status) SELECT maintenance_id, port, jenis_layanan, no_layanan, qrcode_spl, qrcode_dc, status FROM maintenance_validasi_port WHERE maintenance_id = '.$req->id);
    }
    return back();
  }
  public function reboundaryForm($id){
    $data = DB::table('maintaince')->where('id', $id)->first();
    $list = DB::table('maintenance_reboundary')->where('maintenance_id', $id)->get();
    return view('marina.tech.reboundary', compact('data', 'list'));
  }
}
