@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <style>
  	.panel-body {

  		padding : 0px !important;
  	}
  	.panel-body table td,th {
  		padding : 2px !important;
  		font-size: 14px !important;
  	}
  	.panel {
  		border-radius: 3px !important;
  	}
    .redx {
      background-color: #00FFFF;
    }
  </style>
  <h3>{{ $title }}</h3>
  <span class="label label-tomman">Lastsync : {{ $lastsync[0]->UpdatedDatetime }}</span>
  <br />

  <div class="row">
  	<div class="col-sm-8">

  			<div class="panel panel-tomman">
  				<div class="panel-heading">Saldo by JAM (REGULER)</div>
  				<div class="panel-body">
  					<div class="table-responsive">
			  		<table class="table styledTable">
			  			<tr>
			  				<th rowspan="2">ORG</th>
			  				<th rowspan="2">HOLD</th>
			  				<th colspan="9">JAM</th>
			  				<th rowspan="2">TOTAL</th>
			  				<th colspan="2">DATA</th>
			  			</tr>
			  			<tr>
			  				<th width="50"><1</th>
			  				<th width="50"><2</th>
			  				<th width="50"><3</th>
			  				<th width="50" class="redx">3-4</th>
			  				<th width="50">4-6</th>
			  				<th width="50">6-8</th>
			  				<th width="50">8-10</th>
			  				<th width="50">10-12</th>
			  				<th width="50">>12</th>
			  				<th width="50"><12</th>
			  				<th width="50">>12</th>
			  			</tr>
			  			@foreach ($query as $result)
			  			<tr>
			  				<td class="areatext"><a href="/assuranceNossaNew/{{ $result->ORG }}">{{ $result->ORG }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/HOLD/{{ $result->ORG }}">{{ $result->HOLD }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x1jam/{{ $result->ORG }}">{{ $result->x1jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x2jam/{{ $result->ORG }}">{{ $result->x2jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x3jam/{{ $result->ORG }}">{{ $result->x3jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x3dan4jam/{{ $result->ORG }}">{{ $result->x3dan4jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x4dan6jam/{{ $result->ORG }}">{{ $result->x4dan6jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x6dan8jam/{{ $result->ORG }}">{{ $result->x6dan8jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x8dan10jam/{{ $result->ORG }}">{{ $result->x8dan10jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x10dan12jam/{{ $result->ORG }}">{{ $result->x10dan12jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x12jam/{{ $result->ORG }}">{{ $result->x12jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/ALL/{{ $result->ORG }}">{{ $result->Jumlah }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/xkurang12jam/{{ $result->ORG }}">{{ $result->xkurang12jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/ALL/x12jam/{{ $result->ORG }}">{{ $result->x12jam }}</a></td>
			  			</tr>
			  			@endforeach
			  		</table>
			  	</div>
			  </div>
	  	</div>
  	</div>
  	<div class="col-sm-8">

  			<div class="panel panel-tomman">
  				<div class="panel-heading">Saldo by JAM (REGULER MYI)</div>
  				<div class="panel-body">
  					<div class="table-responsive">
			  		<table class="table styledTable">
			  			<tr>
			  				<th rowspan="2">ORG</th>
			  				<th rowspan="2">HOLD</th>
			  				<th colspan="7">JAM</th>
			  				<th rowspan="2">TOTAL</th>
			  				<th colspan="2">DATA</th>
			  			</tr>
			  			<tr>
			  				<th width="50">0-2</th>
			  				<th width="50">2-4</th>
			  				<th width="50">4-6</th>
			  				<th width="50">6-8</th>
			  				<th width="50">8-10</th>
			  				<th width="50">10-12</th>
			  				<th width="50">>12</th>
			  				<th width="50"><12</th>
			  				<th width="50">>12</th>
			  			</tr>
			  			@foreach ($query_myi as $result)
			  			<tr>
			  				<td class="areatext">{{ $result->ORG }}</td>
			  				<td align="center"><a href="/assuranceNossaList/6/HOLD/{{ $result->ORG }}">{{ $result->HOLD }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x0dan2jam/{{ $result->ORG }}">{{ $result->x0dan2jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x2dan4jam/{{ $result->ORG }}">{{ $result->x2dan4jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x4dan6jam/{{ $result->ORG }}">{{ $result->x4dan6jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x6dan8jam/{{ $result->ORG }}">{{ $result->x6dan8jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x8dan10jam/{{ $result->ORG }}">{{ $result->x8dan10jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x10dan12jam/{{ $result->ORG }}">{{ $result->x10dan12jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x12jam/{{ $result->ORG }}">{{ $result->x12jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/ALL/{{ $result->ORG }}">{{ $result->Jumlah }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/xkurang12jam/{{ $result->ORG }}">{{ $result->xkurang12jam }}</a></td>
			  				<td align="center"><a href="/assuranceNossaList/6/x12jam/{{ $result->ORG }}">{{ $result->x12jam }}</a></td>
			  			</tr>
			  			@endforeach
			  		</table>
			  	</div>
			  </div>
	  	</div>
  	</div>
  </div>
@endsection
