<?php

namespace App\DA;

use Illuminate\Database\Eloquent\Model;

class tmpodpModel extends Model
{
    protected $table 	= 'tmpodp';
    public $timestamps 	= false;
}
