@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    MONITORING WORK ORDER CCAN
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <!--
    <li class="{{ (Request::path() == 'migrasi') ? 'active' : '' }}"><a href="/migrasi">ALL MIGRASI</a></li>
    <li class="{{ (Request::segment(2) == 'report') ? 'active' : '' }}"><a href="/migrasi/report">REPORT</a></li>
    <li class="{{ (Request::segment(2) == 'upload') ? 'active' : '' }}"><a href="/migrasi/upload">UPLOAD</a></li>
    -->
    <li class="{{ (Request::path() == 'ccan') ? 'active' : '' }}"><a href="/ccan">CCAN</a></li>
    <li class="{{ (Request::segment(2) == 'report-ccan') ? 'active' : '' }}"><a href="/migrasi/report-ccan">REPORT</a></li>
    <li class="{{ (Request::segment(2) == 'upload-ccan') ? 'active' : '' }}"><a href="/migrasi/upload-ccan">UPLOAD</a></li>
    
  </ul>
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Order</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $no => $data)
      <tr>
        <td>{{ ++$no }}</td>
		@if ($data->id<>"")
        <td><span><a href="/{{ $data->id }}">{{ $data->ND ? : '' }}</a></span><br/>
		@else
		<td><span>{{ $data->ND ? : '' }}</span><br/>
		@endif	
        <span>{{ $data->Nama ? : '' }}</span> <span>{{ $data->Alamat ? : '' }}</span><br/>
        <span>{{ $data->layanan ? : '' }} </span> <span>{{ $data->jenis_layanan ? : '' }} </span> <span>{{ $data->sid ? : '' }} </span><br/>
        <span>{{ $data->service ? : ''}} </span> <span>{{ $data->pic ? : ''}} </span></td>
        <td>
        @if(!empty($data->id_r))
          <a href="migrasi/dispatch/{{ $data->ND }}" class="label label-info" data-toggle="tooltip" title="{{ $data->uraian }}">Re-Dispatch</a><br />
          <span class="label label-warning" title="{{ $data->uraian }}">{{ $data->uraian }}</span><br />
          <span class="label label-info" title="{{ $data->uraian }}">{{ $data->status_laporan_teknisi }}</span>
        @else
          <a href="migrasi/dispatch/{{ $data->ND }}" class="label label-info">Dispatch</a>
        @endif
        <a href="orderCcan/{{ $data->ND }}" class="label label-info">Edit</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  <script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
@endsection
