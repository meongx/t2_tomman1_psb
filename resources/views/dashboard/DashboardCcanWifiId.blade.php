@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }

    .left-td {
      text-align: left;;
    }

    .warna1{
      background-color: #9abcf4;
    }

    .warna2{
      background-color: #e0962f;
    }

    .warna3{
      background-color: #3ec156;
    }

    .warna4{
      background-color: #fca4a4;
    }

    .warna5{
      background-color: #f2de5e ;
    }

    .warna6{
      background-color: #e2410b;
    }

    .warna7{
      background-color: #309bff;
    }

    .text1{
      color: black;
    }

    .link:link{
      color: black;
    }

    .link:visited{
      color: black;
    }
</style>
  <div id="screnshoot">  
    <!-- <h3>Periode {{ $tgl }}</h3> -->
    
    <div class="panel panel-primary"> 
        <div class="panel-heading">Dashboard</div>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-6">
                <h5>Dashboard WIFI ID  CCAN [Periode {{$tgl}}]</h5>
                <table class="table table-striped table-bordered dataTable">
                  <tr></tr>
                    <th>STATUS</th>
                    <th>INNER</th>
                    <th>BBR</th>
                    <th>KDG</th>
                    <th>TJL</th>
                    <th>BLC</th>
                    <th>JUMLAH</th>
                  </tr>
                  <?php
                    $no_update_jumlah = 0;
                    $no_update_inner = 0;
                    $no_update_bbr = 0;
                    $no_update_kdg = 0;
                    $no_update_tjl = 0;
                    $no_update_blc = 0;
                    $total_inner = 0;                                                                                                                                                                                                                                         
                    $total_bbr = 0;
                    $total_kdg = 0;
                    $total_tjl = 0;
                    $total_blc = 0;
                    $total = 0;
                  ?>
               
                    @foreach ($data as $result)
                        @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                        <tr>
                            <td>{{ $result->laporan_status }}</td>
                            <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/{{ $result->laporan_status }}/INNER">{{ $result->WO_INNER }}</a></td>
                            <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/{{ $result->laporan_status }}/BBR">{{ $result->WO_BBR }}</a></td>
                            <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/{{ $result->laporan_status }}/KDG">{{ $result->WO_KDG }}</a></td>
                            <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/{{ $result->laporan_status }}/TJL">{{ $result->WO_TJL }}</a></td>
                            <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/{{ $result->laporan_status }}/BLC">{{ $result->WO_BLC }}</a></td>
                            <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/{{ $result->laporan_status }}/ALL">{{ $result->jumlah }}</a></td>
                        </tr>
                        <?php
                          $total_inner += $result->WO_INNER;
                          $total_bbr += $result->WO_BBR;
                          $total_kdg += $result->WO_KDG;
                          $total_tjl += $result->WO_TJL;
                          $total_blc += $result->WO_BLC;
                        ?>
                        @else
                        <?php
                          $no_update_jumlah += $result->jumlah;
                          $no_update_inner += $result->WO_INNER;
                          $no_update_bbr += $result->WO_BBR;
                          $no_update_kdg += $result->WO_KDG;
                          $no_update_tjl += $result->WO_TJL;
                          $no_update_blc += $result->WO_BLC;
                        ?>

                        @endif
                    @endforeach
            
                  <tr>
                    <td>NO UPDATE</td>
                    <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/NO UPDATE/INNER">{{ $no_update_inner }}</a></td>
                    <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/NO UPDATE/BBR">{{ $no_update_bbr }}</a></td>
                    <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/NO UPDATE/KDG">{{ $no_update_kdg }}</a></td>
                    <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/NO UPDATE/TJL">{{ $no_update_tjl }}</a></td>
                    <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/NO UPDATE/BLC">{{ $no_update_blc }}</a></td>
                    <td><a href="/dashboard/dashboard-wifi/{{ $tgl }}/NO UPDATE/ALL">{{ $no_update_jumlah }}</a></td>
                  </tr>
                  <?php
                  $total_inner = $total_inner + $no_update_inner;
                  $total_bbr = $total_bbr + $no_update_bbr;
                  $total_kdg = $total_kdg + $no_update_kdg;
                  $total_tjl = $total_tjl + $no_update_tjl;
                  $total_blc = $total_blc + $no_update_blc;
                  $total = $total_inner + $total_bbr + $total_kdg + $total_tjl + $total_blc;
                  ?>
                  <tr>
                    <td class="warna7 text1">TOTAL</td>
                    <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-wifi/{{ $tgl }}/ALL/INNER">{{ $total_inner }}</a></td>
                    <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-wifi/{{ $tgl }}/ALL/BBR">{{ $total_bbr }}</a></td>
                    <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-wifi/{{ $tgl }}/ALL/KDG">{{ $total_kdg }}</a></td>
                    <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-wifi/{{ $tgl }}/ALL/TJL">{{ $total_tjl }}</a></td>
                    <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-wifi/{{ $tgl }}/ALL/BLC">{{ $total_blc }}</a></td>
                    <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-wifi/{{ $tgl }}/ALL/ALL">{{ $total }}</a></td>
                  </tr>
                </table>
            </div> 

            <div class="col-sm-6">
              <h5>Dashboard DATIN CCAN [Periode {{$tgl}}]</h5>
              <table class="table table-striped table-bordered dataTable">
                <tr></tr>
                  <th>STATUS</th>
                  <th>INNER</th>
                  <th>BBR</th>
                  <th>KDG</th>
                  <th>TJL</th>
                  <th>BLC</th>
                  <th>JUMLAH</th>
                </tr>
                <?php
                  $no_update_jumlah = 0;
                  $no_update_inner = 0;
                  $no_update_bbr = 0;
                  $no_update_kdg = 0;
                  $no_update_tjl = 0;
                  $no_update_blc = 0;
                  $total_inner = 0;                                                                                                                                                                                                                                         
                  $total_bbr = 0;
                  $total_kdg = 0;
                  $total_tjl = 0;
                  $total_blc = 0;
                  $total = 0;
                ?>
             
                  @foreach ($dataDatin as $result)
                      @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                      <tr>
                          <td>{{ $result->laporan_status }}</td>
                          <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/{{ $result->laporan_status }}/INNER">{{ $result->WO_INNER }}</a></td>
                          <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/{{ $result->laporan_status }}/BBR">{{ $result->WO_BBR }}</a></td>
                          <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/{{ $result->laporan_status }}/KDG">{{ $result->WO_KDG }}</a></td>
                          <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/{{ $result->laporan_status }}/TJL">{{ $result->WO_TJL }}</a></td>
                          <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/{{ $result->laporan_status }}/BLC">{{ $result->WO_BLC }}</a></td>
                          <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/{{ $result->laporan_status }}/ALL">{{ $result->jumlah }}</a></td>
                      </tr>
                      <?php
                        $total_inner += $result->WO_INNER;
                        $total_bbr += $result->WO_BBR;
                        $total_kdg += $result->WO_KDG;
                        $total_tjl += $result->WO_TJL;
                        $total_blc += $result->WO_BLC;
                      ?>
                      @else
                      <?php
                        $no_update_jumlah += $result->jumlah;
                        $no_update_inner += $result->WO_INNER;
                        $no_update_bbr += $result->WO_BBR;
                        $no_update_kdg += $result->WO_KDG;
                        $no_update_tjl += $result->WO_TJL;
                        $no_update_blc += $result->WO_BLC;
                      ?>

                      @endif
                  @endforeach
          
                <tr>
                  <td>NO UPDATE</td>
                  <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/NO UPDATE/INNER">{{ $no_update_inner }}</a></td>
                  <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/NO UPDATE/BBR">{{ $no_update_bbr }}</a></td>
                  <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/NO UPDATE/KDG">{{ $no_update_kdg }}</a></td>
                  <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/NO UPDATE/TJL">{{ $no_update_tjl }}</a></td>
                  <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/NO UPDATE/BLC">{{ $no_update_blc }}</a></td>
                  <td><a href="/dashboard/dashboard-datin/{{ $tgl }}/NO UPDATE/ALL">{{ $no_update_jumlah }}</a></td>
                </tr>
                <?php
                $total_inner = $total_inner + $no_update_inner;
                $total_bbr = $total_bbr + $no_update_bbr;
                $total_kdg = $total_kdg + $no_update_kdg;
                $total_tjl = $total_tjl + $no_update_tjl;
                $total_blc = $total_blc + $no_update_blc;
                $total = $total_inner + $total_bbr + $total_kdg + $total_tjl + $total_blc;
                ?>
                <tr>
                  <td class="warna7 text1">TOTAL</td>
                  <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-datin/{{ $tgl }}/ALL/INNER">{{ $total_inner }}</a></td>
                  <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-datin/{{ $tgl }}/ALL/BBR">{{ $total_bbr }}</a></td>
                  <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-datin/{{ $tgl }}/ALL/KDG">{{ $total_kdg }}</a></td>
                  <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-datin/{{ $tgl }}/ALL/TJL">{{ $total_tjl }}</a></td>
                  <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-datin/{{ $tgl }}/ALL/BLC">{{ $total_blc }}</a></td>
                  <td class="warna7 text1"><a class="link" href="/dashboard/dashboard-datin/{{ $tgl }}/ALL/ALL">{{ $total }}</a></td>
                </tr>
              </table>
            </div>      
          </div>
        </div>
    </div>
    
    <div class="panel panel-primary">
        <div class="panel-heading">Dashboard Undispatch</div>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-6">
              <form>
                <div class="row">
                    <div class="col-md-4">
                      Filter :
                      <select class="form-control" id="jenis">
                          <option value="ALL">ALL</option>
                          <option value="wifi">Wifi Id</option>
                          <option value="astinet">Astinet</option>
                          <option value="vpn">Vpn Id</option>

                      </select>
                     
                    </div>
                </div>
                <select>
                    
                </select>
              </form>

              <div id="undispatch">
                <table class="table table-striped table-bordered dataTable">
                    <tr>
                        <th>Area</th>
                        <th>Undispatch</th>
                    </tr>

                    <?php $jumlah = 0; $jmlKendala = 0;?>
                    @foreach($dataundispatch as $potensi)
                      <tr>
                          <td>{{ $potensi->area }}</td>
                          <td><a href="/dashboard/undispatch-ccan/{{ date('Y-m-d') }}/{{ $potensi->area }}/ALL">{{ $potensi->undispatch ?: '0'}}</a></td>
                          <?php 
                              $jumlah += $potensi->undispatch;
                          ?>
                      </tr>
                    @endforeach
                    <tr>
                        <td>Total</td>
                        <td><a href="/dashboard/undispatch-ccan/{{ date('Y-m-d') }}/ALL/ALL">{{ $jumlah }}</a></td>
                    </tr>
                </table>
              </div>
            </div> 
       
          </div>
          
        </div>
    </div>

  </div>

  <div class="panel panel-primary">
  <div class="panel-heading">Matrik HD CCAN</div>
  <div class="panel-body table-responsive">
    <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered">
              <tr>
                  <th width="2%">No</th>
                  <th width="5%">NIK</th>
                  <th width="8%">Nama HD</th>
                  <th width="5%">Total Dispatch</th>
                  <th>List</th>
              </tr>
              
              @foreach($matrik as $no=>$data)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $data['nik'] }}</td>
                    <td>{{ $data['nama'] }}</td>
                    <td>{{ $data['jumlah'] }}</td>
                    <td class="left-td">
                        @foreach($data['dataWo'] as $wo)
                            <span class="label label-primary">{{ $wo['Ndem'] }}</span>
                        @endforeach
                    </td>
                </tr>
              @endforeach
          </table>
        </div>
    </div>
  </div>
</div>
@endsection

@section('plugins')
  <script type="text/javascript">
      $(function(){
          $('#jenis').select2();

          $('#jenis').click(function(){
            let a     = $('#jenis').val(),
                url = "/dashboard/dashboad-wifi-ajax/"+a;
  
            $.ajax({
                url : url,
                dataType : 'html',
                success : function(data){
                    $('#undispatch').html(data);
                },
                error : function(){
                    alert('gagal');
                }
            })
              
          })
      })
  </script>
@endsection