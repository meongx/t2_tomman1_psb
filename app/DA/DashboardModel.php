<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class DashboardModel{

  public static function unspec(){
    $query = DB::SELECT('
        SELECT a.Workzone,count(*) as Jumlah FROM data_nossa_1 a WHERE a.Incident_Symptom = "PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC" group by a.Workzone order by Jumlah DESC
      ');
    return $query;
  }

  public static function kehadiran($date){
    $query = DB::SELECT('
        SELECT
        *,
        SUM(CASE WHEN (e.squad="ALPHA.ASR" OR e.squad="ALPHA.WILSUS") THEN 1 ELSE 0 END) as alphaasr,
        count(*) as jumlah
        FROM
        1_2_employee c
      LEFT JOIN regu a ON (a.nik1 = c.nik OR a.nik2 = c.nik)
      LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      LEFT JOIN absen d ON c.nik = d.nik
      LEFT JOIN employee_squad e ON c.squad = e.squad_id
        WHERE
          a.ACTIVE <> 0 AND
          DATE(d.tglAbsen) = "'.$date.'"
        GROUP BY b.title
      ');
    return $query;
  }

  public static function kehadiran2($date){
    $query = DB::SELECT('
        SELECT
        *,
        count(*) as jumlah
        FROM
        1_2_employee c
      LEFT JOIN regu a ON (a.nik1 = c.nik OR a.nik2 = c.nik)
      LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      LEFT JOIN absen d ON c.nik = d.nik
        WHERE
          a.ACTIVE <> 0 AND
          DATE(d.tglAbsen) = "'.$date.'" AND
          b.ket_sektor = 1
        GROUP BY b.title
      ');
    return $query;
  }

  public static function detailJumlahAbsen($sektor, $date){
      $sql = 'SELECT
                SUM(CASE WHEN (a.approval=1 and b.sttsWo=0) THEN 1 ELSE 0 END) as approveAsr,
                SUM(CASE WHEN (a.approval=3 and b.sttsWo=0) THEN 1 ELSE 0 END) as declineAsr,
                SUM(CASE WHEN (a.approval=1 and b.sttsWo=1) THEN 1 ELSE 0 END) as approveProv,
                SUM(CASE WHEN (a.approval=3 and b.sttsWo=1) THEN 1 ELSE 0 END) as declineProv,
                SUM(CASE WHEN b.sttsWo=0 THEN 1 ELSE 0 END) as requestAsr,
                SUM(CASE WHEN b.sttsWo=1 THEN 1 ELSE 0 END) as requestProv
              FROM
                absen a
                LEFT JOIN regu b ON (b.nik1 = a.nik OR b.nik2 = a.nik)
              WHERE
                b.ACTIVE <> 0 AND
                b.mainsector="'.$sektor.'" AND
                DATE(a.date_created)="'.$date.'"';

      return DB::select($sql);
  }

  public static function unspeclist($sto){
    $where_sto = '';
    if ($sto<>"ALL"){
      $where_sto = ' AND a.Workzone = "'.$sto.'"';
    }
    $query = DB::SELECT('
        SELECT
        a.*
        FROM
          data_nossa_1 a
        WHERE
          a.Incident_Symptom = "PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC"
        '.$where_sto.'
      ');
    return $query;
  }

  public static function panjangtarikan($tgl){

    $query = DB::SELECT('
      SELECT
        *,
        SUM( i.num ) AS dc,
        dt.id as id_dt
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
       -- LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN rfc_sal i on plm.id_item_bantu=i.id_item_bantu
       LEFT JOIN roc ON dt.Ndem = roc.no_tiket
       LEFT JOIN Data_Pelanggan_Starclick psb ON dt.Ndem = psb.orderId
       LEFT JOIN mdf m ON roc.sto_trim = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       dt.tgl like "%'.$tgl.'%" AND
       pl.status_laporan = 1
       GROUP BY dt.id
    ');
    return $query;
  }

  public static function get_assuranceStatusOpen($witel){
    if ($witel=="ALL"){
      $where_witel = "";
    } else {
      $where_witel = ' AND a.Witel = "'.$witel.'"';
    }
    $query = DB::SELECT('
      SELECT
        d.laporan_status,
        count(*) as jumlah
      FROM
        data_nossa_1 a
        LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      WHERE
      a.Source <> "PROACTIVE_TICKET" AND
      a.Incident_Symptom NOT LIKE "%NON NUMBERING%" AND
      a.Customer_Name <> "ASSET DUMMY SEGMENT DCS" AND
      a.Induk_Gamas = ""
      '.$where_witel.'
      GROUP BY d.laporan_status
    ');
    return $query;
  }

  public static function by_actual_solution($field,$periode,$mitrax){
     if ($mitrax=="ALL"){
       $where_mitra = '';
     } else {
       $where_mitra = ' AND c.mitra="'.$mitrax.'"';
     }
     $query = DB::SELECT('
     SELECT
       a.'.$field.' as ACTUAL_SOLUTION,
       count(*) as JUMLAH
     FROM
       nonatero_detil a
     LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
     LEFT JOIN regu c ON b.id_regu = c.id_regu
     WHERE a.THNBLN="'.$periode.'" '.$where_mitra.' GROUP BY a.'.$field);
     return $query;
   }


    public static function get_last_sync_nossa(){
      return DB::SELECT('select UpdatedDatetime from data_nossa_1 limit 1');
    }

        public static function nonatero_group_by($par,$periode,$mitrax){
          if ($mitrax == "ALL"){
            $q_mitra = '';
          } else {
            $q_mitra = ' AND d.mitra = "'.$mitrax.'"';
          }

          $query = DB::SELECT('
            SELECT
              '.$par.' as ACTUAL_SOLUTION,
              count(*) as JUMLAH
            FROM
              nonatero_detil a
            LEFT JOIN nonatero_detil_actual_solution b ON a.ACTUAL_SOLUTION = b.ACTUAL_SOLUTION
            LEFT JOIN dispatch_teknisi dt ON a.TROUBLE_NO = dt.Ndem
            LEFT JOIN regu d ON dt.id_regu = d.id_regu
            WHERE
              a.THNBLN = "'.$periode.'" AND
              a.IS_GAMAS = 0
              '.$q_mitra.'
            GROUP BY '.$par.'
            ORDER BY JUMLAH ASC
          ');
          return $query;
        }

        public static function getNossa($channel,$witel,$org){
        $q_channel = '';
        if ($channel<>"ALL"){
          $q_channel = ' AND a.Channel = "'.$channel.'"';
        }
        $q_witel = '';
        if ($witel<>"ALL"){
          $q_witel = ' AND a.Witel = "'.$witel.'"';
        }
        date_default_timezone_set('Asia/Makassar');
        $query = DB::SELECT('
          SELECT
            '.$org.' as ORG,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,"'.date('Y-m-d H:i:s').'")<0   THEN 1 ELSE 0 END) as HOLD,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<1 THEN 1 ELSE 0 END) as x1jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=1 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<2 THEN 1 ELSE 0 END) as x2jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<3 THEN 1 ELSE 0 END) as x3jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=3 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<4 THEN 1 ELSE 0 END) as x3dan4jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<2 THEN 1 ELSE 0 END) as x0dan2jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<4 THEN 1 ELSE 0 END) as x2dan4jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=4 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<6 THEN 1 ELSE 0 END) as x4dan6jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=6 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<8 THEN 1 ELSE 0 END) as x6dan8jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=8 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<10 THEN 1 ELSE 0 END) as x8dan10jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=10 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<12 THEN 1 ELSE 0 END) as x10dan12jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=12 THEN 1 ELSE 0 END) as x12jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<12 THEN 1 ELSE 0 END) as xkurang12jam,
            count(*) as Jumlah
          FROM
            data_nossa_1 a
            LEFT JOIN mdf b ON a.Workzone = b.mdf
          WHERE
            a.Witel <> "" AND
            a.Source <> "PROACTIVE_TICKET" AND
            a.Incident_Symptom NOT LIKE "%NON NUMBERING%" AND
            a.Customer_Name <> "ASSET DUMMY SEGMENT DCS" AND
            a.Induk_Gamas = ""
            '.$q_channel.'
            '.$q_witel.'
          GROUP BY
            '.$org.'
        ');
        return $query;
      }

      public static function getNossaList($channel,$periode,$witel){
        date_default_timezone_set('Asia/Makassar');

        $q_channel = '';
        if ($channel<>"ALL"){
          $q_channel = ' AND a.Channel = "'.$channel.'"';
        }

        if ($witel == "ALL"){
          $q_witel = '';
        } else {
          $q_witel = 'AND (a.Witel = "'.$witel.'" OR h.sektor_asr = "'.$witel.'")';
        }

        switch ($periode) {
          case 'x0dan2jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<2';
          break;

          case 'x1jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<1';
          break;

          case 'x2jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=1 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<2';
          break;

          case 'x3jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<3';
          break;
          case 'x3dan4jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=3 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<4';
          break;

          case 'x2dan4jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<4';
          break;
          case 'x4dan6jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=4 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<6';
          break;
          case 'x6dan8jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=6 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<8';
          break;
          case 'x8dan10jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR ,a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=8 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<10';
          break;
          case 'x10dan12jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=10 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<12';
          break;
          case 'x12jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=12';
          break;
          case 'xkurang12jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<12';
          break;
          case 'HOLD':
            //$q_periode = 'AND (UmurDatetime BETWEEN "'.date('Y-m-d 00:00:00').'" AND "'.date('Y-m-d 08:00:05').'") OR a.UmurDatetime>="'.date('Y-m-d 17:00:00').'"';
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,"'.date('Y-m-d H:i:s').'")<0';
          break;

          default:
              $q_periode = '';
            break;
        }

        $query = DB::SELECT('
          SELECT
            a.*,
            TIMESTAMPDIFF(HOUR,a.UmurDatetime,"'.date('Y-m-d H:i:s').'") as Umur,
            c.uraian as Team,
            g.laporan_status as Status,
            e.action as Action,
            f.penyebab as Sebab
          FROM
            data_nossa_1 a
            LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
            LEFT JOIN regu c ON b.id_regu = c.id_regu
            LEFT JOIN psb_laporan d ON b.id = d.id_tbl_mj
            LEFT JOIN psb_laporan_action e ON d.action = e.laporan_action_id
            LEFT JOIN psb_laporan_penyebab f ON f.idPenyebab = d.penyebabId
            LEFT JOIN psb_laporan_status g ON d.status_laporan = g.laporan_status_id
            LEFT JOIN mdf h ON a.Workzone = h.mdf
          WHERE
            a.Source <> "PROACTIVE_TICKET" AND
            a.Incident_Symptom NOT LIKE "%NON NUMBERING%" AND
            a.Customer_Name <> "ASSET DUMMY SEGMENT DCS" and
            a.Induk_Gamas = ""
            '.$q_witel.'
            '.$q_periode.'
            '.$q_channel.'
          ');
        return $query;
      }

      public static function PI($transaksi,$dateStart,$dateEnd){
        switch ($transaksi){
          case "NEWSALES" :
            $q_transaksi = ' AND a.JENISPSB LIKE ("%AO%") ';
          break;
          case "2ND_STB" :
            $q_transaksi = ' AND (a.PACKAGE_NAME LIKE "%SWSTBHYBR2%" OR a.PACKAGE_NAME LIKE "%2nd STB%" OR a.PACKAGE_NAME LIKE "%Second%")';
          break;
          case "MO" :
            $q_transaksi = ' AND (a.PACKAGE_NAME NOT LIKE "%SWSTBHYBR2%" OR a.PACKAGE_NAME NOT LIKE "%2nd STB%" OR a.PACKAGE_NAME NOT LIKE "%Second%") AND a.JENISPSB LIKE "MO%"';
          break;
          default :
            $q_transaksi = '';
          break;
        }
        $query = 'SELECT count(*) as jumlah,';
        for ($i=0;$i<24;$i++) :
          $query .= 'SUM(CASE WHEN HOUR(a.ORDER_DATE)='.$i.' THEN 1 ELSE 0 END) as h'.$i.',';
        endfor;
        $query .= 'a.WITEL FROM 4_0_master_order_1_log_pi a
        WHERE
          date(a.ORDER_DATE) BETWEEN "'.$dateStart.'" AND "'.$dateEnd.'"
          '.$q_transaksi.'
        GROUP BY a.WITEL';
        $result = DB::connection('mysql4')->SELECT($query);
        return $result;
      }

      public static function PIlist($witel,$jam,$dateStart,$dateEnd){
        if ($witel=="ALL"){
          $q_witel = '';
        } else {
          $q_witel = 'AND WITEL = "'.$witel.'"';
        }
        if ($jam=="ALL"){
          $q_jam = '';
        } else {
          $q_jam = 'AND HOUR(ORDER_DATE)='.$jam;
        }
        $query = DB::connection('mysql4')->SELECT('SELECT * FROM 4_0_master_order_1_log_pi WHERE date(ORDER_DATE) BETWEEN "'.$dateStart.'" AND "'.$dateEnd.'" '.$q_witel.' '.$q_jam.' ');
        return $query;
      }

      public static function kpi_assurance_mitra($periode){
        $query = DB::SELECT('SELECT c.mitra_main as mitra FROM nonatero_detil a LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.NDEM LEFT JOIN regu c ON b.id_regu = c.id_regu WHERE a.THNBLN="'.$periode.'" GROUP BY c.mitra_main');
        return $query;
      }

      public static function kpi_assurance_mitrax($periode,$mitrax){
        if ($mitrax == "ALL"){
          $q_mitra = '';
        } else {
          $q_mitra = ' AND d.mitra_main = "'.$mitrax.'"';
        }

        $query = DB::SELECT('
          SELECT
            count(*) as JUMLAH,
            SUM(a.IS_12HS) as R1DS,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  a.TGL_TECH_CLOSE)<48 THEN 1 ELSE 0 END) as R48,
            (SELECT COUNT(*) FROM nonatero_detil_gaul aa LEFT JOIN dispatch_teknisi bb ON aa.TROUBLE_NO = bb.Ndem LEFT JOIN regu cc ON bb.id_regu = cc.id_regu LEFT JOIN psb_laporan dd ON bb.id = dd.id_tbl_mj WHERE dd.status_laporan = 1 AND cc.mitra_main = d.mitra_main AND aa.THNBLN = "'.$periode.'") as GAUL,
            d.mitra_main as sektor_asr
          FROM
            mdf b
          LEFT JOIN nonatero_detil a ON b.mdf = a.STO
          LEFT JOIN dispatch_teknisi c ON a.TROUBLE_NO = c.Ndem
          LEFT JOIN regu d ON c.id_regu = d.id_regu
          LEFT JOIN psb_laporan e ON c.id = e.id_tbl_mj
          WHERE
            a.THNBLN LIKE "'.$periode.'%" AND
            a.IS_GAMAS = 0 AND
            e.status_laporan = 1
            '.$q_mitra.'
          GROUP BY
            d.mitra_main
        ');
        return $query;
      }
      public static function kpi_assurance_mitra_tech($periode,$mitrax){
        if ($mitrax == "ALL"){
          $q_mitra = '';
        } else {
          $q_mitra = ' AND d.mitra = "'.$mitrax.'"';
        }

        $query = DB::SELECT('
          SELECT
            count(*) as JUMLAH,
            SUM(a.IS_12HS) as R1DS,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  a.TGL_TECH_CLOSE)<48 THEN 1 ELSE 0 END) as R48,
            (SELECT COUNT(*) FROM nonatero_detil_gaul aa LEFT JOIN dispatch_teknisi bb ON aa.TROUBLE_NO = bb.Ndem LEFT JOIN regu cc ON bb.id_regu = cc.id_regu WHERE cc.crewid = d.crewid AND aa.THNBLN LIKE "'.$periode.'%") as GAUL,
            d.crewid as sektor_asr
          FROM
            mdf b
          LEFT JOIN nonatero_detil a ON b.mdf = a.STO
          LEFT JOIN dispatch_teknisi c ON a.TROUBLE_NO = c.Ndem
          LEFT JOIN regu d ON c.id_regu = d.id_regu
          WHERE
            a.THNBLN LIKE "'.$periode.'%"
            '.$q_mitra.'
          GROUP BY
            d.crewid
        ');
        return $query;
      }
    public static function kpi_assurance_periode(){
      $query = DB::SELECT('SELECT THNBLN FROM nonatero_detil GROUP BY THNBLN');
      return $query;
    }

    // public static function kpi_assurance_mitra($periode){
    //   $query = DB::SELECT('SELECT c.mitra FROM nonatero_detil a LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.NDEM LEFT JOIN regu c ON b.id_regu = c.id_regu WHERE a.THNBLN="'.$periode.'" GROUP BY c.mitra');
    //   return $query;
    // }

    public static function get_last_update_kpi_assurance(){
      $query = DB::SELECT('select UPDATE_TIME from nonatero_detil ORDER BY UPDATE_TIME DESC limit 1 ');
      return $query;
    }

    public static function kpi_assuranceListGaul($sektor,$periode){
      if ($sektor=="ALL"){
        $q_sektor = '';
      } else {
        $q_sektor = 'AND b.sektor_asr = "'.$sektor.'"';
      }
      $query = DB::SELECT('
        SELECT
          a.*,
          d.uraian,
          f.action,
          g.penyebab as sebab,
          d.mitra,
          c.id
        FROM
          nonatero_detil_gaul a
        LEFT JOIN mdf b ON a.CMDF = b.mdf
        LEFT JOIN dispatch_teknisi c ON a.TROUBLE_NO = c.Ndem
        LEFT JOIN regu d ON c.id_regu = d.id_regu
        LEFT JOIN psb_laporan e ON c.id = e.id_tbl_mj
        LEFT JOIN psb_laporan_action f ON e.action = f.laporan_action_id
        LEFT JOIN psb_laporan_penyebab g ON g.idPenyebab = e.penyebabId
        WHERE
          a.THNBLN = "'.$periode.'"
          '.$q_sektor.'
      ');
      return $query;
    }

    public static function kpi_assuranceList($tipe,$sektor,$periode){
      if ($sektor=="ALL"){
        $q_sektor = '';
      } else {
        $q_sektor = 'AND b.sektor_asr = "'.$sektor.'"';
      }

      switch ($tipe) {
        case 'NC48':
            $q_tipe = 'AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  a.TGL_TECH_CLOSE)>=48';
          break;
        case 'NC1DS':
            $q_tipe = 'AND a.IS_12HS = 0';
          break;
        case 'REAL48':
            $q_tipe = 'AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  a.TGL_TECH_CLOSE)<48';
          break;
        case 'REAL1DS':
            $q_tipe = 'AND a.IS_12HS = 1';
          break;
        default:
            $q_tipe = '';
          break;
      }

      $query = DB::SELECT('
        SELECT
        a.*,
        d.uraian,
        f.action,
        g.penyebab as sebab,
        d.mitra,
        c.id
        FROM
          nonatero_detil a
        LEFT JOIN mdf b ON a.STO = b.mdf
        LEFT JOIN dispatch_teknisi c ON a.TROUBLE_NO = c.Ndem
        LEFT JOIN regu d ON c.id_regu = d.id_regu
        LEFT JOIN psb_laporan e ON c.id = e.id_tbl_mj
        LEFT JOIN psb_laporan_action f ON e.action = f.laporan_action_id
        LEFT JOIN psb_laporan_penyebab g ON g.idPenyebab = e.penyebabId
        WHERE
          a.THNBLN = "'.$periode.'"
          '.$q_sektor.'
          '.$q_tipe.'
      ');
      return $query;
    }

    public static function kpi_assurance($periode,$mitrax){

      if ($mitrax == "ALL"){
        $q_mitra = '';
      } else {
        $q_mitra = ' AND d.mitra = "'.$mitrax.'"';
      }

      $query = DB::SELECT('
        SELECT
          count(*) as JUMLAH,
          SUM(a.IS_12HS) as R1DS,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  a.TGL_TECH_CLOSE)<48 THEN 1 ELSE 0 END) as R48,
          (SELECT COUNT(*) FROM nonatero_detil_gaul aa WHERE aa.CMDF = b.mdf AND aa.THNBLN = "'.$periode.'") as GAUL,
          b.sektor_asr
        FROM
          mdf b
        LEFT JOIN nonatero_detil a ON b.mdf = a.STO
        LEFT JOIN dispatch_teknisi c ON a.TROUBLE_NO = c.Ndem
        LEFT JOIN regu d ON c.id_regu = d.id_regu
        WHERE
          a.THNBLN = "'.$periode.'"
          '.$q_mitra.'
        GROUP BY
          b.sektor_asr
      ');
      return $query;
    }



  public static function provisioning_wallboard_byjam(){
      date_default_timezone_set('Asia/Makassar');
    $query = DB::connection('mysql4')->SELECT('
      SELECT
        count(*) as jumlah,
        a.WITEL,
        TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" ) as umur,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<2 THEN 1 ELSE 0 END) as x0dan2jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<4 THEN 1 ELSE 0 END) as x2dan4jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=4 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<6 THEN 1 ELSE 0 END) as x4dan6jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=6 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<8 THEN 1 ELSE 0 END) as x6dan8jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=8 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<10 THEN 1 ELSE 0 END) as x8dan10jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=10 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<12 THEN 1 ELSE 0 END) as x10dan12jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=12 THEN 1 ELSE 0 END) as x12jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<8 THEN 1 ELSE 0 END) as x8jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=8 THEN 1 ELSE 0 END) as xlebih8jam
      FROM 4_0_master_order_1 a WHERE a.WITEL IN ("KALSEL","KALBAR","KALTENG") AND a.ORDER_STATUS = 16 AND a.JENISPSB LIKE ("%AO%") GROUP BY a.WITEL');
    return $query;
  }
  public static function provisioning_wallboard_byhari(){
      date_default_timezone_set('Asia/Makassar');
    $query = DB::connection('mysql4')->SELECT('
      SELECT
        count(*) as jumlah,
        a.WITEL,
        TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" ) as umur,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as hari1,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<48 THEN 1 ELSE 0 END) as hari2,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<72 THEN 1 ELSE 0 END) as hari3,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=72 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<96 THEN 1 ELSE 0 END) as hari4,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=96 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<168 THEN 1 ELSE 0 END) as hari7,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=168 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<360 THEN 1 ELSE 0 END) as hari15,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=360 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<720 THEN 1 ELSE 0 END) as hari30,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=720 THEN 1 ELSE 0 END) as lebih30hari,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as kurang1hari,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 THEN 1 ELSE 0 END) as lebih1hari
      FROM 4_0_master_order_1 a WHERE a.WITEL IN ("KALSEL","KALBAR","KALTENG") AND a.ORDER_STATUS = 16 AND a.JENISPSB LIKE ("%AO%") GROUP BY a.WITEL');
    return $query;
  }
  public static function provisioning2_wallboard_byhari(){
      date_default_timezone_set('Asia/Makassar');
    $query = DB::connection('mysql4')->SELECT('
      SELECT
        count(*) as jumlah,
        a.WITEL,
        TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" ) as umur,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as hari1,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<48 THEN 1 ELSE 0 END) as hari2,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<72 THEN 1 ELSE 0 END) as hari3,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=72 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<96 THEN 1 ELSE 0 END) as hari4,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=96 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<168 THEN 1 ELSE 0 END) as hari7,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=168 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<360 THEN 1 ELSE 0 END) as hari15,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=360 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<720 THEN 1 ELSE 0 END) as hari30,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=720 THEN 1 ELSE 0 END) as lebih30hari,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as kurang1hari,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 THEN 1 ELSE 0 END) as lebih1hari
      FROM 4_0_master_order_1 a WHERE a.WITEL IN ("KALSEL","KALBAR","KALTENG") AND a.ORDER_STATUS = 16 AND (a.PACKAGE_NAME LIKE "%SWSTBHYBR2%" OR a.PACKAGE_NAME LIKE "%2nd STB%" OR a.PACKAGE_NAME LIKE "%Second%") GROUP BY a.WITEL');
    return $query;
  }

  public static function provisioning_wallboard_list($id,$witel){
  date_default_timezone_set('Asia/Makassar');
    switch ($id) {
      case '0_2jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<2';
      break;
      case '2_4jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<4';
      break;
      case '4_6jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=4 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<6';
      break;
      case '6_8jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=6 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<8';
      break;
      case '8_10jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=8 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<10';
      break;
      case '10_12jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=10 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<12';
      break;
      case '12jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")>=12';
      break;
      case '8jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<8';
      break;
      case 'lebih8jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")>=8';
      break;
      default:
          $whereID = '';
        break;
    }

    if ($witel=="ALL"){
      $whereWITEL = 'AND a.WITEL IN ("KALSEL","KALBAR","KALTENG")';
    } else {
      $whereWITEL = 'AND a.WITEL = "'.$witel.'"';
    }

    $query = DB::connection('mysql4')->SELECT('
      SELECT
        *
      FROM
        4_0_master_order_1 a
      WHERE
        a.JENISPSB LIKE ("%AO%") AND
        a.ORDER_STATUS = 16
        '.$whereID.'
        '.$whereWITEL.'
    ');
    return $query;
  }


  public static function provisioning_wallboard_list_by_hari($id,$witel){
  date_default_timezone_set('Asia/Makassar');
    switch ($id) {
      case 'hari1':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24';
      break;
      case 'hari2':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<48';
      break;
      case 'hari3':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<72';
      break;
      case 'hari4':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=72 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<96';
      break;
      case 'hari7':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=96 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<168';
      break;
      case 'hari15':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=168 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<360';
      break;
      case 'hari30':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=360 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<720';
      break;
      case 'lebih30hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=720';
      break;
      case 'kurang1hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24';
      break;
      case 'lebih1hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24';
      break;
      default:
          $whereID = '';
        break;
    }

    if ($witel=="ALL"){
      $whereWITEL = 'AND a.WITEL IN ("KALSEL","KALBAR","KALTENG")';
    } else {
      $whereWITEL = 'AND a.WITEL = "'.$witel.'"';
    }

    $query = DB::connection('mysql4')->SELECT('
      SELECT
        *
      FROM
        4_0_master_order_1 a
      WHERE
        a.JENISPSB LIKE ("%AO%") AND
        a.ORDER_STATUS = 16
        '.$whereID.'
        '.$whereWITEL.'
    ');
    return $query;
  }

  public static function provisioning2_wallboard_list_by_hari($id,$witel){
  date_default_timezone_set('Asia/Makassar');
    switch ($id) {
      case 'hari1':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24';
      break;
      case 'hari2':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<48';
      break;
      case 'hari3':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<72';
      break;
      case 'hari4':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=72 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<96';
      break;
      case 'hari7':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=96 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<168';
      break;
      case 'hari15':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=168 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<360';
      break;
      case 'hari30':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=360 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<720';
      break;
      case 'lebih30hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=720';
      break;
      case 'kurang1hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24';
      break;
      case 'lebih1hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24';
      break;
      default:
          $whereID = '';
        break;
    }

    if ($witel=="ALL"){
      $whereWITEL = 'AND a.WITEL IN ("KALSEL","KALBAR","KALTENG")';
    } else {
      $whereWITEL = 'AND a.WITEL = "'.$witel.'"';
    }

    $query = DB::connection('mysql4')->SELECT('
      SELECT
        *
      FROM
        4_0_master_order_1 a
      WHERE
        (a.PACKAGE_NAME LIKE "%SWSTBHYBR2%" OR a.PACKAGE_NAME LIKE "%2nd STB%" OR a.PACKAGE_NAME LIKE "%Second%") AND
        a.ORDER_STATUS = 16
        '.$whereID.'
        '.$whereWITEL.'
    ');
    return $query;
  }

  public static function listbyumur($sektor,$durasi,$channel){
    date_default_timezone_set('Asia/Makassar');

    if ($channel == "ALL"){
      $channel_q = '';
    } else {
      $channel_q = 'AND a.channel = "'.$channel.'"';
    }

    switch ($durasi){
      case "lebih24" :
        $durasi_q = 'AND a.hari >= 1';
      break;
      case "a0dan2" :
        $durasi_q = 'AND (a.hari BETWEEN 0 AND 0.083)';
      break;
      case "a2dan4" :
        $durasi_q = 'AND (a.hari BETWEEN 0.083 AND 0.16)';
      break;
      case "a4dan6" :
        $durasi_q = 'AND (a.hari BETWEEN 0.16 AND 0.25)';
      break;
      case "a6dan8" :
        $durasi_q = 'AND (a.hari BETWEEN 0.25 AND 0.33)';
      break;
      case "a8dan10" :
        $durasi_q = 'AND (a.hari BETWEEN 0.33 AND 0.416)';
      break;
      case "a10dan12" :
        $durasi_q = 'AND (a.hari BETWEEN 0.416 AND 0.5)';
      break;
      case "a12dan24" :
        $durasi_q = 'AND (a.hari BETWEEN 0.5 AND 1)';
      break;
      case "kurang24" :
        $durasi_q = 'AND (a.hari < 1)';
      break;
      case "kurang12" :
        $durasi_q = 'AND (a.hari*24))<12';
      break;
      case "ALL" :
        $durasi_q = '';
      break;
      default :
        $durasi_q = '';
      break;
    }

    if ($sektor=="ALL"){
      $sektor_q = '';
    } else if ($sektor=="UNDEFINED"){
      $sektor_q = 'AND b.sektor_asr is NULL ';
    } else {
      $sektor_q = 'AND b.sektor_asr = "'.$sektor.'"';
    }

    $query = DB::SELECT('
      SELECT
      a.*,
      a.trouble_no as no_tiket,
      a.nd_telp as no_telp,
      a.nd_int as no_internet,
      a.nd_int as no_speedy,
      a.trouble_opentime as tgl_open,
      a.sto as sto_trim,
      c.id,
      c.updated_at,
      e.uraian,
      f.sektor,
      g.laporan_status,
      h.action,
      d.catatan,
      i.penyebab
      FROM
        rock_excel a
      LEFT JOIN mdf b ON a.sto = b.mdf
      LEFT JOIN dispatch_teknisi c ON a.trouble_no = c.Ndem
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN regu e ON c.id_regu = e.id_regu
      LEFT JOIN group_telegram f ON e.mainsector = f.chat_id
      LEFT JOIN psb_laporan_status g ON d.status_laporan = g.laporan_status_id
      LEFT JOIN psb_laporan_action h ON d.action = h.laporan_action_id
      LEFT JOIN psb_laporan_penyebab i ON d.penyebabId = i.idPenyebab
      WHERE
      1
      '.$sektor_q.'
      '.$durasi_q.'
      '.$channel_q.'
    ');
    return $query;
  }

  public static function rock_active_by_umur(){
    date_default_timezone_set('Asia/Makassar');
    $query = DB::SELECT('
      SELECT
      b.sektor_asr,
      count(*) as jumlah,
      SUM(CASE WHEN (a.hari) >=0.5 THEN 1 ELSE 0 END) as lebih12,
      SUM(CASE WHEN (a.hari) <0.5 THEN 1 ELSE 0 END) as kurang12,
      SUM(CASE WHEN (a.hari) BETWEEN 0 AND 0.083333 THEN 1 ELSE 0 END) as a0dan2,
      SUM(CASE WHEN (a.hari) BETWEEN 0.083333 AND 0.16667 THEN 1 ELSE 0 END) as a2dan4,
      SUM(CASE WHEN (a.hari) BETWEEN 0.16667 AND 0.25 THEN 1 ELSE 0 END) as a4dan6,
      SUM(CASE WHEN (a.hari)BETWEEN 0.25 AND 0.3334 THEN 1 ELSE 0 END) as a6dan8,
      SUM(CASE WHEN (a.hari) BETWEEN 0.3334 AND 0.416 THEN 1 ELSE 0 END) as a8dan10,
      SUM(CASE WHEN (a.hari) BETWEEN 0.416 AND 0.5 THEN 1 ELSE 0 END) as a10dan12,
      SUM(CASE WHEN (a.hari) BETWEEN 0.5 AND 1 THEN 1 ELSE 0 END) as a12dan24,
      SUM(CASE WHEN (a.hari) >= 1 THEN 1 ELSE 0 END) as lebih24,
      SUM(CASE WHEN (a.hari) < 1 THEN 1 ELSE 0 END) as kurang24
  FROM
        rock_excel a
      LEFT JOIN mdf b ON a.sto = b.mdf
      WHERE a.is_gamas = 0
      GROUP BY b.sektor_asr
      ORDER BY jumlah DESC
    ');
    return $query;
  }
  public static function rock_active_by_umur_myi(){
    date_default_timezone_set('Asia/Makassar');
    $query = DB::SELECT('
      SELECT
      b.sektor_asr,
      count(*) as jumlah,
      SUM(CASE WHEN a.hari>=0.5 THEN 1 ELSE 0 END) as lebih12,
      SUM(CASE WHEN a.hari<0.5 THEN 1 ELSE 0 END) as kurang12,
      SUM(CASE WHEN a.hari BETWEEN 0 AND 0.083333 THEN 1 ELSE 0 END) as a0dan2,
      SUM(CASE WHEN a.hari BETWEEN 0.083333 AND 0.16667 THEN 1 ELSE 0 END) as a2dan4,
      SUM(CASE WHEN a.hari BETWEEN 0.16667 AND 0.25 THEN 1 ELSE 0 END) as a4dan6,
      SUM(CASE WHEN a.hari BETWEEN 0.25 AND 0.3334 THEN 1 ELSE 0 END) as a6dan8,
      SUM(CASE WHEN a.hari BETWEEN 0.3334 AND 0.416 THEN 1 ELSE 0 END) as a8dan10,
      SUM(CASE WHEN a.hari BETWEEN 0.416 AND 0.5 THEN 1 ELSE 0 END) as a10dan12,
      SUM(CASE WHEN a.hari BETWEEN 0.5 AND 1 THEN 1 ELSE 0 END) as a12dan24,
      SUM(CASE WHEN a.hari >= 1 THEN 1 ELSE 0 END) as lebih24,
      SUM(CASE WHEN a.hari < 1 THEN 1 ELSE 0 END) as kurang24
      FROM
        rock_excel a
      LEFT JOIN mdf b ON a.sto = b.mdf
      Where
        a.channel = "MY INDIHOME" AND
        a.is_gamas = 0
      GROUP BY b.sektor_asr
      ORDER BY jumlah DESC
    ');
    return $query;
  }

  public static function umurTiket(){
    date_default_timezone_set('Asia/Makassar');

    $query = DB::SELECT('
      SELECT
        a.*,
        TIMESTAMPDIFF( DAY , aaa.Reported_Date,  "'.date('Y-m-d H:i:s').'" ) as lamaHari,
        TIMESTAMPDIFF( HOUR , aaa.Reported_Date,  "'.date('Y-m-d H:i:s').'" ) as lamaJam,
        d.laporan_status,
        e.uraian,
        f.title,
        a.trouble_no as TROUBLE_NO,
        aaa.Summary as HEADLINE,
        aaa.Reported_Date as TROUBLE_OPENTIME,
        c.modified_at as tgl_update,
        c.created_at as tgl_created,
        aaa.Reported_Date as tglOpen
      FROM
        rock_excel a
      LEFT JOIN roc aa ON a.trouble_no = aa.no_tiket
      LEFT JOIN data_nossa_1_log aaa ON a.trouble_no = aaa.Incident
      LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN regu e ON b.id_regu = e.id_regu
      LEFT JOIN group_telegram f ON e.mainsector = f.chat_id
      ORDER BY aaa.Reported_Date
    ');
    return $query;
  }

  public static function listTimMitra($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND a.mitra is NULL ';
    } else {
      $getWhere = ' AND a.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        a.*
      FROM
        regu a
        LEFT JOIN dispatch_teknisi b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      WHERE
        c.status_laporan = 1 AND
        b.tgl LIKE "%'.$tgl.'%"
          '.$getWhere.'
        GROUP BY a.uraian
    ');
    return $query;
  }

  public static function nonatero_active_by_pic(){
    $query = DB::SELECT('
      SELECT
        d.PIC,
        SUM(CASE WHEN b.troubleno_parent is not NULL AND d.PIC = "TA" THEN 1 ELSE 0 END) as isGamas_ta,
        SUM(CASE WHEN c.id is null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_ndt_before,
        SUM(CASE WHEN c.id is not null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_dt_before,
        SUM(CASE WHEN b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_before,
        SUM(CASE WHEN b.troubleno_parent is not null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isGamas_before,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as jumlah_before,
        SUM(CASE WHEN c.id is null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_ndt_after,
        SUM(CASE WHEN c.id is not null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_dt_after,
        SUM(CASE WHEN b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_after,
        SUM(CASE WHEN b.troubleno_parent is not null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isGamas_after,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as jumlah_after,
        count(*) as jumlah
      FROM  `nonatero_excel` a
      LEFT JOIN rock_gamas_opn b ON a.trouble_no = b.trouble_no
      LEFT JOIN dispatch_teknisi c ON a.trouble_no = c.Ndem
      LEFT JOIN loker_pic d ON a.LOKER_DISPATCH = d.LOKER
      WHERE
        a.LOKER_DISPATCH <> ""
      GROUP BY d.PIC
    ');
    return $query;
  }

  public static function nonatero_active(){
    $query = DB::SELECT('
    SELECT
      a.LOKER_DISPATCH,
      d.PIC,
      SUM(CASE WHEN b.troubleno_parent is not NULL AND d.PIC = "TA" THEN 1 ELSE 0 END) as isGamas_ta,
      SUM(CASE WHEN c.id is null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_ndt_before,
      SUM(CASE WHEN c.id is not null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_dt_before,
      SUM(CASE WHEN b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_before,
      SUM(CASE WHEN b.troubleno_parent is not null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isGamas_before,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as jumlah_before,
      SUM(CASE WHEN c.id is null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_ndt_after,
      SUM(CASE WHEN c.id is not null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_dt_after,
      SUM(CASE WHEN b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_after,
      SUM(CASE WHEN b.troubleno_parent is not null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isGamas_after,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as jumlah_after,
      count(*) as jumlah
    FROM  `nonatero_excel` a
    LEFT JOIN rock_gamas_opn b ON a.trouble_no = b.trouble_no
    LEFT JOIN dispatch_teknisi c ON a.trouble_no = c.Ndem
    LEFT JOIN loker_pic d ON a.LOKER_DISPATCH = d.LOKER
    Where
      a.LOKER_DISPATCH <> ""
    GROUP BY a.LOKER_DISPATCH
    ORDER BY d.PIC,jumlah DESC
    ');
    return $query;
  }

  public static function ms2nmigrasi($date){
    $query = DB::SELECT('
      SELECT
        a.datel,
        SUM(CASE WHEN dt.id IS NOT NULL THEN 1 ELSE 0 END) as jumlah_asg,
        SUM(CASE WHEN dt.id IS NULL THEN 1 ELSE 0 END) as jumlah_notasg,
        count(*) as jumlah
      FROM
        ms2n_migrasi a
      LEFT JOIN Data_Pelanggan_Starclick b ON a.orderId = b.orderId
      LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem
      WHERE
        a.last_update LIKE "'.$date.'%"
      GROUP BY a.datel
    ');
    return $query;
  }

  public static function ms2n($date){
    $query = DB::SELECT('
      SELECT
        a.Kandatel,
        SUM(CASE WHEN a.JENIS = "3P" THEN 1 ELSE 0 END) as Jumlah_3P,
        SUM(CASE WHEN a.JENIS = "3P" AND dt.id is null THEN 1 ELSE 0 END) as Jumlah_3P_NOTASG,
        SUM(CASE WHEN a.JENIS = "3P" AND dt.id is not null THEN 1 ELSE 0 END) as Jumlah_3P_ASG,
        SUM(CASE WHEN a.JENIS = "2P" THEN 1 ELSE 0 END) as Jumlah_2P,
        SUM(CASE WHEN a.JENIS = "2P" AND dt.id is null THEN 1 ELSE 0 END) as Jumlah_2P_NOTASG,
        SUM(CASE WHEN a.JENIS = "2P" AND dt.id is not null THEN 1 ELSE 0 END) as Jumlah_2P_ASG,
        count(*) as Jumlah
      FROM
        ms2n a
        LEFT JOIN Data_Pelanggan_Starclick dps ON MID(a.kode_sc,3,10) = dps.orderId
        LEFT JOIN dispatch_teknisi dt ON dt.Ndem = MID(a.kode_sc,3,10)
      WHERE
        a.Status = "PS" AND
        a.Tgl_PS like "'.$date.'%"
      GROUP BY a.Kandatel
    ');
    return $query;
  }

  public static function listms2n($tgl,$so,$jenis,$status){

    if ($so == "ALL"){
      $where_so = '';
    } else {
      $where_so = ' AND a.Kandatel = "'.$so.'"';
    }

    if ($jenis == "ALL"){
      $where_jenis = '';
    } else {
      $where_jenis = ' AND a.JENIS = "'.$jenis.'"';
    }

    if ($status == "ALL"){
      $where_status = '';
    } else if ($status == "ASG"){
      $where_status = ' AND c.id is NOT NULL';
    } else {
      $where_status = ' AND c.id is NULL';
    }

    $query = DB::SELECT('
      SELECT
        a.*,
        b.*,
        pl.*,
        c.id as id_dt,

        DATE_FORMAT(a.Tgl_PS,"%d %M %Y") as Tgl_PS,
        SUM( i.num ) AS dc_preconn,
        SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140") then plm.qty else 0 end) as tiang_7,
        SUM(case when plm.id_item IN ("PU-S9.0-140") then plm.qty else 0 end) as tiang_9,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        0 as conduit,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
      FROM
        ms2n a
        LEFT JOIN Data_Pelanggan_Starclick b ON MID(a.kode_sc,3,10) = b.orderId
        LEFT JOIN dispatch_teknisi c ON c.Ndem = MID(a.kode_sc,3,10)
        LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON c.jenis_layanan = dtjl.jenis_layanan
        LEFT JOIN psb_laporan pl ON c.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
        LEFT JOIN item i ON plm.id_item = i.id_item
      WHERE
        a.Status = "PS" AND
        a.Tgl_PS LIKE "%'.$tgl.'%"
        '.$where_status.'
        '.$where_jenis.'
        '.$where_so.'
      GROUP BY a.Ndem
    ');
    return $query;
  }

  public static function rekonMitrProv($tgl){
      $query = DB::SELECT('
      SELECT
        a.mitra,
        SUM(CASE WHEN d.sc<>"" THEN 1 ELSE 0 END) as jumlah_prov
      FROM
      regu a
      LEFT JOIN dispatch_teknisi b ON a.id_regu = b.id_regu
      LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON b.jenis_layanan = dtjl.jenis_layanan
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_myir_wo d ON b.Ndem = d.sc
      LEFT JOIN dossier_master dm ON b.Ndem = dm.ND
      LEFT JOIN roc r ON b.Ndem = r.no_tiket
      WHERE
        c.status_laporan in ("1","37","38") AND
        b.tgl LIKE "'.$tgl.'%"
      GROUP BY a.mitra
    ');
    return $query;
  }

  public static function rekonMitraList($tgl){
	  $query = DB::SELECT('
      SELECT
      	*
      FROM
        regu a
        LEFT JOIN dispatch_teknisi b ON a.id_regu = b.id_regu
        LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON b.jenis_layanan = dtjl.jenis_layanan
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN Data_Pelanggan_Starclick d ON b.Ndem = d.orderId
        LEFT JOIN dossier_master dm ON b.Ndem = dm.ND
        LEFT JOIN roc r ON b.Ndem = r.no_tiket
      WHERE
        c.status_laporan in ("1","37","38") AND
        b.tgl LIKE "'.$tgl.'%" AND
        d.orderId <> ""

    ');
    return $query;
  }

  public static function qc_borneo($tgl){
	  $query = DB::SELECT('
	  	SELECT
	  	c.mitra,
	  	SUM(CASE WHEN b.foto_berita = "OK" THEN 1 ELSE 0 END) as foto_berita_ok,
	  	SUM(CASE WHEN b.foto_rumah = "OK" THEN 1 ELSE 0 END) as foto_rumah_ok,
	  	SUM(CASE WHEN b.foto_teknisi = "OK" THEN 1 ELSE 0 END) as foto_teknisi_ok,
	  	SUM(CASE WHEN b.foto_odp = "OK" THEN 1 ELSE 0 END) as foto_odp_ok,
	  	SUM(CASE WHEN b.foto_redaman = "OK" THEN 1 ELSE 0 END) as foto_redaman_ok,
	  	SUM(CASE WHEN b.foto_ont = "OK" THEN 1 ELSE 0 END) as foto_ont_ok,
	  	SUM(CASE WHEN b.tagging_lokasi = "OK" THEN 1 ELSE 0 END) as tagging_pelanggan_ok,
      count(*) as jumlah
	  	FROM
	  		dispatch_teknisi a
	  	LEFT JOIN qc_borneo b ON a.Ndem = b.sc
	  	LEFT JOIN regu c ON a.id_regu = c.id_regu
	  	WHERE
	  		a.tgl LIKE "'.$tgl.'%" AND
        b.unit LIKE "%ASO%" AND
	  		b.sc <> ""
	  	GROUP BY c.mitra
	  ');
	  return $query;
  }


  public static function rekonMitra($tgl){
    $query = DB::SELECT('
      SELECT
        a.mitra,
        -- count(*) as jumlah,
        SUM(IF(dm.ND<>"",1,0)+IF(r.Incident<>"",1,0)) as jumlah,
        SUM(CASE WHEN b.Ndem NOT LIKE "IN%" THEN 1 ELSE 0 END) as jumlah_prov,
        SUM(CASE WHEN (d.sc<>"") THEN 1 ELSE 0 END) as jumlah_prov_anomali,
        SUM(CASE WHEN dm.ND<>"" THEN 1 ELSE 0 END) as jumlah_migrasi,
        SUM(CASE WHEN r.Incident<>"" THEN 1 ELSE 0 END) as jumlah_asr
      FROM
      regu a
      LEFT JOIN dispatch_teknisi b ON a.id_regu = b.id_regu
      LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON b.jenis_layanan = dtjl.jenis_layanan
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_myir_wo d ON b.Ndem = d.sc
      LEFT JOIN dossier_master dm ON b.Ndem = dm.ND
      LEFT JOIN data_nossa_1_log r ON b.Ndem = r.Incident
      WHERE
        c.status_laporan in ("1") AND
        b.tgl LIKE "'.$tgl.'%"
      GROUP BY a.mitra
    ');
    return $query;
  }

  public static function rekonMitraDc($tgl){
      $sql = 'SELECT
                a.mitra,
                SUM(CASE WHEN IF(d.id_item = "Preconnectorized-1C-100-NonAcc",e.num * d.qty,0) + IF(d.id_item = "Preconnectorized-1C-75-NonAcc",e.num * d.qty,0) + IF(d.id_item = "Preconnectorized-1C-50-NonAcc",e.num * d.qty,0) + IF(d.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),e.num * d.qty,0) + IF(d.id_item = "AC-OF-SM-1B",d.qty,0) > 75 THEN 1 ELSE 0 END) as dc_preconn
              FROM
                regu a
              LEFT JOIN dispatch_teknisi b ON b.id_regu = a.id_regu
              LEFT JOIN psb_laporan c ON c.id_tbl_mj = b.id
              LEFT JOIN psb_laporan_material d ON d.psb_laporan_id = c.id
              LEFT JOIN rfc_sal e ON d.id_item_bantu = e.id_item_bantu
              Where
                c.status_laporan IN ("1","37","38") AND
                b.tgl LIKE "%'.$tgl.'%"
              GROUP BY a.mitra
             ';

      $data = DB::select($sql);

      return $data;
  }

  public static function rekonMitraDcList($mitra, $tgl){
      $where_mitra = '';
      if($mitra<>"ALL"){
          $where_mitra = 'AND a.mitra="'.$mitra.'"';
      }

      $sql = 'SELECT
                *,
                f.laporan_status as statusTeknisi,
                SUM(IF(d.id_item = "Preconnectorized-1C-100-NonAcc",e.num * d.qty,0) + IF(d.id_item = "Preconnectorized-1C-75-NonAcc",e.num * d.qty,0) + IF(d.id_item = "Preconnectorized-1C-50-NonAcc",e.num * d.qty,0) + IF(d.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),e.num * d.qty,0) + IF(d.id_item = "AC-OF-SM-1B",d.qty,0)) as dc_preconn
              FROM
                regu a
              LEFT JOIN dispatch_teknisi b ON b.id_regu = a.id_regu
              LEFT JOIN psb_laporan c ON c.id_tbl_mj = b.id
              LEFT JOIN psb_laporan_material d ON d.psb_laporan_id = c.id
              LEFT JOIN rfc_sal e ON d.id_item_bantu = e.id_item_bantu
              LEFT JOIN psb_laporan_status f ON f.laporan_status_id = c.status_laporan
              Where
                c.status_laporan IN ("1","37","38") AND
                b.tgl LIKE "%'.$tgl.'%"
                '.$where_mitra.'
              GROUP BY b.Ndem HAVING dc_preconn > 75
             ';

      $data = DB::select($sql);
      return $data;
  }

  public static function migrasiListMitra($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        dt.updated_at as tanggal_dispatch,
        dm.STO as sto,
        dm.ND as orderId,
        dm.NCLI as orderNcli,
        dm.ND as ndemPots,
        dm.ND_REFERENCE as ndemSpeedy,
        dm.NAMA as orderName,
        "MIGRASI AS IS" as jenisPsb,
        "~" as orderStatus,
        DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        0 as orderDatePs,
         SUM(IF(plm.id_item = "Preconnectorized-1C-100-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-75-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-50-NonAcc",i.num * plm.qty,0) + IF(plm.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),i.num * plm.qty,0) + IF(plm.id_item = "AC-OF-SM-1B",plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-80-NonAcc",i.num * plm.qty,0)) as dc_preconn,
        -- SUM( i.num * plm.qty) AS dc_preconn,
        SUM(case when plm.id_item = "AC-OF-SM-1B" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6", "UTP-C6", "UTP-C5") then plm.qty else 0 end) as utp,
        -- SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "Preconnectorized-1C-100-NonAcc" then plm.qty else 0 end) as precon100,
        SUM(case when plm.id_item = "Preconnectorized-1C-75-NonAcc" then plm.qty else 0 end) as precon75,
        SUM(case when plm.id_item = "Preconnectorized-1C-50-NonAcc" then plm.qty else 0 end) as precon50,
        SUM(case when plm.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc") then plm.qty else 0 end) as preconnonacc,
        SUM(CASE when plm.id_item in ("BREKET","BREKET-A") THEN plm.qty else 0 end) as breket,
        SUM(CASE when plm.id_item in ("S-Clamp-Spriner","S-Clamp-Sprinerr") THEN plm.qty else 0 end) as clamps,
        SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pulstrap,
        SUM(CASE when plm.id_item = "RJ45-5" THEN plm.qty else 0 end) as rj45,
        SUM(CASE when plm.id_item in ("RS-IN-SC-1","RS-IN-SC-11") THEN plm.qty else 0 end) as roset,
        SUM(CASE when plm.id_item = "SOC-ILS" THEN plm.qty else 0 end) as SOCIlsintentech,
        SUM(CASE when plm.id_item = "SOC-SUM" THEN plm.qty else 0 end) as SOCSumitomo,
        SUM(CASE when plm.id_item = "AD-SC" THEN plm.qty else 0 end) as adsc,
        0 as conduit,
        dm.LQUARTIER as orderAddr,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       -- LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN rfc_sal i on plm.id_item_bantu=i.id_item_bantu
       LEFT JOIN dossier_master dm ON dt.Ndem = dm.ND
       LEFT JOIN mdf m ON dm.STO = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       dm.ND <> "" AND
       dt.tgl like "%'.$tgl.'%" AND
       pl.status_laporan in ("1","37","38")
       '.$getWhere.'
       GROUP BY dt.id
    ');
    return $query;
  }

  public static function migrasiListMitraMs2n($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        dt.updated_at as tanggal_dispatch,
        dm.STO as sto,
        dm.ND as orderId,
        dm.NCLI as orderNcli,
        dm.ND as ndemPots,
        dm.ND_REFERENCE as ndemSpeedy,
        dm.NAMA as orderName,
        "MIGRASI AS IS" as jenisPsb,
        "~" as orderStatus,
        DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        0 as orderDatePs,
        SUM( i.num ) AS dc_preconn,
        SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        0 as conduit,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
        dm.LQUARTIER as orderAddr,
        dtjl.mode
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN dossier_master dm ON dt.Ndem = dm.ND
       LEFT JOIN mdf m ON dm.STO = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       dm.ND <> "" AND
       dt.Ndem IN (
         5262021738,
5114705289,
5113353464,
5262022104,
5113356183,
5114774783,
5114415997,
5114220211,
5113269530,
5113271530,
5113352009,
5113362779,
5113361848,
5113263733,
5262023738,
5113265059,
5113267576,
5114721399,
5114707167,
5113300179,
5113304241,
5114314553,
5114412959,
5114707934,
5114774200,
5114412256,
5114777760,
5114786622,
5113364289,
5114720179,
5114780928,
51874366,
5262021275,
5113250118,
5113353032,
5262021322,
5262025217,
5114722468,
51870600,
51823041,
5113352387,
5113269632,
5276064117,
5114281873,
5114786237,
5276064118,
5276064052,
5113357952,
5114365841,
5113354180,
5113257651,
5113250273,
5114364316,
5113256540,
5113353537,
5113357071,
5113301701,
5113303702,
5114283624,
5114422365,
5114225000,
5114721457,
51742271,
5113352730,
5113352722,
5113263375,
5114314679,
5114413112,
5113256537,
5113256553,
5262022816,
5262022620,
51732170,
5262025257,
5114221617,
5114420333,
5113267984,
5113267855,
5113364565,
5113256219,
5114788091,
5113352783,
5113352720,
5114773635,
51821658,
5114780623,
5113262898,
5113268054,
5113300277,
5113253508,
5113266748,
5113266478,
5113251010,
5114411742,
5262024999,
5113304241,
5113354224,
5113254661,
5114316836,
5114721012,
5114220908,
5113307152,
5113261301,
5113271928,
5114369688,
5114780092,
5113350973,
5114777133,
51743132,
51741437,
5114411771,
5113361129,
5114221037,
5113258809,
5114413185,
51744228,
5114778180,
51261546,
51744397,
5114788108,
5114783288,
51261805,
51743034,
5113301561,
5113274896,
51741826,
51742710,
51741371,
5122062028,
5114784420,
5114363152,
5113360410,
5114282368,
5113252462,
5114365324,
5113300065,
5122062050,
5113250948,
5114784720,
5113306332,
5114414655,
5114415257,
51261106,
5113305508,
5114363118,
5113355273,
5113276516,
5114774457,
5114787857,
5114705097,
5113351379,
5113364839,
51261533,
5113364859,
5113258863,
51261480,
5113364809,
5114369670,
5262025474,
5114787822,
5114413122,
5113364669,
5114782327,
5113307384,
5114774392,
51861102,
5113354823,
5113302338,
5113367024,
5113353323,
5114411710,
5114705167,
5113360353,
5113305186,
5113367080,
5114777335,
51741987,
5114791387,
5114781127,
5114316630,
51731339,
5113364889,
51261582,
5114229328,
5113305183,
5113360273,
5113274503,
5113274502,
5114366201,
5113305014,
5113358003,
5113304487,
5114417105,
5114424676,
5113304650,
5113367377,
5114369200,
51261401,
5114777549,
51741268,
5114777455,
5114774516,
5114773622,
5113305467,
5113304203,
5114783285,
5113359663,
5114772585,
5113264881,
51742266,
51741034,
5113270665,
51743353,
5262023369,
51824457,
5113300154,
51741708,
51743559,
5114721267,
51724906,
51741638,
5114720640,
5114774220,
5113361842,
5114424217,
5113300733,
5113300727,
51721610,
51741306,
5262025107,
5114705353,
5114705354,
5114364100,
5114721420,
5114773269,
5113361001,
5113250293,
5113251986,
5113362842,
5114775027,
5113366853,
5114775028,
5113306318,
5114775029,
5114775030,
5113267373,
5113366502,
5114411141,
5113275029,
51821471,
5114422868,
5113354137,
5113353833,
5113357528,
5113259918,
5113271074,
5113272789,
5113262702,
5113268835,
5113271321,
5113270746,
5113354530,
5114424202,
5114366721,
5113354895,
5113253247,
5114412957,
5113360789,
5113300618,
5113357939,
5113364791,
51744340,
5114721463,
5114781693,
51741223,
5114721641,
51261034,
5114720243,
51742260,
5114721188,
5114722850,
51721188,
51741181,
51741240,
51722512,
51721021,
51721279,
51743362,
5113255267,
5113268325,
5113305978,
5113307525,
5113268496,
5114723014,
5113272221,
51742162,
5113304842,
5113252982,
5114774135,
5114773673,
5114783308,
51743740,
5113268118,
5113268030,
5113268119,
51743430,
5122062814,
5114720855,
51261038,
5114221015,
5113253810,
51221457,
5276064120,
5114721618,
5113262121,
5114777346,
5262025612,
5114787794,
51742877,
5113253687,
5113277062,
5113252472,
5114781790,
5114787739,
51261755,
5114721593,
5114281269,
5113306004,
5113308220,
5113256419,
5113250158,
5114705033,
5114365450,
5113254870,
5114410515,
5113257601,
5113256038,
5113257641,
5113256315,
5113265965,
5113255966,
5113251715,
5113267248,
5113261843,
5113250333,
5113257721,
5113257879,
5113274292,
5113257661,
5113276067,
5113269272,
5113265019,
5113276887,
5122062879,
5113304829,
5114410551,
5113350931,
5114365969,
5113352348,
5113367848,
5114221226,
5113256440,
5114413406,
5113255597,
5113364670,
5113250265,
5114364594,
5122062494,
5113250313,
5113355216,
5113353353,
51221470,
5113366157,
5113302702,
5114423358,
51221841,
5113352000,
5113362600,
5113277084,
5113306451,
5113355512,
5114782634,
51741788,
5113260417,
51743849,
5114723083,
5113360268,
5113364869,
5113255075,
5114221234,
5113275010,
5114363032,
5113352135,
5113306793,
5114413419,
51741182,
5113363883,
5114787949,
5113367666,
5262025463,
5114722864,
5113302515,
5262023136,
5113257066,
5114707235,
5113260122,
5113270007,
5113267257,
51723400,
5114368561,
5114420904,
5113304059,
5114784575,
5113360973,
5113356934,
5114420036,
5114368129,
5114783503,
5262025240,
5114413425,
5113267381,
5113269346,
5113267081,
5113303233,
5114221677,
5114364666,
5114722469,
5113261227,
5114424104,
5114770732,
51874416,
5113275492,
5114369853,
5114722926,
5114780034,
5114773021,
5114422317,
5113265578,
51741239,
51742854,
5114368866,
5113256839,
51742224,
5114721465,
5113259789,
5113275371,
5113271369,
5113250805,
5113277084,
51861357,
5113360620,
5113255734,
5114770058,
5114424619,
5113354567,
5113363625,
5114368319,
5114369697,
5262023710,
5113272736,
51742913,
51722324,
5113271993,
5113252864,
51263166,
51263008,
51263288,
5114722277,
5114364195,
5113301121,
5113353191,
5113267642,
51743046,
5114721038,
5113267103,
51221871,
5114316562,
51742276,
5113305131,
5113357658,
5114721326,
5262021715,
5113274664,
5113251896,
5262030881,
5113263643,
5114721334,
5113275720,
5113353550,
5113263433,
5113358900,
5113351956,
5114363065,
5262025642,
5262021686,
5262025284,
5262025246,
5262025647,
5262024254,
5262025589,
5262025254,
5113262603,
51723307,
5113353608,
51721626,
5113252023,
5113353926,
5113306482,
5113257141,
5114784962,
5114413398,
51723529,
5113257180,
5113307982,
5113351951,
5114720678,
5113250668,
51742664,
5113268223,
5113367588,
5114774490,
5113363135,
5113366515,
5113257880,
5113357483,
5114420042,
5262025301,
5276064035,
5113361912,
5113354285,
5276064045,
5262027154,
51824967,
5114413468,
5114220323,
5113256058,
5113262093,
51741412,
5113354149,
5113252218,
5113263243,
51724450,
51741873,
5114721756,
5114770760,
5113253890,
5113270027,
5113268715,
5114770724,
5113262626,
5113308026,
5113252958,
5276064111,
5113269404,
5114367108,
5113271870,
5114780136,
5262025249,
5113250911,
51742694,
5114720961,
5114790372,
5113358093,
5113301590,
51741321,
51741213,
5114770741,
5114770705,
5113250121,
5113272114,
51742842,
5113270000,
5113272349,
5113262376,
5113272344,
5113306505,
51731695,
5114721373,
5276064068,
5113254406,
5113267987,
5113305556,
5262030948,
5113262964,
5113352153,
5113267194,
5113259719,
5114423145,
5114420388,
5113264175,
5114788082,
5114721091,
51723625,
5114790306,
5114785351,
5113358685,
5113262806,
5113263441,
5114365628,
5113353788,
5113365833,
51721375,
5114311002,
5122062043,
5262022592,
5276064069,
5262025473,
5114782964,
5114315915,
5113259065,
5113357229,
5113366417,
5113353706,
5113260852,
5113271555,
5113266782,
5276064105,
5122062502,
5113350110,
5113304104,
5113267493,
5113268359,
5113277508,
51724930,
51723225,
51724663,
51742634,
5262022778,
51743813,
5114720424,
5114774424,
5113358631,
51724286,
5113262959,
5114420805,
5113255459,
5113302378,
5114777045,
5114787979,
5114785127,
5113306680,
1,
5113357566,
5113302926,
5113350278,
5114722634,
5113365893,
5114785355,
5113259570,
5113363634,
5113352329,
51721577,
5114365196,
52762164,
5113361832,
5114423195,
5113353818,
5113260195,
5262030935,
51875107,
5113364290,
52761013,
5114420401,
5113303537,
5262025214,
5114420684,
5114420225,
5114420439,
5114420769,
5113302919,
5113302927,
5113306895,
5114420519,
5113304333,
5113261217,
5113255983,
51871798,
51874404,
5113276026,
5113251644,
5114420545,
51871613,
5113351138,
5262025259,
5113302660,
5113261782,
5116741673,
5113363899,
5113271458,
5114420102,
5113356251,
5113265551,
5262025322,
5113367040,
5113264988,
52761695,
5113363701,
5114421111,
5114721530,
51874454,
5114777254,
5113359474,
5114364040,
5113358046,
5114723233,
5262021515,
5113362405,
5114368443,
5113357290,
5113357291,
5114785353,
5114785354,
51741649,
5113358098,
51742833,
5114415992,
5113253866,
5113352324,
5114368440,
5114777673,
5262025579,
51724824,
5262025499,
5262025588,
5113366516,
51743320,
5113267883,
5113353575,
5113305092,
5113262209,
5113201126,
5113358449,
5113258715,
5113250174,
5114705508,
51724814,
1,
51723479,
5113351726,
5113263786,
5114787895,
51724861,
51743503
         ) AND
       pl.status_laporan = "1"
       GROUP BY dt.id
    ');
    return $query;
  }

  public static function provisioningListMitra($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        DATE_FORMAT(dt.tgl,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
        SUM(IF(plm.id_item = "Preconnectorized-1C-100-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-75-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-50-NonAcc",i.num * plm.qty,0) + IF(plm.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),i.num * plm.qty,0) + IF(plm.id_item = "AC-OF-SM-1B",plm.qty,0) + IF(plm.id_item in ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80"),i.num * plm.qty,0)) as dc_preconn,
        -- SUM( i.num * plm.qty) AS dc_preconn,
        SUM(case when plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        -- SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "Preconnectorized-1C-100-NonAcc" then plm.qty else 0 end) as precon100,
        SUM(case when plm.id_item in ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80") then plm.qty else 0 end) as precon80,
        SUM(case when plm.id_item = "Preconnectorized-1C-75-NonAcc" then plm.qty else 0 end) as precon75,
        SUM(case when plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") then plm.qty else 0 end) as precon50,
        SUM(case when plm.id_item = "Preconnectorized-1C-150-NonAcc" then plm.qty else 0 end) as preconnonacc,
        SUM(CASE when plm.id_item in ("BREKET","BREKET-A") THEN plm.qty else 0 end) as breket,
        SUM(CASE when plm.id_item in ("S-Clamp-Spriner","S-Clamp-Sprinerr") THEN plm.qty else 0 end) as clamps,
        SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pulstrap,
        SUM(CASE when plm.id_item = "RJ45-5" THEN plm.qty else 0 end) as rj45,
        SUM(CASE when plm.id_item in ("RS-IN-SC-1","RS-IN-SC-11") THEN plm.qty else 0 end) as roset,
        SUM(CASE when plm.id_item = "SOC-ILS" THEN plm.qty else 0 end) as SOCIlsintentech,
        SUM(CASE when plm.id_item = "SOC-SUM" THEN plm.qty else 0 end) as SOCSumitomo,
        SUM(CASE when plm.id_item = "AD-SC" THEN plm.qty else 0 end) as adsc,
        0 as conduit,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
        pl.jml_tiang,
        pl.myir,
        gt.SM,
        m.datel as area_migrasi
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       -- LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN rfc_sal i on plm.id_item_bantu = i.id_item_bantu
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN maintenance_datel m ON dps.sto = m.sto
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id

     WHERE
       (dt.dispatch_by is NULL OR dt.dispatch_by = 5) AND
       dt.tgl like "%'.$tgl.'%" AND
       pl.status_laporan in ("1","37","38")
       '.$getWhere.'
       GROUP BY pl.id
    ');

    return $query;
  }
 public static function provisioningListMitra_API($jenis,$tgl,$witel,$datel,$sto){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $getWhere_Witel = '';
    if ($witel<>"ALL"){
      $getWhere_Witel = 'AND m.witel = "'.$witel.'"';
    }
    $getWhere_Datel = '';
    if ($datel<>"ALL"){
      $getWhere_Datel = 'AND m.datel = "'.$datel.'"';
    }
    $getWhere_Sto = '';
    if ($sto<>"ALL"){
      $getWhere_Sto = 'AND m.sto = "'.$sto.'"';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        DATE_FORMAT(dt.tgl,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
        SUM(IF(plm.id_item = "Preconnectorized-1C-100-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-75-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-50-NonAcc",i.num * plm.qty,0) + IF(plm.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),i.num * plm.qty,0) + IF(plm.id_item = "AC-OF-SM-1B",plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-80-NonAcc",i.num * plm.qty,0)) as dc_preconn,
        -- SUM( i.num * plm.qty) AS dc_preconn,
        SUM(case when plm.id_item = "AC-OF-SM-1B" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        -- SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "Preconnectorized-1C-100-NonAcc" then plm.qty else 0 end) as precon100,
        SUM(case when plm.id_item = "Preconnectorized-1C-75-NonAcc" then plm.qty else 0 end) as precon75,
        SUM(case when plm.id_item = "Preconnectorized-1C-50-NonAcc" then plm.qty else 0 end) as precon50,
        SUM(case when plm.id_item = "Preconnectorized-1C-150-NonAcc" then plm.qty else 0 end) as preconnonacc,
        SUM(CASE when plm.id_item in ("BREKET","BREKET-A") THEN plm.qty else 0 end) as breket,
        SUM(CASE when plm.id_item in ("S-Clamp-Spriner","S-Clamp-Sprinerr") THEN plm.qty else 0 end) as clamps,
        SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pulstrap,
        SUM(CASE when plm.id_item = "RJ45-5" THEN plm.qty else 0 end) as rj45,
        SUM(CASE when plm.id_item in ("RS-IN-SC-1","RS-IN-SC-11") THEN plm.qty else 0 end) as roset,
        SUM(CASE when plm.id_item = "SOC-ILS" THEN plm.qty else 0 end) as SOCIlsintentech,
        SUM(CASE when plm.id_item = "SOC-SUM" THEN plm.qty else 0 end) as SOCSumitomo,
        SUM(CASE when plm.id_item = "AD-SC" THEN plm.qty else 0 end) as adsc,
        0 as conduit,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
        pl.jml_tiang,
        pl.myir,
        gt.SM,
        m.datel as area_migrasi
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       -- LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN rfc_sal i on plm.id_item_bantu=i.id_item_bantu
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN maintenance_datel m ON dps.sto = m.sto
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id

     WHERE
       dt.dispatch_by is NULL AND
       dps.orderId <> "" AND
       dps.orderDatePs like "%'.$tgl.'%" AND
       pl.status_laporan in ("1","37","38")
       '.$getWhere.'
       '.$getWhere_Witel.'
       '.$getWhere_Datel.'
       '.$getWhere_Sto.'
       GROUP BY pl.id
    ');

    return $query;
  }
  public static function assuranceListMitra($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.updated_at as tanggal_dispatch,
        roc.STO as sto,
        dn1log.Workzone,
        roc.no_tiket as orderId,
        roc.no_tiket as orderNcli,
        dps.ndemPots as ndemPots,
        dps.internet as ndemSpeedy,
        dps.orderName as orderName,
        "ASSURANCE" as jenisPsb,
        "~" as orderStatus,
        dps.orderAddr as orderAddr,
        dt.id as id_dt,
        DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        DATE_FORMAT(roc.tgl_open,"%Y-%m-%d") as orderDatePs,
        SUM( i.num ) AS dc_preconn,
        -- SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        -- SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6", "UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        -- SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        -- SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        -- SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        -- SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
        SUM(case when plm.id_item = "AC-OF-SM-1B" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "AD-SC" then plm.qty else 0 end) as AD_SC,
        SUM(case when plm.id_item = "ODP-CA-16" then plm.qty else 0 end) as ODP_CA_16,
        SUM(case when plm.id_item = "ODP-CA-8" then plm.qty else 0 end) as ODP_CA_8,
        SUM(case when plm.id_item = "ODP-PB-16" then plm.qty else 0 end) as ODP_PB_16,
        SUM(case when plm.id_item = "ODP-PB-8" then plm.qty else 0 end) as ODP_PB_8,
        SUM(case when plm.id_item = "PS-1-4" then plm.qty else 0 end) as PS_1_4,
        SUM(case when plm.id_item = "PS-1-8" then plm.qty else 0 end) as PS_1_8,
        SUM(case when plm.id_item = "PS-1-16" then plm.qty else 0 end) as PS_1_16,
        SUM(case when plm.id_item = "SOC-SUM" then plm.qty else 0 end) as SOC_SUM,
        SUM(case when plm.id_item = "SOC-ILS" then plm.qty else 0 end) as SOC_ILS,
        0 as conduit,
        dtjl.mode,
        pla.action,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
       -- LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN rfc_sal i on plm.id_item_bantu=i.id_item_bantu
       LEFT JOIN roc ON dt.Ndem = roc.no_tiket
       LEFT JOIN data_nossa_1_log dn1log ON dt.Ndem = dn1log.Incident
       LEFT JOIN Data_Pelanggan_Starclick dps ON dn1log.Service_No = dps.internet
       LEFT JOIN mdf m ON roc.sto_trim = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       roc.no_tiket <> "" AND
       dt.tgl like "%'.$tgl.'%" AND
       pl.status_laporan = 1
       '.$getWhere.'
       GROUP BY dt.id
    ');
    return $query;
  }

  public static function provisioningList($tgl,$jenis,$status,$so,$ketTw=null){
     $whereTgl = ' pl.modified_at like "%'.$tgl.'%"';
    if ($ketTw<>null){
        $ketTgl = explode('_',$ketTw);

        $whereTgl = ' (pl.modified_at "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };

    $where_area = "";
    if ($so<>"ALL"){
      $where_area = ' AND m.datel = "'.$so.'"';
    }
    if ($status == "NO UPDATE"){
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    } else if ($status == "ALL"){
      $where_status = '';
    } else {
      $where_status = '  AND pls.laporan_status = "'.$status.'"';
    }
    $query = DB::SELECT('
      SELECTcx
        *,
        dt.id as id_dt,
        DATE_FORMAT(dt.created_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
        DATE_FORMAT(dps.orderDate,"%Y-%m-%d") as tanggal_wo,
        SUM( i.num ) AS dc_preconn,
        SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        0 as conduit,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
        tt.ketTiang,
        dps.sto,
        pl.redaman_odp,
        pl.status_kendala,
        pl.tgl_status_kendala,
        gt.title as sektorNama,
        m.datel as area_migrasi,
        gt.SM
      FROM
       dispatch_teknisi dt
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN maintenance_datel m ON dps.sto = m.sto
       LEFT JOIN regu r ON dt.id_regu = r.id_regu
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
       LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
     WHERE
       dt.dispatch_by is NULL AND
       dps.orderId <> "" AND
       '.$whereTgl.'
       '.$where_status.'
       '.$where_area.'
       GROUP BY dt.id
       ORDER BY gt.urut
    ');
    return $query;
  }

  public static function reportPotensi(){
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
      $enddate = date('Y-m-d');
      $data = DB::SELECT('
      SELECT
        m.area_migrasi,
        SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is NULL THEN 1 ELSE 0 END) as jumlah_undispatch
      FROM
        Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'") AND
        dps.orderStatusId = 16 AND m.mdf<>""
        GROUP BY m.area_migrasi
        ORDER BY jumlah_undispatch DESC
      ');
      return $data;

        // count(*) as jumlah
        // -- SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is not NULL THEN 1 ELSE 0 END) as jumlah_dispatch,
        // -- SUM(CASE WHEN dps.orderStatusId = 16 AND pl.status_laporan not in (1,5,6,8,28,29,30,31) AND dt.id is not NULL THEN 1 ELSE 0 END) as fo_kendala,
        // -- SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is NULL AND TIMESTAMPDIFF(HOUR, dps.orderDate, now()) <= 2 THEN 1 ELSE 0 END) as jumlah_undispatchKurang2,
        // -- SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is NULL AND TIMESTAMPDIFF(HOUR, dps.orderDate, now()) > 2 THEN 1 ELSE 0 END) as jumlah_undispatchLebih2,
        // -- SUM(CASE WHEN dps.orderStatus LIKE "%fallout%" THEN 1 ELSE 0 END) as fo,
        // (dps.orderStatusId = 16 OR dps.orderStatus LIKE "%fallout%" ) AND m.mdf<>""
        // SUM(CASE WHEN dps.orderStatusId in (49,50,51) AND pl.status_laporan = 3 AND dt.id is not NULL THEN 1 ELSE 0 END) as fo_kendala,
        // SUM(CASE WHEN dps.orderStatusId in (49,50,51) AND pl.status_laporan = 1 AND dt.id is not NULL THEN 1 ELSE 0 END) as fo_up,
        // SUM(CASE WHEN dps.orderStatusId = 52 AND dt.id is not NULL THEN 1 ELSE 0 END) as act_com,
        // SUM(CASE WHEN dps.orderStatusId = 7 AND dt.id is not NULL AND dps.orderDatePs LIKE "%'.$enddate.'%" THEN 1 ELSE 0 END) as ps,

       // (dps.orderStatusId = 16 OR dps.orderStatus like "%fallout%")
        // (dps.orderStatusId in (16,7,52,49,50,51) OR dps.orderStatus LIKE "%fallout%" ) AND m.mdf<>""
  }

  public static function listpotensi($jenis,$area){
    $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
    $enddate = date('Y-m-d');

    if ($area == "ALL"){
      $where_area = '';
    } else {
      $where_area = 'AND m.area_migrasi="'.$area.'"';
    }
    switch ($jenis){
      case "undispatch" :
        $where = '
          (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
          AND dps.orderStatusId in (16,40,14,5,4,2,0,49) AND m.mdf<>""
          AND (dt.id is NULL or dt.id_regu is NULL)
        ';
      break;

      // case "dispatched" :
      //   $where = '
      //     (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
      //     AND dps.orderStatusId = 16
      //     AND dt.id is not NULL
      //   ';
      // break;
      // case "pi" :
      //   $where = '
      //     (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
      //     AND dps.orderStatusId = 16
      //   ';
      // break;
      // case "fo" :
      //   $where = '
      //     (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
      //     AND dps.orderStatus LIKE "%fallout%"
      //   ';
      // break;

      case "kendala" :
        $where = '
            (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
            AND dps.orderStatusId = 16
            AND pl.status_laporan not in (1,5,6,8,28,29,30,31,32,37,38,4,7)
            AND dt.id is not NULL
        ';
      break;

      // case "foup" :
      //   $where = '
      //       (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
      //       AND dps.orderStatusId in (49,50,51)
      //       AND pl.status_laporan=1
      //       AND dt.id is not NULL
      //   ';
      // break;

      // case "actcom" :
      //   $where = '
      //       (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
      //       AND dps.orderStatusId=52
      //       AND dt.id is not NULL
      //   ';
      // break;

      // case "ps" :
      //   $where = '
      //       (DATE(dps.orderDate) LIKE "%'.$enddate.'%")
      //       AND dps.orderStatusId=7
      //       AND dt.id is not NULL
      //   ';
      // break;

      default :
        $where = '';
      break;
    }
    // $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
    // $enddate = date('Y-m-d');
    $data = DB::SELECT('
      SELECT
        *
      FROM
        Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
        LEFT JOIN mdf m ON dps.sto = m.mdf
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
      WHERE
        '.$where.'
        '.$where_area.'
      ');

    return $data;
    // (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
  }

  public static function provisioning($jenis,$tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        dt.dispatch_by is NULL AND
        dps.orderId <> "" AND
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status
      ORDER BY pls.urutan asc, pls.laporan_status asc
    ');
    return $query;
  }

  public static function absenkanbosku(){
    $query = DB::SELECT('select * from a2s_kehadiran where tgl = "'.date('Y-m-d').'" AND hadir = "TIDAK ABSEN" AND jadwal = "MASUK"');
    return $query;
  }

  public static function getRekon($date){
    $query = DB::SELECT('
      SELECT
        a.JENIS,
        count(*) as jumlah_order
      FROM
       ms2n a
      WHERE
       a.Status = "PS" AND
       a.Tgl_PS LIKE "%'.$date.'%"
      GROUP BY a.jenis
    ');
    return $query;
  }

  public static function getRekonList($jenis,$date){
    $query = DB::SELECT('
      SELECT
        *
      FROM
        ms2n a
        LEFT JOIN mdf b ON a.mdf = b.mdf
      WHERE
        a.Status = "PS" AND
        a.Tgl_PS LIKE "%'.$date.'%" AND
        a.JENIS = "'.$jenis.'"
    ');
    return $query;
  }

  public static function getMigrasiStatusList($tgl,$jenis,$status,$so,$order){
    $where_area = "";
    if ($so<>"ALL"){
      $where_area = ' AND m.area_migrasi = "'.$so.'"';
    }

    if ($status=="NO UPDATE"){
        $whereStatus = 'AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL)';
    }
    else if ($status=="ALL"){
        $whereStatus = '';
    }
    else{
        $whereStatus = "AND pls.laporan_status = '".$status."'";
    };

    if ($order=="ALL"){
        $where_order = '';
    }
    else if ($order=='EMPTY'){
        $where_order = "AND dt.ketOrder is NULL";
    }
    else{
      $where_order = "AND dt.ketOrder='".$order."'";
    };

    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        dt.updated_at as tanggal_dispatch,
        dm.STO as sto,
        dm.ND as orderId,
        dm.NCLI as orderNcli,
        dm.ND as ndemPots,
        dm.ND_REFERENCE as ndemSpeedy,
        dm.NAMA as orderName,
        "MIGRASI AS IS" as jenisPsb,
        "~" as orderStatus,
        DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        0 as orderDatePs,
        SUM( i.num ) AS dc_preconn,
        SUM(case when plm.id_item = "AC-OF-SM-1B" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        0 as conduit,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
        dm.LQUARTIER as orderAddr,
        tt.ketTiang,
        dt.ketOrder
      FROM
       dispatch_teknisi dt
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN rfc_sal i on plm.id_item_bantu=i.id_item_bantu
       LEFT JOIN dossier_master dm ON dt.Ndem = dm.ND
       LEFT JOIN mdf m ON dm.STO = m.mdf
       LEFT JOIN regu r ON dt.id_regu = r.id_regu
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
       LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
     WHERE
       dm.ND <> "" AND
       dt.tgl like "%'.$tgl.'%"
       '.$whereStatus.'
       '.$where_area.'
       '.$where_order.'
       GROUP BY dm.ND
    ');

   // -- LEFT JOIN item i ON plm.id_item = i.id_item
    // -- SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
    // -- SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
    // -- SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
    // -- SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
    // -- SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,

    return $query;
  }

  public static function getMigrasiStatus($tgl,$jenis){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah,
       dt.ketOrder
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN dossier_master dm ON dt.Ndem = dm.ND
       LEFT JOIN dataStartClickMigrasi dps ON dt.Ndem = dps.orderId
       LEFT JOIN mdf m ON (dm.STO = m.mdf oR dps.Sto = m.mdf)
      WHERE
        (dm.ND <> "" OR dps.orderId<>"")AND
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status, dt.ketOrder
      ORDER BY pls.laporan_status_id
    ');
      // GROUP BY dt.ketOrder
      // GROUP BY dt.ketOrder
    return $query;
  }

  public static function getMigrasi($tgl){
    $query = DB::SELECT('
      SELECT
        *,
        1 as UP2,
        (SELECT
          count(*)
        FROM
          dossier_master dm
        LEFT JOIN mdf m ON dm.STO = m.mdf
        LEFT JOIN dispatch_teknisi dt ON dm.ND = dt.Ndem
          WHERE m.area_migrasi = a.area_migrasi AND dt.Ndem <> "" AND dt.tgl = "'.$tgl.'"
        ) +
        (SELECT
          count(*)
        FROM
          dossier_master dm
        LEFT JOIN mdf m ON dm.STO = m.mdf
        LEFT JOIN dispatch_teknisi dt ON dm.ND_REFERENCE = dt.Ndem
          WHERE m.area_migrasi = a.area_migrasi AND dt.Ndem <> "" AND dt.tgl = "'.$tgl.'"
        ) as WO_HI,
        (SELECT
          count(*)
         FROM dossier_master dm
         LEFT JOIN mdf m ON dm.STO = m.mdf
         LEFT JOIN dispatch_teknisi dt ON (dm.ND = dt.Ndem)
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         WHERE m.area_migrasi = a.area_migrasi AND pl.status_laporan = 1 AND dt.tgl = "'.$tgl.'") +
         (SELECT
           count(*)
          FROM dossier_master dm
          LEFT JOIN mdf m ON dm.STO = m.mdf
          LEFT JOIN dispatch_teknisi dt ON (dm.ND_REFERENCE = dt.Ndem)
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          WHERE m.area_migrasi = a.area_migrasi AND pl.status_laporan = 1 AND dt.tgl = "'.$tgl.'")
          as UP,
        (SELECT
          count(*)
         FROM dossier_master dm
         LEFT JOIN mdf m ON dm.STO = m.mdf
         LEFT JOIN dispatch_teknisi dt ON (dm.ND = dt.Ndem)
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         WHERE m.area_migrasi = a.area_migrasi AND pl.status_laporan = 1 AND dt.tgl = "'.$tgl.'") +
         (SELECT
           count(*)
          FROM dossier_master dm
          LEFT JOIN mdf m ON dm.STO = m.mdf
          LEFT JOIN dispatch_teknisi dt ON (dm.ND_REFERENCE = dt.Ndem)
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          WHERE m.area_migrasi = a.area_migrasi AND pl.status_laporan = 5 AND dt.tgl = "'.$tgl.'")
          as OGP,
        (SELECT
          count(*)
         FROM dossier_master dm
         LEFT JOIN mdf m ON dm.STO = m.mdf
         LEFT JOIN dispatch_teknisi dt ON (dm.ND = dt.Ndem)
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         WHERE m.area_migrasi = a.area_migrasi AND pls.grup = "KT" AND dt.tgl = "'.$tgl.'") +
         (SELECT
           count(*)
          FROM dossier_master dm
          LEFT JOIN mdf m ON dm.STO = m.mdf
          LEFT JOIN dispatch_teknisi dt ON (dm.ND_REFERENCE = dt.Ndem)
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          WHERE m.area_migrasi = a.area_migrasi AND pls.grup = "KT" AND dt.tgl = "'.$tgl.'")
          as KT,
        (SELECT
          count(*)
         FROM dossier_master dm
         LEFT JOIN mdf m ON dm.STO = m.mdf
         LEFT JOIN dispatch_teknisi dt ON (dm.ND = dt.Ndem)
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         WHERE m.area_migrasi = a.area_migrasi AND pls.grup = "KP" AND dt.tgl = "'.$tgl.'") +
         (SELECT
           count(*)
          FROM dossier_master dm
          LEFT JOIN mdf m ON dm.STO = m.mdf
          LEFT JOIN dispatch_teknisi dt ON (dm.ND_REFERENCE = dt.Ndem)
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          WHERE m.area_migrasi = a.area_migrasi AND pls.grup = "KP" AND dt.tgl = "'.$tgl.'")
          as KP,

         (SELECT
            count(*)
          FROM Data_Pelanggan_Starclick dps
            LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE m.area_migrasi = a.area_migrasi AND dps.orderStatusId = "7" AND jenisPsb LIKE "%MIGRATE%" AND date(orderDatePs) = "'.$tgl.'"
         ) as PS,
         (SELECT
            count(*)
          FROM Data_Pelanggan_Starclick dps
            LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE m.area_migrasi = a.area_migrasi AND dps.orderStatusId = "7" AND jenisPsb = "Migrate" AND date(orderDatePs) = "'.$tgl.'"
         ) as PS_asis
      FROM
        mdf a
      LEFT JOIN area_migrasi am ON a.area_migrasi = am.area_migrasi
      WHERE am.area_migrasi <> ""
      GROUP BY a.area_migrasi
      ORDER BY am.id_area_migrasi
    ');
    return $query;
  }

  public static function CountWO($id){
    // $Where  = DashboardModel::GetWhere($id,NULL);
    // $result = DashboardModel::QueryCount($Where);
    // return $result;
    echo "test";
  }

  public static function getAssuranceList($date,$status){
    if ($status == "ALL"){
      $getWhere = '';
    } else if ($status == "NO UPDATE") {
      $getWhere = 'AND (d.laporan_status = "'.$status.'" OR c.id_tbl_mj is NULL)';
    } else {
      $getWhere = 'AND d.laporan_status = "'.$status.'"';
    }

    $query = DB::SELECT('
      SELECT
        *,
        a.Segment_Status,
        b.id as id_dt,
        e.action,
        g.penyebab,
        a.*,
        a.Summary as headline,
        a.Reported_Date as tgl_open,
        0 as umur,
        a.Incident as no_tiket,
        aa.Status as aaIncident,
        a.jenis_ont as ontByHd,
        c.typeont as ontByTeknisi,
        c.modified_at as tglClose,
        c.created_at as tglCloseCreate,
        a.Workzone,
        ib.ONU_Link_Status as status_ukur
      FROM
        dispatch_teknisi b
        LEFT JOIN data_nossa_1_log a ON a.Incident = b.Ndem
        LEFT JOIN data_nossa_1 aa ON a.Incident = aa.Incident
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
        LEFT JOIN psb_laporan_action e ON c.action = e.laporan_action_id
        LEFT JOIN psb_laporan_penyebab g ON c.penyebabId = g.idPenyebab
        LEFT JOIN regu f ON b.id_regu = f.id_regu
        LEFT JOIN group_telegram gt ON f.mainsector = gt.chat_id
        LEFT JOIN ibooster ib ON b.Ndem = ib.order_id
      WHERE
        a.Incident <> "" AND
        b.tgl LIKE "%'.$date.'%" '.$getWhere.'
      ORDER BY
      a.Reported_Date DESC
    ');
    return $query;
  }
  public static function getAssuranceList2($date,$status,$area){
    if ($area == "ALL") {
      $where_status = "";
    } else {
      $where_status = '  gt.title = "'.$area.'" AND ';
    }
    if ($status == "UNDISPATCH"){
      $where_grup = ' c.id is NULL AND TIMESTAMPDIFF( HOUR , ne.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0';
    } else if ($status == "UNDISPATCH_after") {
      $where_grup = ' c.id is NULL AND TIMESTAMPDIFF( HOUR , ne.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0';
    } else if ($status == "SISA"){
      $where_grup = ' (e.grup = "'.$status.'" OR (c.id is not NULL AND e.grup is NULL)) AND date(c.tgl)="'.date('Y-m-d').'"';

    } else if ($status == "SISA_H"){
      $where_grup = ' (e.grup = "SISA" OR (c.id is not NULL AND e.grup is NULL)) AND date(c.tgl) > "'.date('Y-m-d').'"';
    } else if ($status == "SISA_EXP"){
      $where_grup = ' (e.grup = "'.$status.'" OR (c.id is not NULL AND e.grup is NULL)) AND date(c.tgl) < "'.date('Y-m-d').'"';
    } else {
      $where_grup = ' e.grup = "'.$status.'"';
    }
    $query = DB::SELECT('
      SELECT
        *,
        d.action,
        ne.TROUBLE_NO as no_tiket,
        ne.ND_TELP as no_telp,
        ne.ND_INT as no_speedy,
        ne.HEADLINE as headline,
        ne.TROUBLE_OPENTIME as tgl_open,
        ne.CMDF as sto_trim,
        (ne.hari*24) as umur,
        c.id as id_dt,
        h.action as action
      FROM
        rock_excel ne
      LEFT JOIN mdf a ON a.mdf = ne.CMDF
      LEFT JOIN group_telegram gt ON a.sektor_asr = gt.sektor
      LEFT JOIN dispatch_teknisi c ON ne.TROUBLE_NO = c.Ndem
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_penyebab g ON d.penyebabId = g.idPenyebab
      LEFT JOIN psb_laporan_action h ON d.action = h.laporan_action_id
      LEFT JOIN regu f ON c.id_regu = f.id_regu
      WHERE
        '.$where_status.'
        '.$where_grup.'
      ORDER BY
      ne.hari DESC
    ');
    return $query;
  }

  public static function getAssuranceListbyAction($date,$status,$source,$sektor){
    if ($source=="TOMMAN"){
      $where_source = 'AND dd.source="'.$source.'"';
    } else {
      $where_source = 'AND dd.source<>"TOMMAN"';
    }
    if ($sektor=="ALL"){
      $where_sektor = '';
    } else {
      $where_sektor = 'AND gt.sektor = "'.$sektor.'"';
    }
    $status = urldecode($status);
    if ($status == "ALL"){
      $getWhere = "";
    } else {
      $getWhere = 'AND b.action LIKE  "%'.$status.'%"';
    }
    $query = DB::SELECT('
    select
      *,
      c.id as id_dt,
      dd.Workzone as sto,
      a.modified_at as tglClose,
      a.created_at as tglCloseCreate,
      aa.status as aaIncident,
      ib.ONU_Link_Status as status_ukur
    from
      psb_laporan a
      left join psb_laporan_action b ON a.action = b.laporan_action_id
      left join psb_laporan_penyebab bb ON a.penyebabId = bb.idPenyebab
      left join dispatch_teknisi c ON a.id_tbl_mj = c.id
      left join roc d ON c.Ndem = d.no_tiket
      left join data_nossa_1_log dd ON c.Ndem = dd.Incident
      LEFT JOIN data_nossa_1 aa ON dd.Incident = aa.Incident
      left join regu e ON c.id_regu = e.id_regu
      left join psb_laporan_status f ON a.status_laporan = f.laporan_status_id
      left join group_telegram gt ON e.mainsector = gt.chat_id
      left join ibooster ib ON c.Ndem = ib.order_id
      where b.action is not NULL AND c.tgl LIKE "%'.$date.'%" '.$getWhere.'
      '.$where_source.'
      '.$where_sektor.'
 order by d.umur DESC');
    return $query;
  }


  public static function Tiket2(){
    $query = DB::SELECT('
      SELECT
        f.title,
        f.chat_id,
        count(*) as jumlah,
        SUM(CASE WHEN (e.grup = "SISA" OR d.id IS NULL) THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN e.grup = "KP" THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN e.grup = "KT" THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN e.grup = "HR" THEN 1 ELSE 0 END) as HR,
        SUM(CASE WHEN e.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN d.status_laporan = 1 THEN 1 ELSE 0 END) as UP
      FROM
        rock_excel ne
      LEFT JOIN mdf a  ON a.mdf = ne.CMDF
      LEFT JOIN dispatch_teknisi c ON ne.TROUBLE_NO = c.Ndem
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN regu g ON c.id_regu = g.id_regu
      LEFT JOIN group_telegram f ON f.chat_id = g.mainsector
      WHERE
        ne.TROUBLE_NO <> "" AND
        ne.is_gamas = 0
      GROUP BY f.title
      ORDER BY jumlah DESC
    ');
    return $query;
  }

  public static function Tiket_INT($date){
    $query = DB::SELECT('
      SELECT
        f.title,
        f.chat_id,
        count(*) as jumlah,
        SUM(CASE WHEN (e.grup = "SISA" OR d.id IS NULL) THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN e.grup = "KP" THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN e.grup = "KT" THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN e.grup = "HR" THEN 1 ELSE 0 END) as HR,
        SUM(CASE WHEN e.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN d.status_laporan = 1 AND date(d.modified_at) = "'.$date.'" THEN 1 ELSE 0 END) as UP
      FROM
        data_nossa_1_log ne
      LEFT JOIN mdf a  ON a.mdf = ne.Workzone
      LEFT JOIN dispatch_teknisi c ON ne.Incident = c.Ndem
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN regu g ON c.id_regu = g.id_regu
      LEFT JOIN group_telegram f ON f.chat_id = g.mainsector
      WHERE
        ne.Source = "TOMMAN" AND 
        (
          e.grup IN ("SISA","KP","KT","OGP","HR") OR
          d.id IS NULL OR
          (d.status_laporan = 1 AND date(d.modified_at) = "'.$date.'")
        )
      GROUP BY f.title
      ORDER BY jumlah DESC
    ');
    return $query;
  }

  public static function Tiket2List($sektor, $status){
    $where_sektor  = 'AND f.chat_id="'.$sektor.'"';
    if ($sektor=='UNDISPATCH'){
        $where_sektor = 'AND f.chat_id IS NULL';
    }
    elseif ($sektor=='ALL'){
        $where_sektor = '';
    }

    $where_status  = 'AND e.grup="'.$status.'"';
    if ($status=='np'){
        $where_status = 'AND (e.grup="SISA" OR d.id IS NULL)';
    }
    elseif($status=='up'){
        $where_status = 'AND d.status_laporan=1';
    }
    elseif ($status=='open'){
        $where_status = '';
    }
    elseif($status=='sisa'){
        $where_status='AND (e.grup IN ("SISA","KP","KT","OGP") OR d.id IS NULL)';
    };

    $sql = '
      SELECT
        *
      FROM
        rock_excel ne
      LEFT JOIN mdf a  ON a.mdf = ne.CMDF
      LEFT JOIN dispatch_teknisi c ON ne.TROUBLE_NO = c.Ndem
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_penyebab plp ON plp.idPenyebab = d.penyebabId
      LEFT JOIN psb_laporan_action pla on pla.laporan_action_id = d.action
      LEFT JOIN regu g ON c.id_regu = g.id_regu
      LEFT JOIN group_telegram f ON f.chat_id = g.mainsector
      LEFT JOIN ibooster i ON i.ND = ne.nd_int
      WHERE
        ne.TROUBLE_NO <> "" AND
        ne.is_gamas = 0
        '.$where_sektor.'
        '.$where_status.'
      GROUP BY c.Ndem
      ORDER BY i.IP_NE desc, i.Calling_Station_id desc
    ';

    $query = DB::SELECT($sql);
    return $query;
  }

  public static function Tiket(){
    $dateNow = date('Y-m-d');
    //$lastWeek = mktime(date("H"), date("i"), date("s"), date("m"), date("d")-6, date("Y"));
    $lastWeek = date('Y-m-01');
    $query = DB::SELECT('
      SELECT
      date_format(a.ts,"%d-%m-%Y") as day,
      count(*) as jumlah,
      SUM(CASE WHEN d.status_laporan = 1 THEN 1 ELSE 0 END) as jumlah_open
      FROM roc a
      LEFT JOIN roc_active b ON a.no_tiket = b.no_tiket
      LEFT JOIN dispatch_teknisi c ON a.no_tiket = c.Ndem
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      WHERE DATE(a.ts) BETWEEN "'.$lastWeek.'" AND "'.$dateNow.'" GROUP BY day ORDER BY a.ts
    ');
    return $query;
  }

  public static function Action($date){
    $get_sektor = DB::SELECT('
    select
    e.chat_id,
    e.sektor,
    sum(case when f.source<>"TOMMAN" then 1 else 0 end) as jumlah_IN
    from
    psb_laporan a
    left join psb_laporan_action b ON a.action = b.laporan_action_id
    left join dispatch_teknisi c ON a.id_tbl_mj = c.id
    left join regu d ON c.id_regu = d.id_regu
    left join group_telegram e ON d.mainsector = e.chat_id
    left join data_nossa_1_log f ON c.Ndem = f.Incident
    where
    c.tgl LIKE "%'.$date.'%" AND b.action is not NULL and c.dispatch_by<>4 AND
    e.ket_sektor = 1 group by e.sektor order by e.urut');
    $SQL = '
      select
        count(*) as jumlah,
        sum(case when f.source = "TOMMAN" then 1 else 0 end) as jumlah_INT,
        sum(case when f.source <> "TOMMAN" then 1 else 0 end) as jumlah_IN,
        ';
    foreach ($get_sektor as $sektor){
    $SQL .= '
        sum(CASE WHEN e.chat_id="'.$sektor->chat_id.'" AND f.source<>"TOMMAN" THEN 1 ELSE 0 END) as '.$sektor->sektor.',
        sum(CASE WHEN e.chat_id="'.$sektor->chat_id.'" AND f.source="TOMMAN" THEN 1 ELSE 0 END) as INT_'.$sektor->sektor.',';
    }

    $SQL .= '
      b.action
      from
        psb_laporan a
        left join psb_laporan_action b ON a.action = b.laporan_action_id
        left join dispatch_teknisi c ON a.id_tbl_mj = c.id
        left join regu d ON c.id_regu = d.id_regu
        left join group_telegram e ON d.mainsector = e.chat_id
        left join data_nossa_1_log f ON c.Ndem = f.Incident
        where c.tgl LIKE "%'.$date.'%" AND b.action is not NULL and c.dispatch_by<>4 group by a.action order by jumlah_IN DESC';
    $query = DB::SELECT($SQL);
    return $query;
  }

  public static function Status($date){
    $query = DB::SELECT('
      SELECT
        d.laporan_status,
        count(*) as jumlah
      FROM
      dispatch_teknisi b
      LEfT JOIN data_nossa_1_log a ON a.Incident = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
        WHERE
        SUBSTR(b.Ndem,1,2) = "IN" AND
        b.tgl LIKE "%'.$date.'%"
      GROUP BY
        d.laporan_status
    ');
    return $query;
  }

  public static function RocActive(){
    $query = DB::SELECT('
      SELECT
        a.sto as sto,
        count(*) as jumlah,
        SUM(CASE WHEN b.Ndem <> "" THEN 1 ELSE 0 END) as dispatched,
        SUM(CASE WHEN b.Ndem is NULL THEN 1 ELSE 0 END) as undispatched
      FROM
        roc_active a
        LEFT JOIN
        dispatch_teknisi b ON a.no_tiket = b.Ndem
      WHERE
        a.sto NOT IN ("KS. TUBUN")
      GROUP BY
        a.sto
      ORDER BY jumlah DESC
    ');
    return $query;
  }

  public static function GetCount($jenis,$status){
    $tgl_ps   = date('Y-m-d');
    $lastWeek = mktime(date("H"), date("i"), date("s"), date("m"), date("d")-6, date("Y"));
    $jenis = strtoupper($jenis);
    switch ($jenis) {
      case "REKON" :
        switch ($status){
          case "INDIHOME" :
            $SQL = '
              SELECT
                count(*) as Jumlah_Indihome,
                SUM(CASE WHEN a.Status_Indihome="SALES_3P_BUNDLED" THEN 1 ELSE 0 END) as Jumlah_0p_3p,
                SUM(CASE WHEN a.Status_Indihome IN ("MIGRASI_2P_3P_BUNDLED","MIGRASI_2P_3P_UNBUNDLED") THEN 1 ELSE 0 END) as Jumlah_2p_3p,
                SUM(CASE WHEN a.Status_Indihome IN ("MIGRASI_1P_3P_BUNDLED","MIGRASI_1P_3P_UNBUNDLED") THEN 1 ELSE 0 END) as Jumlah_1p_3p

              FROM
                ms2n a
              WHERE
                (a.Tgl_PS BETWEEN "2016-10-26" AND "2016-11-25") AND
                a.Status = "PS"
            ';
          break;
        }
      break;
      case "NAL" :
        switch ($status) {
          case "PS" :
            $SQL = '
              SELECT
                Tgl_PS,
                count(*) as jumlah
              FROM
                ms2n
              WHERE
                (Tgl_PS between "'.date('Y-m-d',$lastWeek).'" AND "'.$tgl_ps.'") AND
                Status = "PS"
              GROUP BY
                Tgl_PS
            ';
          break;
          case "Transaksi" :
            $SQL = '
              SELECT
                Tgl_Reg,
                SUM(CASE WHEN Status = "PI"
                THEN 1
                ELSE 0
                END ) AS PI,
                SUM(CASE WHEN Status = "FO"
                THEN 1
                ELSE 0
                END ) AS FO,
                SUM(CASE WHEN Status = "VA"
                THEN 1
                ELSE 0
                END ) AS VA

              FROM
                ms2n
              WHERE
                (Tgl_Reg between "'.date('Y-m-d',$lastWeek).'" AND "'.$tgl_ps.'")
              GROUP BY
                Tgl_Reg
            ';
          break;
        }
      break;
    }
    $query = DB::select($SQL);
    return $query;
  }

  public static function GetQueryList($jenis,$status,$area){
    $tgl_ps   = date('Y-m-d');
    $jenis = strtoupper($jenis);

    if ($area=="ALL"){
      $Where = '';
    } else {
      $Where = 'AND cc.area = "'.$area.'"';
    }

    switch ($jenis){
      case "MBSP" :
        $SQL = '
          select
            *,
            gg.uraian,
            dd.jenisPsb,
            dd.orderStatus,
            dd.Kcontact,
            ee.id as id_dt,
            PGMAX as orderId
          from prospek a
            left join mdf b on a.CMDF = b.mdf
            left join ms2n c on a.ND_POTS = c.ND
            left join (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON a.NCLI_POTS = pg.orderNcli
            LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
            left join dispatch_teknisi ee ON dd.orderId = ee.Ndem
            left join psb_laporan ff ON ee.id = ff.id_tbl_mj
            left join regu gg ON ee.id_regu = gg.id_regu
        ';
        switch ($status){
          case "F2F" :
            $SQL .= '
              WHERE
              b.area = "'.$area.'" AND
              dd.jenisPsb = "Migrate" AND dd.orderStatus="Completed (PS)" AND c.Status is NULL
            ';
          break;
          case "C2F" :
            $SQL .= '
              WHERE
              b.area = "'.$area.'" AND
              dd.orderId is NULL AND
              c.Status is NULL
            ';
          break;

        }
      break;
      case "NAL" :
        $status = strtoupper($status);
        switch ($status) {
          case "PS" :
          $SQL = '
          SELECT
          *,
          gg.uraian,
          dd.jenisPsb,
          dd.orderStatus,
          dd.Kcontact,
          ee.id as id_dt,
          PGMAX as orderId
          FROM
            ms2n bb
          left join mdf cc on bb.mdf = cc.mdf
          left join (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
          LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
          left join dispatch_teknisi ee ON dd.orderId = ee.Ndem
          left join psb_laporan ff ON ee.id = ff.id_tbl_mj
          left join regu gg ON ee.id_regu = gg.id_regu
          left join prospek hh ON bb.Ncli = hh.NCLI
            where bb.Status="PS" AND bb.Tgl_PS = "'.$tgl_ps.'"
            '.$Where.'
            GROUP BY bb.Ncli
          ';
          break;
          case "RWOS" :
          $SQL = '
          SELECT
          *,
          gg.uraian,
          dd.jenisPsb,
          dd.orderStatus,
          dd.Kcontact,
            ee.id as id_dt,
          PGMAX as orderId
          FROM
            ms2n bb
          left join mdf cc on bb.mdf = cc.mdf
          left join (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
          LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
          left join dispatch_teknisi ee ON dd.orderId = ee.Ndem
          left join psb_laporan ff ON ee.id = ff.id_tbl_mj
          left join regu gg ON ee.id_regu = gg.id_regu
          left join prospek hh ON bb.Ncli = hh.NCLI
            where dd.orderStatus="Process ISISKA (RWOS)" AND bb.Status<>"PS"
            '.$Where.'
            GROUP BY bb.Ncli
          ';
          break;
          case "COMPLETED" :
            $SQL = '
            SELECT
            *,
            gg.uraian,
            dd.jenisPsb,
            dd.orderStatus,
            dd.Kcontact,
              ee.id as id_dt,
            PGMAX as orderId
            FROM
              ms2n bb
            left join mdf cc on bb.mdf = cc.mdf
            left join (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
            LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
            left join dispatch_teknisi ee ON dd.orderId = ee.Ndem
            left join psb_laporan ff ON ee.id = ff.id_tbl_mj
            left join regu gg ON ee.id_regu = gg.id_regu
            left join prospek hh ON bb.Ncli = hh.NCLI
              where dd.orderStatus="Process OSS (Activation Completed)" AND bb.Status<>"PS"
              '.$Where.'
              GROUP BY bb.Ncli
            ';
          break;
          case "FALLOUTACTIVATION" :
            $SQL = '
            SELECT
            *,
            gg.uraian,
            dd.jenisPsb,
            dd.orderStatus,
            dd.Kcontact,
              ee.id as id_dt,
            PGMAX as orderId
            FROM
              ms2n bb
            left join mdf cc on bb.mdf = cc.mdf
            left join (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
            LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
            left join dispatch_teknisi ee ON dd.orderId = ee.Ndem
            left join psb_laporan ff ON ee.id = ff.id_tbl_mj
            left join regu gg ON ee.id_regu = gg.id_regu
            left join prospek hh ON bb.Ncli = hh.NCLI
              where dd.orderStatus="Fallout (Activation)" AND bb.Status<>"PS"
              '.$Where.'
              GROUP BY bb.Ncli
            ';
          break;
          case "FALLOUTDATA" :
            $SQL = '
            SELECT
            *,
            gg.uraian,
            dd.jenisPsb,
            dd.orderStatus,
            dd.Kcontact,
              ee.id as id_dt,
            PGMAX as orderId
            FROM
              ms2n bb
            left join mdf cc on bb.mdf = cc.mdf
            left join (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
            LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
            left join dispatch_teknisi ee ON dd.orderId = ee.Ndem
            left join psb_laporan ff ON ee.id = ff.id_tbl_mj
            left join regu gg ON ee.id_regu = gg.id_regu
            left join prospek hh ON bb.Ncli = hh.NCLI
              where dd.orderStatus="Fallout (Data)" AND bb.Status<>"PS"
              '.$Where.'
              GROUP BY bb.Ncli
            ';
          break;
          case "OGP" :
            $SQL = '
              SELECT
              *,
              gg.uraian,
              dd.jenisPsb,
              dd.orderStatus,
              dd.Kcontact,
                ee.id as id_dt,
              PGMAX as orderId
              FROM
                ms2n bb
              left join mdf cc on bb.mdf = cc.mdf
              left join (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
              LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
              left join dispatch_teknisi ee ON dd.orderId = ee.Ndem
              left join psb_laporan ff ON ee.id = ff.id_tbl_mj
              left join regu gg ON ee.id_regu = gg.id_regu
              left join prospek hh ON bb.Ncli = hh.NCLI
              where ff.status_laporan = 5 AND bb.Status <> "PS"
              '.$Where.'
              GROUP BY bb.Ncli
            ';
          break;
        }
      break;
    }
    $query = DB::select($SQL);
    return $query;
  }

  public static function GetQuery($id,$search){
    $tgl_ps   = date('Y-m-d');
    $tomorrow = mktime(date("H"), date("i"), date("s"), date("m"), date("d")+1, date("Y"));
    $id       = strtoupper($id);
    switch ($id){
      case "NAL" :
        $SQL = '
        SELECT
						c.area,
						count(*) as jumlah,
						sum(case when (b.orderStatus = "Fallout (Activation)" ) then 1 else 0 end) as fallout_activation,
						sum(case when (b.orderStatus = "Fallout (Data)" ) then 1 else 0 end) as fallout_data,
						sum(case when (e.status_laporan = "5" AND b.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") ) then 1 else 0 end) as OGP,
						(select count(*) from ms2n bb left join mdf cc on bb.mdf = cc.mdf left join Data_Pelanggan_Starclick dd ON bb.Ncli = dd.orderNcli WHERE cc.area = c.area AND bb.Status<>"PS" AND dd.orderStatus = "Process ISISKA (RWOS)") as RWOS,
						(select count(*) from ms2n bb left join mdf cc on bb.mdf = cc.mdf left join Data_Pelanggan_Starclick dd ON bb.Ncli = dd.orderNcli WHERE cc.area = c.area AND bb.Status<>"PS" AND dd.orderStatus = "Process OSS (Activation Completed)") as Completed,
						(select count(*) from ms2n bb left join mdf cc on bb.mdf = cc.mdf where cc.area=c.area AND bb.Status="PS" AND bb.Tgl_PS = "'.$tgl_ps.'") as PS
					FROM `dispatch_teknisi` a
					LEFT JOIN Data_Pelanggan_Starclick b ON a.Ndem = b.orderId
					LEFT JOIN mdf c ON b.sto = c.mdf
					LEFT JOIN ms2n d ON b.orderNcli = d.Ncli
					LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
					WHERE
						date(a.updated_at) = "'.$tgl_ps.'" AND
						d.Status <> "PS"
					GROUP BY c.area
          ORDER BY PS desc
        ';
      break;
      case "MBSP" :
        $SQL = '
          select
            b.area,
            count(*) as jumlah,
            (SUM(CASE WHEN c.Status = "PS" THEN 1 ELSE 0 END)) as PS_REGULER,
            (SUM(CASE WHEN c.Status = "PS" AND c.Deskripsi IN ("Upgrade MBSP dari 2P ke Indihome","Upgrade MBSP dari 1P ke Indihome") THEN 1 ELSE 0 END)) as PS_MBSP,
            (SUM(CASE WHEN d.jenisPsb = "Migrate" AND d.orderStatus="Completed (PS)" AND c.Status is NULL THEN 1 ELSE 0 END)) as F2F,
            (SUM(CASE WHEN d.orderId is NULL AND c.Status is NULL THEN 1 ELSE 0 END)) as C2F
          from prospek a
            left join mdf b on a.CMDF = b.mdf
            left join ms2n c on a.ND_POTS = c.ND
            left join Data_Pelanggan_Starclick d ON a.NCLI_POTS = d.orderNcli
          group by b.area
        ';
      break;
    }
    $query = DB::select($SQL);
    return $query;
  }

  public static function GetQueryReport($id,$search){
    $tgl_ps   = date('Y-m-d');
    $tomorrow = mktime(date("H"), date("i"), date("s"), date("m"), date("d")+1, date("Y"));
    $id       = strtoupper($id);
    switch ($id){
      case "NAL" :
        $SQL = '
        SELECT
    					c.area,
    					count(*) as JUMLAH,
    					sum(case when (b.orderStatus = "Fallout (Activation)" ) then 1 else 0 end) as fallout_activation,
    					sum(case when (b.orderStatus = "Fallout (Data)" ) then 1 else 0 end) as fallout_data,
    					sum(case when (b.orderStatus = "Process ISISKA (RWOS)"  ) then 1 else 0 end) as RWOS,
    					sum(case when (b.orderStatus = "Process OSS (Activation Completed)" ) then 1 else 0 end) as Completed,
    					sum(case when (e.status_laporan = "5" ) then 1 else 0 end) as OGP,
    					sum(case when (e.status_laporan is NULL) then 1 else 0 end) as BELUM_SURVEY,
    					sum(case when (e.status_laporan = "2") then 1 else 0 end) as KENDALA_TEKNIS,
    					sum(case when (e.status_laporan = "3") then 1 else 0 end) as KENDALA_PELANGGAN,
              sum(case when (e.status_laporan = "1") then 1 else 0 end) as UP,
              sum(case when (e.status_laporan = "7") then 1 else 0 end) as RESCHEDULE,
              sum(case when (e.status_laporan = "8") then 1 else 0 end) as SURVEY_OK,
              sum(case when (e.status_laporan = "9") then 1 else 0 end) as FOLLOW_UP_SALES,
              (select count(*) from ms2n bb left join Data_Pelanggan_Starclick cc on bb.Ncli = cc.orderNcli left join dispatch_teknisi dd on cc.orderId = dd.Ndem left join mdf ee on bb.mdf = ee.mdf where ee.area = c.area AND bb.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND cc.orderStatus = "Process OSS (Provision Issued)" AND dd.id_regu is NULL) as UNDISPATCH,
              sum(case when (e.status_laporan = "4") then 1 else 0 end) as HR
    				FROM `dispatch_teknisi` a
    				LEFT JOIN Data_Pelanggan_Starclick b ON a.Ndem = b.orderId
    				LEFT JOIN mdf c ON b.sto = c.mdf
    				LEFT JOIN ms2n d ON b.orderId = SUBSTR(d.kode_sc,3,10)
    				LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
            WHERE
  					date(a.updated_at) = "'.$tgl_ps.'" AND
  					c.area is not null AND
  					d.Status <> ""
    				GROUP BY c.area
        ';
      break;
    }
    $query = DB::select($SQL);
    return $query;
  }

  public function reportPotensiDua()
  {
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
      $enddate = date('Y-m-d');
      $data = DB::SELECT('
      SELECT
        m.area_migrasi,
        SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is not NULL THEN 1 ELSE 0 END) as jumlah_dispatch,
        SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is NULL THEN 1 ELSE 0 END) jumlah_undispatch,
        SUM(CASE WHEN dps.orderStatus LIKE "%fallout%" THEN 1 ELSE 0 END) as fo,
        SUM(CASE WHEN dps.orderStatusId in (49,50,51) AND pl.status_laporan = 3 AND dt.id is not NULL THEN 1 ELSE 0 END) as fo_kendala,
        SUM(CASE WHEN dps.orderStatusId in (49,50,51) AND pl.status_laporan = 1 AND dt.id is not NULL THEN 1 ELSE 0 END) as fo_up,
        SUM(CASE WHEN dps.orderStatusId = 52 AND dt.id is not NULL THEN 1 ELSE 0 END) as act_com,
        SUM(CASE WHEN dps.orderStatusId = 7 AND dt.id is not NULL AND dps.orderDatePs LIKE "%'.$enddate.'%" THEN 1 ELSE 0 END) as ps,
        count(*) as jumlah
      FROM
        Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'") AND
        (dps.orderStatusId in (16,7,52,49,50,51) OR dps.orderStatus LIKE "%fallout%" ) AND m.mdf<>""
        GROUP BY m.area_migrasi
        ORDER BY jumlah DESC
      ');
      return $data;
  }

  public static function provisioningCari($tgl,$where){
    $query = DB::SELECT('
      SELECT t1.* from
        (SELECT
         pls.laporan_status_id,
         pls.laporan_status,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
         dispatch_teknisi dt
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
        WHERE
          dt.dispatch_by is NULL AND
          dps.orderId <> "" AND
          dt.tgl like "%'.$tgl.'%"
        GROUP BY pls.laporan_status
        ORDER BY pls.laporan_status_id)
      as t1
      where t1.laporan_status_id in '.$where.'

    ');
    // dd($query);
    return $query;
  }

  public static function ActionGuarante($date){
    $query = DB::SELECT('
      select
        b.action,
        count(*) as jumlah
      from
        psb_laporan a
        left join psb_laporan_action b ON a.action = b.laporan_action_id
        left join dispatch_teknisi c ON a.id_tbl_mj = c.id
        where c.tgl LIKE "%'.$date.'%" AND b.action is not NULL and c.dispatch_by = 4 group by a.action order by jumlah DESC');
    return $query;
  }

  public static function getGuaranteListbyAction($date,$status){
    $status = urldecode($status);
    if ($status == "ALL"){
      $getWhere = "";
    } else {
      $getWhere = 'AND b.action LIKE  "%'.$status.'%"';
    }
    $query = DB::SELECT('
    select
      *
    from
      psb_laporan a
      left join psb_laporan_action b ON a.action = b.laporan_action_id
      left join psb_laporan_penyebab plp on a.penyebabId=plp.idPenyebab
      left join dispatch_teknisi c ON a.id_tbl_mj = c.id
      left join roc d ON c.Ndem = d.no_tiket
      left join regu e ON c.id_regu = e.id_regu
      left join psb_laporan_status f ON a.status_laporan = f.laporan_status_id
      left join group_telegram gt ON e.mainsector = gt.chat_id
      where b.action is not NULL AND c.dispatch_by = 4 AND c.tgl LIKE "%'.$date.'%" '.$getWhere.'
 order by d.umur DESC');
    return $query;
  }

   public static function provisioningListFilter($tgl,$jenis,$status,$so,$where){
    $where_area = "";
    if ($so<>"ALL"){
      $where_area = ' AND m.area_migrasi = "'.$so.'"';
    }
    if ($status == "NO UPDATE"){
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    } else if ($status == "ALL"){
      $where_status = ' AND pls.laporan_status_id in '.$where;
    } else {
      $where_status = '  AND pls.laporan_status = "'.$status.'"';
    }
    $query = DB::SELECT('
      SELECT
          *,
          dt.id as id_dt,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          SUM( i.num ) AS dc_preconn,
          SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
          SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
          SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
          SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
          SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
          0 as conduit,
          dtjl.mode,
          SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
          tt.ketTiang,
          dps.sto,
          pls.laporan_status_id
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
         LEFT JOIN item i ON plm.id_item = i.id_item
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by is NULL AND
         dps.orderId <> "" AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
    ');

    return $query;
  }

  public static function provisioningListMitraSebelumRfc($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        DATE_FORMAT(dt.tgl,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
        SUM(IF(plm.id_item = "preconnectorized 100 M",i.num * plm.qty,0) + IF(plm.id_item = "preconnectorized 75 M",i.num * plm.qty,0) + IF(plm.id_item = "preconnectorized 50 M",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-150-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "DC-ROLL",plm.qty,0)) as dc_preconn,
        -- SUM( i.num * plm.qty) AS dc_preconn,
        SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6", "UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "preconnectorized 100 M" then plm.qty else 0 end) as precon100,
        SUM(case when plm.id_item = "preconnectorized 75 M" then plm.qty else 0 end) as precon75,
        SUM(case when plm.id_item = "preconnectorized 50 M" then plm.qty else 0 end) as precon50,
        SUM(case when plm.id_item = "Preconnectorized-1C-150-NonAcc" then plm.qty else 0 end) as preconnonacc,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(CASE when plm.id_item = "Breket" THEN plm.qty else 0 end) as breket,
        SUM(CASE when plm.id_item = "Clamp-s" THEN plm.qty else 0 end) as clamps,
        SUM(CASE when plm.id_item = "Pulstrap" THEN plm.qty else 0 end) as pulstrap,
        SUM(CASE when plm.id_item = "Rj-45" THEN plm.qty else 0 end) as rj45,
        SUM(CASE when plm.id_item = "RS-IN-SC" THEN plm.qty else 0 end) as roset,
        SUM(CASE when plm.id_item = "SOC-Ilsintentech" THEN plm.qty else 0 end) as SOCIlsintentech,
        SUM(CASE when plm.id_item = "SOC-Sumitomo" THEN plm.qty else 0 end) as SOCSumitomo,
        SUM(CASE when plm.id_item = " AD-SC" THEN plm.qty else 0 end) as adsc,
        0 as conduit,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
      FROM
       regu r
       LEFT JOIN  dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN mdf m ON dps.sto = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       dt.dispatch_by is NULL AND
       dps.orderId <> "" AND
       dt.tgl like "%'.$tgl.'%" AND
       pl.status_laporan = 1
       '.$getWhere.'
       GROUP BY dt.id
    ');

    return $query;
  }

  public static function migrasiListMitraSebelumRfc($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        dt.updated_at as tanggal_dispatch,
        dm.STO as sto,
        dm.ND as orderId,
        dm.NCLI as orderNcli,
        dm.ND as ndemPots,
        dm.ND_REFERENCE as ndemSpeedy,
        dm.NAMA as orderName,
        "MIGRASI AS IS" as jenisPsb,
        "~" as orderStatus,
        DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        0 as orderDatePs,
        SUM(IF(plm.id_item = "preconnectorized 100 M",i.num * plm.qty,0) + IF(plm.id_item = "preconnectorized 75 M",i.num * plm.qty,0) + IF(plm.id_item = "preconnectorized 50 M",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-150-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "DC-ROLL",plm.qty,0)) as dc_preconn,
        -- SUM( i.num * plm.qty) AS dc_preconn,
        SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6", "UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "preconnectorized 100 M" then plm.qty else 0 end) as precon100,
        SUM(case when plm.id_item = "preconnectorized 75 M" then plm.qty else 0 end) as precon75,
        SUM(case when plm.id_item = "preconnectorized 50 M" then plm.qty else 0 end) as precon50,
        SUM(case when plm.id_item = "Preconnectorized-1C-150-NonAcc" then plm.qty else 0 end) as preconnonacc,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(CASE when plm.id_item = "Breket" THEN plm.qty else 0 end) as breket,
        SUM(CASE when plm.id_item = "Clamp-s" THEN plm.qty else 0 end) as clamps,
        SUM(CASE when plm.id_item = "Pulstrap" THEN plm.qty else 0 end) as pulstrap,
        SUM(CASE when plm.id_item = "Rj-45" THEN plm.qty else 0 end) as rj45,
        SUM(CASE when plm.id_item = "RS-IN-SC" THEN plm.qty else 0 end) as roset,
        SUM(CASE when plm.id_item = "SOC-Ilsintentech" THEN plm.qty else 0 end) as SOCIlsintentech,
        SUM(CASE when plm.id_item = "SOC-Sumitomo" THEN plm.qty else 0 end) as SOCSumitomo,
        SUM(CASE when plm.id_item = " AD-SC" THEN plm.qty else 0 end) as adsc,
        0 as conduit,
        dm.LQUARTIER as orderAddr,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN dossier_master dm ON dt.Ndem = dm.ND
       LEFT JOIN mdf m ON dm.STO = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       dm.ND <> "" AND
       dt.tgl like "%'.$tgl.'%" AND
       pl.status_laporan = "1"
       '.$getWhere.'
       GROUP BY dt.id
    ');
    return $query;
  }

  public static function scbe($tgl, $ket=null){
    $whereTgl = ' (dt.tgl like "'.$tgl.'%" OR pl.modified_at like "'.$tgl.'%")';
      if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (dt.tgl between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };
    $get_datel = DB::table('maintenance_datel')->where('witel',session('witel'))->get();
    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,';
    foreach ($get_datel as $datel){
       $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as WO_'.$datel->datel.',';
    }
    $SQL .= '
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
       LEFT JOIN data_starclick_ao dps ON dt.Ndem = dps.orderId
       LEFT JOIN maintenance_datel m ON (my.sto = m.sto OR dps.sto = m.sto)
      WHERE
        m.witel = "'.session('witel').'" AND
        (dt.dispatch_by="5" OR dt.dispatch_by IS NULL) AND
        (my.myir <> "" OR dps.orderId <> "")  AND
        pls.aktif_dash=1 AND
        '.$whereTgl.'

      GROUP BY pls.laporan_status
      ORDER BY pls.udash asc, pls.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);

    return $query;
  }

  public static function scbeWitel($tgl, $ket=null, $witel){
    $whereTgl = '(pl.modified_at like "'.$tgl.'%")';
    // $whereTgl = '(dt.tgl like "'.$tgl.'%")';
    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (dt.tgl between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };
    $get_datel = DB::table('maintenance_datel')->where('witel',$witel)->groupBy('datel')->get();
    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.statusGrup,';
    foreach ($get_datel as $datel){
      $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as '.$datel->datel.',';
    }
    $SQL .= '
      pls.laporan_status,
      count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON (dt.Ndem=my.myir OR dt.Ndem=my.sc)
       LEFT JOIN data_starclick_ao dps ON dt.Ndem = dps.orderId
       LEFT JOIN maintenance_datel m ON (my.sto = m.sto OR dps.sto = m.sto)
      WHERE
        (dt.dispatch_by="5" OR dt.dispatch_by IS NULL) AND
        (my.myir <> "" OR dps.orderId <> "")  AND
        pls.aktif_dash=1 AND
        pls.grup <> "SISA" AND
        m.witel="'.session('witel').'" AND
        '.$whereTgl.'

      GROUP BY pls.laporan_status
      ORDER BY pls.udash asc, pls.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);

    return $query;
  }

  public static function scbeWitel_no_update($tgl, $ket=null, $witel){
    $get_datel = DB::table('maintenance_datel')->where('witel',$witel)->get();
    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.statusGrup,';
    foreach ($get_datel as $datel){
      $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as '.$datel->datel.',';
    }
    $SQL .= '
      pls.laporan_status,
      count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON (dt.Ndem=my.myir OR dt.Ndem=my.sc)
       LEFT JOIN maintenance_datel m ON (my.sto = m.sto)
      WHERE
        m.witel="'.session('witel').'" AND
        (dt.dispatch_by="5" OR dt.dispatch_by IS NULL) AND
        (my.myir <> "")  AND
        (pls.grup = "SISA" OR pl.id IS NULL)

      GROUP BY pls.laporan_status
      ORDER BY pls.udash asc, pls.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);

    return $query;
  }

  public static function scbeNew($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
       LEFT JOIN mdf m ON my.sto = m.mdf
      WHERE
        dt.dispatch_by="5" AND
        my.myir <> "" AND
        dt.tgl like "%'.$tgl.'%"

      GROUP BY pls.laporan_status
      ORDER BY pls.statusGrup asc, pls.urutan asc, pls.laporan_status asc
    ');
    return $query;
  }

  public static function scbeUp($tgl, $ket=null){
    $whereTgl = ' (dt.tgl like "%'.$tgl.'%" OR pl.modified_at like "'.$tgl.'%")';
    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (dt.tgl between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };

    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem=dps.orderId
       LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        dt.dispatch_by is null AND
        dps.orderId <> "" AND
        pls.laporan_status_id ="1" AND
        '.$whereTgl.'

      GROUP BY pls.laporan_status
      ORDER BY pls.statusGrup asc, pls.urutan asc, pls.laporan_status asc
    ');
    return $query;
  }

  public static function scbeList($tgl,$jenis,$status,$so,$ket,$ketTw=null){
    $whereTgl = ' (pl.modified_at like "'.$tgl.'%")';
      if ($ketTw<>null){
        $ketTgl = explode('_',$ketTw);

        $whereTgl = ' (dt.tgl between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };

    $where_area = '';
    if ($so<>"ALL"){
      $where_area = ' m.datel = "'.$so.'" AND ';
    }

    if ($status == "NO UPDATE"){
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    } else if ($status == "ALL"){
      $where_status = ' ';
    } else {
      $where_status = '  AND pls.laporan_status = "'.$status.'"';
    }

    $where_ket = '';

    if ($ket=="KENDALA"){
      $where_ket = ' AND pls.stts_dash in ("hs","daman","amo","aso","PT1")';
    };

    $query = DB::SELECT('
      SELECT
          *,
          dt.id as id_dt,
          dt.created_at as tanggal_dispatch,
          pl.modified_at as modified_at,
          my.orderDate as orderDatePs,
          my.orderDate as tanggal_wo,
          dtjl.mode,
          tt.ketTiang,
          my.sto as stoMy,
          pls.laporan_status_id,
          my.customer,
          my.myir,
          my.sto,
          my.created_by as kode_sales,
          dps.*,
          pl.redaman_odp,
          pl.status_kendala,
          pl.tgl_status_kendala,
          gt.title as sektorNama,
          pls.stts_dash,
          psl.nama_sales,
          pl.dropcore_label_code,
          m.datel as area_migrasi
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN data_starclick_ao dps ON dt.Ndem = dps.orderId
         LEFT JOIN psb_myir_wo my ON  (dt.Ndem=my.myir OR dt.Ndem=my.sc)
         LEFT JOIN maintenance_datel m ON (my.sto = m.sto OR dps.sto = m.sto)
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
         LEFT JOIN psb_sales psl ON psl.id = my.sales_id
       WHERE
       '.$where_area.'
         (dt.dispatch_by="5" OR dt.dispatch_by IS NULL) AND
         (my.myir <> "" OR dps.orderId<>"")  AND
         pls.aktif_dash=1 AND
         '.$whereTgl.'
         '.$where_ket.'
         '.$where_status.'
         Order by gt.urut asc
    ');

    return $query;
  }

  public static function get_potensi_ps($tgl, $ket=null,$witel){
    $startdate = date('Y-m-d', mktime(0, 0, 0, date("m",strtotime($tgl)), date("d",strtotime($tgl))-5, date("Y",strtotime($tgl))));
    $enddate = date('Y-m-d',strtotime($tgl));

    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $startdate = $ketTgl[0];
        $enddate   = $ketTgl[1];
    };
    $get_datel = DB::table('maintenance_datel')->where('witel',$witel)->groupBy('datel')->get();
    $SQL = '
        SELECT
         dps.orderStatusId,
         dps.orderStatus,';
         foreach ($get_datel as $datel){
           $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as '.$datel->datel.',';
         }
         $SQL .='
         count(*) as jumlah
        FROM
            dispatch_teknisi dt
            LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
            LEFT JOIN maintenance_datel m ON dps.sto = m.sto
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE
          dps.witel = "'.session('witel').'" AND
          dt.dispatch_by is NULL AND
          dps.orderId <> "" AND
          dps.jenisPsb LIKE "%AO%" AND
          (dps.orderStatus in (
            "OSS PONR",
            "WFM ACTIVATION COMPLETE",
            "OSS PROVISIONING COMPLETE",
            "TIBS FULFILL BILLING START",
            "TIBS FULFILL BILLING COMPLETED",
            "EAI COMPLETED",
            "Process OSS (Provision Completed)","Process ISISKA (RWOS)","Fallout (Data)","Fallout (Activation)","Process OSS (Activation Completed)") OR
          (dps.orderStatus="Fallout (WFM)" AND pls.laporan_status_id in ("1","37","38"))) AND
          (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ';
    $query = DB::select($SQL);

    return $query;
  }

  public static function reportPotensiPsStar($tgl, $ket=null){
    $startdate = date('Y-m-d', mktime(0, 0, 0, date("m",strtotime($tgl)), date("d",strtotime($tgl))-5, date("Y",strtotime($tgl))));
    $enddate = date('Y-m-d',strtotime($tgl));

    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $startdate = $ketTgl[0];
        $enddate   = $ketTgl[1];
    };

    $query = DB::select('
        SELECT
         dps.orderStatusId,
         dps.orderStatus,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" || m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
            dispatch_teknisi dt
            LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
            LEFT JOIN mdf m ON dps.sto = m.mdf
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE
          dps.witel = "'.session('witel').'" AND
          dt.dispatch_by is NULL AND
          dps.orderId <> "" AND
          dps.jenisPsb LIKE "%AO%" AND
          (dps.orderStatus in (
            "OSS PONR",
            "WFM ACTIVATION COMPLETE",
            "OSS PROVISIONING COMPLETE",
            "TIBS FULFILL BILLING START",
            "TIBS FULFILL BILLING COMPLETED",
            "EAI COMPLETED",
            "Process OSS (Provision Completed)","Process ISISKA (RWOS)","Fallout (Data)","Fallout (Activation)","Process OSS (Activation Completed)") OR
          (dps.orderStatus="Fallout (WFM)" AND pls.laporan_status_id in ("1","37","38"))) AND
          (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ');

    return $query;
  }

  public static function get_potensi_ps_up($tgl, $ket=null,$witel){
    $whereTgl = ' DATE(dps.orderDatePs) LIKE "%'.$tgl.'%" ';
    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (DATE(dps.orderDatePs) between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };
    $get_datel = DB::table('maintenance_datel')->where('witel',$witel)->groupBy('datel')->get();
    $SQL = '
        SELECT
         dps.orderStatusId,
         dps.orderStatus,';
    foreach($get_datel as $datel){
         $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as '.$datel->datel.',';
    }
    $SQL .= '
         count(*) as jumlah
        FROM
           dispatch_teknisi dt
            LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
            LEFT JOIN maintenance_datel m ON dps.sto = m.sto
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE
          dps.witel = "'.session('witel').'" AND
          dt.dispatch_by is NULL AND
          dps.orderId <> "" AND
          dps.jenisPsb LIKE "%AO%" AND
          dps.orderStatusId in ("1500","7")  AND
          '.$whereTgl.'
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ';
    $query = DB::select($SQL);
          // dps.orderDate BETWEEN "'.$startdate.'" AND "'.$enddate.'"
    return $query;
  }

  public static function reportPotensiPsStarUp($tgl, $ket=null){
    $whereTgl = ' DATE(dps.orderDatePs) LIKE "%'.$tgl.'%" ';
    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (DATE(dps.orderDatePs) between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };

    $query = DB::select('
        SELECT
         dps.orderStatusId,
         dps.orderStatus,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" || m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
           dispatch_teknisi dt
            LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
            LEFT JOIN mdf m ON dps.sto = m.mdf
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE
          dps.witel = "'.session('witel').'" AND
          dt.dispatch_by is NULL AND
          dps.orderId <> "" AND
          dps.jenisPsb LIKE "%AO%" AND
          dps.orderStatusId in ("1500","7")  AND
          '.$whereTgl.'
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ');
          // dps.orderDate BETWEEN "'.$startdate.'" AND "'.$enddate.'"
    return $query;
  }

  public static function reportPotensiPsStarFalloutWfm($tgl){
    $query = DB::select('
        SELECT
         dps.orderStatusId,
         dps.orderStatus,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" || m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
           dispatch_teknisi dt
            LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
            LEFT JOIN mdf m ON dps.sto = m.mdf
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE
          dt.dispatch_by is NULL AND
          dps.orderId <> "" AND
          dps.jenisPsb LIKE "%AO%" AND
          dps.orderStatus in ("Fallout (WFM)")  AND
          DATE(dps.orderDatePs) LIKE "%'.$tgl.'%"
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ');
          // dps.orderDate BETWEEN "'.$startdate.'" AND "'.$enddate.'"
    return $query;
  }

  public static function reportPotensiPsScbe($tgl){
      $query = DB::SELECT('
        SELECT
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" || m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
         dispatch_teknisi dt
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
         LEFT JOIN mdf m ON my.sto = m.mdf
        WHERE
          dt.dispatch_by="5" AND
          my.myir <> "" AND
          pls.laporan_status_id in ("31","5","28","29","4") AND
          dt.tgl like "%'.$tgl.'%"
    ');
    return $query;
  }

  public static function potensiPs($tgl,$jenis,$status,$so,$ketTw=null){
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m",strtotime($tgl)), date("d",strtotime($tgl))-5, date("Y",strtotime($tgl))));
      $enddate = date('Y-m-d',strtotime($tgl));

      if ($ketTw<>null){
          $ketTgl = explode('_',$ketTw);

          $startdate = $ketTgl[0];
          $enddate   = $ketTgl[1];
      };

      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND m.area_migrasi = "'.$so.'"';
      };

      if ($so=="KDG"){
          $where_area = ' AND m.area_migrasi in ("KDG","TJL")';
      };

      if ($status == "ALL"){
        $where_status = ' AND (dps.orderStatus in ("Process OSS (Provision Completed)","Process ISISKA (RWOS)","Fallout (Data)","Fallout (Activation)","Process OSS (Activation Completed)") OR (dps.orderStatus="Fallout (WFM)" AND pls.laporan_status_id in ("1","37","38")))';
      }
      else if ($status=="Fallout (WFM)"){
          $where_status = ' AND (dps.orderStatus="Fallout (WFM)" AND pls.laporan_status_id in ("1","37","38"))';
      }
      else {
        $where_status = '  AND dps.orderStatus = "'.$status.'"';
      };

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          dps.orderDate as tanggal_order,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          pl.modified_at as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          pl.redaman_iboster,
          dps.*
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
         LEFT JOIN item i ON plm.id_item = i.id_item
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by is NULL AND
         dps.orderId <> "" AND
         dps.jenisPsb LIKE "%AO%" AND
         (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');
         // dt.tgl like "%'.$tgl.'%"

      return $query;
  }

  public static function potensiPsUp($tgl,$jenis,$status,$so,$ketTw=null){
      $whereTgl = ' DATE(dps.orderDatePs) LIKE "%'.$tgl.'%" ';
      if ($ketTw<>null){
          $ketTgl = explode('_',$ketTw);

          $whereTgl = ' (DATE(dps.orderDatePs) between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
      };

      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND m.datel = "'.$so.'"';
      };

      if ($so=="TANJUNG"){
          $where_area = ' AND m.datel in ("KANDANGAN","TANJUNG")';
      };

      $where_status = '';

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          dps.orderDate as tanggal_order,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          pl.modified_at as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          pl.redaman_iboster,
          dps.*,
          m.datel as area_migrasi
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN maintenance_datel m ON dps.sto = m.sto
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by is NULL AND
         dps.orderId <> "" AND
         dps.jenisPsb LIKE "%AO%" AND
         dps.orderStatusId in ("1500","52","7") AND
         '.$whereTgl.'
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');

      return $query;
  }

  public static function potensiPsScbe($tgl,$jenis,$status,$so){
    $where_area = "";
    if ($so<>"ALL"){
      $where_area = ' AND m.area_migrasi = "'.$so.'"';
    };

    if ($so=="KDG"){
        $where_area = ' AND m.area_migrasi in ("KDG","TJL")';
    };

    $where_status = ' AND pls.laporan_status_id in ("31","5","28","29","4")';
    $query = DB::SELECT('
      SELECT
          *,
          dt.id as id_dt,
          dt.updated_at as tanggal_dispatch,
          pl.modified_at as modified_at,
          DATE_FORMAT(my.orderDate,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          my.sto,
          pls.laporan_status_id,
          my.customer,
          my.myir,
          my.sto,
          pls.*
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_myir_wo my ON dt.Ndem = my.myir
         LEFT JOIN mdf m ON my.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by="5" AND
         my.myir <> "" AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
    ');

    return $query;
  }

  public static function reportPotensiPsStarMo($tgl){
    $query = DB::select('
        SELECT
         dps.orderStatusId,
         dps.orderStatus,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
         Data_Pelanggan_Starclick dps
         LEFT JOIN mdf m ON dps.sto = m.mdf
        WHERE
          dps.orderId <> "" AND
          dps.jenisPsb in ("MO|VOICE","MO|IPTV","MO|INTERNET + VOICE","MO|INTERNET + IPTV","MO|INTERNET","MO|BUNDLING","MO (INTERNET)","MO
MIGRATE|MO|INTERNET + IPTV","MIGRATE|MO|BUNDLING","MIGRATE|AS + MO|INTERNET + VOICE","MIGRATE|AS + MO|BUNDLING","MIGRATE","AS|VOICE","AS + MO|INTERNET + VOICE","AS + MO|BUNDLING") AND
          dps.orderStatus in ("Completed (PS)","Process OSS (Provision Issued)","Process OSS (Provision Completed)","Process ISISKA (RWOS)","Fallout (Data)","Fallout (Activation)","Fallout (WFM)","Process OSS (Activation Completed)") AND
          dps.orderDate like "%'.$tgl.'%"
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ');
    return $query;
  }

  public static function reportPotensiPsStarMoAsli($tgl){
    $query = DB::select('
        SELECT
         dps.orderStatusId,
         dps.orderStatus,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
         dispatch_teknisi dt
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
        WHERE
          dt.dispatch_by is NULL AND
          dps.orderId <> "" AND
          dps.jenisPsb in ("MO|VOICE","MO|IPTV","MO|INTERNET + VOICE","MO|INTERNET + IPTV","MO|INTERNET","MO|BUNDLING","MO (INTERNET)","MO
MIGRATE|MO|INTERNET + IPTV","MIGRATE|MO|BUNDLING","MIGRATE|AS + MO|INTERNET + VOICE","MIGRATE|AS + MO|BUNDLING","MIGRATE","AS|VOICE","AS + MO|INTERNET + VOICE","AS + MO|BUNDLING") AND
          dps.orderStatus in ("Completed (PS)","Process OSS (Provision Issued)","Process OSS (Provision Completed)","Process ISISKA (RWOS)","Fallout (Data)","Fallout (Activation)","Fallout (WFM)","Process OSS (Activation Completed)") AND
          dt.tgl like "%'.$tgl.'%"
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ');
    return $query;
  }

  public static function potensiPsMo($tgl,$jenis,$status,$so){
      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND m.area_migrasi = "'.$so.'"';
      };

      if ($status == "ALL"){
        $where_status = '';
      } else {
        $where_status = '  AND dps.orderStatus = "'.$status.'"';
      };

      $query = DB::SELECT('
        SELECT
          dps.*
        FROM
         Data_Pelanggan_Starclick dps
         LEFT JOIN mdf m ON dps.sto = m.mdf
       WHERE
         dps.orderId <> "" AND
         dps.jenisPsb in ("MO|VOICE","MO|IPTV","MO|INTERNET + VOICE","MO|INTERNET + IPTV","MO|INTERNET","MO|BUNDLING","MO (INTERNET)","MO
MIGRATE|MO|INTERNET + IPTV","MIGRATE|MO|BUNDLING","MIGRATE|AS + MO|INTERNET + VOICE","MIGRATE|AS + MO|BUNDLING","MIGRATE","AS|VOICE","AS + MO|INTERNET + VOICE","AS + MO|BUNDLING") AND
         dps.orderDate like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dps.orderId
      ');

      return $query;
  }

  public static function potensiPsMoAsli($tgl,$jenis,$status,$so){
      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND m.area_migrasi = "'.$so.'"';
      };

      if ($status == "ALL"){
        $where_status = '';
      } else {
        $where_status = '  AND dps.orderStatus = "'.$status.'"';
      };

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          dps.*
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by is NULL AND
         dps.orderId <> "" AND
         dps.jenisPsb in ("MO|VOICE","MO|IPTV","MO|INTERNET + VOICE","MO|INTERNET + IPTV","MO|INTERNET","MO|BUNDLING","MO (INTERNET)","MO
MIGRATE|MO|INTERNET + IPTV","MIGRATE|MO|BUNDLING","MIGRATE|AS + MO|INTERNET + VOICE","MIGRATE|AS + MO|BUNDLING","MIGRATE","AS|VOICE","AS + MO|INTERNET + VOICE","AS + MO|BUNDLING") AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');

      return $query;
  }

  public static function dashboardTransaksi($tgl){
    $query = DB::select('
        SELECT
        pls.laporan_status_id,
        pls.laporan_status,
         SUM(CASE WHEN dt.jenis_layanan = "ADDONSTB" THEN 1 ELSE 0 END) as wo_ADDONSTB,
         SUM(CASE WHEN dt.jenis_layanan = "CHANGESTB" THEN 1 ELSE 0 END) as wo_CHANGESTB,
         SUM(CASE WHEN dt.jenis_layanan = "2ndSTB" THEN 1 ELSE 0 end) as wo_2ndSTB,
         SUM(CASE WHEN dt.jenis_layanan = "3ndSTB" THEN 1 ELSE 0 end) as wo_3ndSTB,
         SUM(CASE WHEN dt.jenis_layanan = "WifiExtender" THEN 1 else 0 END) as wo_WifiExtender,
         SUM(CASE WHEN dt.jenis_layanan = "plc" THEN 1 else 0 END) as wo_plc,
         SUM(CASE WHEN dt.jenis_layanan = "PDA" THEN 1 else 0 END) as wo_pda,
         SUM(CASE WHEN dt.jenis_layanan = "CHANGESTB2NDSTB" THEN 1 else 0 END) as wo_CHANGESTB2NDSTB,
         SUM(CASE WHEN dt.jenis_layanan = "CHANGESTBWIFIEXTENDER" THEN 1 else 0 END) as wo_CHANGESTBWIFIEXTENDER,
         SUM(CASE WHEN dt.jenis_layanan = "ADDSTBPLC" THEN 1 else 0 END) as wo_ADDSTBPLC,
         SUM(CASE WHEN dt.jenis_layanan = "ADDSTBWIFIEXTENDER" THEN 1 else 0 END) as wo_ADDSTBWIFIEXTENDER,
         SUM(CASE WHEN dt.jenis_layanan = "2NDSTBPLC" THEN 1 else 0 END) as wo_2NDSTBPLC,
         SUM(CASE WHEN dt.jenis_layanan = "2NDSTBWIFIEXTENDER" THEN 1 else 0 END) as wo_2NDSTBWIFIEXTENDER,
         SUM(CASE WHEN dt.jenis_layanan = "3RDSTBPLC" THEN 1 else 0 END) as wo_3RDSTBPLC,
         SUM(CASE WHEN dt.jenis_layanan = "3RDSTBWIFIEXTENDER" THEN 1 else 0 END) as wo_3RDSTBWIFIEXTENDER,
         count(*) as jumlah
        FROM
         dispatch_teknisi dt
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
        WHERE
          dt.dispatch_by is NULL AND
          dps.orderId <> "" AND
          dt.jenis_layanan in ("ADDONSTB","CHANGESTB","2ndSTB","WifiExtender","plc","PDA","CHANGESTB2NDSTB","CHANGESTBWIFIEXTENDER","ADDSTBPLC","ADDSTBWIFIEXTENDER","2NDSTBPLC","2NDSTBWIFIEXTENDER","3RDSTBPLC","3RDSTBWIFIEXTENDER") AND
          (pl.status_laporan is NULL OR
          pls.laporan_status_id in ("1","16","49","50","6","48","45","12","5","29","28","51","46","53","64","65","4","31")) AND
          dt.tgl like "%'.$tgl.'%"
        GROUP BY pls.laporan_status
        ORDER BY pls.urutan ASC
      ');
    return $query;
  }

  public static function reportDashboardTransaksi($tgl,$jenis,$status,$so){
      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND dt.jenis_layanan = "'.$so.'"';
      };

      if ($status == "NO UPDATE"){
        $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
      } else if ($status == "ALL"){
        $where_status = ' AND (pl.status_laporan is NULL OR
          pls.laporan_status_id in ("1","16","49","50","6","48","45","12","5","29","28","51","46","64","65")) ';
      } else {
        $where_status = '  AND pls.laporan_status = "'.$status.'"';
      }

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          dt.jenis_layanan as dtJenisLayanan,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          dps.*,
          gt.title
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
         LEFT JOIN item i ON plm.id_item = i.id_item
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by is NULL AND
         dps.orderId <> "" AND
         dt.jenis_layanan in ("ADDONSTB","CHANGESTB","2ndSTB","WifiExtender","plc","PDA","CHANGESTB2NDSTB","CHANGESTBWIFIEXTENDER","ADDSTBPLC","ADDSTBWIFIEXTENDER","2NDSTBPLC","2NDSTBWIFIEXTENDER","3RDSTBPLC","3RDSTBWIFIEXTENDER") AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');
      // dd($query);
      return $query;
  }

  public static function dashboardTransaksiSmartIndihome($tgl){
    $query = DB::select('
        SELECT
        pls.laporan_status_id,
        pls.laporan_status,
         SUM(CASE WHEN dt.jenis_layanan = "SMARTINDIHOME" THEN 1 else 0 END) as wo_smartindihome,
         count(*) as jumlah
        FROM
         dispatch_teknisi dt
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_myir_wo dps ON dt.Ndem = dps.myir
         LEFT JOIN mdf m ON dps.sto = m.mdf
        WHERE
          dt.dispatch_by = 7 AND
          dps.myir <> "" AND
          dt.jenis_layanan in ("SMARTINDIHOME") AND
          (pl.status_laporan is NULL OR
          pls.laporan_status_id in ("1","16","49","50","6","48","45","12","5","29","28","51","46","53","31")) AND
          dt.tgl like "%'.$tgl.'%"
        GROUP BY pls.laporan_status
        ORDER BY pls.urutan ASC
      ');
    return $query;
  }

  public static function reportDashboardTransaksiSmartIndhome($tgl,$jenis,$status,$so){
      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND dt.jenis_layanan = "'.$so.'"';
      };

      if ($status == "NO UPDATE"){
        $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
      } else if ($status == "ALL"){
        $where_status = ' AND (pl.status_laporan is NULL OR
          pls.laporan_status_id in ("1","16","49","50","6","48","45","12","5","29","28","51","46","31")) ';
      } else {
        $where_status = '  AND pls.laporan_status = "'.$status.'"';
      }

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          dt.jenis_layanan as dtJenisLayanan,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
          0 as orderDatePs,
          tt.ketTiang,
          dps.*
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
         LEFT JOIN item i ON plm.id_item = i.id_item
         LEFT JOIN psb_myir_wo dps ON dt.Ndem = dps.myir
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by = 7 AND
         dps.myir <> "" AND
         dt.jenis_layanan in ("SMARTINDIHOME") AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');
      // dd($query);
      return $query;
  }

  public static function getKetDashboard($nik){
      return DB::table('group_telegram')->where('TL_NIK',$nik)->where('title','LIKE','%TERRITORY%')->first();
  }

  public static function dashboardTeritory($tgl,$ketDashboard){
    if ($ketDashboard==1){
        $getWO = 'SUM(CASE WHEN m.chat_id = "-253578253" THEN 1 ELSE 0 END) as woTlWahyu,
                  SUM(CASE WHEN m.chat_id = "-1001405220793" THEN 1 ELSE 0 END) as woTlRokhman,
                  SUM(CASE WHEN m.chat_id = "-1001188642735" THEN 1 ELSE 0 END) as woTlArie,';

        $ket = ' m.ket_dashboard=1 AND';
    }
    elseif($ketDashboard==2){
      $getWO = 'SUM(CASE WHEN m.chat_id = "-283767249" THEN 1 ELSE 0 END) as woTlJunai,
                SUM(CASE WHEN m.chat_id = "-348628322" THEN 1 ELSE 0 END) as woTlHasan,
                SUM(CASE WHEN m.chat_id = "-1001155521550" THEN 1 ELSE 0 END) as woTlTamami,
                SUM(CASE WHEN m.chat_id = "-1001318576095" THEN 1 ELSE 0 END) as woTlIqbal,';

      $ket = ' m.ket_dashboard=2 AND';
    }
    elseif($ketDashboard==3){
      $getWO = 'SUM(CASE WHEN m.chat_id = "-114836111" THEN 1 ELSE 0 END) as woTlSarpani,
                SUM(CASE WHEN m.chat_id = "-1001077578882" THEN 1 ELSE 0 END) as woTlHifni,';
      $ket = ' m.ket_dashboard=3 AND';
    }
    elseif($ketDashboard==4){
      $getWO = 'SUM(CASE WHEN m.chat_id = "-1001186288135" THEN 1 ELSE 0 END) as woTlSamsudin,';
      $ket = ' m.ket_dashboard=4 AND';
    }
    elseif ($ketDashboard==5){
      $getWO = 'SUM(CASE WHEN m.chat_id = "-1001499053925" THEN 1 ELSE 0 END) as woTlArif,
                SUM(CASE WHEN m.chat_id = "-1001439986938" THEN 1 ELSE 0 END) as woTlVherda,
                SUM(CASE WHEN m.chat_id = "-307695867" THEN 1 ELSE 0 END) as woTlRestu,
                SUM(CASE WHEN m.chat_id = "-1001194255648" THEN 1 ELSE 0 END) as woTlVherda2,
                SUM(CASE WHEN m.chat_id = "-316682210" THEN 1 ELSE 0 END) as woTlHasbullah,
                SUM(CASE WHEN m.chat_id = "-242478590" THEN 1 ELSE 0 END) as woTlIrwan,';
      $ket = ' m.ket_dashboard=5 AND';
    };

    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       '.$getWO.'
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN regu r ON dt.id_regu=r.id_regu
       LEFT JOIN group_telegram m ON r.mainsector = m.chat_id
      WHERE
        dt.dispatch_by is NULL AND
        dps.orderId <> "" AND
        r.ACTIVE=1 AND
        '.$ket.'
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status
      ORDER BY pls.urutan asc, pls.laporan_status asc
    ');

    return $query;
  }

  public static function listDashboardTeritori($tgl,$jenis,$status,$so){
      $where_area = "";
      if ($so==1){
        $where_area = ' AND gt.chat_id in ("-253578253","-1001405220793","-1001188642735") ';
      }
      elseif ($so==2){
        $where_area = ' AND gt.chat_id in ("-283767249","-348628322","-1001155521550","-1001318576095") ';
      }
      elseif ($so==3){
        $where_area = ' AND gt.chat_id in ("-114836111","-1001077578882") ';
      }
      elseif ($so==4){
        $where_area = ' AND gt.chat_id in ("-1001186288135") ';
      }
      elseif ($so==5){
        $where_area = ' AND gt.chat_id in ("-242478590","-307695867","-316682210","-1001194255648","-1001439986938","-1001499053925") ';
      }
      elseif ($so=="ALL"){
        $where_area = '';
      }
      else {
        $where_area = ' AND gt.chat_id = "'.$so.'"';
      };

      if ($status=="NOUPDATE"){
        $where_status = ' AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
      }
      else if ($status == "ALL"){
        $where_status = '';
      } else {
       $where_status = '  AND pls.laporan_status = "'.$status.'"';
      };

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          dps.*
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by is NULL AND
         dps.orderId <> "" AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');

      return $query;
  }

  public static function scbeKendala($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       pls.stts_dash,
       SUM(CASE WHEN m.datel = "BANJARMASIN" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.datel = "BANJARBARU" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.datel = "KANDANGAN" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.datel = "TANJUNG" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.datel = "BATULICIN" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
       LEFT JOIN data_starclick_ao dps ON dt.Ndem=dps.orderId
       LEFT JOIN maintenance_datel m ON (my.sto = m.sto OR dps.sto = m.sto)
      WHERE
        (dt.dispatch_by="5" OR dt.dispatch_by is null) AND
        (my.myir <> "" OR dps.orderId<>"") AND
        pls.aktif_dash=1 AND
        pls.stts_dash IN ("hs","daman","aso","amo") AND
        dt.tgl like "%'.$tgl.'%"

      GROUP BY pls.laporan_status
      ORDER BY pls.urut_stts asc, pls.laporan_status_id asc
    ');
    return $query;
  }

  public static function scbeKendalaWitel($tgl){
    $get_datel = DB::table('maintenance_datel')->where('witel',session('witel'))->groupBy('datel')->get();

    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       pls.stts_dash,';
    foreach ($get_datel as $datel){
      $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as '.$datel->datel.',';
    }

       $SQL .= '
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
       LEFT JOIN data_starclick_ao dps ON dt.Ndem=dps.orderId
       LEFT JOIN maintenance_datel m ON (my.sto = m.sto OR dps.sto = m.sto)
      WHERE
        (dt.dispatch_by="5" OR dt.dispatch_by is null) AND
        (my.myir <> "" OR dps.orderId<>"") AND
        pls.aktif_dash=1 AND
        pls.stts_dash IN ("hs","daman","aso","amo") AND
        dt.tgl like "'.$tgl.'%"

      GROUP BY pls.laporan_status
      ORDER BY pls.urut_stts asc, pls.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);
    return $query;
  }

  public static function scbeKendalaPt1($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       pls.stts_dash,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
       LEFT JOIN data_starclick_ao dps ON dt.Ndem=dps.orderId
       LEFT JOIN mdf m ON (my.sto = m.mdf OR dps.sto = m.mdf)
      WHERE
        (dt.dispatch_by="5" OR dt.dispatch_by is null) AND
        (my.myir <> "" OR dps.orderId<>"") AND
        pls.aktif_dash=1 AND
        pls.stts_dash IN ("PT1") AND
        dt.tgl like "%'.$tgl.'%"

      GROUP BY pls.laporan_status
      ORDER BY pls.urut_stts asc, pls.laporan_status_id asc
    ');
    return $query;
  }

  public static function dashReboundaryDispatch($tgl){
    $query = DB::SELECT('
      SELECT
       SUM(CASE WHEN m.area_migrasi = "INNER" AND rs.dispatch = "1" THEN 1 ELSE 0 END) as WO_INNER_dispatch,
       SUM(CASE WHEN m.area_migrasi = "INNER" AND rs.dispatch = "0" THEN 1 ELSE 0 END) as WO_INNER_undispatch,
       SUM(CASE WHEN m.area_migrasi = "BBR" AND rs.dispatch = "1" THEN 1 ELSE 0 END) as WO_BBR_dispatch,
       SUM(CASE WHEN m.area_migrasi = "BBR" AND rs.dispatch = "0" THEN 1 ELSE 0 END) as WO_BBR_undispatch,
       SUM(CASE WHEN m.area_migrasi = "TJL" AND rs.dispatch = "1" THEN 1 ELSE 0 END) as WO_TJL_dispatch,
       SUM(CASE WHEN m.area_migrasi = "TJL" AND rs.dispatch = "0" THEN 1 ELSE 0 END) as WO_TJL_undispatch,
       SUM(CASE WHEN m.area_migrasi = "BLC" AND rs.dispatch = "1" THEN 1 ELSE 0 END) as WO_BLC_dispatch,
       SUM(CASE WHEN m.area_migrasi = "BLC" AND rs.dispatch = "0" THEN 1 ELSE 0 END) as WO_BLC_undispatch,
       count(*) as jumlah
      FROM
       reboundary_survey rs
       LEFT JOIN Data_Pelanggan_Starclick dps ON rs.scid=dps.orderId
       LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        rs.updated_at like "%'.$tgl.'%"
    ');
    return $query;
  }

  public static function listDispatchReboundary($area, $tgl, $status){
    if ($area=='ALL'){
        return DB::table('reboundary_survey')
                ->leftJoin('dispatch_teknisi','reboundary_survey.no_tiket_reboundary','=','dispatch_teknisi.Ndem')
                ->leftJoin('Data_Pelanggan_Starclick','reboundary_survey.scid','=','Data_Pelanggan_Starclick.orderId')
                ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
                ->leftJoin('group_telegram','reboundary_survey.chat_sektor','=','group_telegram.chat_id')
                ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
                ->leftJoin('mdf','Data_Pelanggan_Starclick.sto','=','mdf.mdf')
                ->select('reboundary_survey.*','Data_Pelanggan_Starclick.*', 'regu.id_regu', 'regu.uraian', 'group_telegram.chat_id', 'group_telegram.title', 'psb_laporan.status_laporan','psb_laporan_status.laporan_status', 'mdf.area_migrasi', 'dispatch_teknisi.id as idDt', 'reboundary_survey.idTimLama', 'reboundary_survey.uraianTimLama')
                ->where('reboundary_survey.updated_at','LIKE', '%'.$tgl.'%')
                ->where('reboundary_survey.dispatch',$status)
                ->get();
    }
    else{
        return DB::table('reboundary_survey')
                ->leftJoin('dispatch_teknisi','reboundary_survey.no_tiket_reboundary','=','dispatch_teknisi.Ndem')
                ->leftJoin('Data_Pelanggan_Starclick','reboundary_survey.scid','=','Data_Pelanggan_Starclick.orderId')
                ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
                ->leftJoin('group_telegram','reboundary_survey.chat_sektor','=','group_telegram.chat_id')
                ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
                ->leftJoin('mdf','Data_Pelanggan_Starclick.sto','=','mdf.mdf')
                ->select('reboundary_survey.*','Data_Pelanggan_Starclick.*', 'regu.id_regu', 'regu.uraian', 'group_telegram.chat_id', 'group_telegram.title', 'psb_laporan.status_laporan','psb_laporan_status.laporan_status', 'mdf.area_migrasi', 'dispatch_teknisi.id as idDt', 'reboundary_survey.idTimLama', 'reboundary_survey.uraianTimLama')
                ->where('reboundary_survey.updated_at','LIKE', '%'.$tgl.'%')
                ->where('mdf.area_migrasi',$area)
                ->where('reboundary_survey.dispatch',$status)
                ->get();
    };
  }

  public static function progressReboundary($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN reboundary_survey my ON dt.Ndem=my.no_tiket_reboundary
       LEFT JOIN Data_Pelanggan_Starclick dps ON my.scid=dps.orderId
       LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        dt.dispatch_by="6" AND
        my.no_tiket_reboundary <> ""  AND
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status
      ORDER BY pls.stts_grup_order asc, pls.urutan asc
    ');
    return $query;
  }

  public static function reboundaryList($tgl,$jenis,$status,$so){
    $where_area = '';
    if ($so<>"ALL"){
      $where_area = ' AND m.area_migrasi = "'.$so.'"';
    }

    if ($status == "NO UPDATE"){
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    } else if ($status == "ALL"){
      $where_status = ' ';
    } else {
      $where_status = '  AND pls.laporan_status = "'.$status.'"';
    }

    $query = DB::SELECT('
      SELECT
          *,
          dt.id as id_dt,
          dt.created_at as tanggal_dispatch,
          pl.modified_at as modified_at,
          tt.ketTiang,
          pls.laporan_status_id,
          dps.*,
          my.catatan as catatan_reboundary,
          pl.catatan as catatan_teknisi
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
         LEFT JOIN reboundary_survey my ON dt.Ndem = my.no_tiket_reboundary
         LEFT JOIN Data_Pelanggan_Starclick dps ON my.scid = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by="6" AND
         my.scid <> "" AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
    ');

    return $query;
  }

  public static function dashboadWifiId($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN micc_wo mw ON dt.Ndem=mw.ND
       LEFT JOIN mdf m ON mw.MDF = m.mdf
      WHERE
        mw.ND <> ""  AND
        mw.jenis_layanan = "WIFI.ID" AND
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status
      ORDER BY pls.stts_grup_order asc, pls.urutan asc
    ');
    return $query;
  }

  public static function dashboadDatin($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN micc_wo mw ON dt.Ndem=mw.ND
       LEFT JOIN mdf m ON mw.MDF = m.mdf
      WHERE
        mw.ND <> ""  AND
        (mw.jenis_layanan LIKE "%ASTINET%" OR mw.jenis_layanan LIKE "%VPNIP%" OR mw.jenis_layanan LIKE "%METRO%" ) AND
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status
      ORDER BY pls.stts_grup_order asc, pls.urutan asc
    ');
    return $query;
  }

  public static function listWifiId($area, $tgl, $status){
    if ($area=="ALL"){
        $where_area = '';
    }
    else{
        $where_area = 'AND m.area_migrasi = "'.$area.'"';
    };

    if ($status=="ALL"){
        $where_status = '';
    }
    else if ($status=="NO UPDATE"){
        $where_status = ' AND (pl.status_laporan=6 OR pl.status_laporan=NULL OR pl.status_laporan="")';
    }
    else{
        $where_status = ' AND pls.laporan_status="'.$status.'"';
    }

    $sql = 'SELECT
              *,
            mw.jenis_layanan as layananMicc
            FROM
              micc_wo mw
            LEFT JOIN dispatch_teknisi dt ON mw.ND = dt.Ndem
            LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
            LEFT JOIN mdf m ON mw.MDF=m.mdf
            LEFT JOIN regu r ON dt.id_regu=r.id_regu
            LEFT JOIN group_telegram gr ON r.mainsector=gr.chat_id
            WHERE
              dt.tgl LIKE "%'.$tgl.'%" AND
              mw.jenis_layanan="WIFI.ID"'.$where_area.$where_status;

    return DB::select($sql);
  }

  public static function listDatin($area, $tgl, $status){
    if ($area=="ALL"){
        $where_area = '';
    }
    else{
        $where_area = ' AND m.area_migrasi = "'.$area.'"';
    };

    if ($status=="ALL"){
        $where_status = '';
    }
    else if ($status=="NO UPDATE"){
        $where_status = ' AND (pl.status_laporan=6 OR pl.status_laporan=NULL OR pl.status_laporan="")';
    }
    else{
        $where_status = ' AND pls.laporan_status="'.$status.'"';
    }

    $sql = 'SELECT
              *,
            mw.jenis_layanan as layananMicc
            FROM
              micc_wo mw
            LEFT JOIN dispatch_teknisi dt ON mw.ND = dt.Ndem
            LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
            LEFT JOIN mdf m ON mw.MDF=m.mdf
            LEFT JOIN regu r ON dt.id_regu=r.id_regu
            LEFT JOIN group_telegram gr ON r.mainsector=gr.chat_id
            WHERE
              dt.tgl LIKE "%'.$tgl.'%" AND
              (mw.jenis_layanan LIKE "%ASTINET%" OR mw.jenis_layanan LIKE "%VPNIP%" OR mw.jenis_layanan LIKE "%METRO%" )'.$where_area.$where_status;

    return DB::select($sql);
  }

  public static function getListUndispatchCcan($tgl, $area, $ket){
      $where_ket = '';
      if ($ket<>"ALL"){
        $where_ket = 'AND dps.layanan LIKE "%'.$ket.'%"';
      };

      $where_area = 'AND m.area_migrasi="'.$area.'"';
      if ($area=="ALL"){
        $where_area = '';
      };

      $sql = '
            SELECT
                *
            FROM
                micc_wo dps
                LEFT JOIN dispatch_teknisi dt ON dps.ND = dt.Ndem
                LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
                LEFT JOIN mdf m ON dps.MDF = m.mdf
            WHERE
                m.mdf<>""
                AND dt.id is NULL
                '.$where_area.'
                '.$where_ket.'
            ';

      return  DB::select($sql);
  }

  public static function getHrPs($bulan){
      $sql = 'SELECT
                a.*, c.modified_at as tglStatus, e.*, d.uraian
              FROM
                dispatch_teknisi a
              LEFT JOIN Data_Pelanggan_Starclick b ON b.orderId = a.Ndem
              LEFT JOIN psb_laporan c ON c.id_tbl_mj = a.id
              LEFT JOIN regu d ON d.id_regu = a.id_regu
              LEFT JOIN group_telegram e ON e.chat_id = d.mainsector
              WHERE
                b.orderStatus="Completed (PS)" AND
                c.status_laporan = "4" AND
                c.modified_at LIKE "%'.$bulan.'%"
              ';

      $data = DB::select($sql);
      return $data;
  }

  public static function getMatrixCcan($tgl, $user){
      $sql = 'SELECT
                a.Ndem, a.tgl, a.updated_by, c.nama
              FROM
                dispatch_teknisi a
              LEFT JOIN micc_wo b ON b.ND = a.Ndem
              LEFT JOIN roc d ON d.no_tiket = a.Ndem
              LEFT JOIN 1_2_employee c ON c.nik = a.updated_by
              WHERE
                (b.ND IS NOT NULL OR d.loker_ta="2") AND
                a.updated_by = "'.$user.'" AND
                a.tgl LIKE "%'.$tgl.'%"
              GROUP BY a.Ndem
              ';

      return DB::select($sql);
  }

  public static function getListUmurByNossa($tgl){
      $sql = 'SELECT
                *
              FROM
                data_nossa_1_log a
              LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
              LEFT JOIN regu c ON b.id_regu = c.id_regu
              WHERE
                a.Reported_Date LIKE "%'.$tgl.'%" AND
                (a.Customer_Segment IS NOT NULL OR a.Customer_Segment NOT IN ("DCS"))
              ORDER BY
                a.TTR_Witel DESC
             ';
      $data = DB::select($sql);
      return $data;
  }

  public static function dashboardUmurByNossa($tgl){
        $sql = 'SELECT
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("MM_ASTINET","MM_IPVPN") AND TTR_Witel < 3) THEN 1 ELSE 0 END ) as datin_C ,
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("MM_ASTINET","MM_IPVPN") AND TTR_Witel > 3) THEN 1 ELSE 0 END ) as datin_NC ,
                SUM(CASE WHEN (Customer_Segment IN ("DGS") AND Service_Type IN ("VOICE") AND TTR_Witel < 5) THEN 1 ELSE 0 END ) as voice_dgs_c,
                SUM(CASE WHEN (Customer_Segment IN ("DGS") AND Service_Type IN ("VOICE") AND TTR_Witel > 5) THEN 1 ELSE 0 END ) as voice_dgs_nc,
                SUM(CASE WHEN (Customer_Segment IN ("DBS", "DES") AND Service_Type IN ("VOICE") AND TTR_Witel < 8.5) THEN 1 ELSE 0 END ) as voice_dbs_des_c,
                SUM(CASE WHEN (Customer_Segment IN ("DBS", "DES") AND Service_Type IN ("VOICE") AND TTR_Witel > 8.5) THEN 1 ELSE 0 END ) as voice_dbs_des_nc,
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("INTERNET","IPTV") AND TTR_Witel < 8.5) THEN 1 ELSE 0 END ) as inetIptv_c,
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("INTERNET","IPTV") AND TTR_Witel > 8.5) THEN 1 ELSE 0 END ) as inetIptv_nc,
                SUM(CASE WHEN (Customer_Segment IN ("DWS") AND Service_Type IN ("MM_METRO_ETHERENT","SITE") AND TTR_Witel < 3) THEN 1 ELSE 0 END ) as squadOlo_c,
                SUM(CASE WHEN (Customer_Segment IN ("DWS") AND Service_Type IN ("MM_METRO_ETHERENT","SITE") AND TTR_Witel > 3) THEN 1 ELSE 0 END ) as squadOlo_nc,
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("LOC_AP","MM_WIFI_ID") AND TTR_Witel < 8.5) THEN 1 ELSE 0 END ) as wifiId_c,
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("LOC_AP","MM_WIFI_ID") AND TTR_Witel > 8.5) THEN 1 ELSE 0 END ) as wifiId_nc
              FROM
                data_nossa_1_log
              WHERE
                Reported_Date LIKE "%'.$tgl.'%"
             ';

        $data = DB::select($sql);
        return $data;
  }

  public static function getWOPs($tgl){
     $sql = 'SELECT
                a.id as id_dt,
                d.title,
                d.chat_id,
                c.id_regu,
                c.uraian,
                SUM(CASE WHEN e.status_laporan="1" THEN 1 ELSE 0 END) as jumlahPs,
                SUM(CASE WHEN (e.status_laporan="1" AND e.dropcore_label_code IS NOT NULL) THEN 1 ELSE 0 END) as jumlahPSQr

            FROM
              dispatch_teknisi a
            LeFT JOIN Data_Pelanggan_Starclick b ON a.Ndem = b.orderId
            LEFT JOIN regu c ON a.id_regu = c.id_regu
            LEFT JOIN group_telegram d ON d.chat_id = c.mainsector
            LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
            LEFT JOIN jenis_layanan f ON a.jenis_layanan = f.jenis_layanan
            WHERE
              a.tgl LIKE "%'.$tgl.'%" AND
              e.status_laporan = 1 AND
              b.orderId <> "" AND
              d.ketTer = 1
            GROUP BY
              d.title
            ';

    return DB::select($sql);
  }

  public static function getWoPsDetail($tgl, $chatId, $status){
      if ($status==0){
          $where_status = 'AND e.status_laporan="1"';
      }
      elseif($status==1){
          $where_status = 'AND (e.status_laporan="1" AND e.dropcore_label_code IS NOT NULL)';
      };

      if($chatId=="ALL"){
          $where_chatid = '';
      }
      else{
          $where_chatid = 'AND d.chat_id="'.$chatId.'"';
      };

      $sql = 'SELECT
                d.title,
                d.chat_id,
                c.id_regu,
                c.uraian,
                b.orderStatus,
                e.dropcore_label_code,
                g.laporan_status,
                b.orderDatePs,
                b.*,
                a.Ndem as orderId,
                e.modified_at as tglUp,
                b.jenisPsb,
                a.id as id_dt,
                e.nama_odp,
                e.snont,
                e.port_number,
                h.*,
                i.namaOdp
                FROM
                  dispatch_teknisi a
                LeFT JOIN Data_Pelanggan_Starclick b ON a.Ndem = b.orderId
                LEFT JOIN regu c ON a.id_regu = c.id_regu
                LEFT JOIN group_telegram d ON d.chat_id = c.mainsector
                LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
                LEFT JOIN jenis_layanan f ON a.jenis_layanan = f.jenis_layanan
                LEFT JOIN psb_laporan_status g ON e.status_laporan = g.laporan_status_id
                LEFT JOIN maintenance_datel h ON h.sto = b.sto
                LEFT JOIN psb_myir_wo i ON b.orderId = i.sc
                WHERE
                  a.tgl LIKE "%'.$tgl.'%" AND
                  e.status_laporan = 1 AND
                  b.orderId <> "" AND
                  d.ketTer = 1
              '.$where_status.'
              '.$where_chatid.'
            ';

      return DB::select($sql);
  }

}

?>
