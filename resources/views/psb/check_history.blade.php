@extends('tech_layout')

@section('content')
<h3>Histroy Order Check</h3>
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Search
      </div>
      <div class="panel-body">
        <form method="post">
        Input : <input type="text" name="input" value="{{ $input }}" required> <input type="submit" name="submit" value="Submit" />
        </form>
      </div>
    </div>
  </div>
  <!-- result !-->
  @if ($input <> "")
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Result Provisioning Order
      </div>
      <div class="panel-body">
        <div class="alert alert-{{ $alert }}">{!! $alertText !!}</div>
        @foreach ($check_tomman as $num => $result)
        #{{ $num }}
        <table class="table">
          <tr>
            @if ($result->id<>"")
            <td  width="200">Order ID</td><td><a class="label label-success" href="/{{ $result->id }}">{{ $result->orderId }}</a></td>
            @else
            <td  width="200">Order ID</td><td>{{ $result->orderId }}</td>
            @endif</tr>
          <tr>
            <td>Nama</td><td>{{ $result->orderName }}</td>
          </tr>
          <tr>
            <td>Alamat SC</td><td>{{ $result->orderAddr }}</td>
          </tr>
          <tr>
            <td>Alamat Sales</td><td>{{ $result->alamatSales ? : $result->manja }}</td>
          </tr>
          <tr>
            <td>Jenis Order</td><td>{{ $result->jenisPsb }} / {{ $result->orderStatus }}</td>
          </tr>
          <tr>
            <td>Alpro </td><td>SC. {{ $result->alproname }} / TEK. {{ $result->nama_odp }}</td>
          </tr>
          <tr>
            <td>Tgl Order</td><td>{{ $result->orderDate }}</td>
          </tr>
          <tr>
            <td>Teknisi </td><td>{{ $result->uraian }}</td>
          </tr>

        </table>
        @endforeach
      </div>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Result Assurance Order
      </div>
      <div class="panel-body">
        <div class="alert alert-{{ $alert_asr }}">{!! $alertText_asr !!}</div>
        @foreach ($check_tomman_asr as $num => $result)
        #{{ $num }}
        <table class="table">
          <tr>
            @if ($result->id<>"")
            <td  width="200">Order ID</td><td><a class="label label-success" href="/{{ $result->id }}">{{ $result->Incident }}</a></td>
            @else
            <td  width="200">Order ID</td><td>{{ $result->Incident }}</td>
            @endif
          </tr>
          <tr>
            <td>Nama</td><td>{{ $result->Customer_Name }}</td>
          </tr>
          <tr>
            <td>Summary</td><td>{{ $result->Summary }}</td>
          </tr>
          <tr>
            <td>Contact</td><td>{{ $result->Contact_Phone }}</td>
          </tr>
          <tr>
            <td>Alpro </td><td>NOSSA. {{ $result->Datek }} / TEK. {{ $result->nama_odp }}</td>
          </tr>

          <tr>
            <td>Reported Date</td><td>{{ $result->Reported_Date }}</td>
          </tr>
          <tr>
            <td>Teknisi </td><td>{{ $result->uraian }}</td>
          </tr>

        </table>
        @endforeach
      </div>
    </div>
  </div>
  @endif
</div>
@endsection
