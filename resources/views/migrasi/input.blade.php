@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>
   UPLOAD WORK ORDER MIGRASI <a href="/ms2n/sync/VA" class="btn btn-info">
      <span class="glyphicon glyphicon-refresh"> SyncNow</span>
    </a>
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <li class="{{ (Request::path() == 'migrasi') ? 'active' : '' }}"><a href="/migrasi">ALL MIGRASI</a></li>
    <li class="{{ (Request::segment(2) == 'report') ? 'active' : '' }}"><a href="/migrasi/report">REPORT</a></li>
    <li class="{{ (Request::segment(2) == 'upload') ? 'active' : '' }}"><a href="/migrasi/upload">UPLOAD</a></li>
  </ul>
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    <input type="hidden" name="wos" value="[]" />
    <div class="form-group">
      <label class="control-label" for="tes">Import Excel</label>
      <input type="file" id="tes" class="form-control"/>
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
    <div id="jumlah"></div>
    <div class="list-group"></div>

  </form>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />

  <script src="/bower_components/excel/zip/WebContent/zip.js"></script>
  <script src="/bower_components/excel/async-master/dist/async.js"></script>
  <script src="/bower_components/excel/underscore.js"></script>
  <script src="/bower_components/excel/xlsxParser.js"></script>
  <script>
    $(function() {
      var migrasi = [];
      $("#tes").change(function(e){
        migrasi = [];
        $(".loading").css({"visibility":"visible"});
        $(".removable").remove();
        var file = document.getElementById('tes').files[0];
        zip.workerScriptsPath = "/bower_components/excel/zip/WebContent/";
        xlsxParser.parse(file).then(function(data) {
          var count = 0;
          for(var i=1;i<data.length;i++){
            var e_nd        = data[i][1],
                e_nd_speedy = data[i][2],
                e_nama      = data[i][3],
                e_alamat    = data[i][5],
                e_mdf       = data[i][13],
                e_odp       = data[i][24],
                e_olt       = data[i][22],
                e_kcontact  = '-';
            if(e_nd){
              migrasi.push({
                mdf: e_mdf,
                nd : e_nd,
                nd_speedy: e_nd_speedy,
                nama : e_nama,
                alamat : e_alamat,
                odp : e_odp,
                olt : e_olt,
                kcontact : e_kcontact
              });
              $(".list-group").append("<div class='list-group-item removable'><span>" + e_nama + "<span><br/><span>" + e_nd + "<span><br/><span>" + e_nd_speedy + "<span><br/><span>" + e_nama + "<span><br/><span>" + e_alamat + "<span><br/><span>" + e_odp + "<span><br/><span>" + e_olt + "<span><br/><span>" + e_kcontact + "<span><br/>");
              count++;
            }
          }

          $("#jumlah").append("<span class=removable>Ticked Found : " + count + "</span>");
          $(".loading").css({"visibility":"hidden"});
        }, function(err) {
          alert.log('error', err);
        });
      })
      $('#submit-form').submit(function() {
        $('input[name=wos]').val(JSON.stringify(migrasi));
      });


	  })
  </script>
@endsection
