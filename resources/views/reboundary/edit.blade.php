@extends('layout')

@section('header')
@endsection

@section('content')
  @include('partial.alerts')

  <form method="POST" >
      <div class="panel panel-primary">
        <div class="panel-heading">Input Data Reboundary</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                          <label>SCID</label>
                          <input type="text" class="form-control" name="scid" id="scid" value="{{ $data->scid ?: old('scid') }}" readonly>
                          {!! $errors->first('scid', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                <div class="col-md-6">
                      <div class="form-group">
                          <label>Koordinat Pelanggan</label>
                          <input type="text" class="form-control" name="koorPel" id="koorPel" value="{{ $data->koor_pelanggan ?: old('koorPel') }}">
                          {!! $errors->first('koorPel', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                          <label>ODP Lama</label>
                          <input type="text" class="form-control" name="odpLama" id="odpLama" value="{{ $data->odp_lama ?: old('odpLama') }}">
                          {!! $errors->first('odpLama', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                <div class="col-md-6">
                      <div class="form-group">
                          <label>Koordinat ODP Lama</label>
                          <input type="text" class="form-control" name="koorOdpLama" id="koorOdpLama" value="{{ $data->koor_odp_lama ?: old('koorOdpLama') }}">
                          {!! $errors->first('koorOdpLama', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                          <label>ODP Baru</label>
                          <input type="text" class="form-control" name="odpBaru" id="odpBaru" value="{{ $data->odp_baru ?: old('odpBaru') }}">
                          {!! $errors->first('odpBaru', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                <div class="col-md-6">
                      <div class="form-group">
                          <label>Koordinat ODP Baru</label>
                          <input type="text" class="form-control" name="koorOdpBaru" id="koorOdpBaru" value="{{ $data->koor_odp_baru ?: old('koorOdpBaru') }}">
                          {!! $errors->first('koorOdpBaru', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                          <label>Tarikan Lama (M)</label>
                          <input type="text" class="form-control" name="tarikanLama" id="tarikanLama" value="{{ $data->tarikan_lama ?: old('tarikanLama') }}">
                          {!! $errors->first('tarikanLama', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                <div class="col-md-6">
                      <div class="form-group">
                          <label>Estimasi Tarikan (M)</label>
                          <input type="text" class="form-control" name="tarikanEstimasi" id="tarikanEstimasi" value="{{ $data->estimasi_tarikan ?: old('tarikanEstimasi') }}">
                          {!! $errors->first('tarikanEstimasi', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                          <label>Catatan</label>
                          <textarea class="form-control" rows="1" name="catatan">{{ $data->catatan ?: old('catatan') }}</textarea>
                          {!! $errors->first('catatan', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                 <div class="col-md-6">
                   <div class="form-group">
                        <label>Sektor</label>
                        <input name="sektor" type="hidden" id="sektor" class="form-control" value="{{ $data->chat_sektor ?: old('sektor') }}"  />
                        {!! $errors->first('sektor', '<span class="label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            <input type="submit" class="btn btn-primary" value="Simpan">
            <a href="/reboundary/list/{{ date('Y-m-d') }}" class="btn btn-default">Batal</a>
        </div>
        
      </div>
  </form>

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
      $(document).ready(function(){          
          $("#odpLama").inputmask("AAA-AAA-A{2,3}/999");
          $("#odpBaru").inputmask("AAA-AAA-A{2,3}/999");

          var data_sektor= <?= json_encode($dataSektor) ?>;
          var sektor = function() {
            return {
              data: data_sektor,
              placeholder: 'Pilih Sektor',
              formatResult: function(data) {
              return '<strong style="margin-left:5px">'+data.text+'</strong>';
              }
            }
          }
          $('#sektor').select2(sektor());
      });
  </script>

@endsection
  