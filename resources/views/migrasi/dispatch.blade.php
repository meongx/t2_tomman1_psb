@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id }}" />
    @endif
    <h3>
      <a href="../" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Dispatch WO {{ $data->Nama }}
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-regu">Regu</label>
      <input name="id_regu" type="hidden" id="input-regu" class="form-control" value="<?= $data->id_regu ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-tgl">Tanggal</label>
      <input name="tgl" type="tgl" id="input-tgl" class="form-control" value="<?= $data->tgl ?>" />
    </div>    
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>
  @if (isset($data->id))
  <form id="delete-form" method="post" autocomplete="off">
    <input name="_method" type="hidden" value="DELETE">
      <div style="margin:40px 0 20px">
        <button class="btn btn-danger">Hapus</button>
      </div>
  </form>
  @endif
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />  
  <script>
    $(function() {
      $('.btn-danger').click(function() {
        var sure = confirm('Yakin hapus data ?');
        if (sure) {
          $('#delete-form').submit();
        }
      })
      var state= <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Input Regu',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.telp+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-regu').select2(regu());
      var day = {
        format: 'yyyy-mm-dd',
        viewMode: 0, 
        minViewMode: 0
      };
      $('#input-tgl').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });;
    })
  </script>
@endsection
