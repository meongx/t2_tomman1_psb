@extends('layout')

@section('content')
  @include('partial.alerts')
  <center><h3>DASHBOARD KEHADIRAN {{ $date }}</h3></center>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading">
  				
  			</div>
  			<div class="panel-body">
  				<table class="table">
  					<tr>
  						<th rowspan="2">No</th>
  						<th rowspan="2">Sektor</th>
  						<th rowspan="2">Jumlah</th>
  						<th colspan="3">Tek. Assurance</th>
  						<th colspan="3">Tek. Provisioning</th>
  					</tr>
  					<tr>
  						<th>Request</th>
  						<th>Approved</th>
  						<th>Decline</th>
  						<th>Request</th>
  						<th>Approved</th>
  						<th>Decline</th>
  					</tr>
             <?php
                $total = 0;
                $totalrequestASR = 0;
                $totalapprovalASR = 0;
                $totaldeclineASR = 0;
                $totalrequestPROV = 0;
                $totalapprovalPROV = 0;
                $totaldeclinePROV = 0;
              
              ?>
            @if (count($list)>1)
             
    					@foreach ($list as $num => $result)
              <?php
                $total += $result['jumlahTim'];
                $totalrequestASR += $result['requestAsr'];
                $totalapprovalASR += $result['approvalAsr'];
                $totaldeclineASR += $result['declineAsr'];
                $totalrequestPROV += $result['requestProv'];
                $totalapprovalPROV += $result['approvalProv'];
                $totaldeclinePROV += $result['declineProv'];
              ?>
    					<tr>
    						<td>{{ ++$num }}</td>
    						<td>{{ $result['sektor'] }}</td>
    						<td>{{ $result['jumlahTim'] }}</td>
    						<td>{{ $result['requestAsr'] }}</td>
                <td>{{ $result['approvalAsr'] }}</td>
                <td>{{ $result['declineAsr'] }}</td>
                <td>{{ $result['requestProv'] }}</td>
                <td>{{ $result['approvalProv'] }}</td>
                <td>{{ $result['declineProv'] }}</td>
    					</tr>
    					@endforeach
            @endif
            <tr>
              <td colspan="2">TOTAL</td>
              <td>{{ $total }}</td>
              <td>{{ $totalrequestASR }}</td>
              <td>{{ $totalapprovalASR }}</td>
              <td>{{ $totaldeclineASR }}</td>
              <td>{{ $totalrequestPROV }}</td>
              <td>{{ $totalapprovalPROV }}</td>
              <td>{{ $totaldeclinePROV }}</td>

            </tr>
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection