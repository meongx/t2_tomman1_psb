
@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .panel-content {
      padding : 10px;
    }
  </style>
  <div class="panel panel-primary">
    <div class="panel-heading"><center>Profile</center></div>
    <div class="panel-content">
      <div class="row">
        <div class="col-sm-6">
          <img class="img-rounded" src="http://apps.telkomakses.co.id/wimata/photo/crop_91153709.jpg" height="150" />
          <br />
          <br />
        </div>
        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">Penyelesaian</div>
            <div class="panel-content">
              <center>
                Dari Orderdan yang masuk<br />
                <h4>95.5%</h4>
              </center>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-primary">
        <div class="panel-heading">Provisioning</div>
        <div class="panel-content">...</div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="panel panel-primary">
        <div class="panel-heading">Assurance</div>
        <div class="panel-content">...</div>
      </div>
    </div>

  </div>
@endsection
