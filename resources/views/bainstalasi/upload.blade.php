
@extends('layout')

@section('content')
  @include('partial.alerts')
<form method="post" enctype="multipart/form-data">
  <h3>Upload New File {{ $no_internet }}</h3>
  <input type="hidden" name="id" value="{{ $no_internet }}">
  <input type="file" name="userfile" />
  <br />
  <input type="submit" value="submit" />
</form>
@endsection
