@extends('layout')
@section('content')
  @include('partial.alerts')
	<div class="panel panel-primary">
		<div class="panel-heading">List Plasa - Sales</div>
		<div class="panel-body table-responsive">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<a href="/dshr/plasa-sales/plasa/form" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span></a>
					</div>

					<h4>Filter :</h4>
					<input type="text" name="filter" id="filter" value="ALL"><br><br>
					<input type="hidden" name="tglFilter" id="tglFilter" value="{{ $tgl }}">

					<ul class="nav nav-tabs" style="width: 100%;">
            <li ><a href="/dshr/plasa-sales/list-wo-by-sales/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Today</a></li>
            <li @if ($active == "validasi") class="active" @endif ><a href="/qc1_list/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">QC 1</a></li>
					  <li @if ($active == "dispatch") class="active" @endif ><a href="/belum_dispatch/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Belum Terdispatch</a></li>
					  <li><a href="/dshr/plasa-sales/list-dispatch/{{ date('Y-m') }}">Terdispatch</a></li>
					</ul>

					<br>

					<div id="isi">
						<table class="table table-bordered">
							<tr>
								<th>No</th>
								<th>Tgl Order</th>
								<th>Tgl Dispatch</th>
								<th>Myir / SC</th>
								<th>SC Sinkron</th>
								<th>Pelanggaan</th>
								<th>Tanggal Lahir</th>
								<th>Alamat</th>
								<th>STO</th>
								<th>ODP</th>
								<th>Layanan</th>
								<th>PSB</th>
								<th>Paket Indihome</th>
								<th>Paket Sales</th>
								<th>Email</th>
								<th>Kcontack</th>
								<th>Ket Dispatch</th>
								<th>Regu</th>
								<th>Sales ID / Kode Plasa</th>
								<th>Status Laporan</th>
								<th>Ket</th>
							</tr>

							@foreach($getData as $no=>$data)
								<tr>
									<td>{{ ++$no }}</td>
									<td>{{ $data->orderDate }}</td>
									<td>{{ $data->tglDispatch }}</td>
									<td>{{ $data->myirInput }}</td>
									<td>
										@if($data->dispatch==1 && $data->ket==0 )
											<a href="/dshr/plasa-sales/input-sc/{{ $data->myirInput }}" class="btn btn-primary btn-sm" id="add_sc">Add SC</a><br>
										@endif
										{{ $data->Ndem ?: '-'}}
									</td>
									<td>{{ $data->customer }}</td>
									<td>{{ $data->tgl_lahir ?: ' - ' }}</td>
									<td>{{ $data->alamatLengkap }}</td>
									<td>{{ $data->sto_wo }}</td>
									<td>{{ $data->namaOdp }}</td>
									<td>{{ $data->layanan }}</td>
									<td>{{ $data->psb ?: '-'}}</td>
									<td>{{ $data->paket_harga ?: '-' }}</td>
									<td>{{ $data->paket_sales ?: '-' }}</td>
									<td>{{ $data->email ?: '-' }}</td>
									<td>{{ $data->kcontack ?: '-' }}</td>

									<td>
										@if($data->ket_input==0)
											<a href="/dshr/plasa-sales/dispatch/plasa/{{ $data->myirInput }}" class="btn btn-primary btn-sm">Re-Dispatch</a>
										@else
											<a href="/dshr/plasa-sales/dispatch/sales/{{ $data->myirInput }}" class="btn btn-primary btn-sm">Re-Dispatch</a>
										@endif
									</td>

									<td>{{ $data->uraian }} - {{ $data->title }}</td>
									<td>{{ $data->sales_id ?: $data->created_by }}</td>
									<td>{{ $data->laporan_status ?: 'NEED PROGRESS' }}</td>

									@if($data->ket_input==0)
										<td>
											PLASA
											<a href="/dshr/plasa-sales/cetak-plasa/{{ $data->id_psbWo }}" target="_blank" class="btn btn-info btn-sm">
												<i class="glyphicon glyphicon-print" aria-hidden="true"></i>
											</a>
										</td>
									@else
										<td>SALES</td>
									@endif
								</tr>
							@endforeach
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection
@section('plugins')
	<script>
		$(function(){
			var dataFilter = [
				{'id':'ALL', 'text' : 'ALL'},
				{'id':'plasa', 'text' : 'Plasa'},
				{'id':'sales', 'text' : 'Sales'},
			];

			$('#filter').select2({
				data: dataFilter,
				placeholder: 'Pilih Filter'
			});

			$('#filter').on('click', function(){
				var nilai = $('#filter').val(),
					tgl   = $('#tglFilter').val(),
					url   = "/dshr/plasa-sales/list-dispatch/ajax/"+nilai+'/'+tgl;

				$.ajax({
					url: url,
					dataType: 'HTML',
					success : function(data){
						$('#isi').html(data)
					}
				})

			});

		})
	</script>
@endsection
