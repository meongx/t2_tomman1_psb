@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    {{ $sebab }} MS2N {{ $area }}
  </h3>

  <div class="list-group">
	  <div class="table-responsive">
	  <table class="table" style="padding:5px;">
		  <thead>
			  <tr>
			  <th>No</th>
			  <th>Order</th>
			  <th>SC</th>
			  <th>Order Status</th>
			  <th>Order Date</th>
			  <th>Updated_at</th>
			  <th>ND</th>
			  <th>Nama Pelanggan</th>
			  <th>Status Indihome</th>
			  <th>NCLI</th>
			  <th>STO</th>
        <th>ODP</th>
        <th>Chanel</th>
        <th>Sales</th>

			  <th>Keterangan Sebab</th>
			  </tr>
		  </thead>
		  <tbody>
		@foreach ($get_potensi as $no => $potensi)
			<tr>
				<td>{{ ++$no }}</td>
				@if ($potensi->id_dt<>'')
				<td>
					<a href="/{{ $potensi->id_dt }}">{{ $potensi->uraian }}</a><br />
					<span><a href="http://mydashboard.telkom.co.id/ms2/update_demand_useetv2.php?ndem={{ $potensi->Ndem }}&etat=VA" class="label label-info">Update MS2N</a></span><br />
				</td>
				@else
				<td>Undispatch<br />
				<span><a href="http://mydashboard.telkom.co.id/ms2/update_demand_useetv2.php?ndem={{ $potensi->Ndem }}&etat=VA" class="label label-info">Update MS2N</a></span><br />
				</td>
				@endif
				<td>{{ $potensi->orderId ? : '' }}</td>
				<td>{{ $potensi->orderStatus ? : '' }}</td>
				<td>{{ $potensi->orderDate ? : '' }}</td>
				<td>{{ $potensi->updated_at ? : '' }}</td>
				<td>{{ $potensi->ND  ? : '' }}<br />{{ $potensi->ND_Speedy ? : '' }}</td>
				<td>{{ $potensi->Nama ? : '' }}</td>
				<td>{{ $potensi->Status_Indihome ? : '' }}<br />{{ $potensi->jenisPsb ? : '' }}</td>
				<td>{{ $potensi->Ncli ? : '' }}</td>
				<td>{{ $potensi->sto ? : '' }}</td>
        <td>{{ $potensi->alproname ? : '' }}</td>
        <td>{{ $potensi->Chanel ? : '' }}</td>
        {{-- */ $sales = explode(";",$potensi->Kcontact); /* --}}
        @if (count($sales)>3)
        <td>{{ $sales[3] }}</td>
        @else
        <td>No Sales</td>
        @endif

				<td width="200">{{ $potensi->Ket_Sebab }}</td>
			</tr>
		@endforeach
		  </tbody>
	  </table>
	  </div>
  </div>

@endsection
