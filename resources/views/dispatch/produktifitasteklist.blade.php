@extends('layout')

@section('content')
  @include('partial.alerts')
  	<h3>{{ $title }}</h3>
  	<div class="row">
	    <div class="col-sm-12">
	      <div class="panel panel-default">
	        <div class="panel-content table-responsive">
	        	<table class="table">
	        		<tr>
	        			<th>No</th>
	        			<th>Tim</th>
	        			<th>Sektor</th>
	        			<th>Order</th>
	        			<th>Dispatch</th>
	        			<th>Status</th>
	        			<th>Catatan</th>
	        		</tr>
	        		@foreach ($get_produktifitasteklist as $num => $produktifitasteklist)
	        		<tr>
	        			<td>{{ ++$num }}</td>
	        			<td>{{ $produktifitasteklist->uraian }}</td>
	        			<td>{{ $produktifitasteklist->sektor }}</td>
	        			<td>{{ $produktifitasteklist->Ndem }}</td>
	        			<td>{{ $produktifitasteklist->tgl }}</td>
	        			<td>{{ $produktifitasteklist->laporan_status ? : 'NEED PROGRESS' }}</td>
	        			<td>{{ $produktifitasteklist->catatan }}</td>
	        		</tr>
	        		@endforeach
	        	</table>
	        </div>
	    </div>
	</div>
@endsection