<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table>
		<tr>
			<td>No. Order</td>
			<td>:</td>
			<td>{{ $getData->myir }}</td>
		</tr>

		<tr>
			<td>Tgl Order</td>
			<td>:</td>
			<td>{{ $getData->orderDate }}</td>
		</tr>

		<tr>
			<td>Nama Pemohon</td>
			<td>:</td>
			<td>{{ $getData->customer }}</td>
		</tr>

		<tr>
			<td>Nama Tagihan</td>
			<td>:</td>
			<td>{{ $getData->nama_tagihan }}</td>
		</tr>

		<tr>
			<td>Email</td>
			<td>:</td>
			<td>{{ $getData->email ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>KTP</td>
			<td>:</td>
			<td>{{ $getData->no_ktp ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>Nama Ibu</td>
			<td>:</td>
			<td>{{ $getData->nama_ibu ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>npwp</td>
			<td>:</td>
			<td>{{ $getData->npwp ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>Alamat</td>
			<td>:</td>
			<td>{{ $getData->alamatLengkap ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>Kelurahan</td>
			<td>:</td>
			<td>{{ $getData->kelurahan ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>Koordinat Pelanggan</td>
			<td>:</td>
			<td>{{ $getData->koorPelanggan ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>ODP</td>
			<td>:</td>
			<td>{{ $getData->namaOdp ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>Koordinat ODP</td>
			<td>:</td>
			<td>{{ $getData->koorOdp ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>Layanan</td>
			<td>:</td>
			<td>{{ $getData->layanan ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>Paket Indhome</td>
			<td>:</td>
			<td>{{ $getData->paket_harga ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>No. Internet</td>
			<td>:</td>
			<td>{{ $getData->no_internet ?: ' - ' }}</td>
		</tr>

		<tr>
			<td>Kcontack</td>
			<td>:</td>
			<td>{{ $getData->kcontack ?: ' - ' }}</td>
		</tr>

	</table>

<script>
    window.onload = window.print();
    // window.close();
 </script>
</body>
</html>
