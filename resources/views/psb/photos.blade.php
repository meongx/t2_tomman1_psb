@extends('layout')

@section('content')
  @include('partial.alerts')
    <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
      <input name="_method" type="hidden" value="PUT">
      <div id="rows" class="row text-center input-photos" style="margin: 20px 0">
    <label class="control-label"><b>Dokumentasi</b></label>
    <input type="file" id="files" class="hidden" name="photo[]" accept="image/jpeg" multiple />
    <button type="button" class="btn btn-sm btn-info choose">
      <i class="glyphicon glyphicon-camera"></i>
    </button>
    <button class="btn btn-sm btn-primary">Simpan</button><br/>
      </div>
      @foreach($th as $path)
        @if (file_exists($path))
          <?php
            $path = str_replace("/home/ta/htdocs/tomman1_psb/public","",$path);
            $hilang = "/".$id."th/";
            $img = str_replace($hilang, "/".$id."/", $path);
          ?>
          <div class="col-xs-2">
          <a href="{{ $img }}" target="_BLANK">
              <img src="{{ $path }}"/>
              </a>
              
            </div>
        @endif
      @endforeach
    </form>
    <script>
      $('input[type=file]').change(function() {
  console.log(this.name);
  var files = $('#files')[0].files; 
  for (var i = 0, f; f = files[i]; i++) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(this.files[i]);
    console.log(this.files[i]);
    oFReader.onload = function (oFREvent) {
      $('#rows').append('<div class="col-xs-2"><img id="pprev_'+ i +'" src="'+oFREvent.target.result+'" alt="" /></div>');
    };
  }   
      });
      $('.choose').on('click',  function() {
  $(this).parent().find('input[type=file]').click();
      });
    </script>
@endsection