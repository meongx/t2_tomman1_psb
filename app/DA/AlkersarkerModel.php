<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class AlkersarkerModel{
  public static function list_teknisi($id){
    $query = DB::SELECT('select *, (select qty from alkersarker_employee b where b.alkersarker_id=a.alkersarker_id and b.nik = "'.$id.'") as qty from alkersarker a');
    return $query;
  }
  public static function list_teknisi_avai($id){
    $query = DB::SELECT('select * from alkersarker_employee a left join alkersarker b ON a.alkersarker_id = b.alkersarker_id WHERE a.nik = "'.$id.'" AND a.qty > 0');
    return $query;
  }
  public static function get_alkersarker_employee($nik_tl){
    return DB::SELECT('
      SELECT
      *
      FROM
        1_2_employee a
      LEFT JOIN regu b ON (a.nik = b.nik1 OR a.nik = b.nik2)
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      WHERE
        b.ACTIVE="1" AND
        c.TL_NIK = "'.$nik_tl.'"
    ');

  }
  public static function get_alkersarker(){
    return DB::table('alkersarker')->get();
  }
  public static function save($nik,$alkersarker){
    $alkersarker = json_decode($alkersarker);
    foreach ($alkersarker as $data){
      if ($data->qty<>""){
          $check = DB::table('alkersarker_employee')
                        ->where('nik',$nik)
                        ->where('alkersarker_id',$data->alkersarker_id)
                        ->first();
          if ($check){
            $query = DB::table('alkersarker_employee')
                ->where('alkersarker_employee_id',$check->alkersarker_employee_id)
                ->update([
                  'qty' => $data->qty
                ]);
          } else {
            $query = DB::table('alkersarker_employee')
                ->insert([
                  'nik' => $nik,
                  'alkersarker_id' => $data->alkersarker_id,
                  'qty' => $data->qty
                ]);
          }
      }
    }
    return true;
  }
}

?>
