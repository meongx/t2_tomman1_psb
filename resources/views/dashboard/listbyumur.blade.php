@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
      th {
        background-color: #FF0000;
        color : #FFF;
        text-align: center;
        vertical-align: middle;
      }
      td {
        color : #000;
      }
    </style>

  <a href="/dashboard/assurance/{{ date('Y-m-d') }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List Assurance {{ $sektor }} {{ $durasi }}</h3>
  <div class="row">
    <div class="col-sm-12">
      <div>
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>No.</th>
          <th width=100>No Tiket</th>
          <th width=100>ND Internet</th>
          <th width=400>Tiket</th>
          <th>Tim</th>
          <th>Sektor</th>
          <th width=400>Order</th>
          <th width="500">Headline</th>
          <th>Tgl_Open</th>
          <th>Status Laporan</th>
          <th>STO</th>
          <th>Umur</th>
          <th>Loker Dispatch</th>
          <th>Channel</th>
          <th>Sebab</th>
          <th>Action</th>
          <th width="300">Catatan Tek.</th>
        </tr>
        @foreach ($getAssuranceList as $num => $AssuranceList)
        <tr>
          <td>{{ ++$num }}</td>
          <td><a href="/tiket/{{ $AssuranceList->id }}">{{ $AssuranceList->no_tiket }}</a></td>
          <td>{{ $AssuranceList->no_internet }}</td>
          <td>{{ $AssuranceList->updated_at }}</td>
          <td>
            {{ $AssuranceList->uraian }}
          </td>
          <td>
            {{ $AssuranceList->sektor }}
          </td>
          <td>
            {{ $AssuranceList->no_telp }} ~ {{ $AssuranceList->no_speedy }}<br />
          </td>
          <td>{{ $AssuranceList->headline }}</td>
          <td>{{ $AssuranceList->tgl_open }}</td>
          <td>{{ $AssuranceList->laporan_status ? : 'NO UPDATE' }}</td>
          <td>{{ $AssuranceList->sto_trim }}</td>
          <td>{{ $AssuranceList->hari*24 }}</td>
          <td>{{ $AssuranceList->loker_dispatch }}</td>
          <td>{{ $AssuranceList->channel }}</td>
          <td>{{ $AssuranceList->penyebab }}</td>
          <td>{{ $AssuranceList->action }}</td>
          <td>{{ $AssuranceList->catatan }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    </div>
  </div>
@endsection
