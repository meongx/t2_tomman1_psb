@extends('tech_layout')

@section('content')
  @include('partial.alerts')
  <style>
    .red {
      background-color: #FF0000;

    }
    .warning1 {
      background-color: #f39c12;
    }
    .warning2 {
      background-color: #e67e22;
    }
    .warning3 {
      background-color: #e74c3c;
    }


    .default {
      background-color: #888888;
    }
    .line {
      border-radius: 5px;
      padding : 3px;
    }
  </style>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmAwGwcaeJKLu7f3Noyhw2ihC8s8aaoPs"></script> -->

  <?php
    if (count($checkQCOndesk)>0){
      echo "<div class='alert alert-danger'> ANDA MEMILIKI ORDER REVISI</div>";
      $list = $checkQCOndesk;
    }
  ?>
  <ul class="nav nav-tabs" style="margin-bottom:20px;">
    <li class="{{ (Request::path() == '/') ? 'active' : '' }}"><a href="/">NEED PROGRESS</a></li>
    <li class="{{ (Request::segment(2) == 'KENDALA') ? 'active' : '' }}"><a href="/workorder/KENDALA">KENDALA</a></li>
    <li class="{{ (Request::segment(2) == 'UP') ? 'active' : '' }}"><a href="/workorder/UP">UP</a></li>
    <li class="{{ (Request::segment(1) == 'ibooster') ? 'active' : '' }}"><a href="/ibooster">IBOOSTER</a></li>
    <li class="{{ (Request::segment(1) == 'searchbarcode') ? 'active' : '' }}"><a href="/searchbarcode">BARCODE</a></li>
    <!-- <li><a href="/stok">Materials/NTE</a></li> -->
  </ul>


   <!-- bulan wo -->
  @if (Request::segment(2) == 'KENDALA' || Request::segment(2) == 'UP')
    <div class="panel panel-default">
      <div class="panel-body">
        <form method="GET">
            Bulan :
            <input type="text" name="bulan" id="bulan">
            <input type="submit" value="Filter" />
        </form><br>

      </div>
    </div>
  @endif

<!--   <ul class="nav nav-tabs" style="margin-bottom:20px;">
    <li class="{{ (Request::path() == '/') ? 'active' : '' }}"><a href="/">PROVISIONING</a></li>
    <li class="{{ (Request::segment(2) == 'KENDALA') ? 'active' : '' }}"><a href="/workorder/KENDALA">ASSURANCE</a></li>
    <li class="{{ (Request::segment(2) == 'UP') ? 'active' : '' }}"><a href="/workorder/UP">MIGRASI</a></li>
  </ul> -->

  <?php if ($updatenote->note<>"") : ?>
  <div class="alert alert-success"><?php echo ($updatenote->note) ?></div>
  <?php endif?>
  <?php if (count($checkONGOING)>0) : ?>

  <div class="content" >

    <?php foreach($checkONGOING as $ONGOING) : ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <?php
            if($ONGOING->dispatch_by==6){
                $order = 'INX'.$ONGOING->Ndem;
            }
            else{
                $order = $ONGOING->Ndem;
            }
         ?>

        Picked Up Order <span class="label label-info" style="font-size:12px;">{{ $order }}</span> <span class="label label-info" style="font-size:12px;">{{ $ONGOING->Service_No ? : $ONGOING->ndemSpeedy ? : $ONGOING->myir }}</span>
      </div>
      <div class="panel-content" style="padding:0px;">
        <div class="row">
          <div class="col-sm-12">
            <!-- <div id="Maps" class="Maps" style="height:200px;">
            </div> -->
            <a href="tel::{{ $ONGOING->Contact_Phone ? : $ONGOING->orderKontak }}" class="btn btn-default form-control">
              <span class="glyphicon glyphicon-earphone"></span> {{ $ONGOING->Contact_Name ? : "#NA" }} {{ $ONGOING->Contact_Phone ? : $ONGOING->orderKontak }}
            </a>
            <!-- Lokasi : {{ $ONGOING->lon }},{{ $ONGOING->lat }} -->
          </div>
          <div class="col-sm-12" style="padding-left:30px;padding-top:10px;padding-bottom:10px;">
            {{ $ONGOING->orderCity ? : 'DATA ALAMAT TIDAK LENGKAP' }}
          </div>
          <div class="col-sm-12">
          </div>
          <div class="col-sm-12">
            @if(strtoupper(substr($ONGOING->Ndem,0,1)) == "I")
              <a href="/tiket/{{ $ONGOING->id_dt }}" class="form-control responsive btn btn-sm btn-warning btn-bottom">CLICK HERE FOR REPORT</a>
            @elseif(strtoupper(substr($ONGOING->Ndem,0,1)) == "6" && $ONGOING->dispatch_by==6)
              <a href="/reboundary/{{ $ONGOING->id_dt }}" class="form-control responsive btn btn-sm btn-warning  btn-bottom">CLICK HERE FOR REPORT</a>
            @else
              <a href="/{{ $ONGOING->id_dt }}" class="form-control responsive btn btn-sm btn-warning  btn-bottom">CLICK HERE FOR REPORT</a>
            @endif
          </div>
        </div>
      </div>
    </div>
      <?php endforeach; ?>
      <br />
  </div>
  <?php endif; ?>
  <br />

  <div class="list-group" style="margin-top:-20px;padding:0px;">
    <?php
      if (count($list_ffg)>0) {
        echo "<div class='alert alert-danger'> ANDA MEMILIKI ORDER GANGGUAN FFG</div>";
        $list = $list_ffg;
      }

    ?>

            @foreach($list as $data)
                <!-- <div class="btn-group" role="group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Report to Group Sektor
                  <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    @foreach($group_telegram as $group)
                      <?php

                        if ($group->chat_id == session('auth')->mainsector) { $style = 'class="red"'; } else { $style = ''; }
                      ?>
                      <li <?php echo $style ?>><a href="/sendtotelegram/{{ $data->id_dt }}/{{ $group->chat_id }}">{{ $group->title }}</a></li>
                    @endforeach
                  </ul>
                </div> -->
              <?php
                if ($data->rebUmur>84){
                  $color = "warning3";
                } else if ($data->rebUmur>42 && $data->rebUmur<=84){
                  $color = "warning2";
                } else if ($data->rebUmur>24 && $data->rebUmur<=42){
                  $color = "warning1";
                } else {
                  $color = "default";
                }
              ?>
              @if($data->dispatch_by == '4')
                <div href="/guarantee/{{ $data->id_dt }}" class="list-group-item">
              @elseif($data->dispatch_by != '2')
                <div href="/{{ $data->id_dt }}" class="list-group-item">
              @else
                <div href="/tiket/{{ $data->id_dt }}" class="list-group-item">
              @endif
              <div class="linex {{ $color }}"></div>
              @if ($data->qcondesk_status == "rejected" and $data->status_laporan<>1)
              <p class="alert alert-warning">
                {{ $data->qcondesk_alasan }} <br />[Note from {{ $data->qcNama }} ({{ $data->qcPosition }})]
              </p>
              @endif

              @if (count($checkONGOING)==0)
                 @if ($data->dispatch_by=='6')
                    <a href="/reboundary/{{ $data->id_dt }}" class="responsive label label-warning">==> CLICK HERE FOR REPORT</a>
                 @elseif($data->dispatch_by != '2')
                    <a href="/{{ $data->id_dt }}" class="responsive label label-warning">==> CLICK HERE FOR REPORT</a>
                 @else
                    <a href="/tiket/{{ $data->id_dt }}" class="responsive label label-warning">==> CLICK HERE FOR REPORT</a>
                 @endif
               @endif
               <br />

               <?php
                if ($data->jenisPsb<>NULL && $data->dispatch_by<>4){
                  $orderId = "SC.".$data->orderId;
                  $jenisWO = $data->jenisPsb;
                } else if ($data->jenisPsb<>NULL && $data->dispatch_by==4){
                  $orderId = "FG.".$data->orderId;
                  $jenisWO = $data->jenisPsb;
                } else if ($data->ND<>NULL){
                  $orderId = $data->ND;
                  $jenisWO = "MIGRASI AS IS";
                } else if ($data->datek=="ONTOFFLINE"){
                  $orderId = "";
                  $jenisWO = "ONT OFFLINE";
                } else if ($data->dispatch_by==4){
                  $orderId = "FG.".$data->orderId;
                  $jenisWO = $data->jenisPsb;
                } else {
                  $orderId = $data->no_tiket;
                  $jenisWO = "ASSURANCE";
                };

                if($data->dispatch_by==6){
                    $orderId = 'INX'.$data->no_tiket;
                    $jenisWO = "REBOUNDARY";
                };

               ?>
               <span class="label label-info">{{ $jenisWO }}</span>
               <span class="label label-info">{{ $data->laporan_status ? : "NO UPDATE" }}</span>

                <br />

                Cretate At : <br />
                {{ $data->created_at }} <br />
                {{ $orderId ? : $data->no_tiket }}<br />
                @if ($data->no_tiket==NULL)
                @if ($data->no_tiket<>NULL)
                  ORDER OPEN : {{ $data->TROUBLE_OPENTIME ? : ''  ? : $data->tglOpen}}
                @else
                  ORDER OPEN : {{ $data->orderDate  }}
                @endif<br />

                STO : {{ $data->sto_dps ? : $data->neSTO ? : "UNKOWN" }}<br />
                ALPRO : {{ $data->alproname ? : $data->neRK ? :'' }}<br />
                @if ($data->no_tiket<>NULL)
                @else
                NCLI : {{ $data->orderNcli ? : "UNKNOWN" }}<br />
                @endif
                <strong>{{ $data->noTelp ? : $data->neND_TELP ? : $data->ND ? : '' }} ~ {{  $data->neND_INT ? : $data->internet ? : ''}}</strong><br />
                LAYANAN : {{ $data->jenis_layanan ? : $data->nePRODUK_GGN ? : "UNKNOWN" }}<br />
                <strong>{{ $data->orderName ? : $data->neLOKER_DISPATCH }} ({{ $data->orderKontak ? : $data->neHEADLINE }})</strong><br />
                <strong>Email Pelanggan : {{ $data->email ? : "~" }}</strong><br />
                {{ $data->orderCity ? : $data->neKANDATEL ? : "UNKNOWN" }}<br />
                @if ($data->jenisPsb<>NULL)
                LOCATION : {{ $data->lat }}, {{ $data->lon }}<br />
                @endif
                DURATION : {{ $data->duration ? : $data->rebUmur }} Jam<br />

                @if($data->dispatch_by == '' || $data->dispatch_by == '5' )
                    <strong>STO : {{ @$data->stoMy ?: '-' }}</strong><br>
                    <strong>ODP : {{ @$data->namaOdp ?: '-' }}</strong><br>
                    <strong>Koordinat Pelanggan : {{ @$data->kordinatPel ?: '-' }}</strong><br>
                    <strong>Alamat Sales : {{ @$data->alamatSales ?: '-' }}</strong><br>
                    <strong>Pelanggan : {{ @$data->customer }}</strong><br>

                    @if($data->dispatch_by=='')
                        <strong><u>No. Order : SC. {{ $data->Ndem }}</u></strong><br>
                    @endif

                    <strong>MYIR : {{ $data->myir ?: '-' }}</strong><br>
                    <strong>PIC Pelanggan : {{ @$data->picPelanggan ?: '-' }}</strong><br>
                    <strong>No. Internet : {{ @$data->no_internet ?: '-'}}</strong><br>
                    <strong>No. Telp : {{ @$data->no_telp }}</strong><br>
                    <strong>Sales : {{ @$data->kode_sales ?: '-' }} - {{ @$data->nama_sales ?: '-' }} </strong><br>

                @endif

               @if ($data->status_gangguan<>NULL)
               <span class="label label-danger">OPEN</span>
               @else

               @endif
               <span>{{ $data->KcontactSC ? : '' }}</span><br />

               @if ($data->jenisPsb<>NULL)
               MANJA : {{ $data->manja }}<br />
               @endif

               @if (!empty($data->pic))
                   {{ $data->pic }} <br />
               @endif

               @if ($data->dispatch_by == '4')
                 @if (empty($data->ndemPots))
                   <b> Keterangan Fullfillment Guarante : </b> <br />
                   <b>{{ $data->ketFull ?: '~'}}</b>
                 @endif
               @endif
               @else
               <table class="table">
                 <tr>
                   <td>{{ $data->Customer_Name }} - {{ $data->neND_INT }}</td>
                 </tr>
                 <tr>
                   <td>{{ $data->neHEADLINE }}</td>
                 </tr>
                 <tr>
                   <td>{{ $data->alamatGGN ? : '#NA' }}<br /></small></td>
                 </tr>
                 <tr>
                   <td>
                     <table cellpadding=2>
                       <tr>
                         <td>Tgl Create </td>
                         <td>: {{ $data->Reported_Date }}</td>
                       </tr>
                       <tr>
                         <td>Jadwal Visit </td>
                         <td>: {{ $data->Booking_Date }}</td>
                       </tr>
                     </table>

                   </td>
                 </tr>
               </table>
               <a href="tel::{{ $data->Contact_Phone ? : '0' }}" class="btn btn-default form-control">
                 <span class="glyphicon glyphicon-earphone"></span> {{ $data->Contact_Name }} ~ {{ $data->Contact_Phone ? : '0' }}
               </a>
               @endif
              <?php
                //if ($data->headline<>'') :
                //$headline = explode('+',$data->headline);
                // echo "<b><i>".$data->headline."</i></b><br />";
                //for($i=0;$i<4;$i++){
                //  echo $headline[$i]."<br />";
                //}
                //echo "Link : ";
                //if ($headline[7]=="ONLINE"){
                //  $status_link = "success";
                //} else {
                //  $status_link = "danger";
                //}
                //echo '<span class="label label-'.$status_link.'">'.$headline[7].'</span>';
                //echo ' INET : ';
                //if ($headline[9]=="Start"){
                //  $status_link = "success";
                //} else {
                //  $status_link = "danger";
                //}
                //echo '<span class="label label-'.$status_link.'">'.$headline[9].'</span>';
              //endif;
              ?>

              </div>
              <br />

            @endforeach

  </div>

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script>
  (function() {
    var day = {
            format : 'yyyy-mm',
            viewMode: "months",
            minViewMode: "months",
            autoClose: true
          };

        $('#bulan').datepicker(day).on('changeDate', function(e){
          $(this).datepicker('hide');
        });

    var initMap = function(lat, lon) {
      var marker = null;
      var center = new google.maps.LatLng(lat, lon),
          withMarker = (!isNaN(lat) && !isNaN(lon)) && (lat != 0 && lon != 0),
          zoom = withMarker ? 18 : 12;
      map = new google.maps.Map(
          document.getElementById('Maps'),
          {
              center: center,
              zoom: zoom,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          }
      );
    }
    var showMarker = function(lat,lng) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat,lng),
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP
            });
      }
      <?php if (count($checkONGOING)>0) :
         foreach($checkONGOING as $ONGOING) : ?>
    initMap({{ $ONGOING->lat ? : '-3.332081'}},{{ $ONGOING->lon ? : '114.67323'}});
    showMarker({{ $ONGOING->lat ? : '-3.332081'}},{{ $ONGOING->lon ? : '114.67323'}});
    <?php endforeach; endif; ?>
  })();
  </script>
@endsection
