@extends('layout')

@section('header')
@endsection

@section('content')
  @include('partial.alerts')

  <form method="POST" >
      <div class="panel panel-primary">
        <div class="panel-heading">Input Myir</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                          <label>Tracker ID</label>
                          <input type="text" class="form-control" name="trackerId" id="trackerId" value="{{ old('trackerId') }}">
                          {!! $errors->first('trackerId', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                <div class="col-md-6">
                      <div class="form-group">
                          <label>Channel</label>
                          <input type="hidden" class="form-control" name="channel" id="channel" value="{{ old('channel') }}">
                          {!! $errors->first('channel', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                          <label>Kode Sales</label>
                          <input type="text" class="form-control" name="kodeSales" id="kodeSales" value="{{ $kodeSales }}" readonly>
                          {!! $errors->first('kodeSales', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                <div class="col-md-6">
                      <div class="form-group">
                          <label>Tgl Registrasi</label>
                          <input type="text" class="form-control" name="tglRegistrasi" id="tglRegistrasi" value="{{ date('Y-m-d') }}">
                          {!! $errors->first('tglRegistrasi', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                          <label>Nama Pelanggan</label>
                          <input type="text" class="form-control" name="nmPelanggan" id="nmPelanggan" value="{{ old('nmPelanggan') }}">
                          {!! $errors->first('nmPelanggan', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                <div class="col-md-6">
                      <div class="form-group">
                          <label>Jenis Layanan</label>
                          <input type="hidden" class="form-control" name="jnsLayanan" id="jnsLayanan" value="{{ old('jnsLayanan') }}">
                          {!! $errors->first('jnsLayanan', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                          <label>ODP</label>
                          <input type="text" class="form-control" name="odp" id="odp" value="{{ old('odp') }}">
                          {!! $errors->first('odp', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                <div class="col-md-6">
                      <div class="form-group">
                          <label>Koordinat Pelanggan</label>
                          <input type="text" class="form-control" name="koorPelanggan" id="koorPelanggan" value="{{ old('koorPelanggan') }}">
                          {!! $errors->first('koorPelanggan', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                      <div class="form-group">
                          <label>Alamat Lengkap</label>
                          <input type="text" class="form-control" name="alamat" id="alamat" value="{{ old('alamat') }}">
                          {!! $errors->first('alamat', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                <div class="col-md-4">
                      <div class="form-group">
                          <label>PIC Pelanggan</label>
                          <input type="text" class="form-control" name="picPelanggan" id="picPelanggan" value="{{ old('picPelanggan') }}">
                          {!! $errors->first('picPelanggan', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

                <div class="col-md-4">
                      <div class="form-group">
                          <label>Transaksi PSB</label>
                          <input type="text" class="form-control" name="transaksiPsb" id="transaksiPsb" value="{{ old('transaksiPsb') }}">
                          {!! $errors->first('transaksiPsb', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>
            </div>

            <input type="submit" class="btn btn-primary" value="Simpan">
            <a href="/sales/listhi" class="btn btn-default">Batal</a>
        </div>
        
      </div>
  </form>

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
      $(document).ready(function(){
          var channelData = [
            {"id":"salesAgen", "text":"Sales Agen"},
            {"id":"plasa", "text":"Plasa"},
            {"id":"canvaser", "text":"Canvaser"},
            {"id":"cap", "text":"Cap"},

          ];
          
          var channel = function() {
            return {
              data: channelData,
              placeholder: 'Input Channel',
              formatSelection: function(data) { return data.text },
              formatResult: function(data) {
                return  data.text;
              }
            }
          }

          $('#channel').select2(channel());

          var layananData = [
            {"id":"2PINETUSEETV", "text":"AO INTERNET + IPTV"},
            {"id":"2PINETVOICE", "text":"AO INTERNET + VOICE"},
            {"id":"3P", "text":"AO INTERNET + VOICE + IPTV"},
          ];
          
          var layanan = function() {
            return {
              data: layananData,
              placeholder: 'Input Channel',
              formatSelection: function(data) { return data.text },
              formatResult: function(data) {
                return  data.text;
              }
            }
          }

          $('#jnsLayanan').select2(layanan());

          var psbData = [
            {"id":"dtd", "text":"DTD"},
            {"id":"openTabel", "text":"Open Tabel"},
            {"id":"sosmed", "text":"Sosmed"},
            {"id":"referensi", "text":"Refrensi"},
            {"id":"dihubungi", "text":"Dihubungi"},
            {"id":"plasa", "text":"Plasa"},
            {"id":"downline", "text":"Downline"},
          ];
          
          var psb = function() {
            return {
              data: psbData,
              placeholder: 'Input Transaksi PSB',
              formatSelection: function(data) { return data.text },
              formatResult: function(data) {
                return  data.text;
              }
            }
          }
          
          $('#transaksiPsb').select2(psb());
          $("#odp").inputmask("AAA-AAA-A{2,3}/999");

          $('#tglRegistrasi').datepicker({
            format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
          }).datepicker("setDate", "today");

      });
  </script>

@endsection
  