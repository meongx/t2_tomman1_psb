@extends('layout')

@section('content')
  @include('partial.alerts')

  <div class="row">
				<div class="col-md-12">

					<!-- Default panel -->
					<div class="panel panel-default">
						<div class="panel-heading">
							DECLINE INFORMATION
						</div>
            <div class="panel-body">
              <form method="post">
                <input type="hidden" name="absen_id" value="{{ $status_absen_by_id->absen_id }}" />
              <table class="table">
                <tr>
                  <td>NIK</td>
                  <td>{{ $status_absen_by_id->nik }}</td>
                </tr>

                <tr>
                  <td>NAME</td>
                  <td>{{ $status_absen_by_id->nama }}</td>
                </tr>

                <tr>
                  <td>TIME</td>
                  <td>{{ $status_absen_by_id->date_created }}</td>
                </tr>

                <tr>
                  <td>REASON</td>
                  <td>
                    <textarea name="keterangan" rows="8" cols="80"></textarea>
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td><button class="btn btn-danger">DECLINE</button></td>
                </tr>
              </table>
            </form>
            </div>
          </div>
        </div>
  </div>
@endsection
