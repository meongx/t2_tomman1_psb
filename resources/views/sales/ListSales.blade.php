@extends('layout')

@section('content')
  @include('partial.alerts')
  
  <!-- Modal -->
  <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Sales</h5>
        </div>
        <div class="modal-body">
            <div id="pesan-error"></div>
            <form method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="POST">

                <div class="form-group">
                    <label class="label-control" for="kode">Kode Sales</label>
                    <input type="text" name="kode" id="kode" class="form-control">
                </div>

                <div class="form-group">
                    <label class="label-control" for="nama">Nama Sales</label>
                    <input type="text" name="nama" id="nama" class="form-control">
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-primary" id="btn-simpan">Simpan</button>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-primary">
      <div class="panel-heading">
          List Sales
      </div>

      <div class="panel-body">
        <h3>
            <button class="btn btn-info" id="addData">
              <span class="glyphicon glyphicon-plus"></span>
            </button>
            Sales
        </h3>

        <form class="row" style="margin-bottom: 20px">
          <div class="col-md-12">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Cari Berdasarkan Kode / Nama Sales" name="cari"/>
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">
                      <span class="glyphicon glyphicon-search"></span>
                    </button>
                  </span>
              </div>
          </div>
        </form>

        <div class="list-group">
          @foreach($getData as $data)
            <a href="" class="list-group-item">
              <strong>{{ $data->kode_sales }}</strong><br>
              <span>{{ $data->nama_sales }}</span>
            </a>
          @endforeach
        </div>
      </div>
  </div>
@endsection

@section('plugins')
  <script>
      $(function(){
          $('#addData').click(function(){
              $('#modal').modal('show')
          });

          $('#btn-simpan').click(function(){
              let url    = '/list/sales',
                  method = 'POST',
                  form   = $('.modal-body form');
                  data   = form.serialize();

              form.find('.form-group').removeClass('has-error');
              form.find('.help-block').remove();
            
              $.ajax({
                  url : url,
                  method : method,
                  data : data,
                  success : function(response){
                      alert('Simpan Sukses');
                      location.reload();
                  },
                  error : function(xhr){
                      let err = xhr.responseJSON;
                      if ($.isEmptyObject(err)==false){
                          if (err.exist != 'ada'){
                            $.each(err,function(key, value){
                                $('#'+key).closest('.form-group').addClass('has-error').append('<span class="help-block"><strong>'+value+'</strong></span>');
                            })
                          }
                          else{
                            $('#pesan-error').addClass('alert alert-danger').append('<spas>Kode Sales Sudah Ada</span>')
                          }
                      }
                  }
              })
          })
      })
  </script>
@endsection